/*
MySQL - 5.5.57-MariaDB : Database - quotientdb
*********************************************************************
*/


/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`quotientdb` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `quotientdb`;

/*Table structure for table `CAROUSEL_COUPONS` */

DROP TABLE IF EXISTS `CAROUSEL_COUPONS`;

CREATE TABLE `CAROUSEL_COUPONS` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `position` varchar(45) DEFAULT NULL,
  `date_time_created` datetime DEFAULT NULL,
  `date_time_modified` datetime DEFAULT NULL,
  `start_date_time` datetime DEFAULT NULL,
  `end_date_time` datetime DEFAULT NULL,
  `carousel_coupons_price` varchar(45) DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  `coupons_id` int(11) NOT NULL,
  `created_by_cms_users_id` int(11) NOT NULL,
  `modified_by_cms_users_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_Carousel_Coupons_Coupons1_idx` (`coupons_id`),
  CONSTRAINT `fk_CAROUSEL_COUPONS_COUPONS1` FOREIGN KEY (`coupons_id`) REFERENCES `COUPONS` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `CB_CONSUMER_BALANCE` */

DROP TABLE IF EXISTS `CB_CONSUMER_BALANCE`;

CREATE TABLE `CB_CONSUMER_BALANCE` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cashback_balance_amount` varchar(45) DEFAULT NULL,
  `date_time_created` datetime DEFAULT NULL,
  `date_time_modified` datetime DEFAULT NULL,
  `consumer_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_CONSUMER_CASHBACK_BALANCE_CONSUMER1_idx` (`consumer_id`),
  CONSTRAINT `fk_CB_CONSUMER_BALANCE_CONSUMER1` FOREIGN KEY (`consumer_id`) REFERENCES `CONSUMER` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `CB_COUPON_STATUS` */

DROP TABLE IF EXISTS `CB_COUPON_STATUS`;

CREATE TABLE `CB_COUPON_STATUS` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` int(11) DEFAULT NULL,
  `title` varchar(45) DEFAULT NULL,
  `date_time_created` datetime DEFAULT NULL,
  `date_time_modified` datetime DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  `created_by_cms_users_id` int(11) NOT NULL,
  `modified_by_cms_users_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `CB_OFFERS` */

DROP TABLE IF EXISTS `CB_OFFERS`;

CREATE TABLE `CB_OFFERS` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(45) DEFAULT NULL,
  `description` varchar(45) DEFAULT NULL,
  `url` varchar(45) DEFAULT NULL,
  `date_time_created` datetime DEFAULT NULL,
  `date_time_modified` datetime DEFAULT NULL,
  `start_date_time` datetime DEFAULT NULL,
  `end_date_time` datetime DEFAULT NULL,
  `cashback_in_percentage` int(11) DEFAULT NULL,
  `created_by_cms_user_id` int(11) NOT NULL,
  `merchant_id` int(11) NOT NULL,
  `cb_coupon_status_id` int(11) NOT NULL,
  `modified_by_cms_users_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_Cashback_Offers_Cashback_Coupon_Status1_idx` (`cb_coupon_status_id`),
  KEY `fk_CASHBACK_OFFERS_MERCHANT1_idx` (`merchant_id`),
  KEY `fk_CASHBACK_OFFERS_ADB_USERS1_idx` (`created_by_cms_user_id`),
  CONSTRAINT `fk_CB_OFFERS_CASHBACK_COUPON_STATUS1` FOREIGN KEY (`cb_coupon_status_id`) REFERENCES `CB_COUPON_STATUS` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_CB_OFFERS_MERCHANT1` FOREIGN KEY (`merchant_id`) REFERENCES `MERCHANT` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_CB_OFFERS_CMS_USERS1` FOREIGN KEY (`created_by_cms_user_id`) REFERENCES `CMS_USERS` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `CB_RETURNS` */

DROP TABLE IF EXISTS `CB_RETURNS`;

CREATE TABLE `CB_RETURNS` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date_time_created` datetime DEFAULT NULL,
  `date_time_modified` datetime DEFAULT NULL,
  `cashback_previous_amount` varchar(45) DEFAULT NULL,
  `cashback_balance_amount` varchar(45) DEFAULT NULL,
  `payout_request_id` int(11) NOT NULL,
  `consumer_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_CASHBACK_RETURNS_PAYOUT_REQUEST1_idx` (`payout_request_id`),
  KEY `fk_CASHBACK_RETURNS_CONSUMER1_idx` (`consumer_id`),
  CONSTRAINT `fk_CB_RETURNS_PAYOUT_REQUEST1` FOREIGN KEY (`payout_request_id`) REFERENCES `PAYOUT_REQUEST` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_CB_RETURNS_CONSUMER1` FOREIGN KEY (`consumer_id`) REFERENCES `CONSUMER` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `CLICKS` */

DROP TABLE IF EXISTS `CLICKS`;

CREATE TABLE `CLICKS` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `count` int(11) NOT NULL,
  `browser_id` int(11) DEFAULT NULL,
  `date_time_created` datetime DEFAULT NULL,
  `consumer_id` int(11) NOT NULL,
  `session_id` int(11) NOT NULL,
  `coupons_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_Clicks_Coupons1_idx` (`coupons_id`),
  KEY `fk_Clicks_Consumer1_idx` (`consumer_id`),
  KEY `fk_CLICKS_SESSION1_idx` (`session_id`),
  CONSTRAINT `fk_CLICKS_COUPONS1` FOREIGN KEY (`coupons_id`) REFERENCES `COUPONS` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_CLICKS_CONSUMER1` FOREIGN KEY (`consumer_id`) REFERENCES `CONSUMER` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_CLICKS_SESSION1` FOREIGN KEY (`session_id`) REFERENCES `SESSION` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `CMS_BLOCK_HISTORY` */

DROP TABLE IF EXISTS `CMS_BLOCK_HISTORY`;

CREATE TABLE `CMS_BLOCK_HISTORY` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `block_ip` int(11) DEFAULT NULL,
  `block_date_time` datetime DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  `date_time_created` datetime DEFAULT NULL,
  `date_time_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `CMS_ROLE_PRIVILEGES` */

DROP TABLE IF EXISTS `CMS_ROLE_PRIVILEGES`;

CREATE TABLE `CMS_ROLE_PRIVILEGES` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date_time_created` datetime DEFAULT NULL,
  `date_time_modified` datetime DEFAULT NULL,
  `cms_user_activity_id` int(11) NOT NULL,
  `cms_user_roles` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_CMS_ROLE_PRIVILEGES_CMS_USER_ACTIVITY1_idx` (`cms_user_activity_id`),
  KEY `fk_CMS_ROLE_PRIVILEGES_CMS_USER_ROLES1_idx` (`cms_user_roles`),
  CONSTRAINT `fk_CMS_ROLE_PRIVILEGES_CMS_USER_ACTIVITY1` FOREIGN KEY (`cms_user_activity_id`) REFERENCES `CMS_USER_ACTIVITY` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_CMS_ROLE_PRIVILEGES_CMS_USER_ROLES1` FOREIGN KEY (`cms_user_roles`) REFERENCES `CMS_USER_ROLES` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `CMS_USERS` */

DROP TABLE IF EXISTS `CMS_USERS`;

CREATE TABLE `CMS_USERS` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(45) DEFAULT NULL,
  `password` varchar(45) DEFAULT NULL,
  `date_time_created` datetime DEFAULT NULL,
  `date_time_modified` datetime DEFAULT NULL,
  `email_address` varchar(45) DEFAULT NULL,
  `first_name` varchar(45) DEFAULT NULL,
  `last_name` varchar(45) DEFAULT NULL,
  `phone` varchar(45) DEFAULT NULL,
  `failed_pwd_attempts` int(11) DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  `created_by_cms_users_id` int(11) NOT NULL,
  `modified_by_cms_users_id` int(11) NOT NULL,
  `cms_user_types_id` int(11) NOT NULL,
  `cms_user_status_id` int(11) NOT NULL,
  `cms_user_roles_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_ADB_USERS_ADB_USER_TYPES1_idx` (`cms_user_types_id`),
  KEY `fk_ADB_USERS_ADB_USER_STATUS1_idx` (`cms_user_status_id`),
  KEY `fk_CMS_USERS_CMS_USER_ROLES1_idx` (`cms_user_roles_id`),
  CONSTRAINT `fk_CMS_USERS_CMS_USER_TYPES1` FOREIGN KEY (`cms_user_types_id`) REFERENCES `CMS_USER_TYPES` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_CMS_USERS_CMS_USER_STATUS1` FOREIGN KEY (`cms_user_status_id`) REFERENCES `CMS_USER_STATUS` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_CMS_USERS_CMS_USER_ROLES1` FOREIGN KEY (`cms_user_roles_id`) REFERENCES `CMS_USER_ROLES` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `CMS_USER_ACTIVITY` */

DROP TABLE IF EXISTS `CMS_USER_ACTIVITY`;

CREATE TABLE `CMS_USER_ACTIVITY` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` int(11) DEFAULT NULL,
  `title` varchar(45) DEFAULT NULL,
  `description` varchar(45) DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  `created_by_cms_users_id` int(11) NOT NULL,
  `modified_by_cms_users_id` int(11) NOT NULL,
  `date_time_created` datetime DEFAULT NULL,
  `date_time_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `CMS_USER_ACTIVITY_LOGS` */

DROP TABLE IF EXISTS `CMS_USER_ACTIVITY_LOGS`;

CREATE TABLE `CMS_USER_ACTIVITY_LOGS` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `start_date_time` datetime DEFAULT NULL,
  `end_date_time` datetime DEFAULT NULL,
  `date_time_created` datetime DEFAULT NULL,
  `cms_user_activity_id` int(11) NOT NULL,
  `cms_users_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_CMS_USER_ACTIVITY_LOGS_CMS_USER_ACTIVITY1_idx` (`cms_user_activity_id`),
  KEY `fk_CMS_USER_ACTIVITY_LOGS_CMS_USER_ID1_idx` (`cms_users_id`),
  CONSTRAINT `fk_CMS_USER_ACTIVITY_LOGS_CMS_USER_ACTIVITY1` FOREIGN KEY (`cms_user_activity_id`) REFERENCES `CMS_USER_ACTIVITY` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_CMS_USER_ACTIVITY_LOGS_CMS_USERS1` FOREIGN KEY (`cms_users_id`) REFERENCES `CMS_USERS` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `CMS_USER_ROLES` */

DROP TABLE IF EXISTS `CMS_USER_ROLES`;

CREATE TABLE `CMS_USER_ROLES` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(45) DEFAULT NULL,
  `active` tinyint(4) DEFAULT NULL,
  `disabled` tinyint(4) DEFAULT NULL,
  `date_time_created` datetime DEFAULT NULL,
  `date_time_modified` datetime DEFAULT NULL,
  `created_by_cms_users_id` int(11) NOT NULL,
  `modified_by_cms_users_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `CMS_USER_STATUS` */

DROP TABLE IF EXISTS `CMS_USER_STATUS`;

CREATE TABLE `CMS_USER_STATUS` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(45) DEFAULT NULL,
  `code` int(11) DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  `created_by_cms_users_id` int(11) NOT NULL,
  `modified_by_cms_users_id` int(11) NOT NULL,
  `date_time_created` datetime DEFAULT NULL,
  `date_time_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `CMS_USER_SYSTEM_IP` */

DROP TABLE IF EXISTS `CMS_USER_SYSTEM_IP`;

CREATE TABLE `CMS_USER_SYSTEM_IP` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date_time_created` datetime DEFAULT NULL,
  `date_time_modified` datetime DEFAULT NULL,
  `cms_users_id` int(11) NOT NULL,
  `cms_block_history_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_ADB_USER_SYSTEM_IP_ADB_BLOCK_HISTORY1_idx` (`cms_block_history_id`),
  KEY `fk_ADB_USER_SYSTEM_IP_idx` (`cms_users_id`),
  CONSTRAINT `fk_CMS_USER_SYSTEM_IP_CMS_BLOCK_HISTORY1` FOREIGN KEY (`cms_block_history_id`) REFERENCES `CMS_BLOCK_HISTORY` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_CMS_USER_SYSTEM_IP_CMS_USERS1` FOREIGN KEY (`cms_users_id`) REFERENCES `CMS_USERS` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `CMS_USER_TYPES` */

DROP TABLE IF EXISTS `CMS_USER_TYPES`;

CREATE TABLE `CMS_USER_TYPES` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(45) DEFAULT NULL,
  `code` int(11) DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  `created_by_cms_users_id` int(11) NOT NULL,
  `modified_by_cms_users_id` int(11) NOT NULL,
  `date_time_created` datetime DEFAULT NULL,
  `date_time_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `COMMISSION` */

DROP TABLE IF EXISTS `COMMISSION`;

CREATE TABLE `COMMISSION` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `commission_amount` varchar(45) DEFAULT NULL,
  `date_time_created` datetime DEFAULT NULL,
  `date_time_modified` datetime DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  `commission_type_id` int(11) NOT NULL,
  `coupons_id` int(11) NOT NULL,
  `shopping_visits_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_Commission_commission_type1_idx` (`commission_type_id`),
  KEY `fk_COMMISSION_COUPONS_idx` (`coupons_id`),
  KEY `fk_COMMISSION_SHOPPING_VISITS1_idx` (`shopping_visits_id`),
  CONSTRAINT `fk_COMMISSION_COMMISSION_TYPE1` FOREIGN KEY (`commission_type_id`) REFERENCES `COMMISSION_TYPE` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_COMMISSION_COUPONS1` FOREIGN KEY (`coupons_id`) REFERENCES `COUPONS` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_COMMISSION_SHOPPING_VISITS1` FOREIGN KEY (`shopping_visits_id`) REFERENCES `SHOPPING_VISITS` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `COMMISSION_TYPE` */

DROP TABLE IF EXISTS `COMMISSION_TYPE`;

CREATE TABLE `COMMISSION_TYPE` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(45) DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  `date_time_created` datetime DEFAULT NULL,
  `date_time_modified` datetime DEFAULT NULL,
  `created_by_cms_users_id` int(11) NOT NULL,
  `modified_by_cms_users_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `CONSUMER` */

DROP TABLE IF EXISTS `CONSUMER`;

CREATE TABLE `CONSUMER` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email_address` varchar(45) DEFAULT NULL,
  `first_name` varchar(45) DEFAULT NULL,
  `last_name` varchar(45) DEFAULT NULL,
  `password` varchar(45) DEFAULT NULL,
  `card_count` int(11) DEFAULT NULL,
  `phone_number` int(12) DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  `date_time_created` datetime DEFAULT NULL,
  `date_time_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `CONSUMER_CARD_DETAILS` */

DROP TABLE IF EXISTS `CONSUMER_CARD_DETAILS`;

CREATE TABLE `CONSUMER_CARD_DETAILS` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `card_number` int(11) DEFAULT NULL,
  `date_time_expiry` datetime DEFAULT NULL,
  `date_time_created` datetime DEFAULT NULL,
  `date_time_modified` datetime DEFAULT NULL,
  `card_name` varchar(45) DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  `consumer_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_CONSUMER_CARD_DETAILS_CONSUMER_idx` (`consumer_id`),
  CONSTRAINT `fk_CONSUMER_CARD_DETAILS_CONSUMER` FOREIGN KEY (`consumer_id`) REFERENCES `CONSUMER` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `CONSUMER_CASHBACKS` */

DROP TABLE IF EXISTS `CONSUMER_CASHBACKS`;

CREATE TABLE `CONSUMER_CASHBACKS` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cashback_amount` datetime DEFAULT NULL,
  `date_time_created` datetime DEFAULT NULL,
  `consumer_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_CONSUMER_CASHBACKS_CONSUMER1_idx` (`consumer_id`),
  CONSTRAINT `fk_CONSUMER_CASHBACKS_CONSUMER1` FOREIGN KEY (`consumer_id`) REFERENCES `CONSUMER` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `CONSUMER_FAVORITE_STORES` */

DROP TABLE IF EXISTS `CONSUMER_FAVORITE_STORES`;

CREATE TABLE `CONSUMER_FAVORITE_STORES` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` varchar(45) DEFAULT NULL,
  `date_time_created` datetime DEFAULT NULL,
  `date_time_modifed` datetime DEFAULT NULL,
  `consumer_id` int(11) NOT NULL,
  `merchant_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_CONSUMER_FAVORITE_STORES_CONSUMER_ID_idx` (`consumer_id`),
  KEY `fk_CONSUMER_FAVORITE_STORES_MERCHANT_ID_idx` (`merchant_id`),
  CONSTRAINT `fk_CONSUMER_FAVORITE_STORES_CONSUMER1` FOREIGN KEY (`consumer_id`) REFERENCES `CONSUMER` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_CONSUMER_FAVORITE_STORES_MERCHANT1` FOREIGN KEY (`merchant_id`) REFERENCES `MERCHANT` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `COUPONS` */

DROP TABLE IF EXISTS `COUPONS`;

CREATE TABLE `COUPONS` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(45) DEFAULT NULL,
  `description` varchar(45) DEFAULT NULL,
  `url` varchar(45) DEFAULT NULL,
  `start_date_time` datetime DEFAULT NULL,
  `end_date_time` datetime DEFAULT NULL,
  `date_time_created` datetime DEFAULT NULL,
  `date_time_modified` datetime DEFAULT NULL,
  `ongoing_offer` varchar(45) DEFAULT NULL,
  `free_shipping` varchar(45) DEFAULT NULL,
  `alerts_status` varchar(45) DEFAULT NULL,
  `staff_pick` varchar(45) DEFAULT NULL,
  `expire_date_visibility` int(11) DEFAULT NULL,
  `click_count_visibility` int(11) DEFAULT NULL,
  `saving_amount_visibility` int(11) DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  `created_by_cms_users_id` int(11) NOT NULL,
  `modified_by_cms_users_id` int(11) NOT NULL,
  `merchant_id` int(11) NOT NULL,
  `coupons_priority_id` int(11) NOT NULL,
  `coupons_status_id` int(11) NOT NULL,
  `coupons_offer_types_id` int(11) NOT NULL,
  `commission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_Coupons_Coupons_Priority1_idx` (`coupons_priority_id`),
  KEY `fk_Coupons_Coupons_Status1_idx` (`coupons_status_id`),
  KEY `fk_Coupons_Coupons_Offer_Types1_idx` (`coupons_offer_types_id`),
  KEY `fk_Coupons_Merchant1_idx` (`merchant_id`),
  KEY `fk_Coupons_Commission1_idx` (`commission_id`),
  KEY `fk_COUPONS_ADB_USERS1_idx` (`created_by_cms_users_id`),
  CONSTRAINT `fk_COUPONS_COUPONS_PRIORITY1` FOREIGN KEY (`coupons_priority_id`) REFERENCES `COUPONS_PRIORITY` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_COUPONS_COUPONS_STATUS1` FOREIGN KEY (`coupons_status_id`) REFERENCES `COUPONS_STATUS` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_COUPONS_COUPONS_OFFER_TYPES1` FOREIGN KEY (`coupons_offer_types_id`) REFERENCES `COUPONS_OFFER_TYPES` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_COUPONS_MERCHANT1` FOREIGN KEY (`merchant_id`) REFERENCES `MERCHANT` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_COUPONS_COMMISSION1` FOREIGN KEY (`commission_id`) REFERENCES `COMMISSION` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_COUPONS_CMS_USERS1` FOREIGN KEY (`created_by_cms_users_id`) REFERENCES `CMS_USERS` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `COUPONS_OFFER_TYPES` */

DROP TABLE IF EXISTS `COUPONS_OFFER_TYPES`;

CREATE TABLE `COUPONS_OFFER_TYPES` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` int(11) DEFAULT NULL,
  `title` varchar(45) DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  `date_time_created` datetime DEFAULT NULL,
  `date_time_modified` datetime DEFAULT NULL,
  `created_by_cms_users_id` int(11) NOT NULL,
  `modified_by_cms_users_id` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `COUPONS_PRIORITY` */

DROP TABLE IF EXISTS `COUPONS_PRIORITY`;

CREATE TABLE `COUPONS_PRIORITY` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` int(11) DEFAULT NULL,
  `title` varchar(45) DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  `date_time_created` datetime DEFAULT NULL,
  `date_time_modified` datetime DEFAULT NULL,
  `created_by_cms_users_id` int(11) NOT NULL,
  `modified_by_cms_users_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `COUPONS_STATUS` */

DROP TABLE IF EXISTS `COUPONS_STATUS`;

CREATE TABLE `COUPONS_STATUS` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` int(11) DEFAULT NULL,
  `title` varchar(45) DEFAULT NULL,
  `date_time_created` datetime DEFAULT NULL,
  `date_time_modifed` datetime DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  `created_by_cms_users_id` int(11) NOT NULL,
  `modified_by_cms_users_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `FAVOURITE_STORE_COUPON` */

DROP TABLE IF EXISTS `FAVOURITE_STORE_COUPON`;

CREATE TABLE `FAVOURITE_STORE_COUPON` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date_time_created` datetime DEFAULT NULL,
  `date_time_modified` datetime DEFAULT NULL,
  `start_date_time` datetime DEFAULT NULL,
  `end_date_time` datetime DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  `merchant_id` int(11) NOT NULL,
  `coupons_id` int(11) NOT NULL,
  `created_by_cms_users_id` int(11) NOT NULL,
  `modified_by_cms_users_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_FAVOURITE_STORE_COUPON_MERCHANT1_idx` (`merchant_id`),
  KEY `fk_FAVOURITE_STORE_COUPON_COUPONS1_idx` (`coupons_id`),
  CONSTRAINT `fk_FAVOURITE_STORE_COUPON_MERCHANT1` FOREIGN KEY (`merchant_id`) REFERENCES `MERCHANT` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_FAVOURITE_STORE_COUPON_COUPONS1` FOREIGN KEY (`coupons_id`) REFERENCES `COUPONS` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `HOME_PAGE_COUPON_PLACEMENT` */

DROP TABLE IF EXISTS `HOME_PAGE_COUPON_PLACEMENT`;

CREATE TABLE `HOME_PAGE_COUPON_PLACEMENT` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `position` varchar(45) DEFAULT NULL,
  `date_time_created` datetime DEFAULT NULL,
  `date_time_modified` datetime DEFAULT NULL,
  `paid_placements_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_Home_Page_Coupon_Placement_Paid_Placements1_idx` (`paid_placements_id`),
  CONSTRAINT `fk_HOMEPAGE_COUPON_PLACEMENT_PAID_PLACEMENT1` FOREIGN KEY (`paid_placements_id`) REFERENCES `PAID_PLACEMENTS` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `MAILING_ADDRESS` */

DROP TABLE IF EXISTS `MAILING_ADDRESS`;

CREATE TABLE `MAILING_ADDRESS` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `street_address_1` varchar(45) DEFAULT NULL,
  `street_address_2` varchar(45) DEFAULT NULL,
  `city` varchar(45) DEFAULT NULL,
  `country` varchar(45) DEFAULT NULL,
  `state_province` varchar(45) DEFAULT NULL,
  `postal_code` int(11) DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  `date_time_created` datetime DEFAULT NULL,
  `date_time_modified` datetime DEFAULT NULL,
  `consumer_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_Mailing_Address_Consumer1_idx` (`consumer_id`),
  CONSTRAINT `fk_MAILING_ADDRESS_CONSUMER1` FOREIGN KEY (`consumer_id`) REFERENCES `CONSUMER` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `MERCHANT` */

DROP TABLE IF EXISTS `MERCHANT`;

CREATE TABLE `MERCHANT` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `affiliate_network_id` int(11) DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  `country` varchar(45) DEFAULT NULL,
  `url` varchar(45) DEFAULT NULL,
  `description` varchar(45) DEFAULT NULL,
  `short_description` varchar(45) DEFAULT NULL,
  `network_advertiser_id` int(11) DEFAULT NULL,
  `tracking_url` varchar(45) DEFAULT NULL,
  `deal_representative_id` int(11) DEFAULT NULL,
  `date_time_created` datetime DEFAULT NULL,
  `date_time_modified` datetime DEFAULT NULL,
  `logo_url` varchar(45) DEFAULT NULL,
  `created_by_cms_users_id` int(11) NOT NULL,
  `modified_by_cms_users_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_MERCHANT_MERCHANT_CATEGORIES_idx` (`category_id`),
  KEY `fk_MERCHANT_ADB_USERS1_idx` (`created_by_cms_users_id`),
  KEY `fk_MERCHANT_ADB_USERS2_idx` (`modified_by_cms_users_id`),
  CONSTRAINT `fk_MERCHANT_MERCHANT_CATEGORIES` FOREIGN KEY (`category_id`) REFERENCES `MERCHANT_CATEGORIES` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_MERCHANT_CMS_USERS1` FOREIGN KEY (`created_by_cms_users_id`) REFERENCES `CMS_USERS` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_MERCHANT_CMS_USERS2` FOREIGN KEY (`modified_by_cms_users_id`) REFERENCES `CMS_USERS` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `MERCHANT_CATEGORIES` */

DROP TABLE IF EXISTS `MERCHANT_CATEGORIES`;

CREATE TABLE `MERCHANT_CATEGORIES` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `description` varchar(45) DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `date_time_created` datetime DEFAULT NULL,
  `date_time_modified` datetime DEFAULT NULL,
  `created_by_cms_users_id` int(11) NOT NULL,
  `modified_by_cms_users_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `PAID_PLACEMENTS` */

DROP TABLE IF EXISTS `PAID_PLACEMENTS`;

CREATE TABLE `PAID_PLACEMENTS` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `position` varchar(45) DEFAULT NULL,
  `paid_placement_price` varchar(45) DEFAULT NULL,
  `date_time_created` datetime DEFAULT NULL,
  `date_time_modified` datetime DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  `start_date_time` datetime DEFAULT NULL,
  `end_date_time` datetime DEFAULT NULL,
  `coupon_id` int(11) NOT NULL,
  `merchant_id` int(11) NOT NULL,
  `created_by_cms_users_id` int(11) NOT NULL,
  `modified_by_cms_users_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_PAID_PLACEMENTS_MERCHANT_idx` (`merchant_id`),
  KEY `fk_PAID_PLACEMENTS_COUPONS_idx` (`coupon_id`),
  CONSTRAINT `fk_PAID_PLACEMENTS_MERCHANT` FOREIGN KEY (`merchant_id`) REFERENCES `MERCHANT` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_PAID_PLACEMENTS_COUPONS` FOREIGN KEY (`coupon_id`) REFERENCES `COUPONS` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `PAYMENT` */

DROP TABLE IF EXISTS `PAYMENT`;

CREATE TABLE `PAYMENT` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `payment_amount` varchar(45) DEFAULT NULL,
  `payment_date_time` datetime DEFAULT NULL,
  `payout_date_time` datetime DEFAULT NULL,
  `date_time_created` datetime DEFAULT NULL,
  `date_time_modified` datetime DEFAULT NULL,
  `payment_approved_by_cms_users_id` int(11) NOT NULL,
  `payment_status_id` int(11) NOT NULL,
  `consumer_id` int(11) NOT NULL,
  `shopping_visits_id` int(11) NOT NULL,
  `payment_types_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_Payment_Payment_Status1_idx` (`payment_status_id`),
  KEY `fk_Payment_Consumer1_idx` (`consumer_id`),
  KEY `fk_Payment_Payment_Types1_idx` (`payment_types_id`),
  KEY `fk_PAYMENT_SHOPPING_VISITS1_idx` (`shopping_visits_id`),
  KEY `fk_PAYMENT_ADB_USERS1_idx` (`payment_approved_by_cms_users_id`),
  CONSTRAINT `fk_PAYMENT_PAYMENT_STATUS1` FOREIGN KEY (`payment_status_id`) REFERENCES `PAYMENT_STATUS` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_PAYMENT_CONSUMER1` FOREIGN KEY (`consumer_id`) REFERENCES `CONSUMER` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_PAYMENT_PAYMENT_TYPES1` FOREIGN KEY (`payment_types_id`) REFERENCES `PAYMENT_TYPES` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_PAYMENT_SHOPPING_VISITS1` FOREIGN KEY (`shopping_visits_id`) REFERENCES `SHOPPING_VISITS` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_PAYMENT_CMS_USERS1` FOREIGN KEY (`payment_approved_by_cms_users_id`) REFERENCES `CMS_USERS` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `PAYMENT_GATEWAY` */

DROP TABLE IF EXISTS `PAYMENT_GATEWAY`;

CREATE TABLE `PAYMENT_GATEWAY` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(45) DEFAULT NULL,
  `last_name` varchar(45) DEFAULT NULL,
  `card_number` int(11) DEFAULT NULL,
  `street_address_1` varchar(45) DEFAULT NULL,
  `street_address_2` varchar(45) DEFAULT NULL,
  `city` varchar(45) DEFAULT NULL,
  `state_province` varchar(45) DEFAULT NULL,
  `country` varchar(45) DEFAULT NULL,
  `postal_code` int(11) DEFAULT NULL,
  `zip_code` varchar(45) DEFAULT NULL,
  `telephone` int(12) DEFAULT NULL,
  `email_address` varchar(45) DEFAULT NULL,
  `api_key` int(11) DEFAULT NULL,
  `date_time_created` datetime DEFAULT NULL,
  `date_time_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `PAYMENT_GATEWAY_TYPES` */

DROP TABLE IF EXISTS `PAYMENT_GATEWAY_TYPES`;

CREATE TABLE `PAYMENT_GATEWAY_TYPES` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date_time_created` datetime DEFAULT NULL,
  `date_time_modified` datetime DEFAULT NULL,
  `payment_types_id` int(11) NOT NULL,
  `payment_gateway_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_Payment_Gateway_types_Payment_Types1_idx` (`payment_types_id`),
  KEY `fk_Payment_Gateway_types_Payment_Gateway1_idx` (`payment_gateway_id`),
  CONSTRAINT `fk_PAYMENT_GATEWAY_TYPES_PAYMENT_TYPES1` FOREIGN KEY (`payment_types_id`) REFERENCES `PAYMENT_TYPES` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_PAYMENT_GATEWAY_TYPES_PAYMENT_GATEWAY1` FOREIGN KEY (`payment_gateway_id`) REFERENCES `PAYMENT_GATEWAY` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `PAYMENT_STATUS` */

DROP TABLE IF EXISTS `PAYMENT_STATUS`;

CREATE TABLE `PAYMENT_STATUS` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` int(11) DEFAULT NULL,
  `title` varchar(45) DEFAULT NULL,
  `date_time_created` datetime DEFAULT NULL,
  `date_time_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `PAYMENT_TYPES` */

DROP TABLE IF EXISTS `PAYMENT_TYPES`;

CREATE TABLE `PAYMENT_TYPES` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(45) DEFAULT NULL,
  `date_time_created` datetime DEFAULT NULL,
  `date_time_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `PAYOUT_REQUEST` */

DROP TABLE IF EXISTS `PAYOUT_REQUEST`;

CREATE TABLE `PAYOUT_REQUEST` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `payout_request_amount` varchar(45) DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  `date_time_created` datetime DEFAULT NULL,
  `date_time_modified` datetime DEFAULT NULL,
  `consumer_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_payoutrequest_Consumer1_idx` (`consumer_id`),
  CONSTRAINT `fk_PAYOUT_REQUEST_CONSUMER1` FOREIGN KEY (`consumer_id`) REFERENCES `CONSUMER` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `POPULAR_STORES` */

DROP TABLE IF EXISTS `POPULAR_STORES`;

CREATE TABLE `POPULAR_STORES` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` tinyint(4) DEFAULT NULL,
  `date_time_created` datetime DEFAULT NULL,
  `date_time_modified` datetime DEFAULT NULL,
  `start_date_time` datetime DEFAULT NULL,
  `end_date_time` datetime DEFAULT NULL,
  `merchant_id` int(11) NOT NULL,
  `created_by_cms_users_id` int(11) NOT NULL,
  `modified_by_cms_users_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_POPULAR_STORES_MERCHANT_idx` (`merchant_id`),
  CONSTRAINT `fk_POPULAR_STORES_MERCHANT1` FOREIGN KEY (`merchant_id`) REFERENCES `MERCHANT` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `RELATED_MERCHANTS` */

DROP TABLE IF EXISTS `RELATED_MERCHANTS`;

CREATE TABLE `RELATED_MERCHANTS` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `merchant_id` int(11) NOT NULL,
  `status` varchar(45) DEFAULT NULL,
  `date_time_created` datetime DEFAULT NULL,
  `date_time_modified` datetime DEFAULT NULL,
  `related_merchant_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_RELATED_MERCHANTS_MERCHANT1_idx` (`related_merchant_id`),
  CONSTRAINT `fk_RELATED_MERCHANTS_MERCHANT1` FOREIGN KEY (`related_merchant_id`) REFERENCES `MERCHANT` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `REVENUE` */

DROP TABLE IF EXISTS `REVENUE`;

CREATE TABLE `REVENUE` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `revenue_amount` varchar(45) DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  `date_time_created` datetime DEFAULT NULL,
  `date_time_modified` datetime DEFAULT NULL,
  `start_date_time` datetime DEFAULT NULL,
  `end_date_time` datetime DEFAULT NULL,
  `merchant_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_Revenue_Merchant1_idx` (`merchant_id`),
  CONSTRAINT `fk_REVENUE_MERCHANT1` FOREIGN KEY (`merchant_id`) REFERENCES `MERCHANT` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `SEASONAL_COUPONS` */

DROP TABLE IF EXISTS `SEASONAL_COUPONS`;

CREATE TABLE `SEASONAL_COUPONS` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date_time_created` datetime DEFAULT NULL,
  `date_time_modified` datetime DEFAULT NULL,
  `seasonal_events_id` int(11) NOT NULL,
  `coupons_id` int(11) NOT NULL,
  `status` varchar(45) DEFAULT NULL,
  `created_by_cms_users_id` int(11) NOT NULL,
  `modified_by_cms_users_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_Seasonal_Coupons_Seasonal_Events1_idx` (`seasonal_events_id`),
  KEY `fk_Seasonal_Coupons_Coupons1_idx` (`coupons_id`),
  CONSTRAINT `fk_SEASONAL_COUPONS_SEASONAL_EVENTS1` FOREIGN KEY (`seasonal_events_id`) REFERENCES `SEASONAL_EVENTS` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_SEASONAL_COUPONS_COUPONS1` FOREIGN KEY (`coupons_id`) REFERENCES `COUPONS` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `SEASONAL_EVENTS` */

DROP TABLE IF EXISTS `SEASONAL_EVENTS`;

CREATE TABLE `SEASONAL_EVENTS` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `normalized_name` varchar(45) DEFAULT NULL,
  `description_heading` varchar(45) DEFAULT NULL,
  `description` varchar(45) DEFAULT NULL,
  `link_text` varchar(45) DEFAULT NULL,
  `tag` varchar(45) DEFAULT NULL,
  `seo_dynamic_description` varchar(45) DEFAULT NULL,
  `seo_meta_description` varchar(45) DEFAULT NULL,
  `seo_title` varchar(45) DEFAULT NULL,
  `popular_stores_heading` varchar(45) DEFAULT NULL,
  `hero_image` varchar(45) DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  `date_time_created` datetime DEFAULT NULL,
  `date_time_modified` datetime DEFAULT NULL,
  `created_by_cms_users_id` int(11) NOT NULL,
  `modified_by_cms_users_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `SEASONAL_POPULAR_STORES` */

DROP TABLE IF EXISTS `SEASONAL_POPULAR_STORES`;

CREATE TABLE `SEASONAL_POPULAR_STORES` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` varchar(45) DEFAULT NULL,
  `date_time_created` datetime DEFAULT NULL,
  `date_time_modified` datetime DEFAULT NULL,
  `merchant_id` int(11) NOT NULL,
  `seasonal_event_id` int(11) NOT NULL,
  `created_by_cms_users_id` int(11) NOT NULL,
  `modified_by_cms_users_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_SEASONAL_POPULAR_STORES_MERCHANT_idx` (`merchant_id`),
  KEY `fk_SEASONAL_POPULAR_STORES_SEASONAL_EVENTS_idx` (`seasonal_event_id`),
  CONSTRAINT `fk_SEASONAL_POPULAR_STORES_MERCHANT` FOREIGN KEY (`merchant_id`) REFERENCES `MERCHANT` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_SEASONAL_POPULAR_STORES_SEASONAL_EVENTS` FOREIGN KEY (`seasonal_event_id`) REFERENCES `SEASONAL_EVENTS` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `SESSION` */

DROP TABLE IF EXISTS `SESSION`;

CREATE TABLE `SESSION` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(45) DEFAULT NULL,
  `user_agent` varchar(45) DEFAULT NULL,
  `user_data` varchar(45) DEFAULT NULL,
  `last_activity` varchar(45) DEFAULT NULL,
  `last_activity_date_time` datetime DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL,
  `session_id` int(11) DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  `date_time_created` datetime DEFAULT NULL,
  `date_time_modifed` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `SHOPPING_VISITS` */

DROP TABLE IF EXISTS `SHOPPING_VISITS`;

CREATE TABLE `SHOPPING_VISITS` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date_time_created` datetime DEFAULT NULL,
  `consumer_id` int(11) NOT NULL,
  `coupons_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_SHOPPING_VISITS_CONSUMER1_idx` (`consumer_id`),
  KEY `fk_SHOPPING_VISITS_COUPONS1_idx` (`coupons_id`),
  CONSTRAINT `fk_SHOPPING_VISITS_CONSUMER1` FOREIGN KEY (`consumer_id`) REFERENCES `CONSUMER` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_SHOPPING_VISITS_COUPONS1` FOREIGN KEY (`coupons_id`) REFERENCES `COUPONS` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `SOCIAL_MEDIA` */

DROP TABLE IF EXISTS `SOCIAL_MEDIA`;

CREATE TABLE `SOCIAL_MEDIA` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `social_media_id` int(11) DEFAULT NULL,
  `social_media_type` varchar(45) DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  `date_time_created` datetime DEFAULT NULL,
  `date_time_modified` datetime DEFAULT NULL,
  `consumer_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_Social_Media_Consumer1_idx` (`consumer_id`),
  CONSTRAINT `fk_SOCIAL_MEDIA_CONSUMER1` FOREIGN KEY (`consumer_id`) REFERENCES `CONSUMER` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `SUBSCRIPTIONS` */

DROP TABLE IF EXISTS `SUBSCRIPTIONS`;

CREATE TABLE `SUBSCRIPTIONS` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` varchar(45) DEFAULT NULL,
  `date_time_created` datetime DEFAULT NULL,
  `date_time_modified` datetime DEFAULT NULL,
  `consumer_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_SUBSCRIPTIONS_CONSUMER1_idx` (`consumer_id`),
  CONSTRAINT `fk_SUBSCRIPTIONS_CONSUMER1` FOREIGN KEY (`consumer_id`) REFERENCES `CONSUMER` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
