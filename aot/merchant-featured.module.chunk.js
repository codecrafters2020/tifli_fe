webpackJsonp(["merchant-featured.module"],{

/***/ "./node_modules/@swimlane/ngx-dnd/release/components/container/container.component.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ContainerComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__directives__ = __webpack_require__("./node_modules/@swimlane/ngx-dnd/release/directives/index.js");


var i = 0;
function getNextId() {
    return i++;
}
/**
 * Component that allows nested ngxDroppable and ngxDraggables
 *
 * @export
 * @class ContainerComponent
 * @implements {OnInit}
 * @implements {AfterViewInit}
 */
var ContainerComponent = (function () {
    function ContainerComponent() {
        this.copy = false;
        this.removeOnSpill = false;
        this.dropZone = "@@DefaultDropZone-" + getNextId() + "@@";
        this.drop = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.drag = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.over = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.out = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.remove = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.cancel = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
    }
    Object.defineProperty(ContainerComponent.prototype, "dropZones", {
        get: function () {
            return this._dropZones || this._defaultZones;
        },
        set: function (val) {
            this._dropZones = val;
        },
        enumerable: true,
        configurable: true
    });
    ContainerComponent.prototype.ngOnInit = function () {
        this._defaultZones = [this.dropZone];
    };
    ContainerComponent.prototype.ngAfterViewInit = function () {
        var _this = this;
        this.droppable.drag.subscribe(function (v) { return _this.drag.emit(v); });
        this.droppable.drop.subscribe(function (v) { return _this.drop.emit(v); });
        this.droppable.over.subscribe(function (v) { return _this.over.emit(v); });
        this.droppable.out.subscribe(function (v) { return _this.out.emit(v); });
        this.droppable.remove.subscribe(function (v) { return _this.remove.emit(v); });
        this.droppable.cancel.subscribe(function (v) { return _this.cancel.emit(v); });
    };
    ContainerComponent.decorators = [
        { type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"], args: [{
                    selector: 'ngx-dnd-container',
                    templateUrl: './container.component.html',
                    styleUrls: ['./container.component.css'],
                    encapsulation: __WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewEncapsulation"].None
                },] },
    ];
    /** @nocollapse */
    ContainerComponent.ctorParameters = function () { return []; };
    ContainerComponent.propDecorators = {
        'model': [{ type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"] },],
        'copy': [{ type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"] },],
        'removeOnSpill': [{ type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"] },],
        'droppableItemClass': [{ type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"] },],
        'dropZone': [{ type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"] },],
        'dropZones': [{ type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"] },],
        'moves': [{ type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"] },],
        'template': [{ type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"] }, { type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["ContentChild"], args: [__WEBPACK_IMPORTED_MODULE_0__angular_core__["TemplateRef"],] },],
        'droppable': [{ type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"] }, { type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"], args: [__WEBPACK_IMPORTED_MODULE_1__directives__["c" /* DroppableDirective */],] },],
        'drop': [{ type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"] },],
        'drag': [{ type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"] },],
        'over': [{ type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"] },],
        'out': [{ type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"] },],
        'remove': [{ type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"] },],
        'cancel': [{ type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"] },],
    };
    return ContainerComponent;
}());

//# sourceMappingURL=container.component.js.map

/***/ }),

/***/ "./node_modules/@swimlane/ngx-dnd/release/components/index.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__container_container_component__ = __webpack_require__("./node_modules/@swimlane/ngx-dnd/release/components/container/container.component.js");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_0__container_container_component__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__item_item_component__ = __webpack_require__("./node_modules/@swimlane/ngx-dnd/release/components/item/item.component.js");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "b", function() { return __WEBPACK_IMPORTED_MODULE_1__item_item_component__["a"]; });


//# sourceMappingURL=index.js.map

/***/ }),

/***/ "./node_modules/@swimlane/ngx-dnd/release/components/item/item.component.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ItemComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1____ = __webpack_require__("./node_modules/@swimlane/ngx-dnd/release/components/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__directives___ = __webpack_require__("./node_modules/@swimlane/ngx-dnd/release/directives/index.js");



/**
 * Component that allows nested ngxDroppable and ngxDraggables
 * Should only be use inside a ngx-dnd-container
 * Outside a ngx-dnd-container use ngxDroppable
 *
 * @export
 * @class ItemComponent
 * @implements {OnInit}
 */
var ItemComponent = (function () {
    function ItemComponent(container, draggableDirective) {
        this.container = container;
        this.draggableDirective = draggableDirective;
        this._copy = false;
        this._removeOnSpill = false;
    }
    Object.defineProperty(ItemComponent.prototype, "dropZone", {
        get: function () {
            return this._dropZone || this.container.dropZone;
        },
        set: function (val) {
            this._dropZone = val;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ItemComponent.prototype, "dropZones", {
        get: function () {
            return this._dropZones || this.container.dropZones;
        },
        set: function (val) {
            this._dropZones = val;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ItemComponent.prototype, "droppableItemClass", {
        get: function () {
            return this._droppableItemClass || this.container.droppableItemClass;
        },
        set: function (val) {
            this._droppableItemClass = val;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ItemComponent.prototype, "removeOnSpill", {
        get: function () {
            return typeof this._removeOnSpill === 'boolean' ? this._removeOnSpill : this.container.removeOnSpill;
        },
        set: function (val) {
            this._removeOnSpill = val;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ItemComponent.prototype, "copy", {
        get: function () {
            return typeof this._copy === 'boolean' ? this._copy : this.container.copy;
        },
        set: function (val) {
            this._copy = val;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ItemComponent.prototype, "hasHandle", {
        get: function () {
            return this.draggableDirective.hasHandle;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ItemComponent.prototype, "moveDisabled", {
        get: function () {
            return !this.draggableDirective.canMove();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ItemComponent.prototype, "classString", {
        get: function () {
            var itemClass = (typeof this.droppableItemClass === 'function') ?
                this.droppableItemClass(this.model) :
                this.droppableItemClass;
            var classes = ['ngx-dnd-item', itemClass || ''];
            if (this.moveDisabled) {
                classes.push('move-disabled');
            }
            if (this.hasHandle) {
                classes.push('has-handle');
            }
            return classes.join(' ');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ItemComponent.prototype, "type", {
        get: function () {
            if (Array.isArray(this.model)) {
                return 'array';
            }
            return typeof this.model;
        },
        enumerable: true,
        configurable: true
    });
    ItemComponent.prototype.ngOnInit = function () {
        this.data = {
            model: this.model,
            type: this.type,
            dropZone: this.dropZone,
            template: this.container.template
        };
    };
    ItemComponent.decorators = [
        { type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"], args: [{
                    selector: 'ngx-dnd-item',
                    templateUrl: './item.component.html',
                    styleUrls: ['./item.component.css'],
                    encapsulation: __WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewEncapsulation"].None
                },] },
    ];
    /** @nocollapse */
    ItemComponent.ctorParameters = function () { return [
        { type: __WEBPACK_IMPORTED_MODULE_1____["a" /* ContainerComponent */], },
        { type: __WEBPACK_IMPORTED_MODULE_2__directives___["b" /* DraggableDirective */], },
    ]; };
    ItemComponent.propDecorators = {
        'model': [{ type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"] },],
        'dropZone': [{ type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"] },],
        'dropZones': [{ type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"] },],
        'droppableItemClass': [{ type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"] },],
        'removeOnSpill': [{ type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"] },],
        'copy': [{ type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"] },],
        'classString': [{ type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["HostBinding"], args: ['class',] },],
    };
    return ItemComponent;
}());

//# sourceMappingURL=item.component.js.map

/***/ }),

/***/ "./node_modules/@swimlane/ngx-dnd/release/directives/index.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__ngx_draggable_directive__ = __webpack_require__("./node_modules/@swimlane/ngx-dnd/release/directives/ngx-draggable.directive.js");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "b", function() { return __WEBPACK_IMPORTED_MODULE_0__ngx_draggable_directive__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ngx_droppable_directive__ = __webpack_require__("./node_modules/@swimlane/ngx-dnd/release/directives/ngx-droppable.directive.js");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "c", function() { return __WEBPACK_IMPORTED_MODULE_1__ngx_droppable_directive__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ngx_drag_handle_directive__ = __webpack_require__("./node_modules/@swimlane/ngx-dnd/release/directives/ngx-drag-handle.directive.js");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_2__ngx_drag_handle_directive__["a"]; });



//# sourceMappingURL=index.js.map

/***/ }),

/***/ "./node_modules/@swimlane/ngx-dnd/release/directives/ngx-drag-handle.directive.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DragHandleDirective; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");

/**
 * Adds properties and events to drag handle elements
 *
 * @export
 * @class DragHandleDirective
 */
var DragHandleDirective = (function () {
    function DragHandleDirective() {
    }
    DragHandleDirective.decorators = [
        { type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Directive"], args: [{ selector: '[ngxDragHandle]' },] },
    ];
    /** @nocollapse */
    DragHandleDirective.ctorParameters = function () { return []; };
    return DragHandleDirective;
}());

//# sourceMappingURL=ngx-drag-handle.directive.js.map

/***/ }),

/***/ "./node_modules/@swimlane/ngx-dnd/release/directives/ngx-draggable.directive.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DraggableDirective; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ngx_droppable_directive__ = __webpack_require__("./node_modules/@swimlane/ngx-dnd/release/directives/ngx-droppable.directive.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_drake_store_service__ = __webpack_require__("./node_modules/@swimlane/ngx-dnd/release/services/drake-store.service.js");



/**
 * Adds properties and events to draggable elements
 *
 * @export
 * @class DraggableDirective
 * @implements {OnInit}
 * @implements {OnDestroy}
 */
var DraggableDirective = (function () {
    function DraggableDirective(el, drakesService, droppableDirective) {
        this.el = el;
        this.drakesService = drakesService;
        this.droppableDirective = droppableDirective;
        this._moves = true;
        /*
        ContentChildren doesn't get children created with NgTemplateOutlet
        See https://github.com/angular/angular/issues/14842
        Implemented via updateElements method
        
        @ContentChildren(DragHandleDirective, {descendants: true})
        handlesList: QueryList<DragHandleDirective>; */
        this.handles = [];
        this.drag = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.dragDelay = 200; // milliseconds
        this.dragDelayed = true;
    }
    Object.defineProperty(DraggableDirective.prototype, "dropZones", {
        get: function () {
            return this._dropZones || this.ngxDraggable || this._parentDropzones;
        },
        set: function (val) {
            this._dropZones = val;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(DraggableDirective.prototype, "hasHandle", {
        get: function () {
            return !!this.handles.length;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(DraggableDirective.prototype, "element", {
        get: function () {
            return this.el.nativeElement;
        },
        enumerable: true,
        configurable: true
    });
    // From: https://github.com/bevacqua/dragula/issues/289#issuecomment-277143172
    DraggableDirective.prototype.onMove = function (e) {
        if (!this._moves || this.dragDelayed) {
            e.stopPropagation();
            clearTimeout(this.touchTimeout);
        }
    };
    DraggableDirective.prototype.onDown = function (e) {
        var _this = this;
        if (this._moves) {
            this.touchTimeout = setTimeout(function () {
                _this.dragDelayed = false;
            }, this.dragDelay);
        }
    };
    DraggableDirective.prototype.onUp = function (e) {
        if (this._moves) {
            clearTimeout(this.touchTimeout);
            this.dragDelayed = true;
        }
    };
    DraggableDirective.prototype.ngOnInit = function () {
        this.update();
    };
    DraggableDirective.prototype.update = function () {
        this._parentDropzones = [this.droppableDirective.dropZone];
        this.drakesService.registerDraggable(this);
        this.updateElements();
    };
    DraggableDirective.prototype.ngOnDestroy = function () {
        this.drakesService.removeDraggable(this);
    };
    DraggableDirective.prototype.updateElements = function () {
        var nativeElement = this.el.nativeElement;
        var handles = nativeElement.querySelectorAll('[ngxdraghandle]');
        this.handles = Array.from(handles).filter(function (h) { return findFirstDraggableParent(h) === nativeElement; });
        function findFirstDraggableParent(c) {
            while (c.parentNode) {
                c = c.parentNode;
                if (c.hasAttribute && c.hasAttribute('ngxdraggable')) {
                    return c;
                }
            }
        }
    };
    DraggableDirective.prototype.canMove = function (source, handle, sibling) {
        if (typeof this._moves === 'boolean')
            return this._moves;
        if (typeof this._moves === 'function')
            return this._moves(this.model, source, handle, sibling);
        return true;
    };
    DraggableDirective.prototype.moves = function (source, handle, sibling) {
        if (!this.canMove(source, handle, sibling))
            return false;
        return this.hasHandle ?
            this.handles.some(function (h) { return handelFor(handle, h); }) :
            true;
        function handelFor(c, p) {
            if (c === p)
                return true;
            while ((c = c.parentNode) && c !== p)
                ; // tslint:disable-line
            return !!c;
        }
    };
    DraggableDirective.prototype.ngDoCheck = function () {
        this.updateElements();
    };
    DraggableDirective.decorators = [
        { type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Directive"], args: [{ selector: '[ngxDraggable]' },] },
    ];
    /** @nocollapse */
    DraggableDirective.ctorParameters = function () { return [
        { type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"], },
        { type: __WEBPACK_IMPORTED_MODULE_2__services_drake_store_service__["a" /* DrakeStoreService */], },
        { type: __WEBPACK_IMPORTED_MODULE_1__ngx_droppable_directive__["a" /* DroppableDirective */], },
    ]; };
    DraggableDirective.propDecorators = {
        'ngxDraggable': [{ type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"] },],
        'model': [{ type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"] },],
        'dropZones': [{ type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"] },],
        '_moves': [{ type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"], args: ['moves',] },],
        'drag': [{ type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"] },],
        'onMove': [{ type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["HostListener"], args: ['touchmove', ['$event'],] },],
        'onDown': [{ type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["HostListener"], args: ['touchstart', ['$event'],] },],
        'onUp': [{ type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["HostListener"], args: ['touchend', ['$event'],] },],
    };
    return DraggableDirective;
}());

//# sourceMappingURL=ngx-draggable.directive.js.map

/***/ }),

/***/ "./node_modules/@swimlane/ngx-dnd/release/directives/ngx-droppable.directive.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DroppableDirective; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_drake_store_service__ = __webpack_require__("./node_modules/@swimlane/ngx-dnd/release/services/drake-store.service.js");


var i = 10000;
function getNextId() {
    return i++;
}
/**
 * Makes the container droppable and children draggable.
 *
 * @export
 * @class DroppableDirective
 * @implements {OnInit}
 * @implements {OnDestroy}
 * @implements {AfterViewInit}
 */
var DroppableDirective = (function () {
    function DroppableDirective(el, renderer, drakesService) {
        this.el = el;
        this.renderer = renderer;
        this.drakesService = drakesService;
        this.copy = false;
        this.removeOnSpill = false;
        this.drop = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.drag = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.over = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.out = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.remove = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.cancel = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
    }
    Object.defineProperty(DroppableDirective.prototype, "container", {
        get: function () {
            return this.el.nativeElement;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(DroppableDirective.prototype, "dropZone", {
        get: function () {
            return this._dropZone || this.ngxDroppable || this.defaultZone;
        },
        set: function (val) {
            this._dropZone = val;
        },
        enumerable: true,
        configurable: true
    });
    DroppableDirective.prototype.ngOnInit = function () {
        this.defaultZone = "@@DefaultDropZone-" + getNextId() + "@@";
        this.drakesService.register(this);
    };
    DroppableDirective.prototype.ngAfterViewInit = function () {
        var _this = this;
        this.over.subscribe(function (ev) {
            _this.renderer.setElementClass(_this.container, 'gu-over', true);
        });
        this.out.subscribe(function (ev) {
            _this.renderer.setElementClass(_this.container, 'gu-over', false);
        });
    };
    DroppableDirective.prototype.ngOnDestroy = function () {
        this.drakesService.remove(this);
    };
    DroppableDirective.decorators = [
        { type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Directive"], args: [{ selector: '[ngxDroppable]' },] },
    ];
    /** @nocollapse */
    DroppableDirective.ctorParameters = function () { return [
        { type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"], },
        { type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Renderer"], },
        { type: __WEBPACK_IMPORTED_MODULE_1__services_drake_store_service__["a" /* DrakeStoreService */], },
    ]; };
    DroppableDirective.propDecorators = {
        'model': [{ type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"] },],
        'copy': [{ type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"] },],
        'removeOnSpill': [{ type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"] },],
        'ngxDroppable': [{ type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"] },],
        'drop': [{ type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"] },],
        'drag': [{ type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"] },],
        'over': [{ type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"] },],
        'out': [{ type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"] },],
        'remove': [{ type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"] },],
        'cancel': [{ type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"] },],
        'dropZone': [{ type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"] },],
    };
    return DroppableDirective;
}());

//# sourceMappingURL=ngx-droppable.directive.js.map

/***/ }),

/***/ "./node_modules/@swimlane/ngx-dnd/release/ngx-dnd.module.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NgxDnDModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common__ = __webpack_require__("./node_modules/@angular/common/esm5/common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__directives__ = __webpack_require__("./node_modules/@swimlane/ngx-dnd/release/directives/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components__ = __webpack_require__("./node_modules/@swimlane/ngx-dnd/release/components/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services__ = __webpack_require__("./node_modules/@swimlane/ngx-dnd/release/services/index.js");





var components = [__WEBPACK_IMPORTED_MODULE_3__components__["a" /* ContainerComponent */], __WEBPACK_IMPORTED_MODULE_3__components__["b" /* ItemComponent */]];
var directives = [__WEBPACK_IMPORTED_MODULE_2__directives__["b" /* DraggableDirective */], __WEBPACK_IMPORTED_MODULE_2__directives__["c" /* DroppableDirective */], __WEBPACK_IMPORTED_MODULE_2__directives__["a" /* DragHandleDirective */]];
var NgxDnDModule = (function () {
    function NgxDnDModule() {
    }
    NgxDnDModule.decorators = [
        { type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"], args: [{
                    imports: [
                        __WEBPACK_IMPORTED_MODULE_1__angular_common__["CommonModule"]
                    ],
                    declarations: components.concat(directives),
                    exports: components.concat(directives),
                    providers: [__WEBPACK_IMPORTED_MODULE_4__services__["a" /* DrakeStoreService */]]
                },] },
    ];
    /** @nocollapse */
    NgxDnDModule.ctorParameters = function () { return []; };
    return NgxDnDModule;
}());

//# sourceMappingURL=ngx-dnd.module.js.map

/***/ }),

/***/ "./node_modules/@swimlane/ngx-dnd/release/services/drake-store.service.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DrakeStoreService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_dragula__ = __webpack_require__("./node_modules/dragula/dragula.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_dragula___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_dragula__);


/**
 * Central service that handles all events
 *
 * @export
 * @class DrakeStoreService
 */
var DrakeStoreService = (function () {
    function DrakeStoreService() {
        this.droppableMap = new WeakMap();
        this.draggableMap = new WeakMap();
        this.dragulaOptions = {};
        this.dragulaOptions = this.createDrakeOptions();
        this.drake = __WEBPACK_IMPORTED_MODULE_1_dragula__([], this.dragulaOptions);
        this.registerEvents();
    }
    DrakeStoreService.prototype.register = function (droppable) {
        this.droppableMap.set(droppable.container, droppable);
        this.drake.containers.push(droppable.container);
    };
    DrakeStoreService.prototype.remove = function (droppable) {
        this.droppableMap.delete(droppable.container);
        var idx = this.drake.containers.indexOf(droppable.container);
        if (idx > -1) {
            this.drake.containers.splice(idx, 1);
        }
    };
    DrakeStoreService.prototype.registerDraggable = function (draggable) {
        this.draggableMap.set(draggable.element, draggable);
    };
    DrakeStoreService.prototype.removeDraggable = function (draggable) {
        this.draggableMap.delete(draggable.element);
    };
    DrakeStoreService.prototype.createDrakeOptions = function () {
        var _this = this;
        var accepts = function (el, target, source, sibling) {
            if (el.contains(target)) {
                return false;
            }
            var elementComponent = _this.draggableMap.get(el);
            var targetComponent = _this.droppableMap.get(target);
            if (elementComponent && targetComponent) {
                return elementComponent.dropZones.includes(targetComponent.dropZone);
            }
            return true;
        };
        var copy = function (el, source) {
            var sourceComponent = _this.droppableMap.get(source);
            if (sourceComponent) {
                return sourceComponent.copy;
            }
            return false;
        };
        var moves = function (el, source, handle, sibling) {
            var elementComponent = _this.draggableMap.get(el);
            if (elementComponent) {
                return elementComponent.moves(source, handle, sibling);
            }
            return true;
        };
        return { accepts: accepts, copy: copy, moves: moves, revertOnSpill: true };
    };
    DrakeStoreService.prototype.registerEvents = function () {
        var _this = this;
        var dragElm;
        var draggedItem;
        this.drake.on('drag', function (el, source) {
            draggedItem = undefined;
            dragElm = el;
            if (!el || !source) {
                return;
            }
            if (_this.draggableMap.has(el)) {
                var elementComponent = _this.draggableMap.get(el);
                draggedItem = elementComponent.model;
                elementComponent.drag.emit({
                    type: 'drag',
                    el: el,
                    source: source,
                    value: draggedItem
                });
            }
            if (_this.droppableMap.has(source)) {
                var sourceComponent = _this.droppableMap.get(source);
                _this.dragulaOptions.removeOnSpill = sourceComponent.removeOnSpill;
                sourceComponent.drag.emit({
                    type: 'drag',
                    el: el,
                    source: source,
                    sourceComponent: sourceComponent,
                    value: draggedItem
                });
            }
        });
        this.drake.on('drop', function (el, target, source) {
            if (_this.droppableMap.has(target)) {
                var targetComponent = _this.droppableMap.get(target);
                var dropElmModel = draggedItem;
                if (_this.droppableMap.has(source)) {
                    var sourceComponent = _this.droppableMap.get(source);
                    var sourceModel = sourceComponent.model;
                    var targetModel = targetComponent.model;
                    var dropIndex = Array.prototype.indexOf.call(target.children, el);
                    var dragIndex = (sourceModel && draggedItem) ? sourceModel.indexOf(draggedItem) : -1;
                    if (dropIndex > -1 && targetModel) {
                        if (dragIndex > -1 && sourceModel && target === source) {
                            sourceModel.splice(dropIndex, 0, sourceModel.splice(dragIndex, 1)[0]);
                        }
                        else {
                            if (el.parentNode === target) {
                                target.removeChild(el);
                            }
                            var copy = !sourceModel || (dragElm !== el);
                            if (copy) {
                                dropElmModel = JSON.parse(JSON.stringify(dropElmModel));
                            }
                            else {
                                if (el.parentNode !== source) {
                                    // add element back, let angular remove it
                                    source.append(el);
                                }
                                sourceModel.splice(dragIndex, 1);
                            }
                            targetModel.splice(dropIndex, 0, dropElmModel);
                        }
                    }
                }
                targetComponent.drop.emit({
                    type: 'drop',
                    el: el,
                    source: source,
                    value: dropElmModel
                });
            }
        });
        this.drake.on('remove', function (el, container, source) {
            if (_this.droppableMap.has(source)) {
                var sourceComponent = _this.droppableMap.get(source);
                var sourceModel = sourceComponent.model;
                var dragIndex = (draggedItem && sourceModel) ? sourceModel.indexOf(draggedItem) : -1;
                if (dragIndex > -1) {
                    if (el.parentNode !== source) {
                        // add element back, let angular remove it
                        source.append(el);
                    }
                    sourceModel.splice(dragIndex, 1);
                }
                sourceComponent.remove.emit({
                    type: 'remove',
                    el: el,
                    container: container,
                    source: source,
                    value: draggedItem
                });
            }
        });
        this.drake.on('cancel', function (el, container, source) {
            if (_this.droppableMap.has(container)) {
                var containerComponent = _this.droppableMap.get(container);
                containerComponent.cancel.emit({
                    type: 'cancel',
                    el: el,
                    container: container,
                    source: source,
                    value: draggedItem
                });
            }
        });
        this.drake.on('over', function (el, container, source) {
            if (_this.droppableMap.has(container)) {
                var containerComponent = _this.droppableMap.get(container);
                containerComponent.over.emit({
                    type: 'over',
                    el: el,
                    container: container,
                    source: source,
                    value: draggedItem
                });
            }
        });
        this.drake.on('out', function (el, container, source) {
            if (_this.droppableMap.has(container)) {
                var containerComponent = _this.droppableMap.get(container);
                containerComponent.out.emit({
                    type: 'out',
                    el: el,
                    container: container,
                    source: source,
                    value: draggedItem
                });
            }
        });
    };
    DrakeStoreService.decorators = [
        { type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"] },
    ];
    /** @nocollapse */
    DrakeStoreService.ctorParameters = function () { return []; };
    return DrakeStoreService;
}());

//# sourceMappingURL=drake-store.service.js.map

/***/ }),

/***/ "./node_modules/@swimlane/ngx-dnd/release/services/index.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__drake_store_service__ = __webpack_require__("./node_modules/@swimlane/ngx-dnd/release/services/drake-store.service.js");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_0__drake_store_service__["a"]; });

//# sourceMappingURL=index.js.map

/***/ }),

/***/ "./node_modules/atoa/atoa.js":
/***/ (function(module, exports) {

module.exports = function atoa (a, n) { return Array.prototype.slice.call(a, n); }


/***/ }),

/***/ "./node_modules/contra/debounce.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var ticky = __webpack_require__("./node_modules/ticky/ticky-browser.js");

module.exports = function debounce (fn, args, ctx) {
  if (!fn) { return; }
  ticky(function run () {
    fn.apply(ctx || null, args || []);
  });
};


/***/ }),

/***/ "./node_modules/contra/emitter.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var atoa = __webpack_require__("./node_modules/atoa/atoa.js");
var debounce = __webpack_require__("./node_modules/contra/debounce.js");

module.exports = function emitter (thing, options) {
  var opts = options || {};
  var evt = {};
  if (thing === undefined) { thing = {}; }
  thing.on = function (type, fn) {
    if (!evt[type]) {
      evt[type] = [fn];
    } else {
      evt[type].push(fn);
    }
    return thing;
  };
  thing.once = function (type, fn) {
    fn._once = true; // thing.off(fn) still works!
    thing.on(type, fn);
    return thing;
  };
  thing.off = function (type, fn) {
    var c = arguments.length;
    if (c === 1) {
      delete evt[type];
    } else if (c === 0) {
      evt = {};
    } else {
      var et = evt[type];
      if (!et) { return thing; }
      et.splice(et.indexOf(fn), 1);
    }
    return thing;
  };
  thing.emit = function () {
    var args = atoa(arguments);
    return thing.emitterSnapshot(args.shift()).apply(this, args);
  };
  thing.emitterSnapshot = function (type) {
    var et = (evt[type] || []).slice(0);
    return function () {
      var args = atoa(arguments);
      var ctx = this || thing;
      if (type === 'error' && opts.throws !== false && !et.length) { throw args.length === 1 ? args[0] : args; }
      et.forEach(function emitter (listen) {
        if (opts.async) { debounce(listen, args, ctx); } else { listen.apply(ctx, args); }
        if (listen._once) { thing.off(type, listen); }
      });
      return thing;
    };
  };
  return thing;
};


/***/ }),

/***/ "./node_modules/crossvent/src/crossvent.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(global) {

var customEvent = __webpack_require__("./node_modules/custom-event/index.js");
var eventmap = __webpack_require__("./node_modules/crossvent/src/eventmap.js");
var doc = global.document;
var addEvent = addEventEasy;
var removeEvent = removeEventEasy;
var hardCache = [];

if (!global.addEventListener) {
  addEvent = addEventHard;
  removeEvent = removeEventHard;
}

module.exports = {
  add: addEvent,
  remove: removeEvent,
  fabricate: fabricateEvent
};

function addEventEasy (el, type, fn, capturing) {
  return el.addEventListener(type, fn, capturing);
}

function addEventHard (el, type, fn) {
  return el.attachEvent('on' + type, wrap(el, type, fn));
}

function removeEventEasy (el, type, fn, capturing) {
  return el.removeEventListener(type, fn, capturing);
}

function removeEventHard (el, type, fn) {
  var listener = unwrap(el, type, fn);
  if (listener) {
    return el.detachEvent('on' + type, listener);
  }
}

function fabricateEvent (el, type, model) {
  var e = eventmap.indexOf(type) === -1 ? makeCustomEvent() : makeClassicEvent();
  if (el.dispatchEvent) {
    el.dispatchEvent(e);
  } else {
    el.fireEvent('on' + type, e);
  }
  function makeClassicEvent () {
    var e;
    if (doc.createEvent) {
      e = doc.createEvent('Event');
      e.initEvent(type, true, true);
    } else if (doc.createEventObject) {
      e = doc.createEventObject();
    }
    return e;
  }
  function makeCustomEvent () {
    return new customEvent(type, { detail: model });
  }
}

function wrapperFactory (el, type, fn) {
  return function wrapper (originalEvent) {
    var e = originalEvent || global.event;
    e.target = e.target || e.srcElement;
    e.preventDefault = e.preventDefault || function preventDefault () { e.returnValue = false; };
    e.stopPropagation = e.stopPropagation || function stopPropagation () { e.cancelBubble = true; };
    e.which = e.which || e.keyCode;
    fn.call(el, e);
  };
}

function wrap (el, type, fn) {
  var wrapper = unwrap(el, type, fn) || wrapperFactory(el, type, fn);
  hardCache.push({
    wrapper: wrapper,
    element: el,
    type: type,
    fn: fn
  });
  return wrapper;
}

function unwrap (el, type, fn) {
  var i = find(el, type, fn);
  if (i) {
    var wrapper = hardCache[i].wrapper;
    hardCache.splice(i, 1); // free up a tad of memory
    return wrapper;
  }
}

function find (el, type, fn) {
  var i, item;
  for (i = 0; i < hardCache.length; i++) {
    item = hardCache[i];
    if (item.element === el && item.type === type && item.fn === fn) {
      return i;
    }
  }
}

/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__("./node_modules/webpack/buildin/global.js")))

/***/ }),

/***/ "./node_modules/crossvent/src/eventmap.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(global) {

var eventmap = [];
var eventname = '';
var ron = /^on/;

for (eventname in global) {
  if (ron.test(eventname)) {
    eventmap.push(eventname.slice(2));
  }
}

module.exports = eventmap;

/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__("./node_modules/webpack/buildin/global.js")))

/***/ }),

/***/ "./node_modules/custom-event/index.js":
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(global) {
var NativeCustomEvent = global.CustomEvent;

function useNative () {
  try {
    var p = new NativeCustomEvent('cat', { detail: { foo: 'bar' } });
    return  'cat' === p.type && 'bar' === p.detail.foo;
  } catch (e) {
  }
  return false;
}

/**
 * Cross-browser `CustomEvent` constructor.
 *
 * https://developer.mozilla.org/en-US/docs/Web/API/CustomEvent.CustomEvent
 *
 * @public
 */

module.exports = useNative() ? NativeCustomEvent :

// IE >= 9
'function' === typeof document.createEvent ? function CustomEvent (type, params) {
  var e = document.createEvent('CustomEvent');
  if (params) {
    e.initCustomEvent(type, params.bubbles, params.cancelable, params.detail);
  } else {
    e.initCustomEvent(type, false, false, void 0);
  }
  return e;
} :

// IE <= 8
function CustomEvent (type, params) {
  var e = document.createEventObject();
  e.type = type;
  if (params) {
    e.bubbles = Boolean(params.bubbles);
    e.cancelable = Boolean(params.cancelable);
    e.detail = params.detail;
  } else {
    e.bubbles = false;
    e.cancelable = false;
    e.detail = void 0;
  }
  return e;
}

/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__("./node_modules/webpack/buildin/global.js")))

/***/ }),

/***/ "./node_modules/dragula/classes.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var cache = {};
var start = '(?:^|\\s)';
var end = '(?:\\s|$)';

function lookupClass (className) {
  var cached = cache[className];
  if (cached) {
    cached.lastIndex = 0;
  } else {
    cache[className] = cached = new RegExp(start + className + end, 'g');
  }
  return cached;
}

function addClass (el, className) {
  var current = el.className;
  if (!current.length) {
    el.className = className;
  } else if (!lookupClass(className).test(current)) {
    el.className += ' ' + className;
  }
}

function rmClass (el, className) {
  el.className = el.className.replace(lookupClass(className), ' ').trim();
}

module.exports = {
  add: addClass,
  rm: rmClass
};


/***/ }),

/***/ "./node_modules/dragula/dragula.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(global) {

var emitter = __webpack_require__("./node_modules/contra/emitter.js");
var crossvent = __webpack_require__("./node_modules/crossvent/src/crossvent.js");
var classes = __webpack_require__("./node_modules/dragula/classes.js");
var doc = document;
var documentElement = doc.documentElement;

function dragula (initialContainers, options) {
  var len = arguments.length;
  if (len === 1 && Array.isArray(initialContainers) === false) {
    options = initialContainers;
    initialContainers = [];
  }
  var _mirror; // mirror image
  var _source; // source container
  var _item; // item being dragged
  var _offsetX; // reference x
  var _offsetY; // reference y
  var _moveX; // reference move x
  var _moveY; // reference move y
  var _initialSibling; // reference sibling when grabbed
  var _currentSibling; // reference sibling now
  var _copy; // item used for copying
  var _renderTimer; // timer for setTimeout renderMirrorImage
  var _lastDropTarget = null; // last container item was over
  var _grabbed; // holds mousedown context until first mousemove

  var o = options || {};
  if (o.moves === void 0) { o.moves = always; }
  if (o.accepts === void 0) { o.accepts = always; }
  if (o.invalid === void 0) { o.invalid = invalidTarget; }
  if (o.containers === void 0) { o.containers = initialContainers || []; }
  if (o.isContainer === void 0) { o.isContainer = never; }
  if (o.copy === void 0) { o.copy = false; }
  if (o.copySortSource === void 0) { o.copySortSource = false; }
  if (o.revertOnSpill === void 0) { o.revertOnSpill = false; }
  if (o.removeOnSpill === void 0) { o.removeOnSpill = false; }
  if (o.direction === void 0) { o.direction = 'vertical'; }
  if (o.ignoreInputTextSelection === void 0) { o.ignoreInputTextSelection = true; }
  if (o.mirrorContainer === void 0) { o.mirrorContainer = doc.body; }

  var drake = emitter({
    containers: o.containers,
    start: manualStart,
    end: end,
    cancel: cancel,
    remove: remove,
    destroy: destroy,
    canMove: canMove,
    dragging: false
  });

  if (o.removeOnSpill === true) {
    drake.on('over', spillOver).on('out', spillOut);
  }

  events();

  return drake;

  function isContainer (el) {
    return drake.containers.indexOf(el) !== -1 || o.isContainer(el);
  }

  function events (remove) {
    var op = remove ? 'remove' : 'add';
    touchy(documentElement, op, 'mousedown', grab);
    touchy(documentElement, op, 'mouseup', release);
  }

  function eventualMovements (remove) {
    var op = remove ? 'remove' : 'add';
    touchy(documentElement, op, 'mousemove', startBecauseMouseMoved);
  }

  function movements (remove) {
    var op = remove ? 'remove' : 'add';
    crossvent[op](documentElement, 'selectstart', preventGrabbed); // IE8
    crossvent[op](documentElement, 'click', preventGrabbed);
  }

  function destroy () {
    events(true);
    release({});
  }

  function preventGrabbed (e) {
    if (_grabbed) {
      e.preventDefault();
    }
  }

  function grab (e) {
    _moveX = e.clientX;
    _moveY = e.clientY;

    var ignore = whichMouseButton(e) !== 1 || e.metaKey || e.ctrlKey;
    if (ignore) {
      return; // we only care about honest-to-god left clicks and touch events
    }
    var item = e.target;
    var context = canStart(item);
    if (!context) {
      return;
    }
    _grabbed = context;
    eventualMovements();
    if (e.type === 'mousedown') {
      if (isInput(item)) { // see also: https://github.com/bevacqua/dragula/issues/208
        item.focus(); // fixes https://github.com/bevacqua/dragula/issues/176
      } else {
        e.preventDefault(); // fixes https://github.com/bevacqua/dragula/issues/155
      }
    }
  }

  function startBecauseMouseMoved (e) {
    if (!_grabbed) {
      return;
    }
    if (whichMouseButton(e) === 0) {
      release({});
      return; // when text is selected on an input and then dragged, mouseup doesn't fire. this is our only hope
    }
    // truthy check fixes #239, equality fixes #207
    if (e.clientX !== void 0 && e.clientX === _moveX && e.clientY !== void 0 && e.clientY === _moveY) {
      return;
    }
    if (o.ignoreInputTextSelection) {
      var clientX = getCoord('clientX', e);
      var clientY = getCoord('clientY', e);
      var elementBehindCursor = doc.elementFromPoint(clientX, clientY);
      if (isInput(elementBehindCursor)) {
        return;
      }
    }

    var grabbed = _grabbed; // call to end() unsets _grabbed
    eventualMovements(true);
    movements();
    end();
    start(grabbed);

    var offset = getOffset(_item);
    _offsetX = getCoord('pageX', e) - offset.left;
    _offsetY = getCoord('pageY', e) - offset.top;

    classes.add(_copy || _item, 'gu-transit');
    renderMirrorImage();
    drag(e);
  }

  function canStart (item) {
    if (drake.dragging && _mirror) {
      return;
    }
    if (isContainer(item)) {
      return; // don't drag container itself
    }
    var handle = item;
    while (getParent(item) && isContainer(getParent(item)) === false) {
      if (o.invalid(item, handle)) {
        return;
      }
      item = getParent(item); // drag target should be a top element
      if (!item) {
        return;
      }
    }
    var source = getParent(item);
    if (!source) {
      return;
    }
    if (o.invalid(item, handle)) {
      return;
    }

    var movable = o.moves(item, source, handle, nextEl(item));
    if (!movable) {
      return;
    }

    return {
      item: item,
      source: source
    };
  }

  function canMove (item) {
    return !!canStart(item);
  }

  function manualStart (item) {
    var context = canStart(item);
    if (context) {
      start(context);
    }
  }

  function start (context) {
    if (isCopy(context.item, context.source)) {
      _copy = context.item.cloneNode(true);
      drake.emit('cloned', _copy, context.item, 'copy');
    }

    _source = context.source;
    _item = context.item;
    _initialSibling = _currentSibling = nextEl(context.item);

    drake.dragging = true;
    drake.emit('drag', _item, _source);
  }

  function invalidTarget () {
    return false;
  }

  function end () {
    if (!drake.dragging) {
      return;
    }
    var item = _copy || _item;
    drop(item, getParent(item));
  }

  function ungrab () {
    _grabbed = false;
    eventualMovements(true);
    movements(true);
  }

  function release (e) {
    ungrab();

    if (!drake.dragging) {
      return;
    }
    var item = _copy || _item;
    var clientX = getCoord('clientX', e);
    var clientY = getCoord('clientY', e);
    var elementBehindCursor = getElementBehindPoint(_mirror, clientX, clientY);
    var dropTarget = findDropTarget(elementBehindCursor, clientX, clientY);
    if (dropTarget && ((_copy && o.copySortSource) || (!_copy || dropTarget !== _source))) {
      drop(item, dropTarget);
    } else if (o.removeOnSpill) {
      remove();
    } else {
      cancel();
    }
  }

  function drop (item, target) {
    var parent = getParent(item);
    if (_copy && o.copySortSource && target === _source) {
      parent.removeChild(_item);
    }
    if (isInitialPlacement(target)) {
      drake.emit('cancel', item, _source, _source);
    } else {
      drake.emit('drop', item, target, _source, _currentSibling);
    }
    cleanup();
  }

  function remove () {
    if (!drake.dragging) {
      return;
    }
    var item = _copy || _item;
    var parent = getParent(item);
    if (parent) {
      parent.removeChild(item);
    }
    drake.emit(_copy ? 'cancel' : 'remove', item, parent, _source);
    cleanup();
  }

  function cancel (revert) {
    if (!drake.dragging) {
      return;
    }
    var reverts = arguments.length > 0 ? revert : o.revertOnSpill;
    var item = _copy || _item;
    var parent = getParent(item);
    var initial = isInitialPlacement(parent);
    if (initial === false && reverts) {
      if (_copy) {
        if (parent) {
          parent.removeChild(_copy);
        }
      } else {
        _source.insertBefore(item, _initialSibling);
      }
    }
    if (initial || reverts) {
      drake.emit('cancel', item, _source, _source);
    } else {
      drake.emit('drop', item, parent, _source, _currentSibling);
    }
    cleanup();
  }

  function cleanup () {
    var item = _copy || _item;
    ungrab();
    removeMirrorImage();
    if (item) {
      classes.rm(item, 'gu-transit');
    }
    if (_renderTimer) {
      clearTimeout(_renderTimer);
    }
    drake.dragging = false;
    if (_lastDropTarget) {
      drake.emit('out', item, _lastDropTarget, _source);
    }
    drake.emit('dragend', item);
    _source = _item = _copy = _initialSibling = _currentSibling = _renderTimer = _lastDropTarget = null;
  }

  function isInitialPlacement (target, s) {
    var sibling;
    if (s !== void 0) {
      sibling = s;
    } else if (_mirror) {
      sibling = _currentSibling;
    } else {
      sibling = nextEl(_copy || _item);
    }
    return target === _source && sibling === _initialSibling;
  }

  function findDropTarget (elementBehindCursor, clientX, clientY) {
    var target = elementBehindCursor;
    while (target && !accepted()) {
      target = getParent(target);
    }
    return target;

    function accepted () {
      var droppable = isContainer(target);
      if (droppable === false) {
        return false;
      }

      var immediate = getImmediateChild(target, elementBehindCursor);
      var reference = getReference(target, immediate, clientX, clientY);
      var initial = isInitialPlacement(target, reference);
      if (initial) {
        return true; // should always be able to drop it right back where it was
      }
      return o.accepts(_item, target, _source, reference);
    }
  }

  function drag (e) {
    if (!_mirror) {
      return;
    }
    e.preventDefault();

    var clientX = getCoord('clientX', e);
    var clientY = getCoord('clientY', e);
    var x = clientX - _offsetX;
    var y = clientY - _offsetY;

    _mirror.style.left = x + 'px';
    _mirror.style.top = y + 'px';

    var item = _copy || _item;
    var elementBehindCursor = getElementBehindPoint(_mirror, clientX, clientY);
    var dropTarget = findDropTarget(elementBehindCursor, clientX, clientY);
    var changed = dropTarget !== null && dropTarget !== _lastDropTarget;
    if (changed || dropTarget === null) {
      out();
      _lastDropTarget = dropTarget;
      over();
    }
    var parent = getParent(item);
    if (dropTarget === _source && _copy && !o.copySortSource) {
      if (parent) {
        parent.removeChild(item);
      }
      return;
    }
    var reference;
    var immediate = getImmediateChild(dropTarget, elementBehindCursor);
    if (immediate !== null) {
      reference = getReference(dropTarget, immediate, clientX, clientY);
    } else if (o.revertOnSpill === true && !_copy) {
      reference = _initialSibling;
      dropTarget = _source;
    } else {
      if (_copy && parent) {
        parent.removeChild(item);
      }
      return;
    }
    if (
      (reference === null && changed) ||
      reference !== item &&
      reference !== nextEl(item)
    ) {
      _currentSibling = reference;
      dropTarget.insertBefore(item, reference);
      drake.emit('shadow', item, dropTarget, _source);
    }
    function moved (type) { drake.emit(type, item, _lastDropTarget, _source); }
    function over () { if (changed) { moved('over'); } }
    function out () { if (_lastDropTarget) { moved('out'); } }
  }

  function spillOver (el) {
    classes.rm(el, 'gu-hide');
  }

  function spillOut (el) {
    if (drake.dragging) { classes.add(el, 'gu-hide'); }
  }

  function renderMirrorImage () {
    if (_mirror) {
      return;
    }
    var rect = _item.getBoundingClientRect();
    _mirror = _item.cloneNode(true);
    _mirror.style.width = getRectWidth(rect) + 'px';
    _mirror.style.height = getRectHeight(rect) + 'px';
    classes.rm(_mirror, 'gu-transit');
    classes.add(_mirror, 'gu-mirror');
    o.mirrorContainer.appendChild(_mirror);
    touchy(documentElement, 'add', 'mousemove', drag);
    classes.add(o.mirrorContainer, 'gu-unselectable');
    drake.emit('cloned', _mirror, _item, 'mirror');
  }

  function removeMirrorImage () {
    if (_mirror) {
      classes.rm(o.mirrorContainer, 'gu-unselectable');
      touchy(documentElement, 'remove', 'mousemove', drag);
      getParent(_mirror).removeChild(_mirror);
      _mirror = null;
    }
  }

  function getImmediateChild (dropTarget, target) {
    var immediate = target;
    while (immediate !== dropTarget && getParent(immediate) !== dropTarget) {
      immediate = getParent(immediate);
    }
    if (immediate === documentElement) {
      return null;
    }
    return immediate;
  }

  function getReference (dropTarget, target, x, y) {
    var horizontal = o.direction === 'horizontal';
    var reference = target !== dropTarget ? inside() : outside();
    return reference;

    function outside () { // slower, but able to figure out any position
      var len = dropTarget.children.length;
      var i;
      var el;
      var rect;
      for (i = 0; i < len; i++) {
        el = dropTarget.children[i];
        rect = el.getBoundingClientRect();
        if (horizontal && (rect.left + rect.width / 2) > x) { return el; }
        if (!horizontal && (rect.top + rect.height / 2) > y) { return el; }
      }
      return null;
    }

    function inside () { // faster, but only available if dropped inside a child element
      var rect = target.getBoundingClientRect();
      if (horizontal) {
        return resolve(x > rect.left + getRectWidth(rect) / 2);
      }
      return resolve(y > rect.top + getRectHeight(rect) / 2);
    }

    function resolve (after) {
      return after ? nextEl(target) : target;
    }
  }

  function isCopy (item, container) {
    return typeof o.copy === 'boolean' ? o.copy : o.copy(item, container);
  }
}

function touchy (el, op, type, fn) {
  var touch = {
    mouseup: 'touchend',
    mousedown: 'touchstart',
    mousemove: 'touchmove'
  };
  var pointers = {
    mouseup: 'pointerup',
    mousedown: 'pointerdown',
    mousemove: 'pointermove'
  };
  var microsoft = {
    mouseup: 'MSPointerUp',
    mousedown: 'MSPointerDown',
    mousemove: 'MSPointerMove'
  };
  if (global.navigator.pointerEnabled) {
    crossvent[op](el, pointers[type], fn);
  } else if (global.navigator.msPointerEnabled) {
    crossvent[op](el, microsoft[type], fn);
  } else {
    crossvent[op](el, touch[type], fn);
    crossvent[op](el, type, fn);
  }
}

function whichMouseButton (e) {
  if (e.touches !== void 0) { return e.touches.length; }
  if (e.which !== void 0 && e.which !== 0) { return e.which; } // see https://github.com/bevacqua/dragula/issues/261
  if (e.buttons !== void 0) { return e.buttons; }
  var button = e.button;
  if (button !== void 0) { // see https://github.com/jquery/jquery/blob/99e8ff1baa7ae341e94bb89c3e84570c7c3ad9ea/src/event.js#L573-L575
    return button & 1 ? 1 : button & 2 ? 3 : (button & 4 ? 2 : 0);
  }
}

function getOffset (el) {
  var rect = el.getBoundingClientRect();
  return {
    left: rect.left + getScroll('scrollLeft', 'pageXOffset'),
    top: rect.top + getScroll('scrollTop', 'pageYOffset')
  };
}

function getScroll (scrollProp, offsetProp) {
  if (typeof global[offsetProp] !== 'undefined') {
    return global[offsetProp];
  }
  if (documentElement.clientHeight) {
    return documentElement[scrollProp];
  }
  return doc.body[scrollProp];
}

function getElementBehindPoint (point, x, y) {
  var p = point || {};
  var state = p.className;
  var el;
  p.className += ' gu-hide';
  el = doc.elementFromPoint(x, y);
  p.className = state;
  return el;
}

function never () { return false; }
function always () { return true; }
function getRectWidth (rect) { return rect.width || (rect.right - rect.left); }
function getRectHeight (rect) { return rect.height || (rect.bottom - rect.top); }
function getParent (el) { return el.parentNode === doc ? null : el.parentNode; }
function isInput (el) { return el.tagName === 'INPUT' || el.tagName === 'TEXTAREA' || el.tagName === 'SELECT' || isEditable(el); }
function isEditable (el) {
  if (!el) { return false; } // no parents were editable
  if (el.contentEditable === 'false') { return false; } // stop the lookup
  if (el.contentEditable === 'true') { return true; } // found a contentEditable element in the chain
  return isEditable(getParent(el)); // contentEditable is set to 'inherit'
}

function nextEl (el) {
  return el.nextElementSibling || manually();
  function manually () {
    var sibling = el;
    do {
      sibling = sibling.nextSibling;
    } while (sibling && sibling.nodeType !== 1);
    return sibling;
  }
}

function getEventHost (e) {
  // on touchend event, we have to use `e.changedTouches`
  // see http://stackoverflow.com/questions/7192563/touchend-event-properties
  // see https://github.com/bevacqua/dragula/issues/34
  if (e.targetTouches && e.targetTouches.length) {
    return e.targetTouches[0];
  }
  if (e.changedTouches && e.changedTouches.length) {
    return e.changedTouches[0];
  }
  return e;
}

function getCoord (coord, e) {
  var host = getEventHost(e);
  var missMap = {
    pageX: 'clientX', // IE8
    pageY: 'clientY' // IE8
  };
  if (coord in missMap && !(coord in host) && missMap[coord] in host) {
    coord = missMap[coord];
  }
  return host[coord];
}

module.exports = dragula;

/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__("./node_modules/webpack/buildin/global.js")))

/***/ }),

/***/ "./node_modules/ticky/ticky-browser.js":
/***/ (function(module, exports) {

var si = typeof setImmediate === 'function', tick;
if (si) {
  tick = function (fn) { setImmediate(fn); };
} else {
  tick = function (fn) { setTimeout(fn, 0); };
}

module.exports = tick;

/***/ }),

/***/ "./src/@fuse/pipes/getById.pipe.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GetByIdPipe; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");

var GetByIdPipe = /** @class */ (function () {
    function GetByIdPipe() {
    }
    GetByIdPipe.prototype.transform = function (value, id, property) {
        var foundItem = value.find(function (item) {
            if (item.id !== undefined) {
                return item.id === id;
            }
            return false;
        });
        if (foundItem) {
            return foundItem[property];
        }
    };
    return GetByIdPipe;
}());



/***/ }),

/***/ "./src/app/main/content/pages/merchant-featured/card.model.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Card; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__fuse_utils__ = __webpack_require__("./src/@fuse/utils/index.ts");

var Card = /** @class */ (function () {
    function Card(card) {
        this.id = card.id || __WEBPACK_IMPORTED_MODULE_0__fuse_utils__["a" /* FuseUtils */].generateGUID();
        this.name = card.name || '';
        this.description = card.description || '';
        this.idAttachmentCover = card.idAttachmentCover || '';
        this.idMembers = card.idMembers || [];
        this.idLabels = card.idLabels || [];
        this.attachments = card.attachments || [];
        this.subscribed = card.subscribed || true;
        this.checklists = card.checklists || [];
        this.checkItems = card.checkItems || 0;
        this.checkItemsChecked = card.checkItemsChecked || 0;
        this.comments = card.comments || [];
        this.activities = card.activities || [];
        this.due = card.due || '';
    }
    return Card;
}());



/***/ }),

/***/ "./src/app/main/content/pages/merchant-featured/merchant-featured-board/add-list/add-list.component.ngfactory.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RenderType_MerchantFeaturedBoardAddListComponent; });
/* harmony export (immutable) */ __webpack_exports__["b"] = View_MerchantFeaturedBoardAddListComponent_0;
/* unused harmony export View_MerchantFeaturedBoardAddListComponent_Host_0 */
/* unused harmony export MerchantFeaturedBoardAddListComponentNgFactory */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__add_list_component_scss_shim_ngstyle__ = __webpack_require__("./src/app/main/content/pages/merchant-featured/merchant-featured-board/add-list/add-list.component.scss.shim.ngstyle.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__node_modules_angular_material_button_typings_index_ngfactory__ = __webpack_require__("./node_modules/@angular/material/button/typings/index.ngfactory.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_material_button__ = __webpack_require__("./node_modules/@angular/material/esm5/button.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_cdk_platform__ = __webpack_require__("./node_modules/@angular/cdk/esm5/platform.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_cdk_a11y__ = __webpack_require__("./node_modules/@angular/cdk/esm5/a11y.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_flex_layout__ = __webpack_require__("./node_modules/@angular/flex-layout/esm5/flex-layout.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__node_modules_angular_material_icon_typings_index_ngfactory__ = __webpack_require__("./node_modules/@angular/material/icon/typings/index.ngfactory.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__angular_material_icon__ = __webpack_require__("./node_modules/@angular/material/esm5/icon.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__angular_forms__ = __webpack_require__("./node_modules/@angular/forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__angular_common__ = __webpack_require__("./node_modules/@angular/common/esm5/common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__add_list_component__ = __webpack_require__("./src/app/main/content/pages/merchant-featured/merchant-featured-board/add-list/add-list.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/**
 * @fileoverview This file was generated by the Angular template compiler. Do not edit.
 *
 * @suppress {suspiciousCode,uselessCode,missingProperties,missingOverride,checkTypes}
 * tslint:disable
 */ 













var styles_MerchantFeaturedBoardAddListComponent = [__WEBPACK_IMPORTED_MODULE_0__add_list_component_scss_shim_ngstyle__["a" /* styles */]];
var RenderType_MerchantFeaturedBoardAddListComponent = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵcrt"]({ encapsulation: 0, styles: styles_MerchantFeaturedBoardAddListComponent, data: {} });

function View_MerchantFeaturedBoardAddListComponent_1(_l) { return __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵvid"](0, [(_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵeld"](0, 0, null, null, 14, "button", [["class", "new-list-form-button"], ["mat-button", ""], ["ngDraggable", "false"]], [[8, "disabled", 0]], [[null, "click"]], function (_v, en, $event) { var ad = true; var _co = _v.component; if (("click" === en)) {
        var pd_0 = (_co.openForm() !== false);
        ad = (pd_0 && ad);
    } return ad; }, __WEBPACK_IMPORTED_MODULE_2__node_modules_angular_material_button_typings_index_ngfactory__["d" /* View_MatButton_0 */], __WEBPACK_IMPORTED_MODULE_2__node_modules_angular_material_button_typings_index_ngfactory__["b" /* RenderType_MatButton */])), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](1, 180224, null, 0, __WEBPACK_IMPORTED_MODULE_3__angular_material_button__["b" /* MatButton */], [__WEBPACK_IMPORTED_MODULE_1__angular_core__["ElementRef"], __WEBPACK_IMPORTED_MODULE_4__angular_cdk_platform__["a" /* Platform */], __WEBPACK_IMPORTED_MODULE_5__angular_cdk_a11y__["j" /* FocusMonitor */]], null, null), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, 0, ["\n        "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵeld"](3, 0, null, 0, 10, "div", [["fxLayout", "row"], ["fxLayoutAlign", "start center"]], null, null, null, null, null)), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](4, 737280, null, 0, __WEBPACK_IMPORTED_MODULE_6__angular_flex_layout__["j" /* LayoutDirective */], [__WEBPACK_IMPORTED_MODULE_6__angular_flex_layout__["m" /* MediaMonitor */], __WEBPACK_IMPORTED_MODULE_1__angular_core__["ElementRef"], __WEBPACK_IMPORTED_MODULE_6__angular_flex_layout__["t" /* StyleUtils */]], { layout: [0, "layout"] }, null), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](5, 737280, null, 0, __WEBPACK_IMPORTED_MODULE_6__angular_flex_layout__["i" /* LayoutAlignDirective */], [__WEBPACK_IMPORTED_MODULE_6__angular_flex_layout__["m" /* MediaMonitor */], __WEBPACK_IMPORTED_MODULE_1__angular_core__["ElementRef"], [2, __WEBPACK_IMPORTED_MODULE_6__angular_flex_layout__["j" /* LayoutDirective */]], __WEBPACK_IMPORTED_MODULE_6__angular_flex_layout__["t" /* StyleUtils */]], { align: [0, "align"] }, null), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n            "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵeld"](7, 0, null, null, 2, "mat-icon", [["class", "mat-red-bg mat-icon"], ["role", "img"]], null, null, null, __WEBPACK_IMPORTED_MODULE_7__node_modules_angular_material_icon_typings_index_ngfactory__["b" /* View_MatIcon_0 */], __WEBPACK_IMPORTED_MODULE_7__node_modules_angular_material_icon_typings_index_ngfactory__["a" /* RenderType_MatIcon */])), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](8, 638976, null, 0, __WEBPACK_IMPORTED_MODULE_8__angular_material_icon__["b" /* MatIcon */], [__WEBPACK_IMPORTED_MODULE_1__angular_core__["ElementRef"], __WEBPACK_IMPORTED_MODULE_8__angular_material_icon__["d" /* MatIconRegistry */], [8, null]], null, null), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, 0, ["update"])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n            "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵeld"](11, 0, null, null, 1, "span", [], null, null, null, null, null)), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["Update list"])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n        "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, 0, ["\n    "]))], function (_ck, _v) { var currVal_1 = "row"; _ck(_v, 4, 0, currVal_1); var currVal_2 = "start center"; _ck(_v, 5, 0, currVal_2); _ck(_v, 8, 0); }, function (_ck, _v) { var currVal_0 = (__WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 1).disabled || null); _ck(_v, 0, 0, currVal_0); }); }
function View_MerchantFeaturedBoardAddListComponent_2(_l) { return __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵvid"](0, [(_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵeld"](0, 0, null, null, 34, "form", [["class", "new-list-form"], ["fxFlex", "1 0 auto"], ["fxLayout", "row"], ["fxLayoutAlign", "start center"], ["novalidate", ""]], [[2, "ng-untouched", null], [2, "ng-touched", null], [2, "ng-pristine", null], [2, "ng-dirty", null], [2, "ng-valid", null], [2, "ng-invalid", null], [2, "ng-pending", null]], [[null, "submit"], [null, "reset"]], function (_v, en, $event) { var ad = true; var _co = _v.component; if (("submit" === en)) {
        var pd_0 = (__WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 2).onSubmit($event) !== false);
        ad = (pd_0 && ad);
    } if (("reset" === en)) {
        var pd_1 = (__WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 2).onReset() !== false);
        ad = (pd_1 && ad);
    } if (("submit" === en)) {
        var pd_2 = (_co.onFormSubmit() !== false);
        ad = (pd_2 && ad);
    } return ad; }, null, null)), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](1, 16384, null, 0, __WEBPACK_IMPORTED_MODULE_9__angular_forms__["F" /* ɵbf */], [], null, null), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](2, 540672, null, 0, __WEBPACK_IMPORTED_MODULE_9__angular_forms__["n" /* FormGroupDirective */], [[8, null], [8, null]], { form: [0, "form"] }, null), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵprd"](2048, null, __WEBPACK_IMPORTED_MODULE_9__angular_forms__["d" /* ControlContainer */], null, [__WEBPACK_IMPORTED_MODULE_9__angular_forms__["n" /* FormGroupDirective */]]), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](4, 16384, null, 0, __WEBPACK_IMPORTED_MODULE_9__angular_forms__["w" /* NgControlStatusGroup */], [__WEBPACK_IMPORTED_MODULE_9__angular_forms__["d" /* ControlContainer */]], null, null), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](5, 737280, null, 0, __WEBPACK_IMPORTED_MODULE_6__angular_flex_layout__["j" /* LayoutDirective */], [__WEBPACK_IMPORTED_MODULE_6__angular_flex_layout__["m" /* MediaMonitor */], __WEBPACK_IMPORTED_MODULE_1__angular_core__["ElementRef"], __WEBPACK_IMPORTED_MODULE_6__angular_flex_layout__["t" /* StyleUtils */]], { layout: [0, "layout"] }, null), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](6, 737280, null, 0, __WEBPACK_IMPORTED_MODULE_6__angular_flex_layout__["i" /* LayoutAlignDirective */], [__WEBPACK_IMPORTED_MODULE_6__angular_flex_layout__["m" /* MediaMonitor */], __WEBPACK_IMPORTED_MODULE_1__angular_core__["ElementRef"], [2, __WEBPACK_IMPORTED_MODULE_6__angular_flex_layout__["j" /* LayoutDirective */]], __WEBPACK_IMPORTED_MODULE_6__angular_flex_layout__["t" /* StyleUtils */]], { align: [0, "align"] }, null), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](7, 737280, null, 0, __WEBPACK_IMPORTED_MODULE_6__angular_flex_layout__["e" /* FlexDirective */], [__WEBPACK_IMPORTED_MODULE_6__angular_flex_layout__["m" /* MediaMonitor */], __WEBPACK_IMPORTED_MODULE_1__angular_core__["ElementRef"], [3, __WEBPACK_IMPORTED_MODULE_6__angular_flex_layout__["j" /* LayoutDirective */]], __WEBPACK_IMPORTED_MODULE_6__angular_flex_layout__["t" /* StyleUtils */]], { flex: [0, "flex"] }, null), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n\n        "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵeld"](9, 0, [[1, 0], ["nameInput", 1]], null, 6, "input", [["formControlName", "name"], ["fxFlex", ""], ["placeholder", "Write a list Name"]], [[2, "ng-untouched", null], [2, "ng-touched", null], [2, "ng-pristine", null], [2, "ng-dirty", null], [2, "ng-valid", null], [2, "ng-invalid", null], [2, "ng-pending", null]], [[null, "input"], [null, "blur"], [null, "compositionstart"], [null, "compositionend"]], function (_v, en, $event) { var ad = true; if (("input" === en)) {
        var pd_0 = (__WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 10)._handleInput($event.target.value) !== false);
        ad = (pd_0 && ad);
    } if (("blur" === en)) {
        var pd_1 = (__WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 10).onTouched() !== false);
        ad = (pd_1 && ad);
    } if (("compositionstart" === en)) {
        var pd_2 = (__WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 10)._compositionStart() !== false);
        ad = (pd_2 && ad);
    } if (("compositionend" === en)) {
        var pd_3 = (__WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 10)._compositionEnd($event.target.value) !== false);
        ad = (pd_3 && ad);
    } return ad; }, null, null)), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](10, 16384, null, 0, __WEBPACK_IMPORTED_MODULE_9__angular_forms__["e" /* DefaultValueAccessor */], [__WEBPACK_IMPORTED_MODULE_1__angular_core__["Renderer2"], __WEBPACK_IMPORTED_MODULE_1__angular_core__["ElementRef"], [2, __WEBPACK_IMPORTED_MODULE_9__angular_forms__["a" /* COMPOSITION_BUFFER_MODE */]]], null, null), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵprd"](1024, null, __WEBPACK_IMPORTED_MODULE_9__angular_forms__["t" /* NG_VALUE_ACCESSOR */], function (p0_0) { return [p0_0]; }, [__WEBPACK_IMPORTED_MODULE_9__angular_forms__["e" /* DefaultValueAccessor */]]), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](12, 671744, null, 0, __WEBPACK_IMPORTED_MODULE_9__angular_forms__["l" /* FormControlName */], [[3, __WEBPACK_IMPORTED_MODULE_9__angular_forms__["d" /* ControlContainer */]], [8, null], [8, null], [2, __WEBPACK_IMPORTED_MODULE_9__angular_forms__["t" /* NG_VALUE_ACCESSOR */]]], { name: [0, "name"] }, null), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵprd"](2048, null, __WEBPACK_IMPORTED_MODULE_9__angular_forms__["u" /* NgControl */], null, [__WEBPACK_IMPORTED_MODULE_9__angular_forms__["l" /* FormControlName */]]), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](14, 16384, null, 0, __WEBPACK_IMPORTED_MODULE_9__angular_forms__["v" /* NgControlStatus */], [__WEBPACK_IMPORTED_MODULE_9__angular_forms__["u" /* NgControl */]], null, null), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](15, 737280, null, 0, __WEBPACK_IMPORTED_MODULE_6__angular_flex_layout__["e" /* FlexDirective */], [__WEBPACK_IMPORTED_MODULE_6__angular_flex_layout__["m" /* MediaMonitor */], __WEBPACK_IMPORTED_MODULE_1__angular_core__["ElementRef"], [3, __WEBPACK_IMPORTED_MODULE_6__angular_flex_layout__["j" /* LayoutDirective */]], __WEBPACK_IMPORTED_MODULE_6__angular_flex_layout__["t" /* StyleUtils */]], { flex: [0, "flex"] }, null), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n\n        "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵeld"](17, 0, null, null, 7, "button", [["fxFlex", "0 1 auto"], ["mat-icon-button", ""]], [[8, "disabled", 0]], null, null, __WEBPACK_IMPORTED_MODULE_2__node_modules_angular_material_button_typings_index_ngfactory__["d" /* View_MatButton_0 */], __WEBPACK_IMPORTED_MODULE_2__node_modules_angular_material_button_typings_index_ngfactory__["b" /* RenderType_MatButton */])), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](18, 180224, null, 0, __WEBPACK_IMPORTED_MODULE_3__angular_material_button__["b" /* MatButton */], [__WEBPACK_IMPORTED_MODULE_1__angular_core__["ElementRef"], __WEBPACK_IMPORTED_MODULE_4__angular_cdk_platform__["a" /* Platform */], __WEBPACK_IMPORTED_MODULE_5__angular_cdk_a11y__["j" /* FocusMonitor */]], null, null), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](19, 737280, null, 0, __WEBPACK_IMPORTED_MODULE_6__angular_flex_layout__["e" /* FlexDirective */], [__WEBPACK_IMPORTED_MODULE_6__angular_flex_layout__["m" /* MediaMonitor */], __WEBPACK_IMPORTED_MODULE_1__angular_core__["ElementRef"], [3, __WEBPACK_IMPORTED_MODULE_6__angular_flex_layout__["j" /* LayoutDirective */]], __WEBPACK_IMPORTED_MODULE_6__angular_flex_layout__["t" /* StyleUtils */]], { flex: [0, "flex"] }, null), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, 0, ["\n            "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵeld"](21, 0, null, 0, 2, "mat-icon", [["class", "mat-icon"], ["role", "img"]], null, null, null, __WEBPACK_IMPORTED_MODULE_7__node_modules_angular_material_icon_typings_index_ngfactory__["b" /* View_MatIcon_0 */], __WEBPACK_IMPORTED_MODULE_7__node_modules_angular_material_icon_typings_index_ngfactory__["a" /* RenderType_MatIcon */])), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](22, 638976, null, 0, __WEBPACK_IMPORTED_MODULE_8__angular_material_icon__["b" /* MatIcon */], [__WEBPACK_IMPORTED_MODULE_1__angular_core__["ElementRef"], __WEBPACK_IMPORTED_MODULE_8__angular_material_icon__["d" /* MatIconRegistry */], [8, null]], null, null), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, 0, ["check"])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, 0, ["\n        "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n        "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵeld"](26, 0, null, null, 7, "button", [["fxFlex", "0 1 auto"], ["mat-icon-button", ""]], [[8, "disabled", 0]], [[null, "click"]], function (_v, en, $event) { var ad = true; var _co = _v.component; if (("click" === en)) {
        var pd_0 = (_co.closeForm() !== false);
        ad = (pd_0 && ad);
    } return ad; }, __WEBPACK_IMPORTED_MODULE_2__node_modules_angular_material_button_typings_index_ngfactory__["d" /* View_MatButton_0 */], __WEBPACK_IMPORTED_MODULE_2__node_modules_angular_material_button_typings_index_ngfactory__["b" /* RenderType_MatButton */])), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](27, 180224, null, 0, __WEBPACK_IMPORTED_MODULE_3__angular_material_button__["b" /* MatButton */], [__WEBPACK_IMPORTED_MODULE_1__angular_core__["ElementRef"], __WEBPACK_IMPORTED_MODULE_4__angular_cdk_platform__["a" /* Platform */], __WEBPACK_IMPORTED_MODULE_5__angular_cdk_a11y__["j" /* FocusMonitor */]], null, null), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](28, 737280, null, 0, __WEBPACK_IMPORTED_MODULE_6__angular_flex_layout__["e" /* FlexDirective */], [__WEBPACK_IMPORTED_MODULE_6__angular_flex_layout__["m" /* MediaMonitor */], __WEBPACK_IMPORTED_MODULE_1__angular_core__["ElementRef"], [3, __WEBPACK_IMPORTED_MODULE_6__angular_flex_layout__["j" /* LayoutDirective */]], __WEBPACK_IMPORTED_MODULE_6__angular_flex_layout__["t" /* StyleUtils */]], { flex: [0, "flex"] }, null), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, 0, ["\n            "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵeld"](30, 0, null, 0, 2, "mat-icon", [["class", "mat-icon"], ["role", "img"]], null, null, null, __WEBPACK_IMPORTED_MODULE_7__node_modules_angular_material_icon_typings_index_ngfactory__["b" /* View_MatIcon_0 */], __WEBPACK_IMPORTED_MODULE_7__node_modules_angular_material_icon_typings_index_ngfactory__["a" /* RenderType_MatIcon */])), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](31, 638976, null, 0, __WEBPACK_IMPORTED_MODULE_8__angular_material_icon__["b" /* MatIcon */], [__WEBPACK_IMPORTED_MODULE_1__angular_core__["ElementRef"], __WEBPACK_IMPORTED_MODULE_8__angular_material_icon__["d" /* MatIconRegistry */], [8, null]], null, null), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, 0, ["close"])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, 0, ["\n        "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n    "]))], function (_ck, _v) { var _co = _v.component; var currVal_7 = _co.form; _ck(_v, 2, 0, currVal_7); var currVal_8 = "row"; _ck(_v, 5, 0, currVal_8); var currVal_9 = "start center"; _ck(_v, 6, 0, currVal_9); var currVal_10 = "1 0 auto"; _ck(_v, 7, 0, currVal_10); var currVal_18 = "name"; _ck(_v, 12, 0, currVal_18); var currVal_19 = ""; _ck(_v, 15, 0, currVal_19); var currVal_21 = "0 1 auto"; _ck(_v, 19, 0, currVal_21); _ck(_v, 22, 0); var currVal_23 = "0 1 auto"; _ck(_v, 28, 0, currVal_23); _ck(_v, 31, 0); }, function (_ck, _v) { var currVal_0 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 4).ngClassUntouched; var currVal_1 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 4).ngClassTouched; var currVal_2 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 4).ngClassPristine; var currVal_3 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 4).ngClassDirty; var currVal_4 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 4).ngClassValid; var currVal_5 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 4).ngClassInvalid; var currVal_6 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 4).ngClassPending; _ck(_v, 0, 0, currVal_0, currVal_1, currVal_2, currVal_3, currVal_4, currVal_5, currVal_6); var currVal_11 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 14).ngClassUntouched; var currVal_12 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 14).ngClassTouched; var currVal_13 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 14).ngClassPristine; var currVal_14 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 14).ngClassDirty; var currVal_15 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 14).ngClassValid; var currVal_16 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 14).ngClassInvalid; var currVal_17 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 14).ngClassPending; _ck(_v, 9, 0, currVal_11, currVal_12, currVal_13, currVal_14, currVal_15, currVal_16, currVal_17); var currVal_20 = (__WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 18).disabled || null); _ck(_v, 17, 0, currVal_20); var currVal_22 = (__WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 27).disabled || null); _ck(_v, 26, 0, currVal_22); }); }
function View_MerchantFeaturedBoardAddListComponent_0(_l) { return __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵvid"](0, [__WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵqud"](671088640, 1, { nameInputField: 0 }), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵeld"](1, 0, null, null, 7, "div", [["class", "list new-list mat-elevation-z1"]], null, null, null, null, null)), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n\n    "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵand"](16777216, null, null, 1, null, View_MerchantFeaturedBoardAddListComponent_1)), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](4, 16384, null, 0, __WEBPACK_IMPORTED_MODULE_10__angular_common__["NgIf"], [__WEBPACK_IMPORTED_MODULE_1__angular_core__["ViewContainerRef"], __WEBPACK_IMPORTED_MODULE_1__angular_core__["TemplateRef"]], { ngIf: [0, "ngIf"] }, null), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n\n    "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵand"](16777216, null, null, 1, null, View_MerchantFeaturedBoardAddListComponent_2)), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](7, 16384, null, 0, __WEBPACK_IMPORTED_MODULE_10__angular_common__["NgIf"], [__WEBPACK_IMPORTED_MODULE_1__angular_core__["ViewContainerRef"], __WEBPACK_IMPORTED_MODULE_1__angular_core__["TemplateRef"]], { ngIf: [0, "ngIf"] }, null), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n"])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n\n"]))], function (_ck, _v) { var _co = _v.component; var currVal_0 = !_co.formActive; _ck(_v, 4, 0, currVal_0); var currVal_1 = _co.formActive; _ck(_v, 7, 0, currVal_1); }, null); }
function View_MerchantFeaturedBoardAddListComponent_Host_0(_l) { return __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵvid"](0, [(_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵeld"](0, 0, null, null, 1, "fuse-scrumboard-board-add-list", [], null, null, null, View_MerchantFeaturedBoardAddListComponent_0, RenderType_MerchantFeaturedBoardAddListComponent)), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](1, 49152, null, 0, __WEBPACK_IMPORTED_MODULE_11__add_list_component__["a" /* MerchantFeaturedBoardAddListComponent */], [__WEBPACK_IMPORTED_MODULE_9__angular_forms__["i" /* FormBuilder */], __WEBPACK_IMPORTED_MODULE_12__angular_router__["o" /* Router */]], null, null)], null, null); }
var MerchantFeaturedBoardAddListComponentNgFactory = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵccf"]("fuse-scrumboard-board-add-list", __WEBPACK_IMPORTED_MODULE_11__add_list_component__["a" /* MerchantFeaturedBoardAddListComponent */], View_MerchantFeaturedBoardAddListComponent_Host_0, {}, { onlistAdd: "onlistAdd" }, []);



/***/ }),

/***/ "./src/app/main/content/pages/merchant-featured/merchant-featured-board/add-list/add-list.component.scss.shim.ngstyle.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return styles; });
/**
 * @fileoverview This file was generated by the Angular template compiler. Do not edit.
 *
 * @suppress {suspiciousCode,uselessCode,missingProperties,missingOverride,checkTypes}
 * tslint:disable
 */ 
var styles = ["[_nghost-%COMP%]   .new-list[_ngcontent-%COMP%] {\n  border-radius: 2px;\n  background-color: #EEF0F2; }\n  [_nghost-%COMP%]   .new-list[_ngcontent-%COMP%]   .new-list-form-button[_ngcontent-%COMP%] {\n    text-transform: none;\n    font-size: 15px;\n    padding: 0 16px;\n    height: 64px;\n    margin: 0;\n    width: 100%; }\n  [_nghost-%COMP%]   .new-list[_ngcontent-%COMP%]   .new-list-form-button[_ngcontent-%COMP%]   mat-icon[_ngcontent-%COMP%] {\n      border-radius: 50%;\n      height: 40px;\n      width: 40px;\n      line-height: 40px;\n      margin-right: 16px; }\n  [_nghost-%COMP%]   .new-list[_ngcontent-%COMP%]   .new-list-form[_ngcontent-%COMP%] {\n    padding: 16px;\n    height: 64px; }\n  [_nghost-%COMP%]   .new-list[_ngcontent-%COMP%]   .new-list-form[_ngcontent-%COMP%]    > input[_ngcontent-%COMP%] {\n      height: 100%; }"];



/***/ }),

/***/ "./src/app/main/content/pages/merchant-featured/merchant-featured-board/add-list/add-list.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MerchantFeaturedBoardAddListComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__("./node_modules/@angular/forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");



var MerchantFeaturedBoardAddListComponent = /** @class */ (function () {
    function MerchantFeaturedBoardAddListComponent(formBuilder, router) {
        this.formBuilder = formBuilder;
        this.router = router;
        this.formActive = false;
        this.onlistAdd = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
    }
    MerchantFeaturedBoardAddListComponent.prototype.openForm = function () {
        debugger;
        // this.form = this.formBuilder.group({
        //     name: ['']
        // });
        // this.formActive = true;
        // this.focusNameField();
        this.onFormSubmit();
        // location.reload();
    };
    MerchantFeaturedBoardAddListComponent.prototype.closeForm = function () {
        this.formActive = false;
    };
    MerchantFeaturedBoardAddListComponent.prototype.focusNameField = function () {
        var _this = this;
        setTimeout(function () {
            _this.nameInputField.nativeElement.focus();
        });
    };
    MerchantFeaturedBoardAddListComponent.prototype.onFormSubmit = function () {
        var _this = this;
        // if ( this.form.valid )
        // {
        //     this.onlistAdd.next(this.form.getRawValue().name);
        //     this.formActive = false;
        // }
        this.onlistAdd.next();
        this.router.navigateByUrl('/RefreshComponent', { skipLocationChange: true }).then(function () {
            return _this.router.navigate(["pages/featured-merchants/"]);
        });
    };
    return MerchantFeaturedBoardAddListComponent;
}());



/***/ }),

/***/ "./src/app/main/content/pages/merchant-featured/merchant-featured-board/dialogs/card/card.component.ngfactory.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export RenderType_MerchantFeaturedCardDialogComponent */
/* unused harmony export View_MerchantFeaturedCardDialogComponent_0 */
/* unused harmony export View_MerchantFeaturedCardDialogComponent_Host_0 */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MerchantFeaturedCardDialogComponentNgFactory; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__card_component_scss_ngstyle__ = __webpack_require__("./src/app/main/content/pages/merchant-featured/merchant-featured-board/dialogs/card/card.component.scss.ngstyle.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__node_modules_angular_material_toolbar_typings_index_ngfactory__ = __webpack_require__("./node_modules/@angular/material/toolbar/typings/index.ngfactory.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_material_dialog__ = __webpack_require__("./node_modules/@angular/material/esm5/dialog.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_material_toolbar__ = __webpack_require__("./node_modules/@angular/material/esm5/toolbar.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_cdk_platform__ = __webpack_require__("./node_modules/@angular/cdk/esm5/platform.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_common__ = __webpack_require__("./node_modules/@angular/common/esm5/common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__angular_flex_layout__ = __webpack_require__("./node_modules/@angular/flex-layout/esm5/flex-layout.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__node_modules_angular_material_button_typings_index_ngfactory__ = __webpack_require__("./node_modules/@angular/material/button/typings/index.ngfactory.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__angular_material_button__ = __webpack_require__("./node_modules/@angular/material/esm5/button.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__angular_cdk_a11y__ = __webpack_require__("./node_modules/@angular/cdk/esm5/a11y.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__node_modules_angular_material_icon_typings_index_ngfactory__ = __webpack_require__("./node_modules/@angular/material/icon/typings/index.ngfactory.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__angular_material_icon__ = __webpack_require__("./node_modules/@angular/material/esm5/icon.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__angular_material_chips__ = __webpack_require__("./node_modules/@angular/material/esm5/chips.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__angular_material_tooltip__ = __webpack_require__("./node_modules/@angular/material/esm5/tooltip.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__angular_cdk_overlay__ = __webpack_require__("./node_modules/@angular/cdk/esm5/overlay.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__angular_cdk_scrolling__ = __webpack_require__("./node_modules/@angular/cdk/esm5/scrolling.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__angular_cdk_bidi__ = __webpack_require__("./node_modules/@angular/cdk/esm5/bidi.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__fuse_pipes_getById_pipe__ = __webpack_require__("./src/@fuse/pipes/getById.pipe.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__node_modules_angular_material_chips_typings_index_ngfactory__ = __webpack_require__("./node_modules/@angular/material/chips/typings/index.ngfactory.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__angular_material_form_field__ = __webpack_require__("./node_modules/@angular/material/esm5/form-field.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__angular_forms__ = __webpack_require__("./node_modules/@angular/forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__angular_material_core__ = __webpack_require__("./node_modules/@angular/material/esm5/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__fuse_directives_fuse_perfect_scrollbar_fuse_perfect_scrollbar_directive__ = __webpack_require__("./src/@fuse/directives/fuse-perfect-scrollbar/fuse-perfect-scrollbar.directive.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__fuse_services_config_service__ = __webpack_require__("./src/@fuse/services/config.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__node_modules_angular_material_form_field_typings_index_ngfactory__ = __webpack_require__("./node_modules/@angular/material/form-field/typings/index.ngfactory.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__angular_material_input__ = __webpack_require__("./node_modules/@angular/material/esm5/input.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_27__card_component__ = __webpack_require__("./src/app/main/content/pages/merchant-featured/merchant-featured-board/dialogs/card/card.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_28__merchant_featured_service__ = __webpack_require__("./src/app/main/content/pages/merchant-featured/merchant-featured.service.ts");
/**
 * @fileoverview This file was generated by the Angular template compiler. Do not edit.
 *
 * @suppress {suspiciousCode,uselessCode,missingProperties,missingOverride,checkTypes}
 * tslint:disable
 */ 





























var styles_MerchantFeaturedCardDialogComponent = [__WEBPACK_IMPORTED_MODULE_0__card_component_scss_ngstyle__["a" /* styles */]];
var RenderType_MerchantFeaturedCardDialogComponent = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵcrt"]({ encapsulation: 2, styles: styles_MerchantFeaturedCardDialogComponent, data: {} });

function View_MerchantFeaturedCardDialogComponent_1(_l) { return __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵvid"](0, [(_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵeld"](0, 0, null, null, 47, "mat-toolbar", [["class", "mat-accent-bg m-0 mat-dialog-title mat-toolbar"], ["matDialogTitle", ""]], [[8, "id", 0], [2, "mat-toolbar-multiple-rows", null], [2, "mat-toolbar-single-row", null]], null, null, __WEBPACK_IMPORTED_MODULE_2__node_modules_angular_material_toolbar_typings_index_ngfactory__["b" /* View_MatToolbar_0 */], __WEBPACK_IMPORTED_MODULE_2__node_modules_angular_material_toolbar_typings_index_ngfactory__["a" /* RenderType_MatToolbar */])), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](1, 81920, null, 0, __WEBPACK_IMPORTED_MODULE_3__angular_material_dialog__["l" /* MatDialogTitle */], [[2, __WEBPACK_IMPORTED_MODULE_3__angular_material_dialog__["k" /* MatDialogRef */]], __WEBPACK_IMPORTED_MODULE_1__angular_core__["ElementRef"], __WEBPACK_IMPORTED_MODULE_3__angular_material_dialog__["e" /* MatDialog */]], null, null), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](2, 4243456, null, 1, __WEBPACK_IMPORTED_MODULE_4__angular_material_toolbar__["a" /* MatToolbar */], [__WEBPACK_IMPORTED_MODULE_1__angular_core__["ElementRef"], __WEBPACK_IMPORTED_MODULE_5__angular_cdk_platform__["a" /* Platform */], __WEBPACK_IMPORTED_MODULE_6__angular_common__["DOCUMENT"]], null, null), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵqud"](603979776, 3, { _toolbarRows: 1 }), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, 0, ["\n\n        "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵeld"](5, 0, null, 0, 41, "div", [["fxFlex", ""], ["fxLayout", "row"], ["fxLayoutAlign", "space-between center"]], null, null, null, null, null)), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](6, 737280, null, 0, __WEBPACK_IMPORTED_MODULE_7__angular_flex_layout__["j" /* LayoutDirective */], [__WEBPACK_IMPORTED_MODULE_7__angular_flex_layout__["m" /* MediaMonitor */], __WEBPACK_IMPORTED_MODULE_1__angular_core__["ElementRef"], __WEBPACK_IMPORTED_MODULE_7__angular_flex_layout__["t" /* StyleUtils */]], { layout: [0, "layout"] }, null), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](7, 737280, null, 0, __WEBPACK_IMPORTED_MODULE_7__angular_flex_layout__["i" /* LayoutAlignDirective */], [__WEBPACK_IMPORTED_MODULE_7__angular_flex_layout__["m" /* MediaMonitor */], __WEBPACK_IMPORTED_MODULE_1__angular_core__["ElementRef"], [2, __WEBPACK_IMPORTED_MODULE_7__angular_flex_layout__["j" /* LayoutDirective */]], __WEBPACK_IMPORTED_MODULE_7__angular_flex_layout__["t" /* StyleUtils */]], { align: [0, "align"] }, null), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](8, 737280, null, 0, __WEBPACK_IMPORTED_MODULE_7__angular_flex_layout__["e" /* FlexDirective */], [__WEBPACK_IMPORTED_MODULE_7__angular_flex_layout__["m" /* MediaMonitor */], __WEBPACK_IMPORTED_MODULE_1__angular_core__["ElementRef"], [3, __WEBPACK_IMPORTED_MODULE_7__angular_flex_layout__["j" /* LayoutDirective */]], __WEBPACK_IMPORTED_MODULE_7__angular_flex_layout__["t" /* StyleUtils */]], { flex: [0, "flex"] }, null), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n\n            "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵeld"](10, 0, null, null, 25, "div", [["fxFlex", ""], ["fxLayout", "row"], ["fxLayoutAlign", "start center"]], null, null, null, null, null)), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](11, 737280, null, 0, __WEBPACK_IMPORTED_MODULE_7__angular_flex_layout__["j" /* LayoutDirective */], [__WEBPACK_IMPORTED_MODULE_7__angular_flex_layout__["m" /* MediaMonitor */], __WEBPACK_IMPORTED_MODULE_1__angular_core__["ElementRef"], __WEBPACK_IMPORTED_MODULE_7__angular_flex_layout__["t" /* StyleUtils */]], { layout: [0, "layout"] }, null), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](12, 737280, null, 0, __WEBPACK_IMPORTED_MODULE_7__angular_flex_layout__["i" /* LayoutAlignDirective */], [__WEBPACK_IMPORTED_MODULE_7__angular_flex_layout__["m" /* MediaMonitor */], __WEBPACK_IMPORTED_MODULE_1__angular_core__["ElementRef"], [2, __WEBPACK_IMPORTED_MODULE_7__angular_flex_layout__["j" /* LayoutDirective */]], __WEBPACK_IMPORTED_MODULE_7__angular_flex_layout__["t" /* StyleUtils */]], { align: [0, "align"] }, null), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](13, 737280, null, 0, __WEBPACK_IMPORTED_MODULE_7__angular_flex_layout__["e" /* FlexDirective */], [__WEBPACK_IMPORTED_MODULE_7__angular_flex_layout__["m" /* MediaMonitor */], __WEBPACK_IMPORTED_MODULE_1__angular_core__["ElementRef"], [3, __WEBPACK_IMPORTED_MODULE_7__angular_flex_layout__["j" /* LayoutDirective */]], __WEBPACK_IMPORTED_MODULE_7__angular_flex_layout__["t" /* StyleUtils */]], { flex: [0, "flex"] }, null), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n\n                "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n                "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n                "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n\n                "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n                "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n                "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n\n                "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n                "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n                "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n\n                "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n                "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n                "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n\n                "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n                "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n                "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n\n                "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n                "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n                "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n\n                "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n                "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n                "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n            "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n\n            "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n            "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵeld"](38, 0, null, null, 6, "button", [["aria-label", "Close Dialog"], ["mat-icon-button", ""]], [[8, "disabled", 0]], [[null, "click"]], function (_v, en, $event) { var ad = true; var _co = _v.component; if (("click" === en)) {
        var pd_0 = (_co.dialogRef.close() !== false);
        ad = (pd_0 && ad);
    } return ad; }, __WEBPACK_IMPORTED_MODULE_8__node_modules_angular_material_button_typings_index_ngfactory__["d" /* View_MatButton_0 */], __WEBPACK_IMPORTED_MODULE_8__node_modules_angular_material_button_typings_index_ngfactory__["b" /* RenderType_MatButton */])), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](39, 180224, null, 0, __WEBPACK_IMPORTED_MODULE_9__angular_material_button__["b" /* MatButton */], [__WEBPACK_IMPORTED_MODULE_1__angular_core__["ElementRef"], __WEBPACK_IMPORTED_MODULE_5__angular_cdk_platform__["a" /* Platform */], __WEBPACK_IMPORTED_MODULE_10__angular_cdk_a11y__["j" /* FocusMonitor */]], null, null), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, 0, ["\n                "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵeld"](41, 0, null, 0, 2, "mat-icon", [["class", "mat-icon"], ["role", "img"]], null, null, null, __WEBPACK_IMPORTED_MODULE_11__node_modules_angular_material_icon_typings_index_ngfactory__["b" /* View_MatIcon_0 */], __WEBPACK_IMPORTED_MODULE_11__node_modules_angular_material_icon_typings_index_ngfactory__["a" /* RenderType_MatIcon */])), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](42, 638976, null, 0, __WEBPACK_IMPORTED_MODULE_12__angular_material_icon__["b" /* MatIcon */], [__WEBPACK_IMPORTED_MODULE_1__angular_core__["ElementRef"], __WEBPACK_IMPORTED_MODULE_12__angular_material_icon__["d" /* MatIconRegistry */], [8, null]], null, null), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, 0, ["close"])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, 0, ["\n            "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n            "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n        "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, 0, ["\n    "]))], function (_ck, _v) { _ck(_v, 1, 0); var currVal_3 = "row"; _ck(_v, 6, 0, currVal_3); var currVal_4 = "space-between center"; _ck(_v, 7, 0, currVal_4); var currVal_5 = ""; _ck(_v, 8, 0, currVal_5); var currVal_6 = "row"; _ck(_v, 11, 0, currVal_6); var currVal_7 = "start center"; _ck(_v, 12, 0, currVal_7); var currVal_8 = ""; _ck(_v, 13, 0, currVal_8); _ck(_v, 42, 0); }, function (_ck, _v) { var currVal_0 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 1).id; var currVal_1 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 2)._toolbarRows.length; var currVal_2 = !__WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 2)._toolbarRows.length; _ck(_v, 0, 0, currVal_0, currVal_1, currVal_2); var currVal_9 = (__WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 39).disabled || null); _ck(_v, 38, 0, currVal_9); }); }
function View_MerchantFeaturedCardDialogComponent_5(_l) { return __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵvid"](0, [(_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵeld"](0, 0, null, null, 11, "mat-chip", [["class", "member-chip mat-accent-bg mat-chip"], ["fxLayout", "row"], ["fxLayoutAlign", "start center"], ["role", "option"]], [[1, "tabindex", 0], [2, "mat-chip-selected", null], [1, "disabled", 0], [1, "aria-disabled", 0], [1, "aria-selected", 0]], [[null, "click"], [null, "keydown"], [null, "focus"], [null, "blur"]], function (_v, en, $event) { var ad = true; if (("click" === en)) {
        var pd_0 = (__WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 1)._handleClick($event) !== false);
        ad = (pd_0 && ad);
    } if (("keydown" === en)) {
        var pd_1 = (__WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 1)._handleKeydown($event) !== false);
        ad = (pd_1 && ad);
    } if (("focus" === en)) {
        var pd_2 = ((__WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 1)._hasFocus = true) !== false);
        ad = (pd_2 && ad);
    } if (("blur" === en)) {
        var pd_3 = (__WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 1)._blur() !== false);
        ad = (pd_3 && ad);
    } return ad; }, null, null)), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](1, 147456, [[25, 4]], 0, __WEBPACK_IMPORTED_MODULE_13__angular_material_chips__["a" /* MatChip */], [__WEBPACK_IMPORTED_MODULE_1__angular_core__["ElementRef"]], null, null), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](2, 737280, null, 0, __WEBPACK_IMPORTED_MODULE_7__angular_flex_layout__["j" /* LayoutDirective */], [__WEBPACK_IMPORTED_MODULE_7__angular_flex_layout__["m" /* MediaMonitor */], __WEBPACK_IMPORTED_MODULE_1__angular_core__["ElementRef"], __WEBPACK_IMPORTED_MODULE_7__angular_flex_layout__["t" /* StyleUtils */]], { layout: [0, "layout"] }, null), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](3, 737280, null, 0, __WEBPACK_IMPORTED_MODULE_7__angular_flex_layout__["i" /* LayoutAlignDirective */], [__WEBPACK_IMPORTED_MODULE_7__angular_flex_layout__["m" /* MediaMonitor */], __WEBPACK_IMPORTED_MODULE_1__angular_core__["ElementRef"], [2, __WEBPACK_IMPORTED_MODULE_7__angular_flex_layout__["j" /* LayoutDirective */]], __WEBPACK_IMPORTED_MODULE_7__angular_flex_layout__["t" /* StyleUtils */]], { align: [0, "align"] }, null), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n                                "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵeld"](5, 16777216, null, null, 3, "img", [["class", "member-chip-avatar"]], [[8, "src", 4]], [[null, "longpress"], [null, "keydown"], [null, "touchend"]], function (_v, en, $event) { var ad = true; if (("longpress" === en)) {
        var pd_0 = (__WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 6).show() !== false);
        ad = (pd_0 && ad);
    } if (("keydown" === en)) {
        var pd_1 = (__WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 6)._handleKeydown($event) !== false);
        ad = (pd_1 && ad);
    } if (("touchend" === en)) {
        var pd_2 = (__WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 6)._handleTouchend() !== false);
        ad = (pd_2 && ad);
    } return ad; }, null, null)), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](6, 147456, null, 0, __WEBPACK_IMPORTED_MODULE_14__angular_material_tooltip__["d" /* MatTooltip */], [__WEBPACK_IMPORTED_MODULE_15__angular_cdk_overlay__["c" /* Overlay */], __WEBPACK_IMPORTED_MODULE_1__angular_core__["ElementRef"], __WEBPACK_IMPORTED_MODULE_16__angular_cdk_scrolling__["d" /* ScrollDispatcher */], __WEBPACK_IMPORTED_MODULE_1__angular_core__["ViewContainerRef"], __WEBPACK_IMPORTED_MODULE_1__angular_core__["NgZone"], __WEBPACK_IMPORTED_MODULE_5__angular_cdk_platform__["a" /* Platform */], __WEBPACK_IMPORTED_MODULE_10__angular_cdk_a11y__["e" /* AriaDescriber */], __WEBPACK_IMPORTED_MODULE_10__angular_cdk_a11y__["j" /* FocusMonitor */], __WEBPACK_IMPORTED_MODULE_14__angular_material_tooltip__["b" /* MAT_TOOLTIP_SCROLL_STRATEGY */], [2, __WEBPACK_IMPORTED_MODULE_17__angular_cdk_bidi__["c" /* Directionality */]], [2, __WEBPACK_IMPORTED_MODULE_14__angular_material_tooltip__["a" /* MAT_TOOLTIP_DEFAULT_OPTIONS */]]], { message: [0, "message"] }, null), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵpid"](0, __WEBPACK_IMPORTED_MODULE_18__fuse_pipes_getById_pipe__["a" /* GetByIdPipe */], []), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵpid"](0, __WEBPACK_IMPORTED_MODULE_18__fuse_pipes_getById_pipe__["a" /* GetByIdPipe */], []), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](9, null, ["", "\n                                "])), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵpid"](0, __WEBPACK_IMPORTED_MODULE_18__fuse_pipes_getById_pipe__["a" /* GetByIdPipe */], []), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n                            "]))], function (_ck, _v) { var _co = _v.component; var currVal_5 = "row"; _ck(_v, 2, 0, currVal_5); var currVal_6 = "start center"; _ck(_v, 3, 0, currVal_6); var currVal_8 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵunv"](_v, 6, 0, __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 7).transform(_co.board.members, _v.context.$implicit, "name")); _ck(_v, 6, 0, currVal_8); }, function (_ck, _v) { var _co = _v.component; var currVal_0 = (__WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 1).disabled ? null : (0 - 1)); var currVal_1 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 1).selected; var currVal_2 = (__WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 1).disabled || null); var currVal_3 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 1).disabled.toString(); var currVal_4 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 1).ariaSelected; _ck(_v, 0, 0, currVal_0, currVal_1, currVal_2, currVal_3, currVal_4); var currVal_7 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵunv"](_v, 5, 0, __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 8).transform(_co.board.members, _v.context.$implicit, "avatar")); _ck(_v, 5, 0, currVal_7); var currVal_9 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵunv"](_v, 9, 0, __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 10).transform(_co.board.members, _v.context.$implicit, "name")); _ck(_v, 9, 0, currVal_9); }); }
function View_MerchantFeaturedCardDialogComponent_4(_l) { return __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵvid"](0, [(_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵeld"](0, 0, null, null, 26, "div", [["class", "members"], ["fxFlex", ""]], null, null, null, null, null)), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](1, 737280, null, 0, __WEBPACK_IMPORTED_MODULE_7__angular_flex_layout__["e" /* FlexDirective */], [__WEBPACK_IMPORTED_MODULE_7__angular_flex_layout__["m" /* MediaMonitor */], __WEBPACK_IMPORTED_MODULE_1__angular_core__["ElementRef"], [3, __WEBPACK_IMPORTED_MODULE_7__angular_flex_layout__["j" /* LayoutDirective */]], __WEBPACK_IMPORTED_MODULE_7__angular_flex_layout__["t" /* StyleUtils */]], { flex: [0, "flex"] }, null), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n                    "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵeld"](3, 0, null, null, 10, "div", [["class", "section-header"], ["fxLayout", "row"], ["fxLayoutAlign", "start center"]], null, null, null, null, null)), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](4, 737280, null, 0, __WEBPACK_IMPORTED_MODULE_7__angular_flex_layout__["j" /* LayoutDirective */], [__WEBPACK_IMPORTED_MODULE_7__angular_flex_layout__["m" /* MediaMonitor */], __WEBPACK_IMPORTED_MODULE_1__angular_core__["ElementRef"], __WEBPACK_IMPORTED_MODULE_7__angular_flex_layout__["t" /* StyleUtils */]], { layout: [0, "layout"] }, null), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](5, 737280, null, 0, __WEBPACK_IMPORTED_MODULE_7__angular_flex_layout__["i" /* LayoutAlignDirective */], [__WEBPACK_IMPORTED_MODULE_7__angular_flex_layout__["m" /* MediaMonitor */], __WEBPACK_IMPORTED_MODULE_1__angular_core__["ElementRef"], [2, __WEBPACK_IMPORTED_MODULE_7__angular_flex_layout__["j" /* LayoutDirective */]], __WEBPACK_IMPORTED_MODULE_7__angular_flex_layout__["t" /* StyleUtils */]], { align: [0, "align"] }, null), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n                        "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵeld"](7, 0, null, null, 2, "mat-icon", [["class", "s-20 mat-icon"], ["role", "img"]], null, null, null, __WEBPACK_IMPORTED_MODULE_11__node_modules_angular_material_icon_typings_index_ngfactory__["b" /* View_MatIcon_0 */], __WEBPACK_IMPORTED_MODULE_11__node_modules_angular_material_icon_typings_index_ngfactory__["a" /* RenderType_MatIcon */])), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](8, 638976, null, 0, __WEBPACK_IMPORTED_MODULE_12__angular_material_icon__["b" /* MatIcon */], [__WEBPACK_IMPORTED_MODULE_1__angular_core__["ElementRef"], __WEBPACK_IMPORTED_MODULE_12__angular_material_icon__["d" /* MatIconRegistry */], [8, null]], null, null), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, 0, ["supervisor_account"])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n                        "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵeld"](11, 0, null, null, 1, "span", [["class", "section-title"]], null, null, null, null, null)), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["Manager"])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n                    "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n                    "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵeld"](15, 0, null, null, 10, "div", [["class", "section-content"]], null, null, null, null, null)), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n                        "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵeld"](17, 0, null, null, 7, "mat-chip-list", [["class", "member-chips mat-chip-list"]], [[1, "tabindex", 0], [1, "aria-describedby", 0], [1, "aria-required", 0], [1, "aria-disabled", 0], [1, "aria-invalid", 0], [1, "aria-multiselectable", 0], [1, "role", 0], [2, "mat-chip-list-disabled", null], [2, "mat-chip-list-invalid", null], [2, "mat-chip-list-required", null], [1, "aria-orientation", 0]], [[null, "focus"], [null, "blur"], [null, "keydown"]], function (_v, en, $event) { var ad = true; if (("focus" === en)) {
        var pd_0 = (__WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 19).focus() !== false);
        ad = (pd_0 && ad);
    } if (("blur" === en)) {
        var pd_1 = (__WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 19)._blur() !== false);
        ad = (pd_1 && ad);
    } if (("keydown" === en)) {
        var pd_2 = (__WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 19)._keydown($event) !== false);
        ad = (pd_2 && ad);
    } return ad; }, __WEBPACK_IMPORTED_MODULE_19__node_modules_angular_material_chips_typings_index_ngfactory__["b" /* View_MatChipList_0 */], __WEBPACK_IMPORTED_MODULE_19__node_modules_angular_material_chips_typings_index_ngfactory__["a" /* RenderType_MatChipList */])), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵprd"](6144, null, __WEBPACK_IMPORTED_MODULE_20__angular_material_form_field__["c" /* MatFormFieldControl */], null, [__WEBPACK_IMPORTED_MODULE_13__angular_material_chips__["c" /* MatChipList */]]), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](19, 1556480, null, 1, __WEBPACK_IMPORTED_MODULE_13__angular_material_chips__["c" /* MatChipList */], [__WEBPACK_IMPORTED_MODULE_1__angular_core__["ElementRef"], __WEBPACK_IMPORTED_MODULE_1__angular_core__["ChangeDetectorRef"], [2, __WEBPACK_IMPORTED_MODULE_17__angular_cdk_bidi__["c" /* Directionality */]], [2, __WEBPACK_IMPORTED_MODULE_21__angular_forms__["x" /* NgForm */]], [2, __WEBPACK_IMPORTED_MODULE_21__angular_forms__["n" /* FormGroupDirective */]], __WEBPACK_IMPORTED_MODULE_22__angular_material_core__["d" /* ErrorStateMatcher */], [8, null]], null, null), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵqud"](603979776, 25, { chips: 1 }), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, 0, ["\n                            "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵand"](16777216, null, 0, 1, null, View_MerchantFeaturedCardDialogComponent_5)), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](23, 802816, null, 0, __WEBPACK_IMPORTED_MODULE_6__angular_common__["NgForOf"], [__WEBPACK_IMPORTED_MODULE_1__angular_core__["ViewContainerRef"], __WEBPACK_IMPORTED_MODULE_1__angular_core__["TemplateRef"], __WEBPACK_IMPORTED_MODULE_1__angular_core__["IterableDiffers"]], { ngForOf: [0, "ngForOf"] }, null), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, 0, ["\n                        "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n                    "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n                "]))], function (_ck, _v) { var _co = _v.component; var currVal_0 = ""; _ck(_v, 1, 0, currVal_0); var currVal_1 = "row"; _ck(_v, 4, 0, currVal_1); var currVal_2 = "start center"; _ck(_v, 5, 0, currVal_2); _ck(_v, 8, 0); _ck(_v, 19, 0); var currVal_14 = _co.card.idMembers; _ck(_v, 23, 0, currVal_14); }, function (_ck, _v) { var currVal_3 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 19)._tabIndex; var currVal_4 = (__WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 19)._ariaDescribedby || null); var currVal_5 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 19).required.toString(); var currVal_6 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 19).disabled.toString(); var currVal_7 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 19).errorState; var currVal_8 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 19).multiple; var currVal_9 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 19).role; var currVal_10 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 19).disabled; var currVal_11 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 19).errorState; var currVal_12 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 19).required; var currVal_13 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 19).ariaOrientation; _ck(_v, 17, 1, [currVal_3, currVal_4, currVal_5, currVal_6, currVal_7, currVal_8, currVal_9, currVal_10, currVal_11, currVal_12, currVal_13]); }); }
function View_MerchantFeaturedCardDialogComponent_3(_l) { return __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵvid"](0, [(_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵeld"](0, 0, null, null, 6, "div", [["class", "section"], ["fxLayout", "column"], ["fxLayout.gt-xs", "row"]], null, null, null, null, null)), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](1, 737280, null, 0, __WEBPACK_IMPORTED_MODULE_7__angular_flex_layout__["j" /* LayoutDirective */], [__WEBPACK_IMPORTED_MODULE_7__angular_flex_layout__["m" /* MediaMonitor */], __WEBPACK_IMPORTED_MODULE_1__angular_core__["ElementRef"], __WEBPACK_IMPORTED_MODULE_7__angular_flex_layout__["t" /* StyleUtils */]], { layout: [0, "layout"], layoutGtXs: [1, "layoutGtXs"] }, null), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n\n                "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n\n                "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵand"](16777216, null, null, 1, null, View_MerchantFeaturedCardDialogComponent_4)), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](5, 16384, null, 0, __WEBPACK_IMPORTED_MODULE_6__angular_common__["NgIf"], [__WEBPACK_IMPORTED_MODULE_1__angular_core__["ViewContainerRef"], __WEBPACK_IMPORTED_MODULE_1__angular_core__["TemplateRef"]], { ngIf: [0, "ngIf"] }, null), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n            "]))], function (_ck, _v) { var _co = _v.component; var currVal_0 = "column"; var currVal_1 = "row"; _ck(_v, 1, 0, currVal_0, currVal_1); var currVal_2 = _co.card.idMembers[0]; _ck(_v, 5, 0, currVal_2); }, null); }
function View_MerchantFeaturedCardDialogComponent_2(_l) { return __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵvid"](0, [(_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵeld"](0, 0, null, null, 129, "div", [["class", "p-24 m-0 mat-dialog-content"], ["fusePerfectScrollbar", ""], ["mat-dialog-content", ""]], null, [["document", "click"]], function (_v, en, $event) { var ad = true; if (("document:click" === en)) {
        var pd_0 = (__WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 2).documentClick($event) !== false);
        ad = (pd_0 && ad);
    } return ad; }, null, null)), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](1, 16384, null, 0, __WEBPACK_IMPORTED_MODULE_3__angular_material_dialog__["i" /* MatDialogContent */], [], null, null), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](2, 4407296, null, 0, __WEBPACK_IMPORTED_MODULE_23__fuse_directives_fuse_perfect_scrollbar_fuse_perfect_scrollbar_directive__["a" /* FusePerfectScrollbarDirective */], [__WEBPACK_IMPORTED_MODULE_1__angular_core__["ElementRef"], __WEBPACK_IMPORTED_MODULE_24__fuse_services_config_service__["b" /* FuseConfigService */], __WEBPACK_IMPORTED_MODULE_5__angular_cdk_platform__["a" /* Platform */]], null, null), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n\n        "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵeld"](4, 0, null, null, 9, "div", [["fxLayout", "column"], ["fxLayout.gt-sm", "row"], ["fxLayoutAlign", "center center"], ["fxLayoutAlign.gt-sm", "space-between center"]], null, null, null, null, null)), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](5, 737280, null, 0, __WEBPACK_IMPORTED_MODULE_7__angular_flex_layout__["j" /* LayoutDirective */], [__WEBPACK_IMPORTED_MODULE_7__angular_flex_layout__["m" /* MediaMonitor */], __WEBPACK_IMPORTED_MODULE_1__angular_core__["ElementRef"], __WEBPACK_IMPORTED_MODULE_7__angular_flex_layout__["t" /* StyleUtils */]], { layout: [0, "layout"], layoutGtSm: [1, "layoutGtSm"] }, null), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](6, 737280, null, 0, __WEBPACK_IMPORTED_MODULE_7__angular_flex_layout__["i" /* LayoutAlignDirective */], [__WEBPACK_IMPORTED_MODULE_7__angular_flex_layout__["m" /* MediaMonitor */], __WEBPACK_IMPORTED_MODULE_1__angular_core__["ElementRef"], [2, __WEBPACK_IMPORTED_MODULE_7__angular_flex_layout__["j" /* LayoutDirective */]], __WEBPACK_IMPORTED_MODULE_7__angular_flex_layout__["t" /* StyleUtils */]], { align: [0, "align"], alignGtSm: [1, "alignGtSm"] }, null), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n\n            "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n            "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n            "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n\n            "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n            "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n            "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n\n        "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n\n        "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n        "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵeld"](16, 0, null, null, 30, "div", [["fxLayout", "row"], ["fxLayoutAlign", "start center"]], null, null, null, null, null)), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](17, 737280, null, 0, __WEBPACK_IMPORTED_MODULE_7__angular_flex_layout__["j" /* LayoutDirective */], [__WEBPACK_IMPORTED_MODULE_7__angular_flex_layout__["m" /* MediaMonitor */], __WEBPACK_IMPORTED_MODULE_1__angular_core__["ElementRef"], __WEBPACK_IMPORTED_MODULE_7__angular_flex_layout__["t" /* StyleUtils */]], { layout: [0, "layout"] }, null), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](18, 737280, null, 0, __WEBPACK_IMPORTED_MODULE_7__angular_flex_layout__["i" /* LayoutAlignDirective */], [__WEBPACK_IMPORTED_MODULE_7__angular_flex_layout__["m" /* MediaMonitor */], __WEBPACK_IMPORTED_MODULE_1__angular_core__["ElementRef"], [2, __WEBPACK_IMPORTED_MODULE_7__angular_flex_layout__["j" /* LayoutDirective */]], __WEBPACK_IMPORTED_MODULE_7__angular_flex_layout__["t" /* StyleUtils */]], { align: [0, "align"] }, null), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n            "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n            "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵeld"](21, 0, null, null, 24, "div", [["class", "card-name"]], null, null, null, null, null)), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n                "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵeld"](23, 0, null, null, 21, "mat-form-field", [["class", "mat-input-container mat-form-field"], ["fxFlex", ""]], [[2, "mat-input-invalid", null], [2, "mat-form-field-invalid", null], [2, "mat-form-field-can-float", null], [2, "mat-form-field-should-float", null], [2, "mat-form-field-hide-placeholder", null], [2, "mat-form-field-disabled", null], [2, "mat-focused", null], [2, "ng-untouched", null], [2, "ng-touched", null], [2, "ng-pristine", null], [2, "ng-dirty", null], [2, "ng-valid", null], [2, "ng-invalid", null], [2, "ng-pending", null]], null, null, __WEBPACK_IMPORTED_MODULE_25__node_modules_angular_material_form_field_typings_index_ngfactory__["b" /* View_MatFormField_0 */], __WEBPACK_IMPORTED_MODULE_25__node_modules_angular_material_form_field_typings_index_ngfactory__["a" /* RenderType_MatFormField */])), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](24, 7389184, null, 7, __WEBPACK_IMPORTED_MODULE_20__angular_material_form_field__["b" /* MatFormField */], [__WEBPACK_IMPORTED_MODULE_1__angular_core__["ElementRef"], __WEBPACK_IMPORTED_MODULE_1__angular_core__["ChangeDetectorRef"], [2, __WEBPACK_IMPORTED_MODULE_22__angular_material_core__["k" /* MAT_LABEL_GLOBAL_OPTIONS */]]], null, null), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵqud"](335544320, 4, { _control: 0 }), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵqud"](335544320, 5, { _placeholderChild: 0 }), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵqud"](335544320, 6, { _labelChild: 0 }), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵqud"](603979776, 7, { _errorChildren: 1 }), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵqud"](603979776, 8, { _hintChildren: 1 }), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵqud"](603979776, 9, { _prefixChildren: 1 }), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵqud"](603979776, 10, { _suffixChildren: 1 }), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](32, 737280, null, 0, __WEBPACK_IMPORTED_MODULE_7__angular_flex_layout__["e" /* FlexDirective */], [__WEBPACK_IMPORTED_MODULE_7__angular_flex_layout__["m" /* MediaMonitor */], __WEBPACK_IMPORTED_MODULE_1__angular_core__["ElementRef"], [3, __WEBPACK_IMPORTED_MODULE_7__angular_flex_layout__["j" /* LayoutDirective */]], __WEBPACK_IMPORTED_MODULE_7__angular_flex_layout__["t" /* StyleUtils */]], { flex: [0, "flex"] }, null), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, 1, ["\n                    "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵeld"](34, 0, null, 1, 9, "input", [["class", "mat-input-element mat-form-field-autofill-control"], ["disabled", ""], ["matInput", ""], ["placeholder", "Name"], ["required", ""]], [[1, "required", 0], [2, "mat-input-server", null], [1, "id", 0], [8, "placeholder", 0], [8, "disabled", 0], [8, "required", 0], [8, "readOnly", 0], [1, "aria-describedby", 0], [1, "aria-invalid", 0], [1, "aria-required", 0], [2, "ng-untouched", null], [2, "ng-touched", null], [2, "ng-pristine", null], [2, "ng-dirty", null], [2, "ng-valid", null], [2, "ng-invalid", null], [2, "ng-pending", null]], [[null, "ngModelChange"], [null, "change"], [null, "input"], [null, "blur"], [null, "compositionstart"], [null, "compositionend"], [null, "focus"]], function (_v, en, $event) { var ad = true; var _co = _v.component; if (("input" === en)) {
        var pd_0 = (__WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 37)._handleInput($event.target.value) !== false);
        ad = (pd_0 && ad);
    } if (("blur" === en)) {
        var pd_1 = (__WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 37).onTouched() !== false);
        ad = (pd_1 && ad);
    } if (("compositionstart" === en)) {
        var pd_2 = (__WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 37)._compositionStart() !== false);
        ad = (pd_2 && ad);
    } if (("compositionend" === en)) {
        var pd_3 = (__WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 37)._compositionEnd($event.target.value) !== false);
        ad = (pd_3 && ad);
    } if (("blur" === en)) {
        var pd_4 = (__WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 41)._focusChanged(false) !== false);
        ad = (pd_4 && ad);
    } if (("focus" === en)) {
        var pd_5 = (__WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 41)._focusChanged(true) !== false);
        ad = (pd_5 && ad);
    } if (("input" === en)) {
        var pd_6 = (__WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 41)._onInput() !== false);
        ad = (pd_6 && ad);
    } if (("ngModelChange" === en)) {
        var pd_7 = ((_co.card.name = $event) !== false);
        ad = (pd_7 && ad);
    } if (("change" === en)) {
        var pd_8 = (_co.updateCard() !== false);
        ad = (pd_8 && ad);
    } return ad; }, null, null)), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](35, 16384, null, 0, __WEBPACK_IMPORTED_MODULE_21__angular_forms__["B" /* RequiredValidator */], [], { required: [0, "required"] }, null), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵprd"](1024, null, __WEBPACK_IMPORTED_MODULE_21__angular_forms__["s" /* NG_VALIDATORS */], function (p0_0) { return [p0_0]; }, [__WEBPACK_IMPORTED_MODULE_21__angular_forms__["B" /* RequiredValidator */]]), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](37, 16384, null, 0, __WEBPACK_IMPORTED_MODULE_21__angular_forms__["e" /* DefaultValueAccessor */], [__WEBPACK_IMPORTED_MODULE_1__angular_core__["Renderer2"], __WEBPACK_IMPORTED_MODULE_1__angular_core__["ElementRef"], [2, __WEBPACK_IMPORTED_MODULE_21__angular_forms__["a" /* COMPOSITION_BUFFER_MODE */]]], null, null), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵprd"](1024, null, __WEBPACK_IMPORTED_MODULE_21__angular_forms__["t" /* NG_VALUE_ACCESSOR */], function (p0_0) { return [p0_0]; }, [__WEBPACK_IMPORTED_MODULE_21__angular_forms__["e" /* DefaultValueAccessor */]]), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](39, 671744, null, 0, __WEBPACK_IMPORTED_MODULE_21__angular_forms__["y" /* NgModel */], [[8, null], [2, __WEBPACK_IMPORTED_MODULE_21__angular_forms__["s" /* NG_VALIDATORS */]], [8, null], [2, __WEBPACK_IMPORTED_MODULE_21__angular_forms__["t" /* NG_VALUE_ACCESSOR */]]], { isDisabled: [0, "isDisabled"], model: [1, "model"] }, { update: "ngModelChange" }), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵprd"](2048, null, __WEBPACK_IMPORTED_MODULE_21__angular_forms__["u" /* NgControl */], null, [__WEBPACK_IMPORTED_MODULE_21__angular_forms__["y" /* NgModel */]]), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](41, 933888, null, 0, __WEBPACK_IMPORTED_MODULE_26__angular_material_input__["b" /* MatInput */], [__WEBPACK_IMPORTED_MODULE_1__angular_core__["ElementRef"], __WEBPACK_IMPORTED_MODULE_5__angular_cdk_platform__["a" /* Platform */], [2, __WEBPACK_IMPORTED_MODULE_21__angular_forms__["u" /* NgControl */]], [2, __WEBPACK_IMPORTED_MODULE_21__angular_forms__["x" /* NgForm */]], [2, __WEBPACK_IMPORTED_MODULE_21__angular_forms__["n" /* FormGroupDirective */]], __WEBPACK_IMPORTED_MODULE_22__angular_material_core__["d" /* ErrorStateMatcher */], [8, null]], { disabled: [0, "disabled"], placeholder: [1, "placeholder"], required: [2, "required"] }, null), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](42, 16384, null, 0, __WEBPACK_IMPORTED_MODULE_21__angular_forms__["v" /* NgControlStatus */], [__WEBPACK_IMPORTED_MODULE_21__angular_forms__["u" /* NgControl */]], null, null), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵprd"](2048, [[4, 4]], __WEBPACK_IMPORTED_MODULE_20__angular_material_form_field__["c" /* MatFormFieldControl */], null, [__WEBPACK_IMPORTED_MODULE_26__angular_material_input__["b" /* MatInput */]]), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, 1, ["\n                "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n            "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n        "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n        "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n\n        "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n        "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵeld"](50, 0, null, null, 29, "div", [["fxLayout", "row"], ["fxLayoutAlign", "start center"]], null, null, null, null, null)), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](51, 737280, null, 0, __WEBPACK_IMPORTED_MODULE_7__angular_flex_layout__["j" /* LayoutDirective */], [__WEBPACK_IMPORTED_MODULE_7__angular_flex_layout__["m" /* MediaMonitor */], __WEBPACK_IMPORTED_MODULE_1__angular_core__["ElementRef"], __WEBPACK_IMPORTED_MODULE_7__angular_flex_layout__["t" /* StyleUtils */]], { layout: [0, "layout"] }, null), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](52, 737280, null, 0, __WEBPACK_IMPORTED_MODULE_7__angular_flex_layout__["i" /* LayoutAlignDirective */], [__WEBPACK_IMPORTED_MODULE_7__angular_flex_layout__["m" /* MediaMonitor */], __WEBPACK_IMPORTED_MODULE_1__angular_core__["ElementRef"], [2, __WEBPACK_IMPORTED_MODULE_7__angular_flex_layout__["j" /* LayoutDirective */]], __WEBPACK_IMPORTED_MODULE_7__angular_flex_layout__["t" /* StyleUtils */]], { align: [0, "align"] }, null), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n            "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵeld"](54, 0, null, null, 24, "div", [["class", "card-name"]], null, null, null, null, null)), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n                "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵeld"](56, 0, null, null, 21, "mat-form-field", [["class", "mat-input-container mat-form-field"], ["fxFlex", ""]], [[2, "mat-input-invalid", null], [2, "mat-form-field-invalid", null], [2, "mat-form-field-can-float", null], [2, "mat-form-field-should-float", null], [2, "mat-form-field-hide-placeholder", null], [2, "mat-form-field-disabled", null], [2, "mat-focused", null], [2, "ng-untouched", null], [2, "ng-touched", null], [2, "ng-pristine", null], [2, "ng-dirty", null], [2, "ng-valid", null], [2, "ng-invalid", null], [2, "ng-pending", null]], null, null, __WEBPACK_IMPORTED_MODULE_25__node_modules_angular_material_form_field_typings_index_ngfactory__["b" /* View_MatFormField_0 */], __WEBPACK_IMPORTED_MODULE_25__node_modules_angular_material_form_field_typings_index_ngfactory__["a" /* RenderType_MatFormField */])), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](57, 7389184, null, 7, __WEBPACK_IMPORTED_MODULE_20__angular_material_form_field__["b" /* MatFormField */], [__WEBPACK_IMPORTED_MODULE_1__angular_core__["ElementRef"], __WEBPACK_IMPORTED_MODULE_1__angular_core__["ChangeDetectorRef"], [2, __WEBPACK_IMPORTED_MODULE_22__angular_material_core__["k" /* MAT_LABEL_GLOBAL_OPTIONS */]]], null, null), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵqud"](335544320, 11, { _control: 0 }), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵqud"](335544320, 12, { _placeholderChild: 0 }), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵqud"](335544320, 13, { _labelChild: 0 }), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵqud"](603979776, 14, { _errorChildren: 1 }), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵqud"](603979776, 15, { _hintChildren: 1 }), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵqud"](603979776, 16, { _prefixChildren: 1 }), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵqud"](603979776, 17, { _suffixChildren: 1 }), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](65, 737280, null, 0, __WEBPACK_IMPORTED_MODULE_7__angular_flex_layout__["e" /* FlexDirective */], [__WEBPACK_IMPORTED_MODULE_7__angular_flex_layout__["m" /* MediaMonitor */], __WEBPACK_IMPORTED_MODULE_1__angular_core__["ElementRef"], [3, __WEBPACK_IMPORTED_MODULE_7__angular_flex_layout__["j" /* LayoutDirective */]], __WEBPACK_IMPORTED_MODULE_7__angular_flex_layout__["t" /* StyleUtils */]], { flex: [0, "flex"] }, null), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, 1, ["\n                    "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵeld"](67, 0, null, 1, 9, "input", [["class", "mat-input-element mat-form-field-autofill-control"], ["disabled", ""], ["matInput", ""], ["placeholder", "Affiliate"], ["required", ""]], [[1, "required", 0], [2, "mat-input-server", null], [1, "id", 0], [8, "placeholder", 0], [8, "disabled", 0], [8, "required", 0], [8, "readOnly", 0], [1, "aria-describedby", 0], [1, "aria-invalid", 0], [1, "aria-required", 0], [2, "ng-untouched", null], [2, "ng-touched", null], [2, "ng-pristine", null], [2, "ng-dirty", null], [2, "ng-valid", null], [2, "ng-invalid", null], [2, "ng-pending", null]], [[null, "ngModelChange"], [null, "change"], [null, "input"], [null, "blur"], [null, "compositionstart"], [null, "compositionend"], [null, "focus"]], function (_v, en, $event) { var ad = true; var _co = _v.component; if (("input" === en)) {
        var pd_0 = (__WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 70)._handleInput($event.target.value) !== false);
        ad = (pd_0 && ad);
    } if (("blur" === en)) {
        var pd_1 = (__WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 70).onTouched() !== false);
        ad = (pd_1 && ad);
    } if (("compositionstart" === en)) {
        var pd_2 = (__WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 70)._compositionStart() !== false);
        ad = (pd_2 && ad);
    } if (("compositionend" === en)) {
        var pd_3 = (__WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 70)._compositionEnd($event.target.value) !== false);
        ad = (pd_3 && ad);
    } if (("blur" === en)) {
        var pd_4 = (__WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 74)._focusChanged(false) !== false);
        ad = (pd_4 && ad);
    } if (("focus" === en)) {
        var pd_5 = (__WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 74)._focusChanged(true) !== false);
        ad = (pd_5 && ad);
    } if (("input" === en)) {
        var pd_6 = (__WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 74)._onInput() !== false);
        ad = (pd_6 && ad);
    } if (("ngModelChange" === en)) {
        var pd_7 = ((_co.card.affiliate = $event) !== false);
        ad = (pd_7 && ad);
    } if (("change" === en)) {
        var pd_8 = (_co.updateCard() !== false);
        ad = (pd_8 && ad);
    } return ad; }, null, null)), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](68, 16384, null, 0, __WEBPACK_IMPORTED_MODULE_21__angular_forms__["B" /* RequiredValidator */], [], { required: [0, "required"] }, null), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵprd"](1024, null, __WEBPACK_IMPORTED_MODULE_21__angular_forms__["s" /* NG_VALIDATORS */], function (p0_0) { return [p0_0]; }, [__WEBPACK_IMPORTED_MODULE_21__angular_forms__["B" /* RequiredValidator */]]), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](70, 16384, null, 0, __WEBPACK_IMPORTED_MODULE_21__angular_forms__["e" /* DefaultValueAccessor */], [__WEBPACK_IMPORTED_MODULE_1__angular_core__["Renderer2"], __WEBPACK_IMPORTED_MODULE_1__angular_core__["ElementRef"], [2, __WEBPACK_IMPORTED_MODULE_21__angular_forms__["a" /* COMPOSITION_BUFFER_MODE */]]], null, null), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵprd"](1024, null, __WEBPACK_IMPORTED_MODULE_21__angular_forms__["t" /* NG_VALUE_ACCESSOR */], function (p0_0) { return [p0_0]; }, [__WEBPACK_IMPORTED_MODULE_21__angular_forms__["e" /* DefaultValueAccessor */]]), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](72, 671744, null, 0, __WEBPACK_IMPORTED_MODULE_21__angular_forms__["y" /* NgModel */], [[8, null], [2, __WEBPACK_IMPORTED_MODULE_21__angular_forms__["s" /* NG_VALIDATORS */]], [8, null], [2, __WEBPACK_IMPORTED_MODULE_21__angular_forms__["t" /* NG_VALUE_ACCESSOR */]]], { isDisabled: [0, "isDisabled"], model: [1, "model"] }, { update: "ngModelChange" }), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵprd"](2048, null, __WEBPACK_IMPORTED_MODULE_21__angular_forms__["u" /* NgControl */], null, [__WEBPACK_IMPORTED_MODULE_21__angular_forms__["y" /* NgModel */]]), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](74, 933888, null, 0, __WEBPACK_IMPORTED_MODULE_26__angular_material_input__["b" /* MatInput */], [__WEBPACK_IMPORTED_MODULE_1__angular_core__["ElementRef"], __WEBPACK_IMPORTED_MODULE_5__angular_cdk_platform__["a" /* Platform */], [2, __WEBPACK_IMPORTED_MODULE_21__angular_forms__["u" /* NgControl */]], [2, __WEBPACK_IMPORTED_MODULE_21__angular_forms__["x" /* NgForm */]], [2, __WEBPACK_IMPORTED_MODULE_21__angular_forms__["n" /* FormGroupDirective */]], __WEBPACK_IMPORTED_MODULE_22__angular_material_core__["d" /* ErrorStateMatcher */], [8, null]], { disabled: [0, "disabled"], placeholder: [1, "placeholder"], required: [2, "required"] }, null), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](75, 16384, null, 0, __WEBPACK_IMPORTED_MODULE_21__angular_forms__["v" /* NgControlStatus */], [__WEBPACK_IMPORTED_MODULE_21__angular_forms__["u" /* NgControl */]], null, null), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵprd"](2048, [[11, 4]], __WEBPACK_IMPORTED_MODULE_20__angular_material_form_field__["c" /* MatFormFieldControl */], null, [__WEBPACK_IMPORTED_MODULE_26__angular_material_input__["b" /* MatInput */]]), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, 1, ["\n                "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n            "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n        "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n        "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n\n        "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n        "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵeld"](83, 0, null, null, 22, "div", [["class", "description"]], null, null, null, null, null)), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n            "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵeld"](85, 0, null, null, 19, "mat-form-field", [["class", "mat-input-container mat-form-field"], ["fxFlex", ""]], [[2, "mat-input-invalid", null], [2, "mat-form-field-invalid", null], [2, "mat-form-field-can-float", null], [2, "mat-form-field-should-float", null], [2, "mat-form-field-hide-placeholder", null], [2, "mat-form-field-disabled", null], [2, "mat-focused", null], [2, "ng-untouched", null], [2, "ng-touched", null], [2, "ng-pristine", null], [2, "ng-dirty", null], [2, "ng-valid", null], [2, "ng-invalid", null], [2, "ng-pending", null]], null, null, __WEBPACK_IMPORTED_MODULE_25__node_modules_angular_material_form_field_typings_index_ngfactory__["b" /* View_MatFormField_0 */], __WEBPACK_IMPORTED_MODULE_25__node_modules_angular_material_form_field_typings_index_ngfactory__["a" /* RenderType_MatFormField */])), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](86, 7389184, null, 7, __WEBPACK_IMPORTED_MODULE_20__angular_material_form_field__["b" /* MatFormField */], [__WEBPACK_IMPORTED_MODULE_1__angular_core__["ElementRef"], __WEBPACK_IMPORTED_MODULE_1__angular_core__["ChangeDetectorRef"], [2, __WEBPACK_IMPORTED_MODULE_22__angular_material_core__["k" /* MAT_LABEL_GLOBAL_OPTIONS */]]], null, null), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵqud"](335544320, 18, { _control: 0 }), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵqud"](335544320, 19, { _placeholderChild: 0 }), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵqud"](335544320, 20, { _labelChild: 0 }), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵqud"](603979776, 21, { _errorChildren: 1 }), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵqud"](603979776, 22, { _hintChildren: 1 }), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵqud"](603979776, 23, { _prefixChildren: 1 }), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵqud"](603979776, 24, { _suffixChildren: 1 }), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](94, 737280, null, 0, __WEBPACK_IMPORTED_MODULE_7__angular_flex_layout__["e" /* FlexDirective */], [__WEBPACK_IMPORTED_MODULE_7__angular_flex_layout__["m" /* MediaMonitor */], __WEBPACK_IMPORTED_MODULE_1__angular_core__["ElementRef"], [3, __WEBPACK_IMPORTED_MODULE_7__angular_flex_layout__["j" /* LayoutDirective */]], __WEBPACK_IMPORTED_MODULE_7__angular_flex_layout__["t" /* StyleUtils */]], { flex: [0, "flex"] }, null), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, 1, ["\n                "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵeld"](96, 0, null, 1, 7, "textarea", [["class", "mat-input-element mat-form-field-autofill-control"], ["columns", "1"], ["disabled", ""], ["mat-maxlength", "150"], ["matInput", ""], ["max-rows", "4"], ["placeholder", "Description"], ["style", "resize: none"]], [[2, "mat-input-server", null], [1, "id", 0], [8, "placeholder", 0], [8, "disabled", 0], [8, "required", 0], [8, "readOnly", 0], [1, "aria-describedby", 0], [1, "aria-invalid", 0], [1, "aria-required", 0], [2, "ng-untouched", null], [2, "ng-touched", null], [2, "ng-pristine", null], [2, "ng-dirty", null], [2, "ng-valid", null], [2, "ng-invalid", null], [2, "ng-pending", null]], [[null, "ngModelChange"], [null, "change"], [null, "input"], [null, "blur"], [null, "compositionstart"], [null, "compositionend"], [null, "focus"]], function (_v, en, $event) { var ad = true; var _co = _v.component; if (("input" === en)) {
        var pd_0 = (__WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 97)._handleInput($event.target.value) !== false);
        ad = (pd_0 && ad);
    } if (("blur" === en)) {
        var pd_1 = (__WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 97).onTouched() !== false);
        ad = (pd_1 && ad);
    } if (("compositionstart" === en)) {
        var pd_2 = (__WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 97)._compositionStart() !== false);
        ad = (pd_2 && ad);
    } if (("compositionend" === en)) {
        var pd_3 = (__WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 97)._compositionEnd($event.target.value) !== false);
        ad = (pd_3 && ad);
    } if (("blur" === en)) {
        var pd_4 = (__WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 101)._focusChanged(false) !== false);
        ad = (pd_4 && ad);
    } if (("focus" === en)) {
        var pd_5 = (__WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 101)._focusChanged(true) !== false);
        ad = (pd_5 && ad);
    } if (("input" === en)) {
        var pd_6 = (__WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 101)._onInput() !== false);
        ad = (pd_6 && ad);
    } if (("ngModelChange" === en)) {
        var pd_7 = ((_co.card.description = $event) !== false);
        ad = (pd_7 && ad);
    } if (("change" === en)) {
        var pd_8 = (_co.updateCard() !== false);
        ad = (pd_8 && ad);
    } return ad; }, null, null)), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](97, 16384, null, 0, __WEBPACK_IMPORTED_MODULE_21__angular_forms__["e" /* DefaultValueAccessor */], [__WEBPACK_IMPORTED_MODULE_1__angular_core__["Renderer2"], __WEBPACK_IMPORTED_MODULE_1__angular_core__["ElementRef"], [2, __WEBPACK_IMPORTED_MODULE_21__angular_forms__["a" /* COMPOSITION_BUFFER_MODE */]]], null, null), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵprd"](1024, null, __WEBPACK_IMPORTED_MODULE_21__angular_forms__["t" /* NG_VALUE_ACCESSOR */], function (p0_0) { return [p0_0]; }, [__WEBPACK_IMPORTED_MODULE_21__angular_forms__["e" /* DefaultValueAccessor */]]), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](99, 671744, null, 0, __WEBPACK_IMPORTED_MODULE_21__angular_forms__["y" /* NgModel */], [[8, null], [8, null], [8, null], [2, __WEBPACK_IMPORTED_MODULE_21__angular_forms__["t" /* NG_VALUE_ACCESSOR */]]], { isDisabled: [0, "isDisabled"], model: [1, "model"] }, { update: "ngModelChange" }), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵprd"](2048, null, __WEBPACK_IMPORTED_MODULE_21__angular_forms__["u" /* NgControl */], null, [__WEBPACK_IMPORTED_MODULE_21__angular_forms__["y" /* NgModel */]]), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](101, 933888, null, 0, __WEBPACK_IMPORTED_MODULE_26__angular_material_input__["b" /* MatInput */], [__WEBPACK_IMPORTED_MODULE_1__angular_core__["ElementRef"], __WEBPACK_IMPORTED_MODULE_5__angular_cdk_platform__["a" /* Platform */], [2, __WEBPACK_IMPORTED_MODULE_21__angular_forms__["u" /* NgControl */]], [2, __WEBPACK_IMPORTED_MODULE_21__angular_forms__["x" /* NgForm */]], [2, __WEBPACK_IMPORTED_MODULE_21__angular_forms__["n" /* FormGroupDirective */]], __WEBPACK_IMPORTED_MODULE_22__angular_material_core__["d" /* ErrorStateMatcher */], [8, null]], { disabled: [0, "disabled"], placeholder: [1, "placeholder"] }, null), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](102, 16384, null, 0, __WEBPACK_IMPORTED_MODULE_21__angular_forms__["v" /* NgControlStatus */], [__WEBPACK_IMPORTED_MODULE_21__angular_forms__["u" /* NgControl */]], null, null), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵprd"](2048, [[18, 4]], __WEBPACK_IMPORTED_MODULE_20__angular_material_form_field__["c" /* MatFormFieldControl */], null, [__WEBPACK_IMPORTED_MODULE_26__angular_material_input__["b" /* MatInput */]]), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, 1, ["\n            "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n        "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n        "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n\n        "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n        "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵeld"](109, 0, null, null, 18, "div", [["class", "sections"]], null, null, null, null, null)), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n\n            "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n            "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵand"](16777216, null, null, 1, null, View_MerchantFeaturedCardDialogComponent_3)), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](113, 16384, null, 0, __WEBPACK_IMPORTED_MODULE_6__angular_common__["NgIf"], [__WEBPACK_IMPORTED_MODULE_1__angular_core__["ViewContainerRef"], __WEBPACK_IMPORTED_MODULE_1__angular_core__["TemplateRef"]], { ngIf: [0, "ngIf"] }, null), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n            "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n\n            "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n            "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n            "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n\n            "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n            "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n            "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n\n            "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n            "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n            "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n\n            "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n            "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n            "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n\n        "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n        "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n    "]))], function (_ck, _v) { var _co = _v.component; _ck(_v, 2, 0); var currVal_0 = "column"; var currVal_1 = "row"; _ck(_v, 5, 0, currVal_0, currVal_1); var currVal_2 = "center center"; var currVal_3 = "space-between center"; _ck(_v, 6, 0, currVal_2, currVal_3); var currVal_4 = "row"; _ck(_v, 17, 0, currVal_4); var currVal_5 = "start center"; _ck(_v, 18, 0, currVal_5); var currVal_20 = ""; _ck(_v, 32, 0, currVal_20); var currVal_38 = ""; _ck(_v, 35, 0, currVal_38); var currVal_39 = ""; var currVal_40 = _co.card.name; _ck(_v, 39, 0, currVal_39, currVal_40); var currVal_41 = ""; var currVal_42 = "Name"; var currVal_43 = ""; _ck(_v, 41, 0, currVal_41, currVal_42, currVal_43); var currVal_44 = "row"; _ck(_v, 51, 0, currVal_44); var currVal_45 = "start center"; _ck(_v, 52, 0, currVal_45); var currVal_60 = ""; _ck(_v, 65, 0, currVal_60); var currVal_78 = ""; _ck(_v, 68, 0, currVal_78); var currVal_79 = ""; var currVal_80 = _co.card.affiliate; _ck(_v, 72, 0, currVal_79, currVal_80); var currVal_81 = ""; var currVal_82 = "Affiliate"; var currVal_83 = ""; _ck(_v, 74, 0, currVal_81, currVal_82, currVal_83); var currVal_98 = ""; _ck(_v, 94, 0, currVal_98); var currVal_115 = ""; var currVal_116 = _co.card.description; _ck(_v, 99, 0, currVal_115, currVal_116); var currVal_117 = ""; var currVal_118 = "Description"; _ck(_v, 101, 0, currVal_117, currVal_118); var currVal_119 = _co.card.idMembers[0]; _ck(_v, 113, 0, currVal_119); }, function (_ck, _v) { var currVal_6 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 24)._control.errorState; var currVal_7 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 24)._control.errorState; var currVal_8 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 24)._canLabelFloat; var currVal_9 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 24)._shouldLabelFloat(); var currVal_10 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 24)._hideControlPlaceholder(); var currVal_11 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 24)._control.disabled; var currVal_12 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 24)._control.focused; var currVal_13 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 24)._shouldForward("untouched"); var currVal_14 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 24)._shouldForward("touched"); var currVal_15 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 24)._shouldForward("pristine"); var currVal_16 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 24)._shouldForward("dirty"); var currVal_17 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 24)._shouldForward("valid"); var currVal_18 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 24)._shouldForward("invalid"); var currVal_19 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 24)._shouldForward("pending"); _ck(_v, 23, 1, [currVal_6, currVal_7, currVal_8, currVal_9, currVal_10, currVal_11, currVal_12, currVal_13, currVal_14, currVal_15, currVal_16, currVal_17, currVal_18, currVal_19]); var currVal_21 = (__WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 35).required ? "" : null); var currVal_22 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 41)._isServer; var currVal_23 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 41).id; var currVal_24 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 41).placeholder; var currVal_25 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 41).disabled; var currVal_26 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 41).required; var currVal_27 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 41).readonly; var currVal_28 = (__WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 41)._ariaDescribedby || null); var currVal_29 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 41).errorState; var currVal_30 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 41).required.toString(); var currVal_31 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 42).ngClassUntouched; var currVal_32 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 42).ngClassTouched; var currVal_33 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 42).ngClassPristine; var currVal_34 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 42).ngClassDirty; var currVal_35 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 42).ngClassValid; var currVal_36 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 42).ngClassInvalid; var currVal_37 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 42).ngClassPending; _ck(_v, 34, 1, [currVal_21, currVal_22, currVal_23, currVal_24, currVal_25, currVal_26, currVal_27, currVal_28, currVal_29, currVal_30, currVal_31, currVal_32, currVal_33, currVal_34, currVal_35, currVal_36, currVal_37]); var currVal_46 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 57)._control.errorState; var currVal_47 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 57)._control.errorState; var currVal_48 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 57)._canLabelFloat; var currVal_49 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 57)._shouldLabelFloat(); var currVal_50 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 57)._hideControlPlaceholder(); var currVal_51 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 57)._control.disabled; var currVal_52 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 57)._control.focused; var currVal_53 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 57)._shouldForward("untouched"); var currVal_54 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 57)._shouldForward("touched"); var currVal_55 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 57)._shouldForward("pristine"); var currVal_56 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 57)._shouldForward("dirty"); var currVal_57 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 57)._shouldForward("valid"); var currVal_58 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 57)._shouldForward("invalid"); var currVal_59 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 57)._shouldForward("pending"); _ck(_v, 56, 1, [currVal_46, currVal_47, currVal_48, currVal_49, currVal_50, currVal_51, currVal_52, currVal_53, currVal_54, currVal_55, currVal_56, currVal_57, currVal_58, currVal_59]); var currVal_61 = (__WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 68).required ? "" : null); var currVal_62 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 74)._isServer; var currVal_63 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 74).id; var currVal_64 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 74).placeholder; var currVal_65 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 74).disabled; var currVal_66 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 74).required; var currVal_67 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 74).readonly; var currVal_68 = (__WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 74)._ariaDescribedby || null); var currVal_69 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 74).errorState; var currVal_70 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 74).required.toString(); var currVal_71 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 75).ngClassUntouched; var currVal_72 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 75).ngClassTouched; var currVal_73 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 75).ngClassPristine; var currVal_74 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 75).ngClassDirty; var currVal_75 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 75).ngClassValid; var currVal_76 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 75).ngClassInvalid; var currVal_77 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 75).ngClassPending; _ck(_v, 67, 1, [currVal_61, currVal_62, currVal_63, currVal_64, currVal_65, currVal_66, currVal_67, currVal_68, currVal_69, currVal_70, currVal_71, currVal_72, currVal_73, currVal_74, currVal_75, currVal_76, currVal_77]); var currVal_84 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 86)._control.errorState; var currVal_85 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 86)._control.errorState; var currVal_86 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 86)._canLabelFloat; var currVal_87 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 86)._shouldLabelFloat(); var currVal_88 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 86)._hideControlPlaceholder(); var currVal_89 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 86)._control.disabled; var currVal_90 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 86)._control.focused; var currVal_91 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 86)._shouldForward("untouched"); var currVal_92 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 86)._shouldForward("touched"); var currVal_93 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 86)._shouldForward("pristine"); var currVal_94 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 86)._shouldForward("dirty"); var currVal_95 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 86)._shouldForward("valid"); var currVal_96 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 86)._shouldForward("invalid"); var currVal_97 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 86)._shouldForward("pending"); _ck(_v, 85, 1, [currVal_84, currVal_85, currVal_86, currVal_87, currVal_88, currVal_89, currVal_90, currVal_91, currVal_92, currVal_93, currVal_94, currVal_95, currVal_96, currVal_97]); var currVal_99 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 101)._isServer; var currVal_100 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 101).id; var currVal_101 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 101).placeholder; var currVal_102 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 101).disabled; var currVal_103 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 101).required; var currVal_104 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 101).readonly; var currVal_105 = (__WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 101)._ariaDescribedby || null); var currVal_106 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 101).errorState; var currVal_107 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 101).required.toString(); var currVal_108 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 102).ngClassUntouched; var currVal_109 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 102).ngClassTouched; var currVal_110 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 102).ngClassPristine; var currVal_111 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 102).ngClassDirty; var currVal_112 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 102).ngClassValid; var currVal_113 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 102).ngClassInvalid; var currVal_114 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 102).ngClassPending; _ck(_v, 96, 1, [currVal_99, currVal_100, currVal_101, currVal_102, currVal_103, currVal_104, currVal_105, currVal_106, currVal_107, currVal_108, currVal_109, currVal_110, currVal_111, currVal_112, currVal_113, currVal_114]); }); }
function View_MerchantFeaturedCardDialogComponent_0(_l) { return __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵvid"](0, [__WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵqud"](402653184, 1, { checklistMenu: 0 }), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵqud"](402653184, 2, { newCheckListTitleField: 0 }), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵeld"](2, 0, null, null, 7, "div", [["class", "dialog-content-wrapper"]], null, null, null, null, null)), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n    "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵand"](16777216, null, null, 1, null, View_MerchantFeaturedCardDialogComponent_1)), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](5, 16384, null, 0, __WEBPACK_IMPORTED_MODULE_6__angular_common__["NgIf"], [__WEBPACK_IMPORTED_MODULE_1__angular_core__["ViewContainerRef"], __WEBPACK_IMPORTED_MODULE_1__angular_core__["TemplateRef"]], { ngIf: [0, "ngIf"] }, null), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n\n    "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵand"](16777216, null, null, 1, null, View_MerchantFeaturedCardDialogComponent_2)), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](8, 16384, null, 0, __WEBPACK_IMPORTED_MODULE_6__angular_common__["NgIf"], [__WEBPACK_IMPORTED_MODULE_1__angular_core__["ViewContainerRef"], __WEBPACK_IMPORTED_MODULE_1__angular_core__["TemplateRef"]], { ngIf: [0, "ngIf"] }, null), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n"])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n"]))], function (_ck, _v) { var _co = _v.component; var currVal_0 = _co.card; _ck(_v, 5, 0, currVal_0); var currVal_1 = _co.card; _ck(_v, 8, 0, currVal_1); }, null); }
function View_MerchantFeaturedCardDialogComponent_Host_0(_l) { return __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵvid"](0, [(_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵeld"](0, 0, null, null, 1, "fuse-scrumboard-board-card-dialog", [], null, null, null, View_MerchantFeaturedCardDialogComponent_0, RenderType_MerchantFeaturedCardDialogComponent)), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](1, 245760, null, 0, __WEBPACK_IMPORTED_MODULE_27__card_component__["a" /* MerchantFeaturedCardDialogComponent */], [__WEBPACK_IMPORTED_MODULE_3__angular_material_dialog__["k" /* MatDialogRef */], __WEBPACK_IMPORTED_MODULE_3__angular_material_dialog__["a" /* MAT_DIALOG_DATA */], __WEBPACK_IMPORTED_MODULE_3__angular_material_dialog__["e" /* MatDialog */], __WEBPACK_IMPORTED_MODULE_28__merchant_featured_service__["b" /* MerchantFeaturedService */]], null, null)], function (_ck, _v) { _ck(_v, 1, 0); }, null); }
var MerchantFeaturedCardDialogComponentNgFactory = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵccf"]("fuse-scrumboard-board-card-dialog", __WEBPACK_IMPORTED_MODULE_27__card_component__["a" /* MerchantFeaturedCardDialogComponent */], View_MerchantFeaturedCardDialogComponent_Host_0, {}, {}, []);



/***/ }),

/***/ "./src/app/main/content/pages/merchant-featured/merchant-featured-board/dialogs/card/card.component.scss.ngstyle.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return styles; });
/**
 * @fileoverview This file was generated by the Angular template compiler. Do not edit.
 *
 * @suppress {suspiciousCode,uselessCode,missingProperties,missingOverride,checkTypes}
 * tslint:disable
 */ 
var styles = ["\n\n\n\n.scrumboard-card-dialog {\n  width: 720px; }\n@media screen and (max-width: 599px) {\n    .scrumboard-card-dialog {\n      width: 100%; } }\n.scrumboard-card-dialog .mat-dialog-container {\n    padding: 0; }\n.scrumboard-card-dialog .mat-dialog-container .mat-toolbar .due-date .mat-form-field {\n      width: auto;\n      margin: 0 8px; }\n.scrumboard-card-dialog .mat-dialog-container .mat-toolbar .due-date .mat-form-field .mat-form-field-wrapper {\n        padding: 0; }\n.scrumboard-card-dialog .mat-dialog-container .mat-toolbar .due-date .mat-form-field .mat-form-field-wrapper .mat-form-field-flex {\n          -webkit-box-align: center;\n              -ms-flex-align: center;\n                  align-items: center; }\n.scrumboard-card-dialog .mat-dialog-container .mat-toolbar .due-date .mat-form-field .mat-form-field-wrapper .mat-form-field-flex .mat-form-field-infix {\n            display: none; }\n.scrumboard-card-dialog .mat-dialog-container .mat-toolbar .due-date .mat-form-field .mat-form-field-wrapper .mat-form-field-flex .mat-input-element {\n            display: none; }\n.scrumboard-card-dialog .mat-dialog-container .mat-dialog-content {\n      position: relative;\n      background-color: #F5F5F5; }\n.scrumboard-card-dialog .mat-dialog-container .mat-dialog-content .card-breadcrumb {\n        font-weight: 500;\n        font-size: 14px; }\n.scrumboard-card-dialog .mat-dialog-container .mat-dialog-content .card-subscribe {\n        margin-right: 8px;\n        color: rgba(0, 0, 0, 0.6); }\n.scrumboard-card-dialog .mat-dialog-container .mat-dialog-content .picker {\n        width: 140px;\n        min-width: 140px; }\n.scrumboard-card-dialog .mat-dialog-container .mat-dialog-content .card-name {\n        width: 100%;\n        font-size: 22px; }\n@media screen and (max-width: 599px) {\n          .scrumboard-card-dialog .mat-dialog-container .mat-dialog-content .card-name {\n            font-size: 14px; } }\n.scrumboard-card-dialog .mat-dialog-container .mat-dialog-content .description {\n        padding-bottom: 16px; }\n.scrumboard-card-dialog .mat-dialog-container .mat-dialog-content .sections .section {\n        border-bottom: 1px solid rgba(0, 0, 0, 0.12);\n        margin-bottom: 32px; }\n.scrumboard-card-dialog .mat-dialog-container .mat-dialog-content .sections .section:last-child {\n          border-bottom: none;\n          margin-bottom: 0; }\n.scrumboard-card-dialog .mat-dialog-container .mat-dialog-content .sections .section:last-child .section-content {\n            padding-bottom: 0; }\n.scrumboard-card-dialog .mat-dialog-container .mat-dialog-content .sections .section .section-header {\n          font-size: 16px; }\n.scrumboard-card-dialog .mat-dialog-container .mat-dialog-content .sections .section .section-header mat-icon {\n            margin-right: 8px;\n            color: rgba(0, 0, 0, 0.6); }\n.scrumboard-card-dialog .mat-dialog-container .mat-dialog-content .sections .section .section-header .section-title {\n            font-weight: 500; }\n.scrumboard-card-dialog .mat-dialog-container .mat-dialog-content .sections .section .section-content {\n          padding: 24px 0 32px 0; }\n.scrumboard-card-dialog .mat-dialog-container .mat-dialog-content .sections .section .labels .section-content {\n          padding: 8px 0 32px 0; }\n.scrumboard-card-dialog .mat-dialog-container .mat-dialog-content .sections .section .labels .label-chips {\n          -webkit-box-shadow: none;\n                  box-shadow: none;\n          padding: 0; }\n.scrumboard-card-dialog .mat-dialog-container .mat-dialog-content .sections .section .labels .label-chips .label-chip {\n            display: block; }\n.scrumboard-card-dialog .mat-dialog-container .mat-dialog-content .sections .section .labels .label-chips .label-chip .chip-remove {\n              cursor: pointer; }\n.scrumboard-card-dialog .mat-dialog-container .mat-dialog-content .sections .section .members .section-content {\n          padding: 8px 0 32px 0; }\n.scrumboard-card-dialog .mat-dialog-container .mat-dialog-content .sections .section .members .member-chips {\n          -webkit-box-shadow: none;\n                  box-shadow: none;\n          padding: 0; }\n.scrumboard-card-dialog .mat-dialog-container .mat-dialog-content .sections .section .members .member-chips .member-chip {\n            padding: 4px 12px 4px 4px; }\n.scrumboard-card-dialog .mat-dialog-container .mat-dialog-content .sections .section .members .member-chips .member-chip .member-chip-avatar {\n              width: 32px;\n              border-radius: 50%; }\n.scrumboard-card-dialog .mat-dialog-container .mat-dialog-content .sections .section .members .member-chips .member-chip .chip-remove {\n              cursor: pointer; }\n.scrumboard-card-dialog .mat-dialog-container .mat-dialog-content .sections .section .attachments .attachment {\n          margin-bottom: 16px; }\n.scrumboard-card-dialog .mat-dialog-container .mat-dialog-content .sections .section .attachments .attachment .attachment-preview {\n            background-color: #EEF0F2;\n            width: 160px;\n            height: 128px;\n            background-size: contain;\n            background-position: 50% 50%;\n            background-repeat: no-repeat;\n            font-weight: 500;\n            color: rgba(0, 0, 0, 0.6);\n            margin-right: 24px; }\n@media screen and (max-width: 599px) {\n              .scrumboard-card-dialog .mat-dialog-container .mat-dialog-content .sections .section .attachments .attachment .attachment-preview {\n                margin-bottom: 24px; } }\n.scrumboard-card-dialog .mat-dialog-container .mat-dialog-content .sections .section .attachments .attachment .attachment-content .attachment-url,\n          .scrumboard-card-dialog .mat-dialog-container .mat-dialog-content .sections .section .attachments .attachment .attachment-content .attachment-name {\n            font-weight: 500;\n            font-size: 16px; }\n.scrumboard-card-dialog .mat-dialog-container .mat-dialog-content .sections .section .attachments .attachment .attachment-content .attachment-is-cover {\n            margin-left: 6px; }\n.scrumboard-card-dialog .mat-dialog-container .mat-dialog-content .sections .section .attachments .attachment .attachment-content .attachment-time {\n            color: rgba(0, 0, 0, 0.6); }\n.scrumboard-card-dialog .mat-dialog-container .mat-dialog-content .sections .section .attachments .attachment .attachment-content .attachment-actions-button {\n            background-color: white;\n            text-transform: capitalize;\n            margin: 12px 0 0 0;\n            padding-left: 12px; }\n.scrumboard-card-dialog .mat-dialog-container .mat-dialog-content .sections .section .attachments .attachment .attachment-content .attachment-actions-button mat-icon {\n              margin-left: 8px;\n              color: rgba(0, 0, 0, 0.6); }\n.scrumboard-card-dialog .mat-dialog-container .mat-dialog-content .sections .section .attachments .add-attachment-button {\n          margin: 0; }\n.scrumboard-card-dialog .mat-dialog-container .mat-dialog-content .sections .section .attachments .add-attachment-button mat-icon {\n            color: rgba(0, 0, 0, 0.6);\n            margin-right: 8px; }\n.scrumboard-card-dialog .mat-dialog-container .mat-dialog-content .sections .section .attachments .add-attachment-button span {\n            font-weight: 500;\n            text-transform: capitalize; }\n.scrumboard-card-dialog .mat-dialog-container .mat-dialog-content .sections .section .checklist .checklist-progress {\n          margin-bottom: 16px; }\n.scrumboard-card-dialog .mat-dialog-container .mat-dialog-content .sections .section .checklist .checklist-progress .checklist-progress-value {\n            margin-right: 12px;\n            font-weight: 500;\n            white-space: nowrap;\n            font-size: 14px; }\n.scrumboard-card-dialog .mat-dialog-container .mat-dialog-content .sections .section .checklist .editable-wrap {\n          -webkit-box-flex: 1;\n              -ms-flex: 1;\n                  flex: 1; }\n.scrumboard-card-dialog .mat-dialog-container .mat-dialog-content .sections .section .checklist .check-items .check-item mat-checkbox {\n          margin-bottom: 0; }\n.scrumboard-card-dialog .mat-dialog-container .mat-dialog-content .sections .section .checklist .check-items .check-item mat-checkbox .mat-label {\n            font-size: 14px; }\n.scrumboard-card-dialog .mat-dialog-container .mat-dialog-content .sections .section .checklist .check-items .check-item mat-checkbox.mat-checked .mat-label {\n            text-decoration: line-through;\n            color: rgba(0, 0, 0, 0.6); }\n.scrumboard-card-dialog .mat-dialog-container .mat-dialog-content .sections .section .checklist .new-check-item-form {\n          padding-top: 16px; }\n.scrumboard-card-dialog .mat-dialog-container .mat-dialog-content .sections .section .checklist .new-check-item-form mat-form-field {\n            margin: 0; }\n.scrumboard-card-dialog .mat-dialog-container .mat-dialog-content .sections .section .checklist .new-check-item-form .mat-button {\n            margin: 0 0 0 16px; }\n.scrumboard-card-dialog .mat-dialog-container .mat-dialog-content .sections .section .comments .comment {\n          margin-bottom: 16px; }\n.scrumboard-card-dialog .mat-dialog-container .mat-dialog-content .sections .section .comments .comment .comment-member-avatar {\n            width: 32px;\n            height: 32px;\n            border-radius: 50%;\n            margin-right: 16px; }\n.scrumboard-card-dialog .mat-dialog-container .mat-dialog-content .sections .section .comments .comment .comment-member-name {\n            font-size: 14px;\n            font-weight: 500; }\n.scrumboard-card-dialog .mat-dialog-container .mat-dialog-content .sections .section .comments .comment .comment-time {\n            font-size: 12px; }\n.scrumboard-card-dialog .mat-dialog-container .mat-dialog-content .sections .section .comments .comment .comment-bubble {\n            position: relative;\n            padding: 8px;\n            background-color: white;\n            border: 1px solid #dcdfe1;\n            font-size: 14px;\n            margin: 4px 0; }\n.scrumboard-card-dialog .mat-dialog-container .mat-dialog-content .sections .section .comments .comment .comment-bubble:after, .scrumboard-card-dialog .mat-dialog-container .mat-dialog-content .sections .section .comments .comment .comment-bubble:before {\n              content: ' ';\n              position: absolute;\n              width: 0;\n              height: 0; }\n.scrumboard-card-dialog .mat-dialog-container .mat-dialog-content .sections .section .comments .comment .comment-bubble:after {\n              left: -7px;\n              right: auto;\n              top: 0px;\n              bottom: auto;\n              border: 11px solid;\n              border-color: white transparent transparent transparent; }\n.scrumboard-card-dialog .mat-dialog-container .mat-dialog-content .sections .section .comments .comment .comment-bubble:before {\n              left: -9px;\n              right: auto;\n              top: -1px;\n              bottom: auto;\n              border: 8px solid;\n              border-color: #dcdfe1 transparent transparent transparent; }\n.scrumboard-card-dialog .mat-dialog-container .mat-dialog-content .sections .section .comments .comment.new-comment mat-form-field {\n            margin: 0; }\n.scrumboard-card-dialog .mat-dialog-container .mat-dialog-content .sections .section .activities .activity {\n          margin-bottom: 12px; }\n.scrumboard-card-dialog .mat-dialog-container .mat-dialog-content .sections .section .activities .activity .activity-member-avatar {\n            width: 24px;\n            height: 24px;\n            border-radius: 50%;\n            margin-right: 16px; }\n.scrumboard-card-dialog .mat-dialog-container .mat-dialog-content .sections .section .activities .activity .activity-member-name {\n            font-size: 14px;\n            font-weight: 500;\n            margin-right: 8px; }\n.scrumboard-card-dialog .mat-dialog-container .mat-dialog-content .sections .section .activities .activity .activity-message {\n            font-size: 14px;\n            margin-right: 8px; }\n.scrumboard-card-dialog .mat-dialog-container .mat-dialog-content .sections .section .activities .activity .activity-time {\n            font-size: 12px; }\n.scrumboard-card-dialog .dialog-content-wrapper {\n    max-height: 85vh;\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-orient: vertical;\n    -webkit-box-direction: normal;\n        -ms-flex-direction: column;\n            flex-direction: column; }\n.scrumboard-members-menu {\n  width: 240px; }\n.scrumboard-members-menu .mat-checkbox-layout,\n  .scrumboard-members-menu .mat-checkbox-label {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-flex: 1;\n        -ms-flex: 1;\n            flex: 1; }\n.scrumboard-labels-menu .mat-menu-content {\n  padding-bottom: 0; }\n.scrumboard-labels-menu .mat-menu-content .mat-checkbox-layout,\n  .scrumboard-labels-menu .mat-menu-content .mat-checkbox-label {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-flex: 1;\n        -ms-flex: 1;\n            flex: 1; }\n.scrumboard-labels-menu .mat-menu-content .views {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-orient: vertical;\n    -webkit-box-direction: normal;\n        -ms-flex-direction: column;\n            flex-direction: column;\n    position: relative;\n    overflow: hidden;\n    width: 240px;\n    min-width: 240px;\n    max-width: 240px;\n    min-height: 240px; }\n.scrumboard-labels-menu .mat-menu-content .views .view {\n      position: absolute;\n      width: 240px;\n      height: 100%;\n      bottom: 0;\n      left: 0;\n      right: 0;\n      top: 0; }\n.scrumboard-labels-menu .mat-menu-content .views .view > .header {\n        border-bottom: 1px solid rgba(0, 0, 0, 0.1); }\n"];



/***/ }),

/***/ "./src/app/main/content/pages/merchant-featured/merchant-featured-board/dialogs/card/card.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MerchantFeaturedCardDialogComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_material__ = __webpack_require__("./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__fuse_components_confirm_dialog_confirm_dialog_component__ = __webpack_require__("./src/@fuse/components/confirm-dialog/confirm-dialog.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__fuse_utils__ = __webpack_require__("./src/@fuse/utils/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__merchant_featured_service__ = __webpack_require__("./src/app/main/content/pages/merchant-featured/merchant-featured.service.ts");





var MerchantFeaturedCardDialogComponent = /** @class */ (function () {
    function MerchantFeaturedCardDialogComponent(dialogRef, data, dialog, merchantFeaturedService) {
        this.dialogRef = dialogRef;
        this.data = data;
        this.dialog = dialog;
        this.merchantFeaturedService = merchantFeaturedService;
        this.toggleInArray = __WEBPACK_IMPORTED_MODULE_3__fuse_utils__["a" /* FuseUtils */].toggleInArray;
    }
    MerchantFeaturedCardDialogComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.onBoardChanged =
            this.merchantFeaturedService.onBoardChanged
                .subscribe(function (board) {
                _this.board = board;
                _this.card = _this.board.cards.find(function (_card) {
                    return _this.data.cardId === _card.id;
                });
                _this.list = _this.board.lists.find(function (_list) {
                    return _this.data.listId === _list.id;
                });
            });
    };
    /**
     * Remove Due date
     */
    MerchantFeaturedCardDialogComponent.prototype.removeDueDate = function () {
        this.card.due = '';
        this.updateCard();
    };
    /**
     * Toggle Subscribe
     */
    MerchantFeaturedCardDialogComponent.prototype.toggleSubscribe = function () {
        this.card.subscribed = !this.card.subscribed;
        this.updateCard();
    };
    /**
     * Toggle Cover Image
     * @param attachmentId
     */
    MerchantFeaturedCardDialogComponent.prototype.toggleCoverImage = function (attachmentId) {
        if (this.card.idAttachmentCover === attachmentId) {
            this.card.idAttachmentCover = '';
        }
        else {
            this.card.idAttachmentCover = attachmentId;
        }
        this.updateCard();
    };
    /**
     * Remove Attachment
     * @param attachment
     */
    MerchantFeaturedCardDialogComponent.prototype.removeAttachment = function (attachment) {
        if (attachment.id === this.card.idAttachmentCover) {
            this.card.idAttachmentCover = '';
        }
        this.card.attachments.splice(this.card.attachments.indexOf(attachment), 1);
        this.updateCard();
    };
    /**
     * Remove Checklist
     * @param checklist
     */
    MerchantFeaturedCardDialogComponent.prototype.removeChecklist = function (checklist) {
        this.card.checklists.splice(this.card.checklists.indexOf(checklist), 1);
        this.updateCard();
    };
    /**
     * Update Checked Count
     * @param list
     */
    MerchantFeaturedCardDialogComponent.prototype.updateCheckedCount = function (list) {
        var checkItems = list.checkItems;
        var checkedItems = 0;
        var allCheckedItems = 0;
        var allCheckItems = 0;
        for (var _i = 0, checkItems_1 = checkItems; _i < checkItems_1.length; _i++) {
            var checkItem = checkItems_1[_i];
            if (checkItem.checked) {
                checkedItems++;
            }
        }
        list.checkItemsChecked = checkedItems;
        for (var _a = 0, _b = this.card.checklists; _a < _b.length; _a++) {
            var item = _b[_a];
            allCheckItems += item.checkItems.length;
            allCheckedItems += item.checkItemsChecked;
        }
        this.card.checkItems = allCheckItems;
        this.card.checkItemsChecked = allCheckedItems;
        this.updateCard();
    };
    /**
     * Remove Checklist Item
     * @param checkItem
     * @param checklist
     */
    MerchantFeaturedCardDialogComponent.prototype.removeChecklistItem = function (checkItem, checklist) {
        checklist.checkItems.splice(checklist.checkItems.indexOf(checkItem), 1);
        this.updateCheckedCount(checklist);
        this.updateCard();
    };
    /**
     * Add Check Item
     * @param {NgForm} form
     * @param checkList
     */
    MerchantFeaturedCardDialogComponent.prototype.addCheckItem = function (form, checkList) {
        var checkItemVal = form.value.checkItem;
        if (!checkItemVal || checkItemVal === '') {
            return;
        }
        var newCheckItem = {
            'name': checkItemVal,
            'checked': false
        };
        checkList.checkItems.push(newCheckItem);
        this.updateCheckedCount(checkList);
        form.setValue({ checkItem: '' });
        this.updateCard();
    };
    /**
     * Add Checklist
     * @param {NgForm} form
     */
    MerchantFeaturedCardDialogComponent.prototype.addChecklist = function (form) {
        this.card.checklists.push({
            id: __WEBPACK_IMPORTED_MODULE_3__fuse_utils__["a" /* FuseUtils */].generateGUID(),
            name: form.value.checklistTitle,
            checkItemsChecked: 0,
            checkItems: []
        });
        form.setValue({ checklistTitle: '' });
        form.resetForm();
        this.checklistMenu.closeMenu();
        this.updateCard();
    };
    /**
     * On Checklist Menu Open
     */
    MerchantFeaturedCardDialogComponent.prototype.onChecklistMenuOpen = function () {
        var _this = this;
        setTimeout(function () {
            _this.newCheckListTitleField.nativeElement.focus();
        });
    };
    /**
     * Add New Comment
     * @param {NgForm} form
     */
    MerchantFeaturedCardDialogComponent.prototype.addNewComment = function (form) {
        var newCommentText = form.value.newComment;
        var newComment = {
            idMember: '36027j1930450d8bf7b10158',
            message: newCommentText,
            time: 'now'
        };
        this.card.comments.unshift(newComment);
        form.setValue({ newComment: '' });
        this.updateCard();
    };
    /**
     * Remove Card
     */
    MerchantFeaturedCardDialogComponent.prototype.removeCard = function () {
        var _this = this;
        this.confirmDialogRef = this.dialog.open(__WEBPACK_IMPORTED_MODULE_2__fuse_components_confirm_dialog_confirm_dialog_component__["a" /* FuseConfirmDialogComponent */], {
            disableClose: false
        });
        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete the card?';
        this.confirmDialogRef.afterClosed().subscribe(function (result) {
            if (result) {
                _this.dialogRef.close();
                _this.merchantFeaturedService.removeCard(_this.card.id, _this.list.id);
            }
        });
    };
    /**
     * Update Card
     */
    MerchantFeaturedCardDialogComponent.prototype.updateCard = function () {
        this.merchantFeaturedService.updateCard(this.card);
    };
    MerchantFeaturedCardDialogComponent.prototype.ngOnDestroy = function () {
        this.onBoardChanged.unsubscribe();
    };
    return MerchantFeaturedCardDialogComponent;
}());



/***/ }),

/***/ "./src/app/main/content/pages/merchant-featured/merchant-featured-board/list/add-card/add-card.component.ngfactory.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RenderType_MerchantFeaturedBoardAddCardComponent; });
/* harmony export (immutable) */ __webpack_exports__["b"] = View_MerchantFeaturedBoardAddCardComponent_0;
/* unused harmony export View_MerchantFeaturedBoardAddCardComponent_Host_0 */
/* unused harmony export MerchantFeaturedBoardAddCardComponentNgFactory */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__add_card_component_scss_shim_ngstyle__ = __webpack_require__("./src/app/main/content/pages/merchant-featured/merchant-featured-board/list/add-card/add-card.component.scss.shim.ngstyle.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_flex_layout__ = __webpack_require__("./node_modules/@angular/flex-layout/esm5/flex-layout.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__node_modules_angular_material_icon_typings_index_ngfactory__ = __webpack_require__("./node_modules/@angular/material/icon/typings/index.ngfactory.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_material_icon__ = __webpack_require__("./node_modules/@angular/material/esm5/icon.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__node_modules_angular_material_core_typings_index_ngfactory__ = __webpack_require__("./node_modules/@angular/material/core/typings/index.ngfactory.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_material_core__ = __webpack_require__("./node_modules/@angular/material/esm5/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__angular_forms__ = __webpack_require__("./node_modules/@angular/forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__node_modules_angular_material_form_field_typings_index_ngfactory__ = __webpack_require__("./node_modules/@angular/material/form-field/typings/index.ngfactory.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__angular_material_form_field__ = __webpack_require__("./node_modules/@angular/material/esm5/form-field.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__node_modules_angular_material_select_typings_index_ngfactory__ = __webpack_require__("./node_modules/@angular/material/select/typings/index.ngfactory.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__angular_material_select__ = __webpack_require__("./node_modules/@angular/material/esm5/select.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__angular_cdk_scrolling__ = __webpack_require__("./node_modules/@angular/cdk/esm5/scrolling.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__angular_cdk_bidi__ = __webpack_require__("./node_modules/@angular/cdk/esm5/bidi.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__angular_common__ = __webpack_require__("./node_modules/@angular/common/esm5/common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__node_modules_angular_material_button_typings_index_ngfactory__ = __webpack_require__("./node_modules/@angular/material/button/typings/index.ngfactory.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__angular_material_button__ = __webpack_require__("./node_modules/@angular/material/esm5/button.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__angular_cdk_platform__ = __webpack_require__("./node_modules/@angular/cdk/esm5/platform.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__angular_cdk_a11y__ = __webpack_require__("./node_modules/@angular/cdk/esm5/a11y.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__add_card_component__ = __webpack_require__("./src/app/main/content/pages/merchant-featured/merchant-featured-board/list/add-card/add-card.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__merchant_featured_service__ = __webpack_require__("./src/app/main/content/pages/merchant-featured/merchant-featured.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/**
 * @fileoverview This file was generated by the Angular template compiler. Do not edit.
 *
 * @suppress {suspiciousCode,uselessCode,missingProperties,missingOverride,checkTypes}
 * tslint:disable
 */ 






















var styles_MerchantFeaturedBoardAddCardComponent = [__WEBPACK_IMPORTED_MODULE_0__add_card_component_scss_shim_ngstyle__["a" /* styles */]];
var RenderType_MerchantFeaturedBoardAddCardComponent = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵcrt"]({ encapsulation: 0, styles: styles_MerchantFeaturedBoardAddCardComponent, data: {} });

function View_MerchantFeaturedBoardAddCardComponent_1(_l) { return __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵvid"](0, [(_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵeld"](0, 0, null, null, 13, "div", [["class", "add-card-button"], ["fxLayout", "row"], ["fxLayoutAlign", "start center"]], null, [[null, "click"]], function (_v, en, $event) { var ad = true; var _co = _v.component; if (("click" === en)) {
        var pd_0 = (_co.openForm() !== false);
        ad = (pd_0 && ad);
    } return ad; }, null, null)), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](1, 737280, null, 0, __WEBPACK_IMPORTED_MODULE_2__angular_flex_layout__["j" /* LayoutDirective */], [__WEBPACK_IMPORTED_MODULE_2__angular_flex_layout__["m" /* MediaMonitor */], __WEBPACK_IMPORTED_MODULE_1__angular_core__["ElementRef"], __WEBPACK_IMPORTED_MODULE_2__angular_flex_layout__["t" /* StyleUtils */]], { layout: [0, "layout"] }, null), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](2, 737280, null, 0, __WEBPACK_IMPORTED_MODULE_2__angular_flex_layout__["i" /* LayoutAlignDirective */], [__WEBPACK_IMPORTED_MODULE_2__angular_flex_layout__["m" /* MediaMonitor */], __WEBPACK_IMPORTED_MODULE_1__angular_core__["ElementRef"], [2, __WEBPACK_IMPORTED_MODULE_2__angular_flex_layout__["j" /* LayoutDirective */]], __WEBPACK_IMPORTED_MODULE_2__angular_flex_layout__["t" /* StyleUtils */]], { align: [0, "align"] }, null), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n    "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵeld"](4, 0, null, null, 5, "div", [], null, null, null, null, null)), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n        "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵeld"](6, 0, null, null, 2, "mat-icon", [["class", "s-20 mat-icon"], ["role", "img"]], null, null, null, __WEBPACK_IMPORTED_MODULE_3__node_modules_angular_material_icon_typings_index_ngfactory__["b" /* View_MatIcon_0 */], __WEBPACK_IMPORTED_MODULE_3__node_modules_angular_material_icon_typings_index_ngfactory__["a" /* RenderType_MatIcon */])), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](7, 638976, null, 0, __WEBPACK_IMPORTED_MODULE_4__angular_material_icon__["b" /* MatIcon */], [__WEBPACK_IMPORTED_MODULE_1__angular_core__["ElementRef"], __WEBPACK_IMPORTED_MODULE_4__angular_material_icon__["d" /* MatIconRegistry */], [8, null]], null, null), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, 0, ["add"])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n    "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n    "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵeld"](11, 0, null, null, 1, "span", [], null, null, null, null, null)), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["Add Merchant"])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n"]))], function (_ck, _v) { var currVal_0 = "row"; _ck(_v, 1, 0, currVal_0); var currVal_1 = "start center"; _ck(_v, 2, 0, currVal_1); _ck(_v, 7, 0); }, null); }
function View_MerchantFeaturedBoardAddCardComponent_3(_l) { return __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵvid"](0, [(_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵeld"](0, 0, null, null, 2, "mat-option", [["class", "mat-option"], ["role", "option"]], [[1, "tabindex", 0], [2, "mat-selected", null], [2, "mat-option-multiple", null], [2, "mat-active", null], [8, "id", 0], [1, "aria-selected", 0], [1, "aria-disabled", 0], [2, "mat-option-disabled", null]], [[null, "click"], [null, "keydown"]], function (_v, en, $event) { var ad = true; if (("click" === en)) {
        var pd_0 = (__WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 1)._selectViaInteraction() !== false);
        ad = (pd_0 && ad);
    } if (("keydown" === en)) {
        var pd_1 = (__WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 1)._handleKeydown($event) !== false);
        ad = (pd_1 && ad);
    } return ad; }, __WEBPACK_IMPORTED_MODULE_5__node_modules_angular_material_core_typings_index_ngfactory__["e" /* View_MatOption_0 */], __WEBPACK_IMPORTED_MODULE_5__node_modules_angular_material_core_typings_index_ngfactory__["b" /* RenderType_MatOption */])), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](1, 8437760, [[9, 4]], 0, __WEBPACK_IMPORTED_MODULE_6__angular_material_core__["u" /* MatOption */], [__WEBPACK_IMPORTED_MODULE_1__angular_core__["ElementRef"], __WEBPACK_IMPORTED_MODULE_1__angular_core__["ChangeDetectorRef"], [2, __WEBPACK_IMPORTED_MODULE_6__angular_material_core__["m" /* MAT_OPTION_PARENT_COMPONENT */]], [2, __WEBPACK_IMPORTED_MODULE_6__angular_material_core__["t" /* MatOptgroup */]]], { value: [0, "value"] }, null), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](2, 0, ["\n                        ", "\n                    "]))], function (_ck, _v) { var currVal_8 = _v.context.$implicit; _ck(_v, 1, 0, currVal_8); }, function (_ck, _v) { var currVal_0 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 1)._getTabIndex(); var currVal_1 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 1).selected; var currVal_2 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 1).multiple; var currVal_3 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 1).active; var currVal_4 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 1).id; var currVal_5 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 1).selected.toString(); var currVal_6 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 1).disabled.toString(); var currVal_7 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 1).disabled; _ck(_v, 0, 0, currVal_0, currVal_1, currVal_2, currVal_3, currVal_4, currVal_5, currVal_6, currVal_7); var currVal_9 = _v.context.$implicit.name; _ck(_v, 2, 0, currVal_9); }); }
function View_MerchantFeaturedBoardAddCardComponent_2(_l) { return __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵvid"](0, [(_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵeld"](0, 0, null, null, 60, "div", [["class", "add-card-form-wrapper"]], null, null, null, null, null)), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n\n    "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵeld"](2, 0, null, null, 57, "form", [["class", "add-card-form"], ["fxLayout", "column"], ["novalidate", ""]], [[2, "ng-untouched", null], [2, "ng-touched", null], [2, "ng-pristine", null], [2, "ng-dirty", null], [2, "ng-valid", null], [2, "ng-invalid", null], [2, "ng-pending", null]], [[null, "submit"], [null, "reset"]], function (_v, en, $event) { var ad = true; var _co = _v.component; if (("submit" === en)) {
        var pd_0 = (__WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 4).onSubmit($event) !== false);
        ad = (pd_0 && ad);
    } if (("reset" === en)) {
        var pd_1 = (__WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 4).onReset() !== false);
        ad = (pd_1 && ad);
    } if (("submit" === en)) {
        var pd_2 = (_co.onFormSubmit() !== false);
        ad = (pd_2 && ad);
    } return ad; }, null, null)), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](3, 16384, null, 0, __WEBPACK_IMPORTED_MODULE_7__angular_forms__["F" /* ɵbf */], [], null, null), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](4, 540672, null, 0, __WEBPACK_IMPORTED_MODULE_7__angular_forms__["n" /* FormGroupDirective */], [[8, null], [8, null]], { form: [0, "form"] }, null), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵprd"](2048, null, __WEBPACK_IMPORTED_MODULE_7__angular_forms__["d" /* ControlContainer */], null, [__WEBPACK_IMPORTED_MODULE_7__angular_forms__["n" /* FormGroupDirective */]]), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](6, 16384, null, 0, __WEBPACK_IMPORTED_MODULE_7__angular_forms__["w" /* NgControlStatusGroup */], [__WEBPACK_IMPORTED_MODULE_7__angular_forms__["d" /* ControlContainer */]], null, null), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](7, 737280, null, 0, __WEBPACK_IMPORTED_MODULE_2__angular_flex_layout__["j" /* LayoutDirective */], [__WEBPACK_IMPORTED_MODULE_2__angular_flex_layout__["m" /* MediaMonitor */], __WEBPACK_IMPORTED_MODULE_1__angular_core__["ElementRef"], __WEBPACK_IMPORTED_MODULE_2__angular_flex_layout__["t" /* StyleUtils */]], { layout: [0, "layout"] }, null), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n\n        "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵeld"](9, 0, null, null, 29, "mat-form-field", [["class", "mat-input-container mat-form-field"], ["floatPlaceholder", "never"], ["fxFlex", ""]], [[2, "mat-input-invalid", null], [2, "mat-form-field-invalid", null], [2, "mat-form-field-can-float", null], [2, "mat-form-field-should-float", null], [2, "mat-form-field-hide-placeholder", null], [2, "mat-form-field-disabled", null], [2, "mat-focused", null], [2, "ng-untouched", null], [2, "ng-touched", null], [2, "ng-pristine", null], [2, "ng-dirty", null], [2, "ng-valid", null], [2, "ng-invalid", null], [2, "ng-pending", null]], null, null, __WEBPACK_IMPORTED_MODULE_8__node_modules_angular_material_form_field_typings_index_ngfactory__["b" /* View_MatFormField_0 */], __WEBPACK_IMPORTED_MODULE_8__node_modules_angular_material_form_field_typings_index_ngfactory__["a" /* RenderType_MatFormField */])), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](10, 7389184, null, 7, __WEBPACK_IMPORTED_MODULE_9__angular_material_form_field__["b" /* MatFormField */], [__WEBPACK_IMPORTED_MODULE_1__angular_core__["ElementRef"], __WEBPACK_IMPORTED_MODULE_1__angular_core__["ChangeDetectorRef"], [2, __WEBPACK_IMPORTED_MODULE_6__angular_material_core__["k" /* MAT_LABEL_GLOBAL_OPTIONS */]]], { floatPlaceholder: [0, "floatPlaceholder"] }, null), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵqud"](335544320, 2, { _control: 0 }), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵqud"](335544320, 3, { _placeholderChild: 0 }), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵqud"](335544320, 4, { _labelChild: 0 }), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵqud"](603979776, 5, { _errorChildren: 1 }), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵqud"](603979776, 6, { _hintChildren: 1 }), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵqud"](603979776, 7, { _prefixChildren: 1 }), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵqud"](603979776, 8, { _suffixChildren: 1 }), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](18, 737280, null, 0, __WEBPACK_IMPORTED_MODULE_2__angular_flex_layout__["e" /* FlexDirective */], [__WEBPACK_IMPORTED_MODULE_2__angular_flex_layout__["m" /* MediaMonitor */], __WEBPACK_IMPORTED_MODULE_1__angular_core__["ElementRef"], [3, __WEBPACK_IMPORTED_MODULE_2__angular_flex_layout__["j" /* LayoutDirective */]], __WEBPACK_IMPORTED_MODULE_2__angular_flex_layout__["t" /* StyleUtils */]], { flex: [0, "flex"] }, null), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, 1, ["\n            "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, 1, ["\n            "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵeld"](21, 0, null, 1, 15, "mat-select", [["class", "mat-select"], ["formControlName", "name"], ["id", "merchantfield"], ["placeholder", "Add Merchant"], ["required", ""], ["role", "listbox"]], [[1, "required", 0], [2, "ng-untouched", null], [2, "ng-touched", null], [2, "ng-pristine", null], [2, "ng-dirty", null], [2, "ng-valid", null], [2, "ng-invalid", null], [2, "ng-pending", null], [1, "id", 0], [1, "tabindex", 0], [1, "aria-label", 0], [1, "aria-labelledby", 0], [1, "aria-required", 0], [1, "aria-disabled", 0], [1, "aria-invalid", 0], [1, "aria-owns", 0], [1, "aria-multiselectable", 0], [1, "aria-describedby", 0], [1, "aria-activedescendant", 0], [2, "mat-select-disabled", null], [2, "mat-select-invalid", null], [2, "mat-select-required", null]], [[null, "keydown"], [null, "focus"], [null, "blur"]], function (_v, en, $event) { var ad = true; if (("keydown" === en)) {
        var pd_0 = (__WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 28)._handleKeydown($event) !== false);
        ad = (pd_0 && ad);
    } if (("focus" === en)) {
        var pd_1 = (__WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 28)._onFocus() !== false);
        ad = (pd_1 && ad);
    } if (("blur" === en)) {
        var pd_2 = (__WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 28)._onBlur() !== false);
        ad = (pd_2 && ad);
    } return ad; }, __WEBPACK_IMPORTED_MODULE_10__node_modules_angular_material_select_typings_index_ngfactory__["b" /* View_MatSelect_0 */], __WEBPACK_IMPORTED_MODULE_10__node_modules_angular_material_select_typings_index_ngfactory__["a" /* RenderType_MatSelect */])), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵprd"](6144, null, __WEBPACK_IMPORTED_MODULE_6__angular_material_core__["m" /* MAT_OPTION_PARENT_COMPONENT */], null, [__WEBPACK_IMPORTED_MODULE_11__angular_material_select__["c" /* MatSelect */]]), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](23, 16384, null, 0, __WEBPACK_IMPORTED_MODULE_7__angular_forms__["B" /* RequiredValidator */], [], { required: [0, "required"] }, null), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵprd"](1024, null, __WEBPACK_IMPORTED_MODULE_7__angular_forms__["s" /* NG_VALIDATORS */], function (p0_0) { return [p0_0]; }, [__WEBPACK_IMPORTED_MODULE_7__angular_forms__["B" /* RequiredValidator */]]), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](25, 671744, null, 0, __WEBPACK_IMPORTED_MODULE_7__angular_forms__["l" /* FormControlName */], [[3, __WEBPACK_IMPORTED_MODULE_7__angular_forms__["d" /* ControlContainer */]], [2, __WEBPACK_IMPORTED_MODULE_7__angular_forms__["s" /* NG_VALIDATORS */]], [8, null], [8, null]], { name: [0, "name"] }, null), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵprd"](2048, null, __WEBPACK_IMPORTED_MODULE_7__angular_forms__["u" /* NgControl */], null, [__WEBPACK_IMPORTED_MODULE_7__angular_forms__["l" /* FormControlName */]]), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](27, 16384, null, 0, __WEBPACK_IMPORTED_MODULE_7__angular_forms__["v" /* NgControlStatus */], [__WEBPACK_IMPORTED_MODULE_7__angular_forms__["u" /* NgControl */]], null, null), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](28, 2080768, [[1, 4], ["nameInput", 4]], 3, __WEBPACK_IMPORTED_MODULE_11__angular_material_select__["c" /* MatSelect */], [__WEBPACK_IMPORTED_MODULE_12__angular_cdk_scrolling__["g" /* ViewportRuler */], __WEBPACK_IMPORTED_MODULE_1__angular_core__["ChangeDetectorRef"], __WEBPACK_IMPORTED_MODULE_1__angular_core__["NgZone"], __WEBPACK_IMPORTED_MODULE_6__angular_material_core__["d" /* ErrorStateMatcher */], __WEBPACK_IMPORTED_MODULE_1__angular_core__["ElementRef"], [2, __WEBPACK_IMPORTED_MODULE_13__angular_cdk_bidi__["c" /* Directionality */]], [2, __WEBPACK_IMPORTED_MODULE_7__angular_forms__["x" /* NgForm */]], [2, __WEBPACK_IMPORTED_MODULE_7__angular_forms__["n" /* FormGroupDirective */]], [2, __WEBPACK_IMPORTED_MODULE_9__angular_material_form_field__["b" /* MatFormField */]], [2, __WEBPACK_IMPORTED_MODULE_7__angular_forms__["u" /* NgControl */]], [8, null], __WEBPACK_IMPORTED_MODULE_11__angular_material_select__["a" /* MAT_SELECT_SCROLL_STRATEGY */]], { placeholder: [0, "placeholder"], required: [1, "required"], compareWith: [2, "compareWith"], id: [3, "id"] }, null), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵqud"](603979776, 9, { options: 1 }), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵqud"](603979776, 10, { optionGroups: 1 }), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵqud"](335544320, 11, { customTrigger: 0 }), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵprd"](2048, [[2, 4]], __WEBPACK_IMPORTED_MODULE_9__angular_material_form_field__["c" /* MatFormFieldControl */], null, [__WEBPACK_IMPORTED_MODULE_11__angular_material_select__["c" /* MatSelect */]]), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, 1, ["\n                    "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵand"](16777216, null, 1, 1, null, View_MerchantFeaturedBoardAddCardComponent_3)), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](35, 802816, null, 0, __WEBPACK_IMPORTED_MODULE_14__angular_common__["NgForOf"], [__WEBPACK_IMPORTED_MODULE_1__angular_core__["ViewContainerRef"], __WEBPACK_IMPORTED_MODULE_1__angular_core__["TemplateRef"], __WEBPACK_IMPORTED_MODULE_1__angular_core__["IterableDiffers"]], { ngForOf: [0, "ngForOf"] }, null), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, 1, ["\n            "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, 1, ["\n            "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, 1, ["\n        "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n\n        "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵeld"](40, 0, null, null, 18, "div", [["class", "pl-8"], ["fxLayout", "row"], ["fxLayoutAlign", "space-between center"]], null, null, null, null, null)), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](41, 737280, null, 0, __WEBPACK_IMPORTED_MODULE_2__angular_flex_layout__["j" /* LayoutDirective */], [__WEBPACK_IMPORTED_MODULE_2__angular_flex_layout__["m" /* MediaMonitor */], __WEBPACK_IMPORTED_MODULE_1__angular_core__["ElementRef"], __WEBPACK_IMPORTED_MODULE_2__angular_flex_layout__["t" /* StyleUtils */]], { layout: [0, "layout"] }, null), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](42, 737280, null, 0, __WEBPACK_IMPORTED_MODULE_2__angular_flex_layout__["i" /* LayoutAlignDirective */], [__WEBPACK_IMPORTED_MODULE_2__angular_flex_layout__["m" /* MediaMonitor */], __WEBPACK_IMPORTED_MODULE_1__angular_core__["ElementRef"], [2, __WEBPACK_IMPORTED_MODULE_2__angular_flex_layout__["j" /* LayoutDirective */]], __WEBPACK_IMPORTED_MODULE_2__angular_flex_layout__["t" /* StyleUtils */]], { align: [0, "align"] }, null), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n\n            "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵeld"](44, 0, null, null, 5, "button", [["aria-label", "add"], ["class", "add-button mat-accent"], ["mat-raised-button", ""]], [[8, "disabled", 0]], null, null, __WEBPACK_IMPORTED_MODULE_15__node_modules_angular_material_button_typings_index_ngfactory__["d" /* View_MatButton_0 */], __WEBPACK_IMPORTED_MODULE_15__node_modules_angular_material_button_typings_index_ngfactory__["b" /* RenderType_MatButton */])), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](45, 180224, null, 0, __WEBPACK_IMPORTED_MODULE_16__angular_material_button__["b" /* MatButton */], [__WEBPACK_IMPORTED_MODULE_1__angular_core__["ElementRef"], __WEBPACK_IMPORTED_MODULE_17__angular_cdk_platform__["a" /* Platform */], __WEBPACK_IMPORTED_MODULE_18__angular_cdk_a11y__["j" /* FocusMonitor */]], { disabled: [0, "disabled"] }, null), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, 0, ["\n                "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵeld"](47, 0, null, 0, 1, "span", [], null, null, null, null, null)), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["Add"])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, 0, ["\n            "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n\n            "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵeld"](51, 0, null, null, 6, "button", [["aria-label", "cancel"], ["class", "cancel-button"], ["mat-icon-button", ""]], [[8, "disabled", 0]], [[null, "click"]], function (_v, en, $event) { var ad = true; var _co = _v.component; if (("click" === en)) {
        var pd_0 = (_co.closeForm() !== false);
        ad = (pd_0 && ad);
    } return ad; }, __WEBPACK_IMPORTED_MODULE_15__node_modules_angular_material_button_typings_index_ngfactory__["d" /* View_MatButton_0 */], __WEBPACK_IMPORTED_MODULE_15__node_modules_angular_material_button_typings_index_ngfactory__["b" /* RenderType_MatButton */])), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](52, 180224, null, 0, __WEBPACK_IMPORTED_MODULE_16__angular_material_button__["b" /* MatButton */], [__WEBPACK_IMPORTED_MODULE_1__angular_core__["ElementRef"], __WEBPACK_IMPORTED_MODULE_17__angular_cdk_platform__["a" /* Platform */], __WEBPACK_IMPORTED_MODULE_18__angular_cdk_a11y__["j" /* FocusMonitor */]], null, null), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, 0, ["\n                "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵeld"](54, 0, null, 0, 2, "mat-icon", [["class", "mat-icon"], ["role", "img"]], null, null, null, __WEBPACK_IMPORTED_MODULE_3__node_modules_angular_material_icon_typings_index_ngfactory__["b" /* View_MatIcon_0 */], __WEBPACK_IMPORTED_MODULE_3__node_modules_angular_material_icon_typings_index_ngfactory__["a" /* RenderType_MatIcon */])), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](55, 638976, null, 0, __WEBPACK_IMPORTED_MODULE_4__angular_material_icon__["b" /* MatIcon */], [__WEBPACK_IMPORTED_MODULE_1__angular_core__["ElementRef"], __WEBPACK_IMPORTED_MODULE_4__angular_material_icon__["d" /* MatIconRegistry */], [8, null]], null, null), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, 0, ["close"])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, 0, ["\n            "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n        "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n    "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n"]))], function (_ck, _v) { var _co = _v.component; var currVal_7 = _co.form; _ck(_v, 4, 0, currVal_7); var currVal_8 = "column"; _ck(_v, 7, 0, currVal_8); var currVal_23 = "never"; _ck(_v, 10, 0, currVal_23); var currVal_24 = ""; _ck(_v, 18, 0, currVal_24); var currVal_47 = ""; _ck(_v, 23, 0, currVal_47); var currVal_48 = "name"; _ck(_v, 25, 0, currVal_48); var currVal_49 = "Add Merchant"; var currVal_50 = ""; var currVal_51 = _co.compareObj; var currVal_52 = "merchantfield"; _ck(_v, 28, 0, currVal_49, currVal_50, currVal_51, currVal_52); var currVal_53 = _co.merchants; _ck(_v, 35, 0, currVal_53); var currVal_54 = "row"; _ck(_v, 41, 0, currVal_54); var currVal_55 = "space-between center"; _ck(_v, 42, 0, currVal_55); var currVal_57 = _co.form.invalid; _ck(_v, 45, 0, currVal_57); _ck(_v, 55, 0); }, function (_ck, _v) { var currVal_0 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 6).ngClassUntouched; var currVal_1 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 6).ngClassTouched; var currVal_2 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 6).ngClassPristine; var currVal_3 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 6).ngClassDirty; var currVal_4 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 6).ngClassValid; var currVal_5 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 6).ngClassInvalid; var currVal_6 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 6).ngClassPending; _ck(_v, 2, 0, currVal_0, currVal_1, currVal_2, currVal_3, currVal_4, currVal_5, currVal_6); var currVal_9 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 10)._control.errorState; var currVal_10 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 10)._control.errorState; var currVal_11 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 10)._canLabelFloat; var currVal_12 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 10)._shouldLabelFloat(); var currVal_13 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 10)._hideControlPlaceholder(); var currVal_14 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 10)._control.disabled; var currVal_15 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 10)._control.focused; var currVal_16 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 10)._shouldForward("untouched"); var currVal_17 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 10)._shouldForward("touched"); var currVal_18 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 10)._shouldForward("pristine"); var currVal_19 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 10)._shouldForward("dirty"); var currVal_20 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 10)._shouldForward("valid"); var currVal_21 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 10)._shouldForward("invalid"); var currVal_22 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 10)._shouldForward("pending"); _ck(_v, 9, 1, [currVal_9, currVal_10, currVal_11, currVal_12, currVal_13, currVal_14, currVal_15, currVal_16, currVal_17, currVal_18, currVal_19, currVal_20, currVal_21, currVal_22]); var currVal_25 = (__WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 23).required ? "" : null); var currVal_26 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 27).ngClassUntouched; var currVal_27 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 27).ngClassTouched; var currVal_28 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 27).ngClassPristine; var currVal_29 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 27).ngClassDirty; var currVal_30 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 27).ngClassValid; var currVal_31 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 27).ngClassInvalid; var currVal_32 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 27).ngClassPending; var currVal_33 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 28).id; var currVal_34 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 28).tabIndex; var currVal_35 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 28)._ariaLabel; var currVal_36 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 28).ariaLabelledby; var currVal_37 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 28).required.toString(); var currVal_38 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 28).disabled.toString(); var currVal_39 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 28).errorState; var currVal_40 = (__WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 28).panelOpen ? __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 28)._optionIds : null); var currVal_41 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 28).multiple; var currVal_42 = (__WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 28)._ariaDescribedby || null); var currVal_43 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 28)._getAriaActiveDescendant(); var currVal_44 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 28).disabled; var currVal_45 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 28).errorState; var currVal_46 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 28).required; _ck(_v, 21, 1, [currVal_25, currVal_26, currVal_27, currVal_28, currVal_29, currVal_30, currVal_31, currVal_32, currVal_33, currVal_34, currVal_35, currVal_36, currVal_37, currVal_38, currVal_39, currVal_40, currVal_41, currVal_42, currVal_43, currVal_44, currVal_45, currVal_46]); var currVal_56 = (__WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 45).disabled || null); _ck(_v, 44, 0, currVal_56); var currVal_58 = (__WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 52).disabled || null); _ck(_v, 51, 0, currVal_58); }); }
function View_MerchantFeaturedBoardAddCardComponent_0(_l) { return __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵvid"](0, [__WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵqud"](671088640, 1, { nameInputField: 0 }), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵand"](16777216, null, null, 1, null, View_MerchantFeaturedBoardAddCardComponent_1)), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](2, 16384, null, 0, __WEBPACK_IMPORTED_MODULE_14__angular_common__["NgIf"], [__WEBPACK_IMPORTED_MODULE_1__angular_core__["ViewContainerRef"], __WEBPACK_IMPORTED_MODULE_1__angular_core__["TemplateRef"]], { ngIf: [0, "ngIf"] }, null), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n\n"])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵand"](16777216, null, null, 1, null, View_MerchantFeaturedBoardAddCardComponent_2)), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](5, 16384, null, 0, __WEBPACK_IMPORTED_MODULE_14__angular_common__["NgIf"], [__WEBPACK_IMPORTED_MODULE_1__angular_core__["ViewContainerRef"], __WEBPACK_IMPORTED_MODULE_1__angular_core__["TemplateRef"]], { ngIf: [0, "ngIf"] }, null), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n"]))], function (_ck, _v) { var _co = _v.component; var currVal_0 = !_co.formActive; _ck(_v, 2, 0, currVal_0); var currVal_1 = _co.formActive; _ck(_v, 5, 0, currVal_1); }, null); }
function View_MerchantFeaturedBoardAddCardComponent_Host_0(_l) { return __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵvid"](0, [(_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵeld"](0, 0, null, null, 1, "fuse-scrumboard-board-add-card", [], null, null, null, View_MerchantFeaturedBoardAddCardComponent_0, RenderType_MerchantFeaturedBoardAddCardComponent)), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](1, 49152, null, 0, __WEBPACK_IMPORTED_MODULE_19__add_card_component__["a" /* MerchantFeaturedBoardAddCardComponent */], [__WEBPACK_IMPORTED_MODULE_7__angular_forms__["i" /* FormBuilder */], __WEBPACK_IMPORTED_MODULE_20__merchant_featured_service__["b" /* MerchantFeaturedService */], __WEBPACK_IMPORTED_MODULE_21__angular_router__["o" /* Router */]], null, null)], null, null); }
var MerchantFeaturedBoardAddCardComponentNgFactory = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵccf"]("fuse-scrumboard-board-add-card", __WEBPACK_IMPORTED_MODULE_19__add_card_component__["a" /* MerchantFeaturedBoardAddCardComponent */], View_MerchantFeaturedBoardAddCardComponent_Host_0, { merchants: "merchants" }, { onCardAdd: "onCardAdd" }, []);



/***/ }),

/***/ "./src/app/main/content/pages/merchant-featured/merchant-featured-board/list/add-card/add-card.component.scss.shim.ngstyle.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return styles; });
/**
 * @fileoverview This file was generated by the Angular template compiler. Do not edit.
 *
 * @suppress {suspiciousCode,uselessCode,missingProperties,missingOverride,checkTypes}
 * tslint:disable
 */ 
var styles = ["[_nghost-%COMP%]   .add-card-button[_ngcontent-%COMP%] {\n  position: relative;\n  height: 48px;\n  min-height: 48px;\n  padding: 0 16px;\n  text-align: left;\n  text-transform: none;\n  font-weight: 500;\n  font-size: 14px;\n  background-color: #DCDFE2;\n  cursor: pointer;\n  border-radius: 2px; }\n  [_nghost-%COMP%]   .add-card-button[_ngcontent-%COMP%]   mat-icon[_ngcontent-%COMP%] {\n    margin-right: 8px;\n    color: rgba(0, 0, 0, 0.6); }\n  [_nghost-%COMP%]   .add-card-form-wrapper[_ngcontent-%COMP%] {\n  background-color: #DCDFE2; }\n  [_nghost-%COMP%]   .add-card-form-wrapper[_ngcontent-%COMP%]   .add-card-form[_ngcontent-%COMP%] {\n    z-index: 999;\n    background: white;\n    display: block;\n    position: relative;\n    padding: 8px;\n    border-top: 1px solid rgba(0, 0, 0, 0.12); }\n  [_nghost-%COMP%]   .add-card-form-wrapper[_ngcontent-%COMP%]   .add-card-form[_ngcontent-%COMP%]   mat-form-field[_ngcontent-%COMP%] {\n      width: 100%;\n      margin: 0;\n      padding: 12px 8px; }"];



/***/ }),

/***/ "./src/app/main/content/pages/merchant-featured/merchant-featured-board/list/add-card/add-card.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MerchantFeaturedBoardAddCardComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__("./node_modules/@angular/forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__merchant_featured_service__ = __webpack_require__("./src/app/main/content/pages/merchant-featured/merchant-featured.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_lodash__ = __webpack_require__("./node_modules/lodash/lodash.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_lodash___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_lodash__);





var MerchantFeaturedBoardAddCardComponent = /** @class */ (function () {
    function MerchantFeaturedBoardAddCardComponent(formBuilder, merchantFeaturedService, router) {
        this.formBuilder = formBuilder;
        this.merchantFeaturedService = merchantFeaturedService;
        this.router = router;
        this.formActive = false;
        this.onCardAdd = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.roleOptions = [
            {
                id: 1,
                name: "Usman"
            },
            {
                id: 2,
                name: "Bilal"
            },
            {
                id: 3,
                name: "Mubashir"
            },
            {
                id: 4,
                name: "Ahmad"
            },
            {
                id: 5,
                name: "Hassan"
            },
            {
                id: 6,
                name: "Mubeen"
            }
        ];
        console.log(merchantFeaturedService.merchants.length);
        debugger;
        debugger;
        var count = this.merchantFeaturedService.featuredMerchants.length;
        var farray = this.merchantFeaturedService.merchants;
        var sarray = new Array;
        var finalarray = new Array;
        for (var i = 0; i < count; i++) {
            sarray.push(this.merchantFeaturedService.featuredMerchants[i].merchant);
        }
        var temp = 0;
        finalarray = __WEBPACK_IMPORTED_MODULE_4_lodash__["differenceBy"](farray, sarray, 'id');
        // console.log(farray);
        // console.log(sarray)
        // console.log(finalarray)
        this.merchants = finalarray;
    }
    MerchantFeaturedBoardAddCardComponent.prototype.openForm = function () {
        this.form = this.formBuilder.group({
            name: ''
        });
        this.formActive = true;
        this.focusNameField();
    };
    MerchantFeaturedBoardAddCardComponent.prototype.compareObj = function (obj1, obj2) {
        return obj1.id !== obj2.id;
    };
    MerchantFeaturedBoardAddCardComponent.prototype.closeForm = function () {
        this.formActive = false;
    };
    MerchantFeaturedBoardAddCardComponent.prototype.focusNameField = function () {
        var _this = this;
        setTimeout(function () {
            _this.nameInputField.nativeElement.focus();
        });
    };
    MerchantFeaturedBoardAddCardComponent.prototype.onFormSubmit = function () {
        if (this.form.valid && this.form.controls.name.value.id != null) {
            debugger;
            var cardName = this.form.getRawValue().name.name;
            var newMerchant = {};
            for (var i = 0; i < this.merchants.length; i++) {
                if (cardName == this.merchants[i].name) {
                    newMerchant = this.merchants[i];
                }
            }
            this.onCardAdd.next(newMerchant);
            this.formActive = false;
            var select = document.getElementById("merchantfield");
            for (var i = select.options.length - 1; i >= 0; i--) {
                select.remove(i);
            }
        }
    };
    return MerchantFeaturedBoardAddCardComponent;
}());



/***/ }),

/***/ "./src/app/main/content/pages/merchant-featured/merchant-featured-board/list/card/card.component.ngfactory.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RenderType_MerchantFeaturedBoardCardComponent; });
/* harmony export (immutable) */ __webpack_exports__["b"] = View_MerchantFeaturedBoardCardComponent_0;
/* unused harmony export View_MerchantFeaturedBoardCardComponent_Host_0 */
/* unused harmony export MerchantFeaturedBoardCardComponentNgFactory */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__card_component_scss_ngstyle__ = __webpack_require__("./src/app/main/content/pages/merchant-featured/merchant-featured-board/list/card/card.component.scss.ngstyle.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__node_modules_angular_material_icon_typings_index_ngfactory__ = __webpack_require__("./node_modules/@angular/material/icon/typings/index.ngfactory.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_material_icon__ = __webpack_require__("./node_modules/@angular/material/esm5/icon.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__card_component__ = __webpack_require__("./src/app/main/content/pages/merchant-featured/merchant-featured-board/list/card/card.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/**
 * @fileoverview This file was generated by the Angular template compiler. Do not edit.
 *
 * @suppress {suspiciousCode,uselessCode,missingProperties,missingOverride,checkTypes}
 * tslint:disable
 */ 






var styles_MerchantFeaturedBoardCardComponent = [__WEBPACK_IMPORTED_MODULE_0__card_component_scss_ngstyle__["a" /* styles */]];
var RenderType_MerchantFeaturedBoardCardComponent = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵcrt"]({ encapsulation: 2, styles: styles_MerchantFeaturedBoardCardComponent, data: {} });

function View_MerchantFeaturedBoardCardComponent_0(_l) { return __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵvid"](0, [(_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n"])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n"])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n\n"])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n"])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵeld"](4, 0, null, null, 28, "div", [["class", "list-card-details"]], null, null, null, null, null)), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n\n    "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n    "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵeld"](7, 0, null, null, 4, "div", [["class", "list-card-sort-handle"]], null, null, null, null, null)), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n        "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵeld"](9, 0, null, null, 1, "mat-icon", [["class", "icon s16 mat-icon"], ["mat-font-icon", "icon-cursor-move"], ["role", "img"]], null, null, null, __WEBPACK_IMPORTED_MODULE_2__node_modules_angular_material_icon_typings_index_ngfactory__["b" /* View_MatIcon_0 */], __WEBPACK_IMPORTED_MODULE_2__node_modules_angular_material_icon_typings_index_ngfactory__["a" /* RenderType_MatIcon */])), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](10, 638976, null, 0, __WEBPACK_IMPORTED_MODULE_3__angular_material_icon__["b" /* MatIcon */], [__WEBPACK_IMPORTED_MODULE_1__angular_core__["ElementRef"], __WEBPACK_IMPORTED_MODULE_3__angular_material_icon__["d" /* MatIconRegistry */], [8, null]], null, null), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n    "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n    "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n\n    "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n    "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n    "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n\n    "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n    "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵeld"](18, 0, null, null, 1, "div", [["class", "list-card-name"]], null, null, null, null, null)), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](19, null, ["", ""])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n    "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n\n    "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n\n        "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n        "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n        "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n\n        "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n        "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n        "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n\n    "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n\n    "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n    "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n    "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n\n"])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n"])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n\n"])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n"])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n\n    "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n\n        "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n        "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n        "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n\n        "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n        "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n        "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n\n    "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n\n    "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n\n        "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n        "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n        "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n\n        "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n        "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n        "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n\n    "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n"])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n"])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n"]))], function (_ck, _v) { _ck(_v, 10, 0); }, function (_ck, _v) { var _co = _v.component; var currVal_0 = _co.card.name; _ck(_v, 19, 0, currVal_0); }); }
function View_MerchantFeaturedBoardCardComponent_Host_0(_l) { return __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵvid"](0, [(_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵeld"](0, 0, null, null, 1, "fuse-scrumboard-board-card", [], null, null, null, View_MerchantFeaturedBoardCardComponent_0, RenderType_MerchantFeaturedBoardCardComponent)), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](1, 114688, null, 0, __WEBPACK_IMPORTED_MODULE_4__card_component__["a" /* MerchantFeaturedBoardCardComponent */], [__WEBPACK_IMPORTED_MODULE_5__angular_router__["a" /* ActivatedRoute */]], null, null)], function (_ck, _v) { _ck(_v, 1, 0); }, null); }
var MerchantFeaturedBoardCardComponentNgFactory = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵccf"]("fuse-scrumboard-board-card", __WEBPACK_IMPORTED_MODULE_4__card_component__["a" /* MerchantFeaturedBoardCardComponent */], View_MerchantFeaturedBoardCardComponent_Host_0, { cardId: "cardId" }, {}, []);



/***/ }),

/***/ "./src/app/main/content/pages/merchant-featured/merchant-featured-board/list/card/card.component.scss.ngstyle.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return styles; });
/**
 * @fileoverview This file was generated by the Angular template compiler. Do not edit.
 *
 * @suppress {suspiciousCode,uselessCode,missingProperties,missingOverride,checkTypes}
 * tslint:disable
 */ 
var styles = ["\n\n\n\n.scrumboard-board-card {\n  position: relative;\n  display: block;\n  width: 100%;\n  margin: 16px 0 !important;\n  background-color: white !important;\n  color: #000;\n  border-radius: 2px;\n  -webkit-transition: -webkit-box-shadow 150ms ease;\n  transition: -webkit-box-shadow 150ms ease;\n  transition: box-shadow 150ms ease;\n  transition: box-shadow 150ms ease, -webkit-box-shadow 150ms ease;\n  cursor: pointer; }\n.scrumboard-board-card .list-card-sort-handle {\n    display: none;\n    position: absolute;\n    top: 0;\n    right: 0;\n    padding: 4px;\n    background: rgba(255, 255, 255, 0.8); }\n.scrumboard-board-card .list-card-details {\n    padding: 16px 16px 10px 16px; }\n.scrumboard-board-card .list-card-details .list-card-labels {\n      margin-bottom: 6px; }\n.scrumboard-board-card .list-card-details .list-card-labels .list-card-label {\n        width: 32px;\n        height: 6px;\n        border-radius: 6px;\n        margin: 0 6px 6px 0; }\n.scrumboard-board-card .list-card-details .list-card-name {\n      font-size: 14px;\n      font-weight: 500;\n      margin-bottom: 12px; }\n.scrumboard-board-card .list-card-details .list-card-badges {\n      margin-bottom: 12px; }\n.scrumboard-board-card .list-card-details .list-card-badges .badge {\n        margin-right: 8px;\n        padding: 4px 8px;\n        border-radius: 2px;\n        background-color: rgba(0, 0, 0, 0.4);\n        color: #FFFFFF; }\n.scrumboard-board-card .list-card-details .list-card-badges .badge mat-icon {\n          margin-right: 4px; }\n.scrumboard-board-card .list-card-details .list-card-badges .badge.due-date {\n          background-color: #4caf50; }\n.scrumboard-board-card .list-card-details .list-card-badges .badge.due-date.overdue {\n            background-color: #f44336; }\n.scrumboard-board-card .list-card-details .list-card-badges .badge.check-items.completed {\n          background-color: #4caf50; }\n.scrumboard-board-card .list-card-details .list-card-members {\n      margin-bottom: 12px; }\n.scrumboard-board-card .list-card-details .list-card-members .list-card-member {\n        margin-right: 8px; }\n.scrumboard-board-card .list-card-details .list-card-members .list-card-member .list-card-member-avatar {\n          border-radius: 50%;\n          width: 32px;\n          height: 32px; }\n.scrumboard-board-card .list-card-footer {\n    border-top: 1px solid rgba(0, 0, 0, 0.12);\n    padding: 0 16px; }\n.scrumboard-board-card .list-card-footer .list-card-footer-item {\n      height: 48px;\n      margin-right: 12px;\n      color: rgba(0, 0, 0, 0.66); }\n.scrumboard-board-card .list-card-footer .list-card-footer-item .value {\n        padding-left: 8px; }\n.scrumboard-board-card .list-card-footer .list-card-footer-item:last-of-type {\n        margin-right: 0; }\n.scrumboard-board-card .ngx-dnd-content {\n    -webkit-user-select: none;\n       -moz-user-select: none;\n        -ms-user-select: none;\n            user-select: none; }\n.scrumboard-board-card.gu-mirror {\n    position: fixed !important;\n    margin: 0 !important;\n    z-index: 9999 !important;\n    opacity: 0.8;\n    -ms-filter: \"progid:DXImageTransform.Microsoft.Alpha(Opacity=80)\";\n    filter: alpha(opacity=80);\n    -webkit-box-shadow: 0px 4px 5px -2px rgba(0, 0, 0, 0.2), 0px 7px 10px 1px rgba(0, 0, 0, 0.14), 0px 2px 16px 1px rgba(0, 0, 0, 0.12);\n            box-shadow: 0px 4px 5px -2px rgba(0, 0, 0, 0.2), 0px 7px 10px 1px rgba(0, 0, 0, 0.14), 0px 2px 16px 1px rgba(0, 0, 0, 0.12); }\n.scrumboard-board-card.gu-hide {\n    display: none !important; }\n.scrumboard-board-card.gu-unselectable {\n    -webkit-user-select: none !important;\n    -moz-user-select: none !important;\n    -ms-user-select: none !important;\n    user-select: none !important; }\n.scrumboard-board-card.gu-transit {\n    opacity: 0.2;\n    -ms-filter: \"progid:DXImageTransform.Microsoft.Alpha(Opacity=20)\";\n    filter: alpha(opacity=20); }\n"];



/***/ }),

/***/ "./src/app/main/content/pages/merchant-featured/merchant-featured-board/list/card/card.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MerchantFeaturedBoardCardComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_moment__ = __webpack_require__("./node_modules/moment/moment.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_moment__);



var MerchantFeaturedBoardCardComponent = /** @class */ (function () {
    function MerchantFeaturedBoardCardComponent(route) {
        this.route = route;
    }
    MerchantFeaturedBoardCardComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.board = this.route.snapshot.data.board;
        this.card = this.board.cards.filter(function (card) {
            return _this.cardId === card.id;
        })[0];
    };
    /**
     * Is the card overdue?
     *
     * @param cardDate
     * @returns {boolean}
     */
    MerchantFeaturedBoardCardComponent.prototype.isOverdue = function (cardDate) {
        return __WEBPACK_IMPORTED_MODULE_2_moment__() > __WEBPACK_IMPORTED_MODULE_2_moment__(new Date(cardDate));
    };
    return MerchantFeaturedBoardCardComponent;
}());



/***/ }),

/***/ "./src/app/main/content/pages/merchant-featured/merchant-featured-board/list/edit-list-name/edit-list-name.component.ngfactory.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RenderType_MerchantFeaturedBoardEditListNameComponent; });
/* harmony export (immutable) */ __webpack_exports__["b"] = View_MerchantFeaturedBoardEditListNameComponent_0;
/* unused harmony export View_MerchantFeaturedBoardEditListNameComponent_Host_0 */
/* unused harmony export MerchantFeaturedBoardEditListNameComponentNgFactory */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__edit_list_name_component_scss_shim_ngstyle__ = __webpack_require__("./src/app/main/content/pages/merchant-featured/merchant-featured-board/list/edit-list-name/edit-list-name.component.scss.shim.ngstyle.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_flex_layout__ = __webpack_require__("./node_modules/@angular/flex-layout/esm5/flex-layout.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__("./node_modules/@angular/forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_common__ = __webpack_require__("./node_modules/@angular/common/esm5/common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__edit_list_name_component__ = __webpack_require__("./src/app/main/content/pages/merchant-featured/merchant-featured-board/list/edit-list-name/edit-list-name.component.ts");
/**
 * @fileoverview This file was generated by the Angular template compiler. Do not edit.
 *
 * @suppress {suspiciousCode,uselessCode,missingProperties,missingOverride,checkTypes}
 * tslint:disable
 */ 






var styles_MerchantFeaturedBoardEditListNameComponent = [__WEBPACK_IMPORTED_MODULE_0__edit_list_name_component_scss_shim_ngstyle__["a" /* styles */]];
var RenderType_MerchantFeaturedBoardEditListNameComponent = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵcrt"]({ encapsulation: 0, styles: styles_MerchantFeaturedBoardEditListNameComponent, data: {} });

function View_MerchantFeaturedBoardEditListNameComponent_1(_l) { return __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵvid"](0, [(_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵeld"](0, 0, null, null, 2, "div", [["class", "list-header-name"], ["fxFlex", "1 0 auto"]], null, null, null, null, null)), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](1, 737280, null, 0, __WEBPACK_IMPORTED_MODULE_2__angular_flex_layout__["e" /* FlexDirective */], [__WEBPACK_IMPORTED_MODULE_2__angular_flex_layout__["m" /* MediaMonitor */], __WEBPACK_IMPORTED_MODULE_1__angular_core__["ElementRef"], [3, __WEBPACK_IMPORTED_MODULE_2__angular_flex_layout__["j" /* LayoutDirective */]], __WEBPACK_IMPORTED_MODULE_2__angular_flex_layout__["t" /* StyleUtils */]], { flex: [0, "flex"] }, null), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](2, null, ["\n    ", "\n"]))], function (_ck, _v) { var currVal_0 = "1 0 auto"; _ck(_v, 1, 0, currVal_0); }, function (_ck, _v) { var _co = _v.component; var currVal_1 = _co.list.name; _ck(_v, 2, 0, currVal_1); }); }
function View_MerchantFeaturedBoardEditListNameComponent_2(_l) { return __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵvid"](0, [(_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵeld"](0, 0, null, null, 9, "form", [["class", "list-header-name-form"], ["fxFlex", "1 0 auto"], ["fxLayout", "row"], ["novalidate", ""]], [[2, "ng-untouched", null], [2, "ng-touched", null], [2, "ng-pristine", null], [2, "ng-dirty", null], [2, "ng-valid", null], [2, "ng-invalid", null], [2, "ng-pending", null]], [[null, "ngSubmit"], [null, "submit"], [null, "reset"]], function (_v, en, $event) { var ad = true; var _co = _v.component; if (("submit" === en)) {
        var pd_0 = (__WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 2).onSubmit($event) !== false);
        ad = (pd_0 && ad);
    } if (("reset" === en)) {
        var pd_1 = (__WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 2).onReset() !== false);
        ad = (pd_1 && ad);
    } if (("ngSubmit" === en)) {
        var pd_2 = (_co.onFormSubmit() !== false);
        ad = (pd_2 && ad);
    } return ad; }, null, null)), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](1, 16384, null, 0, __WEBPACK_IMPORTED_MODULE_3__angular_forms__["F" /* ɵbf */], [], null, null), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](2, 540672, null, 0, __WEBPACK_IMPORTED_MODULE_3__angular_forms__["n" /* FormGroupDirective */], [[8, null], [8, null]], { form: [0, "form"] }, { ngSubmit: "ngSubmit" }), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵprd"](2048, null, __WEBPACK_IMPORTED_MODULE_3__angular_forms__["d" /* ControlContainer */], null, [__WEBPACK_IMPORTED_MODULE_3__angular_forms__["n" /* FormGroupDirective */]]), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](4, 16384, null, 0, __WEBPACK_IMPORTED_MODULE_3__angular_forms__["w" /* NgControlStatusGroup */], [__WEBPACK_IMPORTED_MODULE_3__angular_forms__["d" /* ControlContainer */]], null, null), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](5, 737280, null, 0, __WEBPACK_IMPORTED_MODULE_2__angular_flex_layout__["j" /* LayoutDirective */], [__WEBPACK_IMPORTED_MODULE_2__angular_flex_layout__["m" /* MediaMonitor */], __WEBPACK_IMPORTED_MODULE_1__angular_core__["ElementRef"], __WEBPACK_IMPORTED_MODULE_2__angular_flex_layout__["t" /* StyleUtils */]], { layout: [0, "layout"] }, null), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](6, 737280, null, 0, __WEBPACK_IMPORTED_MODULE_2__angular_flex_layout__["e" /* FlexDirective */], [__WEBPACK_IMPORTED_MODULE_2__angular_flex_layout__["m" /* MediaMonitor */], __WEBPACK_IMPORTED_MODULE_1__angular_core__["ElementRef"], [3, __WEBPACK_IMPORTED_MODULE_2__angular_flex_layout__["j" /* LayoutDirective */]], __WEBPACK_IMPORTED_MODULE_2__angular_flex_layout__["t" /* StyleUtils */]], { flex: [0, "flex"] }, null), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n\n    "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n\n    "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n"]))], function (_ck, _v) { var _co = _v.component; var currVal_7 = _co.form; _ck(_v, 2, 0, currVal_7); var currVal_8 = "row"; _ck(_v, 5, 0, currVal_8); var currVal_9 = "1 0 auto"; _ck(_v, 6, 0, currVal_9); }, function (_ck, _v) { var currVal_0 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 4).ngClassUntouched; var currVal_1 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 4).ngClassTouched; var currVal_2 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 4).ngClassPristine; var currVal_3 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 4).ngClassDirty; var currVal_4 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 4).ngClassValid; var currVal_5 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 4).ngClassInvalid; var currVal_6 = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 4).ngClassPending; _ck(_v, 0, 0, currVal_0, currVal_1, currVal_2, currVal_3, currVal_4, currVal_5, currVal_6); }); }
function View_MerchantFeaturedBoardEditListNameComponent_0(_l) { return __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵvid"](0, [__WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵqud"](402653184, 1, { nameInputField: 0 }), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n\n"])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵand"](16777216, null, null, 1, null, View_MerchantFeaturedBoardEditListNameComponent_1)), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](3, 16384, null, 0, __WEBPACK_IMPORTED_MODULE_4__angular_common__["NgIf"], [__WEBPACK_IMPORTED_MODULE_1__angular_core__["ViewContainerRef"], __WEBPACK_IMPORTED_MODULE_1__angular_core__["TemplateRef"]], { ngIf: [0, "ngIf"] }, null), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n\n"])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵand"](16777216, null, null, 1, null, View_MerchantFeaturedBoardEditListNameComponent_2)), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](6, 16384, null, 0, __WEBPACK_IMPORTED_MODULE_4__angular_common__["NgIf"], [__WEBPACK_IMPORTED_MODULE_1__angular_core__["ViewContainerRef"], __WEBPACK_IMPORTED_MODULE_1__angular_core__["TemplateRef"]], { ngIf: [0, "ngIf"] }, null), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n"]))], function (_ck, _v) { var _co = _v.component; var currVal_0 = !_co.formActive; _ck(_v, 3, 0, currVal_0); var currVal_1 = _co.formActive; _ck(_v, 6, 0, currVal_1); }, null); }
function View_MerchantFeaturedBoardEditListNameComponent_Host_0(_l) { return __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵvid"](0, [(_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵeld"](0, 0, null, null, 1, "fuse-scrumboard-board-edit-list-name", [], null, null, null, View_MerchantFeaturedBoardEditListNameComponent_0, RenderType_MerchantFeaturedBoardEditListNameComponent)), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](1, 49152, null, 0, __WEBPACK_IMPORTED_MODULE_5__edit_list_name_component__["a" /* MerchantFeaturedBoardEditListNameComponent */], [__WEBPACK_IMPORTED_MODULE_3__angular_forms__["i" /* FormBuilder */]], null, null)], null, null); }
var MerchantFeaturedBoardEditListNameComponentNgFactory = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵccf"]("fuse-scrumboard-board-edit-list-name", __WEBPACK_IMPORTED_MODULE_5__edit_list_name_component__["a" /* MerchantFeaturedBoardEditListNameComponent */], View_MerchantFeaturedBoardEditListNameComponent_Host_0, { list: "list" }, { onNameChanged: "onNameChanged" }, []);



/***/ }),

/***/ "./src/app/main/content/pages/merchant-featured/merchant-featured-board/list/edit-list-name/edit-list-name.component.scss.shim.ngstyle.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return styles; });
/**
 * @fileoverview This file was generated by the Angular template compiler. Do not edit.
 *
 * @suppress {suspiciousCode,uselessCode,missingProperties,missingOverride,checkTypes}
 * tslint:disable
 */ 
var styles = ["[_nghost-%COMP%]   .list-header-name[_ngcontent-%COMP%] {\n  text-overflow: ellipsis;\n  overflow: hidden;\n  font-size: 15px;\n  font-weight: 500;\n  cursor: pointer; }"];



/***/ }),

/***/ "./src/app/main/content/pages/merchant-featured/merchant-featured-board/list/edit-list-name/edit-list-name.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MerchantFeaturedBoardEditListNameComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__("./node_modules/@angular/forms/esm5/forms.js");


var MerchantFeaturedBoardEditListNameComponent = /** @class */ (function () {
    function MerchantFeaturedBoardEditListNameComponent(formBuilder) {
        this.formBuilder = formBuilder;
        this.formActive = false;
        this.onNameChanged = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
    }
    MerchantFeaturedBoardEditListNameComponent.prototype.openForm = function () {
        this.form = this.formBuilder.group({
            name: [this.list.name]
        });
        this.formActive = true;
        this.focusNameField();
    };
    MerchantFeaturedBoardEditListNameComponent.prototype.closeForm = function () {
        this.formActive = false;
    };
    MerchantFeaturedBoardEditListNameComponent.prototype.focusNameField = function () {
        var _this = this;
        setTimeout(function () {
            _this.nameInputField.nativeElement.focus();
        });
    };
    MerchantFeaturedBoardEditListNameComponent.prototype.onFormSubmit = function () {
        if (this.form.valid) {
            this.list.name = this.form.getRawValue().name;
            this.onNameChanged.next(this.list.name);
            this.formActive = false;
        }
    };
    return MerchantFeaturedBoardEditListNameComponent;
}());



/***/ }),

/***/ "./src/app/main/content/pages/merchant-featured/merchant-featured-board/list/list.component.ngfactory.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RenderType_MerchantFeaturedBoardListComponent; });
/* harmony export (immutable) */ __webpack_exports__["b"] = View_MerchantFeaturedBoardListComponent_0;
/* unused harmony export View_MerchantFeaturedBoardListComponent_Host_0 */
/* unused harmony export MerchantFeaturedBoardListComponentNgFactory */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__list_component_scss_ngstyle__ = __webpack_require__("./src/app/main/content/pages/merchant-featured/merchant-featured-board/list/list.component.scss.ngstyle.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__card_card_component_ngfactory__ = __webpack_require__("./src/app/main/content/pages/merchant-featured/merchant-featured-board/list/card/card.component.ngfactory.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__swimlane_ngx_dnd_release_directives_ngx_draggable_directive__ = __webpack_require__("./node_modules/@swimlane/ngx-dnd/release/directives/ngx-draggable.directive.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__swimlane_ngx_dnd_release_services_drake_store_service__ = __webpack_require__("./node_modules/@swimlane/ngx-dnd/release/services/drake-store.service.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__swimlane_ngx_dnd_release_directives_ngx_droppable_directive__ = __webpack_require__("./node_modules/@swimlane/ngx-dnd/release/directives/ngx-droppable.directive.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__card_card_component__ = __webpack_require__("./src/app/main/content/pages/merchant-featured/merchant-featured-board/list/card/card.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__add_card_add_card_component_ngfactory__ = __webpack_require__("./src/app/main/content/pages/merchant-featured/merchant-featured-board/list/add-card/add-card.component.ngfactory.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__add_card_add_card_component__ = __webpack_require__("./src/app/main/content/pages/merchant-featured/merchant-featured-board/list/add-card/add-card.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__angular_forms__ = __webpack_require__("./node_modules/@angular/forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__merchant_featured_service__ = __webpack_require__("./src/app/main/content/pages/merchant-featured/merchant-featured.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__angular_flex_layout__ = __webpack_require__("./node_modules/@angular/flex-layout/esm5/flex-layout.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__edit_list_name_edit_list_name_component_ngfactory__ = __webpack_require__("./src/app/main/content/pages/merchant-featured/merchant-featured-board/list/edit-list-name/edit-list-name.component.ngfactory.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__edit_list_name_edit_list_name_component__ = __webpack_require__("./src/app/main/content/pages/merchant-featured/merchant-featured-board/list/edit-list-name/edit-list-name.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__fuse_directives_fuse_perfect_scrollbar_fuse_perfect_scrollbar_directive__ = __webpack_require__("./src/@fuse/directives/fuse-perfect-scrollbar/fuse-perfect-scrollbar.directive.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__fuse_services_config_service__ = __webpack_require__("./src/@fuse/services/config.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__angular_cdk_platform__ = __webpack_require__("./node_modules/@angular/cdk/esm5/platform.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__angular_common__ = __webpack_require__("./node_modules/@angular/common/esm5/common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__list_component__ = __webpack_require__("./src/app/main/content/pages/merchant-featured/merchant-featured-board/list/list.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__angular_material_dialog__ = __webpack_require__("./node_modules/@angular/material/esm5/dialog.es5.js");
/**
 * @fileoverview This file was generated by the Angular template compiler. Do not edit.
 *
 * @suppress {suspiciousCode,uselessCode,missingProperties,missingOverride,checkTypes}
 * tslint:disable
 */ 





















var styles_MerchantFeaturedBoardListComponent = [__WEBPACK_IMPORTED_MODULE_0__list_component_scss_ngstyle__["a" /* styles */]];
var RenderType_MerchantFeaturedBoardListComponent = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵcrt"]({ encapsulation: 2, styles: styles_MerchantFeaturedBoardListComponent, data: {} });

function View_MerchantFeaturedBoardListComponent_1(_l) { return __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵvid"](0, [(_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵeld"](0, 0, null, null, 3, "fuse-scrumboard-board-card", [["class", "scrumboard-board-card mat-elevation-z2 ngx-dnd-item"], ["ngxDraggable", ""]], null, [[null, "click"], [null, "touchmove"], [null, "touchstart"], [null, "touchend"]], function (_v, en, $event) { var ad = true; var _co = _v.component; if (("touchmove" === en)) {
        var pd_0 = (__WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 1).onMove($event) !== false);
        ad = (pd_0 && ad);
    } if (("touchstart" === en)) {
        var pd_1 = (__WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 1).onDown($event) !== false);
        ad = (pd_1 && ad);
    } if (("touchend" === en)) {
        var pd_2 = (__WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 1).onUp($event) !== false);
        ad = (pd_2 && ad);
    } if (("click" === en)) {
        var pd_3 = (_co.openCardDialog(_v.context.$implicit) !== false);
        ad = (pd_3 && ad);
    } return ad; }, __WEBPACK_IMPORTED_MODULE_2__card_card_component_ngfactory__["b" /* View_MerchantFeaturedBoardCardComponent_0 */], __WEBPACK_IMPORTED_MODULE_2__card_card_component_ngfactory__["a" /* RenderType_MerchantFeaturedBoardCardComponent */])), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](1, 475136, null, 0, __WEBPACK_IMPORTED_MODULE_3__swimlane_ngx_dnd_release_directives_ngx_draggable_directive__["a" /* DraggableDirective */], [__WEBPACK_IMPORTED_MODULE_1__angular_core__["ElementRef"], __WEBPACK_IMPORTED_MODULE_4__swimlane_ngx_dnd_release_services_drake_store_service__["a" /* DrakeStoreService */], __WEBPACK_IMPORTED_MODULE_5__swimlane_ngx_dnd_release_directives_ngx_droppable_directive__["a" /* DroppableDirective */]], { ngxDraggable: [0, "ngxDraggable"], model: [1, "model"] }, null), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](2, 114688, null, 0, __WEBPACK_IMPORTED_MODULE_6__card_card_component__["a" /* MerchantFeaturedBoardCardComponent */], [__WEBPACK_IMPORTED_MODULE_7__angular_router__["a" /* ActivatedRoute */]], { cardId: [0, "cardId"] }, null), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n            "]))], function (_ck, _v) { var currVal_0 = ""; var currVal_1 = _v.context.$implicit; _ck(_v, 1, 0, currVal_0, currVal_1); var currVal_2 = _v.context.$implicit; _ck(_v, 2, 0, currVal_2); }, null); }
function View_MerchantFeaturedBoardListComponent_2(_l) { return __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵvid"](0, [(_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵeld"](0, 0, null, null, 5, "div", [["class", "list-footer"]], null, null, null, null, null)), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n        "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵeld"](2, 0, null, null, 2, "fuse-scrumboard-board-add-card", [], null, [[null, "onCardAdd"]], function (_v, en, $event) { var ad = true; var _co = _v.component; if (("onCardAdd" === en)) {
        var pd_0 = (_co.onCardAdd($event) !== false);
        ad = (pd_0 && ad);
    } return ad; }, __WEBPACK_IMPORTED_MODULE_8__add_card_add_card_component_ngfactory__["b" /* View_MerchantFeaturedBoardAddCardComponent_0 */], __WEBPACK_IMPORTED_MODULE_8__add_card_add_card_component_ngfactory__["a" /* RenderType_MerchantFeaturedBoardAddCardComponent */])), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](3, 49152, null, 0, __WEBPACK_IMPORTED_MODULE_9__add_card_add_card_component__["a" /* MerchantFeaturedBoardAddCardComponent */], [__WEBPACK_IMPORTED_MODULE_10__angular_forms__["i" /* FormBuilder */], __WEBPACK_IMPORTED_MODULE_11__merchant_featured_service__["b" /* MerchantFeaturedService */], __WEBPACK_IMPORTED_MODULE_7__angular_router__["o" /* Router */]], null, { onCardAdd: "onCardAdd" }), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n        "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n    "]))], null, null); }
function View_MerchantFeaturedBoardListComponent_0(_l) { return __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵvid"](0, [__WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵqud"](402653184, 1, { listScroll: 0 }), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵeld"](1, 0, null, null, 35, "div", [["class", "list mat-elevation-z1"], ["fxLayout", "column"]], null, null, null, null, null)), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](2, 737280, null, 0, __WEBPACK_IMPORTED_MODULE_12__angular_flex_layout__["j" /* LayoutDirective */], [__WEBPACK_IMPORTED_MODULE_12__angular_flex_layout__["m" /* MediaMonitor */], __WEBPACK_IMPORTED_MODULE_1__angular_core__["ElementRef"], __WEBPACK_IMPORTED_MODULE_12__angular_flex_layout__["t" /* StyleUtils */]], { layout: [0, "layout"] }, null), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n\n    "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n    "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵeld"](5, 0, null, null, 10, "div", [["class", "list-header"], ["fxFlex", ""], ["fxLayout", "row"], ["fxLayoutAlign", "space-between center"]], null, null, null, null, null)), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](6, 737280, null, 0, __WEBPACK_IMPORTED_MODULE_12__angular_flex_layout__["j" /* LayoutDirective */], [__WEBPACK_IMPORTED_MODULE_12__angular_flex_layout__["m" /* MediaMonitor */], __WEBPACK_IMPORTED_MODULE_1__angular_core__["ElementRef"], __WEBPACK_IMPORTED_MODULE_12__angular_flex_layout__["t" /* StyleUtils */]], { layout: [0, "layout"] }, null), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](7, 737280, null, 0, __WEBPACK_IMPORTED_MODULE_12__angular_flex_layout__["i" /* LayoutAlignDirective */], [__WEBPACK_IMPORTED_MODULE_12__angular_flex_layout__["m" /* MediaMonitor */], __WEBPACK_IMPORTED_MODULE_1__angular_core__["ElementRef"], [2, __WEBPACK_IMPORTED_MODULE_12__angular_flex_layout__["j" /* LayoutDirective */]], __WEBPACK_IMPORTED_MODULE_12__angular_flex_layout__["t" /* StyleUtils */]], { align: [0, "align"] }, null), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](8, 737280, null, 0, __WEBPACK_IMPORTED_MODULE_12__angular_flex_layout__["e" /* FlexDirective */], [__WEBPACK_IMPORTED_MODULE_12__angular_flex_layout__["m" /* MediaMonitor */], __WEBPACK_IMPORTED_MODULE_1__angular_core__["ElementRef"], [3, __WEBPACK_IMPORTED_MODULE_12__angular_flex_layout__["j" /* LayoutDirective */]], __WEBPACK_IMPORTED_MODULE_12__angular_flex_layout__["t" /* StyleUtils */]], { flex: [0, "flex"] }, null), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n\n        "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵeld"](10, 0, null, null, 3, "fuse-scrumboard-board-edit-list-name", [["fxFlex", "1 0 auto"]], null, [[null, "onNameChanged"]], function (_v, en, $event) { var ad = true; var _co = _v.component; if (("onNameChanged" === en)) {
        var pd_0 = (_co.onListNameChanged($event) !== false);
        ad = (pd_0 && ad);
    } return ad; }, __WEBPACK_IMPORTED_MODULE_13__edit_list_name_edit_list_name_component_ngfactory__["b" /* View_MerchantFeaturedBoardEditListNameComponent_0 */], __WEBPACK_IMPORTED_MODULE_13__edit_list_name_edit_list_name_component_ngfactory__["a" /* RenderType_MerchantFeaturedBoardEditListNameComponent */])), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](11, 737280, null, 0, __WEBPACK_IMPORTED_MODULE_12__angular_flex_layout__["e" /* FlexDirective */], [__WEBPACK_IMPORTED_MODULE_12__angular_flex_layout__["m" /* MediaMonitor */], __WEBPACK_IMPORTED_MODULE_1__angular_core__["ElementRef"], [3, __WEBPACK_IMPORTED_MODULE_12__angular_flex_layout__["j" /* LayoutDirective */]], __WEBPACK_IMPORTED_MODULE_12__angular_flex_layout__["t" /* StyleUtils */]], { flex: [0, "flex"] }, null), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](12, 49152, null, 0, __WEBPACK_IMPORTED_MODULE_14__edit_list_name_edit_list_name_component__["a" /* MerchantFeaturedBoardEditListNameComponent */], [__WEBPACK_IMPORTED_MODULE_10__angular_forms__["i" /* FormBuilder */]], { list: [0, "list"] }, { onNameChanged: "onNameChanged" }), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n        "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n\n        "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n\n    "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n    "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n\n    "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n    "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵeld"](19, 0, null, null, 10, "div", [["class", "list-content"], ["fxLayout", "column"]], null, null, null, null, null)), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](20, 737280, null, 0, __WEBPACK_IMPORTED_MODULE_12__angular_flex_layout__["j" /* LayoutDirective */], [__WEBPACK_IMPORTED_MODULE_12__angular_flex_layout__["m" /* MediaMonitor */], __WEBPACK_IMPORTED_MODULE_1__angular_core__["ElementRef"], __WEBPACK_IMPORTED_MODULE_12__angular_flex_layout__["t" /* StyleUtils */]], { layout: [0, "layout"] }, null), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n\n        "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵeld"](22, 0, [["listScroll", 1]], null, 6, "div", [["class", "list-cards ngx-dnd-container"], ["fusePerfectScrollbar", ""], ["ngxDroppable", "card"]], null, [[null, "out"], ["document", "click"]], function (_v, en, $event) { var ad = true; var _co = _v.component; if (("document:click" === en)) {
        var pd_0 = (__WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 24).documentClick($event) !== false);
        ad = (pd_0 && ad);
    } if (("out" === en)) {
        var pd_1 = (_co.onDrop($event) !== false);
        ad = (pd_1 && ad);
    } return ad; }, null, null)), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](23, 4407296, null, 0, __WEBPACK_IMPORTED_MODULE_5__swimlane_ngx_dnd_release_directives_ngx_droppable_directive__["a" /* DroppableDirective */], [__WEBPACK_IMPORTED_MODULE_1__angular_core__["ElementRef"], __WEBPACK_IMPORTED_MODULE_1__angular_core__["Renderer"], __WEBPACK_IMPORTED_MODULE_4__swimlane_ngx_dnd_release_services_drake_store_service__["a" /* DrakeStoreService */]], { model: [0, "model"], ngxDroppable: [1, "ngxDroppable"] }, { out: "out" }), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](24, 4407296, [[1, 4]], 0, __WEBPACK_IMPORTED_MODULE_15__fuse_directives_fuse_perfect_scrollbar_fuse_perfect_scrollbar_directive__["a" /* FusePerfectScrollbarDirective */], [__WEBPACK_IMPORTED_MODULE_1__angular_core__["ElementRef"], __WEBPACK_IMPORTED_MODULE_16__fuse_services_config_service__["b" /* FuseConfigService */], __WEBPACK_IMPORTED_MODULE_17__angular_cdk_platform__["a" /* Platform */]], null, null), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n            "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵand"](16777216, null, null, 1, null, View_MerchantFeaturedBoardListComponent_1)), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](27, 802816, null, 0, __WEBPACK_IMPORTED_MODULE_18__angular_common__["NgForOf"], [__WEBPACK_IMPORTED_MODULE_1__angular_core__["ViewContainerRef"], __WEBPACK_IMPORTED_MODULE_1__angular_core__["TemplateRef"], __WEBPACK_IMPORTED_MODULE_1__angular_core__["IterableDiffers"]], { ngForOf: [0, "ngForOf"] }, null), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n        "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n    "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n    "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n\n    "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n    "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵand"](16777216, null, null, 1, null, View_MerchantFeaturedBoardListComponent_2)), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](34, 16384, null, 0, __WEBPACK_IMPORTED_MODULE_18__angular_common__["NgIf"], [__WEBPACK_IMPORTED_MODULE_1__angular_core__["ViewContainerRef"], __WEBPACK_IMPORTED_MODULE_1__angular_core__["TemplateRef"]], { ngIf: [0, "ngIf"] }, null), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n    "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n\n"])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n"]))], function (_ck, _v) { var _co = _v.component; var currVal_0 = "column"; _ck(_v, 2, 0, currVal_0); var currVal_1 = "row"; _ck(_v, 6, 0, currVal_1); var currVal_2 = "space-between center"; _ck(_v, 7, 0, currVal_2); var currVal_3 = ""; _ck(_v, 8, 0, currVal_3); var currVal_4 = "1 0 auto"; _ck(_v, 11, 0, currVal_4); var currVal_5 = _co.list; _ck(_v, 12, 0, currVal_5); var currVal_6 = "column"; _ck(_v, 20, 0, currVal_6); var currVal_7 = _co.list.idCards; var currVal_8 = "card"; _ck(_v, 23, 0, currVal_7, currVal_8); _ck(_v, 24, 0); var currVal_9 = _co.list.idCards; _ck(_v, 27, 0, currVal_9); var currVal_10 = (_co.list.id == "56027cf5a2ca3839a5d36103"); _ck(_v, 34, 0, currVal_10); }, null); }
function View_MerchantFeaturedBoardListComponent_Host_0(_l) { return __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵvid"](0, [(_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵeld"](0, 0, null, null, 1, "fuse-scrumboard-board-list", [], null, null, null, View_MerchantFeaturedBoardListComponent_0, RenderType_MerchantFeaturedBoardListComponent)), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](1, 245760, null, 0, __WEBPACK_IMPORTED_MODULE_19__list_component__["a" /* MerchantFeaturedBoardListComponent */], [__WEBPACK_IMPORTED_MODULE_7__angular_router__["a" /* ActivatedRoute */], __WEBPACK_IMPORTED_MODULE_11__merchant_featured_service__["b" /* MerchantFeaturedService */], __WEBPACK_IMPORTED_MODULE_20__angular_material_dialog__["e" /* MatDialog */]], null, null)], function (_ck, _v) { _ck(_v, 1, 0); }, null); }
var MerchantFeaturedBoardListComponentNgFactory = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵccf"]("fuse-scrumboard-board-list", __WEBPACK_IMPORTED_MODULE_19__list_component__["a" /* MerchantFeaturedBoardListComponent */], View_MerchantFeaturedBoardListComponent_Host_0, { list: "list" }, {}, []);



/***/ }),

/***/ "./src/app/main/content/pages/merchant-featured/merchant-featured-board/list/list.component.scss.ngstyle.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return styles; });
/**
 * @fileoverview This file was generated by the Angular template compiler. Do not edit.
 *
 * @suppress {suspiciousCode,uselessCode,missingProperties,missingOverride,checkTypes}
 * tslint:disable
 */ 
var styles = ["\n\n\n\n.scrumboard-board-list {\n  width: 320px;\n  min-width: 320px;\n  max-width: 320px;\n  padding-right: 24px !important;\n  height: 100%; }\n.scrumboard-board-list .list {\n    max-height: 100%;\n    background-color: #EEF0F2;\n    color: #000;\n    border-radius: 2px;\n    -webkit-transition: -webkit-box-shadow 150ms ease;\n    transition: -webkit-box-shadow 150ms ease;\n    transition: box-shadow 150ms ease;\n    transition: box-shadow 150ms ease, -webkit-box-shadow 150ms ease; }\n.scrumboard-board-list .list .list-header {\n      height: 64px;\n      min-height: 64px;\n      padding: 0 8px 0 16px;\n      border-bottom: 1px solid rgba(0, 0, 0, 0.12); }\n@media (max-width: 959px) {\n        .scrumboard-board-list .list .list-header {\n          height: 48px;\n          min-height: 48px; } }\n.scrumboard-board-list .list .list-content {\n      position: relative;\n      overflow: hidden;\n      overflow-y: auto;\n      min-height: 0; }\n.scrumboard-board-list .list .list-content .list-cards {\n        position: relative;\n        min-height: 32px;\n        padding: 0 16px;\n        background: #eeeeee !important; }\n.scrumboard-board-list .list .list-footer {\n      display: -webkit-box;\n      display: -ms-flexbox;\n      display: flex;\n      -webkit-box-orient: vertical;\n      -webkit-box-direction: normal;\n          -ms-flex-direction: column;\n              flex-direction: column;\n      -webkit-box-flex: 1;\n          -ms-flex: 1 0 auto;\n              flex: 1 0 auto;\n      min-height: 48px; }\n.scrumboard-board-list .ngx-dnd-content {\n    -webkit-user-select: none;\n       -moz-user-select: none;\n        -ms-user-select: none;\n            user-select: none; }\n.scrumboard-board-list.gu-mirror {\n    position: fixed !important;\n    margin: 0 !important;\n    z-index: 9999 !important;\n    opacity: 0.8;\n    -ms-filter: \"progid:DXImageTransform.Microsoft.Alpha(Opacity=80)\";\n    filter: alpha(opacity=80); }\n.scrumboard-board-list.gu-mirror > .list {\n      -webkit-box-shadow: 0px 4px 5px -2px rgba(0, 0, 0, 0.2), 0px 7px 10px 1px rgba(0, 0, 0, 0.14), 0px 2px 16px 1px rgba(0, 0, 0, 0.12);\n              box-shadow: 0px 4px 5px -2px rgba(0, 0, 0, 0.2), 0px 7px 10px 1px rgba(0, 0, 0, 0.14), 0px 2px 16px 1px rgba(0, 0, 0, 0.12); }\n.scrumboard-board-list.gu-hide {\n    display: none !important; }\n.scrumboard-board-list.gu-unselectable {\n    -webkit-user-select: none !important;\n    -moz-user-select: none !important;\n    -ms-user-select: none !important;\n    user-select: none !important; }\n.scrumboard-board-list.gu-transit {\n    opacity: 0.2;\n    -ms-filter: \"progid:DXImageTransform.Microsoft.Alpha(Opacity=20)\";\n    filter: alpha(opacity=20); }\n"];



/***/ }),

/***/ "./src/app/main/content/pages/merchant-featured/merchant-featured-board/list/list.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MerchantFeaturedBoardListComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_material__ = __webpack_require__("./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__fuse_components_confirm_dialog_confirm_dialog_component__ = __webpack_require__("./src/@fuse/components/confirm-dialog/confirm-dialog.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__fuse_directives_fuse_perfect_scrollbar_fuse_perfect_scrollbar_directive__ = __webpack_require__("./src/@fuse/directives/fuse-perfect-scrollbar/fuse-perfect-scrollbar.directive.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__card_model__ = __webpack_require__("./src/app/main/content/pages/merchant-featured/card.model.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__merchant_featured_service__ = __webpack_require__("./src/app/main/content/pages/merchant-featured/merchant-featured.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__dialogs_card_card_component__ = __webpack_require__("./src/app/main/content/pages/merchant-featured/merchant-featured-board/dialogs/card/card.component.ts");








var MerchantFeaturedBoardListComponent = /** @class */ (function () {
    function MerchantFeaturedBoardListComponent(route, merchantFeaturedService, dialog) {
        this.route = route;
        this.merchantFeaturedService = merchantFeaturedService;
        this.dialog = dialog;
        this.addButtonHidden = false;
    }
    MerchantFeaturedBoardListComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.onBoardChanged =
            this.merchantFeaturedService.onBoardChanged
                .subscribe(function (board) {
                debugger;
                _this.board = board;
            });
    };
    MerchantFeaturedBoardListComponent.prototype.ngOnDestroy = function () {
        this.onBoardChanged.unsubscribe();
    };
    MerchantFeaturedBoardListComponent.prototype.onListNameChanged = function (newListName) {
        this.list.name = newListName;
    };
    MerchantFeaturedBoardListComponent.prototype.onCardAdd = function (newCardData) {
        var _this = this;
        if (newCardData === null || newCardData === undefined) {
            return;
        }
        var card = {};
        card.id = newCardData.id;
        card.name = newCardData.name;
        card.affiliate = newCardData.affiliateNetwork.name;
        card.description = newCardData.description;
        card.idMembers = [];
        card.idMembers.push(newCardData.accountManager.id);
        var member = {};
        member.id = newCardData.accountManager.id;
        member.name = "usman";
        member.avatar = 'assets/images/avatars/alice.jpg';
        this.merchantFeaturedService.addCard(this.list.id, new __WEBPACK_IMPORTED_MODULE_5__card_model__["a" /* Card */](card), member);
        setTimeout(function () {
            _this.listScroll.scrollToBottom(0, 400);
        });
    };
    MerchantFeaturedBoardListComponent.prototype.removeList = function (listId) {
        var _this = this;
        this.confirmDialogRef = this.dialog.open(__WEBPACK_IMPORTED_MODULE_3__fuse_components_confirm_dialog_confirm_dialog_component__["a" /* FuseConfirmDialogComponent */], {
            disableClose: false
        });
        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete the list and it\'s all cards?';
        this.confirmDialogRef.afterClosed().subscribe(function (result) {
            if (result) {
                _this.merchantFeaturedService.removeList(listId);
            }
        });
    };
    MerchantFeaturedBoardListComponent.prototype.openCardDialog = function (cardId) {
        this.dialogRef = this.dialog.open(__WEBPACK_IMPORTED_MODULE_7__dialogs_card_card_component__["a" /* MerchantFeaturedCardDialogComponent */], {
            panelClass: 'scrumboard-card-dialog',
            data: {
                cardId: cardId,
                listId: this.list.id
            }
        });
        this.dialogRef.afterClosed()
            .subscribe(function (response) {
        });
    };
    MerchantFeaturedBoardListComponent.prototype.onDrop = function (ev) {
        this.merchantFeaturedService.updateBoard();
    };
    return MerchantFeaturedBoardListComponent;
}());



/***/ }),

/***/ "./src/app/main/content/pages/merchant-featured/merchant-featured-board/merchant-featured-board.component.ngfactory.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export RenderType_MerchantFeaturedBoardComponent */
/* unused harmony export View_MerchantFeaturedBoardComponent_0 */
/* unused harmony export View_MerchantFeaturedBoardComponent_Host_0 */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MerchantFeaturedBoardComponentNgFactory; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__merchant_featured_board_component_scss_shim_ngstyle__ = __webpack_require__("./src/app/main/content/pages/merchant-featured/merchant-featured-board/merchant-featured-board.component.scss.shim.ngstyle.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_flex_layout__ = __webpack_require__("./node_modules/@angular/flex-layout/esm5/flex-layout.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__node_modules_angular_material_icon_typings_index_ngfactory__ = __webpack_require__("./node_modules/@angular/material/icon/typings/index.ngfactory.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_material_icon__ = __webpack_require__("./node_modules/@angular/material/esm5/icon.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__list_list_component_ngfactory__ = __webpack_require__("./src/app/main/content/pages/merchant-featured/merchant-featured-board/list/list.component.ngfactory.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__swimlane_ngx_dnd_release_directives_ngx_draggable_directive__ = __webpack_require__("./node_modules/@swimlane/ngx-dnd/release/directives/ngx-draggable.directive.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__swimlane_ngx_dnd_release_services_drake_store_service__ = __webpack_require__("./node_modules/@swimlane/ngx-dnd/release/services/drake-store.service.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__swimlane_ngx_dnd_release_directives_ngx_droppable_directive__ = __webpack_require__("./node_modules/@swimlane/ngx-dnd/release/directives/ngx-droppable.directive.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__list_list_component__ = __webpack_require__("./src/app/main/content/pages/merchant-featured/merchant-featured-board/list/list.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__merchant_featured_service__ = __webpack_require__("./src/app/main/content/pages/merchant-featured/merchant-featured.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__angular_material_dialog__ = __webpack_require__("./node_modules/@angular/material/esm5/dialog.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__angular_common__ = __webpack_require__("./node_modules/@angular/common/esm5/common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__add_list_add_list_component_ngfactory__ = __webpack_require__("./src/app/main/content/pages/merchant-featured/merchant-featured-board/add-list/add-list.component.ngfactory.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__add_list_add_list_component__ = __webpack_require__("./src/app/main/content/pages/merchant-featured/merchant-featured-board/add-list/add-list.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__angular_forms__ = __webpack_require__("./node_modules/@angular/forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__node_modules_angular_material_sidenav_typings_index_ngfactory__ = __webpack_require__("./node_modules/@angular/material/sidenav/typings/index.ngfactory.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__angular_material_sidenav__ = __webpack_require__("./node_modules/@angular/material/esm5/sidenav.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__angular_cdk_bidi__ = __webpack_require__("./node_modules/@angular/cdk/esm5/bidi.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__fuse_directives_fuse_if_on_dom_fuse_if_on_dom_directive__ = __webpack_require__("./src/@fuse/directives/fuse-if-on-dom/fuse-if-on-dom.directive.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__merchant_featured_board_component__ = __webpack_require__("./src/app/main/content/pages/merchant-featured/merchant-featured-board/merchant-featured-board.component.ts");
/**
 * @fileoverview This file was generated by the Angular template compiler. Do not edit.
 *
 * @suppress {suspiciousCode,uselessCode,missingProperties,missingOverride,checkTypes}
 * tslint:disable
 */ 






















var styles_MerchantFeaturedBoardComponent = [__WEBPACK_IMPORTED_MODULE_0__merchant_featured_board_component_scss_shim_ngstyle__["a" /* styles */]];
var RenderType_MerchantFeaturedBoardComponent = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵcrt"]({ encapsulation: 0, styles: styles_MerchantFeaturedBoardComponent, data: { "animation": [{ type: 7, name: "animate", definitions: [{ type: 1, expr: "void => *", animation: [{ type: 10, animation: { type: 8, animation: [{ type: 6, styles: { opacity: "{{opacity}}", transform: "scale({{scale}}) translate3d({{x}}, {{y}}, {{z}})" }, offset: null }, { type: 4, styles: { type: 6, styles: "*", offset: null }, timings: "{{duration}} {{delay}} cubic-bezier(0.0, 0.0, 0.2, 1)" }], options: { params: { duration: "200ms", delay: "0ms", opacity: "0", scale: "1", x: "0", y: "0", z: "0" } } }, options: null }], options: null }], options: {} }, { type: 7, name: "animateStagger", definitions: [{ type: 0, name: "50", styles: { type: 6, styles: "*", offset: null }, options: undefined }, { type: 0, name: "100", styles: { type: 6, styles: "*", offset: null }, options: undefined }, { type: 0, name: "200", styles: { type: 6, styles: "*", offset: null }, options: undefined }, { type: 1, expr: "void => 50", animation: { type: 11, selector: "@*", animation: [{ type: 12, timings: "50ms", animation: [{ type: 9, options: null }] }], options: { optional: true } }, options: null }, { type: 1, expr: "void => 100", animation: { type: 11, selector: "@*", animation: [{ type: 12, timings: "100ms", animation: [{ type: 9, options: null }] }], options: { optional: true } }, options: null }, { type: 1, expr: "void => 200", animation: { type: 11, selector: "@*", animation: [{ type: 12, timings: "200ms", animation: [{ type: 9, options: null }] }], options: { optional: true } }, options: null }], options: {} }, { type: 7, name: "fadeInOut", definitions: [{ type: 0, name: "0", styles: { type: 6, styles: { display: "none", opacity: 0 }, offset: null }, options: undefined }, { type: 0, name: "1", styles: { type: 6, styles: { display: "block", opacity: 1 }, offset: null }, options: undefined }, { type: 1, expr: "1 => 0", animation: { type: 4, styles: null, timings: "300ms ease-out" }, options: null }, { type: 1, expr: "0 => 1", animation: { type: 4, styles: null, timings: "300ms ease-in" }, options: null }], options: {} }, { type: 7, name: "slideInOut", definitions: [{ type: 0, name: "0", styles: { type: 6, styles: { height: "0px", display: "none" }, offset: null }, options: undefined }, { type: 0, name: "1", styles: { type: 6, styles: { height: "*", display: "block" }, offset: null }, options: undefined }, { type: 1, expr: "1 => 0", animation: { type: 4, styles: null, timings: "300ms ease-out" }, options: null }, { type: 1, expr: "0 => 1", animation: { type: 4, styles: null, timings: "300ms ease-in" }, options: null }], options: {} }, { type: 7, name: "slideIn", definitions: [{ type: 1, expr: "void => left", animation: [{ type: 6, styles: { transform: "translateX(100%)" }, offset: null }, { type: 4, styles: { type: 6, styles: { transform: "translateX(0)" }, offset: null }, timings: "300ms ease-in" }], options: null }, { type: 1, expr: "left => void", animation: [{ type: 6, styles: { transform: "translateX(0)" }, offset: null }, { type: 4, styles: { type: 6, styles: { transform: "translateX(-100%)" }, offset: null }, timings: "300ms ease-in" }], options: null }, { type: 1, expr: "void => right", animation: [{ type: 6, styles: { transform: "translateX(-100%)" }, offset: null }, { type: 4, styles: { type: 6, styles: { transform: "translateX(0)" }, offset: null }, timings: "300ms ease-in" }], options: null }, { type: 1, expr: "right => void", animation: [{ type: 6, styles: { transform: "translateX(0)" }, offset: null }, { type: 4, styles: { type: 6, styles: { transform: "translateX(100%)" }, offset: null }, timings: "300ms ease-in" }], options: null }], options: {} }, { type: 7, name: "slideInLeft", definitions: [{ type: 0, name: "void", styles: { type: 6, styles: { transform: "translateX(-100%)", display: "none" }, offset: null }, options: undefined }, { type: 0, name: "*", styles: { type: 6, styles: { transform: "translateX(0)", display: "flex" }, offset: null }, options: undefined }, { type: 1, expr: "void => *", animation: { type: 4, styles: null, timings: "300ms" }, options: null }, { type: 1, expr: "* => void", animation: { type: 4, styles: null, timings: "300ms" }, options: null }], options: {} }, { type: 7, name: "slideInRight", definitions: [{ type: 0, name: "void", styles: { type: 6, styles: { transform: "translateX(100%)", display: "none" }, offset: null }, options: undefined }, { type: 0, name: "*", styles: { type: 6, styles: { transform: "translateX(0)", display: "flex" }, offset: null }, options: undefined }, { type: 1, expr: "void => *", animation: { type: 4, styles: null, timings: "300ms" }, options: null }, { type: 1, expr: "* => void", animation: { type: 4, styles: null, timings: "300ms" }, options: null }], options: {} }, { type: 7, name: "slideInTop", definitions: [{ type: 0, name: "void", styles: { type: 6, styles: { transform: "translateY(-100%)", display: "none" }, offset: null }, options: undefined }, { type: 0, name: "*", styles: { type: 6, styles: { transform: "translateY(0)", display: "flex" }, offset: null }, options: undefined }, { type: 1, expr: "void => *", animation: { type: 4, styles: null, timings: "300ms" }, options: null }, { type: 1, expr: "* => void", animation: { type: 4, styles: null, timings: "300ms" }, options: null }], options: {} }, { type: 7, name: "slideInBottom", definitions: [{ type: 0, name: "void", styles: { type: 6, styles: { transform: "translateY(100%)", display: "none" }, offset: null }, options: undefined }, { type: 0, name: "*", styles: { type: 6, styles: { transform: "translateY(0)", display: "flex" }, offset: null }, options: undefined }, { type: 1, expr: "void => *", animation: { type: 4, styles: null, timings: "300ms" }, options: null }, { type: 1, expr: "* => void", animation: { type: 4, styles: null, timings: "300ms" }, options: null }], options: {} }, { type: 7, name: "expandCollapse", definitions: [{ type: 0, name: "void", styles: { type: 6, styles: { height: "0px" }, offset: null }, options: undefined }, { type: 0, name: "*", styles: { type: 6, styles: { height: "*" }, offset: null }, options: undefined }, { type: 1, expr: "void => *", animation: { type: 4, styles: null, timings: "300ms ease-out" }, options: null }, { type: 1, expr: "* => void", animation: { type: 4, styles: null, timings: "300ms ease-in" }, options: null }], options: {} }, { type: 7, name: "routerTransitionLeft", definitions: [{ type: 1, expr: "* => *", animation: [{ type: 11, selector: "fuse-content > :enter, fuse-content > :leave", animation: [{ type: 6, styles: { position: "absolute", top: 0, bottom: 0, left: 0, right: 0 }, offset: null }], options: { optional: true } }, { type: 11, selector: "fuse-content > :enter", animation: [{ type: 6, styles: { transform: "translateX(100%)", opacity: 0 }, offset: null }], options: { optional: true } }, { type: 2, steps: [{ type: 3, steps: [{ type: 11, selector: "fuse-content > :leave", animation: [{ type: 6, styles: { transform: "translateX(0)", opacity: 1 }, offset: null }, { type: 4, styles: { type: 6, styles: { transform: "translateX(-100%)", opacity: 0 }, offset: null }, timings: "600ms cubic-bezier(0.0, 0.0, 0.2, 1)" }], options: { optional: true } }, { type: 11, selector: "fuse-content > :enter", animation: [{ type: 6, styles: { transform: "translateX(100%)" }, offset: null }, { type: 4, styles: { type: 6, styles: { transform: "translateX(0%)", opacity: 1 }, offset: null }, timings: "600ms cubic-bezier(0.0, 0.0, 0.2, 1)" }], options: { optional: true } }], options: null }, { type: 11, selector: "fuse-content > :leave", animation: { type: 9, options: null }, options: { optional: true } }, { type: 11, selector: "fuse-content > :enter", animation: { type: 9, options: null }, options: { optional: true } }], options: null }], options: null }], options: {} }, { type: 7, name: "routerTransitionRight", definitions: [{ type: 1, expr: "* => *", animation: [{ type: 11, selector: "fuse-content > :enter, fuse-content > :leave", animation: [{ type: 6, styles: { position: "absolute", top: 0, bottom: 0, left: 0, right: 0 }, offset: null }], options: { optional: true } }, { type: 11, selector: "fuse-content > :enter", animation: [{ type: 6, styles: { transform: "translateX(-100%)", opacity: 0 }, offset: null }], options: { optional: true } }, { type: 2, steps: [{ type: 3, steps: [{ type: 11, selector: "fuse-content > :leave", animation: [{ type: 6, styles: { transform: "translateX(0)", opacity: 1 }, offset: null }, { type: 4, styles: { type: 6, styles: { transform: "translateX(100%)", opacity: 0 }, offset: null }, timings: "600ms cubic-bezier(0.0, 0.0, 0.2, 1)" }], options: { optional: true } }, { type: 11, selector: "fuse-content > :enter", animation: [{ type: 6, styles: { transform: "translateX(-100%)" }, offset: null }, { type: 4, styles: { type: 6, styles: { transform: "translateX(0%)", opacity: 1 }, offset: null }, timings: "600ms cubic-bezier(0.0, 0.0, 0.2, 1)" }], options: { optional: true } }], options: null }, { type: 11, selector: "fuse-content > :leave", animation: { type: 9, options: null }, options: { optional: true } }, { type: 11, selector: "fuse-content > :enter", animation: { type: 9, options: null }, options: { optional: true } }], options: null }], options: null }], options: {} }, { type: 7, name: "routerTransitionUp", definitions: [{ type: 1, expr: "* => *", animation: [{ type: 11, selector: "fuse-content > :enter, fuse-content > :leave", animation: [{ type: 6, styles: { position: "absolute", top: 0, bottom: 0, left: 0, right: 0 }, offset: null }], options: { optional: true } }, { type: 11, selector: "fuse-content > :enter", animation: [{ type: 6, styles: { transform: "translateY(100%)", opacity: 0 }, offset: null }], options: { optional: true } }, { type: 3, steps: [{ type: 11, selector: "fuse-content > :leave", animation: [{ type: 6, styles: { transform: "translateY(0)", opacity: 1 }, offset: null }, { type: 4, styles: { type: 6, styles: { transform: "translateY(-100%)", opacity: 0 }, offset: null }, timings: "600ms cubic-bezier(0.0, 0.0, 0.2, 1)" }], options: { optional: true } }, { type: 11, selector: "fuse-content > :enter", animation: [{ type: 6, styles: { transform: "translateY(100%)" }, offset: null }, { type: 4, styles: { type: 6, styles: { transform: "translateY(0%)", opacity: 1 }, offset: null }, timings: "600ms cubic-bezier(0.0, 0.0, 0.2, 1)" }], options: { optional: true } }], options: null }, { type: 11, selector: "fuse-content > :leave", animation: { type: 9, options: null }, options: { optional: true } }, { type: 11, selector: "fuse-content > :enter", animation: { type: 9, options: null }, options: { optional: true } }], options: null }], options: {} }, { type: 7, name: "routerTransitionDown", definitions: [{ type: 1, expr: "* => *", animation: [{ type: 11, selector: "fuse-content > :enter, fuse-content > :leave", animation: [{ type: 6, styles: { position: "absolute", top: 0, bottom: 0, left: 0, right: 0 }, offset: null }], options: { optional: true } }, { type: 11, selector: "fuse-content > :enter", animation: [{ type: 6, styles: { transform: "translateY(-100%)", opacity: 0 }, offset: null }], options: { optional: true } }, { type: 2, steps: [{ type: 3, steps: [{ type: 11, selector: "fuse-content > :leave", animation: [{ type: 6, styles: { transform: "translateY(0)", opacity: 1 }, offset: null }, { type: 4, styles: { type: 6, styles: { transform: "translateY(100%)", opacity: 0 }, offset: null }, timings: "600ms cubic-bezier(0.0, 0.0, 0.2, 1)" }], options: { optional: true } }, { type: 11, selector: "fuse-content > :enter", animation: [{ type: 6, styles: { transform: "translateY(-100%)" }, offset: null }, { type: 4, styles: { type: 6, styles: { transform: "translateY(0%)", opacity: 1 }, offset: null }, timings: "600ms cubic-bezier(0.0, 0.0, 0.2, 1)" }], options: { optional: true } }], options: null }, { type: 11, selector: "fuse-content > :leave", animation: { type: 9, options: null }, options: { optional: true } }, { type: 11, selector: "fuse-content > :enter", animation: { type: 9, options: null }, options: { optional: true } }], options: null }], options: null }], options: {} }, { type: 7, name: "routerTransitionFade", definitions: [{ type: 1, expr: "* => *", animation: { type: 3, steps: [{ type: 11, selector: "fuse-content > :enter, fuse-content > :leave ", animation: [{ type: 6, styles: { position: "absolute", top: 0, bottom: 0, left: 0, right: 0 }, offset: null }], options: { optional: true } }, { type: 11, selector: "fuse-content > :enter", animation: [{ type: 6, styles: { opacity: 0 }, offset: null }], options: { optional: true } }, { type: 11, selector: "fuse-content > :leave", animation: [{ type: 6, styles: { opacity: 1 }, offset: null }, { type: 4, styles: { type: 6, styles: { opacity: 0 }, offset: null }, timings: "300ms cubic-bezier(0.0, 0.0, 0.2, 1)" }], options: { optional: true } }, { type: 11, selector: "fuse-content > :enter", animation: [{ type: 6, styles: { opacity: 0 }, offset: null }, { type: 4, styles: { type: 6, styles: { opacity: 1 }, offset: null }, timings: "300ms cubic-bezier(0.0, 0.0, 0.2, 1)" }], options: { optional: true } }, { type: 11, selector: "fuse-content > :enter", animation: { type: 9, options: null }, options: { optional: true } }, { type: 11, selector: "fuse-content > :leave", animation: { type: 9, options: null }, options: { optional: true } }], options: null }, options: null }], options: {} }] } });

function View_MerchantFeaturedBoardComponent_1(_l) { return __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵvid"](0, [(_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵeld"](0, 0, null, null, 20, "div", [["class", "breadcrumb text-truncate h1 pl-72"], ["fxLayout", "row"], ["fxLayoutAlign", "start center"]], [[24, "@animate", 0]], null, null, null, null)), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](1, 737280, null, 0, __WEBPACK_IMPORTED_MODULE_2__angular_flex_layout__["j" /* LayoutDirective */], [__WEBPACK_IMPORTED_MODULE_2__angular_flex_layout__["m" /* MediaMonitor */], __WEBPACK_IMPORTED_MODULE_1__angular_core__["ElementRef"], __WEBPACK_IMPORTED_MODULE_2__angular_flex_layout__["t" /* StyleUtils */]], { layout: [0, "layout"] }, null), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](2, 737280, null, 0, __WEBPACK_IMPORTED_MODULE_2__angular_flex_layout__["i" /* LayoutAlignDirective */], [__WEBPACK_IMPORTED_MODULE_2__angular_flex_layout__["m" /* MediaMonitor */], __WEBPACK_IMPORTED_MODULE_1__angular_core__["ElementRef"], [2, __WEBPACK_IMPORTED_MODULE_2__angular_flex_layout__["j" /* LayoutDirective */]], __WEBPACK_IMPORTED_MODULE_2__angular_flex_layout__["t" /* StyleUtils */]], { align: [0, "align"] }, null), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵpod"](3, { x: 0 }), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵpod"](4, { value: 0, params: 1 }), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n                    "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵeld"](6, 0, null, null, 13, "div", [["fxLayout", "row"], ["fxLayoutAlign", "start center"]], null, null, null, null, null)), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](7, 737280, null, 0, __WEBPACK_IMPORTED_MODULE_2__angular_flex_layout__["j" /* LayoutDirective */], [__WEBPACK_IMPORTED_MODULE_2__angular_flex_layout__["m" /* MediaMonitor */], __WEBPACK_IMPORTED_MODULE_1__angular_core__["ElementRef"], __WEBPACK_IMPORTED_MODULE_2__angular_flex_layout__["t" /* StyleUtils */]], { layout: [0, "layout"] }, null), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](8, 737280, null, 0, __WEBPACK_IMPORTED_MODULE_2__angular_flex_layout__["i" /* LayoutAlignDirective */], [__WEBPACK_IMPORTED_MODULE_2__angular_flex_layout__["m" /* MediaMonitor */], __WEBPACK_IMPORTED_MODULE_1__angular_core__["ElementRef"], [2, __WEBPACK_IMPORTED_MODULE_2__angular_flex_layout__["j" /* LayoutDirective */]], __WEBPACK_IMPORTED_MODULE_2__angular_flex_layout__["t" /* StyleUtils */]], { align: [0, "align"] }, null), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n                            "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵeld"](10, 0, null, null, 1, "span", [], null, null, null, null, null)), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["Placements"])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n                            "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵeld"](13, 0, null, null, 2, "mat-icon", [["class", "separator mat-icon"], ["role", "img"]], null, null, null, __WEBPACK_IMPORTED_MODULE_3__node_modules_angular_material_icon_typings_index_ngfactory__["b" /* View_MatIcon_0 */], __WEBPACK_IMPORTED_MODULE_3__node_modules_angular_material_icon_typings_index_ngfactory__["a" /* RenderType_MatIcon */])), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](14, 638976, null, 0, __WEBPACK_IMPORTED_MODULE_4__angular_material_icon__["b" /* MatIcon */], [__WEBPACK_IMPORTED_MODULE_1__angular_core__["ElementRef"], __WEBPACK_IMPORTED_MODULE_4__angular_material_icon__["d" /* MatIconRegistry */], [8, null]], null, null), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, 0, ["chevron_right"])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n                            "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵeld"](17, 0, null, null, 1, "span", [], null, null, null, null, null)), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["Featured Merchants"])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n                    "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n                    "]))], function (_ck, _v) { var currVal_1 = "row"; _ck(_v, 1, 0, currVal_1); var currVal_2 = "start center"; _ck(_v, 2, 0, currVal_2); var currVal_3 = "row"; _ck(_v, 7, 0, currVal_3); var currVal_4 = "start center"; _ck(_v, 8, 0, currVal_4); _ck(_v, 14, 0); }, function (_ck, _v) { var currVal_0 = _ck(_v, 4, 0, "*", _ck(_v, 3, 0, "50px")); _ck(_v, 0, 0, currVal_0); }); }
function View_MerchantFeaturedBoardComponent_3(_l) { return __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵvid"](0, [(_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵeld"](0, 0, null, null, 5, "fuse-scrumboard-board-list", [["class", "scrumboard-board-list list-wrapper ngx-dnd-item"], ["ngxDraggable", "false"]], [[24, "@animate", 0]], [[null, "touchmove"], [null, "touchstart"], [null, "touchend"]], function (_v, en, $event) { var ad = true; if (("touchmove" === en)) {
        var pd_0 = (__WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 1).onMove($event) !== false);
        ad = (pd_0 && ad);
    } if (("touchstart" === en)) {
        var pd_1 = (__WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 1).onDown($event) !== false);
        ad = (pd_1 && ad);
    } if (("touchend" === en)) {
        var pd_2 = (__WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵnov"](_v, 1).onUp($event) !== false);
        ad = (pd_2 && ad);
    } return ad; }, __WEBPACK_IMPORTED_MODULE_5__list_list_component_ngfactory__["b" /* View_MerchantFeaturedBoardListComponent_0 */], __WEBPACK_IMPORTED_MODULE_5__list_list_component_ngfactory__["a" /* RenderType_MerchantFeaturedBoardListComponent */])), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](1, 475136, null, 0, __WEBPACK_IMPORTED_MODULE_6__swimlane_ngx_dnd_release_directives_ngx_draggable_directive__["a" /* DraggableDirective */], [__WEBPACK_IMPORTED_MODULE_1__angular_core__["ElementRef"], __WEBPACK_IMPORTED_MODULE_7__swimlane_ngx_dnd_release_services_drake_store_service__["a" /* DrakeStoreService */], __WEBPACK_IMPORTED_MODULE_8__swimlane_ngx_dnd_release_directives_ngx_droppable_directive__["a" /* DroppableDirective */]], { ngxDraggable: [0, "ngxDraggable"], model: [1, "model"] }, null), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](2, 245760, null, 0, __WEBPACK_IMPORTED_MODULE_9__list_list_component__["a" /* MerchantFeaturedBoardListComponent */], [__WEBPACK_IMPORTED_MODULE_10__angular_router__["a" /* ActivatedRoute */], __WEBPACK_IMPORTED_MODULE_11__merchant_featured_service__["b" /* MerchantFeaturedService */], __WEBPACK_IMPORTED_MODULE_12__angular_material_dialog__["e" /* MatDialog */]], { list: [0, "list"] }, null), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵpod"](3, { duration: 0, x: 1 }), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵpod"](4, { value: 0, params: 1 }), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n                "]))], function (_ck, _v) { var currVal_1 = "false"; var currVal_2 = _v.context.$implicit; _ck(_v, 1, 0, currVal_1, currVal_2); var currVal_3 = _v.context.$implicit; _ck(_v, 2, 0, currVal_3); }, function (_ck, _v) { var currVal_0 = _ck(_v, 4, 0, "*", _ck(_v, 3, 0, "350ms", "100%")); _ck(_v, 0, 0, currVal_0); }); }
function View_MerchantFeaturedBoardComponent_2(_l) { return __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵvid"](0, [(_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵeld"](0, 0, null, null, 17, "div", [["class", "board-content ngx-dnd-container p-16 p-mat-24"], ["fxLayout", "row"], ["ngxDroppable", "list"]], [[24, "@animateStagger", 0]], [[null, "out"]], function (_v, en, $event) { var ad = true; var _co = _v.component; if (("out" === en)) {
        var pd_0 = (_co.onDrop($event) !== false);
        ad = (pd_0 && ad);
    } return ad; }, null, null)), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](1, 4407296, null, 0, __WEBPACK_IMPORTED_MODULE_8__swimlane_ngx_dnd_release_directives_ngx_droppable_directive__["a" /* DroppableDirective */], [__WEBPACK_IMPORTED_MODULE_1__angular_core__["ElementRef"], __WEBPACK_IMPORTED_MODULE_1__angular_core__["Renderer"], __WEBPACK_IMPORTED_MODULE_7__swimlane_ngx_dnd_release_services_drake_store_service__["a" /* DrakeStoreService */]], { model: [0, "model"], ngxDroppable: [1, "ngxDroppable"] }, { out: "out" }), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](2, 737280, null, 0, __WEBPACK_IMPORTED_MODULE_2__angular_flex_layout__["j" /* LayoutDirective */], [__WEBPACK_IMPORTED_MODULE_2__angular_flex_layout__["m" /* MediaMonitor */], __WEBPACK_IMPORTED_MODULE_1__angular_core__["ElementRef"], __WEBPACK_IMPORTED_MODULE_2__angular_flex_layout__["t" /* StyleUtils */]], { layout: [0, "layout"] }, null), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵpod"](3, { value: 0 }), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n\n                "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n                "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵand"](16777216, null, null, 1, null, View_MerchantFeaturedBoardComponent_3)), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](7, 802816, null, 0, __WEBPACK_IMPORTED_MODULE_13__angular_common__["NgForOf"], [__WEBPACK_IMPORTED_MODULE_1__angular_core__["ViewContainerRef"], __WEBPACK_IMPORTED_MODULE_1__angular_core__["TemplateRef"], __WEBPACK_IMPORTED_MODULE_1__angular_core__["IterableDiffers"]], { ngForOf: [0, "ngForOf"] }, null), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n                "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n\n                "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n                "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵeld"](11, 0, null, null, 4, "fuse-scrumboard-board-add-list", [["class", "new-list-wrapper"]], [[24, "@animate", 0]], [[null, "onlistAdd"]], function (_v, en, $event) { var ad = true; var _co = _v.component; if (("onlistAdd" === en)) {
        var pd_0 = (_co.onListAdd($event) !== false);
        ad = (pd_0 && ad);
    } return ad; }, __WEBPACK_IMPORTED_MODULE_14__add_list_add_list_component_ngfactory__["b" /* View_MerchantFeaturedBoardAddListComponent_0 */], __WEBPACK_IMPORTED_MODULE_14__add_list_add_list_component_ngfactory__["a" /* RenderType_MerchantFeaturedBoardAddListComponent */])), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](12, 49152, null, 0, __WEBPACK_IMPORTED_MODULE_15__add_list_add_list_component__["a" /* MerchantFeaturedBoardAddListComponent */], [__WEBPACK_IMPORTED_MODULE_16__angular_forms__["i" /* FormBuilder */], __WEBPACK_IMPORTED_MODULE_10__angular_router__["o" /* Router */]], null, { onlistAdd: "onlistAdd" }), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵpod"](13, { duration: 0, x: 1 }), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵpod"](14, { value: 0, params: 1 }), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n                "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n                "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n\n            "]))], function (_ck, _v) { var _co = _v.component; var currVal_1 = _co.board.lists; var currVal_2 = "list"; _ck(_v, 1, 0, currVal_1, currVal_2); var currVal_3 = "row"; _ck(_v, 2, 0, currVal_3); var currVal_4 = _co.board.lists; _ck(_v, 7, 0, currVal_4); }, function (_ck, _v) { var currVal_0 = _ck(_v, 3, 0, "50"); _ck(_v, 0, 0, currVal_0); var currVal_5 = _ck(_v, 14, 0, "*", _ck(_v, 13, 0, "350ms", "100%")); _ck(_v, 11, 0, currVal_5); }); }
function View_MerchantFeaturedBoardComponent_0(_l) { return __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵvid"](0, [(_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵeld"](0, 0, null, null, 57, "mat-sidenav-container", [["class", "mat-drawer-container mat-sidenav-container"]], null, null, null, __WEBPACK_IMPORTED_MODULE_17__node_modules_angular_material_sidenav_typings_index_ngfactory__["g" /* View_MatSidenavContainer_0 */], __WEBPACK_IMPORTED_MODULE_17__node_modules_angular_material_sidenav_typings_index_ngfactory__["d" /* RenderType_MatSidenavContainer */])), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](1, 1490944, null, 2, __WEBPACK_IMPORTED_MODULE_18__angular_material_sidenav__["f" /* MatSidenavContainer */], [[2, __WEBPACK_IMPORTED_MODULE_19__angular_cdk_bidi__["c" /* Directionality */]], __WEBPACK_IMPORTED_MODULE_1__angular_core__["ElementRef"], __WEBPACK_IMPORTED_MODULE_1__angular_core__["NgZone"], __WEBPACK_IMPORTED_MODULE_1__angular_core__["ChangeDetectorRef"], __WEBPACK_IMPORTED_MODULE_18__angular_material_sidenav__["a" /* MAT_DRAWER_DEFAULT_AUTOSIZE */]], null, null), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵqud"](603979776, 1, { _drawers: 1 }), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵqud"](335544320, 2, { _content: 0 }), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, 2, ["\n\n    "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵeld"](5, 0, null, 2, 50, "div", [["id", "board"]], null, null, null, null, null)), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n\n        "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n        "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵeld"](8, 0, null, null, 35, "div", [["class", "header mat-accent-bg p-16 p-mat-24"], ["fxLayout", "column"]], null, null, null, null, null)), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](9, 737280, null, 0, __WEBPACK_IMPORTED_MODULE_2__angular_flex_layout__["j" /* LayoutDirective */], [__WEBPACK_IMPORTED_MODULE_2__angular_flex_layout__["m" /* MediaMonitor */], __WEBPACK_IMPORTED_MODULE_1__angular_core__["ElementRef"], __WEBPACK_IMPORTED_MODULE_2__angular_flex_layout__["t" /* StyleUtils */]], { layout: [0, "layout"] }, null), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n                "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n\n            "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵeld"](12, 0, null, null, 30, "div", [["class", "header-content"], ["fxFlex", "1 0 auto"], ["fxLayout", "row wrap"], ["fxLayoutAlign", "space-between"]], null, null, null, null, null)), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](13, 737280, null, 0, __WEBPACK_IMPORTED_MODULE_2__angular_flex_layout__["j" /* LayoutDirective */], [__WEBPACK_IMPORTED_MODULE_2__angular_flex_layout__["m" /* MediaMonitor */], __WEBPACK_IMPORTED_MODULE_1__angular_core__["ElementRef"], __WEBPACK_IMPORTED_MODULE_2__angular_flex_layout__["t" /* StyleUtils */]], { layout: [0, "layout"] }, null), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](14, 737280, null, 0, __WEBPACK_IMPORTED_MODULE_2__angular_flex_layout__["i" /* LayoutAlignDirective */], [__WEBPACK_IMPORTED_MODULE_2__angular_flex_layout__["m" /* MediaMonitor */], __WEBPACK_IMPORTED_MODULE_1__angular_core__["ElementRef"], [2, __WEBPACK_IMPORTED_MODULE_2__angular_flex_layout__["j" /* LayoutDirective */]], __WEBPACK_IMPORTED_MODULE_2__angular_flex_layout__["t" /* StyleUtils */]], { align: [0, "align"] }, null), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](15, 737280, null, 0, __WEBPACK_IMPORTED_MODULE_2__angular_flex_layout__["e" /* FlexDirective */], [__WEBPACK_IMPORTED_MODULE_2__angular_flex_layout__["m" /* MediaMonitor */], __WEBPACK_IMPORTED_MODULE_1__angular_core__["ElementRef"], [3, __WEBPACK_IMPORTED_MODULE_2__angular_flex_layout__["j" /* LayoutDirective */]], __WEBPACK_IMPORTED_MODULE_2__angular_flex_layout__["t" /* StyleUtils */]], { flex: [0, "flex"] }, null), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n\n                "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n                "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵeld"](18, 0, null, null, 8, "div", [["fxFlexOrder", "2"], ["fxFlexOrder.gt-xs", "1"], ["fxLayout", "row"], ["fxLayoutAlign", "center center"]], null, null, null, null, null)), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](19, 737280, null, 0, __WEBPACK_IMPORTED_MODULE_2__angular_flex_layout__["j" /* LayoutDirective */], [__WEBPACK_IMPORTED_MODULE_2__angular_flex_layout__["m" /* MediaMonitor */], __WEBPACK_IMPORTED_MODULE_1__angular_core__["ElementRef"], __WEBPACK_IMPORTED_MODULE_2__angular_flex_layout__["t" /* StyleUtils */]], { layout: [0, "layout"] }, null), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](20, 737280, null, 0, __WEBPACK_IMPORTED_MODULE_2__angular_flex_layout__["i" /* LayoutAlignDirective */], [__WEBPACK_IMPORTED_MODULE_2__angular_flex_layout__["m" /* MediaMonitor */], __WEBPACK_IMPORTED_MODULE_1__angular_core__["ElementRef"], [2, __WEBPACK_IMPORTED_MODULE_2__angular_flex_layout__["j" /* LayoutDirective */]], __WEBPACK_IMPORTED_MODULE_2__angular_flex_layout__["t" /* StyleUtils */]], { align: [0, "align"] }, null), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](21, 737280, null, 0, __WEBPACK_IMPORTED_MODULE_2__angular_flex_layout__["h" /* FlexOrderDirective */], [__WEBPACK_IMPORTED_MODULE_2__angular_flex_layout__["m" /* MediaMonitor */], __WEBPACK_IMPORTED_MODULE_1__angular_core__["ElementRef"], __WEBPACK_IMPORTED_MODULE_2__angular_flex_layout__["t" /* StyleUtils */]], { order: [0, "order"], orderGtXs: [1, "orderGtXs"] }, null), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n                    \n                    "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵand"](16777216, null, null, 1, null, View_MerchantFeaturedBoardComponent_1)), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](24, 2113536, null, 0, __WEBPACK_IMPORTED_MODULE_20__fuse_directives_fuse_if_on_dom_fuse_if_on_dom_directive__["a" /* FuseIfOnDomDirective */], [__WEBPACK_IMPORTED_MODULE_1__angular_core__["TemplateRef"], __WEBPACK_IMPORTED_MODULE_1__angular_core__["ViewContainerRef"], __WEBPACK_IMPORTED_MODULE_1__angular_core__["ElementRef"]], null, null), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n                    "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n                "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n                "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n\n                "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n                "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n                "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n\n                "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n                "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵeld"](33, 0, null, null, 7, "div", [["class", "toolbar"], ["fxFlexOrder", "3"], ["fxLayout", "row"], ["fxLayoutAlign", "space-between center"]], null, null, null, null, null)), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](34, 737280, null, 0, __WEBPACK_IMPORTED_MODULE_2__angular_flex_layout__["j" /* LayoutDirective */], [__WEBPACK_IMPORTED_MODULE_2__angular_flex_layout__["m" /* MediaMonitor */], __WEBPACK_IMPORTED_MODULE_1__angular_core__["ElementRef"], __WEBPACK_IMPORTED_MODULE_2__angular_flex_layout__["t" /* StyleUtils */]], { layout: [0, "layout"] }, null), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](35, 737280, null, 0, __WEBPACK_IMPORTED_MODULE_2__angular_flex_layout__["i" /* LayoutAlignDirective */], [__WEBPACK_IMPORTED_MODULE_2__angular_flex_layout__["m" /* MediaMonitor */], __WEBPACK_IMPORTED_MODULE_1__angular_core__["ElementRef"], [2, __WEBPACK_IMPORTED_MODULE_2__angular_flex_layout__["j" /* LayoutDirective */]], __WEBPACK_IMPORTED_MODULE_2__angular_flex_layout__["t" /* StyleUtils */]], { align: [0, "align"] }, null), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](36, 737280, null, 0, __WEBPACK_IMPORTED_MODULE_2__angular_flex_layout__["h" /* FlexOrderDirective */], [__WEBPACK_IMPORTED_MODULE_2__angular_flex_layout__["m" /* MediaMonitor */], __WEBPACK_IMPORTED_MODULE_1__angular_core__["ElementRef"], __WEBPACK_IMPORTED_MODULE_2__angular_flex_layout__["t" /* StyleUtils */]], { order: [0, "order"] }, null), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n\n                    "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n                    "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n                    "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n                "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n                "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n\n            "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n\n        "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n        "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n\n        "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵeld"](46, 0, null, null, 7, "div", [["class", "board-content-wrapper p-16 p-mat-24"], ["fxFlex", ""], ["style", "background-color:#ffffff"]], null, null, null, null, null)), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](47, 737280, null, 0, __WEBPACK_IMPORTED_MODULE_2__angular_flex_layout__["e" /* FlexDirective */], [__WEBPACK_IMPORTED_MODULE_2__angular_flex_layout__["m" /* MediaMonitor */], __WEBPACK_IMPORTED_MODULE_1__angular_core__["ElementRef"], [3, __WEBPACK_IMPORTED_MODULE_2__angular_flex_layout__["j" /* LayoutDirective */]], __WEBPACK_IMPORTED_MODULE_2__angular_flex_layout__["t" /* StyleUtils */]], { flex: [0, "flex"] }, null), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n\n            "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n            "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵand"](16777216, null, null, 1, null, View_MerchantFeaturedBoardComponent_2)), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](51, 2113536, null, 0, __WEBPACK_IMPORTED_MODULE_20__fuse_directives_fuse_if_on_dom_fuse_if_on_dom_directive__["a" /* FuseIfOnDomDirective */], [__WEBPACK_IMPORTED_MODULE_1__angular_core__["TemplateRef"], __WEBPACK_IMPORTED_MODULE_1__angular_core__["ViewContainerRef"], __WEBPACK_IMPORTED_MODULE_1__angular_core__["ElementRef"]], null, null), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n            "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n        "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n        "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n    "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, 2, ["\n\n    "])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, 2, ["\n\n"])), (_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵted"](-1, null, ["\n"]))], function (_ck, _v) { _ck(_v, 1, 0); var currVal_0 = "column"; _ck(_v, 9, 0, currVal_0); var currVal_1 = "row wrap"; _ck(_v, 13, 0, currVal_1); var currVal_2 = "space-between"; _ck(_v, 14, 0, currVal_2); var currVal_3 = "1 0 auto"; _ck(_v, 15, 0, currVal_3); var currVal_4 = "row"; _ck(_v, 19, 0, currVal_4); var currVal_5 = "center center"; _ck(_v, 20, 0, currVal_5); var currVal_6 = "2"; var currVal_7 = "1"; _ck(_v, 21, 0, currVal_6, currVal_7); var currVal_8 = "row"; _ck(_v, 34, 0, currVal_8); var currVal_9 = "space-between center"; _ck(_v, 35, 0, currVal_9); var currVal_10 = "3"; _ck(_v, 36, 0, currVal_10); var currVal_11 = ""; _ck(_v, 47, 0, currVal_11); }, null); }
function View_MerchantFeaturedBoardComponent_Host_0(_l) { return __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵvid"](0, [(_l()(), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵeld"](0, 0, null, null, 1, "fuse-scrumboard-board", [], null, null, null, View_MerchantFeaturedBoardComponent_0, RenderType_MerchantFeaturedBoardComponent)), __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵdid"](1, 245760, null, 0, __WEBPACK_IMPORTED_MODULE_21__merchant_featured_board_component__["a" /* MerchantFeaturedBoardComponent */], [__WEBPACK_IMPORTED_MODULE_10__angular_router__["a" /* ActivatedRoute */], __WEBPACK_IMPORTED_MODULE_13__angular_common__["Location"], __WEBPACK_IMPORTED_MODULE_11__merchant_featured_service__["b" /* MerchantFeaturedService */], __WEBPACK_IMPORTED_MODULE_10__angular_router__["o" /* Router */]], null, null)], function (_ck, _v) { _ck(_v, 1, 0); }, null); }
var MerchantFeaturedBoardComponentNgFactory = __WEBPACK_IMPORTED_MODULE_1__angular_core__["ɵccf"]("fuse-scrumboard-board", __WEBPACK_IMPORTED_MODULE_21__merchant_featured_board_component__["a" /* MerchantFeaturedBoardComponent */], View_MerchantFeaturedBoardComponent_Host_0, {}, {}, []);



/***/ }),

/***/ "./src/app/main/content/pages/merchant-featured/merchant-featured-board/merchant-featured-board.component.scss.shim.ngstyle.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return styles; });
/**
 * @fileoverview This file was generated by the Angular template compiler. Do not edit.
 *
 * @suppress {suspiciousCode,uselessCode,missingProperties,missingOverride,checkTypes}
 * tslint:disable
 */ 
var styles = ["[_nghost-%COMP%]   mat-sidenav-container[_ngcontent-%COMP%] {\n  width: 100%;\n  height: 100%; }\n[_nghost-%COMP%]   mat-sidenav-container[_ngcontent-%COMP%]   mat-sidenav[_ngcontent-%COMP%] {\n    width: 320px !important;\n    min-width: 320px !important;\n    max-width: 320px !important; }\n[_nghost-%COMP%]   mat-sidenav-container[_ngcontent-%COMP%]   #board[_ngcontent-%COMP%] {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-orient: vertical !important;\n    -webkit-box-direction: normal !important;\n        -ms-flex-direction: column !important;\n            flex-direction: column !important;\n    height: 100%; }\n[_nghost-%COMP%]   mat-sidenav-container[_ngcontent-%COMP%]   #board[_ngcontent-%COMP%]    > .header[_ngcontent-%COMP%] {\n      position: relative;\n      min-height: 96px;\n      background-image: none;\n      z-index: 49; }\n[_nghost-%COMP%]   mat-sidenav-container[_ngcontent-%COMP%]   #board[_ngcontent-%COMP%]    > .header[_ngcontent-%COMP%]   .header-content[_ngcontent-%COMP%]   .header-boards-button[_ngcontent-%COMP%] {\n        margin: 0; }\n[_nghost-%COMP%]   mat-sidenav-container[_ngcontent-%COMP%]   #board[_ngcontent-%COMP%]    > .header[_ngcontent-%COMP%]   .header-content[_ngcontent-%COMP%]   .header-board-name[_ngcontent-%COMP%] {\n        font-size: 16px; }\n[_nghost-%COMP%]   mat-sidenav-container[_ngcontent-%COMP%]   #board[_ngcontent-%COMP%]    > .header[_ngcontent-%COMP%]   .header-content[_ngcontent-%COMP%]   .header-board-name[_ngcontent-%COMP%]   .board-subscribe[_ngcontent-%COMP%] {\n          margin-right: 8px; }\n[_nghost-%COMP%]   mat-sidenav-container[_ngcontent-%COMP%]   #board[_ngcontent-%COMP%]    > .header[_ngcontent-%COMP%]   .header-content[_ngcontent-%COMP%]   .header-board-name[_ngcontent-%COMP%]   .editable-buttons[_ngcontent-%COMP%]   mat-icon[_ngcontent-%COMP%] {\n          color: #FFFFFF !important; }\n[_nghost-%COMP%]   mat-sidenav-container[_ngcontent-%COMP%]   #board[_ngcontent-%COMP%]    > .header[_ngcontent-%COMP%]   .header-content[_ngcontent-%COMP%]   .right-side[_ngcontent-%COMP%]    > .mat-button[_ngcontent-%COMP%]:last-child {\n        margin-right: 0; }\n[_nghost-%COMP%]   mat-sidenav-container[_ngcontent-%COMP%]   #board[_ngcontent-%COMP%]   #board-selector[_ngcontent-%COMP%] {\n      position: absolute;\n      top: 96px;\n      right: 0;\n      left: 0;\n      height: 192px;\n      z-index: 48;\n      padding: 24px;\n      opacity: 1; }\n[_nghost-%COMP%]   mat-sidenav-container[_ngcontent-%COMP%]   #board[_ngcontent-%COMP%]   #board-selector[_ngcontent-%COMP%]   .board-list-item[_ngcontent-%COMP%] {\n        width: 128px;\n        height: 192px;\n        padding: 16px;\n        cursor: pointer;\n        position: relative; }\n[_nghost-%COMP%]   mat-sidenav-container[_ngcontent-%COMP%]   #board[_ngcontent-%COMP%]   #board-selector[_ngcontent-%COMP%]   .board-list-item[_ngcontent-%COMP%]   .board-name[_ngcontent-%COMP%] {\n          text-align: center;\n          padding: 16px 0; }\n[_nghost-%COMP%]   mat-sidenav-container[_ngcontent-%COMP%]   #board[_ngcontent-%COMP%]   #board-selector[_ngcontent-%COMP%]   .board-list-item[_ngcontent-%COMP%]   .selected-icon[_ngcontent-%COMP%] {\n          position: absolute;\n          top: 0;\n          left: 50%;\n          width: 32px;\n          height: 32px;\n          margin-left: -16px;\n          border-radius: 50%;\n          text-align: center;\n          color: white; }\n[_nghost-%COMP%]   mat-sidenav-container[_ngcontent-%COMP%]   #board[_ngcontent-%COMP%]   #board-selector[_ngcontent-%COMP%]   .board-list-item[_ngcontent-%COMP%]   .selected-icon[_ngcontent-%COMP%]   i[_ngcontent-%COMP%] {\n            line-height: 32px !important; }\n[_nghost-%COMP%]   mat-sidenav-container[_ngcontent-%COMP%]   #board[_ngcontent-%COMP%]   #board-selector[_ngcontent-%COMP%]   .board-list-item.add-new-board[_ngcontent-%COMP%] {\n          opacity: 0.6; }\n[_nghost-%COMP%]   mat-sidenav-container[_ngcontent-%COMP%]   #board[_ngcontent-%COMP%]   .board-content-wrapper[_ngcontent-%COMP%] {\n      position: relative;\n      background: #E5E7E8; }\n[_nghost-%COMP%]   mat-sidenav-container[_ngcontent-%COMP%]   #board[_ngcontent-%COMP%]   .board-content-wrapper[_ngcontent-%COMP%]   .board-content[_ngcontent-%COMP%] {\n        position: absolute;\n        top: 0;\n        right: 0;\n        bottom: 0;\n        left: 0;\n        height: 100%;\n        overflow-y: hidden;\n        overflow-x: auto;\n        -webkit-overflow-scrolling: touch; }\n[_nghost-%COMP%]   mat-sidenav-container[_ngcontent-%COMP%]   #board[_ngcontent-%COMP%]   .board-content-wrapper[_ngcontent-%COMP%]   .board-content[_ngcontent-%COMP%]   .list-sortable-placeholder[_ngcontent-%COMP%] {\n          background: rgba(0, 0, 0, 0.06);\n          margin-right: 24px; }\n[_nghost-%COMP%]   mat-sidenav-container[_ngcontent-%COMP%]   #board[_ngcontent-%COMP%]   .board-content-wrapper[_ngcontent-%COMP%]   .board-content[_ngcontent-%COMP%]   .new-list-wrapper[_ngcontent-%COMP%] {\n          width: 344px;\n          min-width: 344px;\n          max-width: 344px;\n          padding-right: 24px; }"];



/***/ }),

/***/ "./src/app/main/content/pages/merchant-featured/merchant-featured-board/merchant-featured-board.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MerchantFeaturedBoardComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common__ = __webpack_require__("./node_modules/@angular/common/esm5/common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__merchant_featured_service__ = __webpack_require__("./src/app/main/content/pages/merchant-featured/merchant-featured.service.ts");





var MerchantFeaturedBoardComponent = /** @class */ (function () {
    function MerchantFeaturedBoardComponent(route, location, merchantFeaturedService, router) {
        this.route = route;
        this.location = location;
        this.merchantFeaturedService = merchantFeaturedService;
        this.router = router;
    }
    MerchantFeaturedBoardComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.onBoardChanged =
            this.merchantFeaturedService.onBoardChanged
                .subscribe(function (board) {
                _this.board = board;
            });
        this.merchants = this.merchantFeaturedService.merchants;
    };
    MerchantFeaturedBoardComponent.prototype.ngOnDestroy = function () {
        this.onBoardChanged.unsubscribe();
    };
    // onListAdd(newListName)
    // {
    //     debugger;
    //     if ( newListName === '' )
    //     {
    //         return;
    //     }
    //     this.scrumboardService.addList(new List({name: newListName}));
    // }
    MerchantFeaturedBoardComponent.prototype.onListAdd = function () {
        debugger;
        this.merchantFeaturedService.addList();
    };
    MerchantFeaturedBoardComponent.prototype.onBoardNameChanged = function (newName) {
        this.merchantFeaturedService.updateBoard();
        this.location.go('/apps/scrumboard/boards/' + this.board.id + '/' + this.board.uri);
    };
    MerchantFeaturedBoardComponent.prototype.onDrop = function (ev) {
        var _this = this;
        this.merchantFeaturedService.updateBoard();
        this.router.navigateByUrl('/RefreshComponent', { skipLocationChange: true }).then(function () {
            return _this.router.navigate(["pages/featured-merchants/"]);
        });
    };
    return MerchantFeaturedBoardComponent;
}());



/***/ }),

/***/ "./src/app/main/content/pages/merchant-featured/merchant-featured.module.ngfactory.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MerchantFeaturedModuleNgFactory", function() { return MerchantFeaturedModuleNgFactory; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__merchant_featured_module__ = __webpack_require__("./src/app/main/content/pages/merchant-featured/merchant-featured.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__node_modules_angular_material_dialog_typings_index_ngfactory__ = __webpack_require__("./node_modules/@angular/material/dialog/typings/index.ngfactory.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__node_modules_angular_material_datepicker_typings_index_ngfactory__ = __webpack_require__("./node_modules/@angular/material/datepicker/typings/index.ngfactory.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__node_modules_angular_material_tooltip_typings_index_ngfactory__ = __webpack_require__("./node_modules/@angular/material/tooltip/typings/index.ngfactory.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__fuse_components_confirm_dialog_confirm_dialog_component_ngfactory__ = __webpack_require__("./src/@fuse/components/confirm-dialog/confirm-dialog.component.ngfactory.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__node_modules_angular_material_snack_bar_typings_index_ngfactory__ = __webpack_require__("./node_modules/@angular/material/snack-bar/typings/index.ngfactory.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__merchant_featured_board_merchant_featured_board_component_ngfactory__ = __webpack_require__("./src/app/main/content/pages/merchant-featured/merchant-featured-board/merchant-featured-board.component.ngfactory.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__merchant_featured_board_dialogs_card_card_component_ngfactory__ = __webpack_require__("./src/app/main/content/pages/merchant-featured/merchant-featured-board/dialogs/card/card.component.ngfactory.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__angular_common__ = __webpack_require__("./node_modules/@angular/common/esm5/common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__angular_cdk_bidi__ = __webpack_require__("./node_modules/@angular/cdk/esm5/bidi.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__angular_cdk_platform__ = __webpack_require__("./node_modules/@angular/cdk/esm5/platform.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__angular_cdk_a11y__ = __webpack_require__("./node_modules/@angular/cdk/esm5/a11y.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__angular_cdk_observers__ = __webpack_require__("./node_modules/@angular/cdk/esm5/observers.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__angular_material_core__ = __webpack_require__("./node_modules/@angular/material/esm5/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__angular_cdk_scrolling__ = __webpack_require__("./node_modules/@angular/cdk/esm5/scrolling.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__angular_cdk_overlay__ = __webpack_require__("./node_modules/@angular/cdk/esm5/overlay.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__angular_material_dialog__ = __webpack_require__("./node_modules/@angular/material/esm5/dialog.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__angular_material_datepicker__ = __webpack_require__("./node_modules/@angular/material/esm5/datepicker.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__angular_material_icon__ = __webpack_require__("./node_modules/@angular/material/esm5/icon.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__angular_common_http__ = __webpack_require__("./node_modules/@angular/common/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__angular_platform_browser__ = __webpack_require__("./node_modules/@angular/platform-browser/esm5/platform-browser.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__angular_material_menu__ = __webpack_require__("./node_modules/@angular/material/esm5/menu.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__angular_cdk_layout__ = __webpack_require__("./node_modules/@angular/cdk/esm5/layout.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__angular_material_tooltip__ = __webpack_require__("./node_modules/@angular/material/esm5/tooltip.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__swimlane_ngx_dnd_release_services_drake_store_service__ = __webpack_require__("./node_modules/@swimlane/ngx-dnd/release/services/drake-store.service.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__angular_forms__ = __webpack_require__("./node_modules/@angular/forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_27__angular_flex_layout__ = __webpack_require__("./node_modules/@angular/flex-layout/esm5/flex-layout.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_28__angular_material_select__ = __webpack_require__("./node_modules/@angular/material/esm5/select.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_29__angular_material_autocomplete__ = __webpack_require__("./node_modules/@angular/material/esm5/autocomplete.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_30__angular_material_snack_bar__ = __webpack_require__("./node_modules/@angular/material/esm5/snack-bar.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_31__merchant_featured_service__ = __webpack_require__("./src/app/main/content/pages/merchant-featured/merchant-featured.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_32__app_service__ = __webpack_require__("./src/app/app.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_33__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_34__angular_material_button__ = __webpack_require__("./node_modules/@angular/material/esm5/button.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_35__angular_material_checkbox__ = __webpack_require__("./node_modules/@angular/material/esm5/checkbox.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_36__angular_material_chips__ = __webpack_require__("./node_modules/@angular/material/esm5/chips.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_37__angular_cdk_portal__ = __webpack_require__("./node_modules/@angular/cdk/esm5/portal.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_38__angular_material_form_field__ = __webpack_require__("./node_modules/@angular/material/esm5/form-field.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_39__angular_material_input__ = __webpack_require__("./node_modules/@angular/material/esm5/input.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_40__angular_material_divider__ = __webpack_require__("./node_modules/@angular/material/esm5/divider.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_41__angular_material_list__ = __webpack_require__("./node_modules/@angular/material/esm5/list.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_42__angular_material_progress_bar__ = __webpack_require__("./node_modules/@angular/material/esm5/progress-bar.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_43__angular_material_sidenav__ = __webpack_require__("./node_modules/@angular/material/esm5/sidenav.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_44__angular_material_toolbar__ = __webpack_require__("./node_modules/@angular/material/esm5/toolbar.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_45__swimlane_ngx_dnd_release_ngx_dnd_module__ = __webpack_require__("./node_modules/@swimlane/ngx-dnd/release/ngx-dnd.module.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_46__fuse_directives_directives__ = __webpack_require__("./src/@fuse/directives/directives.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_47__fuse_pipes_pipes_module__ = __webpack_require__("./src/@fuse/pipes/pipes.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_48__fuse_shared_module__ = __webpack_require__("./src/@fuse/shared.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_49__fuse_components_confirm_dialog_confirm_dialog_module__ = __webpack_require__("./src/@fuse/components/confirm-dialog/confirm-dialog.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_50__fuse_components_material_color_picker_material_color_picker_module__ = __webpack_require__("./src/@fuse/components/material-color-picker/material-color-picker.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_51__merchant_featured_board_merchant_featured_board_component__ = __webpack_require__("./src/app/main/content/pages/merchant-featured/merchant-featured-board/merchant-featured-board.component.ts");
/**
 * @fileoverview This file was generated by the Angular template compiler. Do not edit.
 *
 * @suppress {suspiciousCode,uselessCode,missingProperties,missingOverride,checkTypes}
 * tslint:disable
 */ 




















































var MerchantFeaturedModuleNgFactory = __WEBPACK_IMPORTED_MODULE_0__angular_core__["ɵcmf"](__WEBPACK_IMPORTED_MODULE_1__merchant_featured_module__["a" /* MerchantFeaturedModule */], [], function (_l) { return __WEBPACK_IMPORTED_MODULE_0__angular_core__["ɵmod"]([__WEBPACK_IMPORTED_MODULE_0__angular_core__["ɵmpd"](512, __WEBPACK_IMPORTED_MODULE_0__angular_core__["ComponentFactoryResolver"], __WEBPACK_IMPORTED_MODULE_0__angular_core__["ɵCodegenComponentFactoryResolver"], [[8, [__WEBPACK_IMPORTED_MODULE_2__node_modules_angular_material_dialog_typings_index_ngfactory__["a" /* MatDialogContainerNgFactory */], __WEBPACK_IMPORTED_MODULE_3__node_modules_angular_material_datepicker_typings_index_ngfactory__["a" /* MatDatepickerContentNgFactory */], __WEBPACK_IMPORTED_MODULE_4__node_modules_angular_material_tooltip_typings_index_ngfactory__["a" /* TooltipComponentNgFactory */], __WEBPACK_IMPORTED_MODULE_5__fuse_components_confirm_dialog_confirm_dialog_component_ngfactory__["a" /* FuseConfirmDialogComponentNgFactory */], __WEBPACK_IMPORTED_MODULE_6__node_modules_angular_material_snack_bar_typings_index_ngfactory__["a" /* MatSnackBarContainerNgFactory */], __WEBPACK_IMPORTED_MODULE_6__node_modules_angular_material_snack_bar_typings_index_ngfactory__["b" /* SimpleSnackBarNgFactory */], __WEBPACK_IMPORTED_MODULE_7__merchant_featured_board_merchant_featured_board_component_ngfactory__["a" /* MerchantFeaturedBoardComponentNgFactory */], __WEBPACK_IMPORTED_MODULE_8__merchant_featured_board_dialogs_card_card_component_ngfactory__["a" /* MerchantFeaturedCardDialogComponentNgFactory */]]], [3, __WEBPACK_IMPORTED_MODULE_0__angular_core__["ComponentFactoryResolver"]], __WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModuleRef"]]), __WEBPACK_IMPORTED_MODULE_0__angular_core__["ɵmpd"](4608, __WEBPACK_IMPORTED_MODULE_9__angular_common__["NgLocalization"], __WEBPACK_IMPORTED_MODULE_9__angular_common__["NgLocaleLocalization"], [__WEBPACK_IMPORTED_MODULE_0__angular_core__["LOCALE_ID"], [2, __WEBPACK_IMPORTED_MODULE_9__angular_common__["ɵa"]]]), __WEBPACK_IMPORTED_MODULE_0__angular_core__["ɵmpd"](6144, __WEBPACK_IMPORTED_MODULE_10__angular_cdk_bidi__["b" /* DIR_DOCUMENT */], null, [__WEBPACK_IMPORTED_MODULE_9__angular_common__["DOCUMENT"]]), __WEBPACK_IMPORTED_MODULE_0__angular_core__["ɵmpd"](4608, __WEBPACK_IMPORTED_MODULE_10__angular_cdk_bidi__["c" /* Directionality */], __WEBPACK_IMPORTED_MODULE_10__angular_cdk_bidi__["c" /* Directionality */], [[2, __WEBPACK_IMPORTED_MODULE_10__angular_cdk_bidi__["b" /* DIR_DOCUMENT */]]]), __WEBPACK_IMPORTED_MODULE_0__angular_core__["ɵmpd"](4608, __WEBPACK_IMPORTED_MODULE_11__angular_cdk_platform__["a" /* Platform */], __WEBPACK_IMPORTED_MODULE_11__angular_cdk_platform__["a" /* Platform */], []), __WEBPACK_IMPORTED_MODULE_0__angular_core__["ɵmpd"](4608, __WEBPACK_IMPORTED_MODULE_12__angular_cdk_a11y__["l" /* InteractivityChecker */], __WEBPACK_IMPORTED_MODULE_12__angular_cdk_a11y__["l" /* InteractivityChecker */], [__WEBPACK_IMPORTED_MODULE_11__angular_cdk_platform__["a" /* Platform */]]), __WEBPACK_IMPORTED_MODULE_0__angular_core__["ɵmpd"](4608, __WEBPACK_IMPORTED_MODULE_12__angular_cdk_a11y__["k" /* FocusTrapFactory */], __WEBPACK_IMPORTED_MODULE_12__angular_cdk_a11y__["k" /* FocusTrapFactory */], [__WEBPACK_IMPORTED_MODULE_12__angular_cdk_a11y__["l" /* InteractivityChecker */], __WEBPACK_IMPORTED_MODULE_0__angular_core__["NgZone"], __WEBPACK_IMPORTED_MODULE_9__angular_common__["DOCUMENT"]]), __WEBPACK_IMPORTED_MODULE_0__angular_core__["ɵmpd"](136192, __WEBPACK_IMPORTED_MODULE_12__angular_cdk_a11y__["e" /* AriaDescriber */], __WEBPACK_IMPORTED_MODULE_12__angular_cdk_a11y__["c" /* ARIA_DESCRIBER_PROVIDER_FACTORY */], [[3, __WEBPACK_IMPORTED_MODULE_12__angular_cdk_a11y__["e" /* AriaDescriber */]], __WEBPACK_IMPORTED_MODULE_9__angular_common__["DOCUMENT"]]), __WEBPACK_IMPORTED_MODULE_0__angular_core__["ɵmpd"](5120, __WEBPACK_IMPORTED_MODULE_12__angular_cdk_a11y__["p" /* LiveAnnouncer */], __WEBPACK_IMPORTED_MODULE_12__angular_cdk_a11y__["o" /* LIVE_ANNOUNCER_PROVIDER_FACTORY */], [[3, __WEBPACK_IMPORTED_MODULE_12__angular_cdk_a11y__["p" /* LiveAnnouncer */]], [2, __WEBPACK_IMPORTED_MODULE_12__angular_cdk_a11y__["m" /* LIVE_ANNOUNCER_ELEMENT_TOKEN */]], __WEBPACK_IMPORTED_MODULE_9__angular_common__["DOCUMENT"]]), __WEBPACK_IMPORTED_MODULE_0__angular_core__["ɵmpd"](5120, __WEBPACK_IMPORTED_MODULE_12__angular_cdk_a11y__["j" /* FocusMonitor */], __WEBPACK_IMPORTED_MODULE_12__angular_cdk_a11y__["h" /* FOCUS_MONITOR_PROVIDER_FACTORY */], [[3, __WEBPACK_IMPORTED_MODULE_12__angular_cdk_a11y__["j" /* FocusMonitor */]], __WEBPACK_IMPORTED_MODULE_0__angular_core__["NgZone"], __WEBPACK_IMPORTED_MODULE_11__angular_cdk_platform__["a" /* Platform */]]), __WEBPACK_IMPORTED_MODULE_0__angular_core__["ɵmpd"](4608, __WEBPACK_IMPORTED_MODULE_13__angular_cdk_observers__["b" /* MutationObserverFactory */], __WEBPACK_IMPORTED_MODULE_13__angular_cdk_observers__["b" /* MutationObserverFactory */], []), __WEBPACK_IMPORTED_MODULE_0__angular_core__["ɵmpd"](4608, __WEBPACK_IMPORTED_MODULE_14__angular_material_core__["d" /* ErrorStateMatcher */], __WEBPACK_IMPORTED_MODULE_14__angular_material_core__["d" /* ErrorStateMatcher */], []), __WEBPACK_IMPORTED_MODULE_0__angular_core__["ɵmpd"](5120, __WEBPACK_IMPORTED_MODULE_15__angular_cdk_scrolling__["d" /* ScrollDispatcher */], __WEBPACK_IMPORTED_MODULE_15__angular_cdk_scrolling__["b" /* SCROLL_DISPATCHER_PROVIDER_FACTORY */], [[3, __WEBPACK_IMPORTED_MODULE_15__angular_cdk_scrolling__["d" /* ScrollDispatcher */]], __WEBPACK_IMPORTED_MODULE_0__angular_core__["NgZone"], __WEBPACK_IMPORTED_MODULE_11__angular_cdk_platform__["a" /* Platform */]]), __WEBPACK_IMPORTED_MODULE_0__angular_core__["ɵmpd"](5120, __WEBPACK_IMPORTED_MODULE_15__angular_cdk_scrolling__["g" /* ViewportRuler */], __WEBPACK_IMPORTED_MODULE_15__angular_cdk_scrolling__["f" /* VIEWPORT_RULER_PROVIDER_FACTORY */], [[3, __WEBPACK_IMPORTED_MODULE_15__angular_cdk_scrolling__["g" /* ViewportRuler */]], __WEBPACK_IMPORTED_MODULE_11__angular_cdk_platform__["a" /* Platform */], __WEBPACK_IMPORTED_MODULE_0__angular_core__["NgZone"]]), __WEBPACK_IMPORTED_MODULE_0__angular_core__["ɵmpd"](4608, __WEBPACK_IMPORTED_MODULE_16__angular_cdk_overlay__["i" /* ScrollStrategyOptions */], __WEBPACK_IMPORTED_MODULE_16__angular_cdk_overlay__["i" /* ScrollStrategyOptions */], [__WEBPACK_IMPORTED_MODULE_15__angular_cdk_scrolling__["d" /* ScrollDispatcher */], __WEBPACK_IMPORTED_MODULE_15__angular_cdk_scrolling__["g" /* ViewportRuler */], __WEBPACK_IMPORTED_MODULE_0__angular_core__["NgZone"], __WEBPACK_IMPORTED_MODULE_9__angular_common__["DOCUMENT"]]), __WEBPACK_IMPORTED_MODULE_0__angular_core__["ɵmpd"](5120, __WEBPACK_IMPORTED_MODULE_16__angular_cdk_overlay__["e" /* OverlayContainer */], __WEBPACK_IMPORTED_MODULE_16__angular_cdk_overlay__["k" /* ɵa */], [[3, __WEBPACK_IMPORTED_MODULE_16__angular_cdk_overlay__["e" /* OverlayContainer */]], __WEBPACK_IMPORTED_MODULE_9__angular_common__["DOCUMENT"]]), __WEBPACK_IMPORTED_MODULE_0__angular_core__["ɵmpd"](4608, __WEBPACK_IMPORTED_MODULE_16__angular_cdk_overlay__["h" /* OverlayPositionBuilder */], __WEBPACK_IMPORTED_MODULE_16__angular_cdk_overlay__["h" /* OverlayPositionBuilder */], [__WEBPACK_IMPORTED_MODULE_15__angular_cdk_scrolling__["g" /* ViewportRuler */], __WEBPACK_IMPORTED_MODULE_9__angular_common__["DOCUMENT"]]), __WEBPACK_IMPORTED_MODULE_0__angular_core__["ɵmpd"](5120, __WEBPACK_IMPORTED_MODULE_16__angular_cdk_overlay__["f" /* OverlayKeyboardDispatcher */], __WEBPACK_IMPORTED_MODULE_16__angular_cdk_overlay__["n" /* ɵf */], [[3, __WEBPACK_IMPORTED_MODULE_16__angular_cdk_overlay__["f" /* OverlayKeyboardDispatcher */]], __WEBPACK_IMPORTED_MODULE_9__angular_common__["DOCUMENT"]]), __WEBPACK_IMPORTED_MODULE_0__angular_core__["ɵmpd"](4608, __WEBPACK_IMPORTED_MODULE_16__angular_cdk_overlay__["c" /* Overlay */], __WEBPACK_IMPORTED_MODULE_16__angular_cdk_overlay__["c" /* Overlay */], [__WEBPACK_IMPORTED_MODULE_16__angular_cdk_overlay__["i" /* ScrollStrategyOptions */], __WEBPACK_IMPORTED_MODULE_16__angular_cdk_overlay__["e" /* OverlayContainer */], __WEBPACK_IMPORTED_MODULE_0__angular_core__["ComponentFactoryResolver"], __WEBPACK_IMPORTED_MODULE_16__angular_cdk_overlay__["h" /* OverlayPositionBuilder */], __WEBPACK_IMPORTED_MODULE_16__angular_cdk_overlay__["f" /* OverlayKeyboardDispatcher */], __WEBPACK_IMPORTED_MODULE_0__angular_core__["ApplicationRef"], __WEBPACK_IMPORTED_MODULE_0__angular_core__["Injector"], __WEBPACK_IMPORTED_MODULE_0__angular_core__["NgZone"], __WEBPACK_IMPORTED_MODULE_9__angular_common__["DOCUMENT"]]), __WEBPACK_IMPORTED_MODULE_0__angular_core__["ɵmpd"](5120, __WEBPACK_IMPORTED_MODULE_16__angular_cdk_overlay__["l" /* ɵc */], __WEBPACK_IMPORTED_MODULE_16__angular_cdk_overlay__["m" /* ɵd */], [__WEBPACK_IMPORTED_MODULE_16__angular_cdk_overlay__["c" /* Overlay */]]), __WEBPACK_IMPORTED_MODULE_0__angular_core__["ɵmpd"](5120, __WEBPACK_IMPORTED_MODULE_17__angular_material_dialog__["c" /* MAT_DIALOG_SCROLL_STRATEGY */], __WEBPACK_IMPORTED_MODULE_17__angular_material_dialog__["d" /* MAT_DIALOG_SCROLL_STRATEGY_PROVIDER_FACTORY */], [__WEBPACK_IMPORTED_MODULE_16__angular_cdk_overlay__["c" /* Overlay */]]), __WEBPACK_IMPORTED_MODULE_0__angular_core__["ɵmpd"](4608, __WEBPACK_IMPORTED_MODULE_17__angular_material_dialog__["e" /* MatDialog */], __WEBPACK_IMPORTED_MODULE_17__angular_material_dialog__["e" /* MatDialog */], [__WEBPACK_IMPORTED_MODULE_16__angular_cdk_overlay__["c" /* Overlay */], __WEBPACK_IMPORTED_MODULE_0__angular_core__["Injector"], [2, __WEBPACK_IMPORTED_MODULE_9__angular_common__["Location"]], [2, __WEBPACK_IMPORTED_MODULE_17__angular_material_dialog__["b" /* MAT_DIALOG_DEFAULT_OPTIONS */]], __WEBPACK_IMPORTED_MODULE_17__angular_material_dialog__["c" /* MAT_DIALOG_SCROLL_STRATEGY */], [3, __WEBPACK_IMPORTED_MODULE_17__angular_material_dialog__["e" /* MatDialog */]], __WEBPACK_IMPORTED_MODULE_16__angular_cdk_overlay__["e" /* OverlayContainer */]]), __WEBPACK_IMPORTED_MODULE_0__angular_core__["ɵmpd"](4608, __WEBPACK_IMPORTED_MODULE_18__angular_material_datepicker__["h" /* MatDatepickerIntl */], __WEBPACK_IMPORTED_MODULE_18__angular_material_datepicker__["h" /* MatDatepickerIntl */], []), __WEBPACK_IMPORTED_MODULE_0__angular_core__["ɵmpd"](5120, __WEBPACK_IMPORTED_MODULE_18__angular_material_datepicker__["a" /* MAT_DATEPICKER_SCROLL_STRATEGY */], __WEBPACK_IMPORTED_MODULE_18__angular_material_datepicker__["b" /* MAT_DATEPICKER_SCROLL_STRATEGY_PROVIDER_FACTORY */], [__WEBPACK_IMPORTED_MODULE_16__angular_cdk_overlay__["c" /* Overlay */]]), __WEBPACK_IMPORTED_MODULE_0__angular_core__["ɵmpd"](5120, __WEBPACK_IMPORTED_MODULE_19__angular_material_icon__["d" /* MatIconRegistry */], __WEBPACK_IMPORTED_MODULE_19__angular_material_icon__["a" /* ICON_REGISTRY_PROVIDER_FACTORY */], [[3, __WEBPACK_IMPORTED_MODULE_19__angular_material_icon__["d" /* MatIconRegistry */]], [2, __WEBPACK_IMPORTED_MODULE_20__angular_common_http__["c" /* HttpClient */]], __WEBPACK_IMPORTED_MODULE_21__angular_platform_browser__["DomSanitizer"], [2, __WEBPACK_IMPORTED_MODULE_9__angular_common__["DOCUMENT"]]]), __WEBPACK_IMPORTED_MODULE_0__angular_core__["ɵmpd"](5120, __WEBPACK_IMPORTED_MODULE_22__angular_material_menu__["b" /* MAT_MENU_SCROLL_STRATEGY */], __WEBPACK_IMPORTED_MODULE_22__angular_material_menu__["g" /* ɵc21 */], [__WEBPACK_IMPORTED_MODULE_16__angular_cdk_overlay__["c" /* Overlay */]]), __WEBPACK_IMPORTED_MODULE_0__angular_core__["ɵmpd"](4608, __WEBPACK_IMPORTED_MODULE_23__angular_cdk_layout__["d" /* MediaMatcher */], __WEBPACK_IMPORTED_MODULE_23__angular_cdk_layout__["d" /* MediaMatcher */], [__WEBPACK_IMPORTED_MODULE_11__angular_cdk_platform__["a" /* Platform */]]), __WEBPACK_IMPORTED_MODULE_0__angular_core__["ɵmpd"](135680, __WEBPACK_IMPORTED_MODULE_23__angular_cdk_layout__["a" /* BreakpointObserver */], __WEBPACK_IMPORTED_MODULE_23__angular_cdk_layout__["a" /* BreakpointObserver */], [__WEBPACK_IMPORTED_MODULE_23__angular_cdk_layout__["d" /* MediaMatcher */], __WEBPACK_IMPORTED_MODULE_0__angular_core__["NgZone"]]), __WEBPACK_IMPORTED_MODULE_0__angular_core__["ɵmpd"](5120, __WEBPACK_IMPORTED_MODULE_24__angular_material_tooltip__["b" /* MAT_TOOLTIP_SCROLL_STRATEGY */], __WEBPACK_IMPORTED_MODULE_24__angular_material_tooltip__["c" /* MAT_TOOLTIP_SCROLL_STRATEGY_PROVIDER_FACTORY */], [__WEBPACK_IMPORTED_MODULE_16__angular_cdk_overlay__["c" /* Overlay */]]), __WEBPACK_IMPORTED_MODULE_0__angular_core__["ɵmpd"](4608, __WEBPACK_IMPORTED_MODULE_25__swimlane_ngx_dnd_release_services_drake_store_service__["a" /* DrakeStoreService */], __WEBPACK_IMPORTED_MODULE_25__swimlane_ngx_dnd_release_services_drake_store_service__["a" /* DrakeStoreService */], []), __WEBPACK_IMPORTED_MODULE_0__angular_core__["ɵmpd"](4608, __WEBPACK_IMPORTED_MODULE_26__angular_forms__["G" /* ɵi */], __WEBPACK_IMPORTED_MODULE_26__angular_forms__["G" /* ɵi */], []), __WEBPACK_IMPORTED_MODULE_0__angular_core__["ɵmpd"](4608, __WEBPACK_IMPORTED_MODULE_26__angular_forms__["i" /* FormBuilder */], __WEBPACK_IMPORTED_MODULE_26__angular_forms__["i" /* FormBuilder */], []), __WEBPACK_IMPORTED_MODULE_0__angular_core__["ɵmpd"](5120, __WEBPACK_IMPORTED_MODULE_27__angular_flex_layout__["a" /* BREAKPOINTS */], __WEBPACK_IMPORTED_MODULE_27__angular_flex_layout__["d" /* DEFAULT_BREAKPOINTS_PROVIDER_FACTORY */], []), __WEBPACK_IMPORTED_MODULE_0__angular_core__["ɵmpd"](4608, __WEBPACK_IMPORTED_MODULE_27__angular_flex_layout__["b" /* BreakPointRegistry */], __WEBPACK_IMPORTED_MODULE_27__angular_flex_layout__["b" /* BreakPointRegistry */], [__WEBPACK_IMPORTED_MODULE_27__angular_flex_layout__["a" /* BREAKPOINTS */]]), __WEBPACK_IMPORTED_MODULE_0__angular_core__["ɵmpd"](4608, __WEBPACK_IMPORTED_MODULE_27__angular_flex_layout__["l" /* MatchMedia */], __WEBPACK_IMPORTED_MODULE_27__angular_flex_layout__["l" /* MatchMedia */], [__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgZone"], __WEBPACK_IMPORTED_MODULE_0__angular_core__["PLATFORM_ID"], __WEBPACK_IMPORTED_MODULE_9__angular_common__["DOCUMENT"]]), __WEBPACK_IMPORTED_MODULE_0__angular_core__["ɵmpd"](4608, __WEBPACK_IMPORTED_MODULE_27__angular_flex_layout__["m" /* MediaMonitor */], __WEBPACK_IMPORTED_MODULE_27__angular_flex_layout__["m" /* MediaMonitor */], [__WEBPACK_IMPORTED_MODULE_27__angular_flex_layout__["b" /* BreakPointRegistry */], __WEBPACK_IMPORTED_MODULE_27__angular_flex_layout__["l" /* MatchMedia */]]), __WEBPACK_IMPORTED_MODULE_0__angular_core__["ɵmpd"](5120, __WEBPACK_IMPORTED_MODULE_27__angular_flex_layout__["p" /* ObservableMedia */], __WEBPACK_IMPORTED_MODULE_27__angular_flex_layout__["o" /* OBSERVABLE_MEDIA_PROVIDER_FACTORY */], [[3, __WEBPACK_IMPORTED_MODULE_27__angular_flex_layout__["p" /* ObservableMedia */]], __WEBPACK_IMPORTED_MODULE_27__angular_flex_layout__["l" /* MatchMedia */], __WEBPACK_IMPORTED_MODULE_27__angular_flex_layout__["b" /* BreakPointRegistry */]]), __WEBPACK_IMPORTED_MODULE_0__angular_core__["ɵmpd"](6144, __WEBPACK_IMPORTED_MODULE_27__angular_flex_layout__["v" /* ɵa */], null, [__WEBPACK_IMPORTED_MODULE_9__angular_common__["DOCUMENT"]]), __WEBPACK_IMPORTED_MODULE_0__angular_core__["ɵmpd"](4608, __WEBPACK_IMPORTED_MODULE_27__angular_flex_layout__["w" /* ɵb */], __WEBPACK_IMPORTED_MODULE_27__angular_flex_layout__["w" /* ɵb */], [[2, __WEBPACK_IMPORTED_MODULE_27__angular_flex_layout__["v" /* ɵa */]]]), __WEBPACK_IMPORTED_MODULE_0__angular_core__["ɵmpd"](4608, __WEBPACK_IMPORTED_MODULE_27__angular_flex_layout__["r" /* ServerStylesheet */], __WEBPACK_IMPORTED_MODULE_27__angular_flex_layout__["r" /* ServerStylesheet */], []), __WEBPACK_IMPORTED_MODULE_0__angular_core__["ɵmpd"](4608, __WEBPACK_IMPORTED_MODULE_27__angular_flex_layout__["t" /* StyleUtils */], __WEBPACK_IMPORTED_MODULE_27__angular_flex_layout__["t" /* StyleUtils */], [[2, __WEBPACK_IMPORTED_MODULE_27__angular_flex_layout__["r" /* ServerStylesheet */]], [2, __WEBPACK_IMPORTED_MODULE_27__angular_flex_layout__["q" /* SERVER_TOKEN */]], __WEBPACK_IMPORTED_MODULE_0__angular_core__["PLATFORM_ID"]]), __WEBPACK_IMPORTED_MODULE_0__angular_core__["ɵmpd"](5120, __WEBPACK_IMPORTED_MODULE_0__angular_core__["APP_BOOTSTRAP_LISTENER"], function (p0_0, p0_1) { return [__WEBPACK_IMPORTED_MODULE_27__angular_flex_layout__["u" /* removeStyles */](p0_0, p0_1)]; }, [__WEBPACK_IMPORTED_MODULE_9__angular_common__["DOCUMENT"], __WEBPACK_IMPORTED_MODULE_0__angular_core__["PLATFORM_ID"]]), __WEBPACK_IMPORTED_MODULE_0__angular_core__["ɵmpd"](5120, __WEBPACK_IMPORTED_MODULE_28__angular_material_select__["a" /* MAT_SELECT_SCROLL_STRATEGY */], __WEBPACK_IMPORTED_MODULE_28__angular_material_select__["b" /* MAT_SELECT_SCROLL_STRATEGY_PROVIDER_FACTORY */], [__WEBPACK_IMPORTED_MODULE_16__angular_cdk_overlay__["c" /* Overlay */]]), __WEBPACK_IMPORTED_MODULE_0__angular_core__["ɵmpd"](5120, __WEBPACK_IMPORTED_MODULE_29__angular_material_autocomplete__["b" /* MAT_AUTOCOMPLETE_SCROLL_STRATEGY */], __WEBPACK_IMPORTED_MODULE_29__angular_material_autocomplete__["c" /* MAT_AUTOCOMPLETE_SCROLL_STRATEGY_PROVIDER_FACTORY */], [__WEBPACK_IMPORTED_MODULE_16__angular_cdk_overlay__["c" /* Overlay */]]), __WEBPACK_IMPORTED_MODULE_0__angular_core__["ɵmpd"](4608, __WEBPACK_IMPORTED_MODULE_30__angular_material_snack_bar__["b" /* MatSnackBar */], __WEBPACK_IMPORTED_MODULE_30__angular_material_snack_bar__["b" /* MatSnackBar */], [__WEBPACK_IMPORTED_MODULE_16__angular_cdk_overlay__["c" /* Overlay */], __WEBPACK_IMPORTED_MODULE_12__angular_cdk_a11y__["p" /* LiveAnnouncer */], __WEBPACK_IMPORTED_MODULE_0__angular_core__["Injector"], __WEBPACK_IMPORTED_MODULE_23__angular_cdk_layout__["a" /* BreakpointObserver */], [3, __WEBPACK_IMPORTED_MODULE_30__angular_material_snack_bar__["b" /* MatSnackBar */]]]), __WEBPACK_IMPORTED_MODULE_0__angular_core__["ɵmpd"](4608, __WEBPACK_IMPORTED_MODULE_31__merchant_featured_service__["b" /* MerchantFeaturedService */], __WEBPACK_IMPORTED_MODULE_31__merchant_featured_service__["b" /* MerchantFeaturedService */], [__WEBPACK_IMPORTED_MODULE_20__angular_common_http__["c" /* HttpClient */], __WEBPACK_IMPORTED_MODULE_30__angular_material_snack_bar__["b" /* MatSnackBar */], __WEBPACK_IMPORTED_MODULE_32__app_service__["a" /* AppService */]]), __WEBPACK_IMPORTED_MODULE_0__angular_core__["ɵmpd"](4608, __WEBPACK_IMPORTED_MODULE_31__merchant_featured_service__["a" /* BoardResolve */], __WEBPACK_IMPORTED_MODULE_31__merchant_featured_service__["a" /* BoardResolve */], [__WEBPACK_IMPORTED_MODULE_31__merchant_featured_service__["b" /* MerchantFeaturedService */]]), __WEBPACK_IMPORTED_MODULE_0__angular_core__["ɵmpd"](512, __WEBPACK_IMPORTED_MODULE_33__angular_router__["r" /* RouterModule */], __WEBPACK_IMPORTED_MODULE_33__angular_router__["r" /* RouterModule */], [[2, __WEBPACK_IMPORTED_MODULE_33__angular_router__["x" /* ɵa */]], [2, __WEBPACK_IMPORTED_MODULE_33__angular_router__["o" /* Router */]]]), __WEBPACK_IMPORTED_MODULE_0__angular_core__["ɵmpd"](512, __WEBPACK_IMPORTED_MODULE_9__angular_common__["CommonModule"], __WEBPACK_IMPORTED_MODULE_9__angular_common__["CommonModule"], []), __WEBPACK_IMPORTED_MODULE_0__angular_core__["ɵmpd"](512, __WEBPACK_IMPORTED_MODULE_10__angular_cdk_bidi__["a" /* BidiModule */], __WEBPACK_IMPORTED_MODULE_10__angular_cdk_bidi__["a" /* BidiModule */], []), __WEBPACK_IMPORTED_MODULE_0__angular_core__["ɵmpd"](256, __WEBPACK_IMPORTED_MODULE_14__angular_material_core__["f" /* MATERIAL_SANITY_CHECKS */], true, []), __WEBPACK_IMPORTED_MODULE_0__angular_core__["ɵmpd"](512, __WEBPACK_IMPORTED_MODULE_14__angular_material_core__["o" /* MatCommonModule */], __WEBPACK_IMPORTED_MODULE_14__angular_material_core__["o" /* MatCommonModule */], [[2, __WEBPACK_IMPORTED_MODULE_14__angular_material_core__["f" /* MATERIAL_SANITY_CHECKS */]]]), __WEBPACK_IMPORTED_MODULE_0__angular_core__["ɵmpd"](512, __WEBPACK_IMPORTED_MODULE_11__angular_cdk_platform__["b" /* PlatformModule */], __WEBPACK_IMPORTED_MODULE_11__angular_cdk_platform__["b" /* PlatformModule */], []), __WEBPACK_IMPORTED_MODULE_0__angular_core__["ɵmpd"](512, __WEBPACK_IMPORTED_MODULE_14__angular_material_core__["z" /* MatRippleModule */], __WEBPACK_IMPORTED_MODULE_14__angular_material_core__["z" /* MatRippleModule */], []), __WEBPACK_IMPORTED_MODULE_0__angular_core__["ɵmpd"](512, __WEBPACK_IMPORTED_MODULE_12__angular_cdk_a11y__["a" /* A11yModule */], __WEBPACK_IMPORTED_MODULE_12__angular_cdk_a11y__["a" /* A11yModule */], []), __WEBPACK_IMPORTED_MODULE_0__angular_core__["ɵmpd"](512, __WEBPACK_IMPORTED_MODULE_34__angular_material_button__["c" /* MatButtonModule */], __WEBPACK_IMPORTED_MODULE_34__angular_material_button__["c" /* MatButtonModule */], []), __WEBPACK_IMPORTED_MODULE_0__angular_core__["ɵmpd"](512, __WEBPACK_IMPORTED_MODULE_13__angular_cdk_observers__["c" /* ObserversModule */], __WEBPACK_IMPORTED_MODULE_13__angular_cdk_observers__["c" /* ObserversModule */], []), __WEBPACK_IMPORTED_MODULE_0__angular_core__["ɵmpd"](512, __WEBPACK_IMPORTED_MODULE_35__angular_material_checkbox__["c" /* MatCheckboxModule */], __WEBPACK_IMPORTED_MODULE_35__angular_material_checkbox__["c" /* MatCheckboxModule */], []), __WEBPACK_IMPORTED_MODULE_0__angular_core__["ɵmpd"](512, __WEBPACK_IMPORTED_MODULE_36__angular_material_chips__["e" /* MatChipsModule */], __WEBPACK_IMPORTED_MODULE_36__angular_material_chips__["e" /* MatChipsModule */], []), __WEBPACK_IMPORTED_MODULE_0__angular_core__["ɵmpd"](512, __WEBPACK_IMPORTED_MODULE_37__angular_cdk_portal__["g" /* PortalModule */], __WEBPACK_IMPORTED_MODULE_37__angular_cdk_portal__["g" /* PortalModule */], []), __WEBPACK_IMPORTED_MODULE_0__angular_core__["ɵmpd"](512, __WEBPACK_IMPORTED_MODULE_15__angular_cdk_scrolling__["c" /* ScrollDispatchModule */], __WEBPACK_IMPORTED_MODULE_15__angular_cdk_scrolling__["c" /* ScrollDispatchModule */], []), __WEBPACK_IMPORTED_MODULE_0__angular_core__["ɵmpd"](512, __WEBPACK_IMPORTED_MODULE_16__angular_cdk_overlay__["g" /* OverlayModule */], __WEBPACK_IMPORTED_MODULE_16__angular_cdk_overlay__["g" /* OverlayModule */], []), __WEBPACK_IMPORTED_MODULE_0__angular_core__["ɵmpd"](512, __WEBPACK_IMPORTED_MODULE_17__angular_material_dialog__["j" /* MatDialogModule */], __WEBPACK_IMPORTED_MODULE_17__angular_material_dialog__["j" /* MatDialogModule */], []), __WEBPACK_IMPORTED_MODULE_0__angular_core__["ɵmpd"](512, __WEBPACK_IMPORTED_MODULE_18__angular_material_datepicker__["i" /* MatDatepickerModule */], __WEBPACK_IMPORTED_MODULE_18__angular_material_datepicker__["i" /* MatDatepickerModule */], []), __WEBPACK_IMPORTED_MODULE_0__angular_core__["ɵmpd"](512, __WEBPACK_IMPORTED_MODULE_38__angular_material_form_field__["d" /* MatFormFieldModule */], __WEBPACK_IMPORTED_MODULE_38__angular_material_form_field__["d" /* MatFormFieldModule */], []), __WEBPACK_IMPORTED_MODULE_0__angular_core__["ɵmpd"](512, __WEBPACK_IMPORTED_MODULE_19__angular_material_icon__["c" /* MatIconModule */], __WEBPACK_IMPORTED_MODULE_19__angular_material_icon__["c" /* MatIconModule */], []), __WEBPACK_IMPORTED_MODULE_0__angular_core__["ɵmpd"](512, __WEBPACK_IMPORTED_MODULE_39__angular_material_input__["c" /* MatInputModule */], __WEBPACK_IMPORTED_MODULE_39__angular_material_input__["c" /* MatInputModule */], []), __WEBPACK_IMPORTED_MODULE_0__angular_core__["ɵmpd"](512, __WEBPACK_IMPORTED_MODULE_14__angular_material_core__["q" /* MatLineModule */], __WEBPACK_IMPORTED_MODULE_14__angular_material_core__["q" /* MatLineModule */], []), __WEBPACK_IMPORTED_MODULE_0__angular_core__["ɵmpd"](512, __WEBPACK_IMPORTED_MODULE_14__angular_material_core__["x" /* MatPseudoCheckboxModule */], __WEBPACK_IMPORTED_MODULE_14__angular_material_core__["x" /* MatPseudoCheckboxModule */], []), __WEBPACK_IMPORTED_MODULE_0__angular_core__["ɵmpd"](512, __WEBPACK_IMPORTED_MODULE_40__angular_material_divider__["b" /* MatDividerModule */], __WEBPACK_IMPORTED_MODULE_40__angular_material_divider__["b" /* MatDividerModule */], []), __WEBPACK_IMPORTED_MODULE_0__angular_core__["ɵmpd"](512, __WEBPACK_IMPORTED_MODULE_41__angular_material_list__["d" /* MatListModule */], __WEBPACK_IMPORTED_MODULE_41__angular_material_list__["d" /* MatListModule */], []), __WEBPACK_IMPORTED_MODULE_0__angular_core__["ɵmpd"](512, __WEBPACK_IMPORTED_MODULE_22__angular_material_menu__["e" /* MatMenuModule */], __WEBPACK_IMPORTED_MODULE_22__angular_material_menu__["e" /* MatMenuModule */], []), __WEBPACK_IMPORTED_MODULE_0__angular_core__["ɵmpd"](512, __WEBPACK_IMPORTED_MODULE_42__angular_material_progress_bar__["b" /* MatProgressBarModule */], __WEBPACK_IMPORTED_MODULE_42__angular_material_progress_bar__["b" /* MatProgressBarModule */], []), __WEBPACK_IMPORTED_MODULE_0__angular_core__["ɵmpd"](512, __WEBPACK_IMPORTED_MODULE_43__angular_material_sidenav__["h" /* MatSidenavModule */], __WEBPACK_IMPORTED_MODULE_43__angular_material_sidenav__["h" /* MatSidenavModule */], []), __WEBPACK_IMPORTED_MODULE_0__angular_core__["ɵmpd"](512, __WEBPACK_IMPORTED_MODULE_44__angular_material_toolbar__["b" /* MatToolbarModule */], __WEBPACK_IMPORTED_MODULE_44__angular_material_toolbar__["b" /* MatToolbarModule */], []), __WEBPACK_IMPORTED_MODULE_0__angular_core__["ɵmpd"](512, __WEBPACK_IMPORTED_MODULE_23__angular_cdk_layout__["c" /* LayoutModule */], __WEBPACK_IMPORTED_MODULE_23__angular_cdk_layout__["c" /* LayoutModule */], []), __WEBPACK_IMPORTED_MODULE_0__angular_core__["ɵmpd"](512, __WEBPACK_IMPORTED_MODULE_24__angular_material_tooltip__["e" /* MatTooltipModule */], __WEBPACK_IMPORTED_MODULE_24__angular_material_tooltip__["e" /* MatTooltipModule */], []), __WEBPACK_IMPORTED_MODULE_0__angular_core__["ɵmpd"](512, __WEBPACK_IMPORTED_MODULE_45__swimlane_ngx_dnd_release_ngx_dnd_module__["a" /* NgxDnDModule */], __WEBPACK_IMPORTED_MODULE_45__swimlane_ngx_dnd_release_ngx_dnd_module__["a" /* NgxDnDModule */], []), __WEBPACK_IMPORTED_MODULE_0__angular_core__["ɵmpd"](512, __WEBPACK_IMPORTED_MODULE_26__angular_forms__["D" /* ɵba */], __WEBPACK_IMPORTED_MODULE_26__angular_forms__["D" /* ɵba */], []), __WEBPACK_IMPORTED_MODULE_0__angular_core__["ɵmpd"](512, __WEBPACK_IMPORTED_MODULE_26__angular_forms__["p" /* FormsModule */], __WEBPACK_IMPORTED_MODULE_26__angular_forms__["p" /* FormsModule */], []), __WEBPACK_IMPORTED_MODULE_0__angular_core__["ɵmpd"](512, __WEBPACK_IMPORTED_MODULE_26__angular_forms__["A" /* ReactiveFormsModule */], __WEBPACK_IMPORTED_MODULE_26__angular_forms__["A" /* ReactiveFormsModule */], []), __WEBPACK_IMPORTED_MODULE_0__angular_core__["ɵmpd"](512, __WEBPACK_IMPORTED_MODULE_27__angular_flex_layout__["n" /* MediaQueriesModule */], __WEBPACK_IMPORTED_MODULE_27__angular_flex_layout__["n" /* MediaQueriesModule */], []), __WEBPACK_IMPORTED_MODULE_0__angular_core__["ɵmpd"](512, __WEBPACK_IMPORTED_MODULE_27__angular_flex_layout__["x" /* ɵc */], __WEBPACK_IMPORTED_MODULE_27__angular_flex_layout__["x" /* ɵc */], []), __WEBPACK_IMPORTED_MODULE_0__angular_core__["ɵmpd"](512, __WEBPACK_IMPORTED_MODULE_27__angular_flex_layout__["g" /* FlexLayoutModule */], __WEBPACK_IMPORTED_MODULE_27__angular_flex_layout__["g" /* FlexLayoutModule */], [[2, __WEBPACK_IMPORTED_MODULE_27__angular_flex_layout__["q" /* SERVER_TOKEN */]], __WEBPACK_IMPORTED_MODULE_0__angular_core__["PLATFORM_ID"]]), __WEBPACK_IMPORTED_MODULE_0__angular_core__["ɵmpd"](512, __WEBPACK_IMPORTED_MODULE_46__fuse_directives_directives__["a" /* FuseDirectivesModule */], __WEBPACK_IMPORTED_MODULE_46__fuse_directives_directives__["a" /* FuseDirectivesModule */], []), __WEBPACK_IMPORTED_MODULE_0__angular_core__["ɵmpd"](512, __WEBPACK_IMPORTED_MODULE_47__fuse_pipes_pipes_module__["a" /* FusePipesModule */], __WEBPACK_IMPORTED_MODULE_47__fuse_pipes_pipes_module__["a" /* FusePipesModule */], []), __WEBPACK_IMPORTED_MODULE_0__angular_core__["ɵmpd"](512, __WEBPACK_IMPORTED_MODULE_48__fuse_shared_module__["a" /* FuseSharedModule */], __WEBPACK_IMPORTED_MODULE_48__fuse_shared_module__["a" /* FuseSharedModule */], []), __WEBPACK_IMPORTED_MODULE_0__angular_core__["ɵmpd"](512, __WEBPACK_IMPORTED_MODULE_49__fuse_components_confirm_dialog_confirm_dialog_module__["a" /* FuseConfirmDialogModule */], __WEBPACK_IMPORTED_MODULE_49__fuse_components_confirm_dialog_confirm_dialog_module__["a" /* FuseConfirmDialogModule */], []), __WEBPACK_IMPORTED_MODULE_0__angular_core__["ɵmpd"](512, __WEBPACK_IMPORTED_MODULE_50__fuse_components_material_color_picker_material_color_picker_module__["a" /* FuseMaterialColorPickerModule */], __WEBPACK_IMPORTED_MODULE_50__fuse_components_material_color_picker_material_color_picker_module__["a" /* FuseMaterialColorPickerModule */], []), __WEBPACK_IMPORTED_MODULE_0__angular_core__["ɵmpd"](512, __WEBPACK_IMPORTED_MODULE_14__angular_material_core__["v" /* MatOptionModule */], __WEBPACK_IMPORTED_MODULE_14__angular_material_core__["v" /* MatOptionModule */], []), __WEBPACK_IMPORTED_MODULE_0__angular_core__["ɵmpd"](512, __WEBPACK_IMPORTED_MODULE_28__angular_material_select__["d" /* MatSelectModule */], __WEBPACK_IMPORTED_MODULE_28__angular_material_select__["d" /* MatSelectModule */], []), __WEBPACK_IMPORTED_MODULE_0__angular_core__["ɵmpd"](512, __WEBPACK_IMPORTED_MODULE_29__angular_material_autocomplete__["e" /* MatAutocompleteModule */], __WEBPACK_IMPORTED_MODULE_29__angular_material_autocomplete__["e" /* MatAutocompleteModule */], []), __WEBPACK_IMPORTED_MODULE_0__angular_core__["ɵmpd"](512, __WEBPACK_IMPORTED_MODULE_30__angular_material_snack_bar__["d" /* MatSnackBarModule */], __WEBPACK_IMPORTED_MODULE_30__angular_material_snack_bar__["d" /* MatSnackBarModule */], []), __WEBPACK_IMPORTED_MODULE_0__angular_core__["ɵmpd"](512, __WEBPACK_IMPORTED_MODULE_1__merchant_featured_module__["a" /* MerchantFeaturedModule */], __WEBPACK_IMPORTED_MODULE_1__merchant_featured_module__["a" /* MerchantFeaturedModule */], []), __WEBPACK_IMPORTED_MODULE_0__angular_core__["ɵmpd"](256, __WEBPACK_IMPORTED_MODULE_22__angular_material_menu__["a" /* MAT_MENU_DEFAULT_OPTIONS */], { overlapTrigger: true, xPosition: "after", yPosition: "below" }, []), __WEBPACK_IMPORTED_MODULE_0__angular_core__["ɵmpd"](256, __WEBPACK_IMPORTED_MODULE_43__angular_material_sidenav__["a" /* MAT_DRAWER_DEFAULT_AUTOSIZE */], false, []), __WEBPACK_IMPORTED_MODULE_0__angular_core__["ɵmpd"](256, __WEBPACK_IMPORTED_MODULE_24__angular_material_tooltip__["a" /* MAT_TOOLTIP_DEFAULT_OPTIONS */], { showDelay: 0, hideDelay: 0, touchendHideDelay: 1500 }, []), __WEBPACK_IMPORTED_MODULE_0__angular_core__["ɵmpd"](1024, __WEBPACK_IMPORTED_MODULE_33__angular_router__["m" /* ROUTES */], function () { return [[{ path: "**", component: __WEBPACK_IMPORTED_MODULE_51__merchant_featured_board_merchant_featured_board_component__["a" /* MerchantFeaturedBoardComponent */], children: [], resolve: { merchants: __WEBPACK_IMPORTED_MODULE_31__merchant_featured_service__["b" /* MerchantFeaturedService */], board: __WEBPACK_IMPORTED_MODULE_31__merchant_featured_service__["a" /* BoardResolve */] } }]]; }, [])]); });



/***/ }),

/***/ "./src/app/main/content/pages/merchant-featured/merchant-featured.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MerchantFeaturedModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__merchant_featured_service__ = __webpack_require__("./src/app/main/content/pages/merchant-featured/merchant-featured.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__merchant_featured_board_merchant_featured_board_component__ = __webpack_require__("./src/app/main/content/pages/merchant-featured/merchant-featured-board/merchant-featured-board.component.ts");



var routes = [
    {
        path: '**',
        component: __WEBPACK_IMPORTED_MODULE_2__merchant_featured_board_merchant_featured_board_component__["a" /* MerchantFeaturedBoardComponent */],
        children: [],
        resolve: {
            merchants: __WEBPACK_IMPORTED_MODULE_1__merchant_featured_service__["b" /* MerchantFeaturedService */],
            board: __WEBPACK_IMPORTED_MODULE_1__merchant_featured_service__["a" /* BoardResolve */]
        }
    }
];
var MerchantFeaturedModule = /** @class */ (function () {
    function MerchantFeaturedModule() {
    }
    return MerchantFeaturedModule;
}());



/***/ }),

/***/ "./src/app/main/content/pages/merchant-featured/merchant-featured.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return MerchantFeaturedService; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BoardResolve; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__("./node_modules/@angular/common/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_BehaviorSubject__ = __webpack_require__("./node_modules/rxjs/_esm5/BehaviorSubject.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_material__ = __webpack_require__("./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_service__ = __webpack_require__("./src/app/app.service.ts");




var MerchantFeaturedService = /** @class */ (function () {
    function MerchantFeaturedService(http, snackBar, appService) {
        this.http = http;
        this.snackBar = snackBar;
        this.appService = appService;
        this.onBoardsChanged = new __WEBPACK_IMPORTED_MODULE_1_rxjs_BehaviorSubject__["BehaviorSubject"]([]);
        this.onBoardChanged = new __WEBPACK_IMPORTED_MODULE_1_rxjs_BehaviorSubject__["BehaviorSubject"]([]);
    }
    /**
     * Resolve
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    MerchantFeaturedService.prototype.resolve = function (route, state) {
        var _this = this;
        this.routeParams = route.params;
        return new Promise(function (resolve, reject) {
            Promise.all([
                _this.getBoards(),
                _this.getMerchants()
            ]).then(function (_a) {
                var boards = _a[0], merchants = _a[1];
                resolve();
            }, reject);
        });
    };
    MerchantFeaturedService.prototype.getBoards = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.http.get('api/scrumboard-boards')
                .subscribe(function (response) {
                debugger;
                _this.boards = response;
                _this.onBoardsChanged.next(_this.boards);
                resolve(_this.boards);
            }, reject);
        });
    };
    MerchantFeaturedService.prototype.getMerchants = function () {
        var _this = this;
        debugger;
        return new Promise(function (resolve, reject) {
            _this.http.get(_this.appService.merchantService)
                .subscribe(function (response) {
                debugger;
                _this.merchants = response.data;
                resolve(_this.merchants);
            }, reject);
        });
    };
    MerchantFeaturedService.prototype.getBoard = function (boardId) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.http.get('api/scrumboard-boards/' + boardId)
                .subscribe(function (response) {
                debugger;
                _this.board = response;
                _this.onBoardChanged.next(_this.board);
                _this.getFeaturedMerchants();
                resolve(_this.board);
            }, reject);
        });
    };
    MerchantFeaturedService.prototype.getFeaturedMerchants = function () {
        var _this = this;
        debugger;
        return new Promise(function (resolve, reject) {
            _this.http.get(_this.appService.merchantService + 'featured')
                .subscribe(function (response) {
                debugger;
                _this.featuredMerchants = response.data;
                for (var i = 0; i < _this.featuredMerchants.length; i++) {
                    var listIdCard = _this.featuredMerchants[i].merchant.id;
                    _this.board.lists[0].idCards.push(listIdCard);
                    var card = {};
                    card.id = _this.featuredMerchants[i].merchant.id;
                    card.name = _this.featuredMerchants[i].merchant.name;
                    card.affiliate = _this.featuredMerchants[i].merchant.affiliateNetwork.name;
                    card.description = _this.featuredMerchants[i].merchant.description;
                    card.idMembers = [];
                    card.idMembers.push(_this.featuredMerchants[i].merchant.accountManager.id);
                    var member = {};
                    member.id = _this.featuredMerchants[i].merchant.accountManager.id;
                    member.name = "usman";
                    member.avatar = 'assets/images/avatars/alice.jpg';
                    _this.board.cards.push(card);
                    _this.board.members.push(member);
                }
                //this.board = response;
                _this.onBoardChanged.next(_this.board);
                resolve(_this.board);
            }, reject);
        });
    };
    MerchantFeaturedService.prototype.addCard = function (listId, newCard, member) {
        this.board.lists.map(function (list) {
            if (list.id === listId) {
                return list.idCards.push(newCard.id);
            }
        });
        this.board.members.push(member);
        this.board.cards.push(newCard);
        return this.updateBoard();
    };
    // addList(newList)
    // {
    //     debugger;
    //     this.board.lists.push(newList);
    //     return this.updateBoard();
    // }
    MerchantFeaturedService.prototype.addList = function () {
        debugger;
        // this.board.lists.push(newList);
        return this.updateBoard();
    };
    MerchantFeaturedService.prototype.removeList = function (listId) {
        var list = this.board.lists.find(function (_list) {
            return _list.id === listId;
        });
        for (var _i = 0, _a = list.idCards; _i < _a.length; _i++) {
            var cardId = _a[_i];
            this.removeCard(cardId);
        }
        var index = this.board.lists.indexOf(list);
        this.board.lists.splice(index, 1);
        return this.updateBoard();
    };
    MerchantFeaturedService.prototype.removeCard = function (cardId, listId) {
        var card = this.board.cards.find(function (_card) {
            return _card.id === cardId;
        });
        if (listId) {
            var list = this.board.lists.find(function (_list) {
                return listId === _list.id;
            });
            list.idCards.splice(list.idCards.indexOf(cardId), 1);
        }
        this.board.cards.splice(this.board.cards.indexOf(card), 1);
        this.updateBoard();
    };
    MerchantFeaturedService.prototype.updateBoard = function () {
        var _this = this;
        debugger;
        // var featuredMerchant: any = {
        //     position: '',
        //     merchant: {
        //         id: 0
        //     }
        // }
        var featuredMerchantsList = [];
        for (var i = 0; i < this.board.lists[0].idCards.length; i++) {
            var featuredMerchant = {
                position: '',
                merchant: {
                    id: 0
                }
            };
            featuredMerchant.position = i + 1;
            featuredMerchant.merchant.id = this.board.lists[0].idCards[i];
            featuredMerchantsList.push(featuredMerchant);
            //   var pos = findIndex(this.merchants.splice(featuredMerchant,1);
        }
        return new Promise(function (resolve, reject) {
            _this.http.put(_this.appService.merchantService + 'featured', featuredMerchantsList)
                .subscribe(function (response) {
                var msg = "Response recieved";
                if ('meta' in response) {
                    msg = response["meta"].message;
                }
                _this.onBoardChanged.next(_this.board);
                _this.snackBar.open(msg, "Done", {
                    duration: 2000,
                });
                resolve(_this.board);
            }, reject);
        });
    };
    MerchantFeaturedService.prototype.updateCard = function (newCard) {
        this.board.cards.map(function (_card) {
            if (_card.id === newCard.id) {
                return newCard;
            }
        });
        this.updateBoard();
    };
    MerchantFeaturedService.prototype.createNewBoard = function (board) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.http.post('api/scrumboard-boards/' + board.id, board)
                .subscribe(function (response) {
                resolve(board);
            }, reject);
        });
    };
    return MerchantFeaturedService;
}());

var BoardResolve = /** @class */ (function () {
    function BoardResolve(scrumboardService) {
        this.scrumboardService = scrumboardService;
    }
    BoardResolve.prototype.resolve = function (route) {
        // return this.scrumboardService.getBoard(route.paramMap.get('boardId'));
        this.scrumboardService.getMerchants();
        return this.scrumboardService.getBoard('positions');
    };
    return BoardResolve;
}());



/***/ })

});
//# sourceMappingURL=merchant-featured.module.chunk.js.map