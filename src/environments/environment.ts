// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
		qa: false,
    production: false,
    hmr: false,
    // apiUrl: 'http://websvc.westus.cloudapp.azure.com/',
     apiUrl: 'http://192.168.10.9:3000/',
    // apiUrl: 'http://18.156.17.79:3000/',

   //  apiUrl    : 'http://40.112.165.99/',
    cdnUrl: 'https://quotientmedia.blob.core.windows.net/mediacontainer/',
    instrumentationKey: ''
};
