export const environment = {
		qa: false,
    production: true,
    hmr       : false,
    apiUrl: 'http://websvc.westus.cloudapp.azure.com/',
    cdnUrl: 'https://quotientmedia.blob.core.windows.net/mediacontainer/',
    instrumentationKey: '67000639-c41a-4f13-a370-c19b6da62726'
};
