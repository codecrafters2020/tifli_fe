import { NgModule } from '@angular/core';

import { FuseIfOnDomDirective } from '@fuse/directives/fuse-if-on-dom/fuse-if-on-dom.directive';
import { DisableIfUnauthorizedDirective } from '@fuse/directives/disable-if/disable-if-unauthorized.directive';
import { HideIfUnauthorizedDirective } from '@fuse/directives/hide-if/hide-if-unauthorized.directive';
import { FusePerfectScrollbarDirective } from '@fuse/directives/fuse-perfect-scrollbar/fuse-perfect-scrollbar.directive';
import { FuseMatSidenavHelperDirective, FuseMatSidenavTogglerDirective } from '@fuse/directives/fuse-mat-sidenav/fuse-mat-sidenav.directive';

@NgModule({
    declarations: [
        FuseIfOnDomDirective,
        FuseMatSidenavHelperDirective,
        FuseMatSidenavTogglerDirective,
        FusePerfectScrollbarDirective,
        DisableIfUnauthorizedDirective,
        HideIfUnauthorizedDirective
    ],
    imports     : [],
    exports     : [
        FuseIfOnDomDirective,
        FuseMatSidenavHelperDirective,
        FuseMatSidenavTogglerDirective,
        FusePerfectScrollbarDirective,
        DisableIfUnauthorizedDirective,
        HideIfUnauthorizedDirective
    ]
})
export class FuseDirectivesModule
{
}
