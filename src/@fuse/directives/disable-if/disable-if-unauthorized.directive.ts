import { Directive, ElementRef, OnInit, Input } from '@angular/core';
import { AuthGroup } from '../../../app/auth/models/auth.types';
import { AuthService } from '../../../app/auth/auth.service';

@Directive({
    selector: '[DisableIfUnauthorized]'
})
export class DisableIfUnauthorizedDirective implements OnInit {
    @Input('DisableIfUnauthorized') permission: AuthGroup; // Required permission passed in
    constructor(private el: ElementRef, private authService: AuthService) { }
    ngOnInit() {
        if (!this.authService.hasPermission(this.permission)) {
              this.el.nativeElement.disabled = true;
        }
    }
}