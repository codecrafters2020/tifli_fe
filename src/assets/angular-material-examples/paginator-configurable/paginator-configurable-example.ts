import { Component } from '@angular/core';
import { PageEvent } from '@angular/material';

/**
 * @title Configurable paginator
 */
@Component({
    selector   : 'paginator-configurable-example',
    templateUrl: 'paginator-configurable-example.html'
})
export class PaginatorConfigurableExample
{
    // MatPaginator Inputs
    length = 100;
    pageSize = 25;
    pageSizeOptions = [25, 50, 100];

    // MatPaginator Output
    pageEvent: PageEvent;

    setPageSizeOptions(setPageSizeOptionsInput: string)
    {
        this.pageSizeOptions = setPageSizeOptionsInput.split(',').map(str => +str);
    }
}
