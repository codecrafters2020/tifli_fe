export const navigation = [
    {
        'id': 'dashboards',
        'title': 'Dashboards',
        'translate': 'NAV.DASHBOARDS',
        'type': 'item',
        'icon': 'dashboard',
        'url': '/apps/dashboards/project'
    },
    {
        'id'      : 'merchant-coupons',
        'title'   : 'Search Merchant Coupons',
        'type'    : 'item',
        'icon'    : 'search',
        'url'  : '/pages/merchant-coupons/list'
    },
    

    {
        'id': 'offer-provisioning',
        'title': 'Offer Provisioning',
        'translate': 'NAV.OFFER_PROVISIONING',
        'type': 'group',
        'icon': 'pages',
        'children': [
            {
                'id': 'merchant',
                'title': 'Merchant',
                'type': 'collapse',
                'icon': 'card_travel',
                'children': [
                    {
                        'id': 'list-merchants',
                        'title': 'List Merchants',
                        'type': 'item',
                        'url': '/pages/merchant/list'
                    },
                    {
                        'id': 'add-merchant',
                        'title': 'Add Merchant',
                        'type': 'item',
                        'url': '/pages/merchant/add'
                    },
                    {
                        'id': 'monetized-merchants',
                        'title': 'Monetized Merchants',
                        'type': 'item',
                        'url': '/pages/merchant/monetized'
                    },
                    {
                        'id': 'categories-merchant',
                        'title': 'Categories',
                        'type': 'collapse',
                        'children': [
                            {
                                'id': 'list-merchant-categories',
                                'title': 'List Categories',
                                'type': 'item',
                                'url': '/pages/merchant-category/list'
                            },
                            {
                                'id': 'add-merchant-categories',
                                'title': 'Add Categories',
                                'type': 'item',
                                'url': '/pages/merchant-category/add'
                            }
                        ]
                    },
                ]
            },
            {
                'id'      : 'review-merchant',
                'title'   : 'Review Merchant',
                'type'    : 'collapse',
                'icon'    : 'pages',
                'children': [
                    {
                        'id'   : 'review-merchant-assign-list',
                        'title': 'Merchant Assignment',
                        'type' : 'item',
                        'url'  : '/pages/merchant-review/assign-list'
                    },
                    {
                        'id'   : 'review-merchant-list',
                        'title': 'Review Merchants',
                        'type' : 'item',
                        'url'  : '/pages/merchant-review/list'
                    }
                ]
            },
            {
                'id': 'coupons',
                'title': 'Coupons',
                'type': 'collapse',
                'icon': 'local_offer',
                'children': [
                    {
                        'id': 'list-coupons',
                        'title': 'List Coupons',
                        'type': 'item',
                        'url': '/pages/coupon/list'
                    },
                    {
                        'id': 'add-coupon',
                        'title': 'Add Coupon',
                        'type': 'item',
                        'url': '/pages/coupon/add'
                    }
                ]
            },
            {
                'id': 'seasonal-events',
                'title': 'Seasonal Pages',
                'type': 'collapse',
                'icon': 'event',
                'children': [
                    {
                        'id': 'list-seasonal',
                        'title': 'List Seasonal Pages',
                        'type': 'item',
                        'url': '/pages/seasonal-events/list'
                    },
                    {
                        'id': 'add-seasonal-page',
                        'title': 'Add Seasonal Page',
                        'type': 'item',
                        'url': '/pages/seasonal-events/add'
                    }
                ]
            },
            {
                'id': 'affiliate-network',
                'title': 'Affiliate Networks',
                'type': 'collapse',
                'icon': 'group_work',
                'children': [
                    {
                        'id': 'list-affiliate-networks',
                        'title': 'List Affiliate Networks',
                        'type': 'item',
                        'url': '/pages/affiliate-network/list'
                    },
                    {
                        'id': 'add-affiliate-network',
                        'title': 'Add Affiliate Network',
                        'type': 'item',
                        'url': '/pages/affiliate-network/add'
                    }
                ]
            }
        ]
    },
    {
        'id': 'commission-and-cashback',
        'title': 'Commission and Cash Backs',
        'translate': 'NAV.OFFER_PROVISIONING',
        'type': 'group',
        'icon': 'pages',
        'children': [
            {
                'id': 'commission',
                'title': 'Commission',
                'type': 'item',
                'icon': 'monetization_on',
                'url': '/pages/commission/list'
            },
            {
                'id': 'customer-cashbacks',
                'title': 'Customer Cashbacks',
                'type': 'item',
                'icon': 'update',
                'url': '/pages/customer-cashbacks/list'
            },
            {
                'id': 'payout-requests',
                'title': 'Payout Requests',
                'type': 'item',
                'icon': 'attach_money',
                'url': '/pages/payout-requests/list'
            },
            {
                'id': 'sync-commision',
                'title': 'Sync Commission',
                'type': 'item',
                'icon': 'cloud_download',
                'url': '/pages/sync-commission/'
            }
        ]
    },
    {
        'id': 'placement',
        'title': 'Placement',
        'translate': 'NAV.OFFER_PROVISIONING',
        'type': 'group',
        'icon': 'pages',
        'children': [
            {
                'id': 'scheduled-placements',
                'title': 'Scheduled Placements',
                'type': 'collapse',
                'icon': 'event',
                'url': '/pages/scheduled-placements',
                'children': [
                    {
                        'id': 'web-pages',
                        'title': 'Special Pages',
                        'type': 'item',
                        'url': '/pages/scheduled-placement-pages/list'
                    },
                    {
                        'id': 'seasonal-pages',
                        'title': 'Seasonal Pages',
                        'type': 'item',
                        'url': '/pages/scheduled-placement-seasonal/list'
                    },
                    {
                        'id': 'upcoming-sales',
                        'title': 'Upcoming Sales',
                        'type': 'item',
                        'url': '/pages/scheduled-placement-upcoming-sales/list'
                    },
                    {
                        'id': 'home-event',
                        'title': 'Home Navigation Event',
                        'type': 'item',
                        'url': '/pages/scheduled-placement-home-event/list'
                    },
                    {
                        'id': 'top-stores',
                        'title': 'Top Stores',
                        'type': 'item',
                        'url': '/pages/scheduled-placement-top-stores/list'
                    },
                    {
                        'id': 'popular-stores',
                        'title': 'Popular Stores',
                        'type': 'item',
                        'url': '/pages/scheduled-placement-popular-stores/list'
                    },
                    {
                        'id': 'search-merchant-links',
                        'title': 'Search Merchant Links',
                        'type': 'item',
                        'url': '/pages/scheduled-placement-search-merchant-links/list'
                    }
                ]
            },
            {
                'id': 'faculty',
                'title': 'Faculty',
                'type': 'collapse',
                'icon': 'grid_on',
                'children': [
                    {
                        'id': 'list-faculty',
                        'title': 'List Faculty',
                        'type': 'item',
                        'url': '/pages/faculty/list'
                    },
                    {
                        'id': 'add-faculty',
                        'title': 'Add Faculty',
                        'type': 'item',
                        'url': '/pages/faculty/add'
                    },
                ]
            },
            {
                'id': 'carousel-placements',
                'title': 'Carousel Placements',
                'type': 'item',
                'icon': 'photo_library',
                'url': '/pages/carousel',
            },
            {
                'id': 'faculty',
                'title': 'Faculty',
                'type': 'item',
                'icon': 'photo_library',
                'children': [
                    {
                        'id': 'list-faculty',
                        'title': 'List faculty',
                        'type': 'item',
                        'url': '/pages/faculty/list'
                    },
                    {
                        'id': 'add-faculty',
                        'title': 'Add faculty',
                        'type': 'item',
                        'url': '/pages/faculty/add'
                    }
                ]
              
            },

            {
                'id': 'student',
                'title': 'student',
                'type': 'collapse',
                'icon': 'grid_on',
                'children': [
                    {
                        'id': 'list-student',
                        'title': 'List student',
                        'type': 'item',
                        'url': '/pages/student/list'
                    },
                    {
                        'id': 'add-student',
                        'title': 'Add student',
                        'type': 'item',
                        'url': '/pages/student/add'
                    },
                ]
            },
            
            {
                'id': 'student',
                'title': 'Students',
                'type': 'item',
                'icon': 'photo_library',
                'children': [
                    {
                        'id': 'list-student',
                        'title': 'List Student',
                        'type': 'item',
                        'url': '/pages/student/list'
                    },
                    {
                        'id': 'add-student',
                        'title': 'Add Student',
                        'type': 'item',
                        'url': '/pages/student/add'
                    }
                ]
              
            },
            // {
            //     'id'      : 'featured-list',
            //     'title'   : 'Featured List',
            //     'type'    : 'item',
            //     'icon'    : 'favorite',
            //     'url'  : '/pages/merchant/temp',
            // },
            {
                'id': 'featured-merchants-list',
                'title': 'Featured Merchants List',
                'type': 'item',
                'icon': 'favorite_border',
                'url': '/pages/featured-merchants',
            },
            
            {
                'id'   : 'add-featured coupon',
                'title': 'Featured Coupon',
                'type' : 'item',
                'icon' : 'local_offer',
                'url'  : '/pages/featured-coupon/list'
             },
            {
                'id': 'related-offers',
                'title': 'Related Offers',
                'type': 'item',
                'icon': 'local_offer',
                'url': '/pages/merchant/temp',
            }
        ]
    },
    {
        'id': 'administration',
        'title': 'Administration',
        'translate': 'NAV.OFFER_PROVISIONING',
        'type': 'group',
        'icon': 'pages',
        'children': [
            {
                'id': 'list-users',
                'title': 'List Users',
                'type': 'item',
                'icon': 'group',
                'url': '/pages/administration/list-user'
            },
            {
                'id': 'add-user',
                'title': 'Add User',
                'type': 'item',
                'icon': 'person_add',
                'url': '/pages/administration/add-user',
            },
            {
                'id': 'list-user-roles',
                'title': 'List User Roles',
                'type': 'item',
                'icon': 'people_outline',
                'url': '/pages/administration/list-role',
            },
            {
                'id': 'add-user-role',
                'title': 'Add User Role',
                'type': 'item',
                'icon': 'lock_open',
                'url': '/pages/administration/add-role',
            },
            {
                'id': 'list-user-activity',
                'title': 'List User Activities',
                'type': 'item',
                'icon': 'lock_open',
                'url': '/pages/administration/list-user-activity',
            }
        ]
    },
    {
        'id': 'account',
        'title': 'Account',
        'translate': 'NAV.OFFER_PROVISIONING',
        'type': 'group',
        'icon': 'pages',
        'children': [
            {
                'id'      : 'reset-password',
                'title'   : 'Reset Password',
                'type'    : 'item',
                'icon'    : 'lock',
                'url'  : '/pages/administration/reset-password'
            },
            {
                'id'      : 'logout',
                'title'   : 'Logout',
                'type'    : 'item',
                'icon'    : 'lock',
                'click' : 'logout',
                'url'  : '/pages/account/logout'
            }
        ]
    }
];
