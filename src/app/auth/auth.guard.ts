import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { AuthService } from './auth.service'

@Injectable()
export class AuthGuard implements CanActivate {

    constructor(private router: Router, private authService: AuthService) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        if ((this.authService.getUser()) && (this.authService.getToken()) && (this.authService.getPermissions())) {
            var allowed = this.authService.hasPermission(state.url);
        }

        if (allowed) {
            return true;
        } else {
            // not logged in so redirect to login page
            this.router.navigate(['/']);
            return false;
            // return true;
        }

    }

    canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
        return this.canActivate(route, state);
    }

}