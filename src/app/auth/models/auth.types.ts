export type AuthGroup = 'VIEW_ONLY' | 
						'UPDATE_FULL' | 
						'CREATE' | 
						'DELETE' | 
						'/pages/merchant/list' | 
						'/pages/seasonal-events/list';