import { Component, Input, OnDestroy, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';

import { FusePerfectScrollbarDirective } from '@fuse/directives/fuse-perfect-scrollbar/fuse-perfect-scrollbar.directive';
import { FuseSidebarService } from '@fuse/components/sidebar/sidebar.service';

import { navigation } from 'app/navigation/navigation';
import { FuseNavigationService } from '@fuse/components/navigation/navigation.service';

import * as _ from 'lodash';

import { AuthService } from '../../auth/auth.service';

@Component({
    selector     : 'fuse-navbar',
    templateUrl  : './navbar.component.html',
    styleUrls    : ['./navbar.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class FuseNavbarComponent implements OnDestroy
{
    private fusePerfectScrollbar: FusePerfectScrollbarDirective;

    @ViewChild(FusePerfectScrollbarDirective) set directive(theDirective: FusePerfectScrollbarDirective)
    {
        if ( !theDirective )
        {
            return;
        }

        this.fusePerfectScrollbar = theDirective;

        this.navigationServiceWatcher =
            this.navigationService.onItemCollapseToggled.subscribe(() => {
                this.fusePerfectScrollbarUpdateTimeout = setTimeout(() => {
                    this.fusePerfectScrollbar.update();
                }, 310);
            });
    }

    @Input() layout;
    navigation: any;
    navigationServiceWatcher: Subscription;
    fusePerfectScrollbarUpdateTimeout;

    constructor(
        private sidebarService: FuseSidebarService,
        private navigationService: FuseNavigationService,
        private authService: AuthService
    )
    {
        // Navigation data
        this.navigation = navigation;

        /*let dummyPermissions = [
            '/apps/dashboards/analytics',
            '/pages/merchant/list',
            '/pages/merchant/add',
            '/pages/merchant/monetized',
            '/pages/merchant-category/list',
            '/pages/merchant-category/add',
            '/pages/carousel',
            '/pages/seasonal-events/list',
            '/pages/administration/list-role',
        ];*/

        /*for (let i in this.navigation) {
            var newNavigation = findNestedV1(this.navigation[i], 'url', '/pages/seasonal-events/list');
            if (newNavigation) {
                newNavigation = this.navigation[i];
                break;
            }
        }*/

        let permissions = this.authService.getPermissions();
        // let sideBar = this.findNestedV4(_.cloneDeep(this.navigation), 'url', permissions);
        // this.navigation = sideBar;

        // Default layout
        this.layout = 'vertical';
    }

    ngOnInit()
    {
    }

    ngOnDestroy()
    {
        if ( this.fusePerfectScrollbarUpdateTimeout )
        {
            clearTimeout(this.fusePerfectScrollbarUpdateTimeout);
        }

        if ( this.navigationServiceWatcher )
        {
            this.navigationServiceWatcher.unsubscribe();
        }
    }

    toggleSidebarOpened(key)
    {
        this.sidebarService.getSidebar(key).toggleOpen();
    }

    toggleSidebarFolded(key)
    {
        this.sidebarService.getSidebar(key).toggleFold();
    }

    /*findNestedV1(obj, key, value) 
    {
        // Base case
        if (obj[key] === value) {
            return obj;
        } else {
            for (let i in obj) {
                let curr = obj[i];    
                if (typeof curr == 'object') {
                    let found = this.findNestedV1(obj[i], key, value);
                    if (found) {
                        // If the object was found in the recursive call, bubble it up.
                        if (Array.isArray(obj)){
                            obj = [];
                            obj.push(found);
                            return obj;
                        }
                        if (typeof obj == 'object'){
                            obj.children = [];
                            obj.children.push(found);                                    
                            return obj;
                        }
                    }
                }
            }
        }
    }*/

    /*findNestedV2(obj, key, values) 
    {
        // Base case
        let selectedObjects = {};
        for (let index = 0; index < values.length; index++) {
            for (let val of obj) {
                if (val[key] === values[index]) {
                    selectedObjects[index] = val;
                }
                if ((!_.isEmpty(selectedObjects)) && (index === values.length - 1)) {
                    return selectedObjects;
                }
            }
        }
        for (let i in obj) {
            let curr = obj[i];    
            if (typeof curr == 'object') {
                let found = this.findNestedV2(obj[i], key, values);
                if (found) {
                    // If the object was found in the recursive call, bubble it up.
                    if (Array.isArray(obj)){
                        obj = [];
                        obj.push(found);
                        return obj;
                    }
                    if (typeof obj == 'object'){
                        obj.children = [];
                        obj.children.push(found);                                    
                        return obj;
                    }
                }
            }
        }
    }*/

    /*findNestedV3(obj, key, values) 
    {
        // debugger;
        let selectedObjects = [];
        for (let index in obj) {
            for (let jindex = 0; jindex < values.length; jindex++) {
                let currentObj = obj[index];
                let objSize = Object.keys(obj).length;
                if ((obj[Object.keys(obj)[objSize - 1]] == obj[index]) && // last el of object? 
                    (typeof currentObj != 'object') &&
                    (obj[key] == values[jindex])) {
                    this.selectedObjects.push(obj);
                }
                if (typeof currentObj == 'object') {
                    this.findNestedV3(currentObj, key, values);
                }
            }
        }
    }*/

    /*isObjectTraversable(obj) {
        let deadEnd = true;
        for ( let [objKey, objVal] of Object.entries(obj) ) {
            if ( Array.isArray(obj[objKey]) ) {
                deadEnd = false;
            }
        }
        return deadEnd;
    }*/

    /*toObject(arr) {
        var rv = {};
        for (var i = 0; i < arr.length; ++i)
            if (arr[i] !== undefined) rv[i] = arr[i];
        return rv;
    }*/

    findNestedV4(obj, key, permissions) {
        let counter = 0;
        let selectedObjects = [];
        // let selectedObjects = {};
        let objSize = Object.keys(obj).length;
        for ( let [objKey, objVal] of Object.entries(obj) ) {
            for ( let [permKey, permValue] of Object.entries(permissions) ) {
                if ( objVal[key] == permValue ) {
                    // selectedObjects[counter++] = objVal;
                    selectedObjects.push(objVal);
                }
            }

            if (typeof objVal == 'object') {
                let found = this.findNestedV4(objVal, key, permissions);
                if ( found ) {
                    // debugger;
                    if ( Array.isArray(obj) ) {
                        obj = [];
                        for ( let [key, val] of Object.entries(found) ) {
                            selectedObjects.push(val);
                        }
                    }

                    if ( obj.hasOwnProperty('children') ) {
                        obj.children = [];
                        for ( let [key, val] of Object.entries(found) ) {
                            obj.children.push(val);
                        }
                        selectedObjects.push(obj);
                    }
                }
            }

            // end of list/object/array
            // if ( (obj[objKey] == obj[Object.keys(obj)[objSize - 1]]) ) {}
        }

        // base case (end of list/object/array)
        if ( !_.isEmpty(selectedObjects) ) {
            return selectedObjects;
        } else {
            return;
        }
    }


}
