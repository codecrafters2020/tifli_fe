import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';

import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of'
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { AppService } from './../../../../../app.service';

import * as moment from 'moment';
import * as _ from 'lodash';

@Injectable()
export class ListCustomerCashbackHistoryService implements Resolve<any>
{
    coupons: any[];
    customerCashbackHistory: any[];
    onFilesChanged: BehaviorSubject<any> = new BehaviorSubject({});
    onFileSelected: BehaviorSubject<any> = new BehaviorSubject({});
    id: any;


    constructor(private http: HttpClient, private appService: AppService) {
    }

    /**
     * The File Manager App Main Resolver
     * 
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {

        this.id = route.params['id'];

        // return new Promise((resolve, reject) => {

        //     Promise.all([
        //         this.getCustomerCashbackHistory()
        //     ]).then(
        //         ([coupons]) => {
        //             resolve();
        //         },
        //         reject
        //     );
        // });
    }

    getCustomerCashbackHistory(): Promise<any> {
        return new Promise((resolve, reject) => {
            this.http.get(`${this.appService.reportService}consumer/cashback/history?consumerId=` + this.id)
                .subscribe((response: any) => {

                    debugger;
                    this.customerCashbackHistory = response.data;
                    this.onFilesChanged.next(response.data);
                    this.onFileSelected.next(response[0]);
                    resolve(response);
                }, reject);
        });
    }


    getMerchants(): Promise<any> {
        return new Promise((resolve, reject) => {
            this.http.get(`${this.appService.merchantService}`)
                .subscribe((response: any) => {
                    this.coupons = response.data;
                    this.onFilesChanged.next(response.data);
                    this.onFileSelected.next(response[0]);
                    resolve(response);
                }, reject);
        });
    }

    getFilteredCoupons(page: number | string,
        size: number | string,
        sort: string,
        startDate: string,
        endDate: string, ): Promise<any> {
        let params = {
            page,
            size,
            sort,
            startDate,
            endDate
        };

        let queryString = '';
        _.forEach(params, (val, key) => {
            if (val || typeof val === 'number') {
                queryString = queryString + key + '=' + val + '&';
            }
        });

        return new Promise((resolve, reject) => {
            // this.http.get('api/coupon-list')
            // this.http.get(`http://104.42.179.33:8765/cms/api/coupons?page=0&size=20&sort=couponCode,asc&searchTerms=gag&couponCode=123&couponType=Deal`)
            this.http.get(`${this.appService.couponService}coupons/coupons?${queryString}`)
                .subscribe((response: any) => {
                    this.coupons = response.data;
                    this.onFilesChanged.next(response.data);
                    this.onFileSelected.next(response[0]);
                    resolve(response);
                }, reject);
        });
    }

    searchSeasonalEvent(term: string): Observable<any> {
        return this.http.get(`${this.appService.couponService}seasonal/search/?name=${term}`)
            .map(response => response['data'])
            .map(data => data instanceof Array ? data : [])
    }

    searchMerchant(term: string): Observable<any> {
        return this.http.get(`${this.appService.merchantService}search/?name=${term}`)
            .map(response => response['data'])
            .map(data => data instanceof Array ? data : [])
    }

}
