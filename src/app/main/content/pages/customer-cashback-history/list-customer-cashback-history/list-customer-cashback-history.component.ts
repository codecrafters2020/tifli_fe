import { Component, OnInit, ViewEncapsulation } from '@angular/core';

import { fuseAnimations } from '@fuse/animations';

import { ListCustomerCashbackHistoryService } from './list-customer-cashback-history.service';
import { Router } from '@angular/router';

@Component({
    selector     : 'fuse-file-manager',
    templateUrl  : './list-customer-cashback-history.component.html',
    styleUrls    : ['./list-customer-cashback-history.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class ListCustomerCashbackHistoryComponent implements OnInit
{
    selected: any;
    pathArr: string[];

    constructor(private ListCustomerCashbackHistoryService: ListCustomerCashbackHistoryService, private router: Router)
    {
    }

    ngOnInit()
    {
        this.ListCustomerCashbackHistoryService.onFileSelected.subscribe(selected => {
            this.selected = selected;
           // this.pathArr = selected.location.split('>');
        });
    }

    openCouponAdd()
    {
        this.router.navigateByUrl("pages/coupon/add");
    }

    resetGrid()
    {
        this.ListCustomerCashbackHistoryService.getCustomerCashbackHistory();
    }
}
