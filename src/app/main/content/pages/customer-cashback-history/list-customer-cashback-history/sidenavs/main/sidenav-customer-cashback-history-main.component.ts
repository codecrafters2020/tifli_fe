import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';
import { AbstractControl, FormBuilder, FormGroup, FormArray, Validators, EmailValidator } from '@angular/forms';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatRadioModule } from '@angular/material/radio';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { FormControl } from '@angular/forms';
import { Observable, BehaviorSubject} from 'rxjs';
import { of } from 'rxjs/observable/of';
import { map, startWith } from 'rxjs/operators';
import { FuseAngularMaterialModule } from '../../../../../components/angular-material/angular-material.module';

import { ListCustomerCashbackHistoryService } from '../../list-customer-cashback-history.service';


@Component({
    selector   : 'fuse-file-manager-main-sidenav',
    templateUrl: './sidenav-customer-cashback-history-main.component.html',
    styleUrls  : ['./sidenav-customer-cashback-history-main.component.scss']
})

export class SidenavCustomerCashbackHistoryMainComponent
{
    @Input() leftSideNav: any;

    form: FormGroup;
    formErrors: any;

    startDate:any;
    endDate:any;

    merchantSearchResult: any[];
    seasonalEventSearchResult: any[];

    couponTypes = ['Deal', 'In-store', 'Code'];
    couponStatuses = ['Active', 'Inactive'];

    onGoingOffers  : boolean;
    exclusiveOffers  : boolean;
    expiringSoon  : boolean;
    popularOffers  : boolean;

    enableOngoingOffers: boolean = false;
    enableExclusiveOffers: boolean = false;
    enableExpiringSoon: boolean = false;
    enablePopularOffers: boolean = false;

    btnDisabled: boolean = false;

    constructor(private ListCustomerCashbackHistoryService: ListCustomerCashbackHistoryService, 
                private router: Router,
                private formBuilder: FormBuilder)
    {
      // Reactive Form
      this.form = this.formBuilder.group({
          startDate : ['', ],
          endDate : ['', ]
      });

      // Reactive form errors
      this.formErrors = {
      };
    }

    filter() 
    {
      // this.btnDisabled = true;
      this.ListCustomerCashbackHistoryService.getFilteredCoupons('', 
                                                '', 
                                                '',
                                                this.form.controls.startDate.value,
                                                this.form.controls.endDate.value,)
        .then((response) => {
          this.btnDisabled = false;
          this.leftSideNav.toggle()
        })
    }

    ngOnInit() 
    {
      this.form.valueChanges.subscribe(() => {
        // console.log(this.form.value);
      });
    
      this.form.controls.merchantName.valueChanges
        .debounceTime(400)
        .subscribe(searchTerm => {
    			this.ListCustomerCashbackHistoryService.searchMerchant(searchTerm)
    				.subscribe(merchantNames => {
    					this.merchantSearchResult = merchantNames;
    				});
        });

      this.form.controls.seasonalEvent.valueChanges
        .debounceTime(400)
        .subscribe(searchTerm => {
    			this.ListCustomerCashbackHistoryService.searchSeasonalEvent(searchTerm)
    				.subscribe(response => {
    					this.seasonalEventSearchResult = response;
    				});
        });

    }

    displaySelectedName(item: any) 
    {
        return item;
    }
}  

    
