import { Component, OnInit } from '@angular/core';

import { fuseAnimations } from '@fuse/animations/index';

import { ListCustomerCashbackHistoryService } from '../../list-customer-cashback-history.service';

@Component({
    selector   : 'fuse-file-manager-details-sidenav',
    templateUrl: './sidenav-customer-cashback-history-details.component.html',
    styleUrls  : ['./sidenav-customer-cashback-history-details.component.scss'],
    animations : fuseAnimations
})
export class SidenavCustomerCashbackHistoryDetailsComponent implements OnInit
{

    selected: any;

    constructor(private ListCustomerCashbackHistoryService: ListCustomerCashbackHistoryService)
    {

    }

    ngOnInit()
    {
        this.ListCustomerCashbackHistoryService.onFileSelected.subscribe(selected => {
            this.selected = selected;
        });
    }

}
