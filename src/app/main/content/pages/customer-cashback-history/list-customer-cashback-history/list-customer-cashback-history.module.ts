import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CdkTableModule } from '@angular/cdk/table';

import { MatAutocompleteModule, MatFormFieldModule, MatInputModule, MatSelectModule, MatStepperModule, MatButtonModule, MatIconModule, MatRippleModule, MatSidenavModule, MatSlideToggleModule, MatTableModule, MatMenuModule, MatPaginatorModule, MatSortModule } from '@angular/material';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';

import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatRadioModule} from '@angular/material/radio';
import { FuseSharedModule } from '@fuse/shared.module';

import { ListCustomerCashbackHistoryComponent } from './list-customer-cashback-history.component';
import { ListCustomerCashbackHistoryService } from './list-customer-cashback-history.service';
import { GridCustomerCashbackHistoryComponent } from './grid/grid-customer-cashback-history.component';
import { SidenavCustomerCashbackHistoryMainComponent } from './sidenavs/main/sidenav-customer-cashback-history-main.component';
import { SidenavCustomerCashbackHistoryDetailsComponent } from './sidenavs/details/sidenav-customer-cashback-history-details.component';

const routes: Routes = [
    {
        path     : '**',
        component: ListCustomerCashbackHistoryComponent,
        children : [],
        resolve  : {
            files: ListCustomerCashbackHistoryService
        }
    }
];

@NgModule({
    declarations: [
        ListCustomerCashbackHistoryComponent,
        GridCustomerCashbackHistoryComponent,
        SidenavCustomerCashbackHistoryMainComponent,
        SidenavCustomerCashbackHistoryDetailsComponent
    ],
    imports     : [
        RouterModule.forChild(routes),

        CdkTableModule,
        MatAutocompleteModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        MatStepperModule,
        MatButtonModule,
        MatIconModule,
        MatRippleModule,
        MatSlideToggleModule,
        MatSidenavModule,
        MatTableModule,
        MatMenuModule,
        MatPaginatorModule, 
        MatSortModule,
        MatProgressSpinnerModule,

        FuseSharedModule,
        MatDatepickerModule,
        MatCheckboxModule,
        MatRadioModule
    ],
    providers   : [
        ListCustomerCashbackHistoryService
    ]
})
export class ListCustomerCashbackHistoryModule
{
}
