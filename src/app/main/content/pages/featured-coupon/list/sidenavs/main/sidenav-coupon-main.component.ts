import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';
import { AbstractControl, FormBuilder, FormGroup, FormArray, Validators, EmailValidator } from '@angular/forms';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatRadioModule } from '@angular/material/radio';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { FormControl } from '@angular/forms';
import { Observable, BehaviorSubject} from 'rxjs';
import { of } from 'rxjs/observable/of';
import { map, startWith } from 'rxjs/operators';
import { FuseAngularMaterialModule } from '../../../../../components/angular-material/angular-material.module';

import { ListCouponService } from '../../list-coupon.service';


@Component({
    selector   : 'fuse-file-manager-main-sidenav',
    templateUrl: './sidenav-coupon-main.component.html',
    styleUrls  : ['./sidenav-coupon-main.component.scss']
})

export class SidenavCouponMainComponent
{
    @Input() leftSideNav: any;

    form: FormGroup;
    formErrors: any;

    merchantSearchResult: any[];
    seasonalEventSearchResult: any[];

    couponTypes : any[]=[];
    //couponTypes = ['Deal', 'In-store', 'Code'];
    couponStatuses = ['Active', 'Inactive'];

    onGoingOffers  : boolean;
    exclusiveOffers  : boolean;
    expiringSoon  : boolean;
    popularOffers  : boolean;
    freeShipping : boolean;
    // freeship : boolean

    enableOngoingOffers: boolean = false;
    enableExclusiveOffers: boolean = false;
    enableExpiringSoon: boolean = false;
    enablePopularOffers: boolean = false;
    enableFreeShippingOffers: boolean = false;
    // enableShipping: boolean = false;

    btnDisabled: boolean = false;

    constructor(private ListCouponService: ListCouponService, 
                private router: Router,
                private formBuilder: FormBuilder)
    {
      // Reactive Form
      this.form = this.formBuilder.group({
          merchantName : ['', ],
          merchantTag : ['', ],
          seasonalEvent : ['', ],
          couponTypes : ['', ],
          couponStatuses  : ['', ],
          onGoingOffers  : ['', ],
          exclusiveOffers  : ['', ],
          expiringSoon  : ['', ],
          popularOffers  : ['', ],
          freeShipping : ['', ]
      });

      // Reactive form errors
      this.formErrors = {
        merchantName : {},
        merchantTag : {},
        seasonalEvent : {},
        couponTypes : {},
        couponStatuses  : {},
        onGoingOffers  : {},
        exclusiveOffers  : {},
        expiringSoon  : {},
        popularOffers  : {},
        freeShipping : {},
      };

     // this.couponTypes=this.ListCouponService.couponTypes;
    }

    // filter() 
    // {
    //   // this.btnDisabled = true;
    //   this.ListCouponService.getFilteredCoupons('', 
    //                                             '', 
    //                                             'couponCode,asc',
    //                                             this.form.controls.merchantTag.value,
    //                                             this.form.controls.merchantName.value,
    //                                             this.form.controls.seasonalEvent.value,
    //                                             this.form.controls.couponTypes.value,
    //                                             this.form.controls.couponStatuses.value,
    //                                             this.enableOngoingOffers ? (this.onGoingOffers === true ? 1 : 0) : '',
    //                                             this.enableExclusiveOffers ? (this.exclusiveOffers === true ? 1 : 0) : '',
    //                                             this.enableExpiringSoon ? (this.expiringSoon === true ? 1 : 0) : '',
    //                                             this.enableFreeShippingOffers ? (this.freeShipping === true ? 1: 0) : ''
    //                                           )
    //     .then((response) => {
    //       this.btnDisabled = false;
    //       this.leftSideNav.toggle()
    //     })
    // }

    ngOnInit() 
    {
      this.form.valueChanges.subscribe(() => {
        // console.log(this.form.value);
      });
    
      // this.form.controls.merchantName.valueChanges
      //   .debounceTime(400)
      //   .subscribe(searchTerm => {
    	// 		this.ListCouponService.searchMerchant(searchTerm)
    	// 			.subscribe(merchantNames => {
    	// 				this.merchantSearchResult = merchantNames;
    	// 			});
      //   });

      // this.form.controls.seasonalEvent.valueChanges
      //   .debounceTime(400)
      //   .subscribe(searchTerm => {
    	// 		this.ListCouponService.searchSeasonalEvent(searchTerm)
    	// 			.subscribe(response => {
    	// 				this.seasonalEventSearchResult = response;
    	// 			});
      //   });

    }

    displaySelectedName(item: any) 
    {
        return item;
    }
}  

    
