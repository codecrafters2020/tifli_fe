import { Component, OnInit } from '@angular/core';

import { fuseAnimations } from '@fuse/animations/index';

import { ListCouponService } from '../../list-coupon.service';

@Component({
    selector   : 'fuse-file-manager-details-sidenav',
    templateUrl: './sidenav-coupon-details.component.html',
    styleUrls  : ['./sidenav-coupon-details.component.scss'],
    animations : fuseAnimations
})
export class SidenavCouponDetailsComponent implements OnInit
{

    selected: any;

    constructor(private ListCouponService: ListCouponService)
    {

    }

    ngOnInit()
    {
        // this.ListCouponService.onFileSelected.subscribe(selected => {
        //     this.selected = selected;
        // });
    }

}
