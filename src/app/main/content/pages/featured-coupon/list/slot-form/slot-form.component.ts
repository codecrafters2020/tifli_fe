import { Component, Inject, ViewEncapsulation, AfterViewChecked } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef, MatDialog } from '@angular/material';

import { MatColors } from '@fuse/mat-colors';

import { CalendarEvent } from 'angular-calendar';
import { AppService } from './../../../../../../app.service';
import { ListCouponService } from '../list-coupon.service';
import { invalid } from 'moment';
import { DISABLED } from '@angular/forms/src/model';
// import { SlotModel } from '../slot.model';
// import { ScheduledPlacementService} from '../scheduled-placement.service';

@Component({
    selector: 'fuse-calendar-event-form-dialog',
    templateUrl: './slot-form.component.html',
    styleUrls: ['./slot-form.component.scss'],
    encapsulation: ViewEncapsulation.None
})

export class FuseCalendarEventFormDialogComponent {

    btndisable: boolean = false;
    /** Meta Data for Carousel Popup */
    placementTypes: any[] = [
        "Coupon",
        "Merchant",
      
    ];

    positionValues: any[] = [
        1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15
    ];



    /** Meta data ended */

    /** Slot Object */
    slot: any = {
        id: 0,
        position: 1,
        startDate: new Date(),
        endDate: new Date(),

    };


    slotForm: FormGroup;
    action: string = "add";

    merchantSearchResult: any[] = [
        { name: 'Usman', id: 1 },
        { name: 'Farrukh', id: 2 },
        { name: 'Umair', id: 3 }
    ];
    couponSearchResult: any[] = [
        { title: 'Save upto 30% on all samsung coupons', id: 1 },
        { title: 'Save upto 40% on all samsung coupons', id: 2 }
    ];
 
    selectedMerchant: any = {};
    selectedCoupon: any = { title: 'Save upto 40% on all samsung coupons', id: 2 };
  
    selectedId: any;

    couponAttached: boolean = false;

 
    buttonAction: any = "add";

    constructor(
        public dialogRef: MatDialogRef<FuseCalendarEventFormDialogComponent>,
        @Inject(MAT_DIALOG_DATA) private data: any,
        private CouponPlacementService: ListCouponService,
        // private scheduledPlacementService: ScheduledPlacementService
    ) {

        this.action = data.action;
  
        this.slot.startDate = new Date();
        this.slot.endDate = new Date();
        this.slot.position = data.position;
        this.slotForm = this.createSlotForm();
        this.getCoupon();
      }
      createSlotForm() {
        return new FormGroup({
            position: new FormControl(),
            startDate: new FormControl(),
            endDate: new FormControl(),
            merchantId: new FormControl(),
            couponId: new FormControl(),
            seasonalId: new FormControl(),
            categoryId: new FormControl(),
            targetUrl: new FormControl(),
        });
    }

  

    getCoupon() {
        debugger;
      
        this.slotForm.controls.couponId.valueChanges
            .debounceTime(400)
            .subscribe(couponId => {
                this.CouponPlacementService.getCoupon(couponId).then(response => {
                    debugger;
                    if (!(response.meta.message == "success" && response.meta.status == "success")) {
                        this.couponAttached = false;
                        // var buttonn = <HTMLInputElement> document.getElementById("btn1");
                        // buttonn.disabled = true;
                        this.btndisable = true;
                        
                    }
                    else {
                        this.btndisable = false;
                        this.couponAttached = true;
                        this.selectedCoupon = response.data;
                        var merchantId = this.selectedCoupon.merchant.id;
                        this.CouponPlacementService.getMerchant(merchantId).then(response => {
                            this.selectedCoupon.merchant = response.data;
                        });
                        
                    }
                });
            });
    }

  

    closeDialog(action) {
        debugger;
        this.slot.itemId = this.selectedId;

        let data = {
            slot: this.slot,
            coupon: this.selectedCoupon,
            startDate: this.slot.startDate,
            endDate: this.slot.endDate,
            action: action
        }
       
        console.log(this.slot)
        
        this.dialogRef.close(data);
    }


}
