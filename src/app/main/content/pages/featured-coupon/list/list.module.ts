import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CdkTableModule } from '@angular/cdk/table';

import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatAutocompleteModule, MatFormFieldModule, MatInputModule, MatSelectModule, MatStepperModule, MatButtonModule, MatIconModule, MatRippleModule, MatSidenavModule, MatSlideToggleModule, MatTableModule, MatMenuModule, MatPaginatorModule, MatSortModule, MatToolbarModule, MatNativeDateModule } from '@angular/material';

import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatRadioModule} from '@angular/material/radio';
import { FuseSharedModule } from '@fuse/shared.module';
import { FuseCalendarEventFormDialogComponent } from './slot-form/slot-form.component';

import { ListComponent } from './list.component';
import { ListCouponService } from './list-coupon.service';
import { GridCouponComponent } from './grid/grid-featured-coupon.component';
import { SidenavCouponMainComponent } from './sidenavs/main/sidenav-coupon-main.component';
import { SidenavCouponDetailsComponent } from './sidenavs/details/sidenav-coupon-details.component';
import { FancyImageUploaderModule } from 'ng2-fancy-image-uploader';

const routes: Routes = [
    {
        path     : '**',
        component: ListComponent,
        children : [],
        resolve  : {
            files: ListCouponService
        }
    }
];

@NgModule({
    declarations: [
        ListComponent,
        GridCouponComponent,
        SidenavCouponMainComponent,
        SidenavCouponDetailsComponent,
        FuseCalendarEventFormDialogComponent
        
    ],
    imports     : [
        RouterModule.forChild(routes),

        CdkTableModule,
        MatAutocompleteModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        MatStepperModule,
        MatButtonModule,
        MatIconModule,
        MatRippleModule,
        MatSlideToggleModule,
        MatSidenavModule,
        MatTableModule,
        MatMenuModule,
        MatPaginatorModule, 
        MatSortModule,
        MatProgressSpinnerModule,
        MatToolbarModule,
        FuseSharedModule,
        MatDatepickerModule,
        MatCheckboxModule,
        FancyImageUploaderModule,
        MatRadioModule
    ],
    providers   : [
        ListCouponService,

    ],
    entryComponents: [FuseCalendarEventFormDialogComponent]

})
export class ListModule
{
}
