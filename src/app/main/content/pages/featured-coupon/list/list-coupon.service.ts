import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';

import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { from } from 'rxjs/observable/from';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/map';

import { AppService } from '../../../../../app.service';

@Injectable()
export class ListCouponService implements Resolve<any>
{
    featuredcoupons: any;
  //  nonMonetizedMerchants: any;
    currentDisplayedNonMonetizedMerchants: any[];
    onFilesChanged: BehaviorSubject<any> = new BehaviorSubject({});
    onFileSelected: BehaviorSubject<any> = new BehaviorSubject({});
 
    constructor(private http: HttpClient, private appService:AppService)
    {
    }

    /**
     * The File Manager App Main Resolver
     * 
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any
    {

        // return new Promise((resolve, reject) => {

        //     Promise.all([
        //         this.getMerchants(),
               
        //     ]).then(
        //         ([monetizedMerchants]) => {
        //             resolve();
        //         }, reject);
        // });
    }

    getCoupon(couponId): Promise<any>
    {
        debugger;
        if(couponId == null)
        {couponId = 0;}
        return new Promise((resolve, reject) => {
            this.http.get(this.appService.couponService + 'coupons/' + couponId)
                .subscribe((response: any) => {
                    resolve(response);
                }, reject);
        });
    }


    // getMonetized
    getMerchants(): Promise<any>
    {
        return new Promise((resolve, reject) => {
            debugger;
            // this.http.get('http://192.168.1.76:8765/cms/api/merch/monetized/')
            this.http.get(this.appService.featuredService )
                .subscribe((response: any) => {
                    this.featuredcoupons = response.data;
                    this.onFilesChanged.next(response.data);
                    this.onFileSelected.next(response[0]);
                    resolve(response);
                }, reject);
                
        });
    }

   
    getMerchant(merchantId): Promise<any>
    {
        return new Promise((resolve, reject) => {
            this.http.get(this.appService.merchantService + merchantId)
                .subscribe((response: any) => {
                    resolve(response);
                }, reject);
        });
    }
 
    deletefeaturedcoupon(featuredcoupon): Promise<any>
    {
        let par :any =featuredcoupon.selection.selected[0].featuredCouponId;
        for(let i = 1 ; i <featuredcoupon.selection.selected.length;i++ )
        {
            console.log(featuredcoupon.selection.selected[i].featuredCouponId   );
            par= par + ","+featuredcoupon.selection.selected[i].featuredCouponId;
        debugger;
        }
     //   console.log(par);
  //    let updatedList = [ids]
    
        return new Promise((resolve, reject) => {
            this.http.delete(this.appService.couponService +'featured/delete/?featuredIds='+par)
                .subscribe((response: any) => {
                resolve(response);
                this.getMerchants();
                }, reject);
             //   console.log(this.appService.couponService +'featured/delete/?featuredIds='+par)
                debugger;
              

        }
    );
    }


    addFeaturedCoupon(coupon, startDate, endDate): Promise<any>{
        let payload = {
            coupon: coupon,
            startDate: startDate,
            endDate: endDate
        };
        return new Promise((resolve,reject)=> {
            this.http.post(this.appService.couponService + 'featured/', payload).subscribe(response => {
                        /* some operations or conditiond on response */
                        // this.onMerchantsChanged.next(response['data']);
                        resolve(response);
                        this.getMerchants();

            })
        })

    }
}
