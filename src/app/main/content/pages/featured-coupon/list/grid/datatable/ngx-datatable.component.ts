import { Component, Inject, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

import { ListCouponService } from '../../list-coupon.service';

@Component({
    selector   : 'fuse-ngx-datatable',
    templateUrl: './ngx-datatable.component.html',
    styleUrls  : ['./ngx-datatable.component.scss']
})
export class FuseNgxDatatableComponent implements OnInit
{
    rows: any[];
    selected = [];
    loadingIndicator = true;
    reorderable = true;

    constructor(public dialogRef: MatDialogRef<FuseNgxDatatableComponent>,
                @Inject(MAT_DIALOG_DATA) public data: any,
                private MonetizedMerchantService: ListCouponService, 
                private router: Router)
    {
    }

    ngOnInit()
    {
        this.rows = this.MonetizedMerchantService.featuredcoupons;
        this.loadingIndicator = false;
    }

    onCheckboxSelect({ selected })
    {
        this.selected = selected;
    }

    redirect(pagename: string) {
        this.router.navigate([pagename]);
    }


    onCloseClick(): void {
        this.dialogRef.close();
    }

}
