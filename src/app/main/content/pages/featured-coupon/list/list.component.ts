import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, FormArray, Validators, EmailValidator } from '@angular/forms';
import { fuseAnimations } from '@fuse/animations';

import { ListCouponService } from './list-coupon.service';
import { Router } from '@angular/router';
import { FuseCalendarEventFormDialogComponent } from './slot-form/slot-form.component';
import { MatDialog } from '@angular/material';

import {HttpClientModule} from '@angular/common/http';
import * as _ from 'lodash';

@Component({
    selector: 'fuse-file-manager',
    templateUrl: './list.component.html',
    styleUrls: ['./list.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class ListComponent implements OnInit {
    selected: any;
    pathArr: string[];
    isSearch: boolean = false;
    form: FormGroup;
    merchantSearchResult: any[];

    dialogRef:any; // change
    toDate: any = new Date();//change
    fromDate: any = new Date();//change

    constructor(private ListCouponService: ListCouponService, private router: Router, private formBuilder: FormBuilder,public dialog: MatDialog) {
        // this.ListCouponService.getCoupons(); // change last two param of constructor
        // this.form = this.formBuilder.group({
        //     merchantName : ['', ]})

    }

    ngOnInit() {
        

        // this.ListCouponService.onFileSelected.subscribe(selected => {
        //     this.selected = selected;
        //     // this.pathArr = selected.location.split('>');
        // });
    }

  
    add_coupon(event,data)
    {
        debugger;
        // alert("function called" + "---" + data);
        this.dialogRef = this.dialog.open(FuseCalendarEventFormDialogComponent, {
            panelClass: 'event-form-dialog',
            data      : {
                event : data,
            }
        });

        this.dialogRef.afterClosed()
            .subscribe(response => {
                debugger;
               this.ListCouponService.addFeaturedCoupon(response.coupon, response.startDate, response.endDate).then(response => {
               });
            });
    }
    update(Grid) {


        if(confirm('Are you sure you want to delete selected featured coupons?')) { 
     
        this.ListCouponService.deletefeaturedcoupon(Grid);
        // this.router.navigateByUrl("pages/merchant/monetized");


    }}
   
  
    

}
