import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { FuseAngularMaterialModule } from '../../components/angular-material/angular-material.module';

import { FuseSharedModule } from '@fuse/shared.module';

const routes = [
    {
        path        : 'list',
        loadChildren: './list-merchant-category/list-merchant-category.module#ListMerchantCategoryModule'
    },
    {
        path        : 'add',
        loadChildren: './add-merchant-category/add-merchant-category.module#AddMerchantCategoryModule'
    },
    {
        path        : 'edit/:id',
        loadChildren: './edit-merchant-category/edit-merchant-category.module#EditMerchantCategoryModule'
    }
];

@NgModule({
    imports     : [
        FuseSharedModule,
        RouterModule.forChild(routes),
        FuseAngularMaterialModule
    ],
    declarations: []
})
export class MerchantCategoryModule
{
}
