import { Component, OnInit } from '@angular/core';

import { fuseAnimations } from '@fuse/animations/index';

import { ListMerchantCategoryService } from '../../list-merchant-category.service';

@Component({
    selector   : 'fuse-file-manager-details-sidenav',
    templateUrl: './sidenav-merchant-category-details.component.html',
    styleUrls  : ['./sidenav-merchant-category-details.component.scss'],
    animations : fuseAnimations
})
export class SidenavMerchantCategoryDetailsComponent implements OnInit
{

    selected: any;

    constructor(private ListMerchantCategoryService: ListMerchantCategoryService)
    {

    }

    ngOnInit()
    {
        this.ListMerchantCategoryService.onFileSelected.subscribe(selected => {
            this.selected = selected;
        });
    }

}
