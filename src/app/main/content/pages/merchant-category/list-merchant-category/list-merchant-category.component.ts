import { Component, OnInit, ViewEncapsulation } from '@angular/core';

import { fuseAnimations } from '@fuse/animations';

import { ListMerchantCategoryService } from './list-merchant-category.service';
import { Router } from '@angular/router';

@Component({
    selector     : 'fuse-file-manager',
    templateUrl  : './list-merchant-category.component.html',
    styleUrls    : ['./list-merchant-category.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class ListMerchantCategoryComponent implements OnInit
{
    selected: any;
    pathArr: string[];

    constructor(private ListMerchantCategoryService: ListMerchantCategoryService, private router: Router)
    {
    }

    ngOnInit()
    {
        // this.ListMerchantCategoryService.onFileSelected.subscribe(selected => {
        //     this.selected = selected;
        //    // this.pathArr = selected.location.split('>');
        // });
    }

    openMerchantCategoryAdd(){
        this.router.navigateByUrl("pages/merchant-category/add");
    }
}
