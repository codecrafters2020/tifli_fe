import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CdkTableModule } from '@angular/cdk/table';

import { MatFormFieldModule, MatInputModule, MatSelectModule, MatStepperModule, MatButtonModule, MatIconModule, MatRippleModule, MatSidenavModule, MatSlideToggleModule, MatTableModule } from '@angular/material';
import {MatChipsModule} from '@angular/material/chips';
import {MatAutocompleteModule} from '@angular/material/autocomplete';

import { FuseSharedModule } from '@fuse/shared.module';

import { AddMerchantCategoryComponent } from './add-merchant-category.component';
import { AddMerchantCategoryService } from './add-merchant-category.service';
import { FormAddMerchantCategoryComponent } from './form/form-add-merchant-category.component';

const routes: Routes = [
    {
        path     : '**',
        component: AddMerchantCategoryComponent,
        children : [],
        resolve  : {
            files: AddMerchantCategoryService
        }
    }
];

@NgModule({
    declarations: [
        AddMerchantCategoryComponent,
        FormAddMerchantCategoryComponent
    ],
    imports     : [
        RouterModule.forChild(routes),

        CdkTableModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        MatStepperModule,
        MatButtonModule,
        MatIconModule,
        MatRippleModule,
        MatSlideToggleModule,
        MatSidenavModule,
        MatTableModule,
        MatChipsModule,
        MatAutocompleteModule,

        FuseSharedModule
    ],
    providers   : [
        AddMerchantCategoryService
    ]
})
export class AddMerchantCategoryModule
{
}
