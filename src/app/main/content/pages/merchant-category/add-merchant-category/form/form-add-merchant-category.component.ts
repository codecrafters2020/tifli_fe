import { Component, OnInit, ViewChild } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, FormArray, Validators, EmailValidator } from '@angular/forms';
import { AddMerchantCategoryService } from '../add-merchant-category.service';
import { Router } from '@angular/router';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { startWith } from 'rxjs/operators/startWith';
import { map } from 'rxjs/operators/map';
import { MatSnackBar } from '@angular/material';

import {
    MatAutocomplete,
    MatAutocompleteSelectedEvent,
    MatAutocompleteTrigger,
    VERSION,
} from '@angular/material';
import { Subscription } from 'rxjs/Subscription';

@Component({
    selector: 'fuse-file-list',
    templateUrl: './form-add-merchant-category.component.html',
    styleUrls: ['./form-add-merchant-category.component.scss'],
})
export class FormAddMerchantCategoryComponent implements OnInit {
    merchantCategoryStatus: boolean = false;
    form: FormGroup;
    formErrors: any;
    test: any;

    visible: boolean = true;
    selectable: boolean = true;
    removable: boolean = true;
    addOnBlur: boolean = true;

    constructor(private formBuilder: FormBuilder, private AddMerchantCategoryService: AddMerchantCategoryService, private router: Router, private snackBar: MatSnackBar) {
        // Reactive form errors
        this.formErrors = {
            categoryName: {},
            description: {}
        };
    }

    ngOnInit() {
        // Reactive Form
        this.form = this.formBuilder.group({
            categoryName: ['', Validators.required],
            description: ['', Validators.required],
            status: [''],
            subcategories: new FormArray([])
        });

        const control = <FormArray>this.form.controls['subcategories']
        for (let i = 0; i < control.length; i++) {
            control[i]
                .subscribe(value => {
                    debugger;
                    let passwordConfirmValue = this.form.controls.passwordConfirm.value;
                    const control = this.form.controls.passwordConfirm;
                    if (value != passwordConfirmValue)
                        this.formErrors.passwordConfirm.passwordsNotMatch = true;
                    else {
                        this.form.controls['passwordConfirm'].setValue(value);
                    }
                });
        }

        this.form.valueChanges.subscribe(() => {

            this.onFormValuesChanged();
        });



    }

    initSubCategory() {
        return this.formBuilder.group({
            name: ['', Validators.required],
            description: ['', Validators.required],
            status: [this.merchantCategoryStatus ? 'ACTIVE' : 'INACTIVE', Validators.required]
        });
    }

    addNewSubCategory() {
        const control = <FormArray>this.form.controls['subcategories'];
        console.log("qari")
        control.push(this.initSubCategory());

    }

    deleteSubCategory(index: number) {
        const control = <FormArray>this.form.controls['subcategories'];
        control.removeAt(index);
    }



    SearchData(input: any) {
        // debugger;
    }

    onFormValuesChanged() {
        for (const field in this.formErrors) {
            if (!this.formErrors.hasOwnProperty(field)) {
                continue;
            }

            // Clear previous errors
            this.formErrors[field] = {};

            // Get the control
            const control = this.form.get(field);

            if (control && control.dirty && !control.valid) {
                this.formErrors[field] = control.errors;
            }
        }
    }

    onSubmit() {
        // debugger;
        let formValues = this.form.value;
        if (this.form.valid) {
            let payload = {
                'name': formValues.categoryName,
                'description': formValues.description,
                'status': formValues.status ? 'ACTIVE' : 'INACTIVE',
                'subCategories': formValues.subcategories
            }
            this.AddMerchantCategoryService.addMerchantCategory(payload).then(response => {
                // debugger;
                this.snackBar.open(response.meta.message, "Done", {
                    duration: 2000,
                });
                if (response.meta.status == "success")
                    this.redirect('pages/merchant-category/list');
            });
        }

    }

    redirect(pagename: string) {
        this.router.navigate([pagename]);
    }

    log(val: any) { console.log(val); }
}
