import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';

import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { AppService } from './../../../../../app.service';

@Injectable()
export class AddMerchantCategoryService implements Resolve<any>
{
    category:any;
    id:any;
    onUsersChanged: BehaviorSubject<any> = new BehaviorSubject({});
    onUserSelected: BehaviorSubject<any> = new BehaviorSubject({});

    constructor(private http: HttpClient, private appService: AppService)
    {
    }

    /**
     * The File Manager App Main Resolver
     * 
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any
    {
        this.id=route.params['id'];

        // return new Promise((resolve, reject) => {
        //     debugger;
        //     Promise.all([
        //      this.getCategory()
        //     ]).then(
        //         ([users]) => {
        //             resolve();
        //         },
        //         reject);
        // });
    }

    getCategory(): Promise<any>
    {

        return new Promise((resolve, reject) => {
            debugger;
            // this.http.get(this.appService.apiUrl + 'cms/api/merchants/categories/'+this.id)
            this.http.get(this.appService.merchantService + 'categories/' + this.id)
                .subscribe((response: any) => {
                    console.log(response);
                    debugger;
                    if(response.meta.code=="200")
                    {
                        // for(let i=0;i<response.data.length;i++){
                        //     if(response.data[i].id == this.id){
                        //         this.category=response.data[i];
                        //         break;
                        //     }

                        // }
                        this.category = response.data;  
                    }
                    resolve(response);
                }, reject);
        });
    }

    updateMerchantCategory(merchantCategory): Promise<any>
    {
        // debugger;
        return new Promise((resolve, reject) => {
            debugger;
            this.http.put(this.appService.merchantService + 'categories/'+merchantCategory.id, merchantCategory)
                .subscribe((response: any) => {
                    resolve(response);
                }, reject);
        });
    }

}