import { Component, OnInit, ViewEncapsulation } from '@angular/core';

import { fuseAnimations } from '@fuse/animations';

import { AddMerchantCategoryService } from './add-merchant-category.service';

import { Router } from '@angular/router';

@Component({
    selector     : 'fuse-file-manager',
    templateUrl  : './add-merchant-category.component.html',
    styleUrls    : ['./add-merchant-category.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class AddMerchantCategoryComponent implements OnInit
{
    selected: any;
    pathArr: string[];

    constructor(private AddMerchantCategoryService: AddMerchantCategoryService, private router: Router)
    {
    }

    ngOnInit()
    {
        
    }

    openMerchantCategoryList(){
        
        this.router.navigateByUrl("pages/merchant-category/list");
    }
}
