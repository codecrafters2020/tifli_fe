import { Component, OnInit,ViewChild } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, FormArray, Validators, EmailValidator } from '@angular/forms';
import { AddMerchantCategoryService } from '../add-merchant-category.service';
import { Router } from '@angular/router';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { startWith } from 'rxjs/operators/startWith';
import { map } from 'rxjs/operators/map';
import { MatSnackBar } from '@angular/material';

import {
    MatAutocomplete,
    MatAutocompleteSelectedEvent,
    MatAutocompleteTrigger,
    VERSION,
  } from '@angular/material';
import { Subscription } from 'rxjs/Subscription';

@Component({
    selector   : 'fuse-file-list',
    templateUrl: './form-add-merchant-category.component.html',
    styleUrls  : ['./form-add-merchant-category.component.scss'],
})
export class FormAddMerchantCategoryComponent implements OnInit
{
    category: any = {};
    merchantCategoryStatus: boolean = false;
    form: FormGroup;
    formErrors: any;
    test: any;

    visible: boolean = true;
    selectable: boolean = true;
    removable: boolean = true;
    addOnBlur: boolean = true;
    removedSubcategories: any[] = [];
    
    constructor(private formBuilder: FormBuilder, private AddMerchantCategoryService: AddMerchantCategoryService, private router: Router, private snackBar: MatSnackBar)
    {
        debugger
        this.category = AddMerchantCategoryService.category;
        
        if(this.category.status == 'ACTIVE')
            this.category.status = true;
        else
            this.category.status = false;
        // Reactive form errors
        this.formErrors = {
            categoryName : {},
            description  : {}
        };
    }

    ngOnInit()
    {
        // Reactive Form
        this.form = this.formBuilder.group({
            categoryName : ['', Validators.required],
            description  : ['', Validators.required],
            status : [''],
            subcategories : new FormArray([])
        });

        this.form.valueChanges.subscribe(() => {
            this.onFormValuesChanged();
        });
        debugger;
        if(this.category.subCategories != null){
            for(let i=0;i<this.category.subCategories.length;i++){
                const control = <FormArray>this.form.controls['subcategories'];
                control.push(this.loadSubCategory(this.category.subCategories[i]));
            }
        }

    }

    loadSubCategory(category: any) {
        return this.formBuilder.group({
            name : [category.name, Validators.required],
            description  : [category.description, Validators.required],
            status : [this.category.status ? 'ACTIVE' : 'INACTIVE', Validators.required]
        });
    }

    initSubCategory() {
        return this.formBuilder.group({
            name : ['', Validators.required],
            description  : ['', Validators.required],
            status : [this.category.status ? 'ACTIVE' : 'INACTIVE', Validators.required]
        });
    }

    addNewSubCategory() {
        debugger;
        let newSubCategory = {id:'',name:'', description:''};
        this.category.subCategories.push(newSubCategory);
        const control = <FormArray>this.form.controls['subcategories'];
        control.push(this.initSubCategory());
    }

    deleteSubCategory(index: number) {
        debugger;
        const control = <FormArray>this.form.controls['subcategories'];
        control.removeAt(index);
        this.removedSubcategories.push(this.category.subCategories[index]);
        this.category.subCategories.splice(index,1);
    }
    


    SearchData(input: any){
        // debugger;
    }

    onFormValuesChanged()
    {
        for ( const field in this.formErrors )
        {
            if ( !this.formErrors.hasOwnProperty(field) )
            {
                continue;
            }

            // Clear previous errors
            this.formErrors[field] = {};

            // Get the control
            const control = this.form.get(field);

            if ( control && control.dirty && !control.valid )
            {
                this.formErrors[field] = control.errors;
            }
        }
    }

    onSubmit() {
        debugger;
        let formValues = this.form.value;
        if (this.form.valid) {
            // let payload = {
            //     'name': this.category.name,
            //     'description': this.category.description,
            //     'status': this.category.status ? 'ACTIVE' : 'INACTIVE',
            //     'subCategories': this.category.subCategories
            // }
            if(this.category.status == true){
                this.category.status = 'ACTIVE';
            }
            else
                this.category.status = 'INACTIVE';
            
            let payload = this.category;
            this.AddMerchantCategoryService.updateMerchantCategory(payload).then(response => {
                debugger;
                if(response.meta.message === "Category already associated with Merchants and/or Coupons"){
                    for(let i=0;i<this.removedSubcategories.length;i++){
                        const control = <FormArray>this.form.controls['subcategories'];
                        control.controls.push(this.loadSubCategory(this.removedSubcategories[i]));
                        this.category.subCategories.push(this.removedSubcategories[i]);
                    }
                    this.removedSubcategories = [];
                    this.snackBar.open(response.meta.message,"Done", {
                        duration: 5000,
                    });
                }
                else{
                    this.snackBar.open(response.meta.message,"Done", {
                        duration: 2000,
                    });
                }
                // if(response.meta.status == "success")
                //     this.redirect('pages/merchant-category/list');
            }); 
        }
        
    }

    redirect(pagename: string) {
        this.router.navigate([pagename]);
    }

    log(val: any) { console.log(val); }
}
