import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort } from '@angular/material';
import { MatDialog, MatDialogRef } from '@angular/material';
import { DataSource } from '@angular/cdk/collections';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';

import { FuseCalendarEventFormDialogComponent } from './../slot-form/slot-form.component';
import { ListCarouselService } from '../list-carousel.service';
import { fuseAnimations } from '@fuse/animations/index';
import { FuseUtils } from '@fuse/utils';

@Component({
    selector: 'fuse-file-list',
    templateUrl: './grid-carousel.component.html',
    styleUrls: ['./grid-carousel.component.scss'],
    animations: fuseAnimations
})
export class GridCarouselComponent implements OnInit {
    files: any;
    dataSource: any | null;
    displayedColumns = ['icon', 'date', 'position1', 'position2', 'position3', 'position4', 'position5', 'end'];
    // displayedColumns = ['icon', 'date', 'end'];
    selected: any;
    dialogRef: any;


    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild('filter') filter: ElementRef;
    @ViewChild(MatSort) sort: MatSort;

    constructor(private ListCarouselService: ListCarouselService, private router: Router, public dialog: MatDialog) {
        this.ListCarouselService.onPlacementsChanged.subscribe(placements => {
            debugger;
            // this.dataSource = new GridCarouselDataSource(this.ListCarouselService, this.paginator, this.sort);
            this.dataSource = this.ListCarouselService.placements;
        });
        // this.ListCarouselService.onFilesChanged.subscribe(files => {
        //     this.files = files;
        // });
        // this.ListCarouselService.onFileSelected.subscribe(selected => {
        //     this.selected = selected;
        // });
    }

    ngOnInit() {
        debugger;
        // this.dataSource = new GridCarouselDataSource(this.ListCarouselService, this.paginator, this.sort);
        // this.dataSource = new GridCarouselDataSource(this.ListCarouselService, this.paginator, this.sort);
        this.dataSource = this.ListCarouselService.placements;
        // Observable.fromEvent(this.filter.nativeElement, 'keyup')
        //           .debounceTime(150)
        //           .distinctUntilChanged()
        //           .subscribe(() => {
        //               if ( !this.dataSource )
        //               {
        //                   return;
        //               }
        //               this.dataSource.filter = this.filter.nativeElement.value;
        //           });
    }

    onSelect(selected) {
        this.ListCarouselService.onFileSelected.next(selected);
    }

    openUserDashboard(event, userid) {
        this.router.navigateByUrl("apps/dashboards/project/" + userid);
    }

    openUserEdit(event, userid) {
        this.router.navigateByUrl("pages/administration/edit-user/" + userid);
    }

    editPosition(slot) {
        // alert("function called" + "---" + data);
        debugger;
        this.dialogRef = this.dialog.open(FuseCalendarEventFormDialogComponent, {
            panelClass: 'event-form-dialog',
            data: {
                action: 'editPlacement',
                slot: slot,
            }
        });

        this.dialogRef.afterClosed()
            .subscribe(response => {

            });
    }

    addDatePosition(date, position) {
        debugger;
        //alert("hello");
        this.dialogRef = this.dialog.open(FuseCalendarEventFormDialogComponent, {
            panelClass: 'event-form-dialog',
            data: {
                action: 'addDatePositionPlacement',
                date: date,
                position: position
            }
        });

        this.dialogRef.afterClosed()
            .subscribe(response => {
                debugger;
                var startDate: any = new Date()
                var endDate: any = new Date();
                endDate.setDate(endDate.getDate() + 6);
                startDate = this.ListCarouselService.getQueryFormattedDate(startDate);
                endDate = this.ListCarouselService.getQueryFormattedDate(endDate);
                if (response.action == "add") {
                    this.ListCarouselService.addCarouselPlacement(response.slot).then(response => {
                        debugger;
                        this.ListCarouselService.getFiles(startDate, endDate);
                    });
                }
                else if(response.action == "edit"){
                    this.ListCarouselService.updateCarouselPlacement(response.slot).then(response => {
                        debugger;
                        this.ListCarouselService.getFiles(startDate, endDate);
                    });
                }
                else if(response.action == "delete"){
                    debugger;
                    this.ListCarouselService.deleteCarouselPlacement(response.slot).then(response => {
                        debugger;
                        this.ListCarouselService.getFiles(startDate, endDate);
                    });
                }
            });
    }
}

export class GridCarouselDataSource extends DataSource<any>
{
    _filterChange = new BehaviorSubject('');
    _filteredDataChange = new BehaviorSubject('');

    get filteredData(): any {
        return this._filteredDataChange.value;
    }

    set filteredData(value: any) {
        this._filteredDataChange.next(value);
    }

    get filter(): string {
        return this._filterChange.value;
    }

    set filter(filter: string) {
        this._filterChange.next(filter);
    }
    constructor(private ListCarouselService: ListCarouselService, private _paginator: MatPaginator, private _sort: MatSort) {
        super();
        this.filteredData = this.ListCarouselService.placements;
    }

    /** Connect function called by the table to retrieve one stream containing the data to render. */
    // connect(): Observable<any[]>
    // {
    //     return this.ListCarouselService.onFilesChanged;
    // }
    connect(): Observable<any[]> {
        const displayDataChanges = [
            this.ListCarouselService.onFilesChanged,
            this._paginator.page,
            this._filterChange,
            // this._sort.sortChange
        ];

        return Observable.merge(...displayDataChanges).map(() => {
            let data = this.ListCarouselService.placements.slice();

            data = this.filterData(data);

            this.filteredData = [...data];

            //data = this.sortData(data);

            // Grab the page's slice of data.
            const startIndex = this._paginator.pageIndex * this._paginator.pageSize;
            return data.splice(startIndex, this._paginator.pageSize);
        });
    }

    filterData(data) {
        if (!this.filter) {
            return data;
        }
        return FuseUtils.filterArrayByString(data, this.filter);
    }


    disconnect() {
    }
}
