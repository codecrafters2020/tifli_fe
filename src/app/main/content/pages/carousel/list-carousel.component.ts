import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators, FormsModule, FormControl } from '@angular/forms';

import { fuseAnimations } from '@fuse/animations';
import { MAT_DIALOG_DATA, MatDialogRef,MatDialog } from '@angular/material';

import { ListCarouselService } from './list-carousel.service';
import { Router } from '@angular/router';
import { FuseCalendarEventFormDialogComponent } from './slot-form/slot-form.component';

@Component({
    selector     : 'fuse-file-manager',
    templateUrl  : './list-carousel.component.html',
    styleUrls    : ['./list-carousel.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class ListCarouselComponent implements OnInit
{
    selected: any;
    pathArr: string[];
    dialogRef:any;
    toDate: any = new Date();
    fromDate: any = new Date();

    placementShedulingForm:any;
    pages:any[] = [
        {
            name:'Online Offers',
            id:1
        },
        {
            name:'In Store Offers',
            id:2
        },
        {
            name:'4th of july sales 2018',
            id:3
        },
        {
            name:'50% off best buy coupons, promo codes Black Friday 2018',
            id:4
        }
    ]

    constructor(private ListCarouselService: ListCarouselService, private router: Router, private formBuilder: FormBuilder,public dialog: MatDialog)
    {
    }

    ngOnInit()
    {
        this.fromDate = new Date();
        this.toDate = new Date();
        this.toDate.setDate(this.fromDate.getDate() + 6);
        
        this.ListCarouselService.onFileSelected.subscribe(selected => {
            this.selected = selected;
           // this.pathArr = selected.location.split('>');
        });

        this.placementShedulingForm = this.formBuilder.group({
            selectPage: ['', Validators.required],
            fromDate: ['', Validators.required],
            toDate: ['', Validators.required],
        });
    }

    openUserAdd(){
        this.router.navigateByUrl("pages/administration/add-user/");
    }

    refreshGrid(){
        var startDate = this.getQueryFormattedDate(this.fromDate);
        var endDate = this.getQueryFormattedDate(this.toDate);

        this.ListCarouselService.getFiles(startDate, endDate);
    }

    editPosition(event,data){
        // alert("function called" + "---" + data);
        this.dialogRef = this.dialog.open(FuseCalendarEventFormDialogComponent, {
            panelClass: 'event-form-dialog',
            data      : {
                event : data,
            }
        });

        this.dialogRef.afterClosed()
            .subscribe(response => {
                debugger;
                this.ListCarouselService.addCarouselPlacement(response.slot).then(response => {
                    debugger;
                    var startDate = this.getQueryFormattedDate(this.fromDate);
                    var endDate = this.getQueryFormattedDate(this.toDate);
                    this.ListCarouselService.getFiles(startDate, endDate);
                });
            });
    }

    getQueryFormattedDate(date) {
        var year = date.getFullYear();
      
        var month = (1 + date.getMonth()).toString();
        month = month.length > 1 ? month : '0' + month;
      
        var day = date.getDate().toString();
        day = day.length > 1 ? day : '0' + day;
        
        return year + '-' + month + '-' + day;
    }
}
