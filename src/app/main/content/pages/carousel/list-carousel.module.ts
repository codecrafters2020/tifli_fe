import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CdkTableModule } from '@angular/cdk/table';

import { MatAutocompleteModule, MatFormFieldModule, MatInputModule, MatSelectModule, MatStepperModule, MatButtonModule, MatIconModule, MatRippleModule, MatSidenavModule, MatSlideToggleModule, MatTableModule, MatMenuModule, MatPaginatorModule, MatSortModule, MatToolbarModule, MatNativeDateModule } from '@angular/material';

import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatRadioModule} from '@angular/material/radio';
import { FuseSharedModule } from '@fuse/shared.module';

import { ListCarouselComponent } from './list-carousel.component';
import { ListCarouselService } from './list-carousel.service';
import { GridCarouselComponent } from './grid/grid-carousel.component';
import { SidenavCarouselMainComponent } from './sidenavs/main/sidenav-carousel-main.component';
import { SidenavCarouselDetailsComponent } from './sidenavs/details/sidenav-carousel-details.component';
import { FuseCalendarEventFormDialogComponent } from './slot-form/slot-form.component';
import { FancyImageUploaderModule } from 'ng2-fancy-image-uploader';
import {MatTooltipModule} from '@angular/material/tooltip';

const routes: Routes = [
    {
        path     : '**',
        component: ListCarouselComponent,
        children : [],
        resolve  : {
            files: ListCarouselService
        }
    }
];

@NgModule({
    declarations: [
        ListCarouselComponent,
        GridCarouselComponent,
        SidenavCarouselMainComponent,
        SidenavCarouselDetailsComponent,
        FuseCalendarEventFormDialogComponent
    ],
    imports     : [
        RouterModule.forChild(routes),
        MatTooltipModule,

        CdkTableModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        MatStepperModule,
        MatButtonModule,
        MatIconModule,
        MatRippleModule,
        MatSlideToggleModule,
        MatSidenavModule,
        MatTableModule,
        MatMenuModule,
        MatPaginatorModule, 
        MatSortModule,
        MatNativeDateModule,

        FuseSharedModule,
        MatDatepickerModule,
        MatCheckboxModule,
        MatRadioModule,
        MatToolbarModule,
        MatAutocompleteModule,
        FancyImageUploaderModule
    ],
    providers   : [
        ListCarouselService
    ],
    entryComponents: [FuseCalendarEventFormDialogComponent]
})
export class CarouselPlacementModule
{
}
