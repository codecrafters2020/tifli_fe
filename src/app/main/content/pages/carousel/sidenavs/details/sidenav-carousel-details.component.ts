import { Component, OnInit } from '@angular/core';

import { fuseAnimations } from '@fuse/animations/index';

import { ListCarouselService } from '../../list-carousel.service';

@Component({
    selector   : 'fuse-file-manager-details-sidenav',
    templateUrl: './sidenav-carousel-details.component.html',
    styleUrls  : ['./sidenav-carousel-details.component.scss'],
    animations : fuseAnimations
})
export class SidenavCarouselDetailsComponent implements OnInit
{

    selected: any;

    constructor(private ListCarouselService: ListCarouselService)
    {

    }

    ngOnInit()
    {
        this.ListCarouselService.onFileSelected.subscribe(selected => {
            this.selected = selected;
        });
    }

}
