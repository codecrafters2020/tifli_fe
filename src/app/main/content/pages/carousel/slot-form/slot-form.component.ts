import { Component, Inject, ViewEncapsulation, AfterViewChecked } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef, MatDialog } from '@angular/material';

import { FancyImageUploaderOptions, UploadedFile } from 'ng2-fancy-image-uploader';

import { MatColors } from '@fuse/mat-colors';

import { CalendarEvent } from 'angular-calendar';
import { AppService } from './../../../../../app.service';
import { ListCarouselService } from '../list-carousel.service';
import { invalid } from 'moment';
import { DISABLED } from '@angular/forms/src/model';
// import { SlotModel } from '../slot.model';
// import { ScheduledPlacementService} from '../scheduled-placement.service';

@Component({
    selector: 'fuse-calendar-event-form-dialog',
    templateUrl: './slot-form.component.html',
    styleUrls: ['./slot-form.component.scss'],
    encapsulation: ViewEncapsulation.None
})

export class FuseCalendarEventFormDialogComponent {

    btndisable: boolean = false;
    /** Meta Data for Carousel Popup */
    placementTypes: any[] = [
        "Coupon",
        "Merchant",
        "Seasonal Page",
        "Category",
        "In house promo"
    ];

    positionValues: any[] = [
        1, 2, 3, 4, 5
    ];

    imageTypes: any = [
        "Full Image",
        "Half Image",
        "logo Only"
    ];

    categories: any[] = [
        { name: 'Electronics', id: 1 },
        { name: 'Household', id: 2 }
    ];

    options: FancyImageUploaderOptions = {
        thumbnailHeight: 150,
        thumbnailWidth: 150,
        uploadUrl: this.appService.uploadService + 'uploads/',
        allowedImageTypes: ['image/png', 'image/jpeg'],
        maxImageSize: 3
    };
    optionsFullDesktop: FancyImageUploaderOptions = {
        thumbnailHeight: 200,
        thumbnailWidth: 432,
        uploadUrl: this.appService.uploadService + 'uploads/',
        allowedImageTypes: ['image/png', 'image/jpeg'],
        maxImageSize: 3
    };
    optionsFullMobile: FancyImageUploaderOptions = {
        thumbnailHeight: 200,
        thumbnailWidth: 320,
        uploadUrl: this.appService.uploadService + "uploads/",
        allowedImageTypes: ['image/png', 'image/jpeg'],
        maxImageSize: 3
    };
    /** Meta data ended */

    /** Slot Object */
    slot: any = {
        id: 0,
        position: 1,
        startDate: new Date(),
        endDate: new Date(),
        type: "Coupon",
        itemId: '',
        imageType: "Full Image",
        extras: {
            id: 0,
            header: '',
            content: '',
            targetUrl: ''
        },
        images: []
    };
    fullImageDesktop: any = {};
    fullImageMobile: any = {};
    halfImage: any = {};

    dialogTitle: string = "Carousel Placement";
    slotForm: FormGroup;
    action: string = "add";

    merchantSearchResult: any[] = [
        { name: 'Usman', id: 1 },
        { name: 'Farrukh', id: 2 },
        { name: 'Umair', id: 3 }
    ];
    couponSearchResult: any[] = [
        { title: 'Save upto 30% on all samsung coupons', id: 1 },
        { title: 'Save upto 40% on all samsung coupons', id: 2 }
    ];
    seasonalSearchResult: any[] = [
        { name: 'Black Friday 2018', id: 1 },
        { name: '4th of July 2018', id: 2 },
        { name: 'Easter 2018', id: 3 }
    ];
    selectedMerchant: any = {};
    selectedCoupon: any = { title: 'Save upto 40% on all samsung coupons', id: 2 };
    selectedSeasonal: any = {}
    selectedId: any;
    halfImageHeader: any = '';
    halfImageContent: any = '';

    couponAttached: boolean = false;

    isTypeCoupon: boolean = false;
    isTypeMerchant: boolean = false;
    isTypeSeasonal: boolean = false;
    isTypeCategory: boolean = false;
    isTypeInHousePromo: boolean = false;

    isImageTypeFull: boolean = false;
    isImageTypeHalf: boolean = false;
    isImageTypeLogo: boolean = false;

    showFullImageDesktop: boolean = false;
    showFullImageMobile: boolean = false;
    showHalfImage: boolean = false;
    showFullImageDesktopUploader: boolean = true;
    showFullImageMobileUploader: boolean = true;
    showHalfImageUploader: boolean = true;
    
    showFullImageDesktopCancel: boolean = false;
    showFullImageMobileCancel: boolean = false;
    showHalfImageCancel:boolean = false;

    fullImageDesktopPath: any;
    fullImageMobilePath:any;
    halfImagePath: any;

    buttonAction:any = "add";

    constructor(
        public dialogRef: MatDialogRef<FuseCalendarEventFormDialogComponent>,
        @Inject(MAT_DIALOG_DATA) private data: any,
        private formBuilder: FormBuilder,
        private carouselPlacementService: ListCarouselService,
        private appService: AppService
        // private scheduledPlacementService: ScheduledPlacementService
    ) {
      this.categories = carouselPlacementService.categories;
        this.action = data.action;
        if (this.action == "addDatePositionPlacement") {
            this.slot.startDate = new Date(data.date);
            this.slot.endDate = new Date(data.date);
            this.slot.position = data.position;
        }
        else if(this.action == "editPlacement"){
            debugger;
            this.buttonAction = "edit";
            this.slot.endDate = new Date(data.slot.endDate);
            this.slot.extras.id = data.slot.carouselPlacementExtras.id;
            this.slot.extras.header = data.slot.carouselPlacementExtras.header;
            this.slot.extras.content = data.slot.carouselPlacementExtras.content;
            this.slot.extras.targetUrl = data.slot.carouselPlacementExtras.targetUrl;
            this.slot.id = data.slot.id;
            this.slot.imageType = data.slot.imageType;
            this.slot.itemId = data.slot.itemId;
            this.slot.position = data.slot.position;
            this.slot.startDate = new Date(data.slot.startDate);
            this.slot.type = data.slot.type;

            this.selectedId = this.slot.itemId;
            this.halfImageHeader = this.slot.extras.header;
            this.halfImageContent = this.slot.extras.content;

            if(this.slot.imageType == "Full Image"){
                
                this.showFullImageDesktop = true;
                this.showFullImageDesktopUploader = false;
                this.fullImageDesktopPath = this.appService.cdnUrl + this.slot.id + "_full-desktop.png";

                this.showFullImageMobile = true;
                this.showFullImageMobileUploader = false;
                this.fullImageMobilePath = this.appService.cdnUrl + this.slot.id + "_full-mobile.png";
            }

            if(this.slot.imageType == "Half Image"){
                this.showHalfImage = true;
                this.showHalfImageUploader = false;
                this.halfImagePath = this.appService.cdnUrl + this.slot.id + "_half.png";
            }

            if(this.slot.type == "Merchant"){
                debugger;
                this.carouselPlacementService.getMerchant(this.slot.itemId).then(response => {
                    this.selectedMerchant = response.data;
                    this.slotForm.controls.merchantId.setValue(this.selectedMerchant);
                });
            }
        }



        // this.selectedMerchant = this.event.merchant;
        // this.position = this.event.title.split("-")[0];

        // if ( this.action === 'edit' )
        // {
        //     this.dialogTitle = this.event.title;
        //     this.titleParts = this.event.title.split("<br>");
        //     if(this.titleParts.length == 1)
        //         this.dialogTitle = this.titleParts[0];
        //     else
        //         this.dialogTitle = this.titleParts[0] + " " + this.titleParts[1];
        // }
        // else
        // {
        //     this.dialogTitle = 'New Event';
        //     this.event = new SlotModel({
        //         start: data.date,
        //         end  : data.date
        //     });
        // }

        this.subscribeMerchants();

        this.subscribeSeasonalEvents();

        // this.scheduledPlacementService.onCouponsChanged.subscribe(couponsUpdatedList => {
        //     debugger;
        //     this.couponSearchResult = couponsUpdatedList;
        // });

        this.slotForm = this.createSlotForm();

        this.updatePlacementTypeChecks();

        this.updateImageTypeChecks();

        this.searchMerchants();

        this.searchSeasonalEvents();

        this.getCoupon();


        // this.eventForm.controls.coupon.valueChanges
        // .debounceTime(400)
        // .subscribe(searchTerm => {
        // 	this.scheduledPlacementService.searchCoupon(searchTerm);
        // })

    }

    // ngAfterViewChecked (){
    //     debugger;
    //     var btn = document.getElementsByClassName("remove");
    // }

    subscribeMerchants() {
        this.carouselPlacementService.onMerchantsChanged.subscribe(merchantsUpdatedList => {
            debugger;
            this.merchantSearchResult = merchantsUpdatedList;
        });
    }

    subscribeSeasonalEvents() {
        this.carouselPlacementService.onSeasonalChanged.subscribe(seasonalUpdatedList => {
            debugger;
            this.seasonalSearchResult = seasonalUpdatedList;
        });
    }

    getCoupon() {
        debugger;
        this.slotForm.controls.couponId.valueChanges
            .debounceTime(400)
            .subscribe(couponId => {
                this.carouselPlacementService.getCoupon(couponId).then(response => {
                    debugger;
                    if (!(response.meta.message == "success" && response.meta.status == "success")) {
                        this.couponAttached = false;
                        // var buttonn = <HTMLInputElement> document.getElementById("btn1");
                        // buttonn.disabled = true;
                        this.btndisable = true;
                        
                    }
                    else {
                        this.btndisable = false;
                        this.couponAttached = true;
                        this.selectedCoupon = response.data;
                        this.halfImageHeader = this.selectedCoupon.title;
                        this.halfImageContent = this.selectedCoupon.description;
                        var merchantId = this.selectedCoupon.merchant.id;
                        this.carouselPlacementService.getMerchant(merchantId).then(response => {
                            this.selectedCoupon.merchant = response.data;
                        });
                        
                    }
                });
            });
    }

    updateImageTypeChecks() {
        this.slotForm.controls.imageType.valueChanges
            .subscribe(imageType => {
                if (imageType == "Full Image") {
                    debugger;
                    this.isImageTypeFull = true;
                    this.isImageTypeHalf = false;
                    this.isImageTypeLogo = false;
                }
                else if (imageType == "Half Image") {
                    debugger;
                    this.isImageTypeFull = false;
                    this.isImageTypeHalf = true;
                    this.isImageTypeLogo = false;
                }
                else if (imageType == "logo Only") {
                    debugger;
                    this.isImageTypeFull = false;
                    this.isImageTypeHalf = false;
                    this.isImageTypeLogo = true;
                }
            });
    }

    updatePlacementTypeChecks() {
        this.slotForm.controls.placementType.valueChanges
            .subscribe(placementType => {
                if (placementType == "Coupon") {
                    debugger;
                    this.isTypeCoupon = true;
                    this.isTypeMerchant = false;
                    this.isTypeSeasonal = false;
                    this.isTypeCategory = false;
                    this.isTypeInHousePromo = false;
                }
                else if (placementType == "Merchant") {
                    debugger;
                    this.isTypeCoupon = false;
                    this.isTypeMerchant = true;
                    this.isTypeSeasonal = false;
                    this.isTypeCategory = false;
                    this.isTypeInHousePromo = false;
                }
                else if (placementType == "Seasonal Page") {
                    debugger;
                    this.isTypeCoupon = false;
                    this.isTypeMerchant = false;
                    this.isTypeSeasonal = true;
                    this.isTypeCategory = false;
                    this.isTypeInHousePromo = false;
                }
                else if (placementType == "Category") {
                    debugger;
                    this.isTypeCoupon = false;
                    this.isTypeMerchant = false;
                    this.isTypeSeasonal = false;
                    this.isTypeCategory = true;
                    this.isTypeInHousePromo = false;
                }
                else if (placementType == "In house promo") {
                    debugger;
                    this.isTypeCoupon = false;
                    this.isTypeMerchant = false;
                    this.isTypeSeasonal = false;
                    this.isTypeCategory = false;
                    this.isTypeInHousePromo = true;
                }
            });
    }

    searchMerchants() {
        debugger;
        this.slotForm.controls.merchantId.valueChanges
            .debounceTime(400)
            .subscribe(searchTerm => {
                this.carouselPlacementService.searchMerchant(searchTerm);
            });
    }

    searchSeasonalEvents() {
        this.slotForm.controls.seasonalId.valueChanges
            .debounceTime(400)
            .subscribe(searchTerm => {
                this.carouselPlacementService.searchSeasonalEvents(searchTerm);
            });
    }

    onCategoryChange(categoryId) {
        debugger;
        for (let i = 0; i < this.categories.length; i++) {
            if (categoryId == this.categories[i].id) {
                this.halfImageHeader = this.categories[i].name;
                this.halfImageContent = this.categories[i].description;
            }
        }

    }

    createSlotForm() {
        return new FormGroup({
            position: new FormControl(),
            startDate: new FormControl(),
            endDate: new FormControl(),
            merchantId: new FormControl(),
            couponId: new FormControl(),
            seasonalId: new FormControl(),
            categoryId: new FormControl(),
            imageType: new FormControl(),
            halfImageHeading: new FormControl(),
            halfImageContent: new FormControl(),
            targetUrl: new FormControl(),
            placementType: new FormControl(),
        });
    }

    displaySlectedMerchant(selectedMerchant: any): string | undefined {
        debugger;
        if (selectedMerchant != null) {
            this.slot.itemId = selectedMerchant.id;
            this.halfImageHeader = selectedMerchant.name;
            this.halfImageContent = selectedMerchant.description;
        }
        return selectedMerchant ? selectedMerchant.name : undefined;
    }

    displaySlectedSeasonal(selectedSeasonal: any): string | undefined {
        debugger;
        if (selectedSeasonal != null) {
            this.slot.itemId = selectedSeasonal.id;
            this.halfImageHeader = selectedSeasonal.name;
            this.halfImageContent = selectedSeasonal.description;
        }
        return selectedSeasonal ? selectedSeasonal.name : undefined;
    }

    displaySlectedCoupon(selectedCoupon: any): string | undefined {
        debugger;
        // this.selectedCoupon = selectedCoupon;
        // if(this.selectedCoupon.merchant != null || this.selectedCoupon.merchant != undefined)
        // {
        //     this.dialogTitle = this.position + " - " + this.selectedCoupon.id + " " + this.selectedCoupon.merchant.name;
        //     this.eventForm.controls.title.setValue(this.position + " - " + this.selectedCoupon.id + "<br>" + this.selectedCoupon.merchant.name);
        //     this.eventForm.controls.merchant.setValue(this.selectedCoupon.merchant);
        // }
        return selectedCoupon ? selectedCoupon.title : undefined;
    }

    onFullImageDesktopUpload(file: UploadedFile) {
        debugger;
        let response = JSON.parse(file.response);
        this.fullImageDesktop['fileName'] = response.data.fileName;
        this.fullImageDesktop['suffix'] = 'full-desktop';
        var index = this.slot.images.findIndex(image => image.suffix == "full-desktop");
        if (index == -1) {
            this.slot.images.push(this.fullImageDesktop);
        }
        else {
            this.slot.images[index] = this.fullImageDesktop;
        }
        console.log(file.response);

    }

    onFullImageMobileUpload(file: UploadedFile) {
        debugger;
        let response = JSON.parse(file.response);
        this.fullImageMobile['fileName'] = response.data.fileName;
        this.fullImageMobile['suffix'] = 'full-mobile';
        var index = this.slot.images.findIndex(image => image.suffix == "full-mobile");
        if (index == -1) {
            this.slot.images.push(this.fullImageMobile);
        }
        else {
            this.slot.images[index] = this.fullImageMobile;
        }
        console.log(file.response);

    }

    onHalfImageUpload(file: UploadedFile) {
        debugger;
        let response = JSON.parse(file.response);
        this.halfImage['fileName'] = response.data.fileName;
        this.halfImage['suffix'] = 'half';
        var index = this.slot.images.findIndex(image => image.suffix == "half");
        if (index == -1) {
            this.slot.images.push(this.halfImage);
        }
        else {
            this.slot.images[index] = this.halfImage;
        }
        console.log(file.response);

    }

    closeDialog(action) {
        debugger;
        // this.removeFullDesktopImageIfNotPresentInDOM();
        // this.removeFullMobileImageIfNotPresentInDOM();
        // this.removeHalfImageIfNotPresentInDOM();
        // this.removeUnwantedImagesFromSlot();
        this.addExtras();
        this.slot.itemId = this.selectedId;

        let data = {
            slot: this.slot,
            images:this.slot.images,
            action:action
        }
        if(action == 'delete'){
            this.carouselPlacementService.deleteCarouselPlacement(this.slot);
        }
        else if(action == 'edit'){
            this.carouselPlacementService.updateCarouselPlacement(this.slot).then(response => {
                var fromDate:any = new Date();
                var toDate:any = new Date();
                toDate.setDate(fromDate.getDate() + 6);
                fromDate = this.carouselPlacementService.getQueryFormattedDate(fromDate);
                toDate = this.carouselPlacementService.getQueryFormattedDate(toDate);
                this.carouselPlacementService.getFiles(fromDate, toDate);
            });
        }
        this.dialogRef.close(data);
    }

    addExtras(){
        this.slot.extras.header = this.halfImageHeader;
        this.slot.extras.content = this.halfImageContent;
    }

    removeUnwantedImagesFromSlot(){
        var index: any;
        if(this.slot.imageType == "Half Image"){
            index = this.slot.images.findIndex(image => image.suffix == "full-desktop")
            if(index != -1 || index != null)
                this.slot.images.splice(index,1);
            index = this.slot.images.findIndex(image => image.suffix == "full-mobile")
            if(index != -1 || index != null)
                this.slot.images.splice(index,1);
        }
        else if(this.slot.imageType == "Full Image"){
            index = this.slot.images.findIndex(image => image.suffix == "half")
            if(index != -1 || index != null)
                this.slot.images.splice(index,1);
        }
    }

    removeFullDesktopImageIfNotPresentInDOM() {
        var fancyUploaderFullDesktop = document.getElementById("fullImageDesktop");
        if (fancyUploaderFullDesktop != null){
            var images = document.getElementsByTagName("img");
            var isImagePresent = false;
            for (let i = 0; i < images.length; i++) {
                if (fancyUploaderFullDesktop.contains(images[i])) {
                    isImagePresent = true
                    break;
                }
            }
            if (isImagePresent == false) {
                var index = this.slot.images.findIndex(image => image.suffix == "full-desktop")
                if (index != -1) {
                    this.slot.images.splice(index, 1);
                }
            }
        }
    }

    removeFullMobileImageIfNotPresentInDOM() {
        var fancyUploaderFullMobile = document.getElementById("fullImageMobile");
        if (fancyUploaderFullMobile != null) {
            var images = document.getElementsByTagName("img");
            var isImagePresent = false;
            for (let i = 0; i < images.length; i++) {
                if (fancyUploaderFullMobile.contains(images[i])) {
                    isImagePresent = true
                    break;
                }
            }
            if (isImagePresent == false) {
                var index = this.slot.images.findIndex(image => image.suffix == "full-mobile")
                if (index != -1) {
                    this.slot.images.splice(index, 1);
                }
            }
        }
    }

    removeHalfImageIfNotPresentInDOM() {
        var fancyUploaderHalf = document.getElementById("HalfImage");
        if (fancyUploaderHalf != null) {
            var images = document.getElementsByTagName("img");
            var isImagePresent = false;
            for (let i = 0; i < images.length; i++) {
                if (fancyUploaderHalf.contains(images[i])) {
                    isImagePresent = true
                    break;
                }
            }
            if (isImagePresent == false) {
                var index = this.slot.images.findIndex(image => image.suffix == "half")
                if (index != -1) {
                    this.slot.images.splice(index, 1);
                }
            }
        }
    }

    onFullImageDesktopRemove(event: any) {
        alert("hello");
    }

    changeFullImageDesktop(){
        debugger;
        this.showFullImageDesktopUploader = true;
        this.showFullImageDesktopCancel = true
    }

    cancelFullImageDesktop(){
        this.showFullImageDesktopUploader = false;
        this.showFullImageDesktopCancel = false;
    }

    changeFullImageMobile(){
        debugger;
        this.showFullImageMobileUploader = true;
        this.showFullImageMobileCancel = true
    }

    cancelFullImageMobile(){
        this.showFullImageMobileUploader = false;
        this.showFullImageMobileCancel = false;
    }

    changeHalfImage(){
        debugger;
        this.showHalfImageUploader = true;
        this.showHalfImageCancel = true
    }

    cancelHalfImage(){
        this.showHalfImageUploader = false;
        this.showHalfImageCancel = false;
    }
}
