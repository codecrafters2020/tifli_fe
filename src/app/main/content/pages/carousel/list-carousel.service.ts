import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';

import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { AppService } from './../../../../app.service';

import * as moment from 'moment';

@Injectable()
export class ListCarouselService implements Resolve<any>
{
    users: any[] = [];
    placements: any[] = [];
    categories: any;
    onMerchantsChanged: BehaviorSubject<any> = new BehaviorSubject({});
    onCouponsChanged: BehaviorSubject<any> = new BehaviorSubject({});
    onSeasonalChanged: BehaviorSubject<any> = new BehaviorSubject({});

    onPlacementsChanged: BehaviorSubject<any> = new BehaviorSubject({});
    onFilesChanged: BehaviorSubject<any> = new BehaviorSubject({});
    onFileSelected: BehaviorSubject<any> = new BehaviorSubject({});

    constructor(private http: HttpClient, private appService: AppService)
    {
    }

    /**
     * The File Manager App Main Resolver
     * 
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any
    {

        // var startDate = moment.utc().format('YYYY-MM-DD HH:mm:ss');        
        // var stillUtc = moment.utc(startDate).toDate();
        // var local = moment(stillUtc).local().format('YYYY-MM-DD');
        // var utc = local.();\

        // let localTime = Math.floor(Math.floor(moment().valueOf() / 1000) / 1000) * 1000;
        // var dateString = moment.unix(localTime).format("MM/DD/YYYY HH:mm:ss");
        // console.log(localTime);
        // console.log(dateString);   
        debugger;
        var startDateTimeLocal: any = new Date();
        var endDateTimeLocal: any = new Date();
        endDateTimeLocal.setDate(startDateTimeLocal.getDate() + 6);

        //var startDateNoTime = startDateTimeLocal.getFullYear() + "-" + (startDateTimeLocal.getMonth() + 1).toString() + "-" + startDateTimeLocal.getDate();
        
        
        // var startDateLocal = new Date(startDateNoTime);

        // var start_utc_date = startDateLocal.getUTCDate();
        // var start_utc_month = startDateLocal.getUTCMonth() + 1;
        // var start_utc_year = startDateLocal.getUTCFullYear();

        //var endDateNoTime = endDateTimeLocal.getFullYear() + "-" + (endDateTimeLocal.getMonth() + 1).toString() + "-" + endDateTimeLocal.getDate();
        
        
        // var endDateLocal = new Date(endDateNoTime);

        // var end_utc_date = endDateLocal.getUTCDate();
        // var end_utc_month = endDateLocal.getUTCMonth() + 1;
        // var end_utc_year = endDateLocal.getUTCFullYear();

        // var startDate: any = start_utc_year + "-" + start_utc_month + "-" + start_utc_date;
        // var endDate: any = end_utc_year + "-" + end_utc_month + "-" + end_utc_date;

        // var endDate: any = startDate;

        var startDate: any = new Date();
        var endDate: any = new Date();
        endDate.setDate(startDate.getDate() + 6);

        startDate = this.getQueryFormattedDate(startDate);        
        endDate = this.getQueryFormattedDate(endDate);

        // var startDate = moment(startDateTimeLocal).unix();
        // var endDate = moment(endDateTimeLocal).unix();
        
        // return new Promise((resolve, reject) => {

        //     Promise.all([
        //         this.getFiles(startDate,endDate),
        //         this.getCategories()
        //     ]).then(
        //         ([placements, categories]) => {
        //             resolve();
        //         },
        //         reject
        //     );
        // });
    }

    addCarouselPlacement(slot): Promise<any>{
        debugger;
        return new Promise((resolve, reject) => {
            this.http.post(this.appService.placementService + 'carousel/',slot)
                .subscribe((response: any) => {
                    debugger;
                    resolve(response);
                }, reject);
        });
    }

    updateCarouselPlacement(slot): Promise<any>{
        return new Promise((resolve, reject) => {
            this.http.put(this.appService.placementService + 'carousel/' + slot.id, slot)
                .subscribe((response: any) => {
                    debugger;
                    resolve(response);
                }, reject);
        });
    }

    deleteCarouselPlacement(slot): Promise<any>{
        return new Promise((resolve, reject) => {
            this.http.delete(this.appService.placementService + 'carousel/' + slot.id)
                .subscribe((response: any) => {
                    debugger;
                    resolve(response);
                }, reject);
        });
    }

    getFiles(startDate,endDate): Promise<any>
    {
        
        debugger;
        return new Promise((resolve, reject) => {
            this.http.get(this.appService.placementService + 'carousel/?startDate=' + startDate + '&endDate=' + endDate)
            // this.http.get(this.appService.apiUrl + 'cms/api/placements/carousel/?startDate=2018-06-07&endDate=2018-06-10')
                .subscribe((response: any) => {
                    debugger;
                    this.placements = response.data;
                    for(let i=0;i<this.placements.length;i++){
                        //this.placements[i].date = moment(this.placements[i].date).local().format("MMM DD, YYYY");
                        // this.placements[i].date = moment(this.placements[i].date).format("MMM DD, YYYY");
                        var date = this.placements[i].date.split("T")[0];
                        this.placements[i].date = moment(date).format("MMM DD, YYYY");
                        if(this.placements[i].positions[1] != null){
                            this.placements[i].positions[1]["startDate"] = this.placements[i].positions[1].start;
                            this.placements[i].positions[1].start = moment(this.placements[i].positions[1].start).local();
                            this.placements[i].positions[1].end = moment(this.placements[i].positions[1].end).local();
                        }
                        if(this.placements[i].positions[2] != null){
                            this.placements[i].positions[2]["startDate"] = this.placements[i].positions[2].start;
                            this.placements[i].positions[2].start = moment(this.placements[i].positions[2].start).local();
                            this.placements[i].positions[2].end = moment(this.placements[i].positions[2].end).local();
                        }
                        if(this.placements[i].positions[3] != null){
                            this.placements[i].positions[3]["startDate"] = this.placements[i].positions[3].start;
                            this.placements[i].positions[3].start = moment(this.placements[i].positions[3].start).local();
                            this.placements[i].positions[3].end = moment(this.placements[i].positions[3].end).local();
                        }
                        if(this.placements[i].positions[4] != null){
                            this.placements[i].positions[4]["startDate"] = this.placements[i].positions[4].start;
                            this.placements[i].positions[4].start = moment(this.placements[i].positions[4].start).local();
                            this.placements[i].positions[4].end = moment(this.placements[i].positions[4].end).local();
                        }
                        if(this.placements[i].positions[5] != null){
                            this.placements[i].positions[5]["startDate"] = this.placements[i].positions[5].start;
                            this.placements[i].positions[5].start = moment(this.placements[i].positions[5].start).local();
                            this.placements[i].positions[5].end = moment(this.placements[i].positions[5].end).local();
                        }

                        //this.placements[i].date = new Date(this.placements[i].date);
                        // this.placements[i].date.toLocaleString();
                        // this.placements[i].date = this.getFormattedDate(this.placements[i].date);
                    }
                    this.onPlacementsChanged.next(this.placements);
                    console.log(this.placements);
                    resolve(response);
                }, reject);
        });
    }

    getFormattedDate(date) {
        var year = date.getFullYear();
      
        var month = (1 + date.getMonth()).toString();
        month = month.length > 1 ? month : '0' + month;
      
        var day = date.getDate().toString();
        day = day.length > 1 ? day : '0' + day;
        
        return month + '/' + day + '/' + year;
    }

    getQueryFormattedDate(date) {
        var year = date.getFullYear();
      
        var month = (1 + date.getMonth()).toString();
        month = month.length > 1 ? month : '0' + month;
      
        var day = date.getDate().toString();
        day = day.length > 1 ? day : '0' + day;
        
        return year + '-' + month + '-' + day;
    }

    getCategories(): Promise<any>
    {
        return new Promise((resolve, reject) => {
            this.http.get(this.appService.merchantService + 'categories/')
                .subscribe((response: any) => {
                    this.categories = response.data;
                    resolve(response);
                }, reject);
        });
    }

    getCoupon(couponId): Promise<any>
    {
        return new Promise((resolve, reject) => {
            this.http.get(this.appService.couponService + 'coupons/' + couponId)
                .subscribe((response: any) => {
                    resolve(response);
                }, reject);
        });
    }

    getMerchant(merchantId): Promise<any>
    {
        return new Promise((resolve, reject) => {
            this.http.get(this.appService.merchantService + merchantId)
                .subscribe((response: any) => {
                    resolve(response);
                }, reject);
        });
    }

    searchMerchant(searchTerm) {
        debugger;
        this.http.get(this.appService.merchantService + 'search/?name=' + searchTerm).subscribe(response => {
            /* some operations or conditiond on response */

            this.onMerchantsChanged.next(response['data']);
        })
    }

    searchSeasonalEvents(searchTerm) {
        debugger;
        this.http.get(this.appService.couponService + 'seasonal/search/?name=' + searchTerm).subscribe(response => {
            /* some operations or conditiond on response */

            this.onSeasonalChanged.next(response['data']);
        })
    }

    searchCoupon(searchTerm) {
        debugger;
        // this.http.get(this.appService.apiUrl + 'cms/api/merchants/search/?name=' + searchTerm).subscribe(response => {
        //     /* some operations or conditiond on response */

        //     this.onCouponsChanged.next(response['data']);
        // })
        var coupons: any[] = [
            {
                title: 'Save upto 30% on all samsung coupons',
                id: 1,
                description: 'This is description 1',
                merchant: {
                    id: 30,
                    name: 'Apple'
                }
            },
            {
                title: 'Save upto 40% on all samsung coupons',
                id: 2,
                description: 'This is description 2',
                merchant: {
                    id: 31,
                    name: 'Nike'
                }
            },
            {
                title: 'Save upto 50% on all samsung coupons',
                id: 3,
                description: 'This is description 3',
                merchant: {
                    id: 32,
                    name: 'Lenovo'
                }
            },
            {
                title: 'Save upto 60% on all samsung coupons',
                id: 4,
                description: 'This is description 4',
                merchant: {
                    id: 33,
                    name: ''
                }
            }
        ]

        this.onCouponsChanged.next(coupons);
    }

    checkImage(image): Promise<any>{
        return new Promise((resolve, reject) => {
            this.http.get(image)
                .subscribe((response: any) => {
                    debugger;
                    resolve(response);
                }, reject);
        });
    }
}
