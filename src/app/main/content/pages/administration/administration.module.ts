import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { FuseAngularMaterialModule } from '../../components/angular-material/angular-material.module';

import { FuseSharedModule } from '@fuse/shared.module';

const routes = [
    {
        path        : 'list-user',
        loadChildren: './list-user/list-user.module#ListUserModule'
    },
    {
        path        : 'add-user',
        loadChildren: './add-user/add-user.module#AddUserModule'
    },
    {
        path        : 'edit-user/:id',
        loadChildren: './edit-user/edit-user.module#EditUserModule'
    },
    {
        path        : 'reset-password',
        loadChildren: './reset-password/reset-password.module#ResetPasswordModule'
    },
    {
        path        : 'add-role',
        loadChildren: './add-role/add-role.module#AddRoleModule'
    },
    {
        path        : 'list-role',
        loadChildren: './list-role/list-role.module#ListRoleModule'
    },
    {
        path        : 'edit-role/:id',
        loadChildren: './edit-role/edit-role.module#EditRoleModule'
    },
    {
        path        : 'list-user-activity',
        loadChildren: './list-user-activity/list-user-activity.module#ListUserActivityModule'
    }  
];

@NgModule({
    imports     : [
        FuseSharedModule,
        RouterModule.forChild(routes),
        FuseAngularMaterialModule
    ],
    declarations: []
})
export class AdministrationModule
{
}
