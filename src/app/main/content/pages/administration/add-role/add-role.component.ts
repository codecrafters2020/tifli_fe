import { Component, OnInit, ViewEncapsulation } from '@angular/core';

import { fuseAnimations } from '@fuse/animations';

import { AddRoleService } from './add-role.service';

import { Router } from '@angular/router';

@Component({
    selector     : 'fuse-file-manager',
    templateUrl  : './add-role.component.html',
    styleUrls    : ['./add-role.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class AddRoleComponent implements OnInit
{
    selected: any;
    pathArr: string[];
    activities:any[];

    constructor(private AddRoleService: AddRoleService, private router: Router)
    {
    }

    ngOnInit()
    {
        
    }

    openRoleList(){
        
        this.router.navigateByUrl("pages/administration/list-role/");
    }
}
