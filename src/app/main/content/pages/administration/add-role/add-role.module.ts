import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CdkTableModule } from '@angular/cdk/table';

import { MatCheckboxModule, MatFormFieldModule, MatInputModule, MatSelectModule, MatStepperModule, MatButtonModule, MatIconModule, MatRippleModule, MatSidenavModule, MatSlideToggleModule, MatTableModule } from '@angular/material';

import { FuseSharedModule } from '@fuse/shared.module';

import { AddRoleComponent } from './add-role.component';
import { AddRoleService } from './add-role.service';
import { FormAddRoleComponent } from './form/form-add-role.component';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {Nl2BrPipeModule} from 'nl2br-pipe';


const routes: Routes = [
    {
        path     : '**',
        component: AddRoleComponent,
        children : [],
        resolve  : {
            activities: AddRoleService
        }
    }
];

@NgModule({
    declarations: [
        AddRoleComponent,
        FormAddRoleComponent
    ],
    imports     : [
        RouterModule.forChild(routes),

        CdkTableModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        MatStepperModule,
        MatButtonModule,
        MatIconModule,
        MatRippleModule,
        MatSlideToggleModule,
        MatSidenavModule,
        MatTableModule,
        MatCheckboxModule,
        MatExpansionModule,
        MatDatepickerModule,
        Nl2BrPipeModule,

        FuseSharedModule
    ],
    providers   : [
        AddRoleService
    ],

})

export class AddRoleModule
{
}
