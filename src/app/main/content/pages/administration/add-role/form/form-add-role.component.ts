import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators, EmailValidator } from '@angular/forms';
import { AddRoleService } from '../add-role.service';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material';

@Component({
    selector: 'fuse-file-list',
    templateUrl: './form-add-role.component.html',
    styleUrls: ['./form-add-role.component.scss'],
})
export class FormAddRoleComponent implements OnInit {
    form: FormGroup;
    formErrors: any;
    test: any;
    step = 0;

    name: string;
    status: boolean;

    displayedColumns = ['enable', 'name', 'description'];

    roleStatus: boolean = false;
    roleName: string;
    roleDescription: string;
    merchantActivitiesStatus: false;
    couponActivitiesStatus: false;
    administrationActivities: any[] = [];
    merchantActivities: any[] = [];
    couponActivities: any[] = [];
    placementActivities: any[] = [];
    affiliateNetworkActivities: any[] = [];
    seasonalEventActivities: any[] = [];
    commissionCashbackActivities: any[] = [];

    role = {
        title: '',
        status: false,
        description: '',
        activities: []
    };
    activities: any[];


    constructor(private AddRoleService: AddRoleService, private formBuilder: FormBuilder, private router: Router, private snackBar: MatSnackBar) {


        this.activities = this.AddRoleService.activities;
        this.formErrors = {
            roleName: {}
        };
    }

    ngOnInit() {
        debugger;
        this.form = this.formBuilder.group({

            roleName: ['', Validators.required]
        });

        this.form.valueChanges.subscribe(() => {
            this.onFormValuesChanged();
        });


        for (let i = 0; i < this.activities.length; i++) {

            if (this.activities[i].module == 'administration') {
                debugger;
                this.administrationActivities.push(this.activities[i]);
            }
            else if (this.activities[i].module == 'merchant') {
                debugger;
                this.merchantActivities.push(this.activities[i]);
            }
            else if (this.activities[i].module == 'coupon') {
                this.couponActivities.push(this.activities[i]);
            }
            else if (this.activities[i].module == 'placement') {
                this.placementActivities.push(this.activities[i]);
            }

            else if (this.activities[i].module == 'seasonal-event') {
                this.seasonalEventActivities.push(this.activities[i]);
            }

            else if (this.activities[i].module == 'affiliate-network') {
                this.affiliateNetworkActivities.push(this.activities[i]);
            }

            else if (this.activities[i].module == 'commission-cashback') {
                this.commissionCashbackActivities.push(this.activities[i]);
            }
        }
        // console.log(this.merchantActivities);
        // console.log(this.couponActivities);
    }

    onFormValuesChanged() {
        for (const field in this.formErrors) {
            if (!this.formErrors.hasOwnProperty(field)) {
                continue;
            }

            // Clear previous errors
            this.formErrors[field] = {};

            // Get the control
            const control = this.form.get(field);

            if (control && control.dirty && !control.valid) {
                this.formErrors[field] = control.errors;
            }
        }
    }

    setStep(index: number) {
        this.step = index;
    }

    nextStep() {
        this.step++;
    }

    prevStep() {
        this.step--;
    }

    onSubmit() {
        // debugger;
        this.role.title = this.roleName;
        this.role.status = this.roleStatus;
        this.role.description = "This is a description";

        for (let i = 0; i < this.activities.length; i++) {

            if (this.activities[i].module == 'administration') {

                for (let j = 0; j < this.administrationActivities.length; j++) {

                    if (this.activities[i].title == this.administrationActivities[j].title) {

                        this.activities[i].isEnabled = this.administrationActivities[j].isEnabled;
                    }
                }

            }
            else if (this.activities[i].module == 'merchant') {

                for (let j = 0; j < this.merchantActivities.length; j++) {

                    if (this.activities[i].title == this.merchantActivities[j].title) {

                        this.activities[i].isEnabled = this.merchantActivities[j].isEnabled;
                    }
                }

            }
            else if (this.activities[i].module == 'coupon') {

                for (let j = 0; j < this.couponActivities.length; j++) {

                    if (this.activities[i].title == this.couponActivities[j].title) {

                        this.activities[i].isEnabled = this.couponActivities[j].isEnabled;
                    }
                }

            }

            else if (this.activities[i].module == 'placement') {

                for (let j = 0; j < this.placementActivities.length; j++) {

                    if (this.activities[i].title == this.placementActivities[j].title) {

                        this.activities[i] = this.placementActivities[j];
                    }
                }

            }

            else if (this.activities[i].module == 'affiliate-network') {

                for (let j = 0; j < this.affiliateNetworkActivities.length; j++) {

                    if (this.activities[i].title == this.affiliateNetworkActivities[j].title) {

                        this.activities[i] = this.affiliateNetworkActivities[j];
                    }
                }

            }

            else if (this.activities[i].module == 'seasonal-event') {

                for (let j = 0; j < this.seasonalEventActivities.length; j++) {

                    if (this.activities[i].title == this.seasonalEventActivities[j].title) {

                        this.activities[i] = this.seasonalEventActivities[j];
                    }
                }

            }

            else if (this.activities[i].module == 'commission-cashback') {

                for (let j = 0; j < this.commissionCashbackActivities.length; j++) {

                    if (this.activities[i].title == this.commissionCashbackActivities[j].title) {

                        this.activities[i] = this.commissionCashbackActivities[j];
                    }
                }

            }

        }
        this.role.activities = this.activities;
        // if (this.form.valid) {
        this.AddRoleService.addRole(this.role).then(response => {
            debugger;
            this.test = response;
            console.log("response: " + JSON.stringify(response));
            this.snackBar.open(response.meta.message, "Done", {
                duration: 2000,
            });
            if (response.meta.status == "success")
                this.redirect('pages/administration/list-role');
        });
        // }


    }

    redirect(pagename: string) {
        this.router.navigate([pagename]);
    }

}

