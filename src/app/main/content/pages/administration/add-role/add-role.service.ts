import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';

import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { AppService } from './../../../../../app.service';

@Injectable()
export class AddRoleService implements Resolve<any>
{
    activities:any[];
    
    constructor(private http: HttpClient, private appService: AppService)
    {
    }

    /**
     * The File Manager App Main Resolver
     * 
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any
    {

        // return new Promise((resolve, reject) => {
        //     debugger;
        //     Promise.all([
        //      this.getActivities()
        //     ]).then(
        //         ([activities]) => {
        //             resolve();
        //         },
        //         reject);
        // });
    }

    getActivities(): Promise<any>
    {

        return new Promise((resolve, reject) => {
            debugger;
            this.http.get(this.appService.userService + 'activities/')
                .subscribe((response: any) => {
                    console.log(response);
                    debugger;
                    if(response.meta.code=="200")
                    {
                        this.activities=response.data;
                    }
                    resolve(response);
                }, reject);
        });
    }

    addRole(role): Promise<any>
    {
        return new Promise((resolve, reject) => {
            debugger;
            this.http.post(this.appService.userService + 'roles/',role)
                .subscribe((response: any) => {
                    resolve(response);
                }, reject);
        });
    }

}
