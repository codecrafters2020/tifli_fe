import { Component, OnInit, ViewEncapsulation } from '@angular/core';

import { fuseAnimations } from '@fuse/animations';

import { ResetPasswordService } from './reset-password.service';

import { Router } from '@angular/router';

@Component({
    selector     : 'fuse-file-manager',
    templateUrl  : './reset-password.component.html',
    styleUrls    : ['./reset-password.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class ResetPasswordComponent implements OnInit
{
    selected: any;
    pathArr: string[];
    users:any[];

    constructor(private ResetPasswordService: ResetPasswordService, private router: Router)
    {
    }

    ngOnInit()
    {
        // this.users=this.EditUserService.users;
        // alert(JSON.stringify(this.users));
        // this.EditUserService.onUserSelected.subscribe(selected => {
        //     this.selected = selected;
        //     //this.pathArr = selected.location.split('>');
        // });

    }

    openUserList(){
        
        this.router.navigateByUrl("pages/administration/list-user/");
    }

}
