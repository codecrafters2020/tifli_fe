import { Component, OnInit } from '@angular/core';
import {AbstractControl, FormBuilder, FormGroup, Validators,EmailValidator } from '@angular/forms';
import { DataSource } from '@angular/cdk/collections';
import { Observable } from 'rxjs/Observable';
import { ResetPasswordService } from '../reset-password.service';
import {MatSnackBar} from '@angular/material';
import { Router } from '@angular/router';

@Component({
    selector   : 'fuse-file-list',
    templateUrl: './form-reset-password.component.html',
    styleUrls  : ['./form-reset-password.component.scss'],
})
export class FormResetPasswordComponent implements OnInit
{
    
    selected: any;
    
    visible: boolean = true;
    selectable: boolean = true;
    removable: boolean = true;
    addOnBlur: boolean = true;
    
    //user:any;
    data:any;
    form: FormGroup;
    formErrors: any;
    test: any;
    fruits: any[] = [];
    userRoleRequiredError: any;
    roleOptions: any[] = [];
    roles: any[]=[];

    passwordData={
        password :'',
        passwordConfirm : ''
    }; 


    constructor(private formBuilder: FormBuilder, private ResetPasswordService: ResetPasswordService, private router: Router, private snackBar: MatSnackBar)
    {
        debugger;

        // this.roles = this.ProfileUserService.roles;
        // for(let i=0;i<this.roles.length;i++){
        //     if(this.roles[i].status==true)
        //     {
        //         this.roleOptions.push(this.roles[i]);
        //     }
        // }
        // this.user=this.ProfileUserService.user;

        // if(this.user.status == "Active" || this.user.status == "active" || this.user.status == "true")
        //     this.user.status = true;

        // if(this.user.status == "Inctive" || this.user.status == "inactive" || this.user.status == "false")
        //     this.user.status = false;

        // this.user.dateCreated=new Date(this.user.dateCreated);
        // this.user.dateModified=new Date(this.user.dateModified);

        // if(this.user.roles.length > 0){
        //     var i = 0;
        //     for(var role of this.user.roles){
        //         this.fruits.push(role);
        //         var j = 0;
        //         for(var roleOption of this.roleOptions){
        //             if(roleOption.id == role.id)
        //                 this.roleOptions.splice(j,1);
        //             j++;
        //         }
        //         i++;
        //     }
        // }
            // Reactive form errors
        this.formErrors = {
            password:{},
            passwordConfirm: {passwordsNotMatch:false}
        };
    }

    ngOnInit()
    {

        // Reactive Form
        this.form = this.formBuilder.group({
            password: ['', Validators.required],
            passwordConfirm: ['', [Validators.required,confirmPassword]],
        });

        this.form.valueChanges.subscribe(() => {
            this.onFormValuesChanged();
        });

        this.form.controls.password.valueChanges
            .subscribe(value => {
                debugger;
                let passwordConfirmValue = this.form.controls.passwordConfirm.value;
                const control = this.form.controls.passwordConfirm;
                if(value != passwordConfirmValue)
                    this.formErrors.passwordConfirm.passwordsNotMatch = true;
                else{
                    this.form.controls['passwordConfirm'].setValue(value);
                }
            });


    }

    onSelect(selected)
    {
        this.ResetPasswordService.onUserSelected.next(selected);
    }

    onFormValuesChanged()
    {
        debugger;
        for ( const field in this.formErrors )
        {
            if ( !this.formErrors.hasOwnProperty(field) )
            {
                continue;
            }

            // Clear previous errors
            this.formErrors[field] = {};

            // Get the control
            const control = this.form.get(field);

            if ( control && control.dirty && !control.valid )
            {
                this.formErrors[field] = control.errors;
            }
        } 
    }

    onSubmit() {
        debugger;
        if (this.passwordData.password != this.passwordData.passwordConfirm)
        {                 
           this.snackBar.open("Passwords do not match");
        }

        else if (this.form.valid) {
            //this.user.roles = this.fruits;
            this.ResetPasswordService.updatePassword(this.passwordData).then(response => {
                debugger;
                this.test = response;
                this.snackBar.open(response.meta.message,"Done", {
                    duration: 2000,
                  });
                //   if(response.meta.status == "success")
                //   this.redirect('pages/administration/list-user');
            });
        }
         
    }

    redirect(pagename: string) {
        this.router.navigate([pagename]);
    }
 
}

function confirmPassword(control: AbstractControl)
{
    debugger;
    if ( !control.parent || !control )
    {
        return;
    }

    const password = control.parent.get('password');
    const passwordConfirm = control.parent.get('passwordConfirm');

    if ( !password || !passwordConfirm )
    {
        return;
    }

    if ( passwordConfirm.value === '' )
    {
        return;
    }

    if ( password.value !== passwordConfirm.value )
    {
        return {
            passwordsNotMatch: true
        };
    }

}


