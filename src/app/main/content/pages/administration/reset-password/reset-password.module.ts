import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CdkTableModule } from '@angular/cdk/table';

import { MatFormFieldModule, MatInputModule, MatSelectModule, MatStepperModule, MatButtonModule, MatIconModule, MatRippleModule, MatSidenavModule, MatSlideToggleModule, MatTableModule } from '@angular/material';
import {MatChipsModule} from '@angular/material/chips';

import { FuseSharedModule } from '@fuse/shared.module';

import { ResetPasswordComponent } from './reset-password.component';
import { ResetPasswordService } from './reset-password.service';
import { FormResetPasswordComponent } from './form/form-reset-password.component';
import {MatDatepickerModule} from '@angular/material/datepicker';

const routes: Routes = [
    {
        path     : '**',
        component: ResetPasswordComponent,
        children : [],
        resolve  : {
            users: ResetPasswordService
        }
    }
];

@NgModule({
    declarations: [
        ResetPasswordComponent,
        FormResetPasswordComponent
    ],
    imports     : [
        RouterModule.forChild(routes),

        CdkTableModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        MatStepperModule,
        MatButtonModule,
        MatIconModule,
        MatRippleModule,
        MatSlideToggleModule,
        MatSidenavModule,
        MatTableModule,
        MatDatepickerModule,
        MatChipsModule,

        FuseSharedModule
    ],
    providers   : [
        ResetPasswordService
    ]
})
export class ResetPasswordModule
{
}
