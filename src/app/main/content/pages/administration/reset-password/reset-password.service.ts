import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';

import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { AppService } from './../../../../../app.service';
import { AuthService } from '../../../../../auth/auth.service';

@Injectable()
export class ResetPasswordService implements Resolve<any>
{
    // users: any[];
    // roles: any[];
    // user:any;
     //id:any;
    onUsersChanged: BehaviorSubject<any> = new BehaviorSubject({});
    onUserSelected: BehaviorSubject<any> = new BehaviorSubject({});
    constructor(private http: HttpClient, private appService: AppService,private authService:AuthService)
    {
    }

    /**
     * The File Manager App Main Resolver
     * 
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any
    {
        // this.id=route.params['id'];

        return new Promise((resolve, reject) => {
            debugger;
            Promise.all([

            ]).then(
                () => {
                    resolve();
                },
                reject);
        });
    }

    updatePassword(passwordData): Promise<any>
    {
        return new Promise((resolve, reject) => {
            debugger;
            var currUser = this.authService.getUser();
            this.http.patch(this.appService.userService + 'users/'+ currUser.id +'/password?oldPassword='+currUser.password + '&newPassword=' + passwordData.password,{})
                .subscribe((response: any) => {
                    resolve(response);
                }, reject);
        });
    }

}
