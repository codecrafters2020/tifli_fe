import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';

import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { AppService } from './../../../../../app.service';

@Injectable()
export class EditRoleService implements Resolve<any>
{
    // onRolesChanged: BehaviorSubject<any> = new BehaviorSubject({});
    // onRoleSelected: BehaviorSubject<any> = new BehaviorSubject({});
    role:any[];
    id:any;
    
    constructor(private http: HttpClient, private appService: AppService)
    {
    }

    /**
     * The File Manager App Main Resolver
     * 
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any
    {
        
        this.id=route.params['id'];

        // return new Promise((resolve, reject) => {
        //     debugger;
        //     Promise.all([
        //      this.getRole()
        //     ]).then(
        //         ([role]) => {
        //             resolve();
        //         },
        //         reject);
        // });
    }

    getRole(): Promise<any>
    {

        return new Promise((resolve, reject) => {
            debugger;
            //8091
            this.http.get(this.appService.userService + 'roles/'+this.id)
                .subscribe((response: any) => {
                    console.log(response);
                    debugger;
                    if(response.meta.code=="200")
                    {
                        this.role=response.data;
                    }
                    resolve(response);
                }, reject);
        });
    }

    updateRole(role): Promise<any>
    {
        return new Promise((resolve, reject) => {
            debugger;
            this.http.put(this.appService.userService + 'roles/'+this.id,role)
                .subscribe((response: any) => {
                    resolve(response);
                }, reject);
        });
    }

}
