import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CdkTableModule } from '@angular/cdk/table';

import { MatCheckboxModule, MatFormFieldModule, MatInputModule, MatSelectModule, MatStepperModule, MatButtonModule, MatIconModule, MatRippleModule, MatSidenavModule, MatSlideToggleModule, MatTableModule } from '@angular/material';

import { FuseSharedModule } from '@fuse/shared.module';

import { EditRoleComponent } from './edit-role.component';
import { EditRoleService } from './edit-role.service';
import { FormEditRoleComponent } from './form/form-edit-role.component';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {Nl2BrPipeModule} from 'nl2br-pipe';


const routes: Routes = [
    {
        path     : '**',
        component: EditRoleComponent,
        children : [],
        resolve  : {
            role: EditRoleService
        }
    }
];

@NgModule({
    declarations: [
        EditRoleComponent,
        FormEditRoleComponent
    ],
    imports     : [
        RouterModule.forChild(routes),

        CdkTableModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        MatStepperModule,
        MatButtonModule,
        MatIconModule,
        MatRippleModule,
        MatSlideToggleModule,
        MatSidenavModule,
        MatTableModule,
        MatCheckboxModule,
        MatExpansionModule,
        MatDatepickerModule,
        Nl2BrPipeModule,

        FuseSharedModule
    ],
    providers   : [
        EditRoleService
    ],

})

export class EditRoleModule
{
}
