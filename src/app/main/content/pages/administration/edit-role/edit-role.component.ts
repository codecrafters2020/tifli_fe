import { Component, OnInit, ViewEncapsulation } from '@angular/core';

import { fuseAnimations } from '@fuse/animations';

import { EditRoleService } from './edit-role.service';

import { Router } from '@angular/router';

@Component({
    selector     : 'fuse-file-manager',
    templateUrl  : './edit-role.component.html',
    styleUrls    : ['./edit-role.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class EditRoleComponent implements OnInit
{
    selected: any;
    pathArr: string[];
    activities:any[];

    constructor(private EditRoleService: EditRoleService, private router: Router)
    {
    }

    ngOnInit()
    {
        
    }

    openRoleList(){
        
        this.router.navigateByUrl("pages/administration/list-role/");
    }

}
