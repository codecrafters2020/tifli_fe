import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators, EmailValidator } from '@angular/forms';
import { EditRoleService } from '../edit-role.service';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material';

@Component({
    selector: 'fuse-file-list',
    templateUrl: './form-edit-role.component.html',
    styleUrls: ['./form-edit-role.component.scss'],
})
export class FormEditRoleComponent implements OnInit {
    form: FormGroup;
    formErrors: any;
    test: any;
    step = 0;

    name: string;
    status: boolean;

    displayedColumns = ['enable', 'name', 'description'];

    roleStatus: boolean = false;
    roleTitle: string;
    roleDescription: string;
    merchantActivitiesStatus: false;
    couponActivitiesStatus: false;

    userManagementActivities: any[] = [];
    merchantActivities: any[] = [];
    couponActivities: any[] = [];

    placementActivities: any[] = [];
    affiliateNetworkActivities: any[] = [];
    seasonalEventActivities: any[] = [];
    commissionCashbackActivities: any[] = [];

    // newRole = {
    //     title: '',
    //     status: false,
    //     description: '',
    //     activities:[]
    // };
    role: any;


    constructor(private EditRoleService: EditRoleService, private formBuilder: FormBuilder, private router: Router, private snackBar: MatSnackBar) {

        debugger;
        this.role = this.EditRoleService.role;
        console.log(JSON.stringify(this.role));
        this.formErrors = {
            roleTitle: {}
        };
    }

    ngOnInit() {

        this.form = this.formBuilder.group({

            roleTitle: ['', Validators.required]
        });

        this.form.valueChanges.subscribe(() => {
            this.onFormValuesChanged();
        });


        debugger;
        for (let i = 0; i < this.role.activities.length; i++) {

            if (this.role.activities[i].module == 'administration') {
                this.userManagementActivities.push(this.role.activities[i]);
            }
            else if (this.role.activities[i].module == 'merchant') {
                debugger;
                this.merchantActivities.push(this.role.activities[i]);
            }
            else if (this.role.activities[i].module == 'coupon') {
                this.couponActivities.push(this.role.activities[i]);
            }

            else if (this.role.activities[i].module == 'placement') {
                this.placementActivities.push(this.role.activities[i]);
            }

            else if (this.role.activities[i].module == 'seasonal-event') {
                this.seasonalEventActivities.push(this.role.activities[i]);
            }

            else if (this.role.activities[i].module == 'affiliate-network') {
                this.affiliateNetworkActivities.push(this.role.activities[i]);
            }

            else if (this.role.activities[i].module == 'commission-cashback') {
                this.commissionCashbackActivities.push(this.role.activities[i]);
            }

        }
        console.log(this.userManagementActivities);
        console.log(this.merchantActivities);
        console.log(this.couponActivities);
    }

    onFormValuesChanged() {
        for (const field in this.formErrors) {
            if (!this.formErrors.hasOwnProperty(field)) {
                continue;
            }

            // Clear previous errors
            this.formErrors[field] = {};

            // Get the control
            const control = this.form.get(field);

            if (control && control.dirty && !control.valid) {
                this.formErrors[field] = control.errors;
            }
        }
    }

    setStep(index: number) {
        this.step = index;
    }

    nextStep() {
        this.step++;
    }

    prevStep() {
        this.step--;
    }

    onSubmit() {
        // debugger;

        for (let i = 0; i < this.role.activities.length; i++) {
            if (this.role.activities[i].module == 'administration') {

                for (let j = 0; j < this.userManagementActivities.length; j++) {

                    if (this.role.activities[i].title == this.userManagementActivities[j].title) {

                        this.role.activities[i] = this.userManagementActivities[j];
                    }
                }

            }

            else if (this.role.activities[i].module == 'merchant') {

                for (let j = 0; j < this.merchantActivities.length; j++) {

                    if (this.role.activities[i].title == this.merchantActivities[j].title) {

                        this.role.activities[i] = this.merchantActivities[j];
                    }
                }

            }
            else if (this.role.activities[i].module == 'coupon') {

                for (let j = 0; j < this.couponActivities.length; j++) {

                    if (this.role.activities[i].title == this.couponActivities[j].title) {

                        this.role.activities[i] = this.couponActivities[j];
                    }
                }

            }

            else if (this.role.activities[i].module == 'placement') {

                for (let j = 0; j < this.placementActivities.length; j++) {

                    if (this.role.activities[i].title == this.placementActivities[j].title) {

                        this.role.activities[i] = this.placementActivities[j];
                    }
                }

            }

            else if (this.role.activities[i].module == 'affiliate-network') {

                for (let j = 0; j < this.affiliateNetworkActivities.length; j++) {

                    if (this.role.activities[i].title == this.affiliateNetworkActivities[j].title) {

                        this.role.activities[i] = this.affiliateNetworkActivities[j];
                    }
                }

            }

            else if (this.role.activities[i].module == 'seasonal-event') {

                for (let j = 0; j < this.seasonalEventActivities.length; j++) {

                    if (this.role.activities[i].title == this.seasonalEventActivities[j].title) {

                        this.role.activities[i] = this.seasonalEventActivities[j];
                    }
                }

            }

            else if (this.role.activities[i].module == 'commission-cashback') {

                for (let j = 0; j < this.commissionCashbackActivities.length; j++) {

                    if (this.role.activities[i].title == this.commissionCashbackActivities[j].title) {

                        this.role.activities[i] = this.commissionCashbackActivities[j];
                    }
                }

            }

        }

        // if (this.form.valid) {
        this.EditRoleService.updateRole(this.role).then(response => {
            debugger;
            this.test = response;
            console.log("response: " + JSON.stringify(response));
            this.snackBar.open(response.meta.message, "Done", {
                duration: 2000,
            });
            //   if(response.meta.status == "success")
            //   this.redirect('pages/administration/list-role');
        });
        // }


    }

    redirect(pagename: string) {
        this.router.navigate([pagename]);
    }

}

