import { Component, OnInit } from '@angular/core';
import {AbstractControl, FormBuilder, FormGroup, Validators,EmailValidator } from '@angular/forms';
import { DataSource } from '@angular/cdk/collections';
import { Observable } from 'rxjs/Observable';
import { EditUserService } from '../edit-user.service';
import {MatSnackBar} from '@angular/material';
import { Router } from '@angular/router';

@Component({
    selector   : 'fuse-file-list',
    templateUrl: './form-edit-user.component.html',
    styleUrls  : ['./form-edit-user.component.scss'],
})
export class FormEditUserComponent implements OnInit
{
    
    dataSource: FormEditUserDataSource | null;
    selected: any;
    
    visible: boolean = true;
    selectable: boolean = true;
    removable: boolean = true;
    addOnBlur: boolean = true;
    
    user:any;
    data:any;
    form: FormGroup;
    formErrors: any;
    test: any;
    fruits: any[] = [];
    userRoleRequiredError: any;
    roleOptions: any[] = [];
    roles: any[]=[];

    constructor(private formBuilder: FormBuilder, private EditUserService: EditUserService, private router: Router, private snackBar: MatSnackBar)
    {
        debugger;

        this.roles = this.EditUserService.roles;
        for(let i=0;i<this.roles.length;i++){
            if(this.roles[i].status==true)
            {
                this.roleOptions.push(this.roles[i]);
            }
        }
        this.user=this.EditUserService.user;

        if(this.user.status == "Active" || this.user.status == "active" || this.user.status == "true")
            this.user.status = true;

        if(this.user.status == "Inctive" || this.user.status == "inactive" || this.user.status == "false")
            this.user.status = false;

        this.user.dateCreated=new Date(this.user.dateCreated);
        this.user.dateModified=new Date(this.user.dateModified);

        if(this.user.roles.length > 0){
            var i = 0;
            for(var role of this.user.roles){
                this.fruits.push(role);
                var j = 0;
                for(var roleOption of this.roleOptions){
                    if(roleOption.id == role.id)
                        this.roleOptions.splice(j,1);
                    j++;
                }
                i++;
            }
        }
            // Reactive form errors
        this.formErrors = {
            firstName : {},
            lastName  : {},
            emailAddress : {},
            phone  : {},
            userName  : {},
            userType  : {},
            userRole   : {},
            roleRequired:{},
            dateCreated :{},
            dateModified :{}
        };
    }

    ngOnInit()
    {
        this.dataSource = new FormEditUserDataSource(this.EditUserService);

        // Reactive Form
        this.form = this.formBuilder.group({
            firstName : ['', Validators.required],
            lastName  : ['', Validators.required],
            emailAddress : ['', [Validators.required, Validators.email]],
            phone : [''],
            userName : ['', Validators.required],
            userType : ['', Validators.required],
            userRole   : ['',[userRoleRequired]],
            userSelectedRoles:[''],
            dateCreated :['', Validators.required],
            dateModified :['', Validators.required],
            status  : ['']
        });

        this.form.valueChanges.subscribe(() => {
            this.onFormValuesChanged();
        });

    }

    onSelect(selected)
    {
        this.EditUserService.onUserSelected.next(selected);
    }

    onFormValuesChanged()
    {
        debugger;
        for ( const field in this.formErrors )
        {
            if ( !this.formErrors.hasOwnProperty(field) )
            {
                continue;
            }

            // Clear previous errors
            this.formErrors[field] = {};

            // Get the control
            const control = this.form.get(field);

            if ( control && control.dirty && !control.valid )
            {
                this.formErrors[field] = control.errors;
            }
        } 
    }

    onRoleChange(selectedVAlue){
        debugger;
        var i = 0;
        for(var role of this.roleOptions){
            if(selectedVAlue === role.id){
                this.fruits.push(role);
                this.roleOptions.splice(i,1);
                this.userRoleRequiredError = false;
            }
            i++;
        }
    }

    onSubmit() {
        debugger;
        if (this.form.valid) {
            this.user.roles = this.fruits;
            this.EditUserService.updateUser(this.user).then(response => {
                debugger;
                this.test = response;
                this.snackBar.open(response.meta.message,"Done", {
                    duration: 2000,
                  });
                //   if(response.meta.status == "success")
                //   this.redirect('pages/administration/list-user');
            });
        }
         
    }

    redirect(pagename: string) {
        this.router.navigate([pagename]);
    }

    remove(fruit: any): void {
        debugger;
        let index = this.fruits.indexOf(fruit);
    
        if (index >= 0) {
          this.fruits.splice(index, 1);
        }
        this.roleOptions.push(fruit);
        this.form.controls['userRole'].setValue('');
    }
 
}

function userRoleRequired(control: AbstractControl){
    debugger;

    if ( !control.parent || !control )
    {
        return;
    }
    const fruits = control.parent.get('userSelectedRoles');
    if(Array.isArray(fruits.value)){
        if(fruits.value.length == 0)
            return {
                userRoleRequiredError: true
            }
    }
    return;
}

export class FormEditUserDataSource extends DataSource<any>
{
    constructor(private EditUserService: EditUserService)
    {
        super();
    }

    /** Connect function called by the table to retrieve one stream containing the data to render. */
    connect(): Observable<any[]>
    {
        return this.EditUserService.onUsersChanged;
    }

    disconnect()
    {
    }
}

