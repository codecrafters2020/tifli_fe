import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CdkTableModule } from '@angular/cdk/table';

import { MatFormFieldModule, MatInputModule, MatSelectModule, MatStepperModule, MatButtonModule, MatIconModule, MatRippleModule, MatSidenavModule, MatSlideToggleModule, MatTableModule } from '@angular/material';
import {MatChipsModule} from '@angular/material/chips';

import { FuseSharedModule } from '@fuse/shared.module';

import { EditUserComponent } from './edit-user.component';
import { EditUserService } from './edit-user.service';
import { FormEditUserComponent } from './form/form-edit-user.component';
import {MatDatepickerModule} from '@angular/material/datepicker';

const routes: Routes = [
    {
        path     : '**',
        component: EditUserComponent,
        children : [],
        resolve  : {
            users: EditUserService
        }
    }
];

@NgModule({
    declarations: [
        EditUserComponent,
        FormEditUserComponent
    ],
    imports     : [
        RouterModule.forChild(routes),

        CdkTableModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        MatStepperModule,
        MatButtonModule,
        MatIconModule,
        MatRippleModule,
        MatSlideToggleModule,
        MatSidenavModule,
        MatTableModule,
        MatDatepickerModule,
        MatChipsModule,

        FuseSharedModule
    ],
    providers   : [
        EditUserService
    ]
})
export class EditUserModule
{
}
