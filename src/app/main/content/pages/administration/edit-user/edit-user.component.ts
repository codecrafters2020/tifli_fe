import { Component, OnInit, ViewEncapsulation } from '@angular/core';

import { fuseAnimations } from '@fuse/animations';

import { EditUserService } from './edit-user.service';

import { Router } from '@angular/router';

@Component({
    selector     : 'fuse-file-manager',
    templateUrl  : './edit-user.component.html',
    styleUrls    : ['./edit-user.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class EditUserComponent implements OnInit
{
    selected: any;
    pathArr: string[];
    users:any[];

    constructor(private EditUserService: EditUserService, private router: Router)
    {
    }

    ngOnInit()
    {
        // this.users=this.EditUserService.users;
        // alert(JSON.stringify(this.users));
        // this.EditUserService.onUserSelected.subscribe(selected => {
        //     this.selected = selected;
        //     //this.pathArr = selected.location.split('>');
        // });

    }

    openUserList(){
        
        this.router.navigateByUrl("pages/administration/list-user/");
    }

}
