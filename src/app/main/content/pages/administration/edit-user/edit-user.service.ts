import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';

import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { AppService } from './../../../../../app.service';

@Injectable()
export class EditUserService implements Resolve<any>
{
    users: any[];
    roles: any[];
    user:any;
    id:any;
    onUsersChanged: BehaviorSubject<any> = new BehaviorSubject({});
    onUserSelected: BehaviorSubject<any> = new BehaviorSubject({});
    constructor(private http: HttpClient, private appService: AppService)
    {
    }

    /**
     * The File Manager App Main Resolver
     * 
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any
    {
        this.id=route.params['id'];

        // return new Promise((resolve, reject) => {
        //     debugger;
        //     Promise.all([
        //      this.getUser(),
        //      this.getRoles()
        //     ]).then(
        //         ([users,roles]) => {
        //             resolve();
        //         },
        //         reject);
        // });
    }

    getUser(): Promise<any>
    {

        return new Promise((resolve, reject) => {
            debugger;
            this.http.get(this.appService.userService + 'users/'+this.id)
                .subscribe((response: any) => {
                    console.log(response);
                    debugger;
                    if(response.meta.code=="200")
                    {
                        this.user=response.data;
                    }
                    
                    //this.onUsersChanged.next(response);
                    //this.onUserSelected.next(response[0]);
                    resolve(response);
                }, reject);
        });
    }

    getRoles(): Promise<any>
    {

        return new Promise((resolve, reject) => {
            debugger;
            this.http.get(this.appService.userService + 'roles/')
                .subscribe((response: any) => {
                    console.log(response);
                    debugger;
                    if(response.meta.code=="200")
                    {
                        this.roles=response.data;
                    }
                    resolve(response);
                }, reject);
        });
    }

    updateUser(user): Promise<any>
    {
        return new Promise((resolve, reject) => {
            debugger;
            this.http.put(this.appService.userService + 'users/'+user.id,user)
                .subscribe((response: any) => {
                    resolve(response);
                }, reject);
        });
    }

}
