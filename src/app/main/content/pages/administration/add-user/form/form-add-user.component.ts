import { Component, OnInit,ViewChild } from '@angular/core';
import {AbstractControl, FormBuilder, FormGroup, Validators,EmailValidator } from '@angular/forms';
import { AddUserService } from '../add-user.service';
import { Router } from '@angular/router';
import {FormControl} from '@angular/forms';
import {Observable} from 'rxjs/Observable';
import {startWith} from 'rxjs/operators/startWith';
import {map} from 'rxjs/operators/map';
import {MatSnackBar} from '@angular/material';

import {
    MatAutocomplete,
    MatAutocompleteSelectedEvent,
    MatAutocompleteTrigger,
    VERSION,
  } from '@angular/material';
import { Subscription } from 'rxjs/Subscription';

@Component({
    selector   : 'fuse-file-list',
    templateUrl: './form-add-user.component.html',
    styleUrls  : ['./form-add-user.component.scss'],
})
export class FormAddUserComponent implements OnInit
{

     user={
        status : false,
        firstName :'',
        lastName :'',
        emailAddress   : '',
        phone  : '',
        userName : '',
        userType :'',
        password :'',
        passwordConfirm : '',
        roles : []
    }; 

    


    form: FormGroup;
    formErrors: any;
    test: any;

    visible: boolean = true;
    selectable: boolean = true;
    removable: boolean = true;
    addOnBlur: boolean = true;

    fruits: any[] = [];
    userRoleRequiredError: any;

    userRole: FormControl = new FormControl();
    roles:any[]=[];
    roleOptions: any[] = [];
    // roleOptions = [
    //     {name:'admin',id:1},
    //     {name:'merchant',id:2},
    //     {name:'hr representative',id:3}
    // ];
    filteredOptions: Observable<any[]>;
    
    constructor(private formBuilder: FormBuilder,private AddUserService: AddUserService,private router: Router, private snackBar: MatSnackBar)
    {
        // Reactive form errors
        this.formErrors = {
            firstName : {},
            lastName  : {},
            email   : {},
            phone  : {},
            userName  : {},
            userType  : {},
            password:{},
            passwordConfirm: {passwordsNotMatch:false},
            userRole   : {},
            roleRequired:{}
        };
    }
   
    ngOnInit()
    {
        this.roles = this.AddUserService.roles;
        for(let i=0;i<this.roles.length;i++){
            if(this.roles[i].status==true)
            {
                this.roleOptions.push(this.roles[i]);
            }
        }
        // Reactive Form
        this.form = this.formBuilder.group({
            firstName : ['', Validators.required],
            lastName  : ['', Validators.required],
            email   : ['', [Validators.required, Validators.email]],
            phone  : [''],
            userName      : ['', Validators.required],
            userType     : ['', Validators.required],
            password: ['', Validators.required],
            passwordConfirm: ['', [Validators.required,confirmPassword]],
            userRole   : ['',[userRoleRequired]],
            userSelectedRoles:[''],
            status : ['']
        });

        this.form.valueChanges.subscribe(() => {
            this.onFormValuesChanged();
        });

        this.form.controls.password.valueChanges
            .subscribe(value => {
                debugger;
                let passwordConfirmValue = this.form.controls.passwordConfirm.value;
                const control = this.form.controls.passwordConfirm;
                if(value != passwordConfirmValue)
                    this.formErrors.passwordConfirm.passwordsNotMatch = true;
                else{
                    this.form.controls['passwordConfirm'].setValue(value);
                }
            });

    }
    
    

    SearchData(input: any){
        debugger;
    }

    onFormValuesChanged()
    {
        for ( const field in this.formErrors )
        {
            if ( !this.formErrors.hasOwnProperty(field) )
            {
                continue;
            }

            // Clear previous errors
            this.formErrors[field] = {};

            // Get the control
            const control = this.form.get(field);

            if ( control && control.dirty && !control.valid )
            {
                this.formErrors[field] = control.errors;
            }
        }
    }

    onRoleChange(selectedVAlue){
        debugger;
        var i = 0;
        for(var role of this.roleOptions){
            if(selectedVAlue === role.id){
                this.fruits.push(role);
                this.roleOptions.splice(i,1);
                this.userRoleRequiredError = false;
            }
            i++;
        }
    }
    onSubmit() {
        debugger;
        if (this.form.valid) {
            this.user.roles = this.fruits;
            this.AddUserService.addUser(this.user).then(response => {
                debugger;
                this.snackBar.open(response.meta.message,"Done", {
                    duration: 2000,
                });
                if(response.meta.status == "success")
                    this.redirect('pages/administration/list-user');
            }); 
        }
        
    }

    redirect(pagename: string) {
        this.router.navigate([pagename]);
    }

    remove(fruit: any): void {
        debugger;
        let index = this.fruits.indexOf(fruit);
    
        if (index >= 0) {
          this.fruits.splice(index, 1);
        }
        this.roleOptions.push(fruit);
        this.form.controls['userRole'].setValue('');
    }
}

function userRoleRequired(control: AbstractControl){
    debugger;

    if ( !control.parent || !control )
    {
        return;
    }
    const fruits = control.parent.get('userSelectedRoles');
    if(Array.isArray(fruits.value)){
        if(fruits.value.length == 0)
            return {
                userRoleRequiredError: true
            };
    }
    return;
}


function confirmPassword(control: AbstractControl)
{
    debugger;
    if ( !control.parent || !control )
    {
        return;
    }

    const password = control.parent.get('password');
    const passwordConfirm = control.parent.get('passwordConfirm');

    if ( !password || !passwordConfirm )
    {
        return;
    }

    if ( passwordConfirm.value === '' )
    {
        return;
    }

    if ( password.value !== passwordConfirm.value )
    {
        return {
            passwordsNotMatch: true
        };
    }

}