import { Component, OnInit, ViewEncapsulation } from '@angular/core';

import { fuseAnimations } from '@fuse/animations';

import { AddUserService } from './add-user.service';

import { Router } from '@angular/router';

@Component({
    selector     : 'fuse-file-manager',
    templateUrl  : './add-user.component.html',
    styleUrls    : ['./add-user.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class AddUserComponent implements OnInit
{
    selected: any;
    pathArr: string[];

    constructor(private AddUserService: AddUserService, private router: Router)
    {
    }

    ngOnInit()
    {
        
    }

    openUserList(){
        
        this.router.navigateByUrl("pages/administration/list-user/");
    }
}
