import { Component, OnInit, ViewEncapsulation } from '@angular/core';

import { fuseAnimations } from '@fuse/animations';

import { ListRoleService } from './list-role.service';
import { Router } from '@angular/router';

@Component({
    selector     : 'fuse-file-manager',
    templateUrl  : './list-role.component.html',
    styleUrls    : ['./list-role.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class ListRoleComponent implements OnInit
{
    selected: any;
    pathArr: string[];

    constructor(private ListRoleService: ListRoleService, private router: Router)
    {
    }

    ngOnInit()
    {
        this.ListRoleService.onFileSelected.subscribe(selected => {
            this.selected = selected;
           // this.pathArr = selected.location.split('>');
        });
    }

    openUserAdd(){
        this.router.navigateByUrl("pages/administration/add-role/");
    }
}
