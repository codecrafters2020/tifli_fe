import { Component, OnInit } from '@angular/core';

import { fuseAnimations } from '@fuse/animations/index';

import { ListRoleService } from '../../list-role.service';

@Component({
    selector   : 'fuse-file-manager-details-sidenav',
    templateUrl: './sidenav-role-details.component.html',
    styleUrls  : ['./sidenav-role-details.component.scss'],
    animations : fuseAnimations
})
export class SidenavRoleDetailsComponent implements OnInit
{

    selected: any;

    constructor(private ListRoleService: ListRoleService)
    {

    }

    ngOnInit()
    {
        this.ListRoleService.onFileSelected.subscribe(selected => {
            this.selected = selected;
        });
    }

}
