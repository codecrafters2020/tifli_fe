import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CdkTableModule } from '@angular/cdk/table';

import { MatFormFieldModule, MatInputModule, MatSelectModule, MatStepperModule, MatButtonModule, MatIconModule, MatRippleModule, MatSidenavModule, MatSlideToggleModule, MatTableModule, MatMenuModule, MatPaginatorModule, MatSortModule } from '@angular/material';

import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatRadioModule} from '@angular/material/radio';
import { FuseSharedModule } from '@fuse/shared.module';

import { ListRoleComponent } from './list-role.component';
import { ListRoleService } from './list-role.service';
import { GridRoleComponent } from './grid/grid-role.component';
import { SidenavRoleMainComponent } from './sidenavs/main/sidenav-role-main.component';
import { SidenavRoleDetailsComponent } from './sidenavs/details/sidenav-role-details.component';

const routes: Routes = [
    {
        path     : '**',
        component: ListRoleComponent,
        children : [],
        resolve  : {
            files: ListRoleService
        }
    }
];

@NgModule({
    declarations: [
        ListRoleComponent,
        GridRoleComponent,
        SidenavRoleMainComponent,
        SidenavRoleDetailsComponent
    ],
    imports     : [
        RouterModule.forChild(routes),

        CdkTableModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        MatStepperModule,
        MatButtonModule,
        MatIconModule,
        MatRippleModule,
        MatSlideToggleModule,
        MatSidenavModule,
        MatTableModule,
        MatMenuModule,
        MatPaginatorModule, 
        MatSortModule,

        FuseSharedModule,
        MatDatepickerModule,
        MatCheckboxModule,
        MatRadioModule
    ],
    providers   : [
        ListRoleService
    ]
})
export class ListRoleModule
{
}
