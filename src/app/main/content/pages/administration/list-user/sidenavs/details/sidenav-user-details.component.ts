import { Component, OnInit } from '@angular/core';

import { fuseAnimations } from '@fuse/animations/index';

import { ListUserService } from '../../list-user.service';

@Component({
    selector   : 'fuse-file-manager-details-sidenav',
    templateUrl: './sidenav-user-details.component.html',
    styleUrls  : ['./sidenav-user-details.component.scss'],
    animations : fuseAnimations
})
export class SidenavUserDetailsComponent implements OnInit
{

    selected: any;

    constructor(private ListUserService: ListUserService)
    {

    }

    ngOnInit()
    {
        this.ListUserService.onFileSelected.subscribe(selected => {
            this.selected = selected;
        });
    }

}
