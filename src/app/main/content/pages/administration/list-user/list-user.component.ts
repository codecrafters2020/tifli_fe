import { Component, OnInit, ViewEncapsulation } from '@angular/core';

import { fuseAnimations } from '@fuse/animations';

import { ListUserService } from './list-user.service';
import { Router } from '@angular/router';

@Component({
    selector     : 'fuse-file-manager',
    templateUrl  : './list-user.component.html',
    styleUrls    : ['./list-user.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class ListUserComponent implements OnInit
{
    selected: any;
    pathArr: string[];

    constructor(private ListUserService: ListUserService, private router: Router)
    {
    }

    ngOnInit()
    {
        this.ListUserService.onFileSelected.subscribe(selected => {
            this.selected = selected;
           // this.pathArr = selected.location.split('>');
        });
    }

    openUserAdd(){
        this.router.navigateByUrl("pages/administration/add-user/");
    }
}
