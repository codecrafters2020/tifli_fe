import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CdkTableModule } from '@angular/cdk/table';

import { MatAutocompleteModule, MatFormFieldModule, MatInputModule, MatSelectModule, MatStepperModule, MatButtonModule, MatIconModule, MatRippleModule, MatSidenavModule, MatSlideToggleModule, MatTableModule, MatMenuModule, MatPaginatorModule, MatSortModule } from '@angular/material';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';

import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatRadioModule} from '@angular/material/radio';
import { FuseSharedModule } from '@fuse/shared.module';

import { ListUserActivityComponent } from './list-user-activity.component';
import { ListUserActivityService } from './list-user-activity.service';
import { GridUserActivityComponent } from './grid/grid-user-activity.component';
import { SidenavUserActivityMainComponent } from './sidenavs/main/sidenav-user-activity-main.component';
import { SidenavUserActivityDetailsComponent } from './sidenavs/details/sidenav-user-activity-details.component';

const routes: Routes = [
    {
        path     : '**',
        component: ListUserActivityComponent,
        children : [],
        resolve  : {
            files: ListUserActivityService
        }
    }
];

@NgModule({
    declarations: [
        ListUserActivityComponent,
        GridUserActivityComponent,
        SidenavUserActivityMainComponent,
        SidenavUserActivityDetailsComponent
    ],
    imports     : [
        RouterModule.forChild(routes),

        CdkTableModule,
        MatAutocompleteModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        MatStepperModule,
        MatButtonModule,
        MatIconModule,
        MatRippleModule,
        MatSlideToggleModule,
        MatSidenavModule,
        MatTableModule,
        MatMenuModule,
        MatPaginatorModule, 
        MatSortModule,
        MatProgressSpinnerModule,

        FuseSharedModule,
        MatDatepickerModule,
        MatCheckboxModule,
        MatRadioModule
    ],
    providers   : [
        ListUserActivityService
    ]
})
export class ListUserActivityModule
{
}
