import { Component, OnInit, ViewEncapsulation } from '@angular/core';

import { fuseAnimations } from '@fuse/animations';

import { ListUserActivityService } from './list-user-activity.service';
import { Router } from '@angular/router';

@Component({
    selector     : 'fuse-file-manager',
    templateUrl  : './list-user-activity.component.html',
    styleUrls    : ['./list-user-activity.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class ListUserActivityComponent implements OnInit
{
    selected: any;
    pathArr: string[];

    constructor(private ListUserActivityService: ListUserActivityService, private router: Router)
    {
        this.ListUserActivityService.getUserActivities();
    }

    ngOnInit()
    {
        this.ListUserActivityService.onFileSelected.subscribe(selected => {
            this.selected = selected;
           // this.pathArr = selected.location.split('>');
        });
    }

    openCouponAdd()
    {
        this.router.navigateByUrl("pages/coupon/add");
    }

    resetGrid()
    {
        this.ListUserActivityService.getUserActivities();
    }
}
