import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort } from '@angular/material';
import { DataSource } from '@angular/cdk/collections';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';

import { ListUserActivityService } from '../list-user-activity.service';
import { fuseAnimations } from '@fuse/animations/index';
import { FuseUtils } from '@fuse/utils';

@Component({
    selector: 'fuse-file-list',
    templateUrl: './grid-user-activity.component.html',
    styleUrls: ['./grid-user-activity.component.scss'],
    animations: fuseAnimations
})
export class GridUserActivityComponent implements OnInit {
    files: any;
    dataSource: GridUserDataSource | null;
    displayedColumns = ['icon'/*, 'id'*/, 'system', 'module', 'activity', 'timeStamp', 'createdBy', 'userId', 'title', 'entityId', 'entity'];
    selected: any;
    // pagination
    pageSize: number;
    pageSizeOptions: number[];

    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild('filter') filter: ElementRef;
    @ViewChild(MatSort) sort: MatSort;

    constructor(private ListUserActivityService: ListUserActivityService, private router: Router) {
        this.pageSize = 25;
        this.pageSizeOptions = [this.pageSize, this.pageSize * 2, this.pageSize * 4];
    }

    ngOnInit() {

        this.dataSource = new GridUserDataSource(this.ListUserActivityService, this.paginator, this.sort);

    }

    onSelect(selected) {
        this.ListUserActivityService.onFileSelected.next(selected);
    }

    openUserDashboard(event, userid) {
        this.router.navigateByUrl("apps/dashboards/project/" + userid);
    }

    openUserEdit(event, id) {
        this.router.navigateByUrl("pages/coupon/edit/" + id);
    }
}

export class GridUserDataSource extends DataSource<any>
{
    _filterChange = new BehaviorSubject('');
    _filteredDataChange = new BehaviorSubject('');

    get filteredData(): any {
        return this._filteredDataChange.value;
    }

    set filteredData(value: any) {
        this._filteredDataChange.next(value);
    }

    get filter(): string {
        return this._filterChange.value;
    }

    set filter(filter: string) {
        this._filterChange.next(filter);
    }
    constructor(private ListUserActivityService: ListUserActivityService, private _paginator: MatPaginator, private _sort: MatSort) {
        super();
        this.filteredData = this.ListUserActivityService.userActivities;
    }

    /** Connect function called by the table to retrieve one stream containing the data to render. */
    // connect(): Observable<any[]>
    // {
    //     return this.ListCouponService.onFilesChanged;
    // }
    connect(): Observable<any[]> {
        const displayDataChanges = [
            this.ListUserActivityService.onFilesChanged,
            this._paginator.page,
            this._filterChange,
            // this._sort.sortChange
        ];

        return Observable.merge(...displayDataChanges).map(() => {
            let data = this.ListUserActivityService.userActivities.slice();

            data = this.filterData(data);

            this.filteredData = [...data];

            //data = this.sortData(data);

            // Grab the page's slice of data.
            const startIndex = this._paginator.pageIndex * this._paginator.pageSize;
            return data.splice(startIndex, this._paginator.pageSize);
        });
    }

    filterData(data) {
        if (!this.filter) {
            return data;
        }
        return FuseUtils.filterArrayByString(data, this.filter);
    }

    disconnect() {
    }
}
