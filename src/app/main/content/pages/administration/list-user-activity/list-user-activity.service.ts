import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';

import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of'
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { AppService } from './../../../../../app.service';

import * as moment from 'moment';
import * as _ from 'lodash';

@Injectable()
export class ListUserActivityService implements Resolve<any>
{
    coupons: any[];
    users: any[];
    userActivities: any[];
    activities: any[];
    onFilesChanged: BehaviorSubject<any> = new BehaviorSubject({});
    onFileSelected: BehaviorSubject<any> = new BehaviorSubject({});

    constructor(private http: HttpClient, private appService: AppService) {
    }

    /**
     * The File Manager App Main Resolver
     * 
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {

        // return new Promise((resolve, reject) => {

        //     Promise.all([
        //         this.getUserActivities(),
        //         this.getActivities()
        //     ]).then(
        //         ([userActivities, activities]) => {
        //             resolve();
        //         },
        //         reject
        //     );
        // });
    }

    getUserActivities(): Promise<any> {
        return new Promise((resolve, reject) => {

            this.http.get(`${this.appService.logService}logs/currentDay`)
                .subscribe((response: any) => {

                    // response.data.content = [
                    //     {
                    //         id: '1',
                    //         system: 'CMS',
                    //         module: 'Administration',
                    //         activity: 'administration_list-user',
                    //         timeStamp: '16/7/2018',
                    //         createdBy: '1',
                    //         createdByName: 'Saira',
                    //         status: 'Activated',
                    //         title: 'List User',
                    //         entityId: '1',
                    //         entity: 'CMS_Users'

                    //     },
                    //     {
                    //         id: '2',
                    //         system: 'CMS',
                    //         module: 'Merchant',
                    //         activity: 'merchant_add',
                    //         timeStamp: '16/7/2018',
                    //         createdBy: '2',
                    //         createdByName: 'Usman',
                    //         status: 'Activated',
                    //         title: 'Add Merchant',
                    //         entityId: '2',
                    //         entity: 'CMS_Merchants'

                    //     },
                    //     {
                    //         id: '3',
                    //         system: 'Website',
                    //         module: 'Coupon',
                    //         activity: 'coupon_edit',
                    //         timeStamp: '16/7/2018',
                    //         createdBy: '3',
                    //         createdByName: 'Noorain',
                    //         status: 'Activated',
                    //         title: 'Edit Coupon',
                    //         entityId: '3',
                    //         entity: 'CMS_Coupons'

                    //     },
                    //     {
                    //         id: '4',
                    //         system: 'CMS',
                    //         module: 'Seasonal Event',
                    //         activity: 'seasonal-events_list',
                    //         timeStamp: '16/7/2018',
                    //         createdBy: '4',
                    //         createdByName: 'Moiz',
                    //         status: 'Activated',
                    //         title: 'List Seasonal Event',
                    //         entityId: '4',
                    //         entity: 'CMS_Seasonal Events'

                    //     },
                    //     {
                    //         id: '5',
                    //         system: 'CMS',
                    //         module: 'Affiliate Network',
                    //         activity: 'affiliate-network_add',
                    //         timeStamp: '16/7/2018',
                    //         createdBy: '5',
                    //         createdByName: 'Shamikh',
                    //         status: 'Activated',
                    //         title: 'List Affiliate Network',
                    //         entityId: '5',
                    //         entity: 'CMS_Affiliate_Network'
                    //     }
                    // ]

                    this.userActivities = response.data;
                    this.onFilesChanged.next(response.data);
                    this.onFileSelected.next(response[0]);
                    resolve(response);
                }, reject);



        });
    }

    getActivities(): Promise<any> {

        return new Promise((resolve, reject) => {
            debugger;
            this.http.get(this.appService.userService + 'activities/')
                .subscribe((response: any) => {
                    console.log(response);
                    debugger;
                    if (response.meta.code == "200") {
                        this.activities = response.data;
                    }
                    resolve(response);
                }, reject);
        });
    }

    getFilteredUserActivities(page: number | string,
        size: number | string,
        sort: string,
        userName: string,
        firstName: string,
        lastName: string,
        userActivity: string,
        startDate: string,
        endDate: string): Promise<any> {
        let params = {
            page,
            size,
            sort,
            userName,
            firstName,
            lastName,
            userActivity,
            startDate,
            endDate
        };

        let queryString = '';
        _.forEach(params, (val, key) => {
            if (val || typeof val === 'number') {
                queryString = queryString + key + '=' + val + '&';
            }
        });

        return new Promise((resolve, reject) => {
            // this.http.get('api/coupon-list')
            // this.http.get(`http://104.42.179.33:8765/cms/api/coupons?page=0&size=20&sort=couponCode,asc&searchTerms=gag&couponCode=123&couponType=Deal`)
            this.http.get(`${this.appService.couponService}coupons/coupons?${queryString}`)
                .subscribe((response: any) => {
                    debugger;
                    response.data.content = [
                        {
                            id: '1',
                            system: 'CMS',
                            module: 'Administration',
                            activity: 'administration_list-user',
                            timeStamp: '16/7/2018',
                            createdBy: '1',
                            createdByName: 'Saira',
                            status: 'Activated',
                            title: 'List User',
                            entityId: '1',
                            entity: 'CMS_Users'

                        },
                        {
                            id: '2',
                            system: 'CMS',
                            module: 'Merchant',
                            activity: 'merchant_add',
                            timeStamp: '16/7/2018',
                            createdBy: '2',
                            createdByName: 'Usman',
                            status: 'Activated',
                            title: 'Add Merchant',
                            entityId: '2',
                            entity: 'CMS_Merchants'

                        }
                    ]

                    this.userActivities = response.data.content;
                    this.onFilesChanged.next(response.data);
                    this.onFileSelected.next(response[0]);
                    resolve(response);
                }, reject);
        });
    }

    searchUserName(term: string): Observable<any> {
        return this.http.get(`${this.appService.couponService}seasonal/search/?name=${term}`)
            .map(response => response['data'])
            .map(data => data instanceof Array ? data : [])
    }

    searchFirstName(term: string): Observable<any> {
        return this.http.get(`${this.appService.merchantService}search/?name=${term}`)
            .map(response => response['data'])
            .map(data => data instanceof Array ? data : [])
    }

    searchLastName(term: string): Observable<any> {
        return this.http.get(`${this.appService.merchantService}search/?name=${term}`)
            .map(response => response['data'])
            .map(data => data instanceof Array ? data : [])
    }

}
