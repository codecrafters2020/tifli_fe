import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';
import { AbstractControl, FormBuilder, FormGroup, FormArray, Validators, EmailValidator } from '@angular/forms';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatRadioModule } from '@angular/material/radio';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { FormControl } from '@angular/forms';
import { Observable, BehaviorSubject } from 'rxjs';
import { of } from 'rxjs/observable/of';
import { map, startWith } from 'rxjs/operators';
import { FuseAngularMaterialModule } from '../../../../../components/angular-material/angular-material.module';

import { ListUserActivityService } from '../../list-user-activity.service';


@Component({
  selector: 'fuse-file-manager-main-sidenav',
  templateUrl: './sidenav-user-activity-main.component.html',
  styleUrls: ['./sidenav-user-activity-main.component.scss']
})

export class SidenavUserActivityMainComponent {
  @Input() leftSideNav: any;

  form: FormGroup;
  formErrors: any;

  userNameSearchResult: any[];
  firstNameSearchResult: any[];
  lastNameSearchResult: any[];
  userActivities: any[];
  btnDisabled: boolean = false;


  constructor(private ListUserActivityService: ListUserActivityService,
    private router: Router,
    private formBuilder: FormBuilder) {
    // Reactive Form

    this.userActivities = this.ListUserActivityService.activities;


    this.form = this.formBuilder.group({
      userName: ['',],
      firstName: ['',],
      lastName: ['',],
      userActivity: ['',],
      startDate: ['',],
      endDate: ['',]
    });

  }

  filter() {
    // this.btnDisabled = true;
    debugger;
    this.ListUserActivityService.getFilteredUserActivities('',
      '',
      'couponCode,asc',
      this.form.controls.userName.value,
      this.form.controls.firstName.value,
      this.form.controls.lastName.value,
      this.form.controls.userActivity.value,
      this.form.controls.startDate.value,
      this.form.controls.endDate.value)
      .then((response) => {
        this.btnDisabled = false;
        this.leftSideNav.toggle()
      })
  }

  ngOnInit() {
    this.form.valueChanges.subscribe(() => {
      // console.log(this.form.value);
    });

    this.form.controls.userName.valueChanges
      .debounceTime(400)
      .subscribe(searchTerm => {
        this.ListUserActivityService.searchUserName(searchTerm)
          .subscribe(response => {
            this.userNameSearchResult = response;
          });
      });

    this.form.controls.firstName.valueChanges
      .debounceTime(400)
      .subscribe(searchTerm => {
        this.ListUserActivityService.searchFirstName(searchTerm)
          .subscribe(response => {
            this.firstNameSearchResult = response;
          });
      });

    this.form.controls.lastName.valueChanges
      .debounceTime(400)
      .subscribe(searchTerm => {
        this.ListUserActivityService.searchLastName(searchTerm)
          .subscribe(response => {
            this.lastNameSearchResult = response;
          });
      });

  }

  displaySelectedName(item: any) {
    return item;
  }
}


