import { Component, OnInit } from '@angular/core';

import { fuseAnimations } from '@fuse/animations/index';

import { ListUserActivityService } from '../../list-user-activity.service';

@Component({
    selector   : 'fuse-file-manager-details-sidenav',
    templateUrl: './sidenav-user-activity-details.component.html',
    styleUrls  : ['./sidenav-user-activity-details.component.scss'],
    animations : fuseAnimations
})
export class SidenavUserActivityDetailsComponent implements OnInit
{

    selected: any;

    constructor(private ListUserActivityService: ListUserActivityService)
    {

    }

    ngOnInit()
    {
        this.ListUserActivityService.onFileSelected.subscribe(selected => {
            this.selected = selected;
        });
    }

}
