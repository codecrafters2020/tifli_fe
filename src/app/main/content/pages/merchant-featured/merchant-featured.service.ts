import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';

import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import {MatSnackBar} from '@angular/material';
import { AppService } from './../../../../app.service';
import { findIndex } from 'rxjs/operators';

@Injectable()
export class MerchantFeaturedService implements Resolve<any>
{
    boards: any[];
    routeParams: any;
    board: any;
    featuredMerchants:any[];
    merchants:any[];

    onBoardsChanged: BehaviorSubject<any> = new BehaviorSubject([]);
    onBoardChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    constructor(private http: HttpClient, private snackBar: MatSnackBar, private appService:AppService)
    {
    }

    /**
     * Resolve
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any
    {
        this.routeParams = route.params;

        // return new Promise((resolve, reject) => {
        //     Promise.all([
        //         this.getBoards(),
        //         this.getMerchants()
        //     ]).then(
        //         ([boards,merchants]) => {
        //             resolve();
        //         },
        //         reject
        //     );
        // });
    }

    getBoards(): Promise<any>
    {
        return new Promise((resolve, reject) => {
            this.http.get('api/scrumboard-boards')
                .subscribe((response: any) => {
                    debugger;
                    this.boards = response;
                    this.onBoardsChanged.next(this.boards);
                    resolve(this.boards);
                }, reject);
        });
    }

    getMerchants(): Promise<any>
    {
        debugger;
        return new Promise((resolve, reject) => {
            this.http.get(this.appService.merchantService)
                .subscribe((response: any) => {
                    debugger;
                    this.merchants = response.data;
                    resolve(this.merchants);
                }, reject);
        });
    }

    getBoard(boardId): Promise<any>
    {
        return new Promise((resolve, reject) => {
            this.http.get('api/scrumboard-boards/' + boardId)
                .subscribe((response: any) => {
                    debugger
                    this.board = response;
                    this.onBoardChanged.next(this.board);
                    this.getFeaturedMerchants();
                    resolve(this.board);
                }, reject);
        });
    }

    getFeaturedMerchants(): Promise<any>
    {
        debugger;
        return new Promise((resolve, reject) => {
            this.http.get(this.appService.merchantService + 'featured')
                .subscribe((response: any) => {
                    debugger;
                    this.featuredMerchants = response.data;
                    for(let i=0;i<this.featuredMerchants.length;i++){
                        var listIdCard = this.featuredMerchants[i].merchant.id;
                        this.board.lists[0].idCards.push(listIdCard);
                        var card: any = {};
                        card.id = this.featuredMerchants[i].merchant.id;
                        card.name = this.featuredMerchants[i].merchant.name;
                        card.affiliate =  this.featuredMerchants[i].merchant.affiliateNetwork.name;
                        card.description = this.featuredMerchants[i].merchant.description;
                        card.idMembers = [];
                        card.idMembers.push(this.featuredMerchants[i].merchant.accountManager.id);
                        var member: any = {};
                        member.id = this.featuredMerchants[i].merchant.accountManager.id;
                        member.name = "usman";
                        member.avatar = 'assets/images/avatars/alice.jpg'
                        this.board.cards.push(card);
                        this.board.members.push(member);
                    }
                    //this.board = response;
                    this.onBoardChanged.next(this.board);
                    resolve(this.board);
                }, reject);
        });
    }

    addCard(listId, newCard, member)
    {
        this.board.lists.map((list) => {
            if ( list.id === listId )
            {
                return list.idCards.push(newCard.id);
            }
        });

        this.board.members.push(member);
        this.board.cards.push(newCard);

        return this.updateBoard();
    }

    // addList(newList)
    // {
    //     debugger;

    //     this.board.lists.push(newList);

    //     return this.updateBoard();

    // }
    addList()
    {
        debugger;

        // this.board.lists.push(newList);

        return this.updateBoard();

    }

    removeList(listId)
    {
        const list = this.board.lists.find((_list) => {
            return _list.id === listId;
        });

        for ( const cardId of list.idCards )
        {
            this.removeCard(cardId);
        }

        const index = this.board.lists.indexOf(list);

        this.board.lists.splice(index, 1);

        return this.updateBoard();
    }

    removeCard(cardId, listId?)
    {

        const card = this.board.cards.find((_card) => {
            return _card.id === cardId;
        });

        if ( listId )
        {
            const list = this.board.lists.find((_list) => {
                return listId === _list.id;
            });
            list.idCards.splice(list.idCards.indexOf(cardId), 1);
        }

        this.board.cards.splice(this.board.cards.indexOf(card), 1);

        this.updateBoard();
    }

    updateBoard()
    {
        debugger;
        // var featuredMerchant: any = {
        //     position: '',
        //     merchant: {
        //         id: 0
        //     }
        // }
        var featuredMerchantsList: any = [];
        for(let i=0;i<this.board.lists[0].idCards.length;i++){
            
            var featuredMerchant: any = {
                position: '',
                merchant: {
                    id: 0
                }
            }
            
            featuredMerchant.position = i+1;
            featuredMerchant.merchant.id = this.board.lists[0].idCards[i];
            featuredMerchantsList.push(featuredMerchant);
         //   var pos = findIndex(this.merchants.splice(featuredMerchant,1);
            
        }

        return new Promise((resolve, reject) => {
            this.http.put(this.appService.merchantService + 'featured', featuredMerchantsList)
                .subscribe(response => {
                    var msg = "Response recieved";
                    if('meta' in response){
                        msg = response["meta"].message;
                    }
                    this.onBoardChanged.next(this.board);
                    this.snackBar.open(msg,"Done", {
                        duration: 2000,
                    });
                    resolve(this.board);
                }, reject);
        });
    }

    updateCard(newCard)
    {
        this.board.cards.map((_card) => {
            if ( _card.id === newCard.id )
            {
                return newCard;
            }
        });

        this.updateBoard();
    }

    createNewBoard(board)
    {
        return new Promise((resolve, reject) => {
            this.http.post('api/scrumboard-boards/' + board.id, board)
                .subscribe(response => {
                    resolve(board);
                }, reject);
        });
    }

    searchMerchant(term: string): Observable<any> {
        
        return this.http.get(`${this.appService.merchantService}search/?name=${term}`)
            .map(response => response['data'])
            .map(data => data instanceof Array ? data : [])
    }
}

@Injectable()
export class BoardResolve implements Resolve<any>
{
    constructor(private scrumboardService: MerchantFeaturedService)
    {
    }

    resolve(route: ActivatedRouteSnapshot)
    {
        // return this.scrumboardService.getBoard(route.paramMap.get('boardId'));
        this.scrumboardService.getMerchants();
        return this.scrumboardService.getBoard('positions');
    }
}
