import { Component, EventEmitter, Input, OnDestroy, OnInit, Output, ViewEncapsulation } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';

import { fuseAnimations } from '@fuse/animations';
import { FuseUtils } from '@fuse/utils';

import { MerchantFeaturedService } from '../../../../merchant-featured.service';

@Component({
    selector     : 'fuse-scrumboard-label-selector',
    templateUrl  : './label-selector.component.html',
    styleUrls    : ['./label-selector.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})

export class MerchantFeaturedLabelSelectorComponent implements OnInit, OnDestroy
{
    board: any;
    @Input('card') card: any;
    @Output() onCardLabelsChange = new EventEmitter();

    labelsMenuView = 'labels';
    selectedLabel: any;
    newLabel = {
        'id'   : '',
        'name' : '',
        'color': 'mat-blue-400-bg'
    };
    toggleInArray = FuseUtils.toggleInArray;

    onBoardChanged: Subscription;

    constructor(
        private merchantFeaturedService: MerchantFeaturedService
    )
    {
    }

    ngOnInit()
    {
        this.onBoardChanged =
            this.merchantFeaturedService.onBoardChanged
                .subscribe(board => {
                    this.board = board;
                });
    }

    ngOnDestroy()
    {
        this.onBoardChanged.unsubscribe();
    }

    cardLabelsChanged()
    {
        this.onCardLabelsChange.next();
    }

    onLabelChange()
    {
        this.merchantFeaturedService.updateBoard();
    }

    addNewLabel()
    {
        this.newLabel.id = FuseUtils.generateGUID();
        this.board.labels.push(Object.assign({}, this.newLabel));
        this.newLabel.name = '';
        this.labelsMenuView = 'labels';
    }
}
