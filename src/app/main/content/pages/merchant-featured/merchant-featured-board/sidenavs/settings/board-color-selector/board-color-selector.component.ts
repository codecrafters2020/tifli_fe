import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';

import { MatColors } from '@fuse/mat-colors';

import { MerchantFeaturedService } from '../../../../merchant-featured.service';

@Component({
    selector   : 'fuse-scrumboard-board-color-selector',
    templateUrl: './board-color-selector.component.html',
    styleUrls  : ['./board-color-selector.component.scss']
})
export class MerchantFeaturedBoardColorSelectorComponent implements OnInit, OnDestroy
{
    colors: any;
    board: any;
    onBoardChanged: Subscription;

    constructor(
        private merchantFeaturedService: MerchantFeaturedService
    )
    {
        this.colors = MatColors.all;
    }

    ngOnInit()
    {
        this.onBoardChanged =
            this.merchantFeaturedService.onBoardChanged
                .subscribe(board => {
                    this.board = board;
                });
    }

    ngOnDestroy()
    {
        this.onBoardChanged.unsubscribe();
    }

    setColor(color)
    {
        this.board.settings.color = color;
        this.merchantFeaturedService.updateBoard();
    }
}
