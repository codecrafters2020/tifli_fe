import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';

import { fuseAnimations } from '@fuse/animations';

import { MerchantFeaturedService } from '../../../merchant-featured.service';

@Component({
    selector   : 'fuse-scrumboard-board-settings',
    templateUrl: './settings.component.html',
    styleUrls  : ['./settings.component.scss'],
    animations : fuseAnimations
})
export class MerchantFeaturedBoardSettingsSidenavComponent implements OnInit, OnDestroy
{
    board: any;
    view = 'main';
    onBoardChanged: Subscription;

    constructor(
        private merchantFeaturedService: MerchantFeaturedService
    )
    {
    }

    ngOnInit()
    {
        this.onBoardChanged =
            this.merchantFeaturedService.onBoardChanged
                .subscribe(board => {
                    this.board = board;
                });
    }

    ngOnDestroy()
    {
        this.onBoardChanged.unsubscribe();
    }
    
    toggleCardCover()
    {
        this.board.settings.cardCoverImages = !this.board.settings.cardCoverImages;
        this.merchantFeaturedService.updateBoard();
    }

    toggleSubcription()
    {
        this.board.settings.subscribed = !this.board.settings.subscribed;
        this.merchantFeaturedService.updateBoard();
    }
}
