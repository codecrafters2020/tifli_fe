import { Component, OnDestroy, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { Router } from '@angular/router';
import { fuseAnimations } from '@fuse/animations';

import { List } from '../list.model';
import { MerchantFeaturedService } from '../merchant-featured.service';

@Component({
    selector   : 'fuse-scrumboard-board',
    templateUrl: './merchant-featured-board.component.html',
    styleUrls  : ['./merchant-featured-board.component.scss'],
    animations : fuseAnimations
})
export class MerchantFeaturedBoardComponent implements OnInit, OnDestroy
{
    board: any;
    onBoardChanged: Subscription;
    merchants:any;

    constructor(
        private route: ActivatedRoute,
        private location: Location,
        private merchantFeaturedService: MerchantFeaturedService,
        private router: Router
    )
    {
    }

    ngOnInit()
    {
        this.onBoardChanged =
            this.merchantFeaturedService.onBoardChanged
                .subscribe(board => {
                    this.board = board;
                });
        this.merchants = this.merchantFeaturedService.merchants;
    }

    ngOnDestroy()
    {
        this.onBoardChanged.unsubscribe();
    }

    // onListAdd(newListName)
    // {
    //     debugger;
    //     if ( newListName === '' )
    //     {
    //         return;
    //     }

    //     this.scrumboardService.addList(new List({name: newListName}));
    // }
    onListAdd()
    {
        debugger;
        this.merchantFeaturedService.addList();
    }

    onBoardNameChanged(newName)
    {
        this.merchantFeaturedService.updateBoard();
        this.location.go('/apps/scrumboard/boards/' + this.board.id + '/' + this.board.uri);
    }

    onDrop(ev)
    {
        this.merchantFeaturedService.updateBoard();
        // this.router.navigateByUrl('/RefreshComponent', { skipLocationChange: true }).then(() =>
        //     this.router.navigate(["pages/featured-merchants/"]));
    }
}
