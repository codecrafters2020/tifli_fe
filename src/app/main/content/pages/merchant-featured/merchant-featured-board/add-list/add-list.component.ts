import { Component, EventEmitter, Output, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
    selector: 'fuse-scrumboard-board-add-list',
    templateUrl: './add-list.component.html',
    styleUrls: ['./add-list.component.scss']
})
export class MerchantFeaturedBoardAddListComponent {
    formActive = false;
    form: FormGroup;
    @Output() onlistAdd = new EventEmitter();
    @ViewChild('nameInput') nameInputField;

    constructor(
        private formBuilder: FormBuilder,
        private router: Router
    ) {
    }

    openForm() {
        debugger;
        // this.form = this.formBuilder.group({
        //     name: ['']
        // });
        // this.formActive = true;
        // this.focusNameField();
        this.onFormSubmit();
        // location.reload();



    }

    closeForm() {
        this.formActive = false;
    }

    focusNameField() {
        setTimeout(() => {
            this.nameInputField.nativeElement.focus();
        });
    }

    onFormSubmit() {
        // if ( this.form.valid )
        // {
        //     this.onlistAdd.next(this.form.getRawValue().name);
        //     this.formActive = false;
        // }
        this.onlistAdd.next();
        
        //  this.router.navigateByUrl("pages/featured-merchants/", { skipLocationChange: true }).then(() =>
        //     this.router.navigate(["pages/featured-merchants/"]));
    }

}
