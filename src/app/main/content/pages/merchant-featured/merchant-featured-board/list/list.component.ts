import { Component, Input, OnDestroy, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MatDialog, MatDialogRef } from '@angular/material';
import { Subscription } from 'rxjs/Subscription';

import { FuseConfirmDialogComponent } from '@fuse/components/confirm-dialog/confirm-dialog.component';
import { FusePerfectScrollbarDirective } from '@fuse/directives/fuse-perfect-scrollbar/fuse-perfect-scrollbar.directive';

import { Card } from '../../card.model';
import { MerchantFeaturedService } from '../../merchant-featured.service';
import { MerchantFeaturedCardDialogComponent } from '../dialogs/card/card.component';

@Component({
    selector     : 'fuse-scrumboard-board-list',
    templateUrl  : './list.component.html',
    styleUrls    : ['./list.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class MerchantFeaturedBoardListComponent implements OnInit, OnDestroy
{
    board: any;
    dialogRef: any;

    @Input() list;
    @ViewChild(FusePerfectScrollbarDirective) listScroll: FusePerfectScrollbarDirective;

    onBoardChanged: Subscription;
    confirmDialogRef: MatDialogRef<FuseConfirmDialogComponent>;
    addButtonHidden: any = false;

    constructor(
        private route: ActivatedRoute,
        private merchantFeaturedService: MerchantFeaturedService,
        public dialog: MatDialog
    )
    {
    }

    ngOnInit()
    {
        // this.onBoardChanged =
        //     this.merchantFeaturedService.onBoardChanged
        //         .subscribe(board => {
        //             debugger;
        //             this.board = board;
        //         });
    }

    ngOnDestroy()
    {
        // this.onBoardChanged.unsubscribe();
    }

    onListNameChanged(newListName)
    {
        this.list.name = newListName;
    }

    onCardAdd(newCardData)
    {
        if ( newCardData === null || newCardData === undefined )
        {
            return;
        }

        var card: any = {};
        card.id = newCardData.id;
        card.name = newCardData.name;
        card.affiliate = newCardData.affiliateNetwork.name;
        card.description = newCardData.description;
        card.idMembers = [];
        card.idMembers.push(newCardData.accountManager.id);
        var member: any = {};
        member.id = newCardData.accountManager.id;
        member.name = "usman";
        member.avatar = 'assets/images/avatars/alice.jpg'
        
        this.merchantFeaturedService.addCard(this.list.id, new Card(card), member);

        setTimeout(() => {
            this.listScroll.scrollToBottom(0, 400);
        });

    }

    removeList(listId)
    {
        this.confirmDialogRef = this.dialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete the list and it\'s all cards?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if ( result )
            {
                this.merchantFeaturedService.removeList(listId);
            }
        });
    }

    openCardDialog(cardId)
    {
        this.dialogRef = this.dialog.open(MerchantFeaturedCardDialogComponent, {
            panelClass: 'scrumboard-card-dialog',
            data      : {
                cardId: cardId,
                listId: this.list.id
            }
        });
        this.dialogRef.afterClosed()
            .subscribe(response => {

            });
    }

    onDrop(ev)
    {
        this.merchantFeaturedService.updateBoard();
    }

}
