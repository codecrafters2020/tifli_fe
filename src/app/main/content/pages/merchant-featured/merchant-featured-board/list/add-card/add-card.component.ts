import { Component, EventEmitter, Output, ViewChild, Input } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { MerchantFeaturedService } from '../../../merchant-featured.service';
import { Router } from '@angular/router';

import * as _ from "lodash";

@Component({
    selector: 'fuse-scrumboard-board-add-card',
    templateUrl: './add-card.component.html',
    styleUrls: ['./add-card.component.scss']
})
export class MerchantFeaturedBoardAddCardComponent {
    formActive = false;
    
    merchantSearchResult: any[];
    @Output() onCardAdd = new EventEmitter();
    @ViewChild('name') nameInputField;
    @Input() merchants: any[];
    form: FormGroup;
    roleOptions: any = [
        {
            id: 1,
            name: "Usman"
        },
        {
            id: 2,
            name: "Bilal"
        },
        {
            id: 3,
            name: "Mubashir"
        },
        {
            id: 4,
            name: "Ahmad"
        },
        {
            id: 5,
            name: "Hassan"
        },
        {
            id: 6,
            name: "Mubeen"
        }
    ];


    constructor(
        private formBuilder: FormBuilder,
        private merchantFeaturedService: MerchantFeaturedService,
        private router: Router,


    ) {
        this.form = this.formBuilder.group({
            name: ['',]
        })

        let count = this.merchantFeaturedService.featuredMerchants.length;
        let farray = this.merchantFeaturedService.merchants;
        let sarray = [] = new Array;
        let finalarray = [] = new Array;

        for (let i = 0; i < count; i++) {
            sarray.push(this.merchantFeaturedService.featuredMerchants[i].merchant)
        }
        let temp = 0;

        finalarray = _.differenceBy(farray, sarray, 'id');

        // console.log(farray);
        // console.log(sarray)
        // console.log(finalarray)
        this.merchants = finalarray;
        

    }
    ngOnInit() {
        this.form.valueChanges.subscribe(() => {
            // console.log(this.form.value);
          });
        
        
    }

    openForm() {
        debugger;
        this.form = this.formBuilder.group({
            name: ''
        });
        this.formActive = true;
        //this.focusNameField();
        this.form.controls.name.valueChanges
            .subscribe(searchTerm => {
                //alert(searchTerm);
                this.merchantFeaturedService.searchMerchant(searchTerm)
                    .subscribe(merchantNames => {
                        this.merchantSearchResult=_.intersectionBy(merchantNames, this.merchants, 'id'); 
                    });
            });

    }

    compareObj(obj1: any, obj2: any): boolean {
        return obj1.id !== obj2.id;
    }

    closeForm() {
        this.formActive = false;
    }

    

    focusNameField() {
        setTimeout(() => {
            this.nameInputField.nativeElement.focus();
        });
    }

    onFormSubmit() {
        // console.log("this.form.controls.name.value")
        // console.log(this.form.controls.name.value)
        // console.log("this.form.controls.name.value.id")
        // console.log(this.form.controls.name.value.id)
        if (this.form.valid && this.form.controls.name.value != null) {
            const cardName = this.form.getRawValue().name;
            var newMerchant: any = {};

            for (let i = 0; i < this.merchants.length; i++) {
                if (cardName == this.merchants[i].name) {
                    newMerchant = this.merchants[i];

                }
            }

            this.onCardAdd.next(newMerchant);

            this.formActive = false;
            let select: HTMLSelectElement = <HTMLSelectElement>document.getElementById("merchantfield");
            for (let i = select.options.length - 1; i >= 0; i--) {
                select.remove(i);
            }



        }



    }

}

