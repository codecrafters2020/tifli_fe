import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { MatSnackBarModule, MatButtonModule, MatCheckboxModule, MatChipsModule, MatDatepickerModule, MatDialogModule, MatFormFieldModule, MatIconModule, MatInputModule, MatListModule, MatMenuModule, MatProgressBarModule, MatRippleModule, MatSidenavModule, MatToolbarModule, MatTooltipModule, MatSelectModule } from '@angular/material';
import {MatAutocompleteModule} from '@angular/material/autocomplete';

import { FuseSharedModule } from '@fuse/shared.module';
import { FuseConfirmDialogModule, FuseMaterialColorPickerModule } from '@fuse/components';

import { BoardResolve, MerchantFeaturedService } from './merchant-featured.service';
import { MerchantFeaturedBoardComponent } from './merchant-featured-board/merchant-featured-board.component';
import { MerchantFeaturedBoardListComponent } from './merchant-featured-board/list/list.component';
import { MerchantFeaturedBoardCardComponent } from './merchant-featured-board/list/card/card.component';
import { MerchantFeaturedBoardEditListNameComponent } from './merchant-featured-board/list/edit-list-name/edit-list-name.component';
import { MerchantFeaturedBoardAddCardComponent } from './merchant-featured-board/list/add-card/add-card.component';
import { MerchantFeaturedBoardAddListComponent } from './merchant-featured-board/add-list/add-list.component';
import { MerchantFeaturedCardDialogComponent } from './merchant-featured-board/dialogs/card/card.component';
import { MerchantFeaturedLabelSelectorComponent } from './merchant-featured-board/dialogs/card/label-selector/label-selector.component';
import { MerchantFeaturedEditBoardNameComponent } from './merchant-featured-board/edit-board-name/edit-board-name.component';
import { MerchantFeaturedBoardSettingsSidenavComponent } from './merchant-featured-board/sidenavs/settings/settings.component';
import { MerchantFeaturedBoardColorSelectorComponent } from './merchant-featured-board/sidenavs/settings/board-color-selector/board-color-selector.component';
import { NgxDnDModule } from '@swimlane/ngx-dnd';

const routes: Routes = [
    {
        path     : '**',
        component: MerchantFeaturedBoardComponent,
        children: [],
        resolve  : {
            merchants:MerchantFeaturedService,
            board: BoardResolve
        }
    }
];

@NgModule({
    declarations   : [
        // FuseScrumboardComponent,
        MerchantFeaturedBoardComponent,
        MerchantFeaturedBoardListComponent,
        MerchantFeaturedBoardCardComponent,
        MerchantFeaturedBoardEditListNameComponent,
        MerchantFeaturedBoardAddCardComponent,
        MerchantFeaturedBoardAddListComponent,
        MerchantFeaturedCardDialogComponent,
        MerchantFeaturedLabelSelectorComponent,
        MerchantFeaturedEditBoardNameComponent,
        MerchantFeaturedBoardSettingsSidenavComponent,
        MerchantFeaturedBoardColorSelectorComponent
    ],
    imports        : [
        RouterModule.forChild(routes),

        MatButtonModule,
        MatCheckboxModule,
        MatChipsModule,
        MatDatepickerModule,
        MatDialogModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatListModule,
        MatMenuModule,
        MatProgressBarModule,
        MatRippleModule,
        MatSidenavModule,
        MatToolbarModule,
        MatTooltipModule,

        NgxDnDModule,

        FuseSharedModule,
        FuseConfirmDialogModule,
        FuseMaterialColorPickerModule,
        MatSelectModule,
        MatAutocompleteModule,
        MatSnackBarModule
    ],
    providers      : [
        MerchantFeaturedService,
        BoardResolve
    ],
    entryComponents: [MerchantFeaturedCardDialogComponent]
})
export class MerchantFeaturedModule
{
}
