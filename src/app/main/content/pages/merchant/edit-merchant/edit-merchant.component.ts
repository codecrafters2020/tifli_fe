import { Component, OnInit, ViewEncapsulation } from '@angular/core';

import { fuseAnimations } from '@fuse/animations';

import { EditMerchantService } from './edit-merchant.service';

import { Router } from '@angular/router';

@Component({
    selector     : 'fuse-file-manager',
    templateUrl  : './edit-merchant.component.html',
    styleUrls    : ['./edit-merchant.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class EditMerchantComponent implements OnInit
{
    selected: any;
    pathArr: string[];

    constructor(private EditMerchantService: EditMerchantService, private router: Router)
    {
    }

    ngOnInit()
    {
        // this.AddMerchantService.onFileSelected.subscribe(selected => {
        //     this.selected = selected;
        //     this.pathArr = selected.location.split('>');
        // });
    }

    openMerchantList(){
        
        this.router.navigateByUrl("pages/merchant/list/");
    }
}
