import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CdkTableModule } from '@angular/cdk/table';

import { MatFormFieldModule, MatInputModule, MatSelectModule, MatStepperModule, MatButtonModule, MatIconModule, MatRippleModule, MatSidenavModule, MatSlideToggleModule, MatTableModule, MatCheckboxModule} from '@angular/material';

import { FuseSharedModule } from '@fuse/shared.module';

import { EditMerchantComponent } from './edit-merchant.component';
import { EditMerchantService } from './edit-merchant.service';
import { FormEditMerchantComponent } from './form/form-edit-merchant.component';
import { FancyImageUploaderModule } from 'ng2-fancy-image-uploader';
import {MatExpansionModule} from '@angular/material/expansion';


const routes: Routes = [
    {
        path     : '**',
        component:EditMerchantComponent,
        children : [],
        resolve  : {
            merchantById : EditMerchantService
        }
    }
];

@NgModule({
    declarations: [
        EditMerchantComponent,
        FormEditMerchantComponent,
    ],
    imports     : [
        RouterModule.forChild(routes),

        CdkTableModule,
        MatFormFieldModule,
        MatExpansionModule,
        MatInputModule,
        MatSelectModule,
        MatStepperModule,
        MatButtonModule,
        MatIconModule,
        MatRippleModule,
        MatSlideToggleModule,
        MatSidenavModule,
        MatTableModule,
        FancyImageUploaderModule,
        MatCheckboxModule,

        FuseSharedModule
    ],
    providers   : [
        EditMerchantService
    ]
})
export class EditMerchantModule
{
}
