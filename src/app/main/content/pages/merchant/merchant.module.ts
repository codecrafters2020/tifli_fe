import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { FuseAngularMaterialModule } from '../../components/angular-material/angular-material.module';

import { FuseSharedModule } from '@fuse/shared.module';

const routes = [
    {
        path: 'list',
        loadChildren: './list-merchant/list-merchant.module#ListMerchantModule'
    },
    {
        path: 'add',
        loadChildren: './add-merchant/add-merchant.module#AddMerchantModule'
    },
    {
        path: 'edit/:id',
        loadChildren: './edit-merchant/edit-merchant.module#EditMerchantModule'
    },
    {
        path: 'temp',
        loadChildren: './edit-merchant/edit-merchant.module#EditMerchantModule'
    },
    {
        path: 'monetized',
        loadChildren: './monetized-merchant/monetized-merchant.module#MonetizedMerchantModule'
    }
];

@NgModule({
    imports: [
        FuseSharedModule,
        RouterModule.forChild(routes),
        FuseAngularMaterialModule
    ],
    declarations: []
})
export class MerchantModule {
}
