import { Component, ElementRef, OnInit, ViewChild, AfterViewInit, OnChanges, Output, EventEmitter } from '@angular/core';
import { MatPaginator, MatSort } from '@angular/material';
import { DataSource, SelectionModel } from '@angular/cdk/collections';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';

import { MonetizedMerchantService } from '../monetized-merchant.service';
import { fuseAnimations } from '@fuse/animations/index';
import { FuseUtils } from '@fuse/utils';
import * as _ from 'lodash';

@Component({
    selector   : 'fuse-file-list',
    templateUrl: './grid-monetized-merchant.component.html',
    styleUrls  : ['./grid-monetized-merchant.component.scss'],
    animations : fuseAnimations
})
export class GridMonetizedMerchantComponent implements OnInit, OnChanges, AfterViewInit
{
    files: any;
    dataSource: GridMerchantDataSource | null;
    displayedColumns = ['icon', 'merchant_name', 'description', 'select'];
    selected: any;
    lastId: number;
    public selection = new SelectionModel<GridMerchantDataSource>(true, []);

    @Output() notify: EventEmitter<any> = new EventEmitter<any>();

    // pagination
    pageSize: number;
    pageSizeOptions: number[];


    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild('filter') filter: ElementRef;
    @ViewChild(MatSort) sort: MatSort;

    constructor(private MonetizedMerchantService: MonetizedMerchantService, private router: Router)
    {
        this.pageSize = 25;
        this.pageSizeOptions = [this.pageSize, this.pageSize * 2, this.pageSize * 4]; 
        // this.MonetizedMerchantService.onFilesChanged.subscribe(files => {
        //     this.files = files;
        // });
        // this.MonetizedMerchantService.onFileSelected.subscribe(selected => {
        //     this.selected = selected;
        // });
    }

    ngOnChanges(changes) {
    }

    ngAfterViewInit() {
    }

    ngOnInit()
    {
        // debugger;
        // this.dataSource = new GridMerchantDataSource(this.MonetizedMerchantService, this.paginator, this.sort);
        this.dataSource = new GridMerchantDataSource(this.MonetizedMerchantService, this.paginator, this.sort);
        // Observable.fromEvent(this.filter.nativeElement, 'keyup')
        //           .debounceTime(150)
        //           .distinctUntilChanged()
        //           .subscribe(() => {
        //               if ( !this.dataSource )
        //               {
        //                   return;
        //               }
        //               this.dataSource.filter = this.filter.nativeElement.value;
        //           });
    }

    onSelect(selected)
    {
        this.MonetizedMerchantService.onFileSelected.next(selected);
    }

    openMerchantDashboard(event,merchantid){
        this.router.navigateByUrl("apps/dashboards/project/" + merchantid);
    }

    openMerchantEdit(event,merchantid){
        this.router.navigateByUrl("pages/administration/edit-merchant/" + merchantid);
    }

    /** Whether the number of selected elements matches the total number of rows. */
    isAllSelected() {
        const numSelected = this.selection.selected.length;
        const numRows = this.dataSource.filteredData.length;
        return numSelected === numRows;
    }

    /** Selects all rows if they are not all selected; otherwise clear selection. */
    masterToggle() {
        this.isAllSelected() ? this.selection.clear() : this.dataSource.filteredData.forEach(row => this.selection.select(row));
    }
}

export class GridMerchantDataSource extends DataSource<any>
{
    _filterChange = new BehaviorSubject('');
    _filteredDataChange = new BehaviorSubject('');

    get filteredData(): any
    {
        return this._filteredDataChange.value;
    }

    set filteredData(value: any)
    {
        this._filteredDataChange.next(value);
    }

    get filter(): string
    {
        return this._filterChange.value;
    }

    set filter(filter: string)
    {
        this._filterChange.next(filter);
    }
    constructor(private MonetizedMerchantService: MonetizedMerchantService, private _paginator: MatPaginator, private _sort: MatSort)
    {
        super();
        this.filteredData = this.MonetizedMerchantService.monetizedMerchants;
    }

    /** Connect function called by the table to retrieve one stream containing the data to render. */
    // connect(): Observable<any[]>
    // {
    //     return this.MonetizedMerchantService.onFilesChanged;
    // }
    connect(): Observable<any[]>
    {
        const displayDataChanges = [
            this.MonetizedMerchantService.onFilesChanged,
            this._paginator.page,
            this._filterChange,
            // this._sort.sortChange
        ];

        return Observable.merge(...displayDataChanges).map(() => {
            let data = this.MonetizedMerchantService.monetizedMerchants.slice();

            data = this.filterData(data);

            this.filteredData = [...data];

            //data = this.sortData(data);

            // Grab the page's slice of data.
            const startIndex = this._paginator.pageIndex * this._paginator.pageSize;
            return data.splice(startIndex, this._paginator.pageSize);
        });
    }

    filterData(data)
    {
        if ( !this.filter )
        {
            return data;
        }
        return FuseUtils.filterArrayByString(data, this.filter);
    }

    // sortData(data): any[]
    // {
    //     if ( !this._sort.active || this._sort.direction === '' )
    //     {
    //         return data;
    //     }

    //     return data.sort((a, b) => {
    //         let propertyA: number | string = '';
    //         let propertyB: number | string = '';

    //         switch ( this._sort.active )
    //         {
    //             case 'id':
    //                 [propertyA, propertyB] = [a.id, b.id];
    //                 break;
    //             case 'name':
    //                 [propertyA, propertyB] = [a.name, b.name];
    //                 break;
    //             case 'categories':
    //                 [propertyA, propertyB] = [a.categories[0], b.categories[0]];
    //                 break;
    //             case 'price':
    //                 [propertyA, propertyB] = [a.priceTaxIncl, b.priceTaxIncl];
    //                 break;
    //             case 'quantity':
    //                 [propertyA, propertyB] = [a.quantity, b.quantity];
    //                 break;
    //             case 'active':
    //                 [propertyA, propertyB] = [a.active, b.active];
    //                 break;
    //         }

    //         const valueA = isNaN(+propertyA) ? propertyA : +propertyA;
    //         const valueB = isNaN(+propertyB) ? propertyB : +propertyB;

    //         return (valueA < valueB ? -1 : 1) * (this._sort.direction === 'asc' ? 1 : -1);
    //     });
    // }

    disconnect()
    {
    }
}
