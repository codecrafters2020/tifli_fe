import { Component, ElementRef, OnInit, ViewChild, Inject, AfterViewInit, OnChanges } from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { FormControl } from '@angular/forms';
import { DataSource } from '@angular/cdk/collections';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

import { fuseAnimations } from '@fuse/animations/index';
import { FuseUtils } from '@fuse/utils';
import * as _ from 'lodash';
import { delay } from 'q';

import { MonetizedMerchantService } from '../../monetized-merchant.service';

@Component({
    selector   : 'monetized-merchants-modal',
    templateUrl: './grid-monetized-merchant-modal.component.html',
    styleUrls  : ['./grid-monetized-merchant-modal.component.scss'],
    animations : fuseAnimations
})

export class GridMonetizedMerchantModalComponent implements OnInit, OnChanges, AfterViewInit
{
    files: any;
    dataSource: GridMerchantDataSource | null;
    displayedColumns = ['merchant_0', 'merchant_1', 'merchant_2', 'merchant_3'];
    // displayedColumns = ['merchant_name'];

    selected: any[] = [];

    lastId: number;

    // pagination
    pageSize: number;
    pageSizeOptions: number[];

    merchantSearchTerm : FormControl = new FormControl();
    merchantList = [
        {'name': 'Merchant1'},
        {'name': 'Merchant2'},
        {'name': 'Merchant3'}
    ];

    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild('filter') filter: ElementRef;
    @ViewChild(MatSort) sort: MatSort;

    constructor(public dialogRef: MatDialogRef<GridMonetizedMerchantModalComponent>,
                @Inject(MAT_DIALOG_DATA) public data: any,
                private MonetizedMerchantService: MonetizedMerchantService, private router: Router)
    {
        this.pageSize = 25;
        this.pageSizeOptions = [this.pageSize, this.pageSize * 2, this.pageSize * 4];

        this.merchantSearchTerm.valueChanges
        .debounceTime(400)
        .subscribe(searchTerm => {
            this.MonetizedMerchantService.getFilteredNonMonetizedMerchant(searchTerm)
                .subscribe(merchants => {
                    this.dataSource = new GridMerchantDataSource(this.MonetizedMerchantService, 
                                                                 this.paginator, 
                                                                 this.sort, 
                                                                 merchants, 4);
                });
        })
    }

    ngOnChanges(changes) {
    }

    ngAfterViewInit() {
    }

    ngOnInit()
    {
        this.dataSource = new GridMerchantDataSource(this.MonetizedMerchantService, this.paginator, this.sort, undefined, 4);
    }

    checked(event, value)
    {
        let selectedText = value;
        let selectedObject = _.find(this.MonetizedMerchantService.nonMonetizedMerchants, o => o.name == selectedText);
        selectedObject.isMonetized = true;
        this.selected.includes(selectedObject) ? _.remove(this.selected, selectedObject) : this.selected.push(selectedObject);
    }

    closeModal(): void 
    {
        this.dialogRef.close();
    }

    update()
    {
        if (this.selected.length == 0) return;
        this.MonetizedMerchantService.updateMonetizedMerchantsCategory(this.selected)
        .then((response) => {
            this.MonetizedMerchantService.getMonetizedMerchants();
            this.MonetizedMerchantService.getNonMonetizedMerchants();
            this.closeModal();
            delay(3000)
        this.router.navigateByUrl("pages/merchant/monetized/")
        })
    }
}

export class GridMerchantDataSource extends DataSource<any>
{
    _filterChange = new BehaviorSubject('');
    _filteredDataChange = new BehaviorSubject('');
    currentMerchants = [];

    get filteredData(): any
    {
        return this._filteredDataChange.value;
    }

    set filteredData(value: any)
    {
        this._filteredDataChange.next(value);
    }

    get filter(): string
    {
        return this._filterChange.value;
    }

    set filter(filter: string)
    {
        this._filterChange.next(filter);
    }

    constructor(private MonetizedMerchantService: MonetizedMerchantService, private _paginator: MatPaginator, private _sort: MatSort, private response: Array<any>, private _columns: Number)
    {
        super();
        let structuredData = [];
        let row = {};

        this.response ? this.currentMerchants = [...this.response] : this.currentMerchants = [...this.MonetizedMerchantService.nonMonetizedMerchants];

        this.currentMerchants.forEach((value, index) => {
            if (index % 4 === 0) {
                structuredData.push(row);
                row = {};
            }
            row['merchant_' + index % 4] = value.name;
            if (index === this.currentMerchants.length - 1) {
                structuredData.push(row);
                row = {};
                // this.MonetizedMerchantService.nonMonetizedMerchants = structuredData;
                this.currentMerchants = structuredData.filter(value => Object.keys(value).length !== 0);
            }
        });
        this.filteredData = this.currentMerchants;
    }

    /** Connect function called by the table to retrieve one stream containing the data to render. */
    // connect(): Observable<any[]>
    // {
    //     return this.MonetizedMerchantService.onFilesChanged;
    // }
    connect(): Observable<any[]>
    {
        const displayDataChanges = [
            this.MonetizedMerchantService.onFilesChanged,
            this._paginator.page,
            this._filterChange,
            // this._sort.sortChange
        ];

        return Observable
            .merge(...displayDataChanges)
            .map(() => {
                let data = this.currentMerchants.slice();

                data = this.filterData(data);

                this.filteredData = [...data];

                //data = this.sortData(data);

                // Grab the page's slice of data.
                const startIndex = this._paginator.pageIndex * this._paginator.pageSize;
                return data.splice(startIndex, this._paginator.pageSize);
        });
    }

    filterData(data)
    {
        if ( !this.filter )
        {
            return data;
        }
        return FuseUtils.filterArrayByString(data, this.filter);
    }

    disconnect()
    {
        // revert to the default data     
    }
}
