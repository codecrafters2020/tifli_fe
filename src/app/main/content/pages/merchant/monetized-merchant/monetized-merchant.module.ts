import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CdkTableModule } from '@angular/cdk/table';

import { MatFormFieldModule, MatInputModule, MatSelectModule, MatStepperModule, MatButtonModule, MatIconModule, MatRippleModule, MatSidenavModule, MatSlideToggleModule, MatTableModule, MatMenuModule, MatPaginatorModule, MatSortModule, MatAutocompleteModule } from '@angular/material';

import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatRadioModule} from '@angular/material/radio';
import { FuseSharedModule } from '@fuse/shared.module';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { MatDialogModule } from '@angular/material/dialog';

import { MonetizedMerchantComponent } from './monetized-merchant.component';
import { MonetizedMerchantService } from './monetized-merchant.service';
import { GridMonetizedMerchantComponent } from './grid/grid-monetized-merchant.component';
import { FuseNgxDatatableComponent } from './grid/datatable/ngx-datatable.component';
import { GridMonetizedMerchantModalComponent } from './grid/modal/grid-monetized-merchant-modal.component';
import { SidenavMonetizedMerchantMainComponent } from './sidenavs/main/sidenav-monetized-merchant-main.component';
import { SidenavMonetizedMerchantDetailsComponent } from './sidenavs/details/sidenav-monetized-merchant-details.component';

const routes: Routes = [
    {
        path     : '**',
        component: MonetizedMerchantComponent,
        children : [],
        resolve  : {
            files: MonetizedMerchantService
        }
    }
];

@NgModule({
    declarations: [
        MonetizedMerchantComponent,
        GridMonetizedMerchantComponent,
        FuseNgxDatatableComponent,
        GridMonetizedMerchantModalComponent,
        SidenavMonetizedMerchantMainComponent,
        SidenavMonetizedMerchantDetailsComponent
    ],
    imports     : [
        RouterModule.forChild(routes),

        CdkTableModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        MatStepperModule,
        MatButtonModule,
        MatIconModule,
        MatRippleModule,
        MatSlideToggleModule,
        MatSidenavModule,
        MatTableModule,
        MatMenuModule,
        MatPaginatorModule, 
        MatSortModule,
        MatDialogModule,
        MatAutocompleteModule,

        NgxDatatableModule,

        FuseSharedModule,
        MatDatepickerModule,
        MatCheckboxModule,
        MatRadioModule
    ],
    providers   : [
        MonetizedMerchantService
    ],
    entryComponents: [FuseNgxDatatableComponent, GridMonetizedMerchantModalComponent]
})
export class MonetizedMerchantModule
{
}
