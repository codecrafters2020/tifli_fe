import { Component, OnInit, ViewEncapsulation } from '@angular/core';

import { fuseAnimations } from '@fuse/animations';

import { MonetizedMerchantService } from './monetized-merchant.service';
import { Router } from '@angular/router';

import { MatDialog, MatDialogRef } from '@angular/material';
import { FuseNgxDatatableComponent } from './grid/datatable/ngx-datatable.component';
import { GridMonetizedMerchantModalComponent } from './grid/modal/grid-monetized-merchant-modal.component';
import { FormControl } from '@angular/forms';

import * as _ from 'lodash';
import { delay } from 'q';

@Component({
    selector: 'fuse-file-manager',
    templateUrl: './monetized-merchant.component.html',
    styleUrls: ['./monetized-merchant.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class MonetizedMerchantComponent implements OnInit {
    selected: any;
    pathArr: string[];
    addMonetized: boolean = false;
    searchClicked: boolean = true;
    merchantSearchTerm: FormControl = new FormControl();
    dialogRef: MatDialogRef<GridMonetizedMerchantModalComponent>;

    constructor(private MonetizedMerchantService: MonetizedMerchantService, private router: Router, private dialog: MatDialog) {
    }

    ngOnInit() {
        // this.MonetizedMerchantService.onFileSelected.subscribe(selected => {
        //     this.selected = selected;
        //     // this.pathArr = selected.location.split('>');
        // });
    }

    toggle() {
        // this.addMonetized = !this.addMonetized;
        this.dialogRef = this.dialog.open(GridMonetizedMerchantModalComponent, {
            width: '1500px'
        });
    }

    openAddFileDialog() {
    }

    update(monetizedGrid) {
        if (monetizedGrid.selection.selected == 0) return;

        let payload = _.difference(this.MonetizedMerchantService.monetizedMerchants, monetizedGrid.selection.selected);

        this.MonetizedMerchantService.updateNonMonetizedMerchantsCategory(payload)
            .then((response) => {
                setTimeout(() => { this.router.navigateByUrl("pages/merchant/monetized/") }, 2000)
                this.MonetizedMerchantService.getMonetizedMerchants();
                location.reload();

            })
       
        // this.router.navigateByUrl("pages/merchant/monetized");


    }
}
