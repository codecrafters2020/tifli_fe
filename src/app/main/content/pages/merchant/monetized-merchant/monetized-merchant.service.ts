import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';

import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { from } from 'rxjs/observable/from';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/map';

import { AppService } from './../../../../../app.service';

@Injectable()
export class MonetizedMerchantService implements Resolve<any>
{
    monetizedMerchants: any;
    nonMonetizedMerchants: any;
    currentDisplayedNonMonetizedMerchants: any[];
    onFilesChanged: BehaviorSubject<any> = new BehaviorSubject({});
    onFileSelected: BehaviorSubject<any> = new BehaviorSubject({});

    constructor(private http: HttpClient, private appService:AppService)
    {
    }

    /**
     * The File Manager App Main Resolver
     * 
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any
    {

        // return new Promise((resolve, reject) => {

        //     Promise.all([
      //  //         this.getMonetizedMerchants(),
        //         this.getNonMonetizedMerchants()
        //     ]).then(
        //         ([monetizedMerchants, nonMonetizedMerchants]) => {
        //             resolve();
        //         }, reject);
        // });
    }

    getNonMonetizedMerchants(): Promise<any>
    {
        return new Promise((resolve, reject) => {
            this.http.get(this.appService.merchantService + 'monetized/?isMonetized=false')
                .subscribe((response: any) => {                    
                    this.nonMonetizedMerchants = response.data;
                    // console.log(this.nonMonetizedMerchants[1].isActive==false);
                    var newarray = this.nonMonetizedMerchants.filter(function(el){
                        return el.isActive==true;
                    });
                    this.onFilesChanged.next(response.data);
                    this.onFileSelected.next(response[0]);
                    this.nonMonetizedMerchants = newarray;
                    resolve(response);
                }, reject);
        });
    }

    // getMonetized
    getMonetizedMerchants(): Promise<any>
    {
        return new Promise((resolve, reject) => {
            // this.http.get('http://192.168.1.76:8765/cms/api/merch/monetized/')
            this.http.get(this.appService.merchantService + 'monetized/?isMonetized=true')
                .subscribe((response: any) => {
                    this.monetizedMerchants = response.data;
                    this.onFilesChanged.next(response.data);
                    this.onFileSelected.next(response[0]);
                    resolve(response);
                }, reject);
        });
    }


    getManager(id, datasource, index): Promise<any>
    {
        return new Promise((resolve, reject) => {
            this.http.get(this.appService.userService + 'users/${id}')
                .subscribe((response: any) => {
                    datasource[index].manager.name = response.data.userName;
                    resolve(response);
                }, reject);
        });
    }

    updateMonetizedMerchantsCategory(monetizedMerchants): Promise<any>
    {
        let updatedList = [...this.monetizedMerchants, ...monetizedMerchants]
        return new Promise((resolve, reject) => {
            this.http.put(this.appService.merchantService + 'monetized/', updatedList)
                .subscribe((response: any) => {
                    resolve(response);
                }, reject);
        });
    }

    updateNonMonetizedMerchantsCategory(nonMonetizedMerchants): Promise<any>
    {
        let updatedList = [...nonMonetizedMerchants]
        return new Promise((resolve, reject) => {
            this.http.put(this.appService.merchantService + 'monetized/', updatedList)
                .subscribe((response: any) => {
                    resolve(response);
                }, reject);
        });
    }

    getFilteredMonetizedMerchant(queryString: string): Observable<any>
    {
        return this.http.get(this.appService.merchantService + `monetized/search/?isMonetized=true&name=${queryString}`)
            .map(response => response['data'])
            .map(data => data instanceof Array ? data : [])
    }

    getFilteredNonMonetizedMerchant(queryString: string): Observable<any>
    {
        return this.http.get(this.appService.merchantService + `monetized/search/?isMonetized=false&name=${queryString}`)
            .map(response => response['data'])
            .map(data => data instanceof Array ? data : [])
    }
}
