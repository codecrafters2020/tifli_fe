import { Component, OnInit } from '@angular/core';

import { fuseAnimations } from '@fuse/animations/index';

import { MonetizedMerchantService } from '../../monetized-merchant.service';

@Component({
    selector   : 'fuse-file-manager-details-sidenav',
    templateUrl: './sidenav-monetized-merchant-details.component.html',
    styleUrls  : ['./sidenav-monetized-merchant-details.component.scss'],
    animations : fuseAnimations
})
export class SidenavMonetizedMerchantDetailsComponent implements OnInit
{

    selected: any;

    constructor(private MonetizedMerchantService: MonetizedMerchantService)
    {

    }

    ngOnInit()
    {
        this.MonetizedMerchantService.onFileSelected.subscribe(selected => {
            this.selected = selected;
        });
    }

}
