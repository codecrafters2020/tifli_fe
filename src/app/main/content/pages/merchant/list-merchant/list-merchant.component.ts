import { Component, OnInit, ViewEncapsulation } from '@angular/core';

import { fuseAnimations } from '@fuse/animations';

import { ListMerchantService } from './list-merchant.service';
import { Router } from '@angular/router';

@Component({
    selector     : 'fuse-file-manager',
    templateUrl  : './list-merchant.component.html',
    styleUrls    : ['./list-merchant.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class ListMerchantComponent implements OnInit
{
    selected: any;
    pathArr: string[];

    constructor(private ListMerchantService: ListMerchantService, private router: Router)
    {
    }

    ngOnInit()
    {
        // this.ListMerchantService.onFileSelected.subscribe(selected => {
        //     this.selected = selected;
        //    // this.pathArr = selected.location.split('>');
        // });
    }

    openUserAdd(){
        this.router.navigateByUrl("pages/merchant/add/");
    }
}
