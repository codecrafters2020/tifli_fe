import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CdkTableModule } from '@angular/cdk/table';

import { MatFormFieldModule, MatInputModule, MatSelectModule, MatStepperModule, MatButtonModule, MatIconModule, MatRippleModule, MatSidenavModule, MatSlideToggleModule, MatTableModule, MatMenuModule, MatPaginatorModule, MatSortModule, MatAutocomplete, MatAutocompleteModule } from '@angular/material';

import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatRadioModule } from '@angular/material/radio';
import { FuseSharedModule } from '@fuse/shared.module';

import { ListMerchantComponent } from './list-merchant.component';
import { ListMerchantService } from './list-merchant.service';
import { GridMerchantComponent } from './grid/grid-merchant.component';
import { SidenavMerchantMainComponent } from './sidenavs/main/sidenav-merchant-main.component';
import { SidenavMerchantDetailsComponent } from './sidenavs/details/sidenav-merchant-details.component';

const routes: Routes = [
    {
        path: '**',
        component: ListMerchantComponent,
        children: [],
        resolve: {
            files: ListMerchantService
        }
    }
];

@NgModule({
    declarations: [
        ListMerchantComponent,
        GridMerchantComponent,
        SidenavMerchantMainComponent,
        SidenavMerchantDetailsComponent
    ],
    imports: [
        RouterModule.forChild(routes),
        MatAutocompleteModule,
        CdkTableModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        MatStepperModule,
        MatButtonModule,
        MatIconModule,
        MatRippleModule,
        MatSlideToggleModule,
        MatSidenavModule,
        MatTableModule,
        MatMenuModule,
        MatPaginatorModule,
        MatSortModule,
        FuseSharedModule,
        MatDatepickerModule,
        MatCheckboxModule,
        MatRadioModule
    ],

    providers: [
        ListMerchantService
    ]
})
export class ListMerchantModule {
}
