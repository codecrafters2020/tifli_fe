import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { AppService } from './../../../../../app.service';
import { AbstractControl } from '@angular/forms';

@Injectable()
export class AddMerchantService implements Resolve<any>
{

    categoriesList: any[];
    usersList: any[];
    barcodesList: any[];
    affiliateNetworksList: any[];
    merchantAffiliateStatusList: any[];
    merchantsList: any[];
    merchantFound: boolean = false;
    merchantTierList: any[];

    constructor(private http: HttpClient, private appService: AppService) {
    }

    /**
     * The File Manager App Main Resolver
     * 
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {

        // return new Promise((resolve, reject) => {

        //     Promise.all([
        //         this.getCategories(),
        //         this.getUsers(),
        //         this.getBarcodes(),
        //         this.getAffiliateNetworks(),
        //         this.getAffiliateNetworkStatuses(),
        //         this.getMerchants(),
        //         this.getMerchantTiers()
        //     ]).then(
        //         ([categoriesList, usersList, barcodesList, affiliateNetworksList, merchantAffiliateStatusList, merchantsList, merchantTierList]) => {
        //             resolve();
        //         },
        //         reject
        //     );
        // });
    }

    getMerchants(): Promise<any> {

        return new Promise((resolve, reject) => {
            this.http.get(this.appService.merchantService)
                .subscribe((response: any) => {
                    console.log(response);
                    if (response.meta.code == "200") {
                        this.merchantsList = response.data;
                    }
                    resolve(response);
                }, reject);
        });
    }
    //http://websvc.westus.cloudapp.azure.com/merchantapi/merchant/Meh


    findMerchantByName(merchantName: string ): Promise<any>
    {
        return new Promise((resolve, reject) => {
            this.http.get(this.appService.merchantService + 'merchant/'+merchantName)
                .subscribe((response: any) => {
                    console.log(response);
                    debugger;
                    if(response.meta.code=="200")
                    {
                        this.merchantFound=true;
                    }
                    resolve(response);
                }, reject);
        });
    }  

    getCategories(): Promise<any> {

        return new Promise((resolve, reject) => {

            this.http.get(this.appService.merchantService + 'categories/')
                .subscribe((response: any) => {
                    console.log(response);
                    if (response.meta.code == "200") {
                        this.categoriesList = response.data;
                    }
                    // else{
                    //     alert(response.meta.message);
                    // }

                    resolve(response);
                }, reject);
        });
    }

    getUsers(): Promise<any> {

        return new Promise((resolve, reject) => {
            this.http.get(this.appService.userService + 'users/')
                .subscribe((response: any) => {
                    console.log(response);
                    if (response.meta.code == "200") {
                        this.usersList = response.data;
                    }
                    resolve(response);
                }, reject);
        });
    }

    getBarcodes(): Promise<any> {

        return new Promise((resolve, reject) => {
            this.http.get(this.appService.merchantService + 'barcodes/')
                .subscribe((response: any) => {
                    console.log(response);
                    if (response.meta.code == "200") {
                        this.barcodesList = response.data;
                    }
                    resolve(response);
                }, reject);
        });
    }

    getAffiliateNetworks(): Promise<any> {

        return new Promise((resolve, reject) => {
            this.http.get(this.appService.merchantService + 'affiliates/')
                .subscribe((response: any) => {
                    console.log(response);
                    if (response.meta.code == "200") {
                        this.affiliateNetworksList = response.data;
                    }
                    resolve(response);
                }, reject);
        });
    }

    getAffiliateNetworkStatuses(): Promise<any> {

        return new Promise((resolve, reject) => {
            this.http.get(this.appService.merchantService + 'affiliates/statuses/')
                .subscribe((response: any) => {
                    console.log(response);
                    if (response.meta.code == "200") {
                        this.merchantAffiliateStatusList = response.data;
                    }
                    resolve(response);
                }, reject);
        });
    }

    getMerchantTiers(): Promise<any> {

        return new Promise((resolve, reject) => {
            this.http.get(this.appService.merchantService + 'tiers/')
                .subscribe((response: any) => {
                    console.log(response);
                    if (response.meta.code == "200") {
                        this.merchantTierList = response.data;
                    }
                    resolve(response);
                }, reject);
        });
    }

    addMerchant(merchant): Promise<any> {
        return new Promise((resolve, reject) => {
            debugger;
            this.http.post(this.appService.merchantService, merchant)
                .subscribe((response: any) => {
                    resolve(response);
                }, reject);
        });
    }

}
