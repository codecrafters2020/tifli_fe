import { Component, OnInit, ViewEncapsulation } from '@angular/core';

import { fuseAnimations } from '@fuse/animations';

import { AddMerchantService } from './add-merchant.service';

import { Router } from '@angular/router';

@Component({
    selector     : 'fuse-file-manager',
    templateUrl  : './add-merchant.component.html',
    styleUrls    : ['./add-merchant.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class AddMerchantComponent implements OnInit
{
    selected: any;
    pathArr: string[];

    constructor(private AddMerchantService: AddMerchantService, private router: Router)
    {
    }

    ngOnInit()
    {
        // this.AddMerchantService.onFileSelected.subscribe(selected => {
        //     this.selected = selected;
        //     this.pathArr = selected.location.split('>');
        // });
    }

    openMerchantList(){
        
        this.router.navigateByUrl("pages/merchant/list/");
    }
}
