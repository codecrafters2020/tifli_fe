import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CdkTableModule } from '@angular/cdk/table';

import { MatFormFieldModule, MatAutocompleteModule, MatInputModule, MatSelectModule, MatStepperModule, MatButtonModule, MatIconModule, MatRippleModule, MatSidenavModule, MatSlideToggleModule, MatTableModule, MatMenuModule, MatPaginatorModule, MatSortModule } from '@angular/material';

import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatRadioModule} from '@angular/material/radio';
import { FuseSharedModule } from '@fuse/shared.module';

import { ListSeasonalComponent } from './list-seasonal.component';
import { ListSeasonalService } from './list-seasonal.service';
import { GridSeasonalComponent } from './grid/grid-seasonal.component';
import { SidenavSeasonalMainComponent } from './sidenavs/main/sidenav-seasonal-main.component';
import { SidenavSeasonalDetailsComponent } from './sidenavs/details/sidenav-seasonal-details.component';

const routes: Routes = [
    {
        path     : '**',
        component: ListSeasonalComponent,
        children : [],
        resolve  : {
            files: ListSeasonalService
        }
    }
];

@NgModule({
    declarations: [
        ListSeasonalComponent,
        GridSeasonalComponent,
        SidenavSeasonalMainComponent,
        SidenavSeasonalDetailsComponent
    ],
    imports     : [
        RouterModule.forChild(routes),

        CdkTableModule,
        MatAutocompleteModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        MatStepperModule,
        MatButtonModule,
        MatIconModule,
        MatRippleModule,
        MatSlideToggleModule,
        MatSidenavModule,
        MatTableModule,
        MatMenuModule,
        MatPaginatorModule,
        MatSortModule,

        FuseSharedModule,
        MatDatepickerModule,
        MatCheckboxModule,
        MatRadioModule
    ],
    providers   : [
        ListSeasonalService
    ]
})
export class ListSeasonalModule
{
}
