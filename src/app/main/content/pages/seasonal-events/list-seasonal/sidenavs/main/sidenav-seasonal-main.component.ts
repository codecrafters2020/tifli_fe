import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';
import { AbstractControl, FormBuilder, FormGroup, FormArray, Validators, EmailValidator } from '@angular/forms';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatRadioModule } from '@angular/material/radio';
import { FormControl } from '@angular/forms';
import { Observable, BehaviorSubject } from 'rxjs';
import { of } from 'rxjs/observable/of';
import { map, startWith } from 'rxjs/operators';
import { FuseAngularMaterialModule } from '../../../../../components/angular-material/angular-material.module';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { ListSeasonalService } from '../../list-seasonal.service';
@Component({
    selector: 'fuse-file-manager-main-sidenav',
    templateUrl: './sidenav-seasonal-main.component.html',
    styleUrls: ['./sidenav-seasonal-main.component.scss']
})
export class SidenavSeasonalMainComponent {
    @Input() leftSideNav: any;

    form: FormGroup;
    formErrors: any;

    eventSearchResult: any[];
    seasonalStatus = ['Active', 'Inactive'];





    selected: any;

    constructor(private ListSeasonalService: ListSeasonalService,
        private router: Router,

        private formBuilder: FormBuilder) {
        // Reactive Form


        this.form = this.formBuilder.group({
            eventname: ['',]
            // seasonalStatus: ['',],
        });

        // Reactive form errors
        this.formErrors = {
            eventname: {},
            // seasonalStatus: {}
        };
    }

    displaySelectedName(item: any) {
        return item;
    }


    ngOnInit() {
        this.form.valueChanges.subscribe(() => {
            // console.log(this.form.value);
        });
        let x: any;
        this.form.controls.eventname.valueChanges
            .subscribe(searchTerm => {
                this.ListSeasonalService.searchSeasonalEvent(searchTerm)
                    .subscribe(eventname => {
                        x = eventname;
                        this.eventSearchResult = eventname;
                        // if(searchTerm == x[0].name){
                        //     this.router.navigateByUrl("pages/seasonal-events/edit/"+x[0].id);
                        // }
                    });
            });
    }



    filter() {
        // this.btnDisabled = true;
        this.ListSeasonalService.getFilteredSeasonal('',
            '',
            'couponCode,asc',
            this.form.controls.eventname.value
        )
            .then((response) => {

            })
    }

    callSomeFunction(){
        this.eventSearchResult.forEach(event => {
            if(event.name == this.form.controls.eventname.value){
                    this.router.navigateByUrl("pages/seasonal-events/edit/"+event.id);
            }
        })
    }

}
