import { Component, OnInit } from '@angular/core';

import { fuseAnimations } from '@fuse/animations/index';

import { ListSeasonalService } from '../../list-seasonal.service';

@Component({
    selector   : 'fuse-file-manager-details-sidenav',
    templateUrl: './sidenav-seasonal-details.component.html',
    styleUrls  : ['./sidenav-seasonal-details.component.scss'],
    animations : fuseAnimations
})
export class SidenavSeasonalDetailsComponent implements OnInit
{

    selected: any;

    constructor(private ListSeasonalService: ListSeasonalService)
    {

    }

    ngOnInit()
    {
        // this.ListSeasonalService.onFileSelected.subscribe(selected => {
        //     this.selected = selected;
        // });
    }

}
