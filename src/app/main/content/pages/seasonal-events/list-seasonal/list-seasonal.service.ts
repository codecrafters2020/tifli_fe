import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';

import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { AppService } from '../../../../../app.service';

import { promise } from 'protractor';
import * as _ from 'lodash';

@Injectable()
export class ListSeasonalService implements Resolve<any>
{
    seasonalEvents: any[];
    onFilesChanged: BehaviorSubject<any> = new BehaviorSubject({});
    onFileSelected: BehaviorSubject<any> = new BehaviorSubject({});

    constructor(private http: HttpClient, private appService: AppService) {
    }

    /**
     * The File Manager App Main Resolver
     * 
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        // return new Promise((resolve, reject) => {

        //     Promise.all([
        //         this.getSeasonalEvents()
        //     ]).then(
        //         ([seasonalEvents]) => {
        //             resolve();
        //         },
        //         reject
        //     );
        // });
    }

    getSeasonalEvents(): Promise<any> {
        return new Promise((resolve, reject) => {
            // this.http.get('https://reqres.in/api/users?page=2')
            this.http.get(`${this.appService.couponService}seasonal/`)
                .subscribe((response: any) => {
                    this.seasonalEvents = response.data;
                    this.onFilesChanged.next(response.data);
                    this.onFileSelected.next(response[0]);
                    resolve(response);
                }, reject);
        });
    }

    searchSeasonalEvent(term: string): Observable<any> {
        return this.http.get(`${this.appService.seasonalservice}search/?name=${term}`)
            .map(response => response['data'])
            .map(data => data instanceof Array ? data : [])
    }

    searchSeasonalStatus(term: string): Observable<any> {
        return this.http.get(`${this.appService.seasonalservice}search/?status=${term}`)
            .map(response => response['data'])
            .map(data => data instanceof Array ? data : [])
    }

    getFilteredSeasonal(page: number | string,
        size: number | string,
        sort: string,
        seasonalName: string): Promise<any> {
        let params = {
            page,
            size,
            sort,
            seasonalName
        };
        console.log("seasonal name:")
        console.log(seasonalName)

        let queryString = '';
        _.forEach(params, (val, key) => {

            if (val || typeof val === 'number') {
                queryString = 'name=' + seasonalName;
                console.log("query string:")
                console.log(queryString)

            }
        });

        return new Promise((resolve, reject) => {
            // this.http.get('api/coupon-list')
            // this.http.get(`http://104.42.179.33:8765/cms/api/coupons?page=0&size=20&sort=couponCode,asc&searchTerms=gag&couponCode=123&couponType=Deal`)
            this.http.get(`${this.appService.seasonalservice}search?${queryString}`)
                .subscribe((response: any) => {
                    this.seasonalEvents = response.data.content;
                    this.onFilesChanged.next(response.data);
                    this.onFileSelected.next(response[0]);
                    resolve(response);
                }, reject);
        });
    }


}
