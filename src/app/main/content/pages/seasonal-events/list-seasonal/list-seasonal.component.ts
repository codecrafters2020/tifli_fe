import { Component, OnInit, ViewEncapsulation } from '@angular/core';

import { fuseAnimations } from '@fuse/animations';

import { ListSeasonalService } from './list-seasonal.service';
import { Router } from '@angular/router';

@Component({
    selector     : 'fuse-file-manager',
    templateUrl  : './list-seasonal.component.html',
    styleUrls    : ['./list-seasonal.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class ListSeasonalComponent implements OnInit
{
    selected: any;
    pathArr: string[];

    constructor(private ListSeasonalService: ListSeasonalService, private router: Router)
    {
    }

    ngOnInit()
    {
        // this.ListSeasonalService.onFileSelected.subscribe(selected => {
        //     this.selected = selected;
        //    // this.pathArr = selected.location.split('>');
        // });
    }

    openUserAdd(){
        this.router.navigateByUrl("pages/seasonal-events/add/");
    }
}
