import { Component, OnInit, ViewEncapsulation } from '@angular/core';

import { fuseAnimations } from '@fuse/animations';

import { AddSeasonalService } from './add-seasonal.service';

import { Router } from '@angular/router';

@Component({
    selector     : 'fuse-file-manager',
    templateUrl  : './add-seasonal.component.html',
    styleUrls    : ['./add-seasonal.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class AddSeasonalComponent implements OnInit
{
    selected: any;
    pathArr: string[];

    constructor(private AddSeasonalService: AddSeasonalService, private router: Router)
    {
    }

    ngOnInit()
    {
        
    }

    openUserList(){
        
        this.router.navigateByUrl("pages/seasonal-events/list/");
    }
}
