import { Component,
     ElementRef,
     OnInit,
     ViewChild,
     Inject,
     AfterViewInit,
     OnChanges
     } from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { FormControl } from '@angular/forms';
import { DataSource, SelectionModel } from '@angular/cdk/collections';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

import { fuseAnimations } from '@fuse/animations/index';
import { FuseUtils } from '@fuse/utils';
import * as _ from 'lodash';

import { AddSeasonalService } from '../../add-seasonal.service'

@Component({
    selector   : 'popular-stores-modal',
    templateUrl: './grid-popular-stores-modal.component.html',
    styleUrls  : ['./grid-popular-stores-modal.component.scss'],
    animations : fuseAnimations
})

export class GridPopularStoresModalComponent implements OnInit, OnChanges, AfterViewInit
{
    files: any;
    dataSource: GridPopularStoresDataSource | null;
    displayedColumns = ['merchant'];
    public selection = new SelectionModel<GridPopularStoresDataSource>(true, []);
    // displayedColumns = ['merchant_name'];

    selected: any[] = [];

    lastId: number;

    // pagination
    pageSize: number;
    pageSizeOptions: number[];

    merchantSearchTerm : FormControl = new FormControl();


    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild('filter') filter: ElementRef;
    @ViewChild(MatSort) sort: MatSort;

    constructor(public dialogRef: MatDialogRef<GridPopularStoresModalComponent>,
                @Inject(MAT_DIALOG_DATA) public data: any,
                private AddSeasonalService: AddSeasonalService, private router: Router)
    {
        this.pageSize = 25;
        this.pageSizeOptions = [this.pageSize, this.pageSize * 2, this.pageSize * 4];

        
        /*this.merchantSearchTerm.valueChanges
        .debounceTime(400)
        .subscribe(searchTerm => {
            this.AddSeasonalService.getFilteredNonMonetizedMerchant(searchTerm)
                .subscribe(merchants => {
                    this.dataSource = new GridMerchantDataSource(this.MonetizedMerchantService, 
                                                                 this.paginator, 
                                                                 this.sort, 
                                                                 merchants, 4);
                });
        })*/
        
    }

    ngOnChanges(changes) {
    }

    ngAfterViewInit() {
    }

    ngOnInit()
    {
        // this.dataSource = new GridPopularStoresDataSource(this.AddSeasonalService, this.paginator, this.sort, undefined, 4);
        this.dataSource = new GridPopularStoresDataSource(this.AddSeasonalService, this.paginator, this.sort);
    }

    checked(event, value)
    {
        
        let selectedText = value;
        let selectedObject = _.find(this.AddSeasonalService.merchants, o => o.id == selectedText);
        // selectedObject.isMonetized = true;
        this.selected.includes(selectedObject) ? _.remove(this.selected, selectedObject) : this.selected.push(selectedObject);
        
    }

    closeModal(): void 
    {
        this.dialogRef.close();
    }

    update()
    {
        this.selected = this.selection.selected
        if (this.selected.length == 0) return;
        this.dialogRef.close(this.selected)
        
    }

    applyFilter(filterValue: string) {
        filterValue = filterValue.trim(); // Remove whitespace
        filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
        this.dataSource.filter = filterValue;
    }

    /** Whether the number of selected elements matches the total number of rows. */
    isAllSelected() {
        const numSelected = this.selection.selected.length;
        const numRows = this.dataSource.filteredData.length;
        return numSelected === numRows;
    }

    /** Selects all rows if they are not all selected; otherwise clear selection. */
    masterToggle() {
        this.isAllSelected() ? this.selection.clear() : this.dataSource.filteredData.forEach(row => this.selection.select(row));
    }
}

export class GridPopularStoresDataSource extends DataSource<any>
{
    _filterChange = new BehaviorSubject('');
    _filteredDataChange = new BehaviorSubject('');

    get filteredData(): any
    {
        return this._filteredDataChange.value;
    }

    set filteredData(value: any)
    {
        this._filteredDataChange.next(value);
    }

    get filter(): string
    {
        return this._filterChange.value;
    }

    set filter(filter: string)
    {
        this._filterChange.next(filter);
    }
    constructor(private AddSeasonalService: AddSeasonalService, private _paginator: MatPaginator, private _sort: MatSort)
    {
        super();
        this.filteredData = this.AddSeasonalService.merchants;
    }

    /** Connect function called by the table to retrieve one stream containing the data to render. */
    // connect(): Observable<any[]>
    // {
    //     return this.AddSeasonalService.onFilesChanged;
    // }
    connect(): Observable<any[]>
    {
        const displayDataChanges = [
            this.AddSeasonalService.onFilesChanged,
            this._paginator.page,
            this._filterChange,
            // this._sort.sortChange
        ];

        return Observable.merge(...displayDataChanges).map(() => {
            let data = this.AddSeasonalService.merchants.slice();

            data = this.filterData(data);

            this.filteredData = [...data];

            //data = this.sortData(data);

            // Grab the page's slice of data.
            const startIndex = this._paginator.pageIndex * this._paginator.pageSize;
            return data.splice(startIndex, this._paginator.pageSize);
        });
    }

    filterData(data)
    {
        if ( !this.filter )
        {
            return data;
        }
        return FuseUtils.filterArrayByString(data, this.filter);
    }

    // sortData(data): any[]
    // {
    //     if ( !this._sort.active || this._sort.direction === '' )
    //     {
    //         return data;
    //     }

    //     return data.sort((a, b) => {
    //         let propertyA: number | string = '';
    //         let propertyB: number | string = '';

    //         switch ( this._sort.active )
    //         {
    //             case 'id':
    //                 [propertyA, propertyB] = [a.id, b.id];
    //                 break;
    //             case 'name':
    //                 [propertyA, propertyB] = [a.name, b.name];
    //                 break;
    //             case 'categories':
    //                 [propertyA, propertyB] = [a.categories[0], b.categories[0]];
    //                 break;
    //             case 'price':
    //                 [propertyA, propertyB] = [a.priceTaxIncl, b.priceTaxIncl];
    //                 break;
    //             case 'quantity':
    //                 [propertyA, propertyB] = [a.quantity, b.quantity];
    //                 break;
    //             case 'active':
    //                 [propertyA, propertyB] = [a.active, b.active];
    //                 break;
    //         }

    //         const valueA = isNaN(+propertyA) ? propertyA : +propertyA;
    //         const valueB = isNaN(+propertyB) ? propertyB : +propertyB;

    //         return (valueA < valueB ? -1 : 1) * (this._sort.direction === 'asc' ? 1 : -1);
    //     });
    // }

    disconnect()
    {
    }
}
