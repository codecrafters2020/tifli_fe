import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';

import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { AppService } from './../../../../../app.service';

@Injectable()
export class AddSeasonalService implements Resolve<any>
{
    merchants: any[];
    onFilesChanged: BehaviorSubject<any> = new BehaviorSubject({});
    onFileSelected: BehaviorSubject<any> = new BehaviorSubject({});


    constructor(private http: HttpClient, private appService: AppService)
    {
    }

    /**
     * The File Manager App Main Resolver
     * 
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any
    {

        // return new Promise((resolve, reject) => {
        //     // debugger;
        //     Promise.all([
        //         this.getMerchants()
        //     ]).then(([merchants]) => {
        //         resolve();
        //     }, reject)
        // });
    }

    addSeasonalEvent(seasonalEvent): Promise<any>
    {
        // debugger;
        return new Promise((resolve, reject) => {
            // this.http.post(this.appService.apiUrl + 'cms/api/merchants/categories/', merchantCategory)
            this.http.post(`${this.appService.couponService}seasonal/`, seasonalEvent)
                .subscribe((response: any) => {
                    resolve(response);
                }, reject);
        });
    }

    getMerchants(): Promise<any> {
        return new Promise((resolve, reject) => {
            this.http.get(this.appService.merchantService)
                .subscribe((response: any) => {
                    this.merchants = response.data;
                    this.onFilesChanged.next(response.data);
                    this.onFileSelected.next(response[0]);
                    resolve(response);
                }, reject);
        });

    }

}