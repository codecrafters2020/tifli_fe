import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';

import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { AppService } from './../../../../../app.service';

@Injectable()
export class EditSeasonalService implements Resolve<any>
{
    merchants: any[];
    id:any;
    seasonalEvent: any;
    onFilesChanged: BehaviorSubject<any> = new BehaviorSubject({});
    onFileSelected: BehaviorSubject<any> = new BehaviorSubject({});


    constructor(private http: HttpClient, private appService: AppService)
    {
    }

    /**
     * The File Manager App Main Resolver
     * 
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any
    {
        // this.id=route.params['id'];

        // return new Promise((resolve, reject) => {
        //     // debugger;
        //     Promise.all([
        //         this.getMerchants(),
        //         this.getSeasonalEventById()
        //     ]).then(([merchants]) => {
        //         resolve();
        //     }, reject)
        // });
    }

    updateSeasonalEvent(seasonalEvent): Promise<any>
    {
        // debugger;
        return new Promise((resolve, reject) => {
            // this.http.post(this.appService.apiUrl + 'cms/api/merchants/categories/', merchantCategory)
            this.http.put(`${this.appService.couponService}seasonal/${seasonalEvent.id}`, seasonalEvent)
                .subscribe((response: any) => {
                    resolve(response);
                }, reject);
        });
    }

    getSeasonalEventById(): Promise<any> {
        return new Promise((resolve, reject) => {
            this.http.get(this.appService.couponService + 'seasonal/' + this.id)
                .subscribe((response: any) => {
                    // console.log(response);
                    if(response.meta.code=="200")
                    {
                        this.seasonalEvent = response.data;  
                    }
                    resolve(response);
                }, reject);
        });
    }

    getMerchants(): Promise<any> {
        return new Promise((resolve, reject) => {
            this.http.get(this.appService.merchantService)
                .subscribe((response: any) => {
                    this.merchants = response.data;
                    this.onFilesChanged.next(response.data);
                    // this.onFileSelected.next(response[0]);
                    resolve(response);
                }, reject);
        });
    }

    getMerchantById(merchantId): Promise<any> {
        return new Promise((resolve, reject) => {
            this.http.get(this.appService.merchantService + 'merchants/' + merchantId)
                .subscribe((response: any) => {
                    // console.log(response);
                    resolve(response);
                }, reject);
        });
    }

}