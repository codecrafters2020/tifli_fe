import { Component, OnInit, ViewChild } from '@angular/core';
import { 
    AbstractControl,
    FormBuilder,
    FormGroup,
    FormArray,
    Validators,
    EmailValidator 
 } from '@angular/forms';
import { EditSeasonalService } from '../edit-seasonal.service';
import { Router } from '@angular/router';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { startWith } from 'rxjs/operators/startWith';
import { map } from 'rxjs/operators/map';
import { MatSnackBar } from '@angular/material';
import { FancyImageUploaderOptions, UploadedFile } from 'ng2-fancy-image-uploader';
import { Subscription } from 'rxjs/Subscription';
import { GridPopularStoresModalComponent } from './modal/grid-popular-stores-modal.component';
import {
    MatAutocomplete,
    MatAutocompleteSelectedEvent,
    MatAutocompleteTrigger,
    MatDialog,
    MatDialogRef,
    VERSION,
  } from '@angular/material';

import * as _ from 'lodash';

import { AppService } from '../../../../../../app.service';

@Component({
    selector   : 'fuse-file-list',
    templateUrl: './form-edit-seasonal.component.html',
    styleUrls  : ['./form-edit-seasonal.component.scss'],
})


export class FormEditSeasonalComponent implements OnInit {
    form: FormGroup;
    formErrors: any;
    test: any;

    visible: boolean = true;
    selectable: boolean = true;
    removable: boolean = true;
    addOnBlur: boolean = true;

    fancyUploaderOptions: FancyImageUploaderOptions = {
      thumbnailHeight: 300,
      thumbnailWidth: 1050,
      uploadUrl: this.appService.uploadService + 'uploads/',
      allowedImageTypes: ['image/png', 'image/jpeg'],
      maxImageSize: 3
    };

    dialogRef: MatDialogRef<GridPopularStoresModalComponent>;


    normalizedName: string;
    popularStores = [];

    seasonalEvent: any = {}

    seasonStatus = false;
    images:any[]=[];

    editorConfig = {
        editable: true,
        spellcheck: true,
        placeholder: "Enter Description here...",
        toolbar: [
            ["bold", "italic", "underline", "strikeThrough", "superscript", "subscript"],
            ["justifyLeft", "justifyCenter", "justifyRight", "justifyFull", "indent", "outdent"],
            ["cut", "copy", "delete", "removeFormat", "undo", "redo"],
            ["paragraph", "blockquote", "removeBlockquote", "horizontalLine", "orderedList", "unorderedList"]
        ]
    }
        

    constructor(private formBuilder: FormBuilder, private EditSeasonalService: EditSeasonalService, private router: Router, private snackBar: MatSnackBar, private dialog: MatDialog, private appService: AppService)
    {
        this.seasonalEvent = EditSeasonalService.seasonalEvent
        // console.log(this.seasonalEvent)
        this.seasonStatus = this.seasonalEvent.status === 'Active' ? false : true

        if(!this.seasonalEvent.hasOwnProperty('images')){
            this.seasonalEvent.images=this.images;
        }

        if(this.seasonalEvent.seasonalPopularStore != undefined){
        this.seasonalEvent.seasonalPopularStore.forEach((item, index) => {
            // console.log(item, index)
            new Promise((resolve, reject) => {
                resolve(EditSeasonalService.getMerchantById(item.merchant.id))
            }).then(result => {
                console.log(result)
                this.popularStores.push(result['data'])
            })
            // this.popularStores.push(EditSeasonalService.getMerchantById(item.merchant.id))
        })
    }
else{
    this.seasonalEvent.seasonalPopularStore = [];
}
        // this.popularStores = this.seasonalEvent.seasonalPopularStore
        // Reactive form errors
        this.formErrors = {
            name: {},
            // heading: {},
            descriptionHeading: {},
            description: {},
            // startDate: {},
            // endDate: {},
            linkText: {},
            tag: {},
            status: {},
            popularStoresHeading: {}
        };
    }

    ngOnInit()
    {
        // Reactive Form
        this.form = this.formBuilder.group({
            name: ["", Validators.required],
            // heading: ["", Validators.required],
            descriptionHeading: ["", Validators.required],
            description: ["", Validators.required],
            // startDate: ["", Validators.required],
            // endDate: ["", Validators.required],
            linkText: ["", Validators.required],
            tag: ["", Validators.required],
            status: ["", Validators.required],
            popularStoresHeading: ["", Validators.required]
        });

        this.form.valueChanges
            .map(obj => {
                let res = obj.name
                res = res.split(" ")
                for (var i = 0; i < res.length; i++) {
                     res[i] = res[i].toLowerCase()
                }
                res = res.join("-")
                this.normalizedName = res;
                this.seasonalEvent.normalizedName = res;
            })
            .subscribe(result => {
                this.onFormValuesChanged();
            });

        

    }

    onFormValuesChanged()
    {
        for ( const field in this.formErrors )
        {
            if ( !this.formErrors.hasOwnProperty(field) )
            {
                continue;
            }

            // Clear previous errors
            this.formErrors[field] = {};

            // Get the control
            const control = this.form.get(field);

            if ( control && control.dirty && !control.valid )
            {
                this.formErrors[field] = control.errors;
            }
        }
    }

    onSubmit() {
        debugger;
        let formValues = this.form.value;
        if (this.form.valid) {
            for (var i = 0; i < this.popularStores.length; ++i) {
                let popStore = {
                    createdBy: 1,
                    modifiedBy: 1,
                    status: 'Active',
                    merchant :{
                        id: this.popularStores[i].id
                    }
                }
                this.seasonalEvent.seasonalPopularStore.push(popStore)
            }

            this.seasonalEvent.status = this.seasonStatus === true ? 'Pending' : 'Active'

            this.log(this.seasonalEvent)
            
            this.EditSeasonalService.updateSeasonalEvent(this.seasonalEvent).then(response => {
                // debugger;
                console.log(response)
                this.snackBar.open(response.meta.message,"Done", {
                    duration: 2000,
                });
                if(response.meta.status == "success")
                    this.redirect('pages/seasonal-events/list');
            }); 
            
        }
        
    }

    redirect(pagename: string) {
        this.router.navigate([pagename]);
    }

    log(val: any) { console.log(val); }

  
    onUpload(file: UploadedFile) {
        debugger;
        var response: any = {};
        response = JSON.parse(file.response);
        if (response.meta.code == 200) {
            let sImage: any = {}
            sImage.fileName = response.data.fileName;
            sImage.suffix = "seasonal";
            this.seasonalEvent.images.push(sImage);
        }
        
    }

    openAddPopularStore() {
        // this.popularStores.push({a: 1})
        this.dialogRef = this.dialog.open(GridPopularStoresModalComponent, {
            width: '1500px'
        })

        this.dialogRef.afterClosed().subscribe(result => {
          this.popularStores = result ? result : this.popularStores;
        });

    }

    removePopularStore(storeId) {
        this.popularStores = _.remove(this.popularStores, store => {
            return store.id != storeId;
        })
    }
}
