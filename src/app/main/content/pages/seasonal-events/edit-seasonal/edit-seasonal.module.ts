import { NgModule } from '@angular/core';
import { RouterModule, 
    Routes } from '@angular/router';
import { CdkTableModule } from '@angular/cdk/table';

import { 
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatStepperModule,
    MatButtonModule,
    MatIconModule,
    MatRippleModule, 
    MatSidenavModule, 
    MatSlideToggleModule, 
    MatTableModule, 
    MatCheckboxModule,
    MatGridListModule,
    MatPaginatorModule
    } from '@angular/material';
import {MatChipsModule} from '@angular/material/chips';
import {MatAutocompleteModule} from '@angular/material/autocomplete';

import { FuseSharedModule } from '@fuse/shared.module';

import { EditSeasonalComponent } from './edit-seasonal.component';
import { EditSeasonalService } from './edit-seasonal.service';
import { FormEditSeasonalComponent } from './form/form-edit-seasonal.component';
import { FancyImageUploaderModule } from 'ng2-fancy-image-uploader';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { NgxEditorModule } from 'ngx-editor';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { GridPopularStoresModalComponent } from './form/modal/grid-popular-stores-modal.component';


const routes: Routes = [
    {
        path     : '**',
        component: EditSeasonalComponent,
        children : [],
        resolve  : {
            couponsData: EditSeasonalService
        }
    }
];

@NgModule({
    declarations: [
        EditSeasonalComponent,
        FormEditSeasonalComponent,
        GridPopularStoresModalComponent
    ],
    imports     : [
        RouterModule.forChild(routes),

        CdkTableModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        MatStepperModule,
        MatButtonModule,
        MatIconModule,
        MatRippleModule,
        MatSlideToggleModule,
        MatSidenavModule,
        MatTableModule,
        MatChipsModule,
        MatAutocompleteModule,
        MatExpansionModule,
        MatPaginatorModule, 
        MatGridListModule,
        FancyImageUploaderModule,
        MatCheckboxModule,
        MatDatepickerModule,
        NgxEditorModule,
        TooltipModule.forRoot(),
        AngularFontAwesomeModule,
        
        FuseSharedModule
    ],
    providers   : [
        EditSeasonalService
    ],
    entryComponents: [GridPopularStoresModalComponent]
})
export class EditSeasonalModule
{
}
