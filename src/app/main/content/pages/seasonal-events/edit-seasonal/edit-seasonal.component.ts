import { Component, OnInit, ViewEncapsulation } from '@angular/core';

import { fuseAnimations } from '@fuse/animations';

import { EditSeasonalService } from './edit-seasonal.service';

import { Router } from '@angular/router';

@Component({
    selector     : 'fuse-file-manager',
    templateUrl  : './edit-seasonal.component.html',
    styleUrls    : ['./edit-seasonal.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class EditSeasonalComponent implements OnInit
{
    selected: any;
    pathArr: string[];

    constructor(private EditSeasonalService: EditSeasonalService, private router: Router)
    {
    }

    ngOnInit()
    {
        
    }

    openUserList(){
        
        this.router.navigateByUrl("pages/seasonal-events/list/");
    }
}
