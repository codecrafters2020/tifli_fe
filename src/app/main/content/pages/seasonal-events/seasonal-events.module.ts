import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { FuseAngularMaterialModule } from '../../components/angular-material/angular-material.module';

import { FuseSharedModule } from '@fuse/shared.module';

const routes = [
    {
        path        : 'list',
        loadChildren: './list-seasonal/list-seasonal.module#ListSeasonalModule'
    },
    {
        path        : 'add',
        loadChildren: './add-seasonal/add-seasonal.module#AddSeasonalModule'
    },
    {
        path        : 'edit/:id',
        loadChildren: './edit-seasonal/edit-seasonal.module#EditSeasonalModule'
    }
];

@NgModule({
    imports     : [
        FuseSharedModule,
        RouterModule.forChild(routes),
        FuseAngularMaterialModule
    ],
    declarations: []
})
export class SeasonalModule
{
}
