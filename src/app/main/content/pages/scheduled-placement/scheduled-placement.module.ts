import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { MatAutocompleteModule, MatFormFieldModule, MatInputModule, MatSelectModule, MatButtonModule, MatDatepickerModule, MatIconModule, MatSlideToggleModule, MatToolbarModule, MatNativeDateModule } from '@angular/material';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import { CalendarModule } from 'angular-calendar';
import { ColorPickerModule } from 'ngx-color-picker';

import { FuseSharedModule } from '@fuse/shared.module';
import { FuseConfirmDialogModule } from '@fuse/components';

import { ScheduledPlacementService } from './scheduled-placement.service';
import { ScheduledPlacementComponent } from './scheduled-placement.component';
import { FuseCalendarEventFormDialogComponent } from './slot-form/slot-form.component';

const routes: Routes = [
    {
        path     : '**',
        component: ScheduledPlacementComponent,
        children : [],
        resolve  : {
            chat: ScheduledPlacementService
        }
    }
];

@NgModule({
    declarations   : [
        ScheduledPlacementComponent,
        FuseCalendarEventFormDialogComponent
    ],
    imports        : [
        RouterModule.forChild(routes),
        MatFormFieldModule, 
        MatInputModule, 
        MatSelectModule,

        MatAutocompleteModule,
        MatButtonModule,
        MatDatepickerModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatSlideToggleModule,
        MatToolbarModule,
        MatNativeDateModule,
        MatSnackBarModule,

        CalendarModule.forRoot(),
        ColorPickerModule,

        FuseSharedModule,
        FuseConfirmDialogModule
    ],
    providers      : [
        ScheduledPlacementService
    ],
    entryComponents: [FuseCalendarEventFormDialogComponent]
})
export class ScheduledPlacementModule
{
}
