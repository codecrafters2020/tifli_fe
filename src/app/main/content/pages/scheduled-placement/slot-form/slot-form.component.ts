import { Component, Inject, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';

import { MatColors } from '@fuse/mat-colors';

import { CalendarEvent } from 'angular-calendar';
import { SlotModel } from '../slot.model';
import { ScheduledPlacementService} from '../scheduled-placement.service';

@Component({
    selector     : 'fuse-calendar-event-form-dialog',
    templateUrl  : './slot-form.component.html',
    styleUrls    : ['./slot-form.component.scss'],
    encapsulation: ViewEncapsulation.None
})

export class FuseCalendarEventFormDialogComponent
{
    event: SlotModel;
    dialogTitle: string;
    eventForm: FormGroup;
    action: string;
    presetColors = MatColors.presets;
    merchantSearchResult: any[] = [
        {name:'Usman', id:1},
        {name:'Farrukh', id:2},
        {name:'Umair', id:3}
    ];
    couponSearchResult: any[] = [
        {title:'Save upto 30% on all samsung coupons', id:1},
        {title:'Save upto 40% on all samsung coupons', id:2}
    ]
    selectedMerchant: any = {};
    selectedCoupon: any = {title:'Save upto 40% on all samsung coupons', id:2};
    couponSelectionDisabled: boolean = true;
    title: any;
    titleParts: any;
    position: any;

    constructor(
        public dialogRef: MatDialogRef<FuseCalendarEventFormDialogComponent>,
        @Inject(MAT_DIALOG_DATA) private data: any,
        private formBuilder: FormBuilder,
        private scheduledPlacementService: ScheduledPlacementService
    )
    {
        this.event = data.event;
        this.action = data.action;
        this.selectedMerchant = this.event.merchant;
        this.position = this.event.title.split("-")[0];

        if ( this.action === 'edit' )
        {
            debugger;
            this.dialogTitle = this.event.title;
            this.titleParts = this.event.title.split("<br>");
            if(this.titleParts.length == 1)
                this.dialogTitle = this.titleParts[0];
            else
                this.dialogTitle = this.titleParts[0] + " " + this.titleParts[1];
        }
        else
        {
            debugger;
            this.dialogTitle = 'New Event';
            this.event = new SlotModel({
                start: data.date,
                end  : data.date
            });
        }

        this.scheduledPlacementService.onSelectedMerchantsChanged.subscribe(selectedMerchant => {
            debugger;
            this.selectedMerchant = selectedMerchant;
            // this.eventForm.controls.merchant.setValue(this.selectedMerchant);
        })

        this.scheduledPlacementService.onMerchantsChanged.subscribe(merchantsUpdatedList => {
            debugger;
            this.merchantSearchResult = merchantsUpdatedList;
        });

        this.scheduledPlacementService.onCouponsChanged.subscribe(couponsUpdatedList => {
            debugger;
            this.couponSearchResult = couponsUpdatedList;
        });

        this.eventForm = this.createEventForm();
        
        debugger;
        this.eventForm.controls.merchant.valueChanges
        .debounceTime(400)
        .subscribe(searchTerm => {
    		this.scheduledPlacementService.searchMerchant(searchTerm);
        });

        this.eventForm.controls.coupon.valueChanges
        .debounceTime(400)
        .subscribe(searchTerm => {
    		this.scheduledPlacementService.searchCoupon(searchTerm);
        })

    }

    createEventForm()
    {
        return new FormGroup({
            merchant : new FormControl(this.event.merchant),
            coupon : new FormControl(this.event.coupon),
            couponId : new FormControl(this.event.coupon.id),
            couponDescription : new FormControl(this.event.coupon.description),
            title : new FormControl(this.event.title),
            start : new FormControl(this.event.start),
            end   : new FormControl(this.event.end),
            color : this.formBuilder.group({
                primary  : new FormControl(this.event.color.primary),
                secondary: new FormControl(this.event.color.secondary)
            })
        });
    }

    displaySlectedMerchant(selectedMerchant: any): string | undefined {
        this.selectedMerchant = selectedMerchant;
        //this.eventForm.controls.merchant = this.selectedMerchant;
        this.couponSelectionDisabled = false;
        // this.eventForm.controls.coupon.
        return selectedMerchant ? selectedMerchant.name : undefined;
    }

    displaySlectedCoupon(selectedCoupon: any): string | undefined {
        debugger;
        this.selectedCoupon = selectedCoupon;
        if(this.selectedCoupon.merchant != null || this.selectedCoupon.merchant != undefined)
        {
            this.dialogTitle = this.position + " - " + this.selectedCoupon.id + " " + this.selectedCoupon.merchant.name;
            this.eventForm.controls.title.setValue(this.position + " - " + this.selectedCoupon.id + "<br>" + this.selectedCoupon.merchant.name);
            this.scheduledPlacementService.getMerchantById(this.selectedCoupon.merchant.id);
            //this.eventForm.controls.merchant.setValue(this.selectedCoupon.merchant);
        }
        if(this.selectedCoupon.id === 0){
            this.selectedMerchant = {id:0,name:''};
        }
        // else if(this.selectedCoupon.id != null){
        //     this.scheduledPlacementService.getMerchantById(this.selectedCoupon.merchant.id);
        // }
        return selectedCoupon ? selectedCoupon.title : undefined;
    }

}
