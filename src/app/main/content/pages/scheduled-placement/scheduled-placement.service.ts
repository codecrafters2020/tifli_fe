import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import {MatSnackBar} from '@angular/material';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { AppService } from './../../../../app.service';
import * as moment from 'moment';

import {
    startOfDay,
    endOfDay,
    subDays,
    addDays,
    endOfMonth,
    isSameDay,
    isSameMonth,
    addHours
} from 'date-fns';

@Injectable()
export class ScheduledPlacementService implements Resolve<any>
{
    placements: any;
    pages: any;
    placementsFromService: any;
    onSelectedMerchantsChanged: BehaviorSubject<any> = new BehaviorSubject<any>({});
    onplacementsUpdated = new Subject<any>();
    onMerchantsChanged: BehaviorSubject<any[]> = new BehaviorSubject<any[]>([]);
    onCouponsChanged: BehaviorSubject<any[]> = new BehaviorSubject<any[]>([]);
    firstday:any;
    lastday:any;
    slot : any;


    constructor(private http: HttpClient, private appService: AppService,private snackBar: MatSnackBar) {

    }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        // return new Promise((resolve, reject) => {
        //     var selectedDate = startOfDay(new Date());
        //     Promise.all([
        //         this.getPages(),
        //         this.getPlacements(selectedDate, 1, 0)
        //     ]).then(
        //         ([events, pages]) => {

        //             resolve();
        //         },
        //         reject
        //         );
        // });
    }

    getEvents() {
        return new Promise((resolve, reject) => {

            this.http.get('api/calendar/events')
                .subscribe((response: any) => {
                    this.placements = response.data;
                    this.onplacementsUpdated.next(this.placements);
                    resolve(this.placements);
                }, reject);
        });
    }

    updatePlacements(events, selectedDay, selectedPageId, selectedSeasonalId, selectedPageType) {
        debugger;
        var selectedDay = events[0].start;
        var selectedDayDate = selectedDay = moment(selectedDay.date).format("YYYY-MM-DD");
        var queryString = "?startDate=" + selectedDayDate + "&pageId=" + selectedPageId + "&pageType=" + selectedPageType;

        for(let i=0;i<events.length;i++){
            events[i].pageid = selectedPageId;
        }
        
        var payload = {
            placements: events
        };
        return new Promise((resolve, reject) => {
            this.http.post(this.appService.placementService + 'schedule/' + queryString, payload)
                .subscribe((response: any) => {
                    this.snackBar.open(response.meta.message,"Slot Updated!", {
                        duration: 2000,
                      });
                    this.getPlacements(selectedDay, selectedPageId, selectedSeasonalId);
                    //this.getPlacements(selectedDay, selectedPageId, selectedSeasonalId)
                }, reject);
        });
    }

    getPages() {
        return new Promise((resolve, reject) => {
            debugger;
            let seasonalPages = [];
            let pages = [];

            this.http.get(this.appService.placementService + `pages/`)
            .subscribe((response: any) => {
                debugger
                for(let i=0;i<response.data.length;i++){
                    let page = {
                        name:'',
                        pageType:'',
                        id: 0
                    }
                    page.name = response.data[i].pageName;
                    page.pageType = "page";
                    page.id = response.data[i].pageId;
                    pages.push(page);
                }

                this.pages = pages;

                this.http.get(this.appService.couponService + `seasonal/`)
                .subscribe((response: any) => {
                    debugger
                    for(let i=0;i<response.data.length;i++){
                        let page = {
                            name:'',
                            pageType:'',
                            id: 0
                        }
                        page.name = response.data[i].name;
                        page.pageType = "seasonal";
                        page.id = response.data[i].id;
                        this.pages.push(page);
                    }

                    //this.pages = pages;
                    resolve(this.pages);
                }, reject);
                resolve(this.pages);

            }, reject);

           
        });
    }

    getFirstLastDateWeek(selectedDay){
        debugger;
        console.log(typeof selectedDay);
        
        var curr = new Date; // get current date
        var first = selectedDay.getDate() - selectedDay.getDay(); // First day is the day of the month - the day of the week
        var last = first + 6; // last day is the first day + 6
        
        this.firstday = new Date(selectedDay.setDate(first));
        this.lastday = new Date(selectedDay.setDate(last));
        console.log("firstday");
        console.log(this.firstday);
        console.log("lastday");
        console.log(this.lastday);
    }

    getPlacements(selectedDay, pageid, seasonalid) {
        this.getFirstLastDateWeek(selectedDay);
        if (pageid == 0)
            pageid = "";
        if (seasonalid == 0)
            seasonalid = "";

        selectedDay = moment(selectedDay).format("YYYY-MM-DD");
        debugger;
        var queryString = "?startDate=" + selectedDay + "&pageId=" + pageid + "&seasonal=" + seasonalid
        return new Promise((resolve, reject) => {

            // this.http.get('api/calendarmy/events')
            this.http.get(this.appService.placementService + 'schedule/' + queryString)
                .subscribe((response: any) => {
                    debugger;
                    
                    this.transformResponseToPlacementModel(response, pageid);
                    var pageType = "page";

                    if (pageid == "") {
                        pageType = "seasonal";
                        pageid = seasonalid
                    }

                    this.positioningSlotsInCalendar(response, pageid, pageType);

                    this.onplacementsUpdated.next(this.placements);
                    this.snackBar.open(response.meta.message,"Page Refreshed!", {
                        duration: 2000,
                      });
                    resolve(this.placements);
                }, reject);
        });
    }

    transformResponseToPlacementModel(response, pageid) {

        debugger;
        this.placements = [];
        for (let i = 0; i < response.data.placements.length; i++) {
            let startTime: any;
            let endTime: any;
            let startTimeArr: any[];
            let hrs: any;
            let mins: any;
            let colorTheme: any;

            startTimeArr = response.data.placements[i].start.split("T");
            //hrs = startTimeArr[1][0];

            hrs = 12;
            if (parseInt(hrs) < 10) {
                hrs = "0" + hrs;
            }
            if (response.data.placements[i].position < 10) {
                mins = "0" + response.data.placements[i].position.toString();
            }
            else {
                mins = response.data.placements[i].position.toString();
            }
            startTime = startTimeArr[0] + "T" + hrs + ":" + mins + ":00Z";
            endTime = startTimeArr[0] + "T" + hrs + ":30:00Z";
            if (response.data.placements[i].position % 2 == 0) {
                colorTheme = {
                    primary: '#e3bc08',
                    secondary: '#FDF1BA'
                };
            }
            else {
                colorTheme = {
                    primary: '',
                    secondary: ''
                };
            }

            var merchantName = "";

            if (response.data.placements[i].merchant != null) {
                merchantName = response.data.placements[i].merchant.name;
            }

            if (response.data.placements[i].coupon.title != null) {
            let slot: any = {
                id: response.data.placements[0].id,
                start: startTime,
                end: endTime,
                title: mins + ' - ' + response.data.placements[i].coupon.id + '<br> ' + merchantName+ '<br> ' + response.data.placements[i].coupon.title,
                color: colorTheme,
                position: response.data.placements[i].position,
                draggable: false,
                coupon: response.data.placements[i].coupon,
                merchant: response.data.placements[i].merchant,
                pageid: response.data.placements[i].pageid,
                pageType: response.data.placements[i].pageType,
            }
            this.placements.push(slot);
            ;}
            else
            {
                let slot: any = {
                    id: response.data.placements[0].id,
                    start: startTime,
                    end: endTime,
                    title: mins + ' - ' + response.data.placements[i].coupon.id ,
                    color: colorTheme,
                    position: response.data.placements[i].position,
                    draggable: false,
                    coupon: response.data.placements[i].coupon,
                    merchant: response.data.placements[i].merchant,
                    pageid: response.data.placements[i].pageid,
                    pageType: response.data.placements[i].pageType,
                
            }
            this.placements.push(slot);
        }
    }}

    positioningSlotsInCalendar(response, pageid, pageType) {
        debugger;
        var placementPerDay = 15;
        var totalPlacements = placementPerDay * 7;
        var weekChange = false;
        var PrevWeek = 1;
        var curWeek = 0;
        //var curDate = response.data.weekStart;
        var curDate = moment(response.data.weekStart).local().utc().format();
        curDate = curDate.split("T")[0];
        var curPosition = 0;
        var placementFound = false;
        var dateIncrement;
        this.placementsFromService = Object.assign([], this.placements);

            for (let i = 0; i < totalPlacements; i++) {
                curPosition++;
                placementFound = false;
                if (Math.floor(i / 15) != curWeek) {
                    curDate = curDate + "T00:00:00Z"
                    dateIncrement = new Date(curDate);
                    dateIncrement.setDate(dateIncrement.getDate() + 1);
                    dateIncrement = dateIncrement.toJSON();
                    curDate = dateIncrement.split('T')[0];
                    curWeek++;
                    curPosition = 1;
                }
                for (let j = 0; j < this.placementsFromService.length; j++) {
                    let dateToCompare = this.placementsFromService[j].start.split("T")[0];
                
                    if (curDate == dateToCompare && curPosition == this.placementsFromService[j].position) {
                        placementFound = true
                        break;
                    }
                }

                let colorTheme: any;
                if (placementFound == false) {
                    if (curPosition % 2 == 0) {
                        colorTheme = {
                            primary: '#e3bc08',
                            secondary: '#FDF1BA'
                        };
                    }
                    else {
                        colorTheme = {
                            primary: '',
                            secondary: ''
                        };
                    }
                    var slotStartTime;
                    var curSlotPos = '';
                    if (curPosition < 10) {
                        // var d = new Date();
                        // var n = d.getTimezoneOffset();
                        // if(n > -60)
                        //     slotStartTime = "T15:0" + curPosition + ":00Z";    
                        slotStartTime = "T12:0" + curPosition + ":00Z";
                        curSlotPos = "0" + curPosition.toString();
                    }
                    else {
                        slotStartTime = "T12:" + curPosition + ":00Z";
                        curSlotPos = curPosition.toString();
                    }
                    let slot: any = {
                        id: 0,
                        start: curDate + slotStartTime,
                        end: curDate + 'T12:30:00Z',
                        title: curSlotPos + ' - ' ,
                        color: colorTheme,

                        position: curPosition,
                        pageid: pageid,
                        pageType: pageType,
                        draggable: false,
                        coupon: null,
                        merchant: null
                    };
                    this.placements.push(slot);
                }

            }

            // for(let i=0;i<this.placements.length;i++){
            //     if(this.placements[i].coupon != null){
            //         if(this.placements[i].coupon.id == undefined)
            //             this.placements.splice(i,1);
            //     }
            // }
    }

    searchMerchant(searchTerm) {
        debugger;
        this.http.get(this.appService.merchantService + 'search/?name=' + searchTerm).subscribe(response => {
            /* some operations or conditiond on response */

            this.onMerchantsChanged.next(response['data']);
        })
    }

    searchCoupon(searchTerm) {
        debugger;
        this.http.get(this.appService.couponService + 'coupons/search/?titleOrId=' + searchTerm).subscribe(response => {

            this.onCouponsChanged.next(response['data']);

        })
    }

    getQueryFormattedDate(date) {
        var year = date.getFullYear();

        var month = (1 + date.getMonth()).toString();
        month = month.length > 1 ? month : '0' + month;

        var day = date.getDate().toString();
        day = day.length > 1 ? day : '0' + day;

        return year + '-' + month + '-' + day;
    }

    getMerchantById(merchantId) {
        this.http.get(this.appService.merchantService + merchantId).subscribe(response => {

            this.onSelectedMerchantsChanged.next(response['data']);
        })
    }

}
