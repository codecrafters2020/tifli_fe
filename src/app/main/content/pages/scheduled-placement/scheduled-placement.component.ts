import { Component, OnInit, ViewEncapsulation, ViewChild, AfterViewChecked } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators, FormsModule, FormControl } from '@angular/forms';

import { Subject } from 'rxjs/Subject';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { MatDialog, MatDialogRef } from '@angular/material';
import { startOfDay, isSameDay, isSameMonth } from 'date-fns';

import { CalendarEvent, CalendarEventAction, CalendarEventTimesChangedEvent, CalendarMonthViewDay, CalendarWeekViewComponent } from 'angular-calendar';
import { MatSnackBar } from '@angular/material';

import { FuseConfirmDialogComponent } from '@fuse/components/confirm-dialog/confirm-dialog.component';
import { fuseAnimations } from '@fuse/animations';

import { FuseCalendarEventFormDialogComponent } from './slot-form/slot-form.component';
import { SlotModel } from './slot.model';
import { ScheduledPlacementService } from './scheduled-placement.service';

@Component({
    selector: 'fuse-calendar',
    templateUrl: './scheduled-placement.component.html',
    styleUrls: ['./scheduled-placement.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class ScheduledPlacementComponent implements OnInit {
    view: string;
    viewDate: Date;
    events: CalendarEvent[];
    public actions: CalendarEventAction[];
    activeDayIsOpen: boolean;
    refresh: Subject<any> = new Subject();
    dialogRef: any;
    confirmDialogRef: MatDialogRef<FuseConfirmDialogComponent>;
    selectedDay: any;
    pages: any[] = [];
    selectedPage: any = "Coupon Codes";
    onSelectedPageChange: BehaviorSubject<any> = new BehaviorSubject<any>({});
    placementShedulingForm: FormGroup;
    pageInitiated: any = false;
    eventToUpdate: any = null;

    @ViewChild(CalendarWeekViewComponent)
    weekView: CalendarWeekViewComponent;

    constructor(
        public dialog: MatDialog,
        public scheduledPlacementService: ScheduledPlacementService,
        private formBuilder: FormBuilder,
        private snackBar: MatSnackBar
    ) {
        //this.view = 'month';
        this.view = 'week';
        this.viewDate = new Date();
        this.activeDayIsOpen = true;
        this.selectedDay = { date: startOfDay(new Date()) };
        this.pages = scheduledPlacementService.pages;
        this.selectedPage = this.pages[0];
        this.actions = [];
       /**
         * Get events from service/server
         */
        this.setEvents();
    }

    ngOnInit() {
        /**
         * Watch re-render-refresh for updating db
         */
        this.refresh.subscribe(updateDB => {
            // console.warn('REFRESH');
            if (updateDB) {
                // console.warn('UPDATE DB');
                debugger;
                var selectedPageId = 0;
                var selectedSeasonalId = 0;
                if (this.selectedPage.pageType == "page") {
                    selectedPageId = this.selectedPage.id;
                }
                else {
                    selectedSeasonalId = this.selectedPage.id;
                }
                console.log(this.events);
                this.scheduledPlacementService.updatePlacements(this.events, this.selectedDay, selectedPageId, selectedSeasonalId, this.selectedPage.pageType);
            }
        });

        this.scheduledPlacementService.onplacementsUpdated.subscribe(events => {
            debugger;
            this.setEvents();
            this.refresh.next();
            this.weekView.events = this.events;
        });

        this.placementShedulingForm = this.formBuilder.group({
            selectPage: ['', Validators.required],
        });

        this.placementShedulingForm.controls.selectPage.valueChanges
            .subscribe(page => {
                debugger;
                this.selectedPage = page;
                if (this.pageInitiated) {
                    this.getWeeklyData(this.selectedDay);
                }
                this.pageInitiated = true;
            });
    }

    setEvents() {
        debugger;
        this.events = this.scheduledPlacementService.placements.map(item => {
            item.actions = this.actions;
            item.cssClass = "height:200px";
            return new SlotModel(item);
        });
    }

    /**
     * Day clicked
     * @param {MonthViewDay} day
     */
    dayClicked(day: CalendarMonthViewDay): void {
        const date: Date = day.date;
        const events: CalendarEvent[] = day.events;

        if (isSameMonth(date, this.viewDate)) {
            if ((isSameDay(this.viewDate, date) && this.activeDayIsOpen === true) || events.length === 0) {
                this.activeDayIsOpen = false;
            }
            else {
                this.activeDayIsOpen = true;
                this.viewDate = date;
            }
        }
        this.selectedDay = day;
        this.refresh.next();
    }

    /**
     * Edit Event
     * @param {string} action
     * @param {CalendarEvent} event
     */
    editEvent(action: string, event: CalendarEvent) {
        debugger;
        const eventIndex = this.events.indexOf(event);

        this.dialogRef = this.dialog.open(FuseCalendarEventFormDialogComponent, {
            panelClass: 'event-form-dialog',
            data: {
                event: event,
                action: action
            }
        });

        this.dialogRef.afterClosed()
            .subscribe(response => {
                debugger;
                if (!response) {
                    return;
                }
                const actionType: string = response[0];
                const formData: FormGroup = response[1];
                switch (actionType) {
                    /**
                     * Save
                     */
                    case 'save':
                    debugger;
                        //this.events[eventIndex] = Object.assign(this.events[eventIndex], formData.value);
                        this.events[eventIndex]['title'] = formData.controls.title.value;
                        this.events[eventIndex]['coupon'] = formData.controls.coupon.value;
                        this.events[eventIndex]['merchant'] = formData.controls.merchant.value;

                        var titletrim = this.events[eventIndex]['title'];
                        var titletrimArr = titletrim.split("<br>");
                        if (titletrimArr[1] == "undefined") {
                            titletrim = titletrimArr[0] + "<br>" + this.events[eventIndex]['merchant'].name;
                        }
                        this.events[eventIndex]['title'] = titletrim;

                        this.refresh.next(true);

                        break;
                    /**
                     * Delete
                     */
                    case 'clear':
                        debugger;
                        this.events[eventIndex]['title'] = formData.controls.title.value;
                        this.events[eventIndex]['coupon'] = {id:0};
                        this.events[eventIndex]['merchant'] = {id:0};

                        var titletrim = this.events[eventIndex]['title'];
                        var titletrimArr = titletrim.split("-");
                        // if (titletrimArr[1] == "undefined") {
                            titletrim = titletrimArr[0] + " - " + 0;
                        //}
                        this.events[eventIndex]['title'] = titletrim;

                        this.refresh.next(true);
                        // this.deleteEvent(event);

                        break;
                }
            });
    }

    getWeeklyData(selectedDay) {
        debugger;
        var selectedPageId = 0;
        var selectedSeasonalId = 0;
        if (this.selectedPage.pageType == "page") {
            selectedPageId = this.selectedPage.id;
        }
        else {
            selectedSeasonalId = this.selectedPage.id;
        }
        // selectedDay = startOfDay(selectedDay);
        this.scheduledPlacementService.getPlacements(selectedDay.date, selectedPageId, selectedSeasonalId);
    }
}


