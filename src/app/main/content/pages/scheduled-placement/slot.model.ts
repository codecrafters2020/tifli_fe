import { CalendarEventAction } from 'angular-calendar';
import { startOfDay, endOfDay } from 'date-fns';
import * as moment from 'moment';

export class SlotModel
{
    start: Date;
    end?: Date;
    title: string;
    color: {
        primary: string;
        secondary: string;
    };
    actions?: CalendarEventAction[];
    allDay?: boolean;
    cssClass?: string;
    resizable?: {
        beforeStart?: boolean;
        afterEnd?: boolean;
    };
    draggable?: boolean;
    meta?: {
        location: string,
        notes: string,
    };
    merchant: any ={};
    coupon: any = {};
    position:any;
    pageid: any;
    pageType: any;

    constructor(data?)
    {
        data = data || {};
        this.start = moment(data.start).local().toDate();
        this.end = moment(data.end).local().toDate();
        // this.start = new Date(data.start) || startOfDay(new Date());
        // this.end = new Date(data.end) || endOfDay(new Date());
        this.title = data.title || '';
        this.color = {
            primary  : data.color && data.color.primary || '#1e90ff',
            secondary: data.color && data.color.secondary || '#D1E8FF'
        };
        this.draggable = false;
        // this.resizable = {
        //     beforeStart: data.resizable && data.resizable.beforeStart || true,
        //     afterEnd   : data.resizable && data.resizable.afterEnd || true
        // };
        this.actions = data.actions || [];
        // this.allDay = data.allDay || false;
        // this.cssClass = data.cssClass || '';
        // this.meta = {
        //     location: data.meta && data.meta.location || '',
        //     notes   : data.meta && data.meta.notes || ''
        // };
        this.merchant = data.merchant ? data.merchant : {};
        this.coupon = data.coupon ? data.coupon : {};
        this.position = data.position ? data.position : '';
        this.pageid = data.pageid ? data.pageid : '';
        this.pageType = data.pageType ? data.pageType : '';
    }
}
