import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { FuseAngularMaterialModule } from '../../components/angular-material/angular-material.module';

import { FuseSharedModule } from '@fuse/shared.module';

const routes = [
    {
        path: 'list',
        loadChildren: './list-placement/list-home-event.module#ListHomeEventModule'
    }
];

@NgModule({
    imports: [
        FuseSharedModule,
        RouterModule.forChild(routes),
        FuseAngularMaterialModule
    ],
    declarations: []
})
export class ScheduledPlacementHomeEventModule {
}
