import { Input, Component, ElementRef, ChangeDetectionStrategy, ViewEncapsulation, OnInit, ViewChild, AfterViewInit, OnChanges } from '@angular/core';
import { MatPaginator, MatSort } from '@angular/material';
import { DataSource } from '@angular/cdk/collections';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';
import { MAT_DIALOG_DATA, MatDialogRef, MatDialog } from '@angular/material';
import { ListMerchantService } from '../list-merchant.service';
import { fuseAnimations } from '@fuse/animations';
import { FuseUtils } from '@fuse/utils';
import { FuseCalendarEventFormDialogComponent } from '../slot-form/slot-form.component';
import { DisplayGrid, GridsterConfig, GridsterItem, GridType } from 'angular-gridster2';
import { resetState } from 'sweetalert/typings/modules/state';
import { MatSnackBar } from '@angular/material';
import { HttpClient } from '@angular/common/http';
import { AppService } from '../../../../../../app.service';

@Component({
  selector: 'app-swap',
  templateUrl: './grid-merchant.component.html',
  //changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None
})
export class GridMerchantComponent implements OnInit {
  static item: BehaviorSubject<any> = new BehaviorSubject({});;
  slotsData: any[] = [];
  options: GridsterConfig;
  dashboard: Array<GridsterItem>;
  newdashboard: Array<GridsterItem> = [];
  dialogRef: any;
  swapCounter: any = 0;
  static swapChecker: any = 0;
  @Input() showLoadingBar: any;
  @Input() page: any;
  constructor(public ListMerchantService: ListMerchantService, public dialog: MatDialog, public snackBar: MatSnackBar, private http: HttpClient, private appService: AppService) {



  }

  static itemChange(item, ListMerchantService) {
    debugger;
    console.info(item);
    GridMerchantComponent.item.next(item);
  }


  ngOnInit() {
    this.ListMerchantService.onShowLoadingBarChanged.subscribe(data => {
      this.showLoadingBar = data;
    })

    GridMerchantComponent.item.subscribe(slotData => {
      debugger;
      if (slotData.hasOwnProperty('data')) {
        if (this.swapCounter == 2)
          this.swapCounter = 0;

        if (slotData.data.cid == null) {
          this.swapCounter++;
        }
        else {
          this.showLoadingBar = true;
          let date: any;
          if (slotData.x == 0)
            date = this.startDate;
          else if (slotData.x == 1)
            date = this.day2;
          else if (slotData.x == 2)
            date = this.day3;
          else if (slotData.x == 3)
            date = this.day4;
          else if (slotData.x == 4)
            date = this.day5;
          else if (slotData.x == 5)
            date = this.day6;
          else if (slotData.x == 6)
            date = this.endDate;


          date = new Date(date);
          var year = date.getFullYear();

          var month = (1 + date.getMonth()).toString();
          month = month.length > 1 ? month : '0' + month;

          var day = date.getDate().toString();
          day = day.length > 1 ? day : '0' + day;

          date = year + '-' + month + '-' + day;

          // let payload = {
          //   coupon: {
          //     id: slotData.data.cid
          //   },
          //   startDate: date,
          //   position: slotData.y + 1,
          //   seasonalId: this.page.id
          // };
          let payload = {
            seasonalEvent: {
                id: slotData.data.cid
            },
            startDate: date,
            position: slotData.y + 1
        };
          this.http.put(this.appService.placementService + 'schedule/home-nav-links/' + slotData.slotId, payload)
            .subscribe((response: any) => {
              debugger;
              if (response.meta.message == "success") {
                this.swapCounter++
                if (this.swapCounter === 2) {
                  this.swapCounter = 0;
                  this.ListMerchantService.getScheduledPlacement(this.page.id, this.ListMerchantService.startDate, this.ListMerchantService.endDate).then(response => {
                    this.showLoadingBar = false;
                    if (response.meta.message === "success") {
                      this.showLoadingBar = false;
                      this.snackBar.open(response.meta.message, "Slot Updated!", {
                        duration: 2000,
                      });
                    }
                  });
                }
              }
              else {
                this.showLoadingBar = false;
                this.snackBar.open(response.meta.message, "Slots Not Swaped!", {
                  duration: 2000,
                });
              }
            });
        }
      }
    });

    this.ListMerchantService.onSlotsDataChanged.subscribe(data => {
      this.slotsData = data;
      debugger;
      this.changeWeekDates();
      this.pagechange();
      this.ListMerchantService.showLoadingBar = false;
      this.ListMerchantService.onShowLoadingBarChanged.next(false);
    })


    this.options = {
      itemChangeCallback: GridMerchantComponent.itemChange,
      gridType: GridType.ScrollVertical,
      displayGrid: DisplayGrid.None,
      pushItems: false,
      swap: true,
      draggable: {
        enabled: true
      },
      resizable: {
        enabled: false
      },
      maxRows: 1
    };


    this.tempdate = new Date(this.ListMerchantService.startDate);
    debugger;
    this.startDate = new Date(this.tempdate);
    this.startDate.setDate(this.startDate.getDate() + 1);
    this.day2 = new Date(this.startDate).toString();
    this.startDate.setDate(this.startDate.getDate() + 1);
    this.day3 = new Date(this.startDate).toString();
    this.startDate.setDate(this.startDate.getDate() + 1);
    this.day4 = new Date(this.startDate).toString();
    this.startDate.setDate(this.startDate.getDate() + 1);
    this.day5 = new Date(this.startDate).toString();
    this.startDate.setDate(this.startDate.getDate() + 1);
    this.day6 = new Date(this.startDate).toString();

    this.startDate.setDate(this.startDate.getDate() + 1);
    this.endDate = new Date(this.ListMerchantService.endDate);
    this.startDate = this.tempdate;

  }

  formatDate(date) {
    var d = new Date(date),
      month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('-');
  }

  changeWeekDates() {
    this.tempdate = new Date(this.ListMerchantService.startDate);
    debugger;
    this.startDate = new Date(this.ListMerchantService.startDate);
    this.startDate.setDate(this.startDate.getDate() + 1);
    this.day2 = new Date(this.startDate).toString();
    this.startDate.setDate(this.startDate.getDate() + 1);
    this.day3 = new Date(this.startDate).toString();
    this.startDate.setDate(this.startDate.getDate() + 1);
    this.day4 = new Date(this.startDate).toString();
    this.startDate.setDate(this.startDate.getDate() + 1);
    this.day5 = new Date(this.startDate).toString();
    this.startDate.setDate(this.startDate.getDate() + 1);
    this.day6 = new Date(this.startDate).toString();

    this.startDate.setDate(this.startDate.getDate() + 1);
    this.endDate = new Date(this.ListMerchantService.endDate);
    this.startDate = this.tempdate;
  }

  pagechange() {


    this.newdashboard = [
      { cols: 1, rows: 1, y: 0, x: 0, slotId: 0, pageType: "", date: "", data: { cid: null, merchant: "" } },
      { cols: 1, rows: 1, y: 0, x: 1, slotId: 0, pageType: "", date: "", data: { cid: null, merchant: "" } },
      { cols: 1, rows: 1, y: 0, x: 2, slotId: 0, pageType: "", date: "", data: { cid: null, merchant: "" } },
      { cols: 1, rows: 1, y: 0, x: 3, slotId: 0, pageType: "", date: "", data: { cid: null, merchant: "" } },
      { cols: 1, rows: 1, y: 0, x: 4, slotId: 0, pageType: "", date: "", data: { cid: null, merchant: "" } },
      { cols: 1, rows: 1, y: 0, x: 5, slotId: 0, pageType: "", date: "", data: { cid: null, merchant: "" } },
      { cols: 1, rows: 1, y: 0, x: 6, slotId: 0, pageType: "", date: "", data: { cid: null, merchant: "" } }
    ];

    this.mapSlotsData();
    debugger;
    for (let i = 0; i < this.newdashboard.length; i++) {
      if (this.newdashboard[i].x == 0) {
        this.newdashboard[i].date = this.startDate;
      }
      if (this.newdashboard[i].x == 1) {
        this.newdashboard[i].date = this.day2;
      }
      if (this.newdashboard[i].x == 2) {
        this.newdashboard[i].date = this.day3;
      }
      if (this.newdashboard[i].x == 3) {
        this.newdashboard[i].date = this.day4;
      }
      if (this.newdashboard[i].x == 4) {
        this.newdashboard[i].date = this.day5;
      }
      if (this.newdashboard[i].x == 5) {
        this.newdashboard[i].date = this.day6;
      }
      if (this.newdashboard[i].x == 6) {
        this.newdashboard[i].date = this.endDate;
      }
      for (let j = 0; j < this.slotsData.length; j++) {



        if ((this.slotsData[j].position - 1 == this.newdashboard[i].y) && (this.slotsData[j].weekDay == this.newdashboard[i].x)) {
          // if (this.slotsData[j].hasOwnProperty('merchant')) {
            this.newdashboard[i].data.merchant = this.slotsData[j].seasonalEvent.name;
          // }
          // else {
            //this.newdashboard[i].data.merchant = 0;
          // }

          this.newdashboard[i].data.cid = this.slotsData[j].seasonalEvent.id;

          let date = new Date(this.slotsData[j].startDate);
          var userTimezoneOffset = date.getTimezoneOffset() * 60000;
          date = new Date(date.getTime() + userTimezoneOffset);
          this.newdashboard[i].date = date;
          // this.newdashboard[i].data.merchant = this.slotsData[j].coupon.merchant.id;
          // this.newdashboard[i].pageType = this.slotsData[j].pageId;
          this.newdashboard[i].y = this.slotsData[j].position - 1;
          this.newdashboard[i].slotId = this.slotsData[j].id;


        }
      }

    }


    this.dashboard = this.newdashboard;
    console.log(this.dashboard)

  }

  viewall() {
  }

  mapSlotsData() {

  }

  changedOptions() {
    if (this.options.api && this.options.api.optionsChanged) {
      this.options.api.optionsChanged();
    }
  }

  removeItem($event, item) {
    debugger;

    if (item.slotId !== 0) {
      this.showLoadingBar = true;
      $event.preventDefault();
      $event.stopPropagation();
      let nitem = { cols: 1, rows: 1, y: 0, x: 0, pageType: "", data: { cid: null, merchant: "" } }
      this.dashboard.splice(this.dashboard.indexOf(item), 1, nitem);

      this.ListMerchantService.deleteCarouselPlacement(item).then(response => {
        if (response.meta.message === "success") {
          this.ListMerchantService.getScheduledPlacement(this.page.id, this.ListMerchantService.startDate, this.ListMerchantService.endDate).then(response => {
            this.showLoadingBar = false;
            if (response.meta.message === "success") {
              this.snackBar.open(response.meta.message, "Slot Removed!", {
                duration: 2000,
              });
            }
          });
        }
        else {
          this.showLoadingBar = false;
          this.snackBar.open(response.meta.message, "Slot Not Removed!", {
            duration: 2000,
          });
        }
      });
    }
  }



  todaysdate = new Date();
  todaysday = this.todaysdate.getDay();
  tempdate;
  startDate;
  endDate;
  day2;
  day3;
  day4;
  day5;
  day6;

  editPosition(date, position, data) {
    this.dialogRef = this.dialog.open(FuseCalendarEventFormDialogComponent, {
      panelClass: 'event-form-dialog',
      data: {
        action: 'editPlacement',
        date: date,
        position: position,
        data: data
      }
    });

    this.dialogRef.afterClosed()
      .subscribe(response => {

      });
  }


  addDatePosition(date, position, data, slotId, action) {
    debugger;
    this.dialogRef = this.dialog.open(FuseCalendarEventFormDialogComponent, {
      panelClass: 'event-form-dialog',
      data: {
        // action: 'addDatePositionPlacement',
        action: action,
        date: date,
        position: position,
        data: data,
        slotId: slotId
      }
    });



    this.dialogRef.afterClosed()
      .subscribe(response => {
        debugger;
        if (response === undefined) {
          response = {
            action: 'cancel'
          }
        }
        this.showLoadingBar = true;
        var startDate: any = new Date()
        var endDate: any = new Date();
        endDate.setDate(endDate.getDate() + 6);
        startDate = this.ListMerchantService.getQueryFormattedDate(startDate);
        endDate = this.ListMerchantService.getQueryFormattedDate(endDate);
        if (response.action == "add") {
          this.ListMerchantService.addCarouselPlacement(response.slot, this.page).then(response => {
            debugger;
            if (response.meta.message === "success") {
              this.ListMerchantService.getScheduledPlacement(this.page.id, this.ListMerchantService.startDate, this.ListMerchantService.endDate).then(response => {
                this.showLoadingBar = false;
                if (response.meta.message === "success") {
                  this.snackBar.open(response.meta.message, "Slot Updated!", {
                    duration: 2000,
                  });
                }
              });
            }
            else {
              this.snackBar.open(response.meta.message, "Slot Not Updated!", {
                duration: 2000,
              });
            }
          });
        }
        else if (response.action == "edit") {
          this.ListMerchantService.updateCarouselPlacement(response.slot, this.page).then(response => {
            debugger;
            if (response.meta.message === "success") {
              this.ListMerchantService.getScheduledPlacement(this.page.id, this.ListMerchantService.startDate, this.ListMerchantService.endDate).then(response => {
                this.showLoadingBar = false;
                if (response.meta.message === "success") {
                  this.snackBar.open(response.meta.message, "Slot Updated!", {
                    duration: 2000,
                  });
                }
              });
            }
            else {
              this.snackBar.open(response.meta.message, "Slot Not Updated!", {
                duration: 2000,
              });
            }
          });
        }
        else if (response.action == "delete") {
          debugger;
          this.ListMerchantService.deleteCarouselPlacement(response.slot).then(response => {
            debugger;
            this.ListMerchantService.getFiles(startDate, endDate);
          });
        }
        else if (response.action == "cancel") {
          this.showLoadingBar = false;
        }
      });
  }


}



