import { Component, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { FuseCalendarEventFormDialogComponent } from './slot-form/slot-form.component';
import { CalendarEvent, CalendarEventAction, CalendarEventTimesChangedEvent, CalendarMonthViewDay, CalendarWeekViewComponent } from 'angular-calendar';

import { fuseAnimations } from '@fuse/animations';
import { MAT_DIALOG_DATA, MatDialogRef,MatDialog } from '@angular/material';
import { ListMerchantService } from './list-merchant.service';
import { Router } from '@angular/router';
import { AbstractControl, FormBuilder, FormGroup, Validators, FormsModule, FormControl } from '@angular/forms';


@Component({
    selector     : 'fuse-file-manager',
    templateUrl  : './list-merchant.component.html',
    styleUrls    : ['./list-merchant.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class ListMerchantComponent implements OnInit
{

    view: string;
    viewDate: Date;
    selected: any;
    pathArr: string[];
    dialogRef:any;
    selectedPage: any
    placementShedulingForm: FormGroup;
    showLoadingBar:Boolean = false;
    pages: any[] = [];
    
    @ViewChild(CalendarWeekViewComponent)
    weekView: CalendarWeekViewComponent;

    constructor(private ListMerchantService: ListMerchantService, private formBuilder: FormBuilder, private router: Router, public dialog: MatDialog)
    {
        // this.pages = ListMerchantService.pages;
        // this.view = 'week';
        // this.viewDate = new Date();
        // this.selectedPage = this.ListMerchantService.pages[0];
    }

    ngOnInit()
    {
        // this.ListMerchantService.onFileSelected.subscribe(selected => {
        //     this.selected = selected;
        //    // this.pathArr = selected.location.split('>');
        // });

        this.placementShedulingForm = this.formBuilder.group({
            selectPage: ['', Validators.required],
        });

        // this.placementShedulingForm.controls.selectPage.valueChanges
        // .subscribe(page=>{
        //     this.ListMerchantService.showLoadingBar = true;
        //     this.ListMerchantService.onShowLoadingBarChanged.next(true);
        //     this.ListMerchantService.getScheduledPlacement(page.id, null, null);

        // });

    }

    

    nextweek(){
        this.ListMerchantService.showLoadingBar = true;
        this.ListMerchantService.onShowLoadingBarChanged.next(true);
        var pend = this.ListMerchantService.endDate;
        debugger;
        var pstart=new Date(pend);
       pstart.setDate(pstart.getDate() +1);
       
        pend =new Date(pstart);
       pend.setDate(pend.getDate() +6);

        let pageid= this.ListMerchantService.pageid;
        console.log(pstart)
        console.log(pend)

       this.ListMerchantService.getScheduledPlacement(pageid, pstart, pend);
    }

    openUserAdd(){
        this.router.navigateByUrl("pages/merchant/add/");
    }

    previousweek(){
        this.ListMerchantService.showLoadingBar = true;
        this.ListMerchantService.onShowLoadingBarChanged.next(true);
        var pstart = this.ListMerchantService.startDate;
        
        var pend=new Date(pstart);
       pend.setDate(pend.getDate() -1);
       
        pstart =new Date(pend);
       pstart.setDate(pstart.getDate() -6);

        let pageid= this.ListMerchantService.pageid;
        console.log(pstart)
        console.log(pend)

       this.ListMerchantService.getScheduledPlacement(pageid, pstart, pend);
    }
    
}
