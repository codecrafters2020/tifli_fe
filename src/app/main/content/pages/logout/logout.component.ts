import { Component, OnInit, ViewEncapsulation } from '@angular/core';

import { fuseAnimations } from '@fuse/animations';

import { ListUserService } from './list-user.service';
import { Router } from '@angular/router';
import { AuthService } from '../../../../auth/auth.service';

@Component({
    selector     : 'fuse-file-manager',
    templateUrl  : './logout.component.html',
    styleUrls    : ['./logout.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class LogoutComponent implements OnInit
{
    selected: any;
    pathArr: string[];

    constructor(private ListUserService: ListUserService, private router: Router, private authService: AuthService)
    {
        debugger;
        this.authService.logout();
        this.router.navigateByUrl("/");
    }

    ngOnInit()
    {
        
    }
}
