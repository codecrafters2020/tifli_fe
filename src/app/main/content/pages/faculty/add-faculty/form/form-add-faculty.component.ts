import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators, FormsModule, FormControl, RequiredValidator } from '@angular/forms';
import { MatSnackBar } from '@angular/material';
import { AddFacultyService } from '../add-faculty.service';
import { Router } from '@angular/router';
import { FancyImageUploaderOptions, UploadedFile } from 'ng2-fancy-image-uploader';
import { getLocaleDateTimeFormat } from '@angular/common';
import { AppService } from '../../../../../../app.service';

@Component({
    selector: 'fuse-file-list',
    templateUrl: './form-add-faculty.component.html',
    styleUrls: ['./form-add-faculty.component.scss'],
})
export class FormAddFacultyComponent implements OnInit {
    
    //form: FormGroup;
    formErrors: any;
    test: any;
    step: number;

    // Horizontal Stepper
    horizontalStepperStep1: FormGroup;
    horizontalStepperStep2: FormGroup;
   

    horizontalStepperStep1Errors: any;
    horizontalStepperStep2Errors: any;
  
    default_choice={id: "-1", firstName: "None" ,userName: "none"}
    faculty={
        first_name: '',
        last_name: '',
        gender: 'Male',
        city: 'Karachi',
        country:'Pakistan',
        email:'',
        mobile:'',
        classteacher: false,
        role:3,
        password:"pakistan123",
        images :[],
         
      

    }


    displayedColumns = ['enable', 'name', 'description'];
    displayedColumnsPanelHeading = ['CategoryName', 'Description', 'isChecked'];


    countries = [
        { "name": "Afghanistan", "code": "AF" },
        { "name": "land Islands", "code": "AX" },
        { "name": "Pakistan", "code": "PK" },
       
      
    ];

    cities = [
        { "name": "Afghanistan", "code": "AF" },
        { "name": "Karachi", "code": "KHI" }
    ];
    genderlist = [
        { "name": "Male", "code": "M" },
        { "name": "Female", "code": "F" }
    ];
    

    constructor(private AddFacultyService: AddFacultyService, private formBuilder: FormBuilder, private router: Router, private snackBar: MatSnackBar, private appService: AppService) {

        this.horizontalStepperStep1Errors = {
            first_name: {},
            last_name: {},
            country: {},
            city: {},
            email: {},
            mobile: {}
           
           // barcodeFormat: {}
        };

    }

    ngOnInit() {

        // Horizontal Stepper form steps
        this.horizontalStepperStep1 = this.formBuilder.group({

            first_name: ['', [Validators.required]],
            last_name: ['', [Validators.required]],
            country: ['', Validators.required],
            city: ['', Validators.required],
            gender: [''],
            mobile: ['', Validators.required],
            email: ['', Validators.required],
            classteacher: [''],
           
        });

        this.horizontalStepperStep2 = this.formBuilder.group({

            // merchantLargeLogo: ['', Validators.required],
            // merchantSmallLogo: ['', Validators.required]
        });

      

      



        this.horizontalStepperStep1.valueChanges.subscribe(() => {
            this.onFormValuesChanged(this.horizontalStepperStep1, this.horizontalStepperStep1Errors);
        });

        this.horizontalStepperStep2.valueChanges.subscribe(() => {
            this.onFormValuesChanged(this.horizontalStepperStep2, this.horizontalStepperStep2Errors);
        });

    

    }

   
    onFormValuesChanged(horizontalStepperStep: FormGroup, horizontalStepperStepErrors: any) {
        for (const field in horizontalStepperStepErrors) {
            if (!horizontalStepperStepErrors.hasOwnProperty(field)) {
                continue;
            }
            horizontalStepperStepErrors[field] = {};

            const control = horizontalStepperStep.get(field);

            if (control && control.dirty && control.touched && !control.valid) {
                horizontalStepperStepErrors[field] = control.errors;
            }
        }
    }

    finishHorizontalStepper() {
        // alert('You have finished the horizontal stepper!');
        debugger;
       
        try{
        this.AddFacultyService.addfaculty(this.faculty).then(response => {
            debugger;
            this.test = response;
            console.log("response: " + JSON.stringify(response));
            debugger;
            if(response.status !== "200"){
                this.snackBar.open("Something went wrong", "Error", {
                    duration: 2000,
                });
            }
            this.snackBar.open("Sucess", "Done", {
                duration: 2000,
            });
            
                this.redirect('pages/faculty/list');
        });
        }
        catch(err){
            debugger;
        }
    }

    redirect(pagename: string) {
        this.router.navigate([pagename]);
    }

    onUploadLargeImage(file: UploadedFile) {
        debugger;
        var response: any = {};
        response = JSON.parse(file.response)
        if (response.meta.code == 200) {
            if (this.faculty.images.length >= 1) {
                for (let i = 0; i < this.faculty.images.length; i++) {
                    if(this.faculty.images[i].suffix=="large")
                    {
                        this.faculty.images.splice(i, 1);
                    }
                }
            }
            var largeImage: any = {};
            largeImage.fileName = response.data.fileName;
            largeImage.suffix = "large";
            this.faculty.images.push(largeImage);
        }
    }

   
}
