import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { AppService } from '../../../../../app.service';
import { AbstractControl } from '@angular/forms';

@Injectable()
export class AddFacultyService implements Resolve<any>
{

    categoriesList: any[];
    usersList: any[];
    barcodesList: any[];
    affiliateNetworksList: any[];
    merchantAffiliateStatusList: any[];
    merchantsList: any[];
    merchantFound: boolean = false;
    merchantTierList: any[];

    constructor(private http: HttpClient, private appService: AppService) {
    }

    /**
     * The File Manager App Main Resolver
     * 
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {

      
    }

    addfaculty(SmsUser): Promise<any> {
        return new Promise((resolve, reject) => {
            debugger;
            this.http.post(`${this.appService.adminService}faculties`, SmsUser)
                .subscribe((response: any) => {
                    resolve(response);
                }, reject);
        });
    }



}
