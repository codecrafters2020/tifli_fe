import { Component, OnInit } from '@angular/core';

import { fuseAnimations } from '@fuse/animations/index';

import { ListFacultyService } from '../../list-faculty.service';

@Component({
    selector   : 'fuse-file-manager-details-sidenav',
    templateUrl: './sidenav-faculty-details.component.html',
    styleUrls  : ['./sidenav-faculty-details.component.scss'],
    animations : fuseAnimations
})
export class SidenavFacultyDetailsComponent implements OnInit
{

    selected: any;

    constructor(private ListFacultyService: ListFacultyService)
    {

    }

    ngOnInit()
    {
        this.ListFacultyService.onFileSelected.subscribe(selected => {
            this.selected = selected;
        });
    }

}
