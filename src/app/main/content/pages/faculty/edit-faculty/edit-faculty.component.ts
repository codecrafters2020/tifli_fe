import { Component, OnInit, ViewEncapsulation } from '@angular/core';

import { fuseAnimations } from '@fuse/animations';

import { EditFacultyService } from './edit-faculty.service';

import { Router } from '@angular/router';

@Component({
    selector     : 'fuse-file-manager',
    templateUrl  : './edit-faculty.component.html',
    styleUrls    : ['./edit-faculty.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class EditFacultyComponent implements OnInit
{
    selected: any;
    pathArr: string[];

    constructor(private EditFacultyService: EditFacultyService, private router: Router)
    {
    }

    ngOnInit()
    {
        // this.AddMerchantService.onFileSelected.subscribe(selected => {
        //     this.selected = selected;
        //     this.pathArr = selected.location.split('>');
        // });
    }

    openFacultyList(){
        
        this.router.navigateByUrl("pages/faculty/list/");
    }
}
