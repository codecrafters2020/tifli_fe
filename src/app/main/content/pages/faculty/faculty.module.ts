import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { FuseAngularMaterialModule } from '../../components/angular-material/angular-material.module';

import { FuseSharedModule } from '@fuse/shared.module';

const routes = [
    {
        path        : 'list',
        loadChildren: './list-faculty/list-faculty.module#ListFacultyModule'
    },
    {
        path        : 'add',
        loadChildren: './add-faculty/add-faculty.module#AddFacultyModule'
    },
    {
        path        : 'edit/:id',
        loadChildren: './edit-faculty/edit-faculty.module#EditFacultyModule'
    }
];

@NgModule({
    imports     : [
        FuseSharedModule,
        RouterModule.forChild(routes),
        FuseAngularMaterialModule
    ],
    declarations: []
})
export class FacultyModule
{
}
