import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CdkTableModule } from '@angular/cdk/table';

import { MatAutocompleteModule, MatFormFieldModule, MatInputModule, MatSelectModule, MatStepperModule, MatButtonModule, MatIconModule, MatRippleModule, MatSidenavModule, MatSlideToggleModule, MatTableModule, MatMenuModule, MatPaginatorModule, MatSortModule } from '@angular/material';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';

import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatRadioModule} from '@angular/material/radio';
import { FuseSharedModule } from '@fuse/shared.module';

import { ListCommissionComponent } from './list-commission.component';
import { ListCommissionService } from './list-commission.service';
import { GridCommissionComponent } from './grid/grid-commission.component';
import { SidenavCommissionMainComponent } from './sidenavs/main/sidenav-commission-main.component';
import { SidenavCommissionDetailsComponent } from './sidenavs/details/sidenav-commission-details.component';

const routes: Routes = [
    {
        path     : '**',
        component: ListCommissionComponent,
        children : [],
        resolve  : {
            files: ListCommissionService
        }
    }
];

@NgModule({
    declarations: [
        ListCommissionComponent,
        GridCommissionComponent,
        SidenavCommissionMainComponent,
        SidenavCommissionDetailsComponent
    ],
    imports     : [
        RouterModule.forChild(routes),

        CdkTableModule,
        MatAutocompleteModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        MatStepperModule,
        MatButtonModule,
        MatIconModule,
        MatRippleModule,
        MatSlideToggleModule,
        MatSidenavModule,
        MatTableModule,
        MatMenuModule,
        MatPaginatorModule, 
        MatSortModule,
        MatProgressSpinnerModule,

        FuseSharedModule,
        MatDatepickerModule,
        MatCheckboxModule,
        MatRadioModule
    ],
    providers   : [
        ListCommissionService
    ]
})
export class ListCommissionModule
{
}
