import { Component, OnInit, ViewEncapsulation } from '@angular/core';

import { fuseAnimations } from '@fuse/animations';

import { ListCommissionService } from './list-commission.service';
import { Router } from '@angular/router';

@Component({
    selector     : 'fuse-file-manager',
    templateUrl  : './list-commission.component.html',
    styleUrls    : ['./list-commission.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class ListCommissionComponent implements OnInit
{
    selected: any;
    pathArr: string[];

    constructor(private ListCommissionService: ListCommissionService, private router: Router)
    {
    }

    ngOnInit()
    {
        this.ListCommissionService.onFileSelected.subscribe(selected => {
            this.selected = selected;
           // this.pathArr = selected.location.split('>');
        });
    }

    openCouponAdd()
    {
        this.router.navigateByUrl("pages/coupon/add");
    }

    resetGrid()
    {
        this.ListCommissionService.getCommissions();
    }
}
