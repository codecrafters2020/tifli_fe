import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';

import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of'
import 'rxjs/add/observable/forkJoin';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

import { AppService } from './../../../../../app.service';

import * as moment from 'moment';
import * as _ from 'lodash';

class range {
  start: number;
  end: number;
}


@Injectable()
export class ListCommissionService implements Resolve<any>
{
    commissions: any[];
    onFilesChanged: BehaviorSubject<any> = new BehaviorSubject({});
    onFileSelected: BehaviorSubject<any> = new BehaviorSubject({});

    constructor(private http: HttpClient, private appService: AppService) {
    }

    /**
     * The File Manager App Main Resolver
     * 
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {

        // return new Promise((resolve, reject) => {

        //     Promise.all([
        //         this.getCommissions()
        //     ]).then(
        //         ([coupons]) => {
        //             resolve();
        //         },
        //         reject
        //     );
        // });
    }

    getCommissions(): Promise<any> {
        return new Promise((resolve, reject) => {
            // this.http.get(this.appService.apiUrl + 'cms/api/users/')
            // this.http.get('http://104.42.179.33:8765/cms/api/coupons/coupons?page=0&size=20&sort=couponCode,asc&searchTerms=gag&couponCode=123&couponType=Deal')
            this.http.get(`${this.appService.reportService}consumer/commission?page=0&size=50&sorted=transactionId,asc`)
                .subscribe((response: any) => {
                    // debugger;
                    this.commissions = response.data.content;

                    this.onFilesChanged.next(response.data);
                    this.onFileSelected.next(response[0]);
                    resolve(response);
                }, reject);
        });
    }

    getAffiliateNetworks(): Promise<any> {
        return new Promise((resolve, reject) => {
            this.http.get(`${this.appService.merchantService}affiliates/`)
                .subscribe((response: any) => {
                    console.log(response);
                    response.data instanceof Array ? resolve(response.data) : resolve([]);
                }, reject);
        });
    }

    getAllAffiliateNetworkCommissionTypes(): Promise<any> {
        return new Promise((resolve, reject) => {
            this.http.get(`${this.appService.commissionService}type/all`)
                .subscribe((response: any) => {
                    response.data instanceof Array ? resolve(response.data) : resolve([]);
                }, reject);
        });
    }

    getAllAffiliateNetworkCommissionStatuses(): Promise<any> {
        return new Promise((resolve, reject) => {
            this.http.get(`${this.appService.commissionService}status/all`)
                .subscribe((response: any) => {
                    response.data instanceof Array ? resolve(response.data) : resolve([]);
                }, reject);
        });
    }

    getFilteredCommissions(
        page: number | string,
        size: number | string,
        sort: string,
        transactionId: string | number,
        affiliateOfferId: string | number,
        orderId: string | number,
        affiliateCommissionType: string,
        saleAmount: range,
        merchant: string,
        affiliate: string,
        commissionStatus: string,
        affiliateTrackingId: string | number,
        transactionDate: string,
        cashbackPercentage: range,
        cashbackStatus: string,
        cashbackAmount: range,
        commissionAmount: range,
        consumerName: string): any {
        let params = {
            page,
            size,
            sort,
            transactionId,
            affiliateOfferId,
            orderId,
            affiliateCommissionType,
            merchant,
            affiliate,
            commissionStatus,
            affiliateTrackingId,
            transactionDate,
            cashbackStatus,
            consumerName
        };

        let startRangeParams = {
            'saleAmount': saleAmount.start,
            'cashbackPercentage': cashbackPercentage.start,
            'cashbackAmount': cashbackAmount.start,
            'commissionAmount': commissionAmount.start,
        };

        let endRangeParams = {
            'saleAmount': saleAmount.end,
            'cashbackPercentage': cashbackPercentage.end,
            'cashbackAmount': cashbackAmount.end,
            'commissionAmount': commissionAmount.end,
        };

        let queryString = '';

        _.forEach(params, (val, key) => {
            if (val || typeof val === 'number') {
                queryString = queryString + key + '=' + val + '&';
            }
        });
        _.forEach(startRangeParams, (val, key) => {
            if (val || typeof val === 'number') {
                queryString = queryString + key + '=' + val + '&';
            }
        });

        _.forEach(endRangeParams, (val, key) => {
            if (val || typeof val === 'number') {
                queryString = queryString + key + '=' + val + '&';
            }
        });

        // console.log(queryString);

        return new Promise((resolve, reject) => {
            // this.http.get(`${this.appService.reportService}commission?sort=transactionId,asc&transactionId=16802544&orderId=13516041&saleAmount=0&cashbackPercentage=0&cashbackAmount=0&commissionAmount=0&saleAmount=500&cashbackPercentage=33&cashbackAmount=500&commissionAmount=500&`)
            this.http.get(`${this.appService.reportService}consumer/commission?${queryString}`)
                .subscribe((response: any) => {
                    this.commissions = response.data.content
                    this.onFilesChanged.next(response.data);
                    this.onFileSelected.next(response[0]);
                    resolve(response);
                }, reject);
        });
    }

    searchMerchantName(term: string): Observable<any> {
        return this.http.get(`${this.appService.merchantService}search/?name=${term}`)
            .map(response => response['data'])
            .map(data => data instanceof Array ? data : [])
    }

    searchAffiliateCommissionType(id: number): Observable<any> {
        return this.http.get(`${this.appService.commissionService}type/?affiliateId=${id}`)
            .map(response => response['data'])
            .map(data => data instanceof Object ? data : [])
    }

    searchAffiliateCommissionStatus(id: number): Observable<any> {
        return this.http.get(`${this.appService.commissionService}status/?affiliateId=${id}`)
            .map(response => response['data'])
            .map(data => data instanceof Object ? data : [])
    }

    searchAffiliateCommissionTypeAndStatus(id: number): Observable<any> {
        return Observable.forkJoin([
            this.http.get(`${this.appService.commissionService}type/?affiliateId=${id}`)
                .map(response => response['data'])
                .map(data => data instanceof Object ? data : []),
            this.http.get(`${this.appService.commissionService}status/?affiliateId=${id}`)
                .map(response => response['data'])
                .map(data => data instanceof Object ? data : [])
        ])
            .map((data: any[]) => {
                return data;
            });
    }



}
