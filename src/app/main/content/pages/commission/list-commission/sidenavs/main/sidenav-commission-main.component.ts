import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';
import { AbstractControl, FormBuilder, FormGroup, FormArray, Validators, EmailValidator } from '@angular/forms';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatRadioModule } from '@angular/material/radio';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { FormControl } from '@angular/forms';
import { Observable, BehaviorSubject } from 'rxjs';
import { of } from 'rxjs/observable/of';
import { map, startWith } from 'rxjs/operators';
import { FuseAngularMaterialModule } from '../../../../../components/angular-material/angular-material.module';

import { ListCommissionService } from '../../list-commission.service';


@Component({
  selector: 'fuse-file-manager-main-sidenav',
  templateUrl: './sidenav-commission-main.component.html',
  styleUrls: ['./sidenav-commission-main.component.scss']
})

export class SidenavCommissionMainComponent {
  @Input() leftSideNav: any;

  form: FormGroup;
  formErrors: any;


  affiliateCommissionTypeList: any[];
  merchantSearchResult: any[];
  affiliateNetworkSearchResult: any[];
  affiliateCommissionStatusList: any[];
  cashbackStatusList = ['Pending', 'Approved', 'Declined'];
  saleAmountList = [
    {start: 0, end: 500},
    {start: 501, end: 1000},
    {start: 1001, end: 1500},
  ];
  cashbackPercentageList = [
    {start: 0, end: 33},
    {start: 33, end: 66},
    {start: 55, end: 100},
  ];
  cashbackAmountList = [
    {start: 0, end: 500},
    {start: 501, end: 1000},
    {start: 1001, end: 1500},
  ];
  commissionAmountList = [
    {start: 0, end: 500},
    {start: 501, end: 1000},
    {start: 1001, end: 1500},
  ];
  consumerNameSearchResult: any[];


  onGoingOffers: boolean;
  exclusiveOffers: boolean;
  expiringSoon: boolean;
  popularOffers: boolean;

  enableOngoingOffers: boolean = false;
  enableExclusiveOffers: boolean = false;
  enableExpiringSoon: boolean = false;
  enablePopularOffers: boolean = false;

  btnDisabled: boolean = false;

  constructor(private ListCommissionService: ListCommissionService,
    private router: Router,
    private formBuilder: FormBuilder) {

    // Reactive Form
    this.form = this.formBuilder.group({
      transactionID: ['',],
      affiliateOfferID: ['',],
      orderID: ['',],
      affiliateCommissionType: ['',],
      saleAmount: ['',],
      commissionAmount: ['',],
      merchantAffiliateName: ['',],
      affiliateNetwork: [''],
      affiliateCommissionStatus: ['',],
      affiliateTrackingID: ['',],
      transactionDate: ['',],
      cashbackPercentage: ['',],
      cashbackAmount: ['',],
      cashbackStatus: ['',],
      consumerName: ['',]
    });

  }

  filter() {
    // this.btnDisabled = true;
    this.ListCommissionService.getFilteredCommissions(
      '',
      '',
      'transactionId,asc',
      this.form.controls.transactionID.value,
      this.form.controls.affiliateOfferID.value,
      this.form.controls.orderID.value,
      this.form.controls.affiliateCommissionType.value,
      this.form.controls.saleAmount.value,
      this.form.controls.merchantAffiliateName.value,
      this.form.controls.affiliateNetwork.value.name,
      this.form.controls.affiliateCommissionStatus.value,
      this.form.controls.affiliateTrackingID.value,
      this.form.controls.transactionDate.value,
      this.form.controls.cashbackPercentage.value,
      this.form.controls.cashbackStatus.value,
      this.form.controls.cashbackAmount.value,
      this.form.controls.commissionAmount.value,
      this.form.controls.consumerName.value)
      .then((response) => {
        this.btnDisabled = false;
        this.leftSideNav.toggle()
      })
  }

  ngOnInit() {

      Promise.all([
        this.ListCommissionService.getAllAffiliateNetworkCommissionTypes(),
        this.ListCommissionService.getAllAffiliateNetworkCommissionStatuses(),
        this.ListCommissionService.getAffiliateNetworks()
        ]).then(([affiliateTypes, affiliateStatuses, affiliateNetworks]) => {
          console.log('as')
          // console.log(affiliateTypes, affiliateStatuses, affiliateNetworks)
          this.affiliateCommissionTypeList = affiliateTypes;
          this.affiliateCommissionStatusList = affiliateStatuses;
          this.affiliateNetworkSearchResult = affiliateNetworks;
        })

    this.form.valueChanges.subscribe(() => {
      // console.log(this.form.value);
    });

    this.form.controls.merchantAffiliateName.valueChanges
      .debounceTime(400)
      .subscribe(searchTerm => {
        this.ListCommissionService.searchMerchantName(searchTerm)
          .subscribe(response => {
            this.merchantSearchResult = response;
          });
      });

    this.form.controls.affiliateNetwork.valueChanges
      .debounceTime(400)
      .subscribe(searchTerm => {
        this.ListCommissionService.searchAffiliateCommissionTypeAndStatus(this.form.controls.affiliateNetwork.value.id)
          .subscribe(response => {
            let types = Object.keys(response[0]);
            let status = Object.keys(response[1]);
            this.affiliateCommissionTypeList = types;
            this.affiliateCommissionStatusList = status;
          });
      });

    this.form.controls.cashbackPercentage.valueChanges
      .subscribe(searchTerm => {
        parseInt(searchTerm) > 100 ? this.form.controls.cashbackPercentage.setValue('') : searchTerm;
      });

  }

  displaySelectedName(item: any) {
    return item;
  }
}


