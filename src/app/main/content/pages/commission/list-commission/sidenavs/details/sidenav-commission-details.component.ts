import { Component, OnInit } from '@angular/core';

import { fuseAnimations } from '@fuse/animations/index';

import { ListCommissionService } from '../../list-commission.service';

@Component({
    selector   : 'fuse-file-manager-details-sidenav',
    templateUrl: './sidenav-commission-details.component.html',
    styleUrls  : ['./sidenav-commission-details.component.scss'],
    animations : fuseAnimations
})
export class SidenavCommissionDetailsComponent implements OnInit
{

    selected: any;

    constructor(private ListCommissionService: ListCommissionService)
    {

    }

    ngOnInit()
    {
        this.ListCommissionService.onFileSelected.subscribe(selected => {
            this.selected = selected;
        });
    }

}
