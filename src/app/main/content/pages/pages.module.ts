import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { LoginModule } from './authentication/login/login.module';
import { Login2Module } from './authentication/login-2/login-2.module';
import { RegisterModule } from './authentication/register/register.module';
import { Register2Module } from './authentication/register-2/register-2.module';
import { ForgotPasswordModule } from './authentication/forgot-password/forgot-password.module';
import { ForgotPassword2Module } from './authentication/forgot-password-2/forgot-password-2.module';
import { ResetPasswordModule } from './authentication/reset-password/reset-password.module';
import { ResetPassword2Module } from './authentication/reset-password-2/reset-password-2.module';
import { LockModule } from './authentication/lock/lock.module';
import { MailConfirmModule } from './authentication/mail-confirm/mail-confirm.module';
import { ComingSoonModule } from './coming-soon/coming-soon.module';
import { Error404Module } from './errors/404/error-404.module';
import { Error500Module } from './errors/500/error-500.module';
import { InvoiceCompactModule } from './invoices/compact/compact.module';
import { InvoiceModernModule } from './invoices/modern/modern.module';
import { MaintenanceModule } from './maintenance/maintenence.module';
import { PricingModule } from './pricing/pricing.module';
import { ProfileModule } from './profile/profile.module';
import { SearchModule } from './search/search.module';
import { FaqModule } from './faq/faq.module';
import { KnowledgeBaseModule } from './knowledge-base/knowledge-base.module';
//import { FuseFileManagerModule } from './file-manager/file-manager.module';
import { AdministrationModule } from './administration/administration.module';
import { AffiliateNetworkModule } from './affiliate-network/affiliate-network.module';
import { MerchantModule } from './merchant/merchant.module';
import { MerchantCategoryModule } from './merchant-category/merchant-category.module';
import { MerchantFeaturedModule } from './merchant-featured/merchant-featured.module';
import { ScheduledPlacementModule } from './scheduled-placement/scheduled-placement.module';
import { CarouselPlacementModule } from './carousel/list-carousel.module';
import { SeasonalModule } from './seasonal-events/seasonal-events.module'
import { PaidPlacementsModule } from './paid-placements/paid-placements.module'
import {CustomerCashbacksModule} from './customer-cashbacks/customer-cashbacks.module'
import {MatTooltipModule} from '@angular/material/tooltip';

import { AuthGuard } from '../../../auth/auth.guard'

const routes = [
    // {
    //     path        : 'file-manager',
    //     loadChildren: './file-manager/file-manager.module#FuseFileManagerModule'
    // },
    {
        path        : 'merchant',
        loadChildren: './merchant/merchant.module#MerchantModule',
        // canActivateChild: [AuthGuard]

    },
    {
        path        : 'merchant-category',
        loadChildren: './merchant-category/merchant-category.module#MerchantCategoryModule',
        // canActivateChild: [AuthGuard]
    },
    {
        path        : 'administration',
        loadChildren: './administration/administration.module#AdministrationModule',
       // canActivateChild: [AuthGuard]
    },
    {
        path        : 'affiliate-network',
        loadChildren: './affiliate-network/affiliate-network.module#AffiliateNetworkModule',
        // canActivateChild: [AuthGuard]
    },
    {
        path        : 'coupon',
        loadChildren: './coupon/coupon.module#CouponModule',
        // canActivateChild: [AuthGuard]
    },
    {
        path        : 'seasonal-events',
        loadChildren: './seasonal-events/seasonal-events.module#SeasonalModule',
        // canActivateChild: [AuthGuard]
    },
    {
        path        : 'featured-merchants',
        loadChildren: './merchant-featured/merchant-featured.module#MerchantFeaturedModule',
        // canActivateChild: [AuthGuard]
    },
    {
        path        : 'paid-placements',
        loadChildren: './paid-placements/paid-placements.module#PaidPlacementsModule',
        // canActivateChild: [AuthGuard]
    },
    {
        path        : 'scheduled-placements',
        loadChildren: './scheduled-placement/scheduled-placement.module#ScheduledPlacementModule',
        // canActivateChild: [AuthGuard]
    },
    {
        path        : 'carousel',
        loadChildren: './carousel/list-carousel.module#CarouselPlacementModule'
    },
    {
        path        : 'commission',
        loadChildren: './commission/commission.module#CommissionModule'
    },
    {
        path        : 'customer-cashbacks',
        loadChildren: './customer-cashbacks/customer-cashbacks.module#CustomerCashbacksModule'
    },
    {
        path        : 'customer-cashback-history',
        loadChildren: './customer-cashback-history/customer-cashback-history.module#CustomerCashbackHistoryModule'
    },
    {
        path        : 'payout-requests',
        loadChildren: './payout-requests/payout-requests.module#PayoutRequestsModule'
    },
    {
        path        : 'sync-commission',
        loadChildren: './sync-commission/sync-commission.module#SyncCommissionModule'
    },
    {
        path        : 'account/logout',
        loadChildren: './logout/logout.module#LogoutModule'
    },
{
        path        : 'featured-coupon',
        loadChildren: './featured-coupon/featured-coupon.module#FeaturedCouponModule'
       // canActivateChild: [AuthGuard]
    },
    {
        path        : 'merchant-coupons',
        loadChildren: './merchant-coupons/merchant-coupons.module#MerchantCouponsModule'
    },
{
        path        : 'scheduled-placement-pages',
        loadChildren: './scheduled-placement-pages/placement.module#PlacementModule',
        // canActivateChild: [AuthGuard]

    },
    {
        path        : 'scheduled-placement-top-stores',
        loadChildren: './scheduled-placement-top-stores/topstores.module#TopStoresModule',
        // canActivateChild: [AuthGuard]

    },
    {
        path        : 'scheduled-placement-popular-stores',
        loadChildren: './scheduled-placement-popular-stores/popularstores.module#PopularStoresModule',
        // canActivateChild: [AuthGuard]

    },
    {
        path        : 'scheduled-placement-search-merchant-links',
        loadChildren: './scheduled-placement-search-merchants/searchmerchants.module#SearchMerchantsModule',
        // canActivateChild: [AuthGuard]

    },
    {
        path        : 'scheduled-placement-seasonal',
        loadChildren: './scheduled-placement-seasonal/scheduled-placement-seasonal.module#ScheduledPlacementSeasonalModule'
    },
    {
        path        : 'scheduled-placement-upcoming-sales',
        loadChildren: './scheduled-placement-upcoming-sales/scheduled-placement-upcoming-sales.module#ScheduledPlacementUpcomingSalesModule'
    },
    {
        path        : 'scheduled-placement-home-event',
        loadChildren: './scheduled-placement-home-event/scheduled-placement-home-event.module#ScheduledPlacementHomeEventModule'
    },
    {
        path        : 'merchant-review',
        loadChildren: './merchant-review/merchant-review.module#MerchantReviewModule'
    },
    {
        path        : 'faculty',
        loadChildren: './faculty/faculty.module#FacultyModule',
        // canActivateChild: [AuthGuard]
    },
    {
        path        : 'student',
        loadChildren: './student/student.module#StudentModule',
        // canActivateChild: [AuthGuard]
    }
];

@NgModule({
    imports: [
        // Auth
        LoginModule,
        MatTooltipModule,
        Login2Module,
        RegisterModule,
        Register2Module,
        ForgotPasswordModule,
        ForgotPassword2Module,
        ResetPasswordModule,
        ResetPassword2Module,
        LockModule,
        MailConfirmModule,

        // Coming-soon
        ComingSoonModule,

        // Errors
        Error404Module,
        Error500Module,

        // Invoices
        InvoiceModernModule,
        InvoiceCompactModule,

        // Maintenance
        MaintenanceModule,

        // Pricing
        PricingModule,

        // Profile
        ProfileModule,

        // Search
        SearchModule,

        // Faq
        FaqModule,

        // Knowledge base
        KnowledgeBaseModule,

        // Seasonal Events
        // SeasonalModule,

        RouterModule.forChild(routes),

        // MerchantModule,
        // AdministrationModule,
        // PaidPlacementsModule        
    ]
})
export class FusePagesModule
{

}
