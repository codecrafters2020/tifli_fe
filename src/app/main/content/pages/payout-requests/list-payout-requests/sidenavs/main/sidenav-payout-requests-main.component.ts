import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';
import { AbstractControl, FormBuilder, FormGroup, FormArray, Validators, EmailValidator } from '@angular/forms';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatRadioModule } from '@angular/material/radio';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { FormControl } from '@angular/forms';
import { Observable, BehaviorSubject } from 'rxjs';
import { of } from 'rxjs/observable/of';
import { map, startWith } from 'rxjs/operators';
import { FuseAngularMaterialModule } from '../../../../../components/angular-material/angular-material.module';

import { ListPayoutRequestsService } from '../../list-payout-request.service';


@Component({
  selector: 'fuse-file-manager-main-sidenav',
  templateUrl: './sidenav-payout-requests-main.component.html',
  styleUrls: ['./sidenav-payout-requests-main.component.scss']
})

export class SidenavPayoutRequestsMainComponent {
  @Input() leftSideNav: any;

  form: FormGroup;
  formErrors: any;

  customerSearchResult: any[];

  couponTypes = ['Deal', 'In-store', 'Code'];
  couponStatuses = ['Active', 'Inactive'];

  onGoingOffers: boolean;
  exclusiveOffers: boolean;
  expiringSoon: boolean;
  popularOffers: boolean;

  enableOngoingOffers: boolean = false;
  enableExclusiveOffers: boolean = false;
  enableExpiringSoon: boolean = false;
  enablePopularOffers: boolean = false;

  pauoutRequestStatusList: any[] = [
    "Approved",
    "Declined",
    "Pending",
    "Droped"  
  ];

  btnDisabled: boolean = false;

  constructor(private ListPayoutRequestsService: ListPayoutRequestsService,
    private router: Router,
    private formBuilder: FormBuilder) {
    // Reactive Form
    this.form = this.formBuilder.group({
      requestId: ['',],
      customerId: ['',],
      customerName: ['',],
      requestAmount: ['',],
      requestStatus: ['',],
    });

    // Reactive form errors
    this.formErrors = {

    };
  }

  filter() {
    // this.btnDisabled = true;
    this.ListPayoutRequestsService.getFilteredCustomerCashbacks(
      '',
      '',
      // 'couponCode,asc',
      this.form.controls.requestId.value,
      this.form.controls.customerId.value,
      this.form.controls.customerName.value,
      this.form.controls.requestAmount.value,
      this.form.controls.requestStatus.value
    )
      .then((response) => {
        this.btnDisabled = false;
        this.leftSideNav.toggle()
      })
  }

  ngOnInit() {
    this.form.valueChanges.subscribe(() => {
      // console.log(this.form.value);
    });

    this.form.controls.customerName.valueChanges
      .debounceTime(400)
      .subscribe(searchTerm => {
        this.ListPayoutRequestsService.searchCustomer(searchTerm)
          .subscribe(response => {
            this.customerSearchResult = response;
          });
      });

    this.form.controls.customerEmail.valueChanges
      .debounceTime(400)
      .subscribe(searchTerm => {
        this.ListPayoutRequestsService.searchCustomer(searchTerm)
          .subscribe(response => {
            this.customerSearchResult = response;
          });
      });

  }

  displaySelectedName(item: any) {
    return item;
  }
}


