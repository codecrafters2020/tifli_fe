import { Component, OnInit } from '@angular/core';

import { fuseAnimations } from '@fuse/animations/index';

import { ListPayoutRequestsService } from '../../list-payout-request.service';

@Component({
    selector   : 'fuse-file-manager-details-sidenav',
    templateUrl: './sidenav-payout-requests-details.component.html',
    styleUrls  : ['./sidenav-payout-requests-details.component.scss'],
    animations : fuseAnimations
})
export class SidenavPayoutRequestsDetailsComponent implements OnInit
{

    selected: any;

    constructor(private ListPayoutRequestsService: ListPayoutRequestsService)
    {

    }

    ngOnInit()
    {
        this.ListPayoutRequestsService.onFileSelected.subscribe(selected => {
            this.selected = selected;
        });
    }

}
