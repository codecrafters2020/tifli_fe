import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';

import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of'
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { AppService } from './../../../../../app.service';

import * as moment from 'moment';
import * as _ from 'lodash';

@Injectable()
export class ListPayoutRequestsService implements Resolve<any>
{
    coupons: any[];
    customerCashbacks: any[];
    onFilesChanged: BehaviorSubject<any> = new BehaviorSubject({});
    onFileSelected: BehaviorSubject<any> = new BehaviorSubject({});

    constructor(private http: HttpClient, private appService: AppService) {
    }

    /**
     * The File Manager App Main Resolver
     * 
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {

        // return new Promise((resolve, reject) => {

        //     Promise.all([
        //         this.getCustomerCashbacks()
        //     ]).then(
        //         ([customerCashbacks]) => {
        //             resolve();
        //         },
        //         reject
        //     );
        // });
    }

    getCustomerCashbacks(): Promise<any> {
        return new Promise((resolve, reject) => {
            // this.http.get(this.appService.apiUrl + 'cms/api/users/')
            // this.http.get('http://104.42.179.33:8765/cms/api/coupons/coupons?page=0&size=20&sort=couponCode,asc&searchTerms=gag&couponCode=123&couponType=Deal')
            this.http.get(this.appService.reportService + 'payout/filters/')
                .subscribe((response: any) => {
                    debugger;
                    // response.data.content= [
                    //     {
                    //         reqId: 1,
                    //         customerId: 'Usman Irfan',
                    //         customerName: 'mirfan',
                    //         reqAmount: 4000,
                    //         reqstatus: 'Approved',
                    //         reqDate: '2018-06-03T00:01:00.0000Z',
                    //         approvalDate: '2018-06-03T00:01:00.0000Z',
                    //         approvedBy: 'Muhammad Usman',
                    //         declinedDate: null,
                    //         declinedBy: null,
                    //         dropDate: null,
                    //     },
                    //     {
                    //         reqId: 1,
                    //         customerId: 'Usman Irfan',
                    //         customerName: 'mirfan',
                    //         reqAmount: 4000,
                    //         reqstatus: 'Declined',
                    //         reqDate: '2018-06-03T00:01:00.0000Z',
                    //         approvalDate: null,
                    //         approvedBy: null,
                    //         declinedDate: '2018-06-03T00:01:00.0000Z',
                    //         declinedBy: 'Muhammad Usman',
                    //         dropDate: null,
                    //     }
                        // {
                        //     reqId: 1,
                        //     customerId: 'Usman Irfan',
                        //     customerName: 'mirfan',
                        //     reqAmount: 4000,
                        //     reqstatus: 'Declined',
                        //     reqDate: '2018-06-03T00:01:00.0000Z',
                        //     approvalDate: '-',
                        //     approvedBy: '-',
                        //     declinedDate: '2018-06-03T00:01:00.0000Z',
                        //     declinedBy: 'Muhammad Usman',
                        //     dropDate: '-',
                        // },
                        // {
                        //     reqId: 1,
                        //     customerId: 'Usman Irfan',
                        //     customerName: 'mirfan',
                        //     reqAmount: 4000,
                        //     reqstatus: 'Approved',
                        //     reqDate: '2018-06-03T00:01:00.0000Z',
                        //     approvalDate: '2018-06-03T00:01:00.0000Z',
                        //     approvedBy: 'Ahmad Rahman',
                        //     declinedDate: '-',
                        //     declinedBy: '-',
                        //     dropDate: '-',
                        // },
                        // {
                        //     reqId: 1,
                        //     customerId: 'Usman Irfan',
                        //     customerName: 'mirfan',
                        //     reqAmount: 4000,
                        //     reqstatus: 'Approved',
                        //     reqDate: '2018-06-03T00:01:00.0000Z',
                        //     approvalDate: '2018-06-03T00:01:00.0000Z',
                        //     approvedBy: 'Muhammad Usman',
                        //     declinedDate: '-',
                        //     declinedBy: '-',
                        //     dropDate: '-',
                        // },
                        // {
                        //     reqId: 1,
                        //     customerId: 'Usman Irfan',
                        //     customerName: 'mirfan',
                        //     reqAmount: 4000,
                        //     reqstatus: 'Approved',
                        //     reqDate: '2018-06-03T00:01:00.0000Z',
                        //     approvalDate: '2018-06-03T00:01:00.0000Z',
                        //     approvedBy: 'Muhammad Usman',
                        //     declinedDate: '-',
                        //     declinedBy: '-',
                        //     dropDate: '-',
                        // }
                    // ];
                    this.customerCashbacks = response.data.content;
                    // for (let i = 0; i < this.coupons.length; i++) {
                    //     this.coupons[i].endDate = moment(this.coupons[i].endDate).local().format("MMM DD, YYYY");
                    //     this.coupons[i].startDate = moment(this.coupons[i].startDate).local().format("MMM DD, YYYY");
                    // }
                    this.onFilesChanged.next(response.data);
                    this.onFileSelected.next(response[0]);
                    resolve(response);
                }, reject);
        });
    }


    getMerchants(): Promise<any> {
        return new Promise((resolve, reject) => {
            this.http.get(`${this.appService.merchantService}`)
                .subscribe((response: any) => {
                    this.coupons = response.data;
                    this.onFilesChanged.next(response.data);
                    this.onFileSelected.next(response[0]);
                    resolve(response);
                }, reject);
        });
    }

    getFilteredCustomerCashbacks(page: number | string,
        size: number | string,
        //sort: string,
        reqId: string | number,
        customerId: string | number,
        customerName: string,
        reqAmount: string,
        reqStatus: number
        ): Promise<any> {
        let params = {
            page,
            size,
            // sort,
            reqId,
            customerId,
            customerName,
            reqAmount,
            reqStatus
        };

        let queryString = '';
        _.forEach(params, (val, key) => {
            if (val || typeof val === 'number') {
                queryString = queryString + key + '=' + val + '&';
            }
        });

        return new Promise((resolve, reject) => {
            // this.http.get('api/coupon-list')
            // this.http.get(`http://104.42.179.33:8765/cms/api/coupons?page=0&size=20&sort=couponCode,asc&searchTerms=gag&couponCode=123&couponType=Deal`)
            this.http.get(`${this.appService.reportService}payout/filters/?${queryString}`)
                .subscribe((response: any) => {
                    this.customerCashbacks = response.data.content;
                    this.onFilesChanged.next(response.data.content);
                    this.onFileSelected.next(response[0]);
                    resolve(response);
                }, reject);
        });
    }

    // searchCustomerName(term: string): Observable<any> {
    //     return this.http.get(`${this.appService.merchantService}search/?name=${term}`)
    //         .map(response => response['data'])
    //         .map(data => data instanceof Array ? data : [])
    // }

    searchCustomer(term: string): Observable<any> {
        return this.http.get(`${this.appService.merchantService}search/?name=${term}`)
            .map(response => response['data'])
            .map(data => data instanceof Array ? data : [])
    }

    updateRequestStatus(status: string, reqId:any): Promise<any>{
        return new Promise((resolve, reject) => {
            this.http.put(this.appService.reportService + 'payout/updateStatus?id='+ reqId +'&status='+ status,{})
                .subscribe((response: any) => {
                    debugger;
                    resolve(response);
                }, reject);
        });
    }

}
