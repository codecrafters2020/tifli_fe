import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CdkTableModule } from '@angular/cdk/table';

import { MatAutocompleteModule, MatFormFieldModule, MatInputModule, MatSelectModule, MatStepperModule, MatButtonModule, MatIconModule, MatRippleModule, MatSidenavModule, MatSlideToggleModule, MatTableModule, MatMenuModule, MatPaginatorModule, MatSortModule } from '@angular/material';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';

import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatRadioModule} from '@angular/material/radio';
import { FuseSharedModule } from '@fuse/shared.module';

import { ListPayoutRequestsComponent } from './list-payout-request.component';
import { ListPayoutRequestsService } from './list-payout-request.service';
import { GridPayoutRequestsComponent } from './grid/grid-payout-requests.component';
import { SidenavPayoutRequestsMainComponent } from './sidenavs/main/sidenav-payout-requests-main.component';
import { SidenavPayoutRequestsDetailsComponent } from './sidenavs/details/sidenav-payout-requests-details.component';

const routes: Routes = [
    {
        path     : '**',
        component: ListPayoutRequestsComponent,
        children : [],
        resolve  : {
            files: ListPayoutRequestsService
        }
    }
];

@NgModule({
    declarations: [
        ListPayoutRequestsComponent,
        GridPayoutRequestsComponent,
        SidenavPayoutRequestsMainComponent,
        SidenavPayoutRequestsDetailsComponent
    ],
    imports     : [
        RouterModule.forChild(routes),

        CdkTableModule,
        MatAutocompleteModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        MatStepperModule,
        MatButtonModule,
        MatIconModule,
        MatRippleModule,
        MatSlideToggleModule,
        MatSidenavModule,
        MatTableModule,
        MatMenuModule,
        MatPaginatorModule, 
        MatSortModule,
        MatProgressSpinnerModule,

        FuseSharedModule,
        MatDatepickerModule,
        MatCheckboxModule,
        MatRadioModule
    ],
    providers   : [
        ListPayoutRequestsService
    ]
})
export class ListPayoutRequestsModule
{
}
