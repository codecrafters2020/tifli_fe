import { Component, OnInit, ViewEncapsulation } from '@angular/core';

import { fuseAnimations } from '@fuse/animations';

import { ListPayoutRequestsService } from './list-payout-request.service';
import { Router } from '@angular/router';

@Component({
    selector     : 'fuse-file-manager',
    templateUrl  : './list-payout-request.component.html',
    styleUrls    : ['./list-payout-request.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class ListPayoutRequestsComponent implements OnInit
{
    selected: any;
    pathArr: string[];

    constructor(private ListPayoutRequestsService: ListPayoutRequestsService, private router: Router)
    {
    }

    ngOnInit()
    {
        this.ListPayoutRequestsService.onFileSelected.subscribe(selected => {
            this.selected = selected;
           // this.pathArr = selected.location.split('>');
        });
    }

    openCouponAdd()
    {
        this.router.navigateByUrl("pages/coupon/add");
    }

    resetGrid()
    {
        this.ListPayoutRequestsService.getCustomerCashbacks();
    }
}
