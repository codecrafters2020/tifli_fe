import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';

import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { AppService } from './../../../../../app.service';
import { AuthService } from '../../../../../auth/auth.service';

@Injectable()
export class ListMerchantReviewAssignmentService implements Resolve<any>
{
    users: any[];
    merchantsList: any[];
    onFilesChanged: BehaviorSubject<any> = new BehaviorSubject({});
    onFileSelected: BehaviorSubject<any> = new BehaviorSubject({});

    constructor(private http: HttpClient,private appService: AppService,private authService:AuthService)
    {
    }

    /**
     * The File Manager App Main Resolver
     * 
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any
    {

        // return new Promise((resolve, reject) => {

        //     Promise.all([
        //         this.getMerchants(),
        //         this.getUsers()
        //     ]).then(
        //         ([files,users]) => {
        //             resolve();
        //         },
        //         reject
        //     );
        // });
    }

    getMerchants(): Promise<any>
    {
        return new Promise((resolve, reject) => {
            var currUser = this.authService.getUser();
            this.http.get(this.appService.merchantService + 'merchant/manager/'+currUser.id)
                .subscribe((response: any) => {
                    debugger;
                    this.merchantsList = response.data;
                    this.onFilesChanged.next(response.data);
                    this.onFileSelected.next(response[0]);
                    resolve(response);
                }, reject);
        });
    }

    getUsers(): Promise<any>
    {
        return new Promise((resolve, reject) => {
            this.http.get(this.appService.userService + 'users/')
                .subscribe((response: any) => {
                    this.users = response.data;
                    this.onFilesChanged.next(response.data);
                    this.onFileSelected.next(response[0]);
                    resolve(response);
                }, reject);
        });
    }

    assignReview(merchantId,usersList): Promise<any>
    {
        return new Promise((resolve, reject) => {
            this.http.post(this.appService.merchantService+'review/merchant/' + merchantId, usersList)
                .subscribe((response: any) => {
                    debugger;
                    resolve(response);
                }, reject);
        });
    }

    clearAssignments(merchantIds): Promise<any>
    {
        return new Promise((resolve, reject) => {
            this.http.post(this.appService.merchantService+'review/reset/' , merchantIds)
                .subscribe((response: any) => {
                    debugger;
                    resolve(response);
                }, reject);
        });
    }


}
