import { Component, OnInit, ViewEncapsulation } from '@angular/core';

import { fuseAnimations } from '@fuse/animations';

import { ListMerchantReviewAssignmentService } from './list-merchant-review-assignment.service';
import { Router } from '@angular/router';

@Component({
    selector     : 'fuse-file-manager',
    templateUrl  : './list-merchant-review-assignment.component.html',
    styleUrls    : ['./list-merchant-review-assignment.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class ListMerchantReviewAssignmentComponent implements OnInit
{
    selected: any;
    pathArr: string[];

    constructor(private ListMerchantReviewAssignmentService: ListMerchantReviewAssignmentService, private router: Router)
    {
    }

    ngOnInit()
    {
        // this.ListMerchantReviewAssignmentService.onFileSelected.subscribe(selected => {
        //     this.selected = selected;
        //    // this.pathArr = selected.location.split('>');
        // });
    }s

    openMerchantCategoryAdd(){
        this.router.navigateByUrl("pages/merchant-category/add");
    }
}
