import { AbstractControl, FormBuilder, FormGroup, Validators, EmailValidator } from '@angular/forms';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort } from '@angular/material';
import { FormControl } from '@angular/forms';
import { DataSource } from '@angular/cdk/collections';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';

import { ListMerchantReviewAssignmentService } from '../list-merchant-review-assignment.service';
import { fuseAnimations } from '@fuse/animations/index';
import { FuseUtils } from '@fuse/utils';
import { MatSnackBar } from '@angular/material';

@Component({
    selector: 'fuse-file-list',
    templateUrl: './grid-merchant-review-assignment.component.html',
    styleUrls: ['./grid-merchant-review-assignment.component.scss'],
    animations: fuseAnimations
})
export class GridMerchantReviewAssignmentComponent implements OnInit {
    files: any;
    dataSource: GridUserDataSource | null;
    displayedColumns = ['icon', 'merchant-name', 'user-name', 'modified'];
    selected: any;
    usersList: any;
    merchantsList: any;
    form: FormGroup;
    users = [];
    review = {
        id: '',
        userId: ''

    }
    assignReviews: any = [];
    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild('filter') filter: ElementRef;
    @ViewChild(MatSort) sort: MatSort;

    constructor(private formBuilder: FormBuilder, private ListMerchantReviewAssignmentService: ListMerchantReviewAssignmentService, private router: Router, private snackBar: MatSnackBar) {
        this.usersList = this.ListMerchantReviewAssignmentService.users;
        this.merchantsList = this.ListMerchantReviewAssignmentService.merchantsList;

        // if (!this.ListMerchantReviewAssignmentService.merchantsList.hasOwnProperty('usersList')) {
        //     debugger;
        //     for (let i = 0; i < this.ListMerchantReviewAssignmentService.merchantsList.length; i++) {
        //         this.ListMerchantReviewAssignmentService.merchantsList[i].usersList = [];
        //     }
        // }

        // this.ListMerchantCategoryService.onFilesChanged.subscribe(files => {
        //     this.files = files;
        // });
        // this.ListMerchantCategoryService.onFileSelected.subscribe(selected => {
        //     this.selected = selected;
        // });
    }

    ngOnInit() {
        this.dataSource = new GridUserDataSource(this.ListMerchantReviewAssignmentService, this.paginator, this.sort);

        this.form = this.formBuilder.group({
            userName: [''],
            merchantName: ['']
        });
        if (this.ListMerchantReviewAssignmentService.merchantsList.length != 0 || this.ListMerchantReviewAssignmentService.merchantsList != null) {

            for (let i = 0; i < this.ListMerchantReviewAssignmentService.merchantsList.length; i++) {
                this.form.controls[this.ListMerchantReviewAssignmentService.merchantsList[i].id] = new FormControl(false);
            }
        }

        this.form.valueChanges.subscribe(() => {
            this.onFormValuesChanged();
        });

    }

    onFormValuesChanged() {
        // for ( const field in this.formErrors )
        // {
        //     if ( !this.formErrors.hasOwnProperty(field) )
        //     {
        //         continue;
        //     }

        //     // Clear previous errors
        //     this.formErrors[field] = {};

        //     // Get the control
        //     const control = this.form.get(field);

        //     if ( control && control.dirty && !control.valid )
        //     {
        //         this.formErrors[field] = control.errors;
        //     }
        // }
    }

    onSelect(category) {
        // this.ListMerchantService.onFileSelected.next(selected);
        this.router.navigateByUrl("pages/merchant-category/edit/" + category.id);
    }

    openUserDashboard(event, userid) {
        this.router.navigateByUrl("apps/dashboards/project/" + userid);
    }

    openCategoryEdit(event, userid) {
        this.router.navigateByUrl("pages/merchant-category/edit/" + userid);
    }


    compareObjects(obj1: any, obj2: any): boolean {
        return obj1.id === obj2.id;
    }

    AssignReviews(id, usersList) {
        debugger;
        var userIdList: any[] = [];
        for (let i = 0; i < usersList.length; i++) {

            userIdList.push(usersList[i].id);
        }
        this.ListMerchantReviewAssignmentService.assignReview(id, userIdList).then(response => {
            
            if(response.meta.code==200){
                this.snackBar.open(response.meta.message, "Done", {
                    duration: 2000,
                });
            }

        })

    }
    resetAssignments() {
        debugger;
        var merchants: any[] = [];

        for (let i = 0; i < this.merchantsList.length; i++) {
            merchants.push(this.merchantsList[i].id);
        }
        this.ListMerchantReviewAssignmentService.clearAssignments(merchants).then(response => {
            
            if(response.meta.code==200){
                this.snackBar.open(response.meta.message, "Done", {
                    duration: 2000,
                });
            }

        })

    }
}

export class GridUserDataSource extends DataSource<any>
{
    _filterChange = new BehaviorSubject('');
    _filteredDataChange = new BehaviorSubject('');

    get filteredData(): any {
        return this._filteredDataChange.value;
    }

    set filteredData(value: any) {
        this._filteredDataChange.next(value);
    }

    get filter(): string {
        return this._filterChange.value;
    }

    set filter(filter: string) {
        this._filterChange.next(filter);
    }
    constructor(private ListMerchantReviewAssignmentService: ListMerchantReviewAssignmentService, private _paginator: MatPaginator, private _sort: MatSort) {
        super();
        this.filteredData = this.ListMerchantReviewAssignmentService.merchantsList;
    }

    /** Connect function called by the table to retrieve one stream containing the data to render. */
    connect(): Observable<any[]> {
        const displayDataChanges = [
            this.ListMerchantReviewAssignmentService.onFilesChanged,
            this._paginator.page,
            this._filterChange,
            // this._sort.sortChange
        ];

        return Observable.merge(...displayDataChanges).map(() => {
            let data = this.ListMerchantReviewAssignmentService.merchantsList.slice();

            data = this.filterData(data);

            this.filteredData = [...data];

            //data = this.sortData(data);

            // Grab the page's slice of data.
            const startIndex = this._paginator.pageIndex * this._paginator.pageSize;
            return data.splice(startIndex, this._paginator.pageSize);
        });
    }

    filterData(data) {
        if (!this.filter) {
            return data;
        }
        return FuseUtils.filterArrayByString(data, this.filter);
    }

    disconnect() {
    }

}
