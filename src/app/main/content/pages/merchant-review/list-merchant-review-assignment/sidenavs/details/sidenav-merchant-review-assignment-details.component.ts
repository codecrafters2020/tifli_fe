import { Component, OnInit } from '@angular/core';

import { fuseAnimations } from '@fuse/animations/index';

import { ListMerchantReviewAssignmentService } from '../../list-merchant-review-assignment.service';

@Component({
    selector   : 'fuse-file-manager-details-sidenav',
    templateUrl: './sidenav-merchant-review-assignment-details.component.html',
    styleUrls  : ['./sidenav-merchant-review-assignment-details.component.scss'],
    animations : fuseAnimations
})
export class SidenavMerchantReviewAssignmentDetailsComponent implements OnInit
{

    selected: any;

    constructor(private ListMerchantReviewAssignmentService: ListMerchantReviewAssignmentService)
    {

    }

    ngOnInit()
    {
        // this.ListMerchantReviewAssignmentService.onFileSelected.subscribe(selected => {
        //     this.selected = selected;
        // });
    }

}
