import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { FuseAngularMaterialModule } from '../../components/angular-material/angular-material.module';

import { FuseSharedModule } from '@fuse/shared.module';

const routes = [
    {
        path        : 'list',
        loadChildren: './list-merchant-review/list-merchant-review.module#ListMerchantReviewModule'
    },
    {
        path        : 'assign-list',
        loadChildren: './list-merchant-review-assignment/list-merchant-review-assignment.module#ListMerchantReviewAssignmentModule'
    }
];

@NgModule({
    imports     : [
        FuseSharedModule,
        RouterModule.forChild(routes),
        FuseAngularMaterialModule
    ],
    declarations: []
})
export class MerchantReviewModule
{
}
