import { Component, OnInit, ViewEncapsulation } from '@angular/core';

import { fuseAnimations } from '@fuse/animations';

import { ListMerchantReviewService } from './list-merchant-review.service';
import { Router } from '@angular/router';

@Component({
    selector     : 'fuse-file-manager',
    templateUrl  : './list-merchant-review.component.html',
    styleUrls    : ['./list-merchant-review.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class ListMerchantReviewComponent implements OnInit
{
    selected: any;
    pathArr: string[];

    constructor(private ListMerchantReviewService: ListMerchantReviewService, private router: Router)
    {
    }

    ngOnInit()
    {
        // this.ListMerchantReviewService.onFileSelected.subscribe(selected => {
        //     this.selected = selected;
        //    // this.pathArr = selected.location.split('>');
        // });
    }

    openMerchantCategoryAdd(){
        this.router.navigateByUrl("pages/merchant-category/add");
    }
}
