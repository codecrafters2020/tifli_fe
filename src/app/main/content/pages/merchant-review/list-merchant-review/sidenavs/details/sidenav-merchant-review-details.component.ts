import { Component, OnInit } from '@angular/core';

import { fuseAnimations } from '@fuse/animations/index';

import { ListMerchantReviewService } from '../../list-merchant-review.service';

@Component({
    selector   : 'fuse-file-manager-details-sidenav',
    templateUrl: './sidenav-merchant-review-details.component.html',
    styleUrls  : ['./sidenav-merchant-review-details.component.scss'],
    animations : fuseAnimations
})
export class SidenavMerchantReviewDetailsComponent implements OnInit
{

    selected: any;

    constructor(private ListMerchantReviewService: ListMerchantReviewService)
    {

    }

    ngOnInit()
    {
        this.ListMerchantReviewService.onFileSelected.subscribe(selected => {
            this.selected = selected;
        });
    }

}
