import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CdkTableModule } from '@angular/cdk/table';

import { MatFormFieldModule, MatInputModule, MatSelectModule, MatStepperModule, MatButtonModule, MatIconModule, MatRippleModule, MatSidenavModule, MatSlideToggleModule, MatTableModule, MatMenuModule, MatPaginatorModule, MatSortModule } from '@angular/material';

import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatRadioModule} from '@angular/material/radio';
import { FuseSharedModule } from '@fuse/shared.module';

import { ListMerchantReviewComponent } from './list-merchant-review.component';
import { ListMerchantReviewService } from './list-merchant-review.service';
import { GridMerchantReviewComponent } from './grid/grid-merchant-review.component';
import { SidenavMerchantReviewMainComponent } from './sidenavs/main/sidenav-merchant-review-main.component';
import { SidenavMerchantReviewDetailsComponent } from './sidenavs/details/sidenav-merchant-review-details.component';
import { GridMerchantReviewModalComponent } from './grid/modal/grid-merchant-review-modal.component';
import { FancyImageUploaderModule } from 'ng2-fancy-image-uploader';
import {MatExpansionModule} from '@angular/material/expansion';

const routes: Routes = [
    {
        path     : '**',
        component: ListMerchantReviewComponent,
        children : [],
        resolve  : {
            files: ListMerchantReviewService
        }
    }
];

@NgModule({
    declarations: [
        ListMerchantReviewComponent,
        GridMerchantReviewComponent,
        SidenavMerchantReviewMainComponent,
        SidenavMerchantReviewDetailsComponent,
        GridMerchantReviewModalComponent
    ],
    imports     : [
        RouterModule.forChild(routes),

        CdkTableModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        MatStepperModule,
        MatButtonModule,
        MatIconModule,
        MatRippleModule,
        MatSlideToggleModule,
        MatSidenavModule,
        MatTableModule,
        MatMenuModule,
        MatPaginatorModule, 
        MatSortModule,
        FancyImageUploaderModule,
        MatExpansionModule,

        FuseSharedModule,
        MatDatepickerModule,
        MatCheckboxModule,
        MatRadioModule
    ],
    providers   : [
        ListMerchantReviewService
    ],
    entryComponents: [GridMerchantReviewModalComponent]
})
export class ListMerchantReviewModule
{
}
