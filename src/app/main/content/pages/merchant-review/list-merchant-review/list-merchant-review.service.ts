import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';

import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { AppService } from './../../../../../app.service';
import { AuthService } from '../../../../../auth/auth.service';

@Injectable()
export class ListMerchantReviewService implements Resolve<any>
{
    merchantsList: any[];
    merchant: any={};
    categoriesList: any[];
    usersList: any[];
    barcodesList: any[];
    affiliateNetworksList: any[];
    merchantAffiliateStatusList: any[];
    allMerchantsList: any[];
    merchantTierList: any[];
    onFilesChanged: BehaviorSubject<any> = new BehaviorSubject({});
    onFileSelected: BehaviorSubject<any> = new BehaviorSubject({});

    constructor(private http: HttpClient,private appService: AppService,private authService:AuthService)
    {
    }

    /**
     * The File Manager App Main Resolver
     * 
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any
    {

        // return new Promise((resolve, reject) => {

        //     Promise.all([
        //         this.getMerchants(),
        //         this.getCategories(),
        //         this.getUsers(),
        //         this.getBarcodes(),
        //         this.getAffiliateNetworks(),
        //         this.getAffiliateNetworkStatuses(),
        //         this.getAllMerchants(),
        //         this.getMerchantTiers()
        //     ]).then(
        //         ([merchantsList, categoriesList, usersList, barcodesList, affiliateNetworksList, merchantAffiliateStatusList, allMerchantsList, merchantTierList]) => {
        //             resolve();
        //         },
        //         reject
        //     );
        // });
    }

    getMerchantById(id): Promise<any>
    {
        return new Promise((resolve, reject) => {
            this.http.get(this.appService.merchantService + id)
                .subscribe((response: any) => {
                    console.log(response);
                    if(response.meta.code=="200")
                    {
                        this.merchant=response.data;
                    }
                    resolve(response);
                }, reject);
        });
    } 
    
    getCategories(): Promise<any>
    {

        return new Promise((resolve, reject) => {

            this.http.get(this.appService.merchantService + 'categories/')
                .subscribe((response: any) => {
                    console.log(response);
                    if(response.meta.code=="200")
                    {
                        this.categoriesList=response.data;
                    }

                    resolve(response);
                }, reject);
        });
    }

    getUsers(): Promise<any>
    {

        return new Promise((resolve, reject) => {
            this.http.get(this.appService.userService + 'users/')
                .subscribe((response: any) => {
                    console.log(response);
                    if(response.meta.code=="200")
                    {
                        this.usersList=response.data;
                    }
                    resolve(response);
                }, reject);
        });
    }

    getBarcodes(): Promise<any>
    {

        return new Promise((resolve, reject) => {
            this.http.get(this.appService.merchantService + 'barcodes/')
                .subscribe((response: any) => {
                    console.log(response);
                    if(response.meta.code=="200")
                    {
                        this.barcodesList=response.data;
                    }
                    resolve(response);
                }, reject);
        });
    }

    getAffiliateNetworks(): Promise<any>
    {

        return new Promise((resolve, reject) => {
            this.http.get(this.appService.merchantService + 'affiliates/')
                .subscribe((response: any) => {
                    console.log(response);
                    if(response.meta.code=="200")
                    {
                        this.affiliateNetworksList=response.data;
                    }
                    resolve(response);
                }, reject);
        });
    }

    getAffiliateNetworkStatuses(): Promise<any>
    {

        return new Promise((resolve, reject) => {
            this.http.get(this.appService.merchantService + 'affiliates/statuses/')
                .subscribe((response: any) => {
                    console.log(response);
                    if(response.meta.code=="200")
                    {
                        this.merchantAffiliateStatusList=response.data;
                    }
                    resolve(response);
                }, reject);
        });
    }

    getAllMerchants(): Promise<any>
    {

        return new Promise((resolve, reject) => {
            this.http.get(this.appService.merchantService)
                .subscribe((response: any) => {
                    console.log(response);
                    if(response.meta.code=="200")
                    {
                        this.allMerchantsList=response.data;
                    }
                    resolve(response);
                }, reject);
        });
    }

    getMerchantTiers(): Promise<any> {

        return new Promise((resolve, reject) => {
            this.http.get(this.appService.merchantService + 'tiers/')
                .subscribe((response: any) => {
                    console.log(response);
                    if (response.meta.code == "200") {
                        this.merchantTierList = response.data;
                    }
                    resolve(response);
                }, reject);
        });
    }

    updateMerchant(merchant): Promise<any>
    {
        return new Promise((resolve, reject) => {
            debugger;
            // this.http.post('https://httpbin.org/post', merchant)
            console.log("before merchnt")
            console.log(this.appService.merchantService)
            this.http.put(this.appService.merchantService + merchant.id, merchant)
                .subscribe((response: any) => {
                    console.log("after merchnt")
                    console.log(this.appService.merchantService)
                    resolve(response);
                }, reject);
        });
    }

    getMerchants(): Promise<any>
    {
        return new Promise((resolve, reject) => {
            var currUser = this.authService.getUser();
            this.http.get(this.appService.merchantService + 'review/user/'+currUser.id)
                .subscribe((response: any) => {
                    debugger;
                    this.merchantsList = response.data;
                    this.onFilesChanged.next(response.data);
                    this.onFileSelected.next(response[0]);
                    resolve(response);
                }, reject);
        });
    }

    updateMerchants(merchants): Promise<any>
    {
        return new Promise((resolve, reject) => {
            debugger;
            this.http.put(this.appService.merchantService , merchants)
                .subscribe((response: any) => {
                    debugger;
                    resolve(response);
                }, reject);
        });
    }

}
