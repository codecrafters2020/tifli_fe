import { AbstractControl, FormBuilder, FormGroup, Validators, EmailValidator } from '@angular/forms';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort } from '@angular/material';
import { FormControl } from '@angular/forms';
import { DataSource } from '@angular/cdk/collections';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';

import { ListMerchantReviewService } from '../list-merchant-review.service';
import { fuseAnimations } from '@fuse/animations/index';
import { FuseUtils } from '@fuse/utils';
import { AuthService } from '../../../../../../auth/auth.service';
import {GridMerchantReviewModalComponent} from './modal/grid-merchant-review-modal.component';
import { MatDialog, MatDialogRef } from '@angular/material';
import { MatSnackBar } from '@angular/material';
// import { ChangeDetectionStrategy } from '@angular/core';

@Component({
    // changeDetection: ChangeDetectionStrategy.OnPush,
    selector   : 'fuse-file-list',
    templateUrl: './grid-merchant-review.component.html',
    styleUrls  : ['./grid-merchant-review.component.scss'],
    animations : fuseAnimations
})
export class GridMerchantReviewComponent implements OnInit
{
    files: any;
    dataSource: GridUserDataSource | null;
    displayedColumns = ['icon', 'merchant-name', 'reviewed'];
    selected: any;
    form: FormGroup;
    merchantsList:any[] = [];
    dialogRef: MatDialogRef<GridMerchantReviewModalComponent>;
    merchantId: any;

    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild('filter') filter: ElementRef;
    @ViewChild(MatSort) sort: MatSort;

    constructor(private formBuilder: FormBuilder, private ListMerchantReviewService: ListMerchantReviewService, private router: Router ,private authService:AuthService, private dialog: MatDialog, private snackBar: MatSnackBar)
    {
        // this.ListMerchantCategoryService.onFilesChanged.subscribe(files => {
        //     this.files = files;
        // });
        // this.ListMerchantCategoryService.onFileSelected.subscribe(selected => {
        //     this.selected = selected;
        // });
    }

    ngOnInit()
    {
       
        this.dataSource = new GridUserDataSource(this.ListMerchantReviewService, this.paginator, this.sort);

        this.form = this.formBuilder.group({

            merchantName: ['']

        });
        if (this.ListMerchantReviewService.merchantsList.length != 0 || this.ListMerchantReviewService.merchantsList!= null) {

            for (let i = 0; i < this.ListMerchantReviewService.merchantsList.length; i++) {
                this.form.controls[this.ListMerchantReviewService.merchantsList[i].merchant.name] = new FormControl('');
                this.form.controls[this.ListMerchantReviewService.merchantsList[i].merchant.id] = new FormControl(false);
            }
        }

        this.form.valueChanges.subscribe(() => {
            this.onFormValuesChanged();
        });
    }

    onFormValuesChanged() {
        // for ( const field in this.formErrors )
        // {
        //     if ( !this.formErrors.hasOwnProperty(field) )
        //     {
        //         continue;
        //     }

        //     // Clear previous errors
        //     this.formErrors[field] = {};

        //     // Get the control
        //     const control = this.form.get(field);

        //     if ( control && control.dirty && !control.valid )
        //     {
        //         this.formErrors[field] = control.errors;
        //     }
        // }
    }

    onSelect(category) {
        // this.ListMerchantService.onFileSelected.next(selected);
        this.router.navigateByUrl("pages/merchant-category/edit/" + category.id);
    }

    openUserDashboard(event,userid){
        this.router.navigateByUrl("apps/dashboards/project/" + userid);
    }

    openCategoryEdit(event,userid){
        this.router.navigateByUrl("pages/merchant-category/edit/" + userid);
    }

    openModal(merchantId) {
        // this.addMonetized = !this.addMonetized;
        debugger;
        this.merchantId = merchantId;
        this.dialogRef = this.dialog.open(GridMerchantReviewModalComponent, {
            width: '1500px',
            height: '500px',
            data: { merchantId: this.merchantId },
        });
    }

    updateReviewedMerchants(){
        debugger;
        var currUser = this.authService.getUser();
        for(let i=0;i<this.ListMerchantReviewService.merchantsList.length; i++){

            if(this.ListMerchantReviewService.merchantsList[i].merchant.isReviewed===true){

                var merchantObj={
                    id:'',
                    isReviewed:true,
                    lastReviewedBy:'',
                    dateLastReviewed:null
                };
                merchantObj.id=this.ListMerchantReviewService.merchantsList[i].merchant.id;
                merchantObj.dateLastReviewed=new Date();
                merchantObj.lastReviewedBy=currUser.id;
                this.merchantsList.push(merchantObj);
            }
        }
        this.ListMerchantReviewService.updateMerchants(this.merchantsList).then(response => {
            
            if(response.meta.code==200){
                this.snackBar.open(response.meta.message, "Done", {
                    duration: 2000,
                });
            }

        })

        this.merchantsList=[];
    }
}

export class GridUserDataSource extends DataSource<any>
{
    _filterChange = new BehaviorSubject('');
    _filteredDataChange = new BehaviorSubject('');

    get filteredData(): any
    {
        return this._filteredDataChange.value;
    }

    set filteredData(value: any)
    {
        this._filteredDataChange.next(value);
    }

    get filter(): string
    {
        return this._filterChange.value;
    }

    set filter(filter: string)
    {
        this._filterChange.next(filter);
    }
    constructor(private ListMerchantReviewService: ListMerchantReviewService, private _paginator: MatPaginator, private _sort: MatSort)
    {
        super();
        this.filteredData = this.ListMerchantReviewService.merchantsList;
    }

    /** Connect function called by the table to retrieve one stream containing the data to render. */
    // connect(): Observable<any[]>
    // {
    //     return this.ListMerchantCategoryService.onFilesChanged;
    // }
    connect(): Observable<any[]>
    {
        const displayDataChanges = [
            this.ListMerchantReviewService.onFilesChanged,
            this._paginator.page,
            this._filterChange,
            // this._sort.sortChange
        ];

        return Observable.merge(...displayDataChanges).map(() => {
            let data = this.ListMerchantReviewService.merchantsList.slice();

            data = this.filterData(data);

            this.filteredData = [...data];

            //data = this.sortData(data);

            // Grab the page's slice of data.
            const startIndex = this._paginator.pageIndex * this._paginator.pageSize;
            return data.splice(startIndex, this._paginator.pageSize);
        });
    }

    filterData(data)
    {
        if ( !this.filter )
        {
            return data;
        }
        return FuseUtils.filterArrayByString(data, this.filter);
    }

    disconnect()
    {
    }
}
