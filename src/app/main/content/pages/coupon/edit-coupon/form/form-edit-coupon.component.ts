import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators, EmailValidator, FormControl } from '@angular/forms';
import { DataSource } from '@angular/cdk/collections';
import { FancyImageUploaderOptions, UploadedFile } from 'ng2-fancy-image-uploader';
import { Observable } from 'rxjs/Observable';
import { EditCouponService } from '../edit-coupon.service';
import { MatSnackBar, MatStepper } from '@angular/material';
import { Router } from '@angular/router';
import { AppService } from './../../../../../../app.service';
import * as _swal from 'sweetalert';
import { SweetAlert } from 'sweetalert/typings/core';
const swal: SweetAlert = _swal as any;

@Component({
    selector: 'fuse-file-list',
    templateUrl: './form-edit-coupon.component.html',
    styleUrls: ['./form-edit-coupon.component.scss'],
})
export class FormEditCouponComponent implements OnInit {

    //form: FormGroup;
    formErrors: any;
    test: any;
    step: number;
    merchantSearchResult: any[];


    // Horizontal Stepper
    horizontalStepperStep1: FormGroup;
   // horizontalStepperStep2: FormGroup;
   //horizontalStepperStep3: FormGroup;
    horizontalStepperStep4: FormGroup;

    horizontalStepperStep1Errors: any;
   // horizontalStepperStep2Errors: any;
   // horizontalStepperStep3Errors: any;
    horizontalStepperStep4Errors: any;


    displayedColumns = ['id', 'name'];
    displayedColumnsPanelHeading = ['Enable', 'Seasonal Event'];

    InstoreImageOptions: FancyImageUploaderOptions = {
        thumbnailHeight: 200,
        thumbnailWidth: 250,
        //uploadUrl: 'https://fancy-image-uploader-demo.azurewebsites.net/api/demo/upload',
        //uploadUrl: 'http://104.42.179.33:8765/cms/api/uploads/',
        uploadUrl: this.appService.uploadService + 'uploads/',
        allowedImageTypes: ['image/png', 'image/jpeg'],
        maxImageSize: 3,
    };

    dealImageOptions: FancyImageUploaderOptions = {
        thumbnailHeight: 200,
        thumbnailWidth: 250,
        //uploadUrl: 'https://fancy-image-uploader-demo.azurewebsites.net/api/demo/upload',
        //uploadUrl: 'http://104.42.179.33:8765/cms/api/uploads/',
        uploadUrl: this.appService.uploadService + 'uploads/',
        allowedImageTypes: ['image/png', 'image/jpeg'],
        maxImageSize: 3,
    };

    coupon: any;
    merchantsList: any[];
    couponPriorityList: any[];
    couponStatusList: any[];
    couponTypeList: any[];
    seasonalEventList: any[];
    // merchantName: string = "";
    merchant: any;
    couponCategoriesList: any[] = [];
    couponCategory: any;
    redirectUrl: string = "";
    isDeal: boolean = false;
    isInstore: boolean = false;
    images: any[] = [];
    dealImgUrl: string = "";
    inStoreImgUrl: string = "";
    isCode: boolean = false;

    constructor(private EditCouponService: EditCouponService, private formBuilder: FormBuilder, private router: Router, private snackBar: MatSnackBar, private appService: AppService) {

       // debugger;

        this.coupon = EditCouponService.coupon;
        if (!this.coupon.hasOwnProperty('images')) {
            this.coupon.images = this.images;
        }
        this.coupon.startDate = new Date(this.coupon.startDate);
        this.coupon.endDate = new Date(this.coupon.endDate);
        this.coupon.dateCreated = new Date(this.coupon.dateCreated);
        this.coupon.dateModified = new Date(this.coupon.dateModified);

        this.dealImgUrl = this.appService.cdnUrl + this.coupon.id + "_deal.png";
        this.inStoreImgUrl = this.appService.cdnUrl + this.coupon.id + "_inStore.png";


        if (this.coupon.clickCount == '') {
            this.coupon.clickCount = '0';
        }

        if (this.coupon.orderCount == '') {
            this.coupon.orderCount == '0';
        }

        if (this.coupon.commission == '') {
            this.coupon.commission == '0.0';
        }

        if (this.coupon.couponType.title == "Deal") {
            this.isDeal = true;
        }
        else if (this.coupon.couponType.title == "In-store") {
            this.isInstore = true;
            this.coupon.originalPrice = '';
            this.coupon.dealPrice = '';
            this.coupon.saving = '';
        }
        else {
            this.isDeal = false;
            this.isInstore = false;
            this.coupon.originalPrice = '';
            this.coupon.dealPrice = '';
            this.coupon.saving = '';
        }


        this.EditCouponService.getMerchantById(this.coupon.merchant.id).then(response => {
            this.coupon.merchant = response.data;
            console.log("response: " + JSON.stringify(response));
            console.log(this.coupon.merchant);
            //this.couponCategoriesList.pop();
            debugger;
            this.couponCategoriesList.push(this.coupon.merchant.merchantCategoryId);
            if (this.coupon.merchant.categoriesList.length > 0) {
                for (let i = 0; i < this.coupon.merchant.categoriesList.length; i++) {
                    if (this.couponCategoriesList[0].id != this.coupon.merchant.categoriesList[i].id) {
                        if (this.coupon.merchant.categoriesList[i].checked == true) {
                            this.couponCategoriesList.push(this.coupon.merchant.categoriesList[i]);
                        }
                    }
                }

            }
            console.log(this.merchant);

        });

        this.merchantsList = EditCouponService.merchantsList;
        this.couponPriorityList = EditCouponService.couponPriorityList;
        this.couponTypeList = EditCouponService.couponTypeList;
        this.couponStatusList = EditCouponService.couponStatusList;
        this.seasonalEventList = EditCouponService.seasonalEventList;

        // Horizontal Stepper form error
        this.horizontalStepperStep1Errors = {
            merchant: {},
            title: {},
            restrictions: {},
            status: {},
            priority: {},
            startDate: {},
            endDate: {},
            couponType: {},
            couponCategory: {},
            originalPrice: {},
            dealPrice: {},
            saving: {},
            cashbackPercentage: {},
            redirectUrl: {}
        };

        // this.horizontalStepperStep3Errors = {
        //     redirectUrl: {}
        // };



    }

    ngOnInit() {

        // Horizontal Stepper form steps
        this.horizontalStepperStep1 = this.formBuilder.group({

            merchant: ['', Validators.required],
            title: ['', Validators.required],
            description: [''],
            code: [''],
            tags: [''],
            restrictions: [''],
            status: ['', [Validators.required]],
            priority: ['', Validators.required],
            startDate: ['', Validators.required],
            endDate: ['', Validators.required],
            couponType: ['', Validators.required],
            couponCategory: [''],
            originalPrice: [''],
            dealPrice: [''],
            saving: [''],
            customButtonText: [''],
            ongoingOffer: [''],
            exclusiveOffer: [''],
            freeship: [''],
            // showExpiredCoupon: [''],
            alertsEligible: [''],
            showSavings: [''],
            hideExpirationDate: [''],
            excludeSavingsFromAlgo: [''],
            overrideSavingsInTitle: [''],
            overrideSavingsInPod: [''],
            isCashbackCoupon: [''],
            staffPicked: [''],
            cashbackPercentage: [''],
            orderCount: [''],
            dateCreated: [''],
            dateModified: [''],
            clickCount: [''],
            commission: [''],
            metaType: [''],
            metaSaving: [''],
            redirectUrl: ['', Validators.required],
            ciUrl: [''],
            linkCode: ['']

        });

        
        for (let i = 0; i < this.seasonalEventList.length; i++) {
            console.log("Selected Seasonal Events:");
            if (this.coupon.seasonalEventSet.length >= 1) {
                for (let j = 0; j < this.coupon.seasonalEventSet.length; j++) {
                    if (this.seasonalEventList[i].id == this.coupon.seasonalEventSet[j].id) {
                        debugger;
                        //console.log(this.coupon.seasonalEventSet[j].id);
                        this.horizontalStepperStep1.controls[this.seasonalEventList[i].id] = new FormControl(true);
                        break;
                    }
                    else{
                        this.horizontalStepperStep1.controls[this.seasonalEventList[i].id] = new FormControl(false);
                    }
                }
            }
        }

        // this.horizontalStepperStep2 = this.formBuilder.group({

        // });

        // this.horizontalStepperStep3 = this.formBuilder.group({

        //     redirectUrl: ['', Validators.required],
        //     ciUrl: [''],
        //     linkCode: ['']
        // });


        this.horizontalStepperStep1.valueChanges.subscribe(() => {
            this.onFormValuesChanged(this.horizontalStepperStep1, this.horizontalStepperStep1Errors);
        });

        // this.horizontalStepperStep2.valueChanges.subscribe(() => {
        //     this.onFormValuesChanged(this.horizontalStepperStep2, this.horizontalStepperStep2Errors);
        // });

        // this.horizontalStepperStep3.valueChanges.subscribe(() => {
        //     this.onFormValuesChanged(this.horizontalStepperStep3, this.horizontalStepperStep3Errors);
        // });

        this.horizontalStepperStep1.controls.merchant.valueChanges
            .debounceTime(400)
            .subscribe(searchTerm => {
                this.EditCouponService.searchMerchant(searchTerm)
                    .subscribe(merchantNames => {
                        this.merchantSearchResult = merchantNames;
                    });
            });

            this.horizontalStepperStep1.controls.originalPrice.valueChanges
            .subscribe(() => {
                let op = this.horizontalStepperStep1.controls.originalPrice.value;
                let dp = this.horizontalStepperStep1.controls.dealPrice.value;
                let difference = op - dp;
                this.horizontalStepperStep1.controls['saving'].setValue(difference);
            });

        this.horizontalStepperStep1.controls.dealPrice.valueChanges
            .subscribe(() => {
                let op = this.horizontalStepperStep1.controls.originalPrice.value;
                let dp = this.horizontalStepperStep1.controls.dealPrice.value;
                let difference = op - dp;
                this.horizontalStepperStep1.controls['saving'].setValue(difference);
            });

        this.horizontalStepperStep1.controls.startDate.valueChanges
        .subscribe(() => {
            this.setCouponStatus();
        });

        this.horizontalStepperStep1.controls.endDate.valueChanges
        .subscribe(() => {
            this.setCouponStatus();
        });

    }

    setCouponStatus(){
        debugger
        let nowDate: any = new Date();
        nowDate.setHours(0, 0, 0, 0);
        let startDate = this.coupon.startDate;
        startDate.setHours(0, 0, 0, 0);
        let endDate = this.coupon.endDate;
        if (endDate !== "" || endDate !== null)
            endDate.setHours(0, 0, 0, 0);
        let beforeStart = nowDate - startDate;
        let beforeEnd = nowDate - endDate;
        let statuschecked = false;
        let statusActive = false;
        let status: any;
        if (endDate === "" || endDate === null) {
            statusActive = true;
            if (statusActive) {
                for (let i = 0; i < this.couponStatusList.length; i++) {
                    if (this.couponStatusList[i].title === "Active") {
                        status = this.couponStatusList[i];
                        statuschecked = true;
                        break;
                    }
                }
            }
        }
        else {
            if (beforeStart > 0 && beforeEnd < 0) {
                for (let i = 0; i < this.couponStatusList.length; i++) {
                    if (this.couponStatusList[i].title === "Active") {
                        status = this.couponStatusList[i];
                        statuschecked = true;
                        break;
                    }
                }
            }
            else if ((beforeStart > 0 || beforeStart == 0) && (beforeEnd < 0 || beforeEnd == 0)) {
                for (let i = 0; i < this.couponStatusList.length; i++) {
                    if (this.couponStatusList[i].title === "Active") {
                        status = this.couponStatusList[i];
                        statuschecked = true;
                        break;
                    }
                }
            }
            else if (beforeEnd > 0) {
                for (let i = 0; i < this.couponStatusList.length; i++) {
                    if (this.couponStatusList[i].title === "Expired") {
                        status = this.couponStatusList[i];
                        statuschecked = true;
                        break;
                    }
                }
            }
            else if (beforeStart < 0) {
                for (let i = 0; i < this.couponStatusList.length; i++) {
                    if (this.couponStatusList[i].title === "Pending") {
                        status = this.couponStatusList[i];
                        statuschecked = true;
                        break;
                    }
                }
            }
        }
        if (statuschecked) {
            this.coupon.couponStatus = status;
            // this.horizontalStepperStep1.controls['status'].setValue(status);
        }
    }


    onFormValuesChanged(horizontalStepperStep: FormGroup, horizontalStepperStepErrors: any) {
        for (const field in horizontalStepperStepErrors) {
            if (!horizontalStepperStepErrors.hasOwnProperty(field)) {
                continue;
            }


            horizontalStepperStepErrors[field] = {};

            const control = horizontalStepperStep.get(field);

            if (control && control.dirty && !control.valid) {
                horizontalStepperStepErrors[field] = control.errors;
            }
        }

        this.horizontalStepperStep1.controls.endDate.valueChanges
            .subscribe(() => {
                let sd = this.horizontalStepperStep1.controls.startDate.value;
                let ed = this.horizontalStepperStep1.controls.endDate.value;


                if ((Date.parse(sd) > Date.parse(ed))) {
                    alert("End date should be greater than Start date");
                    this.horizontalStepperStep1.controls['endDate'].setValue('');
                }
            });


    }

    finishHorizontalStepper() {

        if (this.coupon.saving == '') {
            this.coupon.saving = '0.0';
        }

        if (this.coupon.originalPrice == '') {
            this.coupon.originalPrice = '0.0';
        }

        if (this.coupon.dealPrice == '') {
            this.coupon.dealPrice = '0.0';
        }

        if (this.coupon.cashbackPercentage == '') {
            this.coupon.cashbackPercentage = '0.0';
        }

        // if (this.coupon.cashbackPercentage == '') {
        //     this.coupon.cashbackPercentage = '0';
        // }

        this.EditCouponService.updateCoupon(this.coupon).then(response => {
            debugger;
            this.test = response;
            console.log("response: " + JSON.stringify(response));
            this.snackBar.open(response.meta.message, "Done", {
                duration: 2000,
            });
        });
        this.router.navigateByUrl("pages/coupon/list/")

    }

    redirect(pagename: string) {
        this.router.navigate([pagename]);
    }

    onUploadInstoreImage(file: UploadedFile) {
        debugger;
        var response: any = {};
        response = JSON.parse(file.response)
        if (response.meta.code == 200) {
            if (this.coupon.images != null) {
                if (this.coupon.images.length >= 1) {
                    for (let i = 0; i < this.coupon.images.length; i++) {
                        //if (this.coupon.images[i].suffix == "deal") {
                        this.coupon.images.splice(i, 1);
                        //}
                    }
                }
            }
            var inStoreImage: any = {};
            inStoreImage.fileName = response.data.fileName;
            inStoreImage.suffix = "inStore";
            this.coupon.images.push(inStoreImage);
        }
    }

    onUploadDealImage(file: UploadedFile) {
        debugger;
        var response: any = {};
        response = JSON.parse(file.response);
        if (response.meta.code == 200) {
            if (this.coupon.images != null) {
                if (this.coupon.images.length >= 1) {
                    for (let i = 0; i < this.coupon.images.length; i++) {
                        //if (this.merchant.images[i].suffix == "small") {
                        this.coupon.images.splice(i, 1);
                        //}
                    }
                }
            }
            var dealImage: any = {};
            dealImage.fileName = response.data.fileName;
            dealImage.suffix = "deal";
            this.coupon.images.push(dealImage);
        }
    }

    hideImage(event: any) {

        var id = event.currentTarget.id;
        if (id == "updateInstoreImageBtn") {

            var imageBtnControl = document.getElementById('updateInstoreImageBtn');
            imageBtnControl.style.display = "none";
            var imageControl = document.getElementById('inStoreImage');
            imageControl.style.display = "none";
            var uploaderControl = document.getElementById('inStoreImageUploader');
            uploaderControl.style.display = "block";
            var cancelControl = document.getElementById('cancelUpdateInstoreImageBtn');
            cancelControl.style.display = "block";
            cancelControl.style.visibility = "visible";


        }
        else if (id == "updateDealImageBtn") {

            var imageBtnControl = document.getElementById('updateDealImageBtn');
            imageBtnControl.style.display = "none";
            var imageControl = document.getElementById('dealImage');
            imageControl.style.display = "none";
            var uploaderControl = document.getElementById('dealImageUploader');
            uploaderControl.style.display = "block";
            var cancelControl = document.getElementById('cancelUpdateDealImageBtn');
            cancelControl.style.display = "block";
            cancelControl.style.visibility = "visible";

        }
    }

    showImage(event: any) {

        var id = event.currentTarget.id;
        if (id == "cancelUpdateInstoreImageBtn") {
            if (this.images != null) {
                if (this.images.length >= 1) {
                    for (let i = 0; i < this.images.length; i++) {
                        if (this.images[i].suffix == "inStore") {
                            this.images.splice(i, 1);
                        }
                    }
                }
            }
            var imageBtnControl = document.getElementById('updateInstoreImageBtn');
            imageBtnControl.style.display = "block";
            var uploaderControl = document.getElementById('inStoreImageUploader');
            uploaderControl.style.display = "none";
            var imageControl = document.getElementById('inStoreImage');
            imageControl.style.display = "block";
            var cancelControl = document.getElementById('cancelUpdateInstoreImageBtn');
            cancelControl.style.display = "none";

        }
        if (id == "cancelUpdateDealImageBtn") {
            if (this.images != null) {
                if (this.images.length >= 1) {
                    for (let i = 0; i < this.images.length; i++) {
                        if (this.images[i].suffix == "deal") {
                            this.images.splice(i, 1);

                        }
                    }
                }
            }
            var imageBtnControl = document.getElementById('updateDealImageBtn');
            imageBtnControl.style.display = "block";
            var uploaderControl = document.getElementById('dealImageUploader');
            uploaderControl.style.display = "none";
            var imageControl = document.getElementById('dealImage');
            imageControl.style.display = "block";
            var cancelControl = document.getElementById('cancelUpdateDealImageBtn');
            cancelControl.style.display = "none";

        }
    }

    populateFields(event: any) {


        var merchantId = event.option.value.id;
        if (this.couponCategoriesList.length > 0) {
            for (let i = this.couponCategoriesList.length; i > 0; i--) {
                this.couponCategoriesList.pop();
            }
        }
        this.EditCouponService.getMerchantById(merchantId).then(response => {

            this.merchant = response.data;
            console.log("response: " + JSON.stringify(response));
            // this.couponCategoriesList.pop();
            this.couponCategoriesList.push(this.merchant.merchantCategoryId);
            if (this.merchant.categoriesList.length > 0) {
                for (let i = 0; i < this.merchant.categoriesList.length; i++) {
                    if (this.couponCategoriesList[0].id != this.merchant.categoriesList[i].id) {
                        if (this.merchant.categoriesList[i].checked == true) {
                            this.couponCategoriesList.push(this.merchant.categoriesList[i]);
                        }
                    }
                }

            }

            this.coupon.redirectUrl = this.merchant.redirectUrl;

        });


    }

    // showOptions(event: any) {
    //     debugger;

    //     if (event.value.title == "Deal") {

    //         this.isInstore = false;
    //         this.isDeal = true;
    //     }
    //     else if (event.value.title == "In-store") {
    //         this.isDeal = false;
    //         this.isInstore = true;
    //     }
    //     else {

    //         this.isDeal = false;
    //         this.isInstore = false;
    //     }
    // }
    showOptions(event: any) {


        if (event.value.title == "Deal") {

            this.isDeal = true;
            this.isInstore = false;
            this.isCode = false;

        }
        else if (event.value.title == "In-store") {

            this.isDeal = false;
            this.isInstore = true;
            this.isCode = false;

            this.coupon.saving = '';
            this.coupon.originalPrice = '';
            this.coupon.dealPrice = '';
            this.horizontalStepperStep1.controls["saving"].markAsUntouched();
            this.horizontalStepperStep1.controls["originalPrice"].markAsUntouched();
            this.horizontalStepperStep1.controls["dealPrice"].markAsUntouched();

            this.horizontalStepperStep1.controls["saving"].setValidators(null);
            this.horizontalStepperStep1Errors.saving = {};
            this.horizontalStepperStep1.controls["saving"].updateValueAndValidity();

            this.horizontalStepperStep1.controls["originalPrice"].setValidators(null);

            this.horizontalStepperStep1.controls["originalPrice"].updateValueAndValidity();

            this.horizontalStepperStep1.controls["dealPrice"].setValidators(null);
            this.horizontalStepperStep1Errors.dealPrice = {};
            this.horizontalStepperStep1.controls["dealPrice"].updateValueAndValidity();

        }

        else if (event.value.title == "Code") {

            this.isDeal = false;
            this.isInstore = false;
            this.isCode = true;

            // this.coupon.saving = '';
            this.coupon.originalPrice = '';
            this.coupon.dealPrice = '';
            this.horizontalStepperStep1.controls["saving"].markAsUntouched();
            this.horizontalStepperStep1.controls["originalPrice"].markAsUntouched();
            this.horizontalStepperStep1.controls["dealPrice"].markAsUntouched();

            this.horizontalStepperStep1.controls["saving"].setValidators(null);
            this.horizontalStepperStep1Errors.saving = {};
            this.horizontalStepperStep1.controls["saving"].updateValueAndValidity();

            this.horizontalStepperStep1.controls["originalPrice"].setValidators(null);
            this.horizontalStepperStep1Errors.originalPrice = {};
            this.horizontalStepperStep1.controls["originalPrice"].updateValueAndValidity();

            this.horizontalStepperStep1.controls["dealPrice"].setValidators(null);
            this.horizontalStepperStep1Errors.dealPrice = {};
            this.horizontalStepperStep1.controls["dealPrice"].updateValueAndValidity();
        }
        else {

            this.isDeal = false;
            this.isInstore = false;
            this.isCode = false;

            this.coupon.saving = '';
            this.coupon.originalPrice = '';
            this.coupon.dealPrice = '';
            this.horizontalStepperStep1.controls["saving"].markAsUntouched();
            this.horizontalStepperStep1.controls["originalPrice"].markAsUntouched();
            this.horizontalStepperStep1.controls["dealPrice"].markAsUntouched();

            this.horizontalStepperStep1.controls["saving"].setValidators(null);
            this.horizontalStepperStep1Errors.saving = {};
            this.horizontalStepperStep1.controls["saving"].updateValueAndValidity();

            this.horizontalStepperStep1.controls["originalPrice"].setValidators(null);
            this.horizontalStepperStep1Errors.originalPrice = {};
            this.horizontalStepperStep1.controls["originalPrice"].updateValueAndValidity();

            this.horizontalStepperStep1.controls["dealPrice"].setValidators(null);
            this.horizontalStepperStep1Errors.dealPrice = {};
            this.horizontalStepperStep1.controls["dealPrice"].updateValueAndValidity();
        }
    }

    decodeUrl() {

        var linkcode = this.horizontalStepperStep1.controls["linkCode"].value;
        var anchorTagArray: any[];
        anchorTagArray = linkcode.match(/<a[^>]*>([^<]+)<\/a>/g);
        var anchorTag = anchorTagArray[0].toString();
        var href: string;
        href = anchorTag.match(/href="([^\'\"]+)/g)[0];
        var url: string;
        url = href.split('href="')[1];
        console.log("URL: " + url);
        var text: string;
        text = anchorTag.match(/<a [^>]+>([^<]+)<\/a>/)[1];
        console.log("Text: " + text);
        this.coupon.redirectUrl = url;
        this.coupon.description = text;
    }

    testUrl() {
        var externalUrl = this.horizontalStepperStep1.controls["redirectUrl"].value;
        window.open(externalUrl);
    }

    validateUrl() {
        debugger;
        var url = this.horizontalStepperStep1.controls["redirectUrl"].value;
        var valid;


        this.EditCouponService.validateUrl(url, this.merchant.affiliateNetworkId).then(response => {
            debugger;
            if (response.meta.code == "200") {
                valid = response.data;
                if (valid == true) {
                    swal("URL Validated", "You entered a valid URL", "success");
                }
                else {
                    swal("Invalid URL ", "The URL you entered is incorrect!", "error");
                }
            }
        });
    }

    displaySlectedMerchant(selectedMerchant: any): string | undefined {
        //debugger;
        return selectedMerchant ? selectedMerchant.name : undefined;
    }

    compareObjects(obj1: any, obj2: any): boolean {
        return obj1.id === obj2.id;
    }

    OnAddSeasonalEvent(values: any, seasonalEvent: any) {
        debugger;
        if (values.checked == true) {

            var couponFound = false;
            // this.coupon.seasonalEventSet.push(seasonalEvent);
            if (this.coupon.seasonalEventSet.length >= 1) {
                for (let i = 0; i < this.coupon.seasonalEventSet.length; i++) {
                    if (this.coupon.seasonalEventSet[i].id == seasonalEvent.id) {
                        couponFound = true;
                        break;
                    }
                }
            }
            if (couponFound == false) {
                this.coupon.seasonalEventSet.push(seasonalEvent);
            }
        }
        else {
            if (this.coupon.seasonalEventSet.length >= 1) {
                for (let i = 0; i < this.coupon.seasonalEventSet.length; i++) {
                    if (this.coupon.seasonalEventSet[i].id == seasonalEvent.id) {
                        this.coupon.seasonalEventSet.splice(i, 1);
                    }
                }
            }
        }

    }
}

