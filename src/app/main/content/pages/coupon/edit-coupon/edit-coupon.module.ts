import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CdkTableModule } from '@angular/cdk/table';

import { MatFormFieldModule, MatInputModule, MatSelectModule, MatStepperModule, MatButtonModule, MatIconModule, MatRippleModule, MatSidenavModule, MatSlideToggleModule, MatTableModule, MatCheckboxModule, MatAutocompleteModule } from '@angular/material';
import { MatChipsModule } from '@angular/material/chips';

import { FuseSharedModule } from '@fuse/shared.module';

import { EditCouponComponent } from './edit-coupon.component';
import { EditCouponService } from './edit-coupon.service';
import { FormEditCouponComponent } from './form/form-edit-coupon.component';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { FancyImageUploaderModule } from 'ng2-fancy-image-uploader';
import { MatExpansionModule } from '@angular/material/expansion';
import { NgxEditorModule } from 'ngx-editor';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { AngularFontAwesomeModule } from 'angular-font-awesome';

const routes: Routes = [
    {
        path: '**',
        component: EditCouponComponent,
        children: [],
        resolve: {
            users: EditCouponService
        }
    }
];

@NgModule({
    declarations: [
        EditCouponComponent,
        FormEditCouponComponent
    ],
    imports: [
        RouterModule.forChild(routes),
        MatAutocompleteModule,
        CdkTableModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        MatStepperModule,
        MatButtonModule,
        MatIconModule,
        MatRippleModule,
        MatSlideToggleModule,
        MatSidenavModule,
        MatTableModule,
        MatDatepickerModule,
        MatChipsModule,
        FancyImageUploaderModule,
        MatCheckboxModule,
        MatDatepickerModule,
        NgxEditorModule,
        TooltipModule.forRoot(),
        AngularFontAwesomeModule,


        FuseSharedModule
    ],
    providers: [
        EditCouponService
    ]
})
export class EditCouponModule {
}
