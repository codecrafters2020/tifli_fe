import { Component, OnInit, ViewEncapsulation } from '@angular/core';

import { fuseAnimations } from '@fuse/animations';

import { EditCouponService } from './edit-coupon.service';

import { Router } from '@angular/router';

@Component({
    selector     : 'fuse-file-manager',
    templateUrl  : './edit-coupon.component.html',
    styleUrls    : ['./edit-coupon.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class EditCouponComponent implements OnInit
{
    selected: any;
    pathArr: string[];
    users:any[];

    constructor(private EditCouponService: EditCouponService, private router: Router)
    {
    }

    ngOnInit()
    {
        // this.users=this.EditCouponService.users;
        // alert(JSON.stringify(this.users));
        // this.EditCouponService.onUserSelected.subscribe(selected => {
        //     this.selected = selected;
        //     //this.pathArr = selected.location.split('>');
        // });

    }

    openCouponList(){
        
        this.router.navigateByUrl("pages/coupon/list/");
    }

}
