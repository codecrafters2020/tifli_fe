import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CdkTableModule } from '@angular/cdk/table';

import { MatFormFieldModule, MatInputModule, MatSelectModule, MatStepperModule, MatButtonModule, MatIconModule, MatRippleModule, MatSidenavModule, MatSlideToggleModule, MatTableModule, MatCheckboxModule } from '@angular/material';
import {MatChipsModule} from '@angular/material/chips';
import {MatAutocompleteModule} from '@angular/material/autocomplete';

import { FuseSharedModule } from '@fuse/shared.module';

import { AddCouponComponent } from './add-coupon.component';
import { AddCouponService } from './add-coupon.service';
import { FormAddCouponComponent } from './form/form-add-coupon.component';
import { FancyImageUploaderModule } from 'ng2-fancy-image-uploader';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatDatepickerModule} from '@angular/material/datepicker';
import { NgxEditorModule } from 'ngx-editor';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { AngularFontAwesomeModule } from 'angular-font-awesome';


const routes: Routes = [
    {
        path     : '**',
        component: AddCouponComponent,
        children : [],
        resolve  : {
            couponsData: AddCouponService
        }
    }
];

@NgModule({
    declarations: [
        AddCouponComponent,
        FormAddCouponComponent
    ],
    imports     : [
        RouterModule.forChild(routes),
        CdkTableModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        MatStepperModule,
        MatButtonModule,
        MatIconModule,
        MatRippleModule,
        MatSlideToggleModule,
        MatSidenavModule,
        MatTableModule,
        MatChipsModule,
        MatAutocompleteModule,
        MatExpansionModule,
        FancyImageUploaderModule,
        MatCheckboxModule,
        MatDatepickerModule,
        NgxEditorModule,
        TooltipModule.forRoot(),
        AngularFontAwesomeModule,
        
        FuseSharedModule
    ],
    providers   : [
        AddCouponService
    ]
})
export class AddCouponModule
{
}
