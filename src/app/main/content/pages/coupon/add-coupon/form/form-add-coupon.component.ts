
import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators, FormsModule, FormControl } from '@angular/forms';
import { MatSnackBar } from '@angular/material';
import { AddCouponService } from '../add-coupon.service';
import { Router } from '@angular/router';
import { FancyImageUploaderOptions, UploadedFile } from 'ng2-fancy-image-uploader';
import { getLocaleDateTimeFormat } from '@angular/common';
import { AppService } from './../../../../../../app.service';

import * as _swal from 'sweetalert';
import { SweetAlert } from 'sweetalert/typings/core';
import { HttpClient } from '../../../../../../../../node_modules/@angular/common/http';
const swal: SweetAlert = _swal as any;


@Component({
    selector: 'fuse-file-list',
    templateUrl: './form-add-coupon.component.html',
    styleUrls: ['./form-add-coupon.component.scss'],
})
export class FormAddCouponComponent implements OnInit {
    //form: FormGroup;
    formErrors: any;
    test: any;
    step: number;
    merchantSearchResult: any[];

    // Horizontal Stepper
    horizontalStepperStep1: FormGroup;
   // horizontalStepperStep2: FormGroup;
   // horizontalStepperStep3: FormGroup;
    horizontalStepperStep4: FormGroup;

    horizontalStepperStep1Errors: any;
    //horizontalStepperStep2Errors: any;
  //  horizontalStepperStep3Errors: any;
    horizontalStepperStep4Errors: any;


    displayedColumns = ['id', 'name'];
    displayedColumnsPanelHeading = ['Enable', 'Seasonal Event'];

    InstoreImageOptions: FancyImageUploaderOptions = {
        thumbnailHeight: 200,
        thumbnailWidth: 250,
        //uploadUrl: 'https://fancy-image-uploader-demo.azurewebsites.net/api/demo/upload',
        //uploadUrl: 'http://104.42.179.33:8765/cms/api/uploads/',
        uploadUrl: this.appService.uploadService + 'uploads/',
        allowedImageTypes: ['image/png', 'image/jpeg'],
        maxImageSize: 3,
    };

    dealImageOptions: FancyImageUploaderOptions = {
        thumbnailHeight: 200,
        thumbnailWidth: 250,
        //uploadUrl: 'https://fancy-image-uploader-demo.azurewebsites.net/api/demo/upload',
        //uploadUrl: 'http://104.42.179.33:8765/cms/api/uploads/',
        uploadUrl: this.appService.uploadService + 'uploads/',
        allowedImageTypes: ['image/png', 'image/jpeg'],
        maxImageSize: 3,
    };

    coupon = {
        id: '',
        code: '',
        merchant: '',
        title: '',
        startDate: new Date(),
        endDate: null,
        description: '',
        tags: '',
        redirectUrl: '',
        staffPicked: false,
        ongoingOffer: false,
        exclusiveOffer: false,
        freeShipping: false,
        //  showExpiredCoupon: false,
        couponStatus: '',
        alertsEligible: false,
        restrictions: '',
        priority: '',
        clickCount: '0',
        ciUrl: '/coupon-codes/go/rs?k=',
        couponType: '',
        originalPrice: '',
        dealPrice: '',
        saving: '',
        couponCategory: null,
        showSavings: false,
        customButtonText: '',
        hideExpirationDate: false,
        excludeSavingsFromAlgo: false,
        overrideSavingsInTitle: false,
        overrideSavingsInPod: false,
        isCashbackCoupon: false,
        metaType: '',
        metaSaving: '',
        createdBy: 1,
        dateCreated: null,
        modifiedBy: '',
        dateModified: null,
        commission: '0',
        images: [],
        cashbackPercentage: '',
        orderCount: '0',
        seasonalEventSet: []
    }

    merchantsList: any[];
    couponPriorityList: any[];
    couponStatusList: any[];
    couponTypeList: any[];
    seasonalEventList: any[];
    // merchantName: string = "";
    merchant: any;
    couponCategoriesList: any[] = [];
    couponCategory: any;
    redirectUrl: string = "";
    isDeal: boolean = false;
    isInstore: boolean = false;
    isCode: boolean = false;

    constructor(private http: HttpClient,private AddCouponService: AddCouponService, private formBuilder: FormBuilder, private router: Router, private snackBar: MatSnackBar, private appService: AppService) {

        debugger;

        this.merchantsList = AddCouponService.merchantsList;
        this.couponPriorityList = AddCouponService.couponPriorityList;
        this.couponTypeList = AddCouponService.couponTypeList;
        this.couponStatusList = AddCouponService.couponStatusList;
        this.seasonalEventList = AddCouponService.seasonalEventList;
        this.merchant = AddCouponService.merchant_id;

        console.log("here " +AddCouponService.merchant_id );



        this.AddCouponService.getMerchantById(this.merchant).then(response => {
            this.coupon.merchant = response.data;
            console.log("response: " + JSON.stringify(response));
            console.log(this.coupon.merchant);
            //this.couponCategoriesList.pop();
            debugger;
        }

    );

        // Horizontal Stepper form error
        this.horizontalStepperStep1Errors = {
            merchant: {},
            title: {},
            tags: {},
            restrictions: {},
            status: {},
            priority: {},
            startDate: {},
            endDate: {},
            couponType: {},
            couponCategory: {},
            originalPrice: {},
            dealPrice: {},
            saving: {},
            cashbackPercentage: {},
            code: {},
            customButtonText: {},
            metaType: {},
            metaSaving: {},
            redirectUrl: {},
            ciUrl: {}

        };

        // this.horizontalStepperStep3Errors = {
        //     redirectUrl: {},
        //     ciUrl: {}
        // };



    }
  
    // compareTwoDates(){
    //     debugger;
    //     console.log(this.coupon.startDate.valueOf);
    //     // if(new Date(this.form.controls['endDate'].value)<new Date(this.form.controls['startDate'].value)){
    //     //    this.error={isError:true,errorMessage:'End Date can\'t before start date'};
    //     // }
    //  }

    ngOnInit() {
        // Horizontal Stepper form steps
        this.horizontalStepperStep1 = this.formBuilder.group({

            merchant: ['', Validators.required],
            title: ['', [Validators.required, Validators.maxLength(254)]],
            description: [''],
            tags: [''],
            code: ['', [Validators.maxLength(254)]],
            restrictions: [''],
            status: ['', [Validators.required]],
            priority: ['', Validators.required],
            startDate: ['', Validators.required],
            endDate: ['', Validators.required],
            couponType: ['', Validators.required],
            couponCategory: [''],
            originalPrice: ['', [Validators.required, Validators.maxLength(254)]],
            dealPrice: ['', [Validators.required, Validators.maxLength(254)]],
            saving: ['',],
            metaType: ['', [Validators.maxLength(254)]],
            metaSaving: ['', [Validators.maxLength(254)]],
            customButtonText: ['', [Validators.maxLength(254)]],
            ongoingOffer: [''],
            exclusiveOffer: [''],
            freeship: [''],
            //   showExpiredCoupon: [''],
            alertsEligible: [''],
            showSavings: [''],
            hideExpirationDate: [''],
            excludeSavingsFromAlgo: [''],
            overrideSavingsInTitle: [''],
            overrideSavingsInPod: [''],
            isCashbackCoupon: [''],
            staffPicked: [''],
            cashbackPercentage: ['', [Validators.maxLength(254)]],
            redirectUrl: ['', [Validators.required, Validators.maxLength(254)]],
            ciUrl: ['', [Validators.maxLength(254)]],
            linkCode: ['']
        });

        for (let i = 0; i < this.seasonalEventList.length; i++) {
            // debugger;
            this.horizontalStepperStep1.controls[this.seasonalEventList[i].id] = new FormControl(false);
        }

        // this.horizontalStepperStep2 = this.formBuilder.group({

        // });

        // this.horizontalStepperStep3 = this.formBuilder.group({

        //     redirectUrl: ['', [Validators.required, Validators.maxLength(254)]],
        //     ciUrl: ['', [Validators.maxLength(254)]],
        //     linkCode: ['']
        // });


        this.horizontalStepperStep1.valueChanges.subscribe(() => {
            this.onFormValuesChanged(this.horizontalStepperStep1, this.horizontalStepperStep1Errors);
        });

        // this.horizontalStepperStep2.valueChanges.subscribe(() => {
        //     this.onFormValuesChanged(this.horizontalStepperStep2, this.horizontalStepperStep2Errors);
        // });

        // this.horizontalStepperStep3.valueChanges.subscribe(() => {
        //     this.onFormValuesChanged(this.horizontalStepperStep3, this.horizontalStepperStep3Errors);
        // });

        this.horizontalStepperStep1.controls.originalPrice.valueChanges
            .subscribe(() => {
                let op = this.horizontalStepperStep1.controls.originalPrice.value;
                let dp = this.horizontalStepperStep1.controls.dealPrice.value;
                let difference = op - dp;
                this.horizontalStepperStep1.controls['saving'].setValue(difference);
            });

        this.horizontalStepperStep1.controls.dealPrice.valueChanges
            .subscribe(() => {
                let op = this.horizontalStepperStep1.controls.originalPrice.value;
                let dp = this.horizontalStepperStep1.controls.dealPrice.value;
                let difference = op - dp;
                this.horizontalStepperStep1.controls['saving'].setValue(difference);
            });

        this.horizontalStepperStep1.controls.merchant.valueChanges
            .debounceTime(400)
            .subscribe(searchTerm => {
                this.AddCouponService.searchMerchant(searchTerm)
                    .subscribe(merchantNames => {
                        this.merchantSearchResult = merchantNames;
                    });
            });

        this.horizontalStepperStep1.controls.startDate.valueChanges
            .subscribe(() => {
                this.setCouponStatus();
            });

        this.horizontalStepperStep1.controls.endDate.valueChanges
            .subscribe(() => {
                this.setCouponStatus();
            });

    }

    setCouponStatus() {
        debugger
        let nowDate: any = new Date();
        nowDate.setHours(0, 0, 0, 0);
        let startDate:any = this.horizontalStepperStep1.controls.startDate.value;
        startDate.setHours(0, 0, 0, 0);
        let endDate:any = this.horizontalStepperStep1.controls.endDate.value;
        if (endDate !== "" && endDate !== null)
            endDate.setHours(0, 0, 0, 0);
        let beforeStart = nowDate - startDate;
        let beforeEnd = nowDate - endDate;
        let statuschecked = false;
        let statusActive = false;
        let status: any;
        if (endDate === "" || endDate === null) {
            statusActive = true;
            if (statusActive) {
                for (let i = 0; i < this.couponStatusList.length; i++) {
                    if (this.couponStatusList[i].title === "Active") {
                        status = this.couponStatusList[i];
                        statuschecked = true;
                        break;
                    }
                }
            }
        }
        else {
            if (beforeStart > 0 && beforeEnd < 0) {
                for (let i = 0; i < this.couponStatusList.length; i++) {
                    if (this.couponStatusList[i].title === "Active") {
                        status = this.couponStatusList[i];
                        statuschecked = true;
                        break;
                    }
                }
            }
            else if ((beforeStart > 0 || beforeStart == 0) && (beforeEnd < 0 || beforeEnd == 0)) {
                for (let i = 0; i < this.couponStatusList.length; i++) {
                    if (this.couponStatusList[i].title === "Active") {
                        status = this.couponStatusList[i];
                        statuschecked = true;
                        break;
                    }
                }
            }
            else if (beforeEnd > 0) {
                for (let i = 0; i < this.couponStatusList.length; i++) {
                    if (this.couponStatusList[i].title === "Expired") {
                        status = this.couponStatusList[i];
                        statuschecked = true;
                        break;
                    }
                }
            }
            else if (beforeStart < 0) {
                for (let i = 0; i < this.couponStatusList.length; i++) {
                    if (this.couponStatusList[i].title === "Pending") {
                        status = this.couponStatusList[i];
                        statuschecked = true;
                        break;
                    }
                }
            }
        }
        if (statuschecked) {
            this.coupon.couponStatus = status;
            // this.horizontalStepperStep1.controls['status'].setValue(status);
        }
    }

    onFormValuesChanged(horizontalStepperStep: FormGroup, horizontalStepperStepErrors: any) {
        for (const field in horizontalStepperStepErrors) {
            if (!horizontalStepperStepErrors.hasOwnProperty(field)) {
                continue;
            }


            horizontalStepperStepErrors[field] = {};

            const control = horizontalStepperStep.get(field);

            if (control && control.dirty && !control.valid) {
                horizontalStepperStepErrors[field] = control.errors;
            }
        }


        this.horizontalStepperStep1.controls.endDate.valueChanges
            .subscribe(() => {
                let sd = this.horizontalStepperStep1.controls.startDate.value;
                let ed = this.horizontalStepperStep1.controls.endDate.value;

                if ((Date.parse(sd) > Date.parse(ed))) {
                    alert("End date should be greater than Start date");
                    this.horizontalStepperStep1.controls['endDate'].setValue(sd);
                }
            });
            this.horizontalStepperStep1.controls.startDate.valueChanges
            .subscribe(() => {
                let sd = this.horizontalStepperStep1.controls.startDate.value;
                let ed = this.horizontalStepperStep1.controls.endDate.value;

                if ((Date.parse(sd) > Date.parse(ed))) {
                    alert("End date should be greater than Start date");
                    this.horizontalStepperStep1.controls['endDate'].setValue(sd);
                }
            });


    }

    finishHorizontalStepper() {

        // if (this.coupon.saving == '') {
        //     this.coupon.saving = '0.0';
        // }

        if (this.coupon.originalPrice == '') {
            this.coupon.originalPrice = '0.0';
        }

        if (this.coupon.dealPrice == '') {
            this.coupon.dealPrice = '0.0';
        }

        if (this.coupon.cashbackPercentage == '') {
            this.coupon.cashbackPercentage = '0.0';
        }
        debugger;

        this.AddCouponService.addCoupon(this.coupon).then(response => {
            debugger;
            this.test = response;
            console.log("response: " + JSON.stringify(response));
            this.snackBar.open(response.meta.message, "Done", {
                duration: 2000,
            });
            if (response.meta.status == "success")
                this.redirect('pages/coupon/list/');
        });
    }

    redirect(pagename: string) {
        this.router.navigate([pagename]);
    }

    onUploadInstoreImage(file: UploadedFile) {
        debugger;
        var response: any = {};
        response = JSON.parse(file.response);
        if (response.meta.code == 200) {
            if (this.coupon.images.length >= 1) {
                for (let i = 0; i < this.coupon.images.length; i++) {
                    // if (this.coupon.images[i].suffix == "small") {
                    this.coupon.images.splice(i, 1);
                    //}
                }
            }
            var inStoreImage: any = {};
            inStoreImage.fileName = response.data.fileName;
            inStoreImage.suffix = "inStore";
            this.coupon.images.push(inStoreImage);
        }
    }


    onUploadDealImage(file: UploadedFile) {
        debugger;
        var response: any = {};
        response = JSON.parse(file.response)
        if (response.meta.code == 200) {
            if (this.coupon.images.length >= 1) {
                for (let i = 0; i < this.coupon.images.length; i++) {
                    //if (this.coupon.images[i].suffix == "large") {
                    this.coupon.images.splice(i, 1);
                    //}
                }
            }
            var dealImage: any = {};
            dealImage.fileName = response.data.fileName;
            dealImage.suffix = "deal";
            this.coupon.images.push(dealImage);
        }
    }


    populateFields(event: any) {
        console.log("event")
        console.log(event)
        console.log("event.option.value")
        console.log(event.option.value)



        var merchantId = event.option.value.id;
        if (this.couponCategoriesList.length > 0) {
            for (let i = this.couponCategoriesList.length; i > 0; i--) {
                this.couponCategoriesList.pop();
            }
        }
        this.AddCouponService.getMerchantById(merchantId).then(response => {
            this.merchant = response.data;
            console.log("response: " + JSON.stringify(response));
            //this.couponCategoriesList.pop();
            this.couponCategoriesList.push(this.merchant.merchantCategoryId);
            if (this.merchant.categoriesList.length > 0) {
                for (let i = 0; i < this.merchant.categoriesList.length; i++) {
                    if (this.couponCategoriesList[0].id != this.merchant.categoriesList[i].id) {
                        if (this.merchant.categoriesList[i].checked == true) {
                            this.couponCategoriesList.push(this.merchant.categoriesList[i]);
                        }
                    }
                }

            }

            this.coupon.redirectUrl = this.merchant.redirectUrl;

        });

    }

    showOptions(event: any) {
        debugger;

        if (event.value.title == "Deal") {

            this.isDeal = true;
            this.isCode = false;
            this.isInstore = false;

            this.horizontalStepperStep1.controls["code"].markAsUntouched();

            this.horizontalStepperStep1.controls["code"].setValidators(null);
            this.horizontalStepperStep1Errors.code = {};
            this.horizontalStepperStep1.controls["code"].updateValueAndValidity();

        }
        else if (event.value.title == "In-store") {

            this.isDeal = false;
            this.isCode = false;
            this.isInstore = true;

            //  this.coupon.saving = '';
            this.coupon.originalPrice = '';
            this.coupon.dealPrice = '';
            this.horizontalStepperStep1.controls["saving"].markAsUntouched();
            this.horizontalStepperStep1.controls["originalPrice"].markAsUntouched();
            this.horizontalStepperStep1.controls["dealPrice"].markAsUntouched();

            this.horizontalStepperStep1.controls["saving"].setValidators(null);
            this.horizontalStepperStep1Errors.saving = {};
            this.horizontalStepperStep1.controls["saving"].updateValueAndValidity();

            this.horizontalStepperStep1.controls["code"].setValidators(null);
            this.horizontalStepperStep1Errors.code = {};
            this.horizontalStepperStep1.controls["code"].updateValueAndValidity();

            this.horizontalStepperStep1.controls["originalPrice"].setValidators(null);
            this.horizontalStepperStep1Errors.originalPrice = {};
            this.horizontalStepperStep1.controls["originalPrice"].updateValueAndValidity();

            this.horizontalStepperStep1.controls["dealPrice"].setValidators(null);
            this.horizontalStepperStep1Errors.dealPrice = {};
            this.horizontalStepperStep1.controls["dealPrice"].updateValueAndValidity();
        }

        else if (event.value.title == "Code") {

            this.isDeal = false;
            this.isInstore = false;
            this.isCode = true;

            // this.coupon.saving = '';
            this.coupon.originalPrice = '';
            this.coupon.dealPrice = '';
            this.horizontalStepperStep1.controls["saving"].markAsUntouched();
            this.horizontalStepperStep1.controls["originalPrice"].markAsUntouched();
            this.horizontalStepperStep1.controls["dealPrice"].markAsUntouched();

            this.horizontalStepperStep1.controls["saving"].setValidators(null);
            this.horizontalStepperStep1Errors.saving = {};
            this.horizontalStepperStep1.controls["saving"].updateValueAndValidity();

            this.horizontalStepperStep1.controls["originalPrice"].setValidators(null);
            this.horizontalStepperStep1Errors.originalPrice = {};
            this.horizontalStepperStep1.controls["originalPrice"].updateValueAndValidity();

            this.horizontalStepperStep1.controls["dealPrice"].setValidators(null);
            this.horizontalStepperStep1Errors.dealPrice = {};
            this.horizontalStepperStep1.controls["dealPrice"].updateValueAndValidity();
        }
        else {
            debugger;
            this.isDeal = false;
            this.isInstore = false;
            this.isCode = false;

            // this.coupon.saving = '';
            this.coupon.originalPrice = '';
            this.coupon.dealPrice = '';
            this.horizontalStepperStep1.controls["code"].markAsUntouched();
            this.horizontalStepperStep1.controls["saving"].markAsUntouched();
            this.horizontalStepperStep1.controls["originalPrice"].markAsUntouched();
            this.horizontalStepperStep1.controls["dealPrice"].markAsUntouched();

            this.horizontalStepperStep1.controls["saving"].setValidators(null);
            this.horizontalStepperStep1Errors.saving = {};
            this.horizontalStepperStep1.controls["saving"].updateValueAndValidity();

            this.horizontalStepperStep1.controls["code"].setValidators(null);
            this.horizontalStepperStep1Errors.code = {};
            this.horizontalStepperStep1.controls["code"].updateValueAndValidity();

            this.horizontalStepperStep1.controls["originalPrice"].setValidators(null);
            this.horizontalStepperStep1Errors.originalPrice = {};
            this.horizontalStepperStep1.controls["originalPrice"].updateValueAndValidity();

            this.horizontalStepperStep1.controls["dealPrice"].setValidators(null);
            this.horizontalStepperStep1Errors.dealPrice = {};
            this.horizontalStepperStep1.controls["dealPrice"].updateValueAndValidity();
        }
    }

    decodeUrl() {
        debugger;
        var linkcode = this.horizontalStepperStep1.controls["linkCode"].value;
        var anchorTagArray: any[];
        anchorTagArray = linkcode.match(/<a[^>]*>([^<]+)<\/a>/g);
        var anchorTag = anchorTagArray[0].toString();
        var href: string;
        href = anchorTag.match(/href="([^\'\"]+)/g)[0];
        var url: string;
        url = href.split('href="')[1];
        console.log("URL: " + url);
        var text: string;
        text = anchorTag.match(/<a [^>]+>([^<]+)<\/a>/)[1];
        console.log("Text: " + text);
        this.coupon.redirectUrl = url;
        this.coupon.description = text;
    }

    testUrl() {

        var externalUrl = this.horizontalStepperStep1.controls["redirectUrl"].value;
        window.open(externalUrl);
    }

    validateUrl() {
        debugger;
        var url = this.horizontalStepperStep1.controls["redirectUrl"].value;
        var valid;


        this.AddCouponService.validateUrl(url, this.merchant.affiliateNetworkId).then(response => {
            debugger;
            if (response.meta.code == "200") {
                valid = response.data;
                if (valid == true) {
                    swal("URL Validated", "You entered a valid URL", "success");
                }
                else {
                    swal("Invalid URL ", "The URL you entered is incorrect!", "error");
                }
            }
        });
    }

    displaySlectedMerchant(selectedMerchant: any): string | undefined {
        debugger;
        return selectedMerchant ? selectedMerchant.name : undefined;
    }

    OnAddSeasonalEvent(values: any, seasonalEvent: any) {
        debugger;
        if (values.checked == true) {
            this.coupon.seasonalEventSet.push(seasonalEvent);
        }
        else {
            if (this.coupon.seasonalEventSet.length >= 1) {
                for (let i = 0; i < this.coupon.seasonalEventSet.length; i++) {
                    if (this.coupon.seasonalEventSet[i].id == seasonalEvent.id) {
                        this.coupon.seasonalEventSet.splice(i, 1);
                    }
                }
            }
        }

    }


}
