import { Component, OnInit, ViewEncapsulation } from '@angular/core';

import { fuseAnimations } from '@fuse/animations';

import { AddCouponService } from './add-coupon.service';

import { Router } from '@angular/router';

@Component({
    selector     : 'fuse-file-manager',
    templateUrl  : './add-coupon.component.html',
    styleUrls    : ['./add-coupon.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class AddCouponComponent implements OnInit
{
    selected: any;
    pathArr: string[];

    constructor(private AddCouponService: AddCouponService, private router: Router)
    {
    }

    ngOnInit()
    {
        
    }

    openCouponList(){
        
        this.router.navigateByUrl("pages/coupon/list/");
    }
}
