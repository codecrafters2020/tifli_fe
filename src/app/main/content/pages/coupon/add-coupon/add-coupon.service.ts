import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot, ActivatedRoute } from '@angular/router';

import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { AppService } from './../../../../../app.service';

@Injectable()
export class AddCouponService implements Resolve<any>
{
    merchant : any;
    merchantsList: any[];
    couponPriorityList : any[];
    couponStatusList : any[];
    couponTypeList : any[];
    seasonalEventList : any[];
    merchant_id : any;
    constructor( private activatedRoute: ActivatedRoute ,private http: HttpClient, private appService: AppService )
    {
    }

    /**
     * The File Manager App Main Resolver
     * 
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any
    {

  this.activatedRoute.queryParams.subscribe(params => {
        let id = params['id'];
        this.merchant_id=id;
      //  console.log("hi i am id " + this.merchant_id); // Print the parameter to the console. 
    });
        // return new Promise((resolve, reject) => {
        //     debugger;
        //     Promise.all([
        //      this.getMerchants(),
        //      this.getCouponStatus(),
        //      this.getCouponPriority(),
        //      this.getCouponType(),
        //      this.getSeasonalEvents()

        //     ]).then(
        //         ([merchantsList, couponPriorityList, couponStatusList, couponTypeList, seasonalEventList]) => {
        //             resolve();
        //         },
        //         reject);
        // });
    }

    getMerchants(): Promise<any>
    {

        return new Promise((resolve, reject) => {
            this.http.get(this.appService.merchantService)
                .subscribe((response: any) => {
                    console.log(response);
                    if(response.meta.code=="200")
                    {
                        this.merchantsList=response.data;
                    }
                    resolve(response);
                }, reject);
        });
    }  
    
    searchMerchant(term: string): Observable<any> {
        return this.http.get(`${this.appService.merchantService}search/?name=${term}`)
            .map(response => response['data'])
            .map(data => data instanceof Array ? data : [])
    }

    getMerchantById(merchantId ): Promise<any>
    {
        return new Promise((resolve, reject) => {
            this.http.get(this.appService.merchantService + merchantId)
                .subscribe((response: any) => {
                    console.log(response);
                    if(response.meta.code=="200")
                    {
                        this.merchant=response.data;
                    }
                    resolve(response);
                }, reject);
        });
    } 

    validateUrl(url,affiliateNetworkId): Promise<any>
    {

        return new Promise((resolve, reject) => {
            debugger;
            this.http.get(this.appService.merchantService + 'validate/'+affiliateNetworkId+'?couponUrl='+url)
                .subscribe((response: any) => {
                    // console.log(response);
                    // debugger;
                    // if(response.meta.code=="200")
                    // {
                    //     this.merchant=response.data;
                    // }
                    resolve(response);
                }, reject);
        });
    }

    getCouponStatus(): Promise<any>
    {

        return new Promise((resolve, reject) => {
            debugger;
            
            this.http.get(this.appService.couponService + 'status/')
                .subscribe((response: any) => {
                    console.log(response);
                    debugger;
                    if(response.meta.code=="200")
                    {
                        this.couponStatusList=response.data;
                    }
                    resolve(response);
                }, reject);
        });
    }

    getCouponPriority(): Promise<any>
    {

        return new Promise((resolve, reject) => {
            debugger;
            
            this.http.get(this.appService.couponService + 'priority/')
            //this.http.get('http://192.168.1.76:9003/priority/')
                .subscribe((response: any) => {
                    console.log(response);
                    debugger;
                    if(response.meta.code=="200")
                    {
                        this.couponPriorityList=response.data;
                    }
                    resolve(response);
                }, reject);
        });
    }

    getCouponType(): Promise<any>
    {

        return new Promise((resolve, reject) => {
            debugger;
            
            this.http.get(this.appService.couponService + 'type/')
            //this.http.get('http://192.168.1.76:9003/type/')
                .subscribe((response: any) => {
                    console.log(response);
                    debugger;
                    if(response.meta.code=="200")
                    {
                        this.couponTypeList=response.data;
                    }
                    resolve(response);
                }, reject);
        });
    }

    getSeasonalEvents(): Promise<any>
    {

        return new Promise((resolve, reject) => {
            debugger;
            
            this.http.get(this.appService.couponService + 'seasonal/')
            //this.http.get('http://192.168.1.76:9003/type/')
                .subscribe((response: any) => {
                    console.log(response);
                    debugger;
                    if(response.meta.code=="200")
                    {
                        this.seasonalEventList=response.data;
                    }
                    resolve(response);
                }, reject);
        });
    }


    addCoupon(coupon): Promise<any>
    {
        return new Promise((resolve, reject) => {
            debugger;
            console.log("URL")
            console.log(this.appService.couponService + 'coupons/')
            console.log("JSON")
            console.log(coupon)
            this.http.post(this.appService.couponService + 'coupons/',coupon)
            //this.http.post('http://192.168.1.76:9005/',coupon)              
                .subscribe((response: any) => {
                    resolve(response);
                }, reject);
        });
    }

}
