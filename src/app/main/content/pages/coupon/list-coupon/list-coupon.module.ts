import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CdkTableModule } from '@angular/cdk/table';

import { MatAutocompleteModule, MatFormFieldModule, MatInputModule, MatSelectModule, MatStepperModule, MatButtonModule, MatIconModule, MatRippleModule, MatSidenavModule, MatSlideToggleModule, MatTableModule, MatMenuModule, MatPaginatorModule, MatSortModule } from '@angular/material';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';

import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatRadioModule} from '@angular/material/radio';
import { FuseSharedModule } from '@fuse/shared.module';

import { ListCouponComponent } from './list-coupon.component';
import { ListCouponService } from './list-coupon.service';
import { GridCouponComponent } from './grid/grid-coupon.component';
import { SidenavCouponMainComponent } from './sidenavs/main/sidenav-coupon-main.component';
import { SidenavCouponDetailsComponent } from './sidenavs/details/sidenav-coupon-details.component';

const routes: Routes = [
    {
        path     : '**',
        component: ListCouponComponent,
        children : [],
        resolve  : {
            files: ListCouponService
        }
    }
];

@NgModule({
    declarations: [
        ListCouponComponent,
        GridCouponComponent,
        SidenavCouponMainComponent,
        SidenavCouponDetailsComponent
    ],
    imports     : [
        RouterModule.forChild(routes),

        CdkTableModule,
        MatAutocompleteModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        MatStepperModule,
        MatButtonModule,
        MatIconModule,
        MatRippleModule,
        MatSlideToggleModule,
        MatSidenavModule,
        MatTableModule,
        MatMenuModule,
        MatPaginatorModule, 
        MatSortModule,
        MatProgressSpinnerModule,

        FuseSharedModule,
        MatDatepickerModule,
        MatCheckboxModule,
        MatRadioModule
    ],
    providers   : [
        ListCouponService
    ]
})
export class ListCouponModule
{
}
