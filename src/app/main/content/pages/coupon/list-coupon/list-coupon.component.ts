import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, FormArray, Validators, EmailValidator } from '@angular/forms';
import { fuseAnimations } from '@fuse/animations';

import { ListCouponService } from './list-coupon.service';
import { Router } from '@angular/router';

@Component({
    selector: 'fuse-file-manager',
    templateUrl: './list-coupon.component.html',
    styleUrls: ['./list-coupon.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class ListCouponComponent implements OnInit {
    selected: any;
    pathArr: string[];
    isSearch: boolean = false;
    form: FormGroup;
    merchantSearchResult: any[];


    constructor(private ListCouponService: ListCouponService, private router: Router, private formBuilder: FormBuilder) {
      //  this.ListCouponService.getCoupons();
        this.form = this.formBuilder.group({
            merchantName : ['', ]})

    }

    ngOnInit() {
        this.ListCouponService.onFileSelected.subscribe(selected => {
            this.selected = selected;
            // this.pathArr = selected.location.split('>');
        });

        this.form.controls.merchantName.valueChanges
        .debounceTime(400)
        .subscribe(searchTerm => {
    			this.ListCouponService.searchMerchant(searchTerm)
    				.subscribe(merchantNames => {
    					this.merchantSearchResult = merchantNames;
    				});
        });
    }

    openCouponAdd() {
        this.router.navigateByUrl("pages/coupon/add");

    }

    resetGrid() {
        this.ListCouponService.getfaculty();
    }
    
    searching() {

        if(this.isSearch == false){
            this.isSearch = true;
        }

        else{
            this.isSearch = false;
            console.log("close it")
        }
    }
}
