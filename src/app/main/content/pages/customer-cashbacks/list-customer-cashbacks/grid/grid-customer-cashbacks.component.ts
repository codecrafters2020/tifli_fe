import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort } from '@angular/material';
import { DataSource } from '@angular/cdk/collections';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';

import { ListCustomerCashbacksService } from '../list-customer-cashbacks.service';
import { fuseAnimations } from '@fuse/animations/index';
import { FuseUtils } from '@fuse/utils';

@Component({
    selector: 'fuse-file-list',
    templateUrl: './grid-customer-cashbacks.component.html',
    styleUrls: ['./grid-customer-cashbacks.component.scss'],
    animations: fuseAnimations
})
export class GridCustomerCashbacksComponent implements OnInit {
    files: any;
    dataSource: GridUserDataSource | null;
    displayedColumns = ['customerId', 'customerName', 'customerEmail', 'cashbackBalance', 'modified', 'detail-button'];
    selected: any;
    // pagination
    pageSize: number;
    pageSizeOptions: number[];

    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild('filter') filter: ElementRef;
    @ViewChild(MatSort) sort: MatSort;

    constructor(private ListCustomerCashbacksService: ListCustomerCashbacksService, private router: Router) {
        this.pageSize = 25;
        this.pageSizeOptions = [this.pageSize, this.pageSize * 2, this.pageSize * 4]; 
        // this.ListCouponService.onFilesChanged.subscribe(files => {
        //     this.files = files;
        // });
        // this.ListCouponService.onFileSelected.subscribe(selected => {
        //     this.selected = selected;
        // });
    }

    ngOnInit() {
        // debugger;
        // this.dataSource = new GridUserDataSource(this.ListCouponService, this.paginator, this.sort);
        this.dataSource = new GridUserDataSource(this.ListCustomerCashbacksService, this.paginator, this.sort);
        // Observable.fromEvent(this.filter.nativeElement, 'keyup')
        //           .debounceTime(150)
        //           .distinctUntilChanged()
        //           .subscribe(() => {
        //               if ( !this.dataSource )
        //               {
        //                   return;
        //               }
        //               this.dataSource.filter = this.filter.nativeElement.value;
        //           });
    }

    onSelect(selected) {
        this.ListCustomerCashbacksService.onFileSelected.next(selected);
    }

    openUserDashboard(event, userid) {
        this.router.navigateByUrl("apps/dashboards/project/" + userid);
    }

    openCustomerCashbackHistory(event, id) {
        this.router.navigateByUrl("pages/customer-cashback-history/list/"+id);
    }
}

export class GridUserDataSource extends DataSource<any>
{
    _filterChange = new BehaviorSubject('');
    _filteredDataChange = new BehaviorSubject('');

    get filteredData(): any {
        return this._filteredDataChange.value;
    }

    set filteredData(value: any) {
        this._filteredDataChange.next(value);
    }

    get filter(): string {
        return this._filterChange.value;
    }

    set filter(filter: string) {
        this._filterChange.next(filter);
    }
    constructor(private ListCustomerCashbacksService: ListCustomerCashbacksService, private _paginator: MatPaginator, private _sort: MatSort) {
        super();
        this.filteredData = this.ListCustomerCashbacksService.customerCashbacks;
    }

    /** Connect function called by the table to retrieve one stream containing the data to render. */
    // connect(): Observable<any[]>
    // {
    //     return this.ListCouponService.onFilesChanged;
    // }
    connect(): Observable<any[]> {
        const displayDataChanges = [
            this.ListCustomerCashbacksService.onFilesChanged,
            this._paginator.page,
            this._filterChange,
            // this._sort.sortChange
        ];

        return Observable.merge(...displayDataChanges).map(() => {
            let data = this.ListCustomerCashbacksService.customerCashbacks.slice();

            data = this.filterData(data);

            this.filteredData = [...data];

            //data = this.sortData(data);

            // Grab the page's slice of data.
            const startIndex = this._paginator.pageIndex * this._paginator.pageSize;
            return data.splice(startIndex, this._paginator.pageSize);
        });
    }

    filterData(data) {
        if (!this.filter) {
            return data;
        }
        return FuseUtils.filterArrayByString(data, this.filter);
    }

    // sortData(data): any[]
    // {
    //     if ( !this._sort.active || this._sort.direction === '' )
    //     {
    //         return data;
    //     }

    //     return data.sort((a, b) => {
    //         let propertyA: number | string = '';
    //         let propertyB: number | string = '';

    //         switch ( this._sort.active )
    //         {
    //             case 'id':
    //                 [propertyA, propertyB] = [a.id, b.id];
    //                 break;
    //             case 'name':
    //                 [propertyA, propertyB] = [a.name, b.name];
    //                 break;
    //             case 'categories':
    //                 [propertyA, propertyB] = [a.categories[0], b.categories[0]];
    //                 break;
    //             case 'price':
    //                 [propertyA, propertyB] = [a.priceTaxIncl, b.priceTaxIncl];
    //                 break;
    //             case 'quantity':
    //                 [propertyA, propertyB] = [a.quantity, b.quantity];
    //                 break;
    //             case 'active':
    //                 [propertyA, propertyB] = [a.active, b.active];
    //                 break;
    //         }

    //         const valueA = isNaN(+propertyA) ? propertyA : +propertyA;
    //         const valueB = isNaN(+propertyB) ? propertyB : +propertyB;

    //         return (valueA < valueB ? -1 : 1) * (this._sort.direction === 'asc' ? 1 : -1);
    //     });
    // }

    disconnect() {
    }
}
