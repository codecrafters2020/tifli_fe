import { Component, OnInit, ViewEncapsulation } from '@angular/core';

import { fuseAnimations } from '@fuse/animations';

import { ListCustomerCashbacksService } from './list-customer-cashbacks.service';
import { Router } from '@angular/router';

@Component({
    selector     : 'fuse-file-manager',
    templateUrl  : './list-customer-cashbacks.component.html',
    styleUrls    : ['./list-customer-cashbacks.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class ListCustomerCashbacksComponent implements OnInit
{
    selected: any;
    pathArr: string[];

    constructor(private ListCustomerCashbacksService: ListCustomerCashbacksService, private router: Router)
    {
    }

    ngOnInit()
    {
        this.ListCustomerCashbacksService.onFileSelected.subscribe(selected => {
            this.selected = selected;
           // this.pathArr = selected.location.split('>');
        });
    }

    openCouponAdd()
    {
        this.router.navigateByUrl("pages/coupon/add");
    }

    resetGrid()
    {
        this.ListCustomerCashbacksService.getCustomerCashbacks();
    }
}
