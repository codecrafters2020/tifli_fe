import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';
import { AbstractControl, FormBuilder, FormGroup, FormArray, Validators, EmailValidator } from '@angular/forms';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatRadioModule } from '@angular/material/radio';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { FormControl } from '@angular/forms';
import { Observable, BehaviorSubject } from 'rxjs';
import { of } from 'rxjs/observable/of';
import { map, startWith } from 'rxjs/operators';
import { FuseAngularMaterialModule } from '../../../../../components/angular-material/angular-material.module';

import { ListCustomerCashbacksService } from '../../list-customer-cashbacks.service';


@Component({
  selector: 'fuse-file-manager-main-sidenav',
  templateUrl: './sidenav-customer-cashbacks-main.component.html',
  styleUrls: ['./sidenav-customer-cashbacks-main.component.scss']
})

export class SidenavCustomerCashbacksMainComponent {
  @Input() leftSideNav: any;

  form: FormGroup;
  formErrors: any;

  customerSearchResult: any[];

  couponTypes = ['Deal', 'In-store', 'Code'];
  couponStatuses = ['Active', 'Inactive'];
  cashbackBalanceList = ['0-500','500-1000','Above 1000']

  onGoingOffers: boolean;
  exclusiveOffers: boolean;
  expiringSoon: boolean;
  popularOffers: boolean;

  enableOngoingOffers: boolean = false;
  enableExclusiveOffers: boolean = false;
  enableExpiringSoon: boolean = false;
  enablePopularOffers: boolean = false;

  btnDisabled: boolean = false;

  constructor(private ListCustomerCashbacksService: ListCustomerCashbacksService,
    private router: Router,
    private formBuilder: FormBuilder) {
    // Reactive Form
    this.form = this.formBuilder.group({
      customerId: ['',],
      customerName: ['',],
      customerEmailAddress: ['',],
      cashbackBalance: ['',]
    });

    // Reactive form errors
    this.formErrors = {

    };
  }

  filter() {
    debugger;
    // this.btnDisabled = true;
    this.ListCustomerCashbacksService.getFilteredCustomerCashbacks('',
      '',
      'customerId,asc',
      this.form.controls.customerId.value,
      this.form.controls.customerName.value,
      this.form.controls.customerEmailAddress.value,
      this.form.controls.cashbackBalance.value)
      .then((response) => {
        this.btnDisabled = false;
        this.leftSideNav.toggle()
      })
  }

  ngOnInit() {
    this.form.valueChanges.subscribe(() => {
      // console.log(this.form.value);
    });

    // this.form.controls.customerName.valueChanges
    //   .debounceTime(400)
    //   .subscribe(searchTerm => {
    //     this.ListCustomerCashbacksService.searchCustomer(searchTerm)
    //       .subscribe(response => {
    //         this.customerSearchResult = response;
    //       });
    //   });

    // this.form.controls.customerEmail.valueChanges
    //   .debounceTime(400)
    //   .subscribe(searchTerm => {
    //     this.ListCustomerCashbacksService.searchCustomer(searchTerm)
    //       .subscribe(response => {
    //         this.customerSearchResult = response;
    //       });
    //   });

  }

  displaySelectedName(item: any) {
    return item;
  }
}


