import { Component, OnInit } from '@angular/core';

import { fuseAnimations } from '@fuse/animations/index';

import { ListCustomerCashbacksService } from '../../list-customer-cashbacks.service';

@Component({
    selector   : 'fuse-file-manager-details-sidenav',
    templateUrl: './sidenav-customer-cashbacks-details.component.html',
    styleUrls  : ['./sidenav-customer-cashbacks-details.component.scss'],
    animations : fuseAnimations
})
export class SidenavCustomerCashbacksDetailsComponent implements OnInit
{

    selected: any;

    constructor(private ListCustomerCashbacksService: ListCustomerCashbacksService)
    {

    }

    ngOnInit()
    {
        this.ListCustomerCashbacksService.onFileSelected.subscribe(selected => {
            this.selected = selected;
        });
    }

}
