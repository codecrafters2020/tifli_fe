import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CdkTableModule } from '@angular/cdk/table';

import { MatAutocompleteModule, MatFormFieldModule, MatInputModule, MatSelectModule, MatStepperModule, MatButtonModule, MatIconModule, MatRippleModule, MatSidenavModule, MatSlideToggleModule, MatTableModule, MatMenuModule, MatPaginatorModule, MatSortModule } from '@angular/material';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';

import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatRadioModule} from '@angular/material/radio';
import { FuseSharedModule } from '@fuse/shared.module';

import { ListCustomerCashbacksComponent } from './list-customer-cashbacks.component';
import { ListCustomerCashbacksService } from './list-customer-cashbacks.service';
import { GridCustomerCashbacksComponent } from './grid/grid-customer-cashbacks.component';
import { SidenavCustomerCashbacksMainComponent } from './sidenavs/main/sidenav-customer-cashbacks-main.component';
import { SidenavCustomerCashbacksDetailsComponent } from './sidenavs/details/sidenav-customer-cashbacks-details.component';

const routes: Routes = [
    {
        path     : '**',
        component: ListCustomerCashbacksComponent,
        children : [],
        resolve  : {
            files: ListCustomerCashbacksService
        }
    }
];

@NgModule({
    declarations: [
        ListCustomerCashbacksComponent,
        GridCustomerCashbacksComponent,
        SidenavCustomerCashbacksMainComponent,
        SidenavCustomerCashbacksDetailsComponent
    ],
    imports     : [
        RouterModule.forChild(routes),

        CdkTableModule,
        MatAutocompleteModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        MatStepperModule,
        MatButtonModule,
        MatIconModule,
        MatRippleModule,
        MatSlideToggleModule,
        MatSidenavModule,
        MatTableModule,
        MatMenuModule,
        MatPaginatorModule, 
        MatSortModule,
        MatProgressSpinnerModule,

        FuseSharedModule,
        MatDatepickerModule,
        MatCheckboxModule,
        MatRadioModule
    ],
    providers   : [
        ListCustomerCashbacksService
    ]
})
export class ListCustomerCashbacksModule
{
}
