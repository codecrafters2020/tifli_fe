import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';

import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of'
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { AppService } from './../../../../../app.service';

import * as moment from 'moment';
import * as _ from 'lodash';

@Injectable()
export class ListCustomerCashbacksService implements Resolve<any>
{
    coupons: any[];
    customerCashbacks: any[];
    onFilesChanged: BehaviorSubject<any> = new BehaviorSubject({});
    onFileSelected: BehaviorSubject<any> = new BehaviorSubject({});

    constructor(private http: HttpClient, private appService: AppService) {
    }

    /**
     * The File Manager App Main Resolver
     * 
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {

        // return new Promise((resolve, reject) => {

        //     Promise.all([
        //         this.getCustomerCashbacks()
        //     ]).then(
        //         ([customerCashbacks]) => {
        //             resolve();
        //         },
        //         reject
        //     );
        // });
    }

    getCustomerCashbacks(): Promise<any> {
        return new Promise((resolve, reject) => {
            this.http.get(`${this.appService.reportService}consumer/cashback?page=0&size=50&sorted=customerId,asc`)
                .subscribe((response: any) => {
                    debugger;
                    this.customerCashbacks = response.data.content;
                    this.onFilesChanged.next(response.data);
                    this.onFileSelected.next(response[0]);
                    resolve(response);
                }, reject);
        });
    }


    getMerchants(): Promise<any> {
        return new Promise((resolve, reject) => {
            this.http.get(`${this.appService.merchantService}`)
                .subscribe((response: any) => {
                    this.coupons = response.data;
                    this.onFilesChanged.next(response.data);
                    this.onFileSelected.next(response[0]);
                    resolve(response);
                }, reject);
        });
    }

    getFilteredCustomerCashbacks(page: number | string,size: number | string,sorted: string,
        customerId: string | number,customerName: string,customerEmailAddress: string,cashbackBalance: string): Promise<any> {

        var cashbackBalanceLowerLimit;
        var cashbackBalanceHigherLimit;
        var cashbackBalanceGreaterThan;
        let cashbackQueryString='';

        if (cashbackBalance == '0-500') {
            cashbackBalanceLowerLimit = 0;
            cashbackBalanceHigherLimit = 500;
            cashbackQueryString='cashbackBalance='+cashbackBalanceLowerLimit+'&cashbackBalance='+cashbackBalanceHigherLimit;
        }
        else if (cashbackBalance == '500-1000') {
            cashbackBalanceLowerLimit = 500;
            cashbackBalanceHigherLimit = 1000;
            cashbackQueryString='cashbackBalance='+cashbackBalanceLowerLimit+'&cashbackBalance='+cashbackBalanceHigherLimit;
        }
        else  if (cashbackBalance == 'Above 1000'){
            cashbackBalanceGreaterThan = 1000;
            cashbackQueryString='cashbackBalance='+cashbackBalanceGreaterThan;

        }
        let params;
        if (cashbackBalanceGreaterThan == null) {
            params = {
                page,
                size,
                sorted,
                customerId,
                customerName,
                customerEmailAddress,
            };
        }
        else{
            params = {
                page,
                size,
                sorted,
                customerId,
                customerName,
                customerEmailAddress
            };
        }

        let queryString = '';
        _.forEach(params, (val, key) => {
            if (val || typeof val === 'number') {
                queryString = queryString + key + '=' + val + '&';
            }
        });


        return new Promise((resolve, reject) => {
            // this.http.get('api/coupon-list')
            // this.http.get(`http://104.42.179.33:8765/cms/api/coupons?page=0&size=20&sort=couponCode,asc&searchTerms=gag&couponCode=123&couponType=Deal`)
            this.http.get(`${this.appService.reportService}consumer/cashback?${queryString}${cashbackQueryString}`)
                .subscribe((response: any) => {
                    debugger;
                    this.customerCashbacks = response.data.content;
                    this.onFilesChanged.next(response.data);
                    this.onFileSelected.next(response[0]);
                    resolve(response);
                }, reject);
        });
    }

    // searchCustomerName(term: string): Observable<any> {
    //     return this.http.get(`${this.appService.merchantService}search/?name=${term}`)
    //         .map(response => response['data'])
    //         .map(data => data instanceof Array ? data : [])
    // }

    searchCustomer(term: string): Observable<any> {
        return this.http.get(`${this.appService.merchantService}search/?name=${term}`)
            .map(response => response['data'])
            .map(data => data instanceof Array ? data : [])
    }

}
