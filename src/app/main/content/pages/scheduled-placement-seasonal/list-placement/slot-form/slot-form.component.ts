import { Component, Inject, ViewEncapsulation, AfterViewChecked } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef, MatDialog } from '@angular/material';

import { MatColors } from '@fuse/mat-colors';

import { CalendarEvent } from 'angular-calendar';
import { AppService } from './../../../../../../app.service';
import { ListMerchantService } from '../list-merchant.service';
import { invalid } from 'moment';
import { DISABLED } from '@angular/forms/src/model';
// import { SlotModel } from '../slot.model';
// import { ScheduledPlacementService} from '../scheduled-placement.service';

@Component({
    selector: 'fuse-calendar-event-form-dialog',
    templateUrl: './slot-form.component.html',
    styleUrls: ['./slot-form.component.scss'],
    encapsulation: ViewEncapsulation.None
})

export class FuseCalendarEventFormDialogComponent {

    btndisable: boolean = false;
    /** Meta Data for Carousel Popup */
    placementTypes: any[] = [
        "Coupon",
        "Merchant",
        "Seasonal Page",
        "Category",
        "In house promo"
    ];

    positionValues: any[] = [
        1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15
    ];



    /** Meta data ended */

    /** Slot Object */
    slot: any = {
        couponId: 0,
        position: 1,
        Date: '',
        id: 0
    };


    dialogTitle: string = "Placement";
    slotForm: FormGroup;
    action: string = "add";

    merchantSearchResult: any[] = [
        { name: 'Usman', id: 1 },
        { name: 'Farrukh', id: 2 },
        { name: 'Umair', id: 3 }
    ];
    couponSearchResult: any[] = [
        { title: 'Save upto 30% on all samsung coupons', id: 1 },
        { title: 'Save upto 40% on all samsung coupons', id: 2 }
    ];

    selectedMerchant: any = {};
    selectedCoupon: any = { title: 'Save upto 40% on all samsung coupons', id: 2 };
    selectedId: any;
    couponAttached: boolean = false;
    isTypeCoupon: boolean = false;
    buttonAction: any = "add";

    constructor(
        public dialogRef: MatDialogRef<FuseCalendarEventFormDialogComponent>,
        @Inject(MAT_DIALOG_DATA) private data: any,
        private formBuilder: FormBuilder,
        private carouselPlacementService: ListMerchantService,
        private appService: AppService
        // private scheduledPlacementService: ScheduledPlacementService
    ) {

        this.action = data.action;

        if (this.action == "addDatePositionPlacement") {
            this.slot.position = data.position;
            this.slot.Date = new Date(data.date);

        }
        else if (this.action == "editPlacement") {
            debugger;
            this.slot.Date = new Date(data.date);
            this.slot.position = data.position;
            this.slot.couponId = data.data.cid;
            this.selectedId = this.slot.couponId;
            this.slot.id = this.data.slotId;

        }


        this.subscribeMerchants();
        this.slotForm = this.createSlotForm();
        this.getCoupon();
    }

    subscribeMerchants() {
        this.carouselPlacementService.onMerchantsChanged.subscribe(merchantsUpdatedList => {
            debugger;
            this.merchantSearchResult = merchantsUpdatedList;
        });
    }


    getCoupon() {
        debugger;
        this.slotForm.controls.couponId.valueChanges
            .debounceTime(400)
            .subscribe(couponId => {
                debugger;
                if (!(couponId === undefined || couponId === null)) {
                    this.carouselPlacementService.getCoupon(couponId).then(response => {
                        debugger;
                        if (!(response.meta.message == "success" && response.meta.status == "success")) {
                            this.couponAttached = false;
                            this.btndisable = true;

                        }
                        else {
                            this.btndisable = false;
                            this.couponAttached = true;
                            this.selectedCoupon = response.data;
                            var merchantId = this.selectedCoupon.merchant.id;

                            this.carouselPlacementService.getMerchant(merchantId).then(response => {
                                this.selectedCoupon.merchant = response.data;
                            });

                        }
                    });
                }

            });

    }







    createSlotForm() {
        return new FormGroup({
            couponId: new FormControl(),
            position: new FormControl(),
            Date: new FormControl(),
        });
    }

    // displaySlectedMerchant(selectedMerchant: any): string | undefined {
    //     debugger;
    //     if (selectedMerchant != null) {
    //         this.slot.itemId = selectedMerchant.id;
    //     }

    //     return selectedMerchant ? selectedMerchant.name : undefined;
    // }



    displaySlectedCoupon(selectedCoupon: any): string | undefined {
        debugger;
        // this.selectedCoupon = selectedCoupon;
        // if(this.selectedCoupon.merchant != null || this.selectedCoupon.merchant != undefined)
        // {
        //     this.dialogTitle = this.position + " - " + this.selectedCoupon.id + " " + this.selectedCoupon.merchant.name;
        //     this.eventForm.controls.title.setValue(this.position + " - " + this.selectedCoupon.id + "<br>" + this.selectedCoupon.merchant.name);
        //     this.eventForm.controls.merchant.setValue(this.selectedCoupon.merchant);
        // }
        return selectedCoupon ? selectedCoupon.title : undefined;
    }



    closeDialog(action) {
        debugger;
        this.slot.couponId = this.selectedId;

        let data = {
            slot: this.slot,
            action: action

        }

        console.log(this.slot)

        this.dialogRef.close(data);
    }


}
