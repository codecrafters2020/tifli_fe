import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { FuseAngularMaterialModule } from '../../components/angular-material/angular-material.module';

import { FuseSharedModule } from '@fuse/shared.module';

const routes = [
    {
        path        : 'list',
        loadChildren: './list-affiliate-network/list-affiliate-network.module#ListAffiliateNetworkModule'
    },
    {
        path        : 'add',
        loadChildren: './add-affiliate-network/add-affiliate-network.module#AddAffiliateNetworkModule'
    },
    {
        path        : 'edit/:id',
        loadChildren: './edit-affiliate-network/edit-affiliate-network.module#EditAffiliateNetworkModule'
    }
];

@NgModule({
    imports     : [
        FuseSharedModule,
        RouterModule.forChild(routes),
        FuseAngularMaterialModule
    ],
    declarations: []
})
export class AffiliateNetworkModule
{
}
