import { Component, OnInit } from '@angular/core';
import {AbstractControl, FormBuilder, FormGroup, Validators,EmailValidator } from '@angular/forms';
import { DataSource } from '@angular/cdk/collections';
import { Observable } from 'rxjs/Observable';
import { EditAffiliateNetworkService } from '../edit-affiliate-network.service';
import {MatSnackBar} from '@angular/material';
import { Router } from '@angular/router';
import { delay } from 'rxjs/operator/delay';

@Component({
    selector   : 'fuse-file-list',
    templateUrl: './form-edit-affiliate-network.component.html',
    styleUrls  : ['./form-edit-affiliate-network.component.scss'],
})
export class FormEditAffiliateNetworkComponent implements OnInit
{
    
    affiliate:any;
    data:any;
    form: FormGroup;
    formErrors: any;

    statusList = ['Active', 'Inactive', 'Pending'];
    
    constructor(private formBuilder: FormBuilder, private EditAffiliateNetworkService: EditAffiliateNetworkService, private router: Router, private snackBar: MatSnackBar)
    {
        debugger;

        this.affiliate=this.EditAffiliateNetworkService.affiliate;

        this.affiliate.dateCreated=new Date(this.affiliate.dateCreated);
        this.affiliate.dateModified=new Date(this.affiliate.dateModified);

        // Reactive form errors
        this.formErrors = {
            name : {},
            status  : {},
            apiUrl : {},
            publisherId : {}
        };
    }

    ngOnInit()
    {
        // Reactive Form
        this.form = this.formBuilder.group({
            name : ['', Validators.required],
            status  : ['', Validators.required],
            apiUrl :  ['', Validators.required],
            publisherId :  ['', Validators.required]
        });

        this.form.valueChanges.subscribe(() => {
            this.onFormValuesChanged();
        });

    }

    // openAffiliateList(){
        
    //     setTimeout(()=>{this.router.navigateByUrl("pages/affiliate-network/list/")},1000)
        
    // }
    

    // onSelect(selected)
    // {
    //     this.EditAffiliateNetworkService.onUserSelected.next(selected);
    // }

    onFormValuesChanged()
    {
        for ( const field in this.formErrors )
        {
            if ( !this.formErrors.hasOwnProperty(field) )
            {
                continue;
            }

            // Clear previous errors
            this.formErrors[field] = {};

            // Get the control
            const control = this.form.get(field);

            if ( control && control.dirty && !control.valid )
            {
                this.formErrors[field] = control.errors;
            }
        } 
    }

    
    onSubmit() {
        debugger;
        if (this.form.valid) {
            this.EditAffiliateNetworkService.updateAffiliateNetwork(this.affiliate).then(response => {
                debugger;
                this.snackBar.open(response.meta.message,"Done", {
                    duration: 2000,
                  });
                    this.router.navigateByUrl("pages/affiliate-network/list/")
                //   if(response.meta.status == "success")
                //   this.redirect('pages/administration/list-user');
            });
        }
         
    }

    redirect(pagename: string) {
        this.router.navigate([pagename]);
    }

}


