import { Component, OnInit, ViewEncapsulation } from '@angular/core';

import { fuseAnimations } from '@fuse/animations';

import { EditAffiliateNetworkService } from './edit-affiliate-network.service';

import { Router } from '@angular/router';

@Component({
    selector     : 'fuse-file-manager',
    templateUrl  : './edit-affiliate-network.component.html',
    styleUrls    : ['./edit-affiliate-network.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class EditAffiliateNetworkComponent implements OnInit
{
    selected: any;
    pathArr: string[];
    users:any[];

    constructor(private EditAffiliateNetworkService: EditAffiliateNetworkService, private router: Router)
    {
    }

    ngOnInit()
    {
        // this.users=this.EditAffiliateNetworkService.users;
        // alert(JSON.stringify(this.users));
        // this.EditAffiliateNetworkService.onUserSelected.subscribe(selected => {
        //     this.selected = selected;
        //     //this.pathArr = selected.location.split('>');
        // });

    }

    openAffiliateList(){
        
        this.router.navigateByUrl("pages/affiliate-network/list/");
    }

}
