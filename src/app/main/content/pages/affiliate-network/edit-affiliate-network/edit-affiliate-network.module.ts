import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CdkTableModule } from '@angular/cdk/table';

import { MatFormFieldModule, MatInputModule, MatSelectModule, MatStepperModule, MatButtonModule, MatIconModule, MatRippleModule, MatSidenavModule, MatSlideToggleModule, MatTableModule } from '@angular/material';
import {MatChipsModule} from '@angular/material/chips';

import { FuseSharedModule } from '@fuse/shared.module';

import { EditAffiliateNetworkComponent } from './edit-affiliate-network.component';
import { EditAffiliateNetworkService } from './edit-affiliate-network.service';
import { FormEditAffiliateNetworkComponent } from './form/form-edit-affiliate-network.component';
import {MatDatepickerModule} from '@angular/material/datepicker';

const routes: Routes = [
    {
        path     : '**',
        component: EditAffiliateNetworkComponent,
        children : [],
        resolve  : {
            users: EditAffiliateNetworkService
        }
    }
];

@NgModule({
    declarations: [
        EditAffiliateNetworkComponent,
        FormEditAffiliateNetworkComponent
    ],
    imports     : [
        RouterModule.forChild(routes),

        CdkTableModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        MatStepperModule,
        MatButtonModule,
        MatIconModule,
        MatRippleModule,
        MatSlideToggleModule,
        MatSidenavModule,
        MatTableModule,
        MatDatepickerModule,
        MatChipsModule,

        FuseSharedModule
    ],
    providers   : [
        EditAffiliateNetworkService
    ]
})
export class EditAffiliateNetworkModule
{
}
