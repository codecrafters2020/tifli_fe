import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';

import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { AppService } from './../../../../../app.service';

@Injectable()
export class EditAffiliateNetworkService implements Resolve<any>
{
    affiliate:any;
    id:any;

    constructor(private http: HttpClient, private appService: AppService)
    {
    }

    /**
     * The File Manager App Main Resolver
     * 
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any
    {
        this.id=route.params['id'];

    //     return new Promise((resolve, reject) => {
    //         debugger;
    //         Promise.all([
    //          this.getAffiliateNetwork(),
    //         ]).then(
    //             ([users]) => {
    //                 resolve();
    //             },
    //             reject);
    //     });
     }

    getAffiliateNetwork(): Promise<any>
    {

        return new Promise((resolve, reject) => {
            debugger;
            this.http.get(this.appService.merchantService + 'affiliates/'+this.id)
                .subscribe((response: any) => {
                    console.log(response);
                    debugger;
                    if(response.meta.code=="200")
                    {
                        this.affiliate=response.data;
                    }
                    
                    resolve(response);
                }, reject);
        });
    }

    updateAffiliateNetwork(affiliate): Promise<any>
    {
        return new Promise((resolve, reject) => {
            debugger;
            this.http.put(this.appService.merchantService + 'affiliates/',affiliate)
                .subscribe((response: any) => {
                    resolve(response);
                }, reject);
        });
    }

}
