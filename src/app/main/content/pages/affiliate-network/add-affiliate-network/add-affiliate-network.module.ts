import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CdkTableModule } from '@angular/cdk/table';

import { MatFormFieldModule, MatInputModule, MatSelectModule, MatStepperModule, MatButtonModule, MatIconModule, MatRippleModule, MatSidenavModule, MatSlideToggleModule, MatTableModule } from '@angular/material';
import {MatChipsModule} from '@angular/material/chips';
import {MatAutocompleteModule} from '@angular/material/autocomplete';

import { FuseSharedModule } from '@fuse/shared.module';

import { AddAffiliateNetworkComponent } from './add-affiliate-network.component';
import { AddAffiliateNetworkService } from './add-affiliate-network.service';
import { FormAddAffiliateNetworkComponent } from './form/form-add-affiliate-network.component';

const routes: Routes = [
    {
        path     : '**',
        component: AddAffiliateNetworkComponent,
        children : [],
        resolve  : {
            files: AddAffiliateNetworkService
        }
    }
];

@NgModule({
    declarations: [
        AddAffiliateNetworkComponent,
        FormAddAffiliateNetworkComponent
    ],
    imports     : [
        RouterModule.forChild(routes),

        CdkTableModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        MatStepperModule,
        MatButtonModule,
        MatIconModule,
        MatRippleModule,
        MatSlideToggleModule,
        MatSidenavModule,
        MatTableModule,
        MatChipsModule,
        MatAutocompleteModule,

        FuseSharedModule
    ],
    providers   : [
        AddAffiliateNetworkService
    ]
})
export class AddAffiliateNetworkModule
{
}
