import { Component, OnInit, ViewEncapsulation } from '@angular/core';

import { fuseAnimations } from '@fuse/animations';

import { AddAffiliateNetworkService } from './add-affiliate-network.service';

import { Router } from '@angular/router';

@Component({
    selector     : 'fuse-file-manager',
    templateUrl  : './add-affiliate-network.component.html',
    styleUrls    : ['./add-affiliate-network.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class AddAffiliateNetworkComponent implements OnInit
{
    selected: any;
    pathArr: string[];

    constructor(private AddAffiliateNetworkService: AddAffiliateNetworkService, private router: Router)
    {
    }

    ngOnInit()
    {
        
    }

    openAffiliateList(){
        
        this.router.navigateByUrl("pages/affiliate-network/list/");
    }
}
