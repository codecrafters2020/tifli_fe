import { Component, OnInit,ViewChild } from '@angular/core';
import {AbstractControl, FormBuilder, FormGroup, Validators,EmailValidator } from '@angular/forms';
import { AddAffiliateNetworkService } from '../add-affiliate-network.service';
import { Router } from '@angular/router';
import {FormControl} from '@angular/forms';
import {Observable} from 'rxjs/Observable';
import {startWith} from 'rxjs/operators/startWith';
import {map} from 'rxjs/operators/map';
import {MatSnackBar} from '@angular/material';

import {
    MatAutocomplete,
    MatAutocompleteSelectedEvent,
    MatAutocompleteTrigger,
    VERSION,
  } from '@angular/material';
import { Subscription } from 'rxjs/Subscription';

@Component({
    selector   : 'fuse-file-list',
    templateUrl: './form-add-affiliate-network.component.html',
    styleUrls  : ['./form-add-affiliate-network.component.scss'],
})
export class FormAddAffiliateNetworkComponent implements OnInit
{

     affiliateNetwork={
        status : 'None',
        name :'',
        createdBy : 0,
        apiUrl : '',
        publisherId : ''
    }; 

    statusList = ['None','Active', 'Inactive', 'Pending'];

    form: FormGroup;
    formErrors: any;
    
    filteredOptions: Observable<any[]>;
    
    constructor(private formBuilder: FormBuilder,private AddAffiliateNetworkService: AddAffiliateNetworkService,private router: Router, private snackBar: MatSnackBar)
    {
        // Reactive form errors
        this.formErrors = {
            name : {},
            status  : {},
            apiUrl : {},
            publisherId : {}
        };
    }

    ngOnInit()
    {
        // Reactive Form
        this.form = this.formBuilder.group({
            name : ['', Validators.required],
            status  : ['', Validators.required],
            apiUrl :  ['', Validators.required],
            publisherId :  ['', Validators.required]
        });

        this.form.valueChanges.subscribe(() => {
            this.onFormValuesChanged();
        });

    }
    
    onFormValuesChanged()
    {
        for ( const field in this.formErrors )
        {
            if ( !this.formErrors.hasOwnProperty(field) )
            {
                continue;
            }

            // Clear previous errors
            this.formErrors[field] = {};

            // Get the control
            const control = this.form.get(field);

            if ( control && control.dirty && !control.valid )
            {
                this.formErrors[field] = control.errors;
            }
        }
    }

    onSubmit() {
        debugger;
        if (this.form.valid) {
            this.affiliateNetwork.createdBy = 1;
            this.AddAffiliateNetworkService.addAffiliate(this.affiliateNetwork).then(response => {
                debugger;
                this.snackBar.open(response.meta.message,"Done", {
                    duration: 2000,
                });
                if(response.meta.status == "success")
                    this.redirect('pages/affiliate-network/list');
            }); 
        }
        
    }

    redirect(pagename: string) {
        this.router.navigate([pagename]);
    }

}
