import { Component, OnInit, ViewEncapsulation } from '@angular/core';

import { fuseAnimations } from '@fuse/animations';

import { ListAffiliateNetworkService } from './list-affiliate-network.service';
import { Router } from '@angular/router';

@Component({
    selector     : 'fuse-file-manager',
    templateUrl  : './list-affiliate-network.component.html',
    styleUrls    : ['./list-affiliate-network.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class ListAffiliateNetworkComponent implements OnInit
{
    selected: any;
    pathArr: string[];

    constructor(private ListAffiliateNetworkService: ListAffiliateNetworkService, private router: Router)
    {
    }

    ngOnInit()
    {
        this.ListAffiliateNetworkService.onFileSelected.subscribe(selected => {
            this.selected = selected;
           // this.pathArr = selected.location.split('>');
        });
    }

    openAffiliateAdd(){
        this.router.navigateByUrl("pages/affiliate-network/add/");
    }
}
