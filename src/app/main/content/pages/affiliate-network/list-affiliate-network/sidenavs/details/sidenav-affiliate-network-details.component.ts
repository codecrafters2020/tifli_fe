import { Component, OnInit } from '@angular/core';

import { fuseAnimations } from '@fuse/animations/index';

import { ListAffiliateNetworkService } from '../../list-affiliate-network.service';

@Component({
    selector   : 'fuse-file-manager-details-sidenav',
    templateUrl: './sidenav-affiliate-network-details.component.html',
    styleUrls  : ['./sidenav-affiliate-network-details.component.scss'],
    animations : fuseAnimations
})
export class SidenavAffiliateNetworkDetailsComponent implements OnInit
{

    selected: any;

    constructor(private ListAffiliateNetworkService: ListAffiliateNetworkService)
    {

    }

    ngOnInit()
    {
        this.ListAffiliateNetworkService.onFileSelected.subscribe(selected => {
            this.selected = selected;
        });
    }

}
