import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CdkTableModule } from '@angular/cdk/table';

import { MatFormFieldModule, MatInputModule, MatSelectModule, MatStepperModule, MatButtonModule, MatIconModule, MatRippleModule, MatSidenavModule, MatSlideToggleModule, MatTableModule, MatMenuModule, MatPaginatorModule, MatSortModule } from '@angular/material';

import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatRadioModule} from '@angular/material/radio';
import { FuseSharedModule } from '@fuse/shared.module';

import { ListAffiliateNetworkComponent } from './list-affiliate-network.component';
import { ListAffiliateNetworkService } from './list-affiliate-network.service';
import { GridAffiliateNetworkComponent } from './grid/grid-affiliate-network.component';
import { SidenavAffiliateNetworkMainComponent } from './sidenavs/main/sidenav-affiliate-network-main.component';
import { SidenavAffiliateNetworkDetailsComponent } from './sidenavs/details/sidenav-affiliate-network-details.component';

const routes: Routes = [
    {
        path     : '**',
        component: ListAffiliateNetworkComponent,
        children : [],
        resolve  : {
            files: ListAffiliateNetworkService
        }
    }
];

@NgModule({
    declarations: [
        ListAffiliateNetworkComponent,
        GridAffiliateNetworkComponent,
        SidenavAffiliateNetworkMainComponent,
        SidenavAffiliateNetworkDetailsComponent
    ],
    imports     : [
        RouterModule.forChild(routes),

        CdkTableModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        MatStepperModule,
        MatButtonModule,
        MatIconModule,
        MatRippleModule,
        MatSlideToggleModule,
        MatSidenavModule,
        MatTableModule,
        MatMenuModule,
        MatPaginatorModule, 
        MatSortModule,

        FuseSharedModule,
        MatDatepickerModule,
        MatCheckboxModule,
        MatRadioModule
    ],
    providers   : [
        ListAffiliateNetworkService
    ]
})
export class ListAffiliateNetworkModule
{
}
