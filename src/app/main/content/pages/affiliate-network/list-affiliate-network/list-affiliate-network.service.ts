import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';

import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { AppService } from './../../../../../app.service';

@Injectable()
export class ListAffiliateNetworkService implements Resolve<any>
{
    affiliateNetworks: any[];
    createdByIds: any[] = [];
    createdByUsers: any[] = [];
    onFilesChanged: BehaviorSubject<any> = new BehaviorSubject({});
    onFileSelected: BehaviorSubject<any> = new BehaviorSubject({});

    constructor(private http: HttpClient, private appService: AppService)
    {
    }

    /**
     * The File Manager App Main Resolver
     * 
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any
    {

        // return new Promise((resolve, reject) => {

        //     Promise.all([
        //         this.getFiles()
        //     ]).then(
        //         ([files]) => {
        //             resolve();
        //         },
        //         reject
        //     );
        // });
    }

    getFiles(): Promise<any>
    {
        return new Promise((resolve, reject) => {
            this.http.get(this.appService.merchantService + 'affiliates/')
                .subscribe((response: any) => {
                    this.affiliateNetworks = response.data;
                    debugger;
                    var i = 0;
                    for(var affiliate of this.affiliateNetworks){
                        this.createdByIds.push(affiliate.createdBy);
                        i++;
                    }
                    this.onFilesChanged.next(response.data);
                    this.onFileSelected.next(response[0]);
                    resolve(response);
                }, reject);
        });
    }

    getUsersByIdList(list): Promise<any>
    {
        return new Promise((resolve, reject) => {
            this.http.post(this.appService.userService + 'users/list/',list)
                .subscribe((response: any) => {
                    debugger;
                    this.createdByUsers = response.data;
                    resolve(response);
                }, reject);
        });
    }

}
