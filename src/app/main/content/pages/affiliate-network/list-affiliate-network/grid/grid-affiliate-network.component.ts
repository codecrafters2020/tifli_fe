import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort } from '@angular/material';
import { DataSource } from '@angular/cdk/collections';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';

import { ListAffiliateNetworkService } from '../list-affiliate-network.service';
import { fuseAnimations } from '@fuse/animations/index';
import { FuseUtils } from '@fuse/utils';

@Component({
    selector   : 'fuse-file-list',
    templateUrl: './grid-affiliate-network.component.html',
    styleUrls  : ['./grid-affiliate-network.component.scss'],
    animations : fuseAnimations
})
export class GridAffiliateNetworkComponent implements OnInit
{
    files: any;
    dataSource: GridUserDataSource | null;
    displayedColumns = ['icon', 'name', 'createdBy','status', 'action-button','detail-button'];
    selected: any;

    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild('filter') filter: ElementRef;
    @ViewChild(MatSort) sort: MatSort;

    constructor(private ListAffiliateNetworkService: ListAffiliateNetworkService, private router: Router)
    {
        
    }

    ngOnInit()
    {
        debugger;
        this.dataSource = new GridUserDataSource(this.ListAffiliateNetworkService, this.paginator, this.sort);
    }

    onSelect(selected)
    {
        this.ListAffiliateNetworkService.onFileSelected.next(selected);
    }

    openAffiliateEdit(event,affiliateid){
        this.router.navigateByUrl("pages/affiliate-network/edit/" + affiliateid);
    }
}

export class GridUserDataSource extends DataSource<any>
{
    _filterChange = new BehaviorSubject('');
    _filteredDataChange = new BehaviorSubject('');
    createdByUsers: any[] = [];
    isCreatedByUsersReady = false;

    get filteredData(): any
    {
        return this._filteredDataChange.value;
    }

    set filteredData(value: any)
    {
        this._filteredDataChange.next(value);
    }

    get filter(): string
    {
        return this._filterChange.value;
    }

    set filter(filter: string)
    {
        this._filterChange.next(filter);
    }
    constructor(private ListAffiliateNetworkService: ListAffiliateNetworkService, private _paginator: MatPaginator, private _sort: MatSort)
    {
        super();
        this.filteredData = this.ListAffiliateNetworkService.affiliateNetworks;
        this.ListAffiliateNetworkService.getUsersByIdList(ListAffiliateNetworkService.createdByIds).then(response => {
            this.createdByUsers = ListAffiliateNetworkService.createdByUsers;
            debugger;
            for(var item of this.filteredData){
                for(var user of this.createdByUsers){
                    if(item.createdBy == user.id){
                        item.createdBy = user;
                        break;
                    }
                }
            }
            this.isCreatedByUsersReady = true;
        });
    }

    /** Connect function called by the table to retrieve one stream containing the data to render. */
    // connect(): Observable<any[]>
    // {
    //     return this.ListAffiliateNetworkService.onFilesChanged;
    // }
    connect(): Observable<any[]>
    {
        const displayDataChanges = [
            this.ListAffiliateNetworkService.onFilesChanged,
            this._paginator.page,
            this._filterChange,
            // this._sort.sortChange
        ];

        return Observable.merge(...displayDataChanges).map(() => {
            let data = this.ListAffiliateNetworkService.affiliateNetworks.slice();

            data = this.filterData(data);

            this.filteredData = [...data];

            //data = this.sortData(data);

            // Grab the page's slice of data.
            const startIndex = this._paginator.pageIndex * this._paginator.pageSize;
            return data.splice(startIndex, this._paginator.pageSize);
        });
    }

    filterData(data)
    {
        if ( !this.filter )
        {
            return data;
        }
        return FuseUtils.filterArrayByString(data, this.filter);
    }

    // sortData(data): any[]
    // {
    //     if ( !this._sort.active || this._sort.direction === '' )
    //     {
    //         return data;
    //     }

    //     return data.sort((a, b) => {
    //         let propertyA: number | string = '';
    //         let propertyB: number | string = '';

    //         switch ( this._sort.active )
    //         {
    //             case 'id':
    //                 [propertyA, propertyB] = [a.id, b.id];
    //                 break;
    //             case 'name':
    //                 [propertyA, propertyB] = [a.name, b.name];
    //                 break;
    //             case 'categories':
    //                 [propertyA, propertyB] = [a.categories[0], b.categories[0]];
    //                 break;
    //             case 'price':
    //                 [propertyA, propertyB] = [a.priceTaxIncl, b.priceTaxIncl];
    //                 break;
    //             case 'quantity':
    //                 [propertyA, propertyB] = [a.quantity, b.quantity];
    //                 break;
    //             case 'active':
    //                 [propertyA, propertyB] = [a.active, b.active];
    //                 break;
    //         }

    //         const valueA = isNaN(+propertyA) ? propertyA : +propertyA;
    //         const valueB = isNaN(+propertyB) ? propertyB : +propertyB;

    //         return (valueA < valueB ? -1 : 1) * (this._sort.direction === 'asc' ? 1 : -1);
    //     });
    // }

    disconnect()
    {
    }
}
