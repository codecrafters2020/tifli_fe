import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, FormArray, Validators, EmailValidator } from '@angular/forms';
import { ListPaidPlacementsService } from '../list-paid-placements.service';
import { Router } from '@angular/router';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { startWith } from 'rxjs/operators/startWith';
import { map } from 'rxjs/operators/map';
import { MatSnackBar, MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

import * as _ from 'lodash';
import moment from 'moment/src/moment';

import {
    MatAutocomplete,
    MatAutocompleteSelectedEvent,
    MatAutocompleteTrigger,
    VERSION,
  } from '@angular/material';
import { Subscription } from 'rxjs/Subscription';

@Component({
    selector   : 'add-paid-placements',
    templateUrl: './form-add-paid-placement.component.html',
    styleUrls  : ['./form-add-paid-placement.component.scss'],
})
export class FormAddPaidPlacementsComponent implements OnInit
{
    merchantCategoryStatus: boolean = false;
    form: FormGroup;
    formErrors: any;
    test: any;

    visible: boolean = true;
    selectable: boolean = true;
    removable: boolean = true;
    addOnBlur: boolean = true;

    merchantNamesSearchResult: any[];
    seasonalEventNamesSearchResult: any[];
    couponNamesSearchResult: any[];

    // merchantMetaSearchResult: any[];
    // seasonalEventMetaSearchResult: any[];
    // couponMetaSearchResult: any[];
    
    constructor(public dialogRef: MatDialogRef<FormAddPaidPlacementsComponent>,
                @Inject(MAT_DIALOG_DATA) public data: any, 
                private formBuilder: FormBuilder, 
                private ListPaidPlacementsService: ListPaidPlacementsService, 
                private router: Router, 
                private snackBar: MatSnackBar)
    {
        // Reactive form errors
        this.formErrors = {
            placementID : {},
            couponID : {},
            merchantName : {},
            couponTitle : {},
            seasonalEvent : {},
            pricePaid : {},
            startDate  : {},
            endDate: {}
        };
    }

    ngOnInit()
    {
        // Reactive Form
        this.form = this.formBuilder.group({
            couponID : ['', Validators.required],
            merchantName : ['', Validators.required],
            couponTitle : ['', Validators.required],
            seasonalEvent : ['', Validators.required],
            pricePaid : ['', Validators.required],
            startDate  : ['', Validators.required],
            endDate  : ['', Validators.required],
        });

        this.form.valueChanges.subscribe(() => {
            this.onFormValuesChanged();
        });

        this.form.controls.merchantName.valueChanges
        .debounceTime(400)
        .subscribe(searchTerm => {
            this.ListPaidPlacementsService.searchMerchant(searchTerm)
            // .map(filteredList => {
                // this.merchantMetaSearchResult = filteredList;
                // return _.map(filteredList, 'name');
            // })
            .subscribe(merchantNames => {
                this.merchantNamesSearchResult = merchantNames;
            });
        });

        this.form.controls.seasonalEvent.valueChanges
        .debounceTime(400)
        .subscribe(searchTerm => {
            this.ListPaidPlacementsService.searchSeasonalEvent(searchTerm)
            // .map(filteredList => {
                // this.seasonalEventMetaSearchResult = filteredList;
                // return _.map(filteredList, 'name');  
            // })
            .subscribe(seasonalEventNames => {
                this.seasonalEventNamesSearchResult = seasonalEventNames;
            });
        });

        this.form.controls.couponTitle.valueChanges
        .debounceTime(400)
        .subscribe(searchTerm => {
            let merchantID = typeof this.form.controls.merchantName.value.id === "number" ? this.form.controls.merchantName.value.id : "" ;
            this.ListPaidPlacementsService.searchCouponByCouponTitleMerchantID(searchTerm, merchantID)
            .subscribe(coupons => {
                this.couponNamesSearchResult = coupons;
            });
        });

    }

    displaySelectedName(item: any) 
    {
        return item.name;
    }

    displaySelectedTitle(item: any) 
    {
        return item.title;
    }

    initSubCategory() 
    {
        return this.formBuilder.group({
            name : ['', Validators.required],
            description  : ['', Validators.required],
            status : [this.merchantCategoryStatus ? 'ACTIVE' : 'INACTIVE', Validators.required]
        });
    }

    closeModal(): void 
    {
        this.dialogRef.close();
    }


    SearchData(input: any){
        // debugger;
    }

    onFormValuesChanged()
    {
        for ( const field in this.formErrors )
        {
            if ( !this.formErrors.hasOwnProperty(field) )
            {
                continue;
            }

            // Clear previous errors
            this.formErrors[field] = {};

            // Get the control
            const control = this.form.get(field);

            if ( control && control.dirty && !control.valid )
            {
                this.formErrors[field] = control.errors;
            }
        }
    }

    onSubmit() {
        let formValues = this.form.value;
        let startDate = moment.utc(formValues.startDate).format();
        let endDate = moment.utc(formValues.endDate).format();
        
        let payload = {
            "paidPlacementPrice": formValues.pricePaid,
            "status" :"Active",
            "createdBy":1,
            "modifiedBy" :1,
            "startDate": startDate,
            "endDate": endDate,
            "coupon":{
                "id" : formValues.couponTitle.id
            },
            "seasonalEvent":{
                "id" : formValues.seasonalEvent.id
            }
        }
        this.ListPaidPlacementsService.addPaidPlacement(payload)
        .then(response => {
            this.closeModal();
            this.ListPaidPlacementsService.getAggregatePaidPlacements()
            .then(paidPlacements => {
                this.ListPaidPlacementsService.paidPlacements = paidPlacements;
                this.ListPaidPlacementsService.onFilesChanged.next(paidPlacements);
                this.ListPaidPlacementsService.onFileSelected.next(paidPlacements[0]);
            });
        });
        // if (this.form.valid) {
        // }
        
    }

    redirect(pagename: string) {
        this.router.navigate([pagename]);
    }

    log(val: any) { console.log(val); }
}
