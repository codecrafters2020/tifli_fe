import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';

import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { AppService } from './../../../../../app.service';

import * as moment from 'moment';

@Injectable()
export class ListPaidPlacementsService implements Resolve<any>
{
    paidPlacements: any[];
    onFilesChanged: BehaviorSubject<any> = new BehaviorSubject({});
    onFileSelected: BehaviorSubject<any> = new BehaviorSubject({});

    constructor(private http: HttpClient, private appService: AppService)
    {
    }

    /**
     * The File Manager App Main Resolver
     * 
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any
    {

        // return new Promise((resolve, reject) => {

        //     Promise.all([
        //         this.getAggregatePaidPlacements()
        //     ]).then(
        //         ([paidPlacements]) => {
        //             this.paidPlacements = paidPlacements;
        //             resolve();
        //         },
        //         reject
        //     );
        // });
    }


    searchMerchant(term: string): Observable<any> {
        return this.http.get(`${this.appService.merchantService}merchants/search/?name=${term}`)
            .map(response => response['data'])
            .map(data => data instanceof Array ? data : [])
    }

    searchSeasonalEvent(term: string): Observable<any> {
        return this.http.get(`${this.appService.couponService}coupons/seasonal/search/?name=${term}`)
            .map(response => response['data'])
            .map(data => data instanceof Array ? data : [])
    }

    searchCouponByCouponTitleMerchantID(term: string, merchantID: any): Observable<any> {
        return this.http.get(`${this.appService.couponService}coupons/search/merchant/?title=${term}&merchant=${merchantID}`)
            .map(response => response['data'])
            .map(data => data instanceof Array ? data : [])
    }

    // Bad approach - Change later
    getAggregatePaidPlacements(): Promise<any>
    {
        return new Promise((resolve, reject) => {
            this.getPaidPlacements()
                .then(paidPlacements => {
                    paidPlacements.forEach((item, index) => {
                        this.getCouponByID(item.coupon.id)
                            .then(response => {
                                item['couponTitle'] = response.data.title;
                                return this.getMerchantByID(response.data.merchant.id);
                            }).then(response => {
                                item['merchantName'] = response.data.name;
                            });
                        this.getSeasonalEventByID(item.seasonalEvent.id)
                            .then(response => {
                                item['seasonalEventName'] = response.data.name;
                            })
                        item['startDate'] = moment(item['startDate']).format("MM/DD/YYYY");
                        item['endDate'] = moment(item['endDate']).format("MM/DD/YYYY");
                    });
                    resolve(paidPlacements);
                });
        });
    }

    getPaidPlacements(): Promise<any>
    {
        return new Promise((resolve, reject) => {
            this.http.get(`${this.appService.placementService}paid/`)
            // this.http.get(`api/paid-placements-list`)
                .subscribe((response: any) => {
                    /*this.paidPlacements = response.data;
                    this.onFilesChanged.next(response.data);
                    this.onFileSelected.next(response[0]);*/
                    resolve(response.data);
                }, reject);
        });
    }

    getCouponByID(id: number): Promise<any>
    {
        return new Promise((resolve, reject) => {
            this.http.get(`${this.appService.couponService}coupons/${id}`)
                .subscribe((response: any) => {
                    resolve(response);
                }, reject);
        });
    }

    getSeasonalEventByID(id: number): Promise<any>
    {
        return new Promise((resolve, reject) => {
            this.http.get(`${this.appService.couponService}coupons/seasonal/${id}`)
                .subscribe((response: any) => {
                    resolve(response);
                }, reject);
        });
    }

    getMerchantByID(id: number): Promise<any>
    {
        return new Promise((resolve, reject) => {
            this.http.get(`${this.appService.merchantService}merchants/${id}`)
                .subscribe((response: any) => {
                    resolve(response);
                }, reject);
        });
    }

    addPaidPlacement(payload): Promise<any>
    {
        return new Promise((resolve, reject) => {
            this.http.post(this.appService.placementService + 'placements/paid/', payload)
            // this.http.post('http://192.168.1.164:9006/paid/', payload)
                .subscribe((response: any) => {
                    resolve(response);
                }, reject);
        });
    }

}
