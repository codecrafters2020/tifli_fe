import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CdkTableModule } from '@angular/cdk/table';

import { MatFormFieldModule, MatInputModule, MatSelectModule, MatStepperModule, MatButtonModule, MatIconModule, MatRippleModule, MatSidenavModule, MatSlideToggleModule, MatTableModule, MatMenuModule, MatPaginatorModule, MatSortModule, MatChipsModule, MatDialogModule, MatAutocompleteModule, MatToolbarModule } from '@angular/material';

import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatRadioModule} from '@angular/material/radio';
import { FuseSharedModule } from '@fuse/shared.module';

import { FormAddPaidPlacementsComponent } from './modal/form-add-paid-placement.component';

import { ListUserComponent } from './list-paid-placements.component';
import { ListPaidPlacementsService } from './list-paid-placements.service';
import { GridPaidPlacementsComponent } from './grid/grid-paid-placements.component';
import { SidenavPaidPlacementsMainComponent } from './sidenavs/main/sidenav-paid-placements-main.component';
import { SidenavPaidPlacementsDetailsComponent } from './sidenavs/details/sidenav-paid-placements-details.component';

const routes: Routes = [
    {
        path     : '**',
        component: ListUserComponent,
        children : [],
        resolve  : {
            files: ListPaidPlacementsService
        }
    }
];

@NgModule({
    declarations: [
        ListUserComponent,
        GridPaidPlacementsComponent,
        SidenavPaidPlacementsMainComponent,
        SidenavPaidPlacementsDetailsComponent,
        FormAddPaidPlacementsComponent
    ],
    imports     : [
        RouterModule.forChild(routes),

        CdkTableModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        MatStepperModule,
        MatButtonModule,
        MatIconModule,
        MatRippleModule,
        MatSlideToggleModule,
        MatSidenavModule,
        MatTableModule,
        MatMenuModule,
        MatPaginatorModule, 
        MatSortModule,
        MatChipsModule,
        MatDialogModule,
        MatAutocompleteModule,
        MatToolbarModule,

        FuseSharedModule,
        MatDatepickerModule,
        MatCheckboxModule,
        MatRadioModule
    ],
    providers   : [
        ListPaidPlacementsService
    ],
    // Modal
    entryComponents: [FormAddPaidPlacementsComponent]
})
export class ListPaidPlacementsModule
{
}
