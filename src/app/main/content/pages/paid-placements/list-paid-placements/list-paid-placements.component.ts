import { Component, OnInit, ViewEncapsulation } from '@angular/core';

import { fuseAnimations } from '@fuse/animations';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';

import { ListPaidPlacementsService } from './list-paid-placements.service';
import { FormAddPaidPlacementsComponent } from './modal/form-add-paid-placement.component';


import { Router } from '@angular/router';


@Component({
    selector     : 'fuse-file-manager',
    templateUrl  : './list-paid-placements.component.html',
    styleUrls    : ['./list-paid-placements.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class ListUserComponent implements OnInit
{
    selected: any;
    pathArr: string[];

    constructor(public dialog: MatDialog, private ListPaidPlacementsService: ListPaidPlacementsService, private router: Router)
    {
    }

    ngOnInit()
    {
        this.ListPaidPlacementsService.onFileSelected.subscribe(selected => {
            this.selected = selected;
           // this.pathArr = selected.location.split('>');
        });
    }

    openPaidPlacementAdd(): void
    {
        let dialogRef = this.dialog.open(FormAddPaidPlacementsComponent, {
          width: '800px'
        });

        dialogRef.afterClosed()
            .subscribe(result => {
            });
    }
}
