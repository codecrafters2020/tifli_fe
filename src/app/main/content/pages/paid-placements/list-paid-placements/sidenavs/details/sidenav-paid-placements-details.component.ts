import { Component, OnInit } from '@angular/core';

import { fuseAnimations } from '@fuse/animations/index';

import { ListPaidPlacementsService } from '../../list-paid-placements.service';

@Component({
    selector   : 'fuse-file-manager-details-sidenav',
    templateUrl: './sidenav-paid-placements-details.component.html',
    styleUrls  : ['./sidenav-paid-placements-details.component.scss'],
    animations : fuseAnimations
})
export class SidenavPaidPlacementsDetailsComponent implements OnInit
{

    selected: any;

    constructor(private ListPaidPlacementsService: ListPaidPlacementsService)
    {

    }

    ngOnInit()
    {
        this.ListPaidPlacementsService.onFileSelected.subscribe(selected => {
            this.selected = selected;
        });
    }

}
