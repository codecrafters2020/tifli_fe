import { Component, OnInit, ViewChild } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators, EmailValidator } from '@angular/forms';
import { AddAffiliateNetworkService } from '../add-affiliate-network.service';
import { Router } from '@angular/router';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { startWith } from 'rxjs/operators/startWith';
import { map } from 'rxjs/operators/map';
import { MatSnackBar } from '@angular/material';
import { FileUploader } from 'ng2-file-upload';
import { AppService } from './../../../../../app.service';

import {
    MatAutocomplete,
    MatAutocompleteSelectedEvent,
    MatAutocompleteTrigger,
    VERSION,
} from '@angular/material';
import { Subscription } from 'rxjs/Subscription';

// const URL = 'http://websvc.westus.cloudapp.azure.com/commissionapi/payment/uploadTest';
//const URL = this.appService.commissionapi + 'payment/upload/9';

@Component({
    selector: 'fuse-file-list',
    templateUrl: './form-add-affiliate-network.component.html',
    styleUrls: ['./form-add-affiliate-network.component.scss'],
})
export class FormAddAffiliateNetworkComponent implements OnInit {
    public uploader: FileUploader = new FileUploader({ url: this.appService.commissionService + 'payment/upload/9' });
    public hasBaseDropZoneOver: boolean = false;
    public hasAnotherDropZoneOver: boolean = false;


    constructor(private formBuilder: FormBuilder, private AddAffiliateNetworkService: AddAffiliateNetworkService, private router: Router, private snackBar: MatSnackBar, private appService:AppService) {
        // Reactive form errors
    }

    ngOnInit() {


    }

    syncCommission() {
        debugger;
        this.AddAffiliateNetworkService.syncCommission().then(response => {
            debugger;
            this.snackBar.open(response.meta.message, "Done", {
                duration: 2000,
            });
            // if(response.meta.status == "success")
            // this.redirect('pages/affiliate-network/list');
        });

    }

    public fileOverBase(e: any): void {
        this.hasBaseDropZoneOver = e;
    }

    public fileOverAnother(e: any): void {
        this.hasAnotherDropZoneOver = e;
    }

    redirect(pagename: string) {
        this.router.navigate([pagename]);
    }

}
