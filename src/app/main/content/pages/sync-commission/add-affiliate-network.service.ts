import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';

import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { AppService } from './../../../../app.service';

@Injectable()
export class AddAffiliateNetworkService implements Resolve<any>
{
    roles: any[];

    constructor(private http: HttpClient, private appService: AppService)
    {
    }

    /**
     * The File Manager App Main Resolver
     * 
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any
    {

        return new Promise((resolve, reject) => {
            debugger;
            Promise.all([
             this.getRoles()
            ]).then(
                ([users]) => {
                    resolve();
                },
                reject);
        });
    }

    getRoles(): Promise<any>
    {

        return new Promise((resolve, reject) => {
            debugger;
            resolve();
        });
    }

    syncCommission(): Promise<any>
    {
        return new Promise((resolve, reject) => {
            debugger;
            this.http.get(this.appService.merchantService + 'affiliates/')
                .subscribe((response: any) => {
                    resolve(response);
                }, reject);
        });
    }

}
