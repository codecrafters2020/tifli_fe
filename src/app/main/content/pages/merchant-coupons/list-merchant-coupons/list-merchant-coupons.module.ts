import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CdkTableModule } from '@angular/cdk/table';

import { MatAutocompleteModule, MatFormFieldModule, MatInputModule, MatSelectModule, MatStepperModule, MatButtonModule, MatIconModule, MatRippleModule, MatSidenavModule, MatSlideToggleModule, MatTableModule, MatMenuModule, MatPaginatorModule, MatSortModule } from '@angular/material';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';

import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatRadioModule} from '@angular/material/radio';
import { FuseSharedModule } from '@fuse/shared.module';

import { ListMerchantCouponsComponent } from './list-merchant-coupons.component';
import { ListMerchantCouponsService } from './list-merchant-coupons.service';

import { GridMerchantOngoingCouponsComponent } from './grid-ongoing-coupons/grid-merchant-ongoing-coupons.component';
import { GridMerchantExpiredCouponsComponent } from './grid-expired-coupons/grid-merchant-expired-coupons.component';
import { GridMerchantOnlineCouponsComponent } from './grid-online-coupons/grid-merchant-online-coupons.component';
import { GridMerchantTopCouponsComponent } from './grid-top-coupons/grid-merchant-top-coupons.component';
import { GridMerchantCashbackCouponsComponent } from './grid-cashback-coupons/grid-merchant-cashback-coupons.component';
import { GridMerchantExclusiveCouponsComponent } from './grid-exclusive-coupons/grid-merchant-exclusive-coupons.component';
import { GridMerchantFreeshipCouponsComponent } from './grid-freeship-coupons/grid-merchant-freeship-coupons.component';
import { GridMerchantFeaturedCouponsComponent } from './grid-featured-coupons/grid-merchant-featured-coupons.component';
import { GridMerchantStaffPickedCouponsComponent } from './grid-staffpicked-coupons/grid-merchant-staffpicked-coupons.component';
import { GridMerchantSeasonalCouponsComponent } from './grid-seasonal-coupons/grid-merchant-seasonal-coupons.component';

import { SidenavMerchantCouponMainComponent } from './sidenavs/main/sidenav-merchant-coupon-main.component';
import { SidenavMerchantCouponDetailsComponent } from './sidenavs/details/sidenav-merchant-coupon-details.component';
import { FormMerchantCouponSearchComponent } from './form/form-merchant-coupon-search.component';
import {MatDividerModule} from '@angular/material/divider';
import {MatCardModule} from '@angular/material/card';

const routes: Routes = [
    {
        path     : '**',
        component: ListMerchantCouponsComponent,
        children : [],
        resolve  : {
            files: ListMerchantCouponsService
        }
    }
];

@NgModule({
    declarations: [
        ListMerchantCouponsComponent,
        GridMerchantOngoingCouponsComponent,
        GridMerchantExpiredCouponsComponent,
        GridMerchantOnlineCouponsComponent,
        GridMerchantTopCouponsComponent,
        GridMerchantCashbackCouponsComponent,
        SidenavMerchantCouponMainComponent,
        GridMerchantExclusiveCouponsComponent,
        GridMerchantFreeshipCouponsComponent,
        GridMerchantFeaturedCouponsComponent,
        GridMerchantStaffPickedCouponsComponent,
        GridMerchantSeasonalCouponsComponent,
        SidenavMerchantCouponDetailsComponent,
        FormMerchantCouponSearchComponent
    ],
    imports     : [
        RouterModule.forChild(routes),

        CdkTableModule,
        MatAutocompleteModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        MatStepperModule,
        MatButtonModule,
        MatIconModule,
        MatRippleModule,
        MatSlideToggleModule,
        MatSidenavModule,
        MatTableModule,
        MatMenuModule,
        MatPaginatorModule, 
        MatSortModule,
        MatProgressSpinnerModule,

        FuseSharedModule,
        MatDatepickerModule,
        MatCheckboxModule,
        MatRadioModule,
        MatDividerModule,
        MatCardModule
    ],
    providers   : [
        ListMerchantCouponsService
    ]
})
export class ListMerchantCouponsModule
{
}
