import { Component, OnInit } from '@angular/core';

import { fuseAnimations } from '@fuse/animations/index';

import { ListMerchantCouponsService } from '../../list-merchant-coupons.service';

@Component({
    selector   : 'fuse-file-manager-details-sidenav',
    templateUrl: './sidenav-merchant-coupon-details.component.html',
    styleUrls  : ['./sidenav-merchant-coupon-details.component.scss'],
    animations : fuseAnimations
})
export class SidenavMerchantCouponDetailsComponent implements OnInit
{

    selected: any;

    constructor(private ListMerchantCouponsService: ListMerchantCouponsService)
    {

    }

    ngOnInit()
    {
        this.ListMerchantCouponsService.onFileSelected.subscribe(selected => {
            this.selected = selected;
        });
    }

}
