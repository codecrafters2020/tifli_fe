import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';

import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of'
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { AppService } from './../../../../../app.service';

import * as moment from 'moment';
import * as _ from 'lodash';

@Injectable()
export class ListMerchantCouponsService implements Resolve<any>
{
    coupons: any[];
    couponTypes: any[];
    onFilesChanged: BehaviorSubject<any> = new BehaviorSubject({});
    onFileSelected: BehaviorSubject<any> = new BehaviorSubject({});
    id: any;
    merchant: any;
    exclusiveCoupons: any[]=[];
    cashbackCoupons: any[]=[];
    freeshipCoupons: any[]=[];
    ongoingCoupons: any[]=[];
    staffPickedCoupons: any[]=[];
    expiredCoupons: any[]=[];
    featuredCoupons: any[]=[];
    seasonalCoupons: any[] = [];

    constructor(private http: HttpClient, private appService: AppService) {
    }

    /**
     * The File Manager App Main Resolver
     * 
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {

        // return new Promise((resolve, reject) => {

        //     Promise.all([

        //     ]).then(
        //         ([]) => {
        //             resolve();
        //         },
        //         reject
        //     );
        // });
    }

    getCoupons(): Promise<any> {
        return new Promise((resolve, reject) => {
            this.http.get(`${this.appService.couponService}coupons/coupons?page=0&size=50&sort=couponCode,asc`)
                .subscribe((response: any) => {
                    this.coupons = response.data.content;
                    this.onFilesChanged.next(response.data);
                    this.onFileSelected.next(response[0]);
                    resolve(response);
                }, reject);
        });
    }


    getMerchants(): Promise<any> {
        return new Promise((resolve, reject) => {
            this.http.get(`${this.appService.merchantService}`)
                .subscribe((response: any) => {
                    this.coupons = response.data;
                    this.onFilesChanged.next(response.data);
                    this.onFileSelected.next(response[0]);
                    resolve(response);
                }, reject);
        });
    }

    getMerchantById(): Promise<any> {
        return new Promise((resolve, reject) => {
            this.http.get(this.appService.merchantService + this.id)
                .subscribe((response: any) => {
                    console.log(response);
                    if (response.meta.code == "200") {
                        this.merchant = response.data;
                    }
                    resolve(response);
                }, reject);
        });
    }

    getExclusiveCoupons(): Promise<any> {
        return new Promise((resolve, reject) => {

            this.http.get(`${this.appService.couponService}section/exclusive/${this.id}`)
                .subscribe((response: any) => {
                    console.log(response);
                    if (response.meta.code == "200") {
                        this.exclusiveCoupons = response.data;
                        this.onFilesChanged.next(response.data);
                        this.onFileSelected.next(response[0]);
                    }
                    resolve(response);
                }, reject);
        });
    }

    getCashbackCoupons(): Promise<any> {
        return new Promise((resolve, reject) => {

            this.http.get(`${this.appService.couponService}section/cashback/${this.id}`)
                .subscribe((response: any) => {
                    console.log(response);
                    if (response.meta.code == "200") {
                        this.cashbackCoupons = response.data;
                        this.onFilesChanged.next(response.data);
                        this.onFileSelected.next(response[0]);
                    }
                    resolve(response);
                }, reject);
        });
    }


    getFreeshipCoupons(): Promise<any> {
        return new Promise((resolve, reject) => {

            this.http.get(`${this.appService.couponService}section/freeshipping/${this.id}`)
                .subscribe((response: any) => {
                    console.log(response);
                    if (response.meta.code == "200") {
                        this.freeshipCoupons = response.data;
                        this.onFilesChanged.next(response.data);
                        this.onFileSelected.next(response[0]);
                    }
                    resolve(response);
                }, reject);
        });
    }

    getOngoingCoupons(): Promise<any> {
        return new Promise((resolve, reject) => {

            this.http.get(`${this.appService.couponService}section/ongoing/${this.id}`)
                .subscribe((response: any) => {
                    console.log(response);
                    if (response.meta.code == "200") {
                        this.ongoingCoupons = response.data;
                        this.onFilesChanged.next(response.data);
                        this.onFileSelected.next(response[0]);
                    }
                    resolve(response);
                }, reject);
        });
    }

    getStaffPickedCoupons(): Promise<any> {
        return new Promise((resolve, reject) => {

            this.http.get(`${this.appService.couponService}section/staffpicked/${this.id}`)
                .subscribe((response: any) => {
                    console.log(response);
                    if (response.meta.code == "200") {
                        this.staffPickedCoupons = response.data;
                        this.onFilesChanged.next(response.data);
                        this.onFileSelected.next(response[0]);
                    }
                    resolve(response);
                }, reject);
        });
    }

    getFeaturedCoupons(): Promise<any> {
        return new Promise((resolve, reject) => {

            this.http.get(`${this.appService.couponService}featured/merchant/featured/${this.id}`)
                .subscribe((response: any) => {
                    console.log(response);
                    if (response.meta.code == "200") {
                        this.featuredCoupons = response.data;
                        this.onFilesChanged.next(response.data);
                        this.onFileSelected.next(response[0]);
                    }
                    resolve(response);
                }, reject);
        });
    }

    getExpiredCoupons(): Promise<any> {
        return new Promise((resolve, reject) => {
            var currentdate = new Date();
            var year = currentdate.getFullYear();
            var month = currentdate.getMonth() + 1;
            var date = currentdate.getDate();

            var endDate = year + "-" + month + "-" + date;

            this.http.get(`${this.appService.couponService}section/expired/${this.id}?endDate=${endDate}`)
                .subscribe((response: any) => {
                    console.log(response);
                    if (response.meta.code == "200") {
                        this.expiredCoupons = response.data;
                        this.onFilesChanged.next(response.data);
                        this.onFileSelected.next(response[0]);
                    }
                    resolve(response);
                }, reject);
        });
    }

    getSeasonalCoupons(): Promise<any> {
        return new Promise((resolve, reject) => {
            this.http.get(`${this.appService.couponService}seasonal/merchant/${this.id}`)
                .subscribe((response: any) => {
                    console.log(response);
                    if (response.meta.code == "200") {
                        this.onFilesChanged.next(response.data);
                        this.onFileSelected.next(response[0]);

                        for (let i = 0; i < response.data.length; i++) {

                            for (let j = 0; j < response.data[i].couponSet.length; j++) {
                                let seasonalCoupon = {
                                    id: '',
                                    title: '',
                                    description: '',
                                    couponType: [],
                                    startDate: null,
                                    endDate: null,
                                    commission: 0,
                                    clickCount: 0,
                                    orderCount: 0,
                                    seasonalEvent: ''
                                };

                                seasonalCoupon.id = response.data[i].couponSet[j].id;
                                seasonalCoupon.title = response.data[i].couponSet[j].title;
                                seasonalCoupon.description = response.data[i].couponSet[j].description;
                                seasonalCoupon.couponType = response.data[i].couponSet[j].couponType;
                                seasonalCoupon.clickCount = response.data[i].couponSet[j].clickCount;
                                seasonalCoupon.orderCount = response.data[i].couponSet[j].orderCount;
                                seasonalCoupon.startDate = response.data[i].couponSet[j].startDate;
                                seasonalCoupon.endDate = response.data[i].couponSet[j].endDate;
                                seasonalCoupon.commission = response.data[i].couponSet[j].commission;
                                seasonalCoupon.seasonalEvent = response.data[i].name;
                                this.seasonalCoupons.push(seasonalCoupon);

                            }

                        }
                        //this.seasonalCoupons = response.data;
                    }
                    resolve(response);
                }, reject);
        });
    }

    // http://websvc.westus.cloudapp.azure.com/couponapi/section/expired/32?endDate=2018-09-15

    getCouponTypes(): Promise<any> {
        return new Promise((resolve, reject) => {
            this.http.get(`${this.appService.couponService}type/`)
                .subscribe((response: any) => {
                    this.couponTypes = response.data;
                    resolve(response);
                }, reject);
        });
    }

    getFilteredCoupons(page: number | string,
        size: number | string,
        sort: string,
        searchTerms: string,
        merchantName: string,
        seasonalSearchTerms: string,
        couponType: string,
        couponStatus: string,
        ongoingOffer: string | number,
        exclusiveOffer: string | number,
        expiringSoon: string | number,
        freeShipping: string | number
    ): Promise<any> {
        debugger;
        let params = {
            page,
            size,
            sort,
            searchTerms,
            merchantName,
            seasonalSearchTerms,
            couponType,
            couponStatus,
            ongoingOffer,
            exclusiveOffer,
            expiringSoon,
            freeShipping
        };

        let queryString = '';
        _.forEach(params, (val, key) => {
            if (val || typeof val === 'number') {
                queryString = queryString + key + '=' + val + '&';
            }
        });

        return new Promise((resolve, reject) => {
            this.http.get(`${this.appService.couponService}coupons/coupons?${queryString}`)
                .subscribe((response: any) => {
                    this.coupons = response.data.content;
                    this.onFilesChanged.next(response.data);
                    this.onFileSelected.next(response[0]);
                    resolve(response);
                }, reject);
        });
    }


    searchCoupon(term: string): Observable<any> {
        return this.http.get(`${this.appService.couponService}coupons/search?titleOrId=${term}`)
            .map(response => response['data'])
            .map(data => data instanceof Array ? data : [])
    }

    searchMerchant(term: string): Observable<any> {
        return this.http.get(`${this.appService.merchantService}search/?name=${term}`)
            .map(response => response['data'])
            .map(data => data instanceof Array ? data : [])
    }

}
