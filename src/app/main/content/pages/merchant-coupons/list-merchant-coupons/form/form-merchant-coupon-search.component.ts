import { Component, OnInit, ViewChild } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators, EmailValidator } from '@angular/forms';
import { ListMerchantCouponsService } from '../list-merchant-coupons.service';
import { Router } from '@angular/router';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { startWith } from 'rxjs/operators/startWith';
import { map } from 'rxjs/operators/map';
import { MatSnackBar } from '@angular/material';
import { Output, EventEmitter } from '@angular/core';

import {
    MatAutocomplete,
    MatAutocompleteSelectedEvent,
    MatAutocompleteTrigger,
    VERSION,
} from '@angular/material';
import { Subscription } from 'rxjs/Subscription';

@Component({
    selector: 'form-merchant-coupon-search',
    templateUrl: './form-merchant-coupon-search.component.html',
    styleUrls: ['./form-merchant-coupon-search.component.scss'],
})
export class FormMerchantCouponSearchComponent implements OnInit {

    form: FormGroup;
    //formErrors: any;

    filteredOptions: Observable<any[]>;

    @Output() messageEvent = new EventEmitter<any>();
    merchantSearchResult: any[];
    couponSearchResult: any[];
    merchant: any;
    coupon: any;

    constructor(private formBuilder: FormBuilder, private ListMerchantCouponsService: ListMerchantCouponsService, private router: Router, private snackBar: MatSnackBar) {
        // Reactive form errors
        // this.formErrors = {
        //     merchant : {},
        //     coupon  : {}
        // };
    }

    ngOnInit() {
        this.form = this.formBuilder.group({
            merchant: [''],
           // coupon: ['']
        });

        this.form.valueChanges.subscribe(() => {
            this.onFormValuesChanged();
        });

        this.form.controls.merchant.valueChanges
            .subscribe(searchTerm => {
                this.ListMerchantCouponsService.searchMerchant(searchTerm)
                    .subscribe(merchants => {
                        debugger;
                        this.merchantSearchResult = merchants;
                    });
            });

        // this.form.controls.coupon.valueChanges
        //     .debounceTime(400)
        //     .subscribe(searchTerm => {
        //         this.ListMerchantCouponsService.searchCoupon(searchTerm)
        //             .subscribe(coupons => {
        //                 debugger;
        //                 this.couponSearchResult = coupons;
        //             });
        //     });


        // this.form.controls.password.valueChanges
        //     .subscribe(value => {
        //         debugger;
        //         let passwordConfirmValue = this.form.controls.passwordConfirm.value;
        //         const control = this.form.controls.passwordConfirm;
        //         if(value != passwordConfirmValue)
        //             this.formErrors.passwordConfirm.passwordsNotMatch = true;
        //         else{
        //             this.form.controls['passwordConfirm'].setValue(value);
        //         }
        //     });

    }



    SearchData(input: any) {
        debugger;
    }

    onFormValuesChanged() {
        // for ( const field in this.formErrors )
        // {
        //     if ( !this.formErrors.hasOwnProperty(field) )
        //     {
        //         continue;
        //     }

        //     // Clear previous errors
        //     this.formErrors[field] = {};

        //     // Get the control
        //     const control = this.form.get(field);

        //     if ( control && control.dirty && !control.valid )
        //     {
        //         this.formErrors[field] = control.errors;
        //     }
        // }
    }

    onMerchantSearchClick() {
        debugger;
        this.merchant=this.form.controls.merchant.value;
        this.merchant.checkData='merchant';
        this.messageEvent.emit(this.merchant);
        
    }

    // onCouponSearchClick() {
    //     debugger;
    //     this.merchant=this.form.controls.coupon.value;
    //     this.merchant.checkData='coupon';
    //     //this.merchant.isCoupon=true;
    //     this.messageEvent.emit(this.coupon);

    // }

    redirect(pagename: string) {
        this.router.navigate([pagename]);
    }

    displaySelectedName(item: any) 
    {
        debugger;
        return item.name;
    }

}