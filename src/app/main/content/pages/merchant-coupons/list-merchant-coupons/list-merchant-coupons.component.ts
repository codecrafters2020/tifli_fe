import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, FormArray, Validators, EmailValidator } from '@angular/forms';
import { fuseAnimations } from '@fuse/animations';

import { ListMerchantCouponsService } from './list-merchant-coupons.service';
import { Router } from '@angular/router';
import { ViewChild, AfterViewInit } from '@angular/core';
import { FormMerchantCouponSearchComponent } from './form/form-merchant-coupon-search.component';


@Component({
    selector: 'fuse-file-manager',
    templateUrl: './list-merchant-coupons.component.html',
    styleUrls: ['./list-merchant-coupons.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class ListMerchantCouponsComponent implements OnInit {

    selected: any;
    pathArr: string[];
    isSearch: boolean = false;
    form: FormGroup;
    merchantSearchResult: any[];
    merchant: any;
    coupon: any;
    merchantName: string;
    message: string;
    loadMerchantCoupons= false;

    constructor(private ListMerchantCouponsService: ListMerchantCouponsService, private router: Router, private formBuilder: FormBuilder) {

    }

    ngOnInit() {
        this.ListMerchantCouponsService.onFileSelected.subscribe(selected => {
            this.selected = selected;
        });
    }

    receiveMessage(event) {
        debugger;
        if (event.checkData == 'coupon') {
            this.coupon = event;
        }
        else if (event.checkData == 'merchant') {
            this.loadMerchantCoupons = true;
            this.merchant = event;
            this.ListMerchantCouponsService.id=this.merchant.id;
            this.ListMerchantCouponsService.getExclusiveCoupons();
            this.ListMerchantCouponsService.getCashbackCoupons();
            this.ListMerchantCouponsService.getFreeshipCoupons();
            this.ListMerchantCouponsService.getOngoingCoupons();
            this.ListMerchantCouponsService.getStaffPickedCoupons();
            this.ListMerchantCouponsService.getExpiredCoupons();
            this.ListMerchantCouponsService.getFeaturedCoupons();
            this.ListMerchantCouponsService.getSeasonalCoupons();

        }
        console.log(this.merchant);
    }


    openCouponAdd() {
        this.router.navigateByUrl("pages/coupon/add");

    }

    resetGrid() {
        this.ListMerchantCouponsService.getCoupons();
    }

    searching() {

        if (this.isSearch == false) {
            this.isSearch = true;
        }

        else {
            this.isSearch = false;
            console.log("close it")
        }
    }
}
