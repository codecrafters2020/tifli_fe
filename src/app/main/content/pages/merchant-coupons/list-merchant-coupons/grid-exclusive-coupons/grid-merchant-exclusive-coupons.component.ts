import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort } from '@angular/material';
import { DataSource } from '@angular/cdk/collections';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';

import { ListMerchantCouponsService } from '../list-merchant-coupons.service';
import { fuseAnimations } from '@fuse/animations/index';
import { FuseUtils } from '@fuse/utils';

@Component({
    selector   : 'grid-merchant-exclusive-coupons',
    templateUrl: './grid-merchant-exclusive-coupons.component.html',
    styleUrls  : ['./grid-merchant-exclusive-coupons.component.scss'],
    animations : fuseAnimations
})
export class GridMerchantExclusiveCouponsComponent implements OnInit
{
    files: any;
    dataSource: GridUserDataSource | null;
    displayedColumns = ['icon', 'coupon_id','title', 'description','type','start_date','end_date', 'revenue', 'clicks', 'orders', 'detail-button', 'modified'];
    selected: any;
    pageSize: number;
    pageSizeOptions: number[];

    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild('filter') filter: ElementRef;
    @ViewChild(MatSort) sort: MatSort;

    constructor(private ListMerchantCouponsService: ListMerchantCouponsService, private router: Router)
    {
        this.pageSize = 25;
        this.pageSizeOptions = [this.pageSize, this.pageSize * 2, this.pageSize * 4]; 
        this.ListMerchantCouponsService.onFilesChanged.subscribe(files => {
            this.files = files;
        });
        this.ListMerchantCouponsService.onFileSelected.subscribe(selected => {
            this.selected = selected;
        });

    }

    ngOnInit()
    {

        this.dataSource = new GridUserDataSource(this.ListMerchantCouponsService, this.paginator, this.sort);

    }

    onSelect(selected)
    {
        debugger;
        this.router.navigateByUrl("pages/coupon/edit/" + selected.id);
    }

    openUserDashboard(event,userid){
        this.router.navigateByUrl("apps/dashboards/project/" + userid);
    }

    openUserEdit(event,id){
        this.router.navigateByUrl("pages/coupon/edit/" + id);
    }
}

export class GridUserDataSource extends DataSource<any>
{
    _filterChange = new BehaviorSubject('');
    _filteredDataChange = new BehaviorSubject('');

    get filteredData(): any
    {
        return this._filteredDataChange.value;
    }

    set filteredData(value: any)
    {
        this._filteredDataChange.next(value);
    }

    get filter(): string
    {
        return this._filterChange.value;
    }

    set filter(filter: string)
    {
        this._filterChange.next(filter);
    }
    constructor(private ListMerchantCouponsService: ListMerchantCouponsService, private _paginator: MatPaginator, private _sort: MatSort)
    {
        super();
        debugger;
        this.filteredData = this.ListMerchantCouponsService.exclusiveCoupons;
    }

    connect(): Observable<any[]>
    {
        const displayDataChanges = [
            this.ListMerchantCouponsService.onFilesChanged,
            this._paginator.page,
            this._filterChange,

        ];

        return Observable.merge(...displayDataChanges).map(() => {
            let data = this.ListMerchantCouponsService.exclusiveCoupons.slice();

            data = this.filterData(data);

            this.filteredData = [...data];


            // Grab the page's slice of data.
            const startIndex = this._paginator.pageIndex * this._paginator.pageSize;
            return data.splice(startIndex, this._paginator.pageSize);
        });
    }

    filterData(data)
    {
        if ( !this.filter )
        {
            return data;
        }
        return FuseUtils.filterArrayByString(data, this.filter);
    }

    disconnect()
    {
    }
}
