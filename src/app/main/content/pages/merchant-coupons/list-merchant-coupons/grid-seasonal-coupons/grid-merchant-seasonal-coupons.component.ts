import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort } from '@angular/material';
import { DataSource } from '@angular/cdk/collections';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';

import { ListMerchantCouponsService } from '../list-merchant-coupons.service';
import { fuseAnimations } from '@fuse/animations/index';
import { FuseUtils } from '@fuse/utils';

@Component({
    selector: 'grid-merchant-seasonal-coupons',
    templateUrl: './grid-merchant-seasonal-coupons.component.html',
    styleUrls: ['./grid-merchant-seasonal-coupons.component.scss'],
    animations: fuseAnimations
})
export class GridMerchantSeasonalCouponsComponent implements OnInit {
    files: any;
    dataSource: GridUserDataSource | null;
    displayedColumns = ['icon', 'coupon_id','title', 'description','type','start_date','end_date', 'revenue', 'clicks', 'orders', 'seasonalEvent', 'detail-button', 'modified'];
    selected: any;
    filterOptionList: any = [
        {
            title: 'show all deals',
            days: '',
            type: '',
            hideUpcoming: false
        },
        {
            title: 'show all upcoming',
            days: '',
            type: '',
            hideUpcoming: false
        },
        {
            title: 'show upcoming within 7 days',
            days: 7,
            type: '',
            hideUpcoming: false
        },
        {
            title: 'show upcoming within 14 days',
            days: 14,
            type: '',
            hideUpcoming: false
        },
        {
            title: 'show upcoming within 30 days',
            days: 30,
            type: '',
            hideUpcoming: false
        },
        {
            title: 'show product deals',
            days: '',
            type: 'Deal',
            hideUpcoming: false
        },
        {
            title:'hide all upcoming',
            days:'',
            type:'',
            hideUpcoming: true

        }
    ];

    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild('filter') filter: ElementRef;
    @ViewChild(MatSort) sort: MatSort;

    constructor(private ListMerchantCouponsService: ListMerchantCouponsService, private router: Router) {

        this.ListMerchantCouponsService.onFilesChanged.subscribe(files => {
            this.files = files;
        });
        this.ListMerchantCouponsService.onFileSelected.subscribe(selected => {
            this.selected = selected;
        });
    }

    filterCoupons(event: any) 
    {
        debugger;
        var days=event.value.days;
        var type=event.value.type;
        var hideUpcoming=event.value.hideUpcoming;

        var merchantTagValue='';
        var merchantNameValue='';
        var seasonalEventValue='';
        var couponTypesValue = 'Code';
        var couponStatusesValue='Active';
        var enableOngoingOffers= '';
        var enableExclusiveOffers='';
        var enableExpiringSoon='';
        var enableFreeShippingOffers='';
        var onGoingOffers=true;
        var exclusiveOffers=true;
        var expiringSoon=true;
        var freeShipping=true;
      // this.btnDisabled = true;
      this.ListMerchantCouponsService.getFilteredCoupons('', 
                                                '', 
                                                'couponCode,asc',
                                                merchantTagValue,
                                                merchantNameValue,
                                                seasonalEventValue,
                                                couponTypesValue,
                                                couponStatusesValue,
                                                enableOngoingOffers ? (onGoingOffers === true ? 1 : 0) : '',
                                                enableExclusiveOffers ? (exclusiveOffers === true ? 1 : 0) : '',
                                                enableExpiringSoon ? (expiringSoon === true ? 1 : 0) : '',
                                                enableFreeShippingOffers ? (freeShipping === true ? 1: 0) : ''
                                              )
        .then((response) => {

        })
    }

    ngOnInit() {
        // debugger;
        // this.dataSource = new GridUserDataSource(this.ListCouponService, this.paginator, this.sort);
        this.dataSource = new GridUserDataSource(this.ListMerchantCouponsService, this.paginator, this.sort);
        // Observable.fromEvent(this.filter.nativeElement, 'keyup')
        //           .debounceTime(150)
        //           .distinctUntilChanged()
        //           .subscribe(() => {
        //               if ( !this.dataSource )
        //               {
        //                   return;
        //               }
        //               this.dataSource.filter = this.filter.nativeElement.value;
        //           });
    }

    onSelect(selected) {
        debugger;
        // this.ListCouponService.onFileSelected.next(selected);
        this.router.navigateByUrl("pages/coupon/edit/" + selected.id);
    }

    openUserDashboard(event, userid) {
        this.router.navigateByUrl("apps/dashboards/project/" + userid);
    }

    openUserEdit(event, id) {
        this.router.navigateByUrl("pages/coupon/edit/" + id);
    }
}

export class GridUserDataSource extends DataSource<any>
{
    _filterChange = new BehaviorSubject('');
    _filteredDataChange = new BehaviorSubject('');

    get filteredData(): any {
        return this._filteredDataChange.value;
    }

    set filteredData(value: any) {
        this._filteredDataChange.next(value);
    }

    get filter(): string {
        return this._filterChange.value;
    }

    set filter(filter: string) {
        this._filterChange.next(filter);
    }
    constructor(private ListMerchantCouponsService: ListMerchantCouponsService, private _paginator: MatPaginator, private _sort: MatSort) {
        super();
        this.filteredData = this.ListMerchantCouponsService.seasonalCoupons;
    }

    /** Connect function called by the table to retrieve one stream containing the data to render. */
    // connect(): Observable<any[]>
    // {
    //     return this.ListCouponService.onFilesChanged;
    // }
    connect(): Observable<any[]> {
        const displayDataChanges = [
            this.ListMerchantCouponsService.onFilesChanged,
            this._paginator.page,
            this._filterChange,
            // this._sort.sortChange
        ];

        return Observable.merge(...displayDataChanges).map(() => {
            let data = this.ListMerchantCouponsService.seasonalCoupons.slice();

            data = this.filterData(data);

            this.filteredData = [...data];

            //data = this.sortData(data);

            // Grab the page's slice of data.
            const startIndex = this._paginator.pageIndex * this._paginator.pageSize;
            return data.splice(startIndex, this._paginator.pageSize);
        });
    }

    filterData(data) {
        if (!this.filter) {
            return data;
        }
        return FuseUtils.filterArrayByString(data, this.filter);
    }

    // sortData(data): any[]
    // {
    //     if ( !this._sort.active || this._sort.direction === '' )
    //     {
    //         return data;
    //     }

    //     return data.sort((a, b) => {
    //         let propertyA: number | string = '';
    //         let propertyB: number | string = '';

    //         switch ( this._sort.active )
    //         {
    //             case 'id':
    //                 [propertyA, propertyB] = [a.id, b.id];
    //                 break;
    //             case 'name':
    //                 [propertyA, propertyB] = [a.name, b.name];
    //                 break;
    //             case 'categories':
    //                 [propertyA, propertyB] = [a.categories[0], b.categories[0]];
    //                 break;
    //             case 'price':
    //                 [propertyA, propertyB] = [a.priceTaxIncl, b.priceTaxIncl];
    //                 break;
    //             case 'quantity':
    //                 [propertyA, propertyB] = [a.quantity, b.quantity];
    //                 break;
    //             case 'active':
    //                 [propertyA, propertyB] = [a.active, b.active];
    //                 break;
    //         }

    //         const valueA = isNaN(+propertyA) ? propertyA : +propertyA;
    //         const valueB = isNaN(+propertyB) ? propertyB : +propertyB;

    //         return (valueA < valueB ? -1 : 1) * (this._sort.direction === 'asc' ? 1 : -1);
    //     });
    // }

    disconnect() {
    }
}
