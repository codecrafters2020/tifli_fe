import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { AppService } from '../../../../../app.service';

@Injectable()
export class EditstudentService implements Resolve<any>
{

    categoriesList : any[];
    usersList: any[];
    barcodesList : any[];
    affiliateNetworksList : any[];
    studentAffiliateStatusList : any[];
    studentList : any[];
    studentFound : boolean=false;
    studentTierList : any[];
    id:any;
    student:any;

    constructor(private http: HttpClient, private appService: AppService)
    {
    }

    /**
     * The File Manager App Main Resolver
     * 
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any
    {

        this.id=route.params['id'];

        // return new Promise((resolve, reject) => {

        //     Promise.all([
        //         this.getCategories(),
        //         this.getUsers(),
        //         this.getBarcodes(),
        //         this.getAffiliateNetworks(),
        //         this.getAffiliateNetworkStatuses(),
        //         this.getMerchants(),
        //         this.getMerchantById(),
        //         this.getMerchantTiers()
        //     ]).then(
        //         ([categoriesList,usersList,barcodesList,affiliateNetworksList,merchantAffiliateStatusList,merchantsList,merchant,merchantTierList]) => {
        //             resolve();
        //         },
        //         reject
        //     );
        // });
    }

    getstudent(): Promise<any>
    {

        return new Promise((resolve, reject) => {
            this.http.get(this.appService.merchantService)
                .subscribe((response: any) => {
                    console.log(response);
                    if(response.meta.code=="200")
                    {
                        this.studentList=response.data;
                    }
                    resolve(response);
                }, reject);
        });
    }  
    
    findstudentByName(studentName: string ): Promise<any>
    {
        return new Promise((resolve, reject) => {
            this.http.get(this.appService.merchantService + 'student/'+studentName)
                .subscribe((response: any) => {
                    console.log(response);
                    //debugger;
                    if(response.meta.code=="200")
                    {
                        this.studentFound=true;
                    }
                    resolve(response);
                }, reject);
        });
    }  

    getstudentById(): Promise<any>
    {
        return new Promise((resolve, reject) => {
            this.http.get(this.appService.merchantService +this.id)
                .subscribe((response: any) => {
                    console.log(response);
                    if(response.meta.code=="200")
                    {
                        this.student=response.data;
                    }
                    resolve(response);
                }, reject);
        });
    }  

    getCategories(): Promise<any>
    {

        return new Promise((resolve, reject) => {

            this.http.get(this.appService.merchantService + 'categories/')
                .subscribe((response: any) => {
                    console.log(response);
                    if(response.meta.code=="200")
                    {
                        this.categoriesList=response.data;
                    }

                    resolve(response);
                }, reject);
        });
    }

    getUsers(): Promise<any>
    {

        return new Promise((resolve, reject) => {
            this.http.get(this.appService.userService + 'users/')
                .subscribe((response: any) => {
                    console.log(response);
                    if(response.meta.code=="200")
                    {
                        this.usersList=response.data;
                    }
                    resolve(response);
                }, reject);
        });
    }

    getBarcodes(): Promise<any>
    {

        return new Promise((resolve, reject) => {
            this.http.get(this.appService.merchantService + 'barcodes/')
                .subscribe((response: any) => {
                    console.log(response);
                    if(response.meta.code=="200")
                    {
                        this.barcodesList=response.data;
                    }
                    resolve(response);
                }, reject);
        });
    }

    getAffiliateNetworks(): Promise<any>
    {

        return new Promise((resolve, reject) => {
            this.http.get(this.appService.merchantService + 'affiliates/')
                .subscribe((response: any) => {
                    console.log(response);
                    if(response.meta.code=="200")
                    {
                        this.affiliateNetworksList=response.data;
                    }
                    resolve(response);
                }, reject);
        });
    }

    getAffiliateNetworkStatuses(): Promise<any>
    {

        return new Promise((resolve, reject) => {
            this.http.get(this.appService.merchantService + 'affiliates/statuses/')
                .subscribe((response: any) => {
                    console.log(response);
                    if(response.meta.code=="200")
                    {
                        this.studentAffiliateStatusList=response.data;
                    }
                    resolve(response);
                }, reject);
        });
    }

    getstudentTiers(): Promise<any> {

        return new Promise((resolve, reject) => {
            this.http.get(this.appService.merchantService + 'tiers/')
                .subscribe((response: any) => {
                    console.log(response);
                    if (response.meta.code == "200") {
                        this.studentTierList = response.data;
                    }
                    resolve(response);
                }, reject);
        });
    }

    updatestudent(student): Promise<any>
    {
        return new Promise((resolve, reject) => {
            debugger;
            // this.http.post('https://httpbin.org/post', merchant)
            console.log("before merchnt")
            console.log(this.appService.merchantService)
            this.http.put(this.appService.merchantService + student.id, student)
                .subscribe((response: any) => {
                    console.log("after merchnt")
                    console.log(this.appService.merchantService)
                    resolve(response);
                }, reject);
        });
    }

}
