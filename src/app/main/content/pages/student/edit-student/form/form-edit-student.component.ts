
import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators, FormsModule, FormControl } from '@angular/forms';
import { MatSnackBar } from '@angular/material';
import { EditstudentService } from '../edit-student.service';
import { Router } from '@angular/router';
import { FancyImageUploaderOptions, UploadedFile } from 'ng2-fancy-image-uploader';
import { getLocaleDateTimeFormat } from '@angular/common';
import { AppService } from '../../../../../../app.service';
import { DISABLED } from '@angular/forms/src/model';

@Component({
    selector: 'fuse-file-list',
    templateUrl: './form-edit-student.component.html',
    styleUrls: ['./form-edit-student.component.scss'],
})
export class FormEditstudentComponent implements OnInit {
    //form: FormGroup;
    formErrors: any;
    test: any;
    step: number;
    student: any = {};

    // Horizontal Stepper
    horizontalStepperStep1: FormGroup;
    horizontalStepperStep2: FormGroup;
    horizontalStepperStep3: FormGroup;
    horizontalStepperStep4: FormGroup;
    horizontalStepperStep5: FormGroup;
    horizontalStepperStep6: FormGroup;

    horizontalStepperStep1Errors: any;
    horizontalStepperStep2Errors: any;
    horizontalStepperStep3Errors: any;
    horizontalStepperStep4Errors: any;
    horizontalStepperStep5Errors: any;
    horizontalStepperStep6Errors: any;

    default_option = {
        id: "-1",
        name: "None"
    }

    default_choice = { firstName: "None", lastName: "", userName: "none" };


    feedSourceOptions = ["Manual", "Affiliate Network", "Coupilia"];

    displayedColumns = ['enable', 'name', 'description'];
    displayedColumnsPanelHeading = ['CategoryName', 'Description', 'isChecked'];

    countries = [
        { "name": "Afghanistan", "code": "AF" },
        { "name": "land Islands", "code": "AX" },
        { "name": "Albania", "code": "AL" },
        { "name": "Algeria", "code": "DZ" },
        { "name": "American Samoa", "code": "AS" },
        { "name": "AndorrA", "code": "AD" },
        { "name": "Angola", "code": "AO" },
        { "name": "Anguilla", "code": "AI" },
        { "name": "Antarctica", "code": "AQ" },
        { "name": "Antigua and Barbuda", "code": "AG" },
        { "name": "Argentina", "code": "AR" },
        { "name": "Armenia", "code": "AM" },
        { "name": "Aruba", "code": "AW" },
        { "name": "Australia", "code": "AU" },
        { "name": "Austria", "code": "AT" },
        { "name": "Azerbaijan", "code": "AZ" },
        { "name": "Bahamas", "code": "BS" },
        { "name": "Bahrain", "code": "BH" },
        { "name": "Bangladesh", "code": "BD" },
        { "name": "Barbados", "code": "BB" },
        { "name": "Belarus", "code": "BY" },
        { "name": "Belgium", "code": "BE" },
        { "name": "Belize", "code": "BZ" },
        { "name": "Benin", "code": "BJ" },
        { "name": "Bermuda", "code": "BM" },
        { "name": "Bhutan", "code": "BT" },
        { "name": "Bolivia", "code": "BO" },
        { "name": "Bosnia and Herzegovina", "code": "BA" },
        { "name": "Botswana", "code": "BW" },
        { "name": "Bouvet Island", "code": "BV" },
        { "name": "Brazil", "code": "BR" },
        { "name": "British Indian Ocean Territory", "code": "IO" },
        { "name": "Brunei Darussalam", "code": "BN" },
        { "name": "Bulgaria", "code": "BG" },
        { "name": "Burkina Faso", "code": "BF" },
        { "name": "Burundi", "code": "BI" },
        { "name": "Cambodia", "code": "KH" },
        { "name": "Cameroon", "code": "CM" },
        { "name": "Canada", "code": "CA" },
        { "name": "Cape Verde", "code": "CV" },
        { "name": "Cayman Islands", "code": "KY" },
        { "name": "Central African Republic", "code": "CF" },
        { "name": "Chad", "code": "TD" },
        { "name": "Chile", "code": "CL" },
        { "name": "China", "code": "CN" },
        { "name": "Christmas Island", "code": "CX" },
        { "name": "Cocos (Keeling) Islands", "code": "CC" },
        { "name": "Colombia", "code": "CO" },
        { "name": "Comoros", "code": "KM" },
        { "name": "Congo", "code": "CG" },
        { "name": "Congo, The Democratic Republic of the", "code": "CD" },
        { "name": "Cook Islands", "code": "CK" },
        { "name": "Costa Rica", "code": "CR" },
        { "name": "Cote D\"Ivoire", "code": "CI" },
        { "name": "Croatia", "code": "HR" },
        { "name": "Cuba", "code": "CU" },
        { "name": "Cyprus", "code": "CY" },
        { "name": "Czech Republic", "code": "CZ" },
        { "name": "Denmark", "code": "DK" },
        { "name": "Djibouti", "code": "DJ" },
        { "name": "Dominica", "code": "DM" },
        { "name": "Dominican Republic", "code": "DO" },
        { "name": "Ecuador", "code": "EC" },
        { "name": "Egypt", "code": "EG" },
        { "name": "El Salvador", "code": "SV" },
        { "name": "Equatorial Guinea", "code": "GQ" },
        { "name": "Eritrea", "code": "ER" },
        { "name": "Estonia", "code": "EE" },
        { "name": "Ethiopia", "code": "ET" },
        { "name": "Falkland Islands (Malvinas)", "code": "FK" },
        { "name": "Faroe Islands", "code": "FO" },
        { "name": "Fiji", "code": "FJ" },
        { "name": "Finland", "code": "FI" },
        { "name": "France", "code": "FR" },
        { "name": "French Guiana", "code": "GF" },
        { "name": "French Polynesia", "code": "PF" },
        { "name": "French Southern Territories", "code": "TF" },
        { "name": "Gabon", "code": "GA" },
        { "name": "Gambia", "code": "GM" },
        { "name": "Georgia", "code": "GE" },
        { "name": "Germany", "code": "DE" },
        { "name": "Ghana", "code": "GH" },
        { "name": "Gibraltar", "code": "GI" },
        { "name": "Greece", "code": "GR" },
        { "name": "Greenland", "code": "GL" },
        { "name": "Grenada", "code": "GD" },
        { "name": "Guadeloupe", "code": "GP" },
        { "name": "Guam", "code": "GU" },
        { "name": "Guatemala", "code": "GT" },
        { "name": "Guernsey", "code": "GG" },
        { "name": "Guinea", "code": "GN" },
        { "name": "Guinea-Bissau", "code": "GW" },
        { "name": "Guyana", "code": "GY" },
        { "name": "Haiti", "code": "HT" },
        { "name": "Heard Island and Mcdonald Islands", "code": "HM" },
        { "name": "Holy See (Vatican City State)", "code": "VA" },
        { "name": "Honduras", "code": "HN" },
        { "name": "Hong Kong", "code": "HK" },
        { "name": "Hungary", "code": "HU" },
        { "name": "Iceland", "code": "IS" },
        { "name": "India", "code": "IN" },
        { "name": "Indonesia", "code": "ID" },
        { "name": "Iran, Islamic Republic Of", "code": "IR" },
        { "name": "Iraq", "code": "IQ" },
        { "name": "Ireland", "code": "IE" },
        { "name": "Isle of Man", "code": "IM" },
        { "name": "Israel", "code": "IL" },
        { "name": "Italy", "code": "IT" },
        { "name": "Jamaica", "code": "JM" },
        { "name": "Japan", "code": "JP" },
        { "name": "Jersey", "code": "JE" },
        { "name": "Jordan", "code": "JO" },
        { "name": "Kazakhstan", "code": "KZ" },
        { "name": "Kenya", "code": "KE" },
        { "name": "Kiribati", "code": "KI" },
        { "name": "Korea, Democratic People\"S Republic of", "code": "KP" },
        { "name": "Korea, Republic of", "code": "KR" },
        { "name": "Kuwait", "code": "KW" },
        { "name": "Kyrgyzstan", "code": "KG" },
        { "name": "Lao People\"S Democratic Republic", "code": "LA" },
        { "name": "Latvia", "code": "LV" },
        { "name": "Lebanon", "code": "LB" },
        { "name": "Lesotho", "code": "LS" },
        { "name": "Liberia", "code": "LR" },
        { "name": "Libyan Arab Jamahiriya", "code": "LY" },
        { "name": "Liechtenstein", "code": "LI" },
        { "name": "Lithuania", "code": "LT" },
        { "name": "Luxembourg", "code": "LU" },
        { "name": "Macao", "code": "MO" },
        { "name": "Macedonia, The Former Yugoslav Republic of", "code": "MK" },
        { "name": "Madagascar", "code": "MG" },
        { "name": "Malawi", "code": "MW" },
        { "name": "Malaysia", "code": "MY" },
        { "name": "Maldives", "code": "MV" },
        { "name": "Mali", "code": "ML" },
        { "name": "Malta", "code": "MT" },
        { "name": "Marshall Islands", "code": "MH" },
        { "name": "Martinique", "code": "MQ" },
        { "name": "Mauritania", "code": "MR" },
        { "name": "Mauritius", "code": "MU" },
        { "name": "Mayotte", "code": "YT" },
        { "name": "Mexico", "code": "MX" },
        { "name": "Micronesia, Federated States of", "code": "FM" },
        { "name": "Moldova, Republic of", "code": "MD" },
        { "name": "Monaco", "code": "MC" },
        { "name": "Mongolia", "code": "MN" },
        { "name": "Montenegro", "code": "ME" },
        { "name": "Montserrat", "code": "MS" },
        { "name": "Morocco", "code": "MA" },
        { "name": "Mozambique", "code": "MZ" },
        { "name": "Myanmar", "code": "MM" },
        { "name": "Namibia", "code": "NA" },
        { "name": "Nauru", "code": "NR" },
        { "name": "Nepal", "code": "NP" },
        { "name": "Netherlands", "code": "NL" },
        { "name": "Netherlands Antilles", "code": "AN" },
        { "name": "New Caledonia", "code": "NC" },
        { "name": "New Zealand", "code": "NZ" },
        { "name": "Nicaragua", "code": "NI" },
        { "name": "Niger", "code": "NE" },
        { "name": "Nigeria", "code": "NG" },
        { "name": "Niue", "code": "NU" },
        { "name": "Norfolk Island", "code": "NF" },
        { "name": "Northern Mariana Islands", "code": "MP" },
        { "name": "Norway", "code": "NO" },
        { "name": "Oman", "code": "OM" },
        { "name": "Pakistan", "code": "PK" },
        { "name": "Palau", "code": "PW" },
        { "name": "Palestinian Territory, Occupied", "code": "PS" },
        { "name": "Panama", "code": "PA" },
        { "name": "Papua New Guinea", "code": "PG" },
        { "name": "Paraguay", "code": "PY" },
        { "name": "Peru", "code": "PE" },
        { "name": "Philippines", "code": "PH" },
        { "name": "Pitcairn", "code": "PN" },
        { "name": "Poland", "code": "PL" },
        { "name": "Portugal", "code": "PT" },
        { "name": "Puerto Rico", "code": "PR" },
        { "name": "Qatar", "code": "QA" },
        { "name": "Reunion", "code": "RE" },
        { "name": "Romania", "code": "RO" },
        { "name": "Russian Federation", "code": "RU" },
        { "name": "RWANDA", "code": "RW" },
        { "name": "Saint Helena", "code": "SH" },
        { "name": "Saint Kitts and Nevis", "code": "KN" },
        { "name": "Saint Lucia", "code": "LC" },
        { "name": "Saint Pierre and Miquelon", "code": "PM" },
        { "name": "Saint Vincent and the Grenadines", "code": "VC" },
        { "name": "Samoa", "code": "WS" },
        { "name": "San Marino", "code": "SM" },
        { "name": "Sao Tome and Principe", "code": "ST" },
        { "name": "Saudi Arabia", "code": "SA" },
        { "name": "Senegal", "code": "SN" },
        { "name": "Serbia", "code": "RS" },
        { "name": "Seychelles", "code": "SC" },
        { "name": "Sierra Leone", "code": "SL" },
        { "name": "Singapore", "code": "SG" },
        { "name": "Slovakia", "code": "SK" },
        { "name": "Slovenia", "code": "SI" },
        { "name": "Solomon Islands", "code": "SB" },
        { "name": "Somalia", "code": "SO" },
        { "name": "South Africa", "code": "ZA" },
        { "name": "South Georgia and the South Sandwich Islands", "code": "GS" },
        { "name": "Spain", "code": "ES" },
        { "name": "Sri Lanka", "code": "LK" },
        { "name": "Sudan", "code": "SD" },
        { "name": "Suriname", "code": "SR" },
        { "name": "Svalbard and Jan Mayen", "code": "SJ" },
        { "name": "Swaziland", "code": "SZ" },
        { "name": "Sweden", "code": "SE" },
        { "name": "Switzerland", "code": "CH" },
        { "name": "Syrian Arab Republic", "code": "SY" },
        { "name": "Taiwan, Province of China", "code": "TW" },
        { "name": "Tajikistan", "code": "TJ" },
        { "name": "Tanzania, United Republic of", "code": "TZ" },
        { "name": "Thailand", "code": "TH" },
        { "name": "Timor-Leste", "code": "TL" },
        { "name": "Togo", "code": "TG" },
        { "name": "Tokelau", "code": "TK" },
        { "name": "Tonga", "code": "TO" },
        { "name": "Trinidad and Tobago", "code": "TT" },
        { "name": "Tunisia", "code": "TN" },
        { "name": "Turkey", "code": "TR" },
        { "name": "Turkmenistan", "code": "TM" },
        { "name": "Turks and Caicos Islands", "code": "TC" },
        { "name": "Tuvalu", "code": "TV" },
        { "name": "Uganda", "code": "UG" },
        { "name": "Ukraine", "code": "UA" },
        { "name": "United Arab Emirates", "code": "AE" },
        { "name": "United Kingdom", "code": "GB" },
        { "name": "United States", "code": "US" },
        { "name": "United States Minor Outlying Islands", "code": "UM" },
        { "name": "Uruguay", "code": "UY" },
        { "name": "Uzbekistan", "code": "UZ" },
        { "name": "Vanuatu", "code": "VU" },
        { "name": "Venezuela", "code": "VE" },
        { "name": "Viet Nam", "code": "VN" },
        { "name": "Virgin Islands, British", "code": "VG" },
        { "name": "Virgin Islands, U.S.", "code": "VI" },
        { "name": "Wallis and Futuna", "code": "WF" },
        { "name": "Western Sahara", "code": "EH" },
        { "name": "Yemen", "code": "YE" },
        { "name": "Zambia", "code": "ZM" },
        { "name": "Zimbabwe", "code": "ZW" }
    ];


    images: any[] = [];
    categoriesList: any[] = [];
    usersList: any[];
    barcodesList: any[] = [];
    affiliateNetworksList: any[];
    studentAffiliateStatusList: any[];
    studentTierList: any[] = [];
    studentList: any[];
    studentName: string = "";
    lastBarCode: any;
    lastMasterStore: any;
    laststudentTier: any;

    largeLogoOptions: FancyImageUploaderOptions = {
        thumbnailHeight: 200,
        thumbnailWidth: 250,
        //uploadUrl: 'https://fancy-image-uploader-demo.azurewebsites.net/api/demo/upload',
        //uploadUrl: 'http://192.168.1.76:8765/cms/api/uploads/',
        uploadUrl: this.appService.uploadService + 'uploads/',
        allowedImageTypes: ['image/png', 'image/jpeg'],
        maxImageSize: 3,
    };

    smallLogoOptions: FancyImageUploaderOptions = {
        thumbnailHeight: 200,
        thumbnailWidth: 250,
        //uploadUrl: 'https://fancy-image-uploader-demo.azurewebsites.net/api/demo/upload',
        //uploadUrl: 'http://192.168.1.76:8765/cms/api/uploads/',
        uploadUrl: this.appService.uploadService + 'uploads/',
        allowedImageTypes: ['image/png', 'image/jpeg'],
        maxImageSize: 3,
    };

    categoriesGroups: any[];
    subCategoriesGroups: any[] = [];
    catControls: any[] = [];
    largeImgUrl: string = "";
    smallImgUrl: string = "";

    constructor(private EditstudentService: EditstudentService, private formBuilder: FormBuilder, private router: Router, private snackBar: MatSnackBar, private appService: AppService) {

        debugger;

        // this.merchant = EditMerchantService.merchant;
        if (!this.student.hasOwnProperty('linkToMasterUsStore') || this.student.linkToMasterUsStore.id < 1) {
            this.student.linkToMasterUsStore = this.default_option;
        }

        if (!this.student.hasOwnProperty('barcodeFormat') || this.student.barcodeFormat.id < 1) {
            this.student.barcodeFormat = this.default_option;
        }

        if (!this.student.hasOwnProperty('tierLookup') || this.student.tierLookup.id < 1) {
            this.student.tierLookup = this.default_option;
        }

        if (!this.student.hasOwnProperty('manager') || this.student.manager.id < 1) {
            this.student.manager = this.default_choice;
        }

        if (!this.student.hasOwnProperty('accountManager') || this.student.accountManager.id < 1) {
            this.student.accountManager = this.default_choice;
        }


        this.largeImgUrl = this.appService.cdnUrl + this.student.id + "_large.png";
        this.smallImgUrl = this.appService.cdnUrl + this.student.id + "_small.png";
        this.categoriesList = EditstudentService.categoriesList;
        // this.categoriesGroups = JSON.parse(JSON.stringify(this.categoriesList));

        // for (let c = 0; c < this.merchant.categoriesList.length; c++) {

        //     if (this.merchant.categoriesList[c].checked === true) {
        //         for (let i = 0; i < this.categoriesList.length; i++) {
        //             //debugger;
        //             if (this.merchant.categoriesList[c].id === this.categoriesList[i].id) {
        //                 this.categoriesList[i].checked = true;
        //             }
        //             else {
        //                 if (this.categoriesList[i].subCategories != null) {
        //                     for (let j = 0; j < this.categoriesList[i].subCategories.length; j++) {

        //                         //debugger;
        //                         if (this.categoriesList[i].subCategories[j].id === this.merchant.categoriesList[c].id) {
        //                             this.categoriesList[i].subCategories[j].checked = true;
        //                         }
        //                     }
        //                 }
        //             }
        //         }
        //     }
        // }
        console.log(JSON.stringify(this.categoriesList));

        // for (let i = 0; i < this.categoriesGroups.length; i++) {

        //     //debugger;
        //     this.subCategoriesGroups[i] =
        //         {
        //             name: 'Sub-Categories',
        //             subCategories: []
        //         };

        //     if (this.categoriesGroups[i].subCategories != null)
        //         this.subCategoriesGroups[i].subCategories = this.categoriesGroups[i].subCategories.slice(0);
        // }

        // for (let i = 0; i < this.categoriesGroups.length; i++) {

        //     //debugger;
        //     if (this.categoriesGroups[i].subCategories != null) {
        //         this.categoriesGroups[i].subCategories.splice(0, this.categoriesGroups[i].subCategories.length);
        //         this.categoriesGroups[i].subCategories[0] = this.subCategoriesGroups[i];
        //     }
        // }
        debugger;
        console.log(JSON.stringify(this.categoriesGroups));

        this.usersList = EditstudentService.usersList;
        // this.usersList.push(this.default_choice);
        this.barcodesList = EditstudentService.barcodesList;
        // this.barcodesList.push(this.default_option);
        this.affiliateNetworksList = EditstudentService.affiliateNetworksList;
        this.studentAffiliateStatusList = EditstudentService.studentAffiliateStatusList;
        this.studentList = EditstudentService.studentList;
        // this.merchantsList.push(this.default_option);
        this.studentTierList = EditstudentService.studentTierList;
        // this.merchantTierList.push(this.default_option);
        debugger;

        // // Horizontal Stepper form error
        this.horizontalStepperStep1Errors = {
            name: {},
            country: {},
            title: {},
            h1: {},
            searchTerms: {},
            barcodeFormat: {}
        };

        // this.horizontalStepperStep2Errors = {
        //     merchantLargeLogo  : {},
        //     merchantSmallLogo  : {}
        // };

        this.horizontalStepperStep3Errors = {
            studentCategory: {}
        };

        this.horizontalStepperStep4Errors = {
            affiliateNetwork: {},
            studentAffiliateStatus: {},
            affiliatestudentId: {}
        };

        this.horizontalStepperStep5Errors = {
            redirectUrl: {},
            ciUrl: {},
            normalizedUrl: {}
        };

        this.horizontalStepperStep6Errors = {
            // manager: {},
            // accountManager: {},
            //adminMemo: {},
            //accountManagerMemo: {}
        };

    }

    ngOnInit() {

        // Horizontal Stepper form steps
        this.horizontalStepperStep1 = this.formBuilder.group({

            name: ['', [Validators.required]],
            country: ['', Validators.required],
            alias: [''],
            description: [''],
            title: ['', Validators.required],
            h1: ['', Validators.required],
            featured: [''],
            monetized: [''],
            displayExpiredOffers: [''],
            isActive: [''],
            searchTerms: ['', Validators.required],
            barcodeFormat: [''],
            linkToMasterUsStore: [''],
            studentTier: ['']
        });
        // this.disableMonetized = false;

        this.horizontalStepperStep2 = this.formBuilder.group({

        });

        this.horizontalStepperStep3 = this.formBuilder.group({

            studentCategory: ['', Validators.required]
        });

        // debugger;
        // for (let i = 0; i < this.categoriesList.length; i++) {
        //     //debugger;
        //     this.horizontalStepperStep3.controls[this.categoriesList[i].name] = new FormControl(false);
        //     if (this.categoriesList[i].subCategories != null) {
        //         for (let j = 0; j < this.categoriesList[i].subCategories.length; j++) {
        //             //debugger;
        //             this.horizontalStepperStep3.controls[this.categoriesList[i].subCategories[j].name] = new FormControl(false);
        //         }
        //     }
        // }

        this.horizontalStepperStep4 = this.formBuilder.group({

            affiliateNetwork: ['', Validators.required],
            studentAffiliateStatus: ['', Validators.required],
            affiliatestudentId: ['', Validators.required]
        });

        this.horizontalStepperStep5 = this.formBuilder.group({

            redirectUrl: ['', Validators.required],
            ciUrl: ['', Validators.required],
            domainUrl: [''],
            normalizedUrl: ['', Validators.required]

        });

        this.horizontalStepperStep6 = this.formBuilder.group({

            manager: [''],
            accountManager: [''],
            adminMemo: [''],
            accountManagerMemo: ['']
        });


        this.horizontalStepperStep1.valueChanges.subscribe(() => {
            this.onFormValuesChanged(this.horizontalStepperStep1, this.horizontalStepperStep1Errors);
        });

        this.horizontalStepperStep2.valueChanges.subscribe(() => {
            this.onFormValuesChanged(this.horizontalStepperStep2, this.horizontalStepperStep2Errors);
        });

        this.horizontalStepperStep3.valueChanges.subscribe(() => {
            this.onFormValuesChanged(this.horizontalStepperStep3, this.horizontalStepperStep3Errors);
        });

        this.horizontalStepperStep4.valueChanges.subscribe(() => {
            this.onFormValuesChanged(this.horizontalStepperStep4, this.horizontalStepperStep4Errors);
        });

        this.horizontalStepperStep5.valueChanges.subscribe(() => {
            this.onFormValuesChanged(this.horizontalStepperStep5, this.horizontalStepperStep5Errors);
        });

        this.horizontalStepperStep6.valueChanges.subscribe(() => {
            this.onFormValuesChanged(this.horizontalStepperStep6, this.horizontalStepperStep6Errors);
        });
        this.horizontalStepperStep1.controls.isActive.valueChanges.subscribe(() => {
            if (this.horizontalStepperStep1.controls.isActive.value == false) {
                this.horizontalStepperStep1.controls.monetized.setValue(false);
                this.horizontalStepperStep1.controls.featured.setValue(false);
            }
        })

        this.horizontalStepperStep1.controls.monetized.valueChanges.subscribe(() => {
            if (this.horizontalStepperStep1.controls.monetized.value == true) {
                if (this.horizontalStepperStep1.controls.isActive.value === false) {
                    this.horizontalStepperStep1.controls.monetized.setValue(false);
                }
                else if(this.horizontalStepperStep1.controls.isActive.value === true){
                
                }
                else if(this.horizontalStepperStep1.controls.isActive.value !== ""){
                    this.horizontalStepperStep1.controls.monetized.setValue(false);
                }
            }

        })

        this.horizontalStepperStep1.controls.featured.valueChanges.subscribe(() => {
            if (this.horizontalStepperStep1.controls.featured.value == true) {
                if (this.horizontalStepperStep1.controls.isActive.value === false) {
                    this.horizontalStepperStep1.controls.featured.setValue(false);
                }
                else if(this.horizontalStepperStep1.controls.isActive.value === true){
                    
                }
                else if(this.horizontalStepperStep1.controls.isActive.value !== ""){
                    this.horizontalStepperStep1.controls.featured.setValue(false);
                }
            }

        })

        // this.horizontalStepperStep1.controls.name.valueChanges.subscribe(() => {
        //     debugger;
        //     let controlValue = this.horizontalStepperStep1.controls.name.value;
        //     let str = this.horizontalStepperStep1.controls.name.value.trim();
        //     //this.merchant.name = str;
        //     if(str !== controlValue)
        //         this.horizontalStepperStep1.controls.name.setValue(str);
        // })



    }

    onFormValuesChanged(horizontalStepperStep: FormGroup, horizontalStepperStepErrors: any) {
        for (const field in horizontalStepperStepErrors) {
            if (!horizontalStepperStepErrors.hasOwnProperty(field)) {
                continue;
            }


            horizontalStepperStepErrors[field] = {};

            const control = horizontalStepperStep.get(field);

            if (control && control.dirty && !control.valid) {
                horizontalStepperStepErrors[field] = control.errors;
            }
        }
    }

    finishHorizontalStepper() {
        this.student.categoriesList = this.categoriesList;
this.lastBarCode = this.student.barcodeFormat;
        this.lastMasterStore = this.student.linkToMasterUsStore;
        this.student = this.student.tierLookup;
        if (this.student.barcodeFormat.id == -1) {
            this.student.barcodeFormat = null;
        }
        if (this.student.linkToMasterUsStore.id == -1) {
            this.student.linkToMasterUsStore = null;
        }
        if (this.student.tierLookup.id == -1) {
            this.student.tierLookup = null;
        }
        if (this.student.manager.id == -1) {
            this.student.manager = null;
        }
        if (this.student.accountManager.id == -1) {
            this.student.accountManager = null;
        }

        debugger;
        this.student.name = this.student.name.trim();

        // alert('You have finished the horizontal stepper!');
        // debugger;
        //this.merchant.categoriesList = this.categoriesList;

        for (let c = 0; c < this.student.categoriesList.length; c++) {

            for (let i = 0; i < this.categoriesList.length; i++) {
                //debugger;
                if (this.student.categoriesList[c].id === this.categoriesList[i].id) {
                    this.student.categoriesList[c].checked = this.categoriesList[i].checked;
                }
                else {
                    if (this.categoriesList[i].subCategories != null) {
                        for (let j = 0; j < this.categoriesList[i].subCategories.length; j++) {

                            //debugger;
                            if (this.categoriesList[i].subCategories[j].id === this.student.categoriesList[c].id) {
                                this.student.categoriesList[c].checked = this.categoriesList[i].subCategories[j].checked;
                            }
                        }
                    }
                }
            }

        }

        this.student.images = this.images;
        try {
            this.EditstudentService.updatestudent(this.student).then(response => {
                debugger;
                this.test = response;
                console.log("response: " + JSON.stringify(response));
                debugger;
                if (response.meta.code !== "200") {
                    this.student.barcodeFormat = this.lastBarCode;
                    this.student.linkToMasterUsStore = this.lastMasterStore;
                    this.student.tierLookup = this.laststudentTier;
                }
                this.snackBar.open(response.meta.message, "Done", {
                    duration: 2000,
                });
                if (response.meta.status == "success")
                    this.redirect('pages/student/list');
            });
        }
        catch (err) {
            debugger;
        }
    }

    redirect(pagename: string) {
        this.router.navigate([pagename]);
    }

    onUploadLargeImage(file: UploadedFile) {
        debugger;
        var response: any = {};
        response = JSON.parse(file.response)

        if (response.meta.code == 200) {
            if (this.images.length >= 1) {
                for (let i = 0; i < this.images.length; i++) {
                    if (this.images[i].suffix == "large") {
                        this.images.splice(i, 1);
                    }
                }
            }
            var largeImage: any = {};
            largeImage.fileName = response.data.fileName;
            largeImage.suffix = "large";
            this.images.push(largeImage);
        }
    }

    onUploadSmallImage(file: UploadedFile) {
        debugger;
        var response: any = {};
        response = JSON.parse(file.response);
        if (response.meta.code == 200) {
            if (this.images.length >= 1) {
                for (let i = 0; i < this.images.length; i++) {
                    if (this.images[i].suffix == "small") {
                        this.images.splice(i, 1);
                    }
                }
            }
            var smallImage: any = {};
            smallImage.fileName = response.data.fileName;
            smallImage.suffix = "small";
            this.images.push(smallImage);
        }
    }



    OnSubcategoriesChangeStatus(values: any, category: any) {
        debugger;
        if (values.checked == false) {
            for (let subcategory of category.subCategories) {
                subcategory.checked = false;
            }
        }

    }
    OnCategoryChangeStatus(values: any, i: any) {
        debugger;
        if (values.checked == true) {
            this.categoriesList[i].checked = true;
        }

    }
    onKey(event: any) { // without type info
        //debugger
        var value = "";
        var valuesArray: any[];
        value += event.target.value + '|';
        valuesArray = value.split('|');

        for (let i = 0; i < value.length; i++) {
            if (i == valuesArray.length - 2) {
                this.studentName = valuesArray[i];
            }
        }
        // this.nameExists();

    }
    nameExists() {

        debugger;
        //const name = this.horizontalStepperStep1.get('name');
        var nameFound;
        this.EditstudentService.findstudentByName(this.studentName);
        if (this.EditstudentService.studentFound == true) {
            //const name = this.horizontalStepperStep1.get('name');
            //console.log("merchant already exists");

            return {
                nameFound: true
            };
        }
        return;

    }
    compareObjects(obj1: any, obj2: any): boolean {
        return obj1.id === obj2.id;
    }

    hideImage(event: any) {
        debugger;
        var id = event.currentTarget.id;
        if (id == "updateLargeImageBtn") {

            var imageBtnControl = document.getElementById('updateLargeImageBtn');
            imageBtnControl.style.display = "none";
            var imageControl = document.getElementById('largeImage');
            imageControl.style.display = "none";
            var uploaderControl = document.getElementById('largeImageUploader');
            uploaderControl.style.display = "block";
            var cancelControl = document.getElementById('CancelUpdateLargeImageBtn');
            cancelControl.style.display = "block";
            cancelControl.style.visibility = "visible";


        }
        else if (id == "updateSmallImageBtn") {

            var imageBtnControl = document.getElementById('updateSmallImageBtn');
            imageBtnControl.style.display = "none";
            var imageControl = document.getElementById('smallImage');
            imageControl.style.display = "none";
            var uploaderControl = document.getElementById('smallImageUploader');
            uploaderControl.style.display = "block";
            var cancelControl = document.getElementById('CancelUpdateSmallImageBtn');
            cancelControl.style.display = "block";
            cancelControl.style.visibility = "visible";

        }
    }

    showImage(event: any) {
        debugger;
        var id = event.currentTarget.id;
        if (id == "CancelUpdateLargeImageBtn") {
            if (this.images.length >= 1) {
                for (let i = 0; i < this.images.length; i++) {
                    if (this.images[i].suffix == "large") {
                        this.images.splice(i, 1);
                    }
                }
            }
            var imageBtnControl = document.getElementById('updateLargeImageBtn');
            imageBtnControl.style.display = "block";
            var uploaderControl = document.getElementById('largeImageUploader');
            uploaderControl.style.display = "none";
            var imageControl = document.getElementById('largeImage');
            imageControl.style.display = "block";
            var cancelControl = document.getElementById('CancelUpdateLargeImageBtn');
            cancelControl.style.display = "none";

        }
        if (id == "CancelUpdateSmallImageBtn") {
            if (this.images.length >= 1) {
                for (let i = 0; i < this.images.length; i++) {
                    if (this.images[i].suffix == "small") {
                        this.images.splice(i, 1);

                    }
                }
            }
            var imageBtnControl = document.getElementById('updateSmallImageBtn');
            imageBtnControl.style.display = "block";
            var uploaderControl = document.getElementById('smallImageUploader');
            uploaderControl.style.display = "none";
            var imageControl = document.getElementById('smallImage');
            imageControl.style.display = "block";
            var cancelControl = document.getElementById('CancelUpdateSmallImageBtn');
            cancelControl.style.display = "none";

        }
    }
}
