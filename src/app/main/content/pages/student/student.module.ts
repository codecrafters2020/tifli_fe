import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { FuseAngularMaterialModule } from '../../components/angular-material/angular-material.module';

import { FuseSharedModule } from '@fuse/shared.module';

const routes = [
    {
        path: 'list',
        loadChildren: './list-student/list-student.module#ListstudentModule'
    },
    {
        path: 'add',
        loadChildren: './add-student/add-student.module#AddStudentModule'
    },
    {
        path: 'edit/:id',
        loadChildren: './edit-student/edit-student.module#EditStudentModule'
    },
    {
        path: 'temp',
        loadChildren: './edit-student/edit-student.module#EditStudentModule'
    },
    
];

@NgModule({
    imports: [
        FuseSharedModule,
        RouterModule.forChild(routes),
        FuseAngularMaterialModule
    ],
    declarations: []
})
export class StudentModule {
}
