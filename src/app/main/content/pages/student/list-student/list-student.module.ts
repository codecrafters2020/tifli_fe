import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CdkTableModule } from '@angular/cdk/table';

import { MatFormFieldModule, MatInputModule, MatSelectModule, MatStepperModule, MatButtonModule, MatIconModule, MatRippleModule, MatSidenavModule, MatSlideToggleModule, MatTableModule, MatMenuModule, MatPaginatorModule, MatSortModule, MatAutocomplete, MatAutocompleteModule } from '@angular/material';

import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatRadioModule } from '@angular/material/radio';
import { FuseSharedModule } from '@fuse/shared.module';

import { ListStudentComponent } from './list-student.component';
import { ListstudentService } from './list-student.service';
import { GridstudentComponent } from './grid/grid-student.component';
import { SidenavstudentMainComponent } from './sidenavs/main/sidenav-student-main.component';
import { SidenavstudentDetailsComponent } from './sidenavs/details/sidenav-student-details.component';

const routes: Routes = [
    {
        path: '**',
        component: ListStudentComponent,
        children: [],
        resolve: {
            files: ListstudentService
        }
    }
];

@NgModule({
    declarations: [
        ListStudentComponent,
        GridstudentComponent,
        SidenavstudentMainComponent,
        SidenavstudentDetailsComponent
    ],
    imports: [
        RouterModule.forChild(routes),
        MatAutocompleteModule,
        CdkTableModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        MatStepperModule,
        MatButtonModule,
        MatIconModule,
        MatRippleModule,
        MatSlideToggleModule,
        MatSidenavModule,
        MatTableModule,
        MatMenuModule,
        MatPaginatorModule,
        MatSortModule,
        FuseSharedModule,
        MatDatepickerModule,
        MatCheckboxModule,
        MatRadioModule
    ],

    providers: [
        ListstudentService
    ]
})
export class ListstudentModule {
}
