import { Component, OnInit, ViewEncapsulation } from '@angular/core';

import { fuseAnimations } from '@fuse/animations';

import { ListstudentService } from './list-student.service';
import { Router } from '@angular/router';

@Component({
    selector     : 'fuse-file-manager',
    templateUrl  : './list-student.component.html',
    styleUrls    : ['./list-student.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class ListStudentComponent implements OnInit
{
    selected: any;
    pathArr: string[];

    constructor(private ListStudentService: ListstudentService, private router: Router)
    {
    }

    ngOnInit()
    {
        // this.ListMerchantService.onFileSelected.subscribe(selected => {
        //     this.selected = selected;
        //    // this.pathArr = selected.location.split('>');
        // });
    }

    openUserAdd(){
        this.router.navigateByUrl("pages/student/add/");
    }
}
