import { Component, OnInit } from '@angular/core';

import { fuseAnimations } from '@fuse/animations/index';

import { ListstudentService } from '../../list-student.service';

@Component({
    selector   : 'fuse-file-manager-details-sidenav',
    templateUrl: './sidenav-student-details.component.html',
    styleUrls  : ['./sidenav-student-details.component.scss'],
    animations : fuseAnimations
})
export class SidenavstudentDetailsComponent implements OnInit
{

    selected: any;

    constructor(private ListstudentService: ListstudentService)
    {

    }

    ngOnInit()
    {
        this.ListstudentService.onFileSelected.subscribe(selected => {
            this.selected = selected;
        });
    }

}
