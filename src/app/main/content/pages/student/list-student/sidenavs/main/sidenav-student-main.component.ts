import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatRadioModule} from '@angular/material/radio';
import { FuseAngularMaterialModule } from '../../../../../components/angular-material/angular-material.module';
import {ListstudentService} from '../../list-student.service'


@Component({
    selector   : 'fuse-file-manager-main-sidenav',
    templateUrl: './sidenav-student-main.component.html',
    styleUrls  : ['./sidenav-student-main.component.scss']
})
export class SidenavstudentMainComponent
{
    selected: any;
    form: FormGroup;
    formErrors: any;

    studentSearchResult: any[];


    constructor(private ListstudentService: ListstudentService,
        private router: Router,

        private formBuilder: FormBuilder) {
        // Reactive Form


        this.form = this.formBuilder.group({
            studentname: ['',]
            // seasonalStatus: ['',],
        });

        // Reactive form errors
        this.formErrors = {
            studentname: {},
            // seasonalStatus: {}
        };
    }


    displaySelectedName(item: any) {
        return item;
    }

    ngOnInit() {
        this.form.valueChanges.subscribe(() => {
            // console.log(this.form.value);
        });
        let x: any;
        this.form.controls.studentname.valueChanges
            .subscribe(searchTerm => {
                this.ListstudentService.searchstudentName(searchTerm)
                    .subscribe(studentname => {
                        x = studentname;
                        this.studentSearchResult = studentname;
                        // if(searchTerm == x[0].name){
                        //     this.router.navigateByUrl("pages/student/edit/"+x[0].id);
                        // }
                    });
            });
    }

    callSomeFunction(){
        this.studentSearchResult.forEach(student => {
            if(student.name == this.form.controls.studentname.value){
                    this.router.navigateByUrl("pages/student/edit/"+student.id);
            }
        })
    }
}
