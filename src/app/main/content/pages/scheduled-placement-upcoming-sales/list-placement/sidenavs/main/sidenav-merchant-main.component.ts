import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatRadioModule} from '@angular/material/radio';
import { FuseAngularMaterialModule } from '../../../../../components/angular-material/angular-material.module';
import {ListMerchantService} from '../../list-merchant.service'


@Component({
    selector   : 'fuse-file-manager-main-sidenav',
    templateUrl: './sidenav-merchant-main.component.html',
    styleUrls  : ['./sidenav-merchant-main.component.scss']
})
export class SidenavMerchantMainComponent
{
    selected: any;
    form: FormGroup;
    formErrors: any;

    merchantSearchResult: any[];


    constructor(private ListMerchantService: ListMerchantService,
        private router: Router,

        private formBuilder: FormBuilder) {
        // Reactive Form


        this.form = this.formBuilder.group({
            merchantname: ['',]
            // seasonalStatus: ['',],
        });

        // Reactive form errors
        this.formErrors = {
            merchantname: {},
            // seasonalStatus: {}
        };
    }


    displaySelectedName(item: any) {
        return item;
    }

    ngOnInit() {
        this.form.valueChanges.subscribe(() => {
            // console.log(this.form.value);
        });
        let x: any;
        this.form.controls.merchantname.valueChanges
            .subscribe(searchTerm => {
                this.ListMerchantService.searchMerchantName(searchTerm)
                    .subscribe(merchantname => {
                        x = merchantname;
                        this.merchantSearchResult = merchantname;
                        // if(searchTerm == x[0].name){
                        //     this.router.navigateByUrl("pages/merchant/edit/"+x[0].id);
                        // }
                    });
            });
    }

    callSomeFunction(){
        this.merchantSearchResult.forEach(merchant => {
            if(merchant.name == this.form.controls.merchantname.value){
                    this.router.navigateByUrl("pages/merchant/edit/"+merchant.id);
            }
        })
    }
}
