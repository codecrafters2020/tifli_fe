import { Component, OnInit } from '@angular/core';

import { fuseAnimations } from '@fuse/animations';

import { ListMerchantService } from '../../list-merchant.service';

@Component({
    selector   : 'fuse-file-manager-details-sidenav',
    templateUrl: './sidenav-merchant-details.component.html',
    styleUrls  : ['./sidenav-merchant-details.component.scss'],
    animations : fuseAnimations
})
export class SidenavMerchantDetailsComponent implements OnInit
{

    selected: any;

    constructor(private ListMerchantService: ListMerchantService)
    {

    }

    ngOnInit()
    {
        this.ListMerchantService.onFileSelected.subscribe(selected => {
            this.selected = selected;
        });
    }

}
