import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort } from '@angular/material';
import { DataSource } from '@angular/cdk/collections';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';

import { ProjectDashboardService } from '../project.service';
import { fuseAnimations } from '@fuse/animations/index';
import { FuseUtils } from '@fuse/utils';

@Component({
    selector   : 'fuse-file-list',
    templateUrl: './grid-user.component.html',
    styleUrls  : ['./grid-user.component.scss'],
    animations : fuseAnimations
})
export class GridUserComponent implements OnInit
{
    files: any;
    dataSource: GridUserDataSource | null;
    displayedColumns = ['name'];
    selected: any;

    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild('filter') filter: ElementRef;
    @ViewChild(MatSort) sort: MatSort;

    constructor(private ProjectDashboardService: ProjectDashboardService)
    {
        // this.ProjectDashboardService.onActivityLogChanged.subscribe(files => {
        //     this.files = files;
        // });
        // this.ProjectDashboardService.onFileSelected.subscribe(selected => {
        //     this.selected = selected;
        // });
    }

    ngOnInit()
    {
        //this.dataSource = new FilesDataSource(this.ProjectDashboardService);
        debugger;
        this.dataSource = new GridUserDataSource(this.ProjectDashboardService, this.paginator, this.sort);
    }

    onSelect(selected)
    {
        //this.ProjectDashboardService.onFileSelected.next(selected);
    }
}

export class GridUserDataSource extends DataSource<any>
{
    _filterChange = new BehaviorSubject('');
    _filteredDataChange = new BehaviorSubject('');

    get filteredData(): any
    {
        return this._filteredDataChange.value;
    }

    set filteredData(value: any)
    {
        this._filteredDataChange.next(value);
    }

    get filter(): string
    {
        return this._filterChange.value;
    }

    set filter(filter: string)
    {
        this._filterChange.next(filter);
    }
    constructor(private ProjectDashboardService: ProjectDashboardService,  private _paginator: MatPaginator, private _sort: MatSort)
    {
        super();
        debugger;
        this.filteredData = this.ProjectDashboardService.activityLog;
    }

    /** Connect function called by the table to retrieve one stream containing the data to render. */
    connect(): Observable<any[]>
    {
        //return this.ProjectDashboardService.onActivityLogChanged;
        const displayDataChanges = [
            this.ProjectDashboardService.onActivityLogChanged,
            this._paginator.page,
            this._filterChange,
            // this._sort.sortChange
        ];

        return Observable.merge(...displayDataChanges).map(() => {
            let data = this.ProjectDashboardService.activityLog.slice();

            data = this.filterData(data);

            this.filteredData = [...data];

            //data = this.sortData(data);

            // Grab the page's slice of data.
            const startIndex = this._paginator.pageIndex * this._paginator.pageSize;
            return data.splice(startIndex, this._paginator.pageSize);
        });
    }

    filterData(data)
    {
        if ( !this.filter )
        {
            return data;
        }
        return FuseUtils.filterArrayByString(data, this.filter);
    }

    disconnect()
    {
    }
}
