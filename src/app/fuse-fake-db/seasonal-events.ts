export class SeasonalEventsFakeDb
{
    public static seasonalEvents = {
        'data': [
            {
                'id': 1,
                'name': '50% off Black Friday Deal',
                'description_heading': "SOme heading",
                'status': "active",
                'date_created': new Date()
            }
        ]
    };
}