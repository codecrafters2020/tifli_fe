import {
    startOfDay,
    endOfDay,
    subDays,
    addDays,
    endOfMonth,
    isSameDay,
    isSameMonth,
    addHours
} from 'date-fns';

export class CalendarMyFakeDb
{

    public static data = [
        {
            id  : 'events',
            data: {
                weekStart: '2018-06-03T00:01:00.0000Z',
                weekEndDate: '2018-06-09T00:01:00.0000Z',
                placements:    [
                    {
                    id: 10,
                    position: 2,
                    coupon: {
                        title: 'Save up 90%',
                        id: '10',
                        description: 'This is my coupons description',
                    },
                    merchant: {
                        name: 'Dell',
                        id: '1'
                    },
                    date    : '2018-06-03T00:01:00.0000Z',
                    pageId  : 1,
                    pageType: 'page' 
                },
                {
                    id: 5,
                    position: 1,
                    coupon: {
                        title: 'Save up 70%',
                        id: '5',
                        description: 'This is my coupons description',
                    },
                    merchant: {
                        name: 'Samsung',
                        id: '5'
                    },
                    date    : '2018-06-03T00:01:00.0000Z',
                    pageId  : 1,
                    pageType: 'page' 
                },



                // {
                //     id: 10,
                //     start    : new Date('2018-06-02T19:01:00.0000Z'),
                //     end      : new Date('2018-06-02T19:30:00.0000Z'),
                //     title    : '01 - 10 <br> Dell',
                //     color    : {
                //         primary  : '#e3bc08',
                //         secondary: '#FDF1BA'
                //     },
                    
                //     draggable: true,
                //     coupon: {
                //         title: 'Save up 90%',
                //         id: '10',
                //         description: 'This is my coupons description',
                //     },
                //     merchant: {
                //         name: 'Dell',
                //         id: '1'
                //     }
                // },
                // {
                //     id: 25,
                //     start    : new Date('2018-06-02T19:02:00.0000Z'),
                //     end      : new Date('2018-06-02T19:30:00.0000Z'),
                //     title    : '02 - 25 <br> Samsung',
                //     draggable: true,
                //     merchant: {
                //         name: 'Samsung',
                //         id: '25'
                //     },
                //     coupon: {
                //         title: 'Save up 80%',
                //         id: '11'
                //     }
                // },


                // {
                //     id: 30,
                //     start    : new Date('2018-06-02T19:03:00.0000Z'),
                //     end      : new Date('2018-06-02T19:30:00.0000Z'),
                //     title    : '03 - 30 <br> Lenovo',
                //     color    : {
                //         primary  : '#e3bc08',
                //         secondary: '#FDF1BA'
                //     },
                    
                //     draggable: true,
                //     merchant: {
                //         name: 'Lenovo',
                //         id: '30'
                //     },
                //     coupon: {
                //         title: 'Save up 80%',
                //         id: '11'
                //     }
                // },
                // {
                //     id: 40,
                //     start    : new Date('2018-06-02T19:04:00.0000Z'),
                //     end      : new Date('2018-06-02T19:30:00.0000Z'),
                //     title    : '04 - 40 <br> Samsung',
                //     draggable: true,
                //     merchant: {
                //         name: 'Samsung',
                //         id: '40'
                //     },
                //     coupon: {
                //         title: 'Save up 80%',
                //         id: '11'
                //     }
                // },
                // {
                //     id: 50,
                //     start    : new Date('2018-06-02T19:05:00.0000Z'),
                //     end      : new Date('2018-06-02T19:30:00.0000Z'),
                //     title    : '05 - 50 <br> Dell',
                //     color    : {
                //         primary  : '#e3bc08',
                //         secondary: '#FDF1BA'
                //     },
                    
                //     draggable: true,
                //     merchant: {
                //         name: 'Dell',
                //         id: '50'
                //     },
                //     coupon: {
                //         title: 'Save up 80%',
                //         id: '11'
                //     }
                // },
                // {
                //     id: 60,
                //     start    : new Date('2018-06-02T19:06:00.0000Z'),
                //     end      : new Date('2018-06-02T19:30:00.0000Z'),
                //     title    : '06 - 60 <br> Lenovo',
                //     draggable: true,
                //     merchant: {
                //         name: 'Lenovo',
                //         id: '60'
                //     },
                //     coupon: {
                //         title: 'Save up 80%',
                //         id: '11'
                //     }
                // },
                // {
                //     id: 70,
                //     start    : new Date('2018-06-02T19:07:00.0000Z'),
                //     end      : new Date('2018-06-02T19:30:00.0000Z'),
                //     title    : '07 - 70 <br> Lenovo',
                //     color    : {
                //         primary  : '#e3bc08',
                //         secondary: '#FDF1BA'
                //     },
                    
                //     draggable: true,
                //     merchant: {
                //         name: 'Lenovo',
                //         id: '70'
                //     },
                //     coupon: {
                //         title: 'Save up 80%',
                //         id: '11'
                //     }
                // },
                // {
                //     id: 80,
                //     start    : new Date('2018-06-02T19:08:00.0000Z'),
                //     end      : new Date('2018-06-02T19:30:00.0000Z'),
                //     title    : '08 - 80 <br> Dell',
                //     draggable: true,
                //     merchant: {
                //         name: 'Dell',
                //         id: '80'
                //     },
                //     coupon: {
                //         title: 'Save up 80%',
                //         id: '11'
                //     }
                // },
                // {
                //     id: 90,
                //     start    : new Date('2018-06-02T19:09:00.0000Z'),
                //     end      : new Date('2018-06-02T19:30:00.0000Z'),
                //     title    : '09 - 90 <br> Samsung',
                //     color    : {
                //         primary  : '#e3bc08',
                //         secondary: '#FDF1BA'
                //     },
                    
                //     draggable: true,
                //     merchant: {
                //         name: 'Samsung',
                //         id: '90'
                //     },
                //     coupon: {
                //         title: 'Save up 80%',
                //         id: '11'
                //     }
                // },
                // {
                //     id: 100,
                //     start    : new Date('2018-06-02T19:10:00.0000Z'),
                //     end      : new Date('2018-06-02T19:30:00.0000Z'),
                //     title    : '10 - 100 <br> Lenovo',
                //     draggable: true,
                //     merchant: {
                //         name: 'Lenovo',
                //         id: '100'
                //     },
                //     coupon: {
                //         title: 'Save up 80%',
                //         id: '11'
                //     }
                // },
                // {
                //     id: 110,
                //     start    : new Date('2018-06-02T19:11:00.0000Z'),
                //     end      : new Date('2018-06-02T19:30:00.0000Z'),
                //     title    : '11 - 110 <br> Apple',
                //     color    : {
                //         primary  : '#e3bc08',
                //         secondary: '#FDF1BA'
                //     },
                //     draggable: true,
                //     merchant: {
                //         name: 'Apple',
                //         id: '110'
                //     },
                //     coupon: {
                //         title: 'Save up 80%',
                //         id: '11'
                //     }
                // },
                // {
                //     id: 120,
                //     start    : new Date('2018-06-02T19:12:00.0000Z'),
                //     end      : new Date('2018-06-02T19:30:00.0000Z'),
                //     title    : '12 - 120 <br> Apple',
                //     draggable: true,
                //     merchant: {
                //         name: 'Apple',
                //         id: '120'
                //     },
                //     coupon: {
                //         title: 'Save up 80%',
                //         id: '11'
                //     }
                // },
                // {
                //     id: 130,
                //     start    : new Date('2018-06-02T19:13:00.0000Z'),
                //     end      : new Date('2018-06-02T19:30:00.0000Z'),
                //     title    : '13 - 130 <br> Dell',
                //     color    : {
                //         primary  : '#e3bc08',
                //         secondary: '#FDF1BA'
                //     },
                //     draggable: true,
                //     merchant: {
                //         name: 'Dell',
                //         id: '130'
                //     },
                //     coupon: {
                //         title: 'Save up 80%',
                //         id: '11'
                //     }
                // },
                // {
                //     id: 140,
                //     start    : new Date('2018-06-02T19:14:00.0000Z'),
                //     end      : new Date('2018-06-02T19:30:00.0000Z'),
                //     title    : '14 - 140 <br> Dell',
                //     draggable: true,
                //     merchant: {
                //         name: 'Dell',
                //         id: '140'
                //     },
                //     coupon: {
                //         title: 'Save up 80%',
                //         id: '11'
                //     }
                // },
                // {
                //     id: 150,
                //     start    : new Date('2018-06-02T19:15:00.0000Z'),
                //     end      : new Date('2018-06-02T19:30:00.0000Z'),
                //     title    : '15 - 150 <br> Samsung',
                //     color    : {
                //         primary  : '#e3bc08',
                //         secondary: '#FDF1BA'
                //     },
                //     draggable: true,
                //     merchant: {
                //         name: 'Samsung',
                //         id: '150'
                //     },
                //     coupon: {
                //         title: 'Save up 80%',
                //         id: '11'
                //     }
                // },




                // {
                //     start    : new Date('2018-06-03T19:01:00.0000Z'),
                //     end      : new Date('2018-06-03T19:30:00.0000Z'),
                //     title    : '1 - ',
                //     allDay   : false,
                //     color    : {
                //         primary  : '#e3bc08',
                //         secondary: '#FDF1BA'
                //     },
                //     resizable: {
                //         beforeStart: true,
                //         afterEnd   : true
                //     },
                //     draggable: true,
                //     meta     : {
                //         location: 'Los Angeles',
                //         notes   : 'Eos eu verear adipiscing, ex ornatus denique iracundia sed, quodsi oportere appellantur an pri.'
                //     }
                // },
                // {
                //     start    : new Date('2018-06-03T19:02:00.0000Z'),
                //     end      : new Date('2018-06-03T19:30:00.0000Z'),
                //     title    : '2 - ',
                //     allDay   : false,
                //     color    : {
                //         primary  : '#1e90ff',
                //         secondary: '#D1E8FF'
                //     },
                //     resizable: {
                //         beforeStart: true,
                //         afterEnd   : true
                //     },
                //     draggable: true,
                //     meta     : {
                //         location: 'Los Angeles',
                //         notes   : 'Eos eu verear adipiscing, ex ornatus denique iracundia sed, quodsi oportere appellantur an pri.'
                //     }
                // },
                // {
                //     start    : new Date('2018-06-03T19:03:00.0000Z'),
                //     end      : new Date('2018-06-03T19:30:00.0000Z'),
                //     title    : '3 - ',
                //     allDay   : false,
                //     color    : {
                //         primary  : '#e3bc08',
                //         secondary: '#FDF1BA'
                //     },
                //     resizable: {
                //         beforeStart: true,
                //         afterEnd   : true
                //     },
                //     draggable: true,
                //     meta     : {
                //         location: 'Los Angeles',
                //         notes   : 'Eos eu verear adipiscing, ex ornatus denique iracundia sed, quodsi oportere appellantur an pri.'
                //     }
                // },
                // {
                //     start    : new Date('2018-06-03T19:04:00.0000Z'),
                //     end      : new Date('2018-06-03T19:30:00.0000Z'),
                //     title    : '4 - ',
                //     allDay   : false,
                //     color    : {
                //         primary  : '#1e90ff',
                //         secondary: '#D1E8FF'
                //     },
                //     resizable: {
                //         beforeStart: true,
                //         afterEnd   : true
                //     },
                //     draggable: true,
                //     meta     : {
                //         location: 'Los Angeles',
                //         notes   : 'Eos eu verear adipiscing, ex ornatus denique iracundia sed, quodsi oportere appellantur an pri.'
                //     }
                // },
                // {
                //     start    : new Date('2018-06-03T19:05:00.0000Z'),
                //     end      : new Date('2018-06-03T19:30:00.0000Z'),
                //     title    : '5 - ',
                //     allDay   : false,
                //     color    : {
                //         primary  : '#e3bc08',
                //         secondary: '#FDF1BA'
                //     },
                //     resizable: {
                //         beforeStart: true,
                //         afterEnd   : true
                //     },
                //     draggable: true,
                //     meta     : {
                //         location: 'Los Angeles',
                //         notes   : 'Eos eu verear adipiscing, ex ornatus denique iracundia sed, quodsi oportere appellantur an pri.'
                //     }
                // },
                // {
                //     start    : new Date('2018-06-03T19:06:00.0000Z'),
                //     end      : new Date('2018-06-03T19:30:00.0000Z'),
                //     title    : '6 - ',
                //     allDay   : false,
                //     color    : {
                //         primary  : '#1e90ff',
                //         secondary: '#D1E8FF'
                //     },
                //     resizable: {
                //         beforeStart: true,
                //         afterEnd   : true
                //     },
                //     draggable: true,
                //     meta     : {
                //         location: 'Los Angeles',
                //         notes   : 'Eos eu verear adipiscing, ex ornatus denique iracundia sed, quodsi oportere appellantur an pri.'
                //     }
                // },
                // {
                //     start    : new Date('2018-06-03T19:07:00.0000Z'),
                //     end      : new Date('2018-06-03T19:30:00.0000Z'),
                //     title    : '7 - ',
                //     allDay   : false,
                //     color    : {
                //         primary  : '#e3bc08',
                //         secondary: '#FDF1BA'
                //     },
                //     resizable: {
                //         beforeStart: true,
                //         afterEnd   : true
                //     },
                //     draggable: true,
                //     meta     : {
                //         location: 'Los Angeles',
                //         notes   : 'Eos eu verear adipiscing, ex ornatus denique iracundia sed, quodsi oportere appellantur an pri.'
                //     }
                // },
                // {
                //     start    : new Date('2018-06-03T19:08:00.0000Z'),
                //     end      : new Date('2018-06-03T19:30:00.0000Z'),
                //     title    : '8 - ',
                //     allDay   : false,
                //     color    : {
                //         primary  : '#1e90ff',
                //         secondary: '#D1E8FF'
                //     },
                //     resizable: {
                //         beforeStart: true,
                //         afterEnd   : true
                //     },
                //     draggable: true,
                //     meta     : {
                //         location: 'Los Angeles',
                //         notes   : 'Eos eu verear adipiscing, ex ornatus denique iracundia sed, quodsi oportere appellantur an pri.'
                //     }
                // },
                // {
                //     start    : new Date('2018-06-03T19:09:00.0000Z'),
                //     end      : new Date('2018-06-03T19:30:00.0000Z'),
                //     title    : '9 - ',
                //     allDay   : false,
                //     color    : {
                //         primary  : '#e3bc08',
                //         secondary: '#FDF1BA'
                //     },
                //     resizable: {
                //         beforeStart: true,
                //         afterEnd   : true
                //     },
                //     draggable: true,
                //     meta     : {
                //         location: 'Los Angeles',
                //         notes   : 'Eos eu verear adipiscing, ex ornatus denique iracundia sed, quodsi oportere appellantur an pri.'
                //     }
                // },
                // {
                //     start    : new Date('2018-06-03T19:10:00.0000Z'),
                //     end      : new Date('2018-06-03T19:30:00.0000Z'),
                //     title    : '10 - ',
                //     allDay   : false,
                //     color    : {
                //         primary  : '#1e90ff',
                //         secondary: '#D1E8FF'
                //     },
                //     resizable: {
                //         beforeStart: true,
                //         afterEnd   : true
                //     },
                //     draggable: true,
                //     meta     : {
                //         location: 'Los Angeles',
                //         notes   : 'Eos eu verear adipiscing, ex ornatus denique iracundia sed, quodsi oportere appellantur an pri.'
                //     }
                // },
                // {
                //     start    : new Date('2018-06-03T19:11:00.0000Z'),
                //     end      : new Date('2018-06-03T19:30:00.0000Z'),
                //     title    : '11 - ',
                //     allDay   : false,
                //     color    : {
                //         primary  : '#e3bc08',
                //         secondary: '#FDF1BA'
                //     },
                //     resizable: {
                //         beforeStart: true,
                //         afterEnd   : true
                //     },
                //     draggable: true,
                //     meta     : {
                //         location: 'Los Angeles',
                //         notes   : 'Eos eu verear adipiscing, ex ornatus denique iracundia sed, quodsi oportere appellantur an pri.'
                //     }
                // },
                // {
                //     start    : new Date('2018-06-03T19:12:00.0000Z'),
                //     end      : new Date('2018-06-03T19:30:00.0000Z'),
                //     title    : '12 - ',
                //     allDay   : false,
                //     color    : {
                //         primary  : '#1e90ff',
                //         secondary: '#D1E8FF'
                //     },
                //     resizable: {
                //         beforeStart: true,
                //         afterEnd   : true
                //     },
                //     draggable: true,
                //     meta     : {
                //         location: 'Los Angeles',
                //         notes   : 'Eos eu verear adipiscing, ex ornatus denique iracundia sed, quodsi oportere appellantur an pri.'
                //     }
                // },
                // {
                //     start    : new Date('2018-06-03T19:13:00.0000Z'),
                //     end      : new Date('2018-06-03T19:30:00.0000Z'),
                //     title    : '13 - ',
                //     allDay   : false,
                //     color    : {
                //         primary  : '#e3bc08',
                //         secondary: '#FDF1BA'
                //     },
                //     resizable: {
                //         beforeStart: true,
                //         afterEnd   : true
                //     },
                //     draggable: true,
                //     meta     : {
                //         location: 'Los Angeles',
                //         notes   : 'Eos eu verear adipiscing, ex ornatus denique iracundia sed, quodsi oportere appellantur an pri.'
                //     }
                // },
                // {
                //     start    : new Date('2018-06-03T19:14:00.0000Z'),
                //     end      : new Date('2018-06-03T19:30:00.0000Z'),
                //     title    : '14 - ',
                //     allDay   : false,
                //     color    : {
                //         primary  : '#1e90ff',
                //         secondary: '#D1E8FF'
                //     },
                //     resizable: {
                //         beforeStart: true,
                //         afterEnd   : true
                //     },
                //     draggable: true,
                //     meta     : {
                //         location: 'Los Angeles',
                //         notes   : 'Eos eu verear adipiscing, ex ornatus denique iracundia sed, quodsi oportere appellantur an pri.'
                //     }
                // },
                // {
                //     start    : new Date('2018-06-03T19:15:00.0000Z'),
                //     end      : new Date('2018-06-03T19:30:00.0000Z'),
                //     title    : '15 - ',
                //     allDay   : false,
                //     color    : {
                //         primary  : '#e3bc08',
                //         secondary: '#FDF1BA'
                //     },
                //     resizable: {
                //         beforeStart: true,
                //         afterEnd   : true
                //     },
                //     draggable: true,
                //     meta     : {
                //         location: 'Los Angeles',
                //         notes   : 'Eos eu verear adipiscing, ex ornatus denique iracundia sed, quodsi oportere appellantur an pri.'
                //     }
                // },






                // {
                //     start    : new Date('2018-06-04T19:01:00.0000Z'),
                //     end      : new Date('2018-06-04T19:30:00.0000Z'),
                //     title    : '1 - ',
                //     allDay   : false,
                //     color    : {
                //         primary  : '#e3bc08',
                //         secondary: '#FDF1BA'
                //     },
                //     resizable: {
                //         beforeStart: true,
                //         afterEnd   : true
                //     },
                //     draggable: true,
                //     meta     : {
                //         location: 'Los Angeles',
                //         notes   : 'Eos eu verear adipiscing, ex ornatus denique iracundia sed, quodsi oportere appellantur an pri.'
                //     }
                // },
                // {
                //     start    : new Date('2018-06-04T19:02:00.0000Z'),
                //     end      : new Date('2018-06-04T19:30:00.0000Z'),
                //     title    : '2 - ',
                //     allDay   : false,
                //     color    : {
                //         primary  : '#1e90ff',
                //         secondary: '#D1E8FF'
                //     },
                //     resizable: {
                //         beforeStart: true,
                //         afterEnd   : true
                //     },
                //     draggable: true,
                //     meta     : {
                //         location: 'Los Angeles',
                //         notes   : 'Eos eu verear adipiscing, ex ornatus denique iracundia sed, quodsi oportere appellantur an pri.'
                //     }
                // },
                // {
                //     start    : new Date('2018-06-04T19:03:00.0000Z'),
                //     end      : new Date('2018-06-04T19:30:00.0000Z'),
                //     title    : '3 - ',
                //     allDay   : false,
                //     color    : {
                //         primary  : '#e3bc08',
                //         secondary: '#FDF1BA'
                //     },
                //     resizable: {
                //         beforeStart: true,
                //         afterEnd   : true
                //     },
                //     draggable: true,
                //     meta     : {
                //         location: 'Los Angeles',
                //         notes   : 'Eos eu verear adipiscing, ex ornatus denique iracundia sed, quodsi oportere appellantur an pri.'
                //     }
                // },
                // {
                //     start    : new Date('2018-06-04T19:04:00.0000Z'),
                //     end      : new Date('2018-06-04T19:30:00.0000Z'),
                //     title    : '4 - ',
                //     allDay   : false,
                //     color    : {
                //         primary  : '#1e90ff',
                //         secondary: '#D1E8FF'
                //     },
                //     resizable: {
                //         beforeStart: true,
                //         afterEnd   : true
                //     },
                //     draggable: true,
                //     meta     : {
                //         location: 'Los Angeles',
                //         notes   : 'Eos eu verear adipiscing, ex ornatus denique iracundia sed, quodsi oportere appellantur an pri.'
                //     }
                // },
                // {
                //     start    : new Date('2018-06-04T19:05:00.0000Z'),
                //     end      : new Date('2018-06-04T19:30:00.0000Z'),
                //     title    : '5 - ',
                //     allDay   : false,
                //     color    : {
                //         primary  : '#e3bc08',
                //         secondary: '#FDF1BA'
                //     },
                //     resizable: {
                //         beforeStart: true,
                //         afterEnd   : true
                //     },
                //     draggable: true,
                //     meta     : {
                //         location: 'Los Angeles',
                //         notes   : 'Eos eu verear adipiscing, ex ornatus denique iracundia sed, quodsi oportere appellantur an pri.'
                //     }
                // },
                // {
                //     start    : new Date('2018-06-04T19:06:00.0000Z'),
                //     end      : new Date('2018-06-04T19:30:00.0000Z'),
                //     title    : '6 - ',
                //     allDay   : false,
                //     color    : {
                //         primary  : '#1e90ff',
                //         secondary: '#D1E8FF'
                //     },
                //     resizable: {
                //         beforeStart: true,
                //         afterEnd   : true
                //     },
                //     draggable: true,
                //     meta     : {
                //         location: 'Los Angeles',
                //         notes   : 'Eos eu verear adipiscing, ex ornatus denique iracundia sed, quodsi oportere appellantur an pri.'
                //     }
                // },
                // {
                //     start    : new Date('2018-06-04T19:07:00.0000Z'),
                //     end      : new Date('2018-06-04T19:30:00.0000Z'),
                //     title    : '7 - ',
                //     allDay   : false,
                //     color    : {
                //         primary  : '#e3bc08',
                //         secondary: '#FDF1BA'
                //     },
                //     resizable: {
                //         beforeStart: true,
                //         afterEnd   : true
                //     },
                //     draggable: true,
                //     meta     : {
                //         location: 'Los Angeles',
                //         notes   : 'Eos eu verear adipiscing, ex ornatus denique iracundia sed, quodsi oportere appellantur an pri.'
                //     }
                // },
                // {
                //     start    : new Date('2018-06-04T19:08:00.0000Z'),
                //     end      : new Date('2018-06-04T19:30:00.0000Z'),
                //     title    : '8 - ',
                //     allDay   : false,
                //     color    : {
                //         primary  : '#1e90ff',
                //         secondary: '#D1E8FF'
                //     },
                //     resizable: {
                //         beforeStart: true,
                //         afterEnd   : true
                //     },
                //     draggable: true,
                //     meta     : {
                //         location: 'Los Angeles',
                //         notes   : 'Eos eu verear adipiscing, ex ornatus denique iracundia sed, quodsi oportere appellantur an pri.'
                //     }
                // },
                // {
                //     start    : new Date('2018-06-04T19:09:00.0000Z'),
                //     end      : new Date('2018-06-04T19:30:00.0000Z'),
                //     title    : '9 - ',
                //     allDay   : false,
                //     color    : {
                //         primary  : '#e3bc08',
                //         secondary: '#FDF1BA'
                //     },
                //     resizable: {
                //         beforeStart: true,
                //         afterEnd   : true
                //     },
                //     draggable: true,
                //     meta     : {
                //         location: 'Los Angeles',
                //         notes   : 'Eos eu verear adipiscing, ex ornatus denique iracundia sed, quodsi oportere appellantur an pri.'
                //     }
                // },
                // {
                //     start    : new Date('2018-06-04T19:10:00.0000Z'),
                //     end      : new Date('2018-06-04T19:30:00.0000Z'),
                //     title    : '10 - ',
                //     allDay   : false,
                //     color    : {
                //         primary  : '#1e90ff',
                //         secondary: '#D1E8FF'
                //     },
                //     resizable: {
                //         beforeStart: true,
                //         afterEnd   : true
                //     },
                //     draggable: true,
                //     meta     : {
                //         location: 'Los Angeles',
                //         notes   : 'Eos eu verear adipiscing, ex ornatus denique iracundia sed, quodsi oportere appellantur an pri.'
                //     }
                // },
                // {
                //     start    : new Date('2018-06-04T19:11:00.0000Z'),
                //     end      : new Date('2018-06-04T19:30:00.0000Z'),
                //     title    : '11 - ',
                //     allDay   : false,
                //     color    : {
                //         primary  : '#e3bc08',
                //         secondary: '#FDF1BA'
                //     },
                //     resizable: {
                //         beforeStart: true,
                //         afterEnd   : true
                //     },
                //     draggable: true,
                //     meta     : {
                //         location: 'Los Angeles',
                //         notes   : 'Eos eu verear adipiscing, ex ornatus denique iracundia sed, quodsi oportere appellantur an pri.'
                //     }
                // },
                // {
                //     start    : new Date('2018-06-04T19:12:00.0000Z'),
                //     end      : new Date('2018-06-04T19:30:00.0000Z'),
                //     title    : '12 - ',
                //     allDay   : false,
                //     color    : {
                //         primary  : '#1e90ff',
                //         secondary: '#D1E8FF'
                //     },
                //     resizable: {
                //         beforeStart: true,
                //         afterEnd   : true
                //     },
                //     draggable: true,
                //     meta     : {
                //         location: 'Los Angeles',
                //         notes   : 'Eos eu verear adipiscing, ex ornatus denique iracundia sed, quodsi oportere appellantur an pri.'
                //     }
                // },
                // {
                //     start    : new Date('2018-06-04T19:13:00.0000Z'),
                //     end      : new Date('2018-06-04T19:30:00.0000Z'),
                //     title    : '13 - ',
                //     allDay   : false,
                //     color    : {
                //         primary  : '#e3bc08',
                //         secondary: '#FDF1BA'
                //     },
                //     resizable: {
                //         beforeStart: true,
                //         afterEnd   : true
                //     },
                //     draggable: true,
                //     meta     : {
                //         location: 'Los Angeles',
                //         notes   : 'Eos eu verear adipiscing, ex ornatus denique iracundia sed, quodsi oportere appellantur an pri.'
                //     }
                // },
                // {
                //     start    : new Date('2018-06-04T19:14:00.0000Z'),
                //     end      : new Date('2018-06-04T19:30:00.0000Z'),
                //     title    : '14 - ',
                //     allDay   : false,
                //     color    : {
                //         primary  : '#1e90ff',
                //         secondary: '#D1E8FF'
                //     },
                //     resizable: {
                //         beforeStart: true,
                //         afterEnd   : true
                //     },
                //     draggable: true,
                //     meta     : {
                //         location: 'Los Angeles',
                //         notes   : 'Eos eu verear adipiscing, ex ornatus denique iracundia sed, quodsi oportere appellantur an pri.'
                //     }
                // },
                // {
                //     start    : new Date('2018-06-04T19:15:00.0000Z'),
                //     end      : new Date('2018-06-04T19:30:00.0000Z'),
                //     title    : '15 - ',
                //     allDay   : false,
                //     color    : {
                //         primary  : '#e3bc08',
                //         secondary: '#FDF1BA'
                //     },
                //     resizable: {
                //         beforeStart: true,
                //         afterEnd   : true
                //     },
                //     draggable: true,
                //     meta     : {
                //         location: 'Los Angeles',
                //         notes   : 'Eos eu verear adipiscing, ex ornatus denique iracundia sed, quodsi oportere appellantur an pri.'
                //     }
                // },







                // {
                //     start    : new Date('2018-06-05T19:01:00.0000Z'),
                //     end      : new Date('2018-06-05T19:30:00.0000Z'),
                //     title    : '1 - ',
                //     allDay   : false,
                //     color    : {
                //         primary  : '#e3bc08',
                //         secondary: '#FDF1BA'
                //     },
                //     resizable: {
                //         beforeStart: true,
                //         afterEnd   : true
                //     },
                //     draggable: true,
                //     meta     : {
                //         location: 'Los Angeles',
                //         notes   : 'Eos eu verear adipiscing, ex ornatus denique iracundia sed, quodsi oportere appellantur an pri.'
                //     }
                // },
                // {
                //     start    : new Date('2018-06-05T19:02:00.0000Z'),
                //     end      : new Date('2018-06-05T19:30:00.0000Z'),
                //     title    : '2 - ',
                //     allDay   : false,
                //     color    : {
                //         primary  : '#1e90ff',
                //         secondary: '#D1E8FF'
                //     },
                //     resizable: {
                //         beforeStart: true,
                //         afterEnd   : true
                //     },
                //     draggable: true,
                //     meta     : {
                //         location: 'Los Angeles',
                //         notes   : 'Eos eu verear adipiscing, ex ornatus denique iracundia sed, quodsi oportere appellantur an pri.'
                //     }
                // },
                // {
                //     start    : new Date('2018-06-05T19:03:00.0000Z'),
                //     end      : new Date('2018-06-05T19:30:00.0000Z'),
                //     title    : '3 - ',
                //     allDay   : false,
                //     color    : {
                //         primary  : '#e3bc08',
                //         secondary: '#FDF1BA'
                //     },
                //     resizable: {
                //         beforeStart: true,
                //         afterEnd   : true
                //     },
                //     draggable: true,
                //     meta     : {
                //         location: 'Los Angeles',
                //         notes   : 'Eos eu verear adipiscing, ex ornatus denique iracundia sed, quodsi oportere appellantur an pri.'
                //     }
                // },
                // {
                //     start    : new Date('2018-06-05T19:04:00.0000Z'),
                //     end      : new Date('2018-06-05T19:30:00.0000Z'),
                //     title    : '4 - ',
                //     allDay   : false,
                //     color    : {
                //         primary  : '#1e90ff',
                //         secondary: '#D1E8FF'
                //     },
                //     resizable: {
                //         beforeStart: true,
                //         afterEnd   : true
                //     },
                //     draggable: true,
                //     meta     : {
                //         location: 'Los Angeles',
                //         notes   : 'Eos eu verear adipiscing, ex ornatus denique iracundia sed, quodsi oportere appellantur an pri.'
                //     }
                // },
                // {
                //     start    : new Date('2018-06-05T19:05:00.0000Z'),
                //     end      : new Date('2018-06-05T19:30:00.0000Z'),
                //     title    : '5 - ',
                //     allDay   : false,
                //     color    : {
                //         primary  : '#e3bc08',
                //         secondary: '#FDF1BA'
                //     },
                //     resizable: {
                //         beforeStart: true,
                //         afterEnd   : true
                //     },
                //     draggable: true,
                //     meta     : {
                //         location: 'Los Angeles',
                //         notes   : 'Eos eu verear adipiscing, ex ornatus denique iracundia sed, quodsi oportere appellantur an pri.'
                //     }
                // },
                // {
                //     start    : new Date('2018-06-05T19:06:00.0000Z'),
                //     end      : new Date('2018-06-05T19:30:00.0000Z'),
                //     title    : '6 - ',
                //     allDay   : false,
                //     color    : {
                //         primary  : '#1e90ff',
                //         secondary: '#D1E8FF'
                //     },
                //     resizable: {
                //         beforeStart: true,
                //         afterEnd   : true
                //     },
                //     draggable: true,
                //     meta     : {
                //         location: 'Los Angeles',
                //         notes   : 'Eos eu verear adipiscing, ex ornatus denique iracundia sed, quodsi oportere appellantur an pri.'
                //     }
                // },
                // {
                //     start    : new Date('2018-06-05T19:07:00.0000Z'),
                //     end      : new Date('2018-06-05T19:30:00.0000Z'),
                //     title    : '7 - ',
                //     allDay   : false,
                //     color    : {
                //         primary  : '#e3bc08',
                //         secondary: '#FDF1BA'
                //     },
                //     resizable: {
                //         beforeStart: true,
                //         afterEnd   : true
                //     },
                //     draggable: true,
                //     meta     : {
                //         location: 'Los Angeles',
                //         notes   : 'Eos eu verear adipiscing, ex ornatus denique iracundia sed, quodsi oportere appellantur an pri.'
                //     }
                // },
                // {
                //     start    : new Date('2018-06-05T19:08:00.0000Z'),
                //     end      : new Date('2018-06-05T19:30:00.0000Z'),
                //     title    : '8 - ',
                //     allDay   : false,
                //     color    : {
                //         primary  : '#1e90ff',
                //         secondary: '#D1E8FF'
                //     },
                //     resizable: {
                //         beforeStart: true,
                //         afterEnd   : true
                //     },
                //     draggable: true,
                //     meta     : {
                //         location: 'Los Angeles',
                //         notes   : 'Eos eu verear adipiscing, ex ornatus denique iracundia sed, quodsi oportere appellantur an pri.'
                //     }
                // },
                // {
                //     start    : new Date('2018-06-05T19:09:00.0000Z'),
                //     end      : new Date('2018-06-05T19:30:00.0000Z'),
                //     title    : '9 - ',
                //     allDay   : false,
                //     color    : {
                //         primary  : '#e3bc08',
                //         secondary: '#FDF1BA'
                //     },
                //     resizable: {
                //         beforeStart: true,
                //         afterEnd   : true
                //     },
                //     draggable: true,
                //     meta     : {
                //         location: 'Los Angeles',
                //         notes   : 'Eos eu verear adipiscing, ex ornatus denique iracundia sed, quodsi oportere appellantur an pri.'
                //     }
                // },
                // {
                //     start    : new Date('2018-06-05T19:10:00.0000Z'),
                //     end      : new Date('2018-06-05T19:30:00.0000Z'),
                //     title    : '10 - ',
                //     allDay   : false,
                //     color    : {
                //         primary  : '#1e90ff',
                //         secondary: '#D1E8FF'
                //     },
                //     resizable: {
                //         beforeStart: true,
                //         afterEnd   : true
                //     },
                //     draggable: true,
                //     meta     : {
                //         location: 'Los Angeles',
                //         notes   : 'Eos eu verear adipiscing, ex ornatus denique iracundia sed, quodsi oportere appellantur an pri.'
                //     }
                // },
                // {
                //     start    : new Date('2018-06-05T19:11:00.0000Z'),
                //     end      : new Date('2018-06-05T19:30:00.0000Z'),
                //     title    : '11 - ',
                //     allDay   : false,
                //     color    : {
                //         primary  : '#e3bc08',
                //         secondary: '#FDF1BA'
                //     },
                //     resizable: {
                //         beforeStart: true,
                //         afterEnd   : true
                //     },
                //     draggable: true,
                //     meta     : {
                //         location: 'Los Angeles',
                //         notes   : 'Eos eu verear adipiscing, ex ornatus denique iracundia sed, quodsi oportere appellantur an pri.'
                //     }
                // },
                // {
                //     start    : new Date('2018-06-05T19:12:00.0000Z'),
                //     end      : new Date('2018-06-05T19:30:00.0000Z'),
                //     title    : '12 - ',
                //     allDay   : false,
                //     color    : {
                //         primary  : '#1e90ff',
                //         secondary: '#D1E8FF'
                //     },
                //     resizable: {
                //         beforeStart: true,
                //         afterEnd   : true
                //     },
                //     draggable: true,
                //     meta     : {
                //         location: 'Los Angeles',
                //         notes   : 'Eos eu verear adipiscing, ex ornatus denique iracundia sed, quodsi oportere appellantur an pri.'
                //     }
                // },
                // {
                //     start    : new Date('2018-06-05T19:13:00.0000Z'),
                //     end      : new Date('2018-06-05T19:30:00.0000Z'),
                //     title    : '13 - ',
                //     allDay   : false,
                //     color    : {
                //         primary  : '#e3bc08',
                //         secondary: '#FDF1BA'
                //     },
                //     resizable: {
                //         beforeStart: true,
                //         afterEnd   : true
                //     },
                //     draggable: true,
                //     meta     : {
                //         location: 'Los Angeles',
                //         notes   : 'Eos eu verear adipiscing, ex ornatus denique iracundia sed, quodsi oportere appellantur an pri.'
                //     }
                // },
                // {
                //     start    : new Date('2018-06-05T19:14:00.0000Z'),
                //     end      : new Date('2018-06-05T19:30:00.0000Z'),
                //     title    : '14 - ',
                //     allDay   : false,
                //     color    : {
                //         primary  : '#1e90ff',
                //         secondary: '#D1E8FF'
                //     },
                //     resizable: {
                //         beforeStart: true,
                //         afterEnd   : true
                //     },
                //     draggable: true,
                //     meta     : {
                //         location: 'Los Angeles',
                //         notes   : 'Eos eu verear adipiscing, ex ornatus denique iracundia sed, quodsi oportere appellantur an pri.'
                //     }
                // },
                // {
                //     start    : new Date('2018-06-05T19:15:00.0000Z'),
                //     end      : new Date('2018-06-05T19:30:00.0000Z'),
                //     title    : '15 - ',
                //     allDay   : false,
                //     color    : {
                //         primary  : '#e3bc08',
                //         secondary: '#FDF1BA'
                //     },
                //     resizable: {
                //         beforeStart: true,
                //         afterEnd   : true
                //     },
                //     draggable: true,
                //     meta     : {
                //         location: 'Los Angeles',
                //         notes   : 'Eos eu verear adipiscing, ex ornatus denique iracundia sed, quodsi oportere appellantur an pri.'
                //     }
                // },







                // {
                //     start    : new Date('2018-06-06T19:01:00.0000Z'),
                //     end      : new Date('2018-06-06T19:30:00.0000Z'),
                //     title    : '1 - ',
                //     allDay   : false,
                //     color    : {
                //         primary  : '#e3bc08',
                //         secondary: '#FDF1BA'
                //     },
                //     resizable: {
                //         beforeStart: true,
                //         afterEnd   : true
                //     },
                //     draggable: true,
                //     meta     : {
                //         location: 'Los Angeles',
                //         notes   : 'Eos eu verear adipiscing, ex ornatus denique iracundia sed, quodsi oportere appellantur an pri.'
                //     }
                // },
                // {
                //     start    : new Date('2018-06-06T19:02:00.0000Z'),
                //     end      : new Date('2018-06-06T19:30:00.0000Z'),
                //     title    : '2 - ',
                //     allDay   : false,
                //     color    : {
                //         primary  : '#1e90ff',
                //         secondary: '#D1E8FF'
                //     },
                //     resizable: {
                //         beforeStart: true,
                //         afterEnd   : true
                //     },
                //     draggable: true,
                //     meta     : {
                //         location: 'Los Angeles',
                //         notes   : 'Eos eu verear adipiscing, ex ornatus denique iracundia sed, quodsi oportere appellantur an pri.'
                //     }
                // },
                // {
                //     start    : new Date('2018-06-06T19:03:00.0000Z'),
                //     end      : new Date('2018-06-06T19:30:00.0000Z'),
                //     title    : '3 - ',
                //     allDay   : false,
                //     color    : {
                //         primary  : '#e3bc08',
                //         secondary: '#FDF1BA'
                //     },
                //     resizable: {
                //         beforeStart: true,
                //         afterEnd   : true
                //     },
                //     draggable: true,
                //     meta     : {
                //         location: 'Los Angeles',
                //         notes   : 'Eos eu verear adipiscing, ex ornatus denique iracundia sed, quodsi oportere appellantur an pri.'
                //     }
                // },
                // {
                //     start    : new Date('2018-06-06T19:04:00.0000Z'),
                //     end      : new Date('2018-06-06T19:30:00.0000Z'),
                //     title    : '4 - ',
                //     allDay   : false,
                //     color    : {
                //         primary  : '#1e90ff',
                //         secondary: '#D1E8FF'
                //     },
                //     resizable: {
                //         beforeStart: true,
                //         afterEnd   : true
                //     },
                //     draggable: true,
                //     meta     : {
                //         location: 'Los Angeles',
                //         notes   : 'Eos eu verear adipiscing, ex ornatus denique iracundia sed, quodsi oportere appellantur an pri.'
                //     }
                // },
                // {
                //     start    : new Date('2018-06-06T19:05:00.0000Z'),
                //     end      : new Date('2018-06-06T19:30:00.0000Z'),
                //     title    : '5 - ',
                //     allDay   : false,
                //     color    : {
                //         primary  : '#e3bc08',
                //         secondary: '#FDF1BA'
                //     },
                //     resizable: {
                //         beforeStart: true,
                //         afterEnd   : true
                //     },
                //     draggable: true,
                //     meta     : {
                //         location: 'Los Angeles',
                //         notes   : 'Eos eu verear adipiscing, ex ornatus denique iracundia sed, quodsi oportere appellantur an pri.'
                //     }
                // },
                // {
                //     start    : new Date('2018-06-06T19:06:00.0000Z'),
                //     end      : new Date('2018-06-06T19:30:00.0000Z'),
                //     title    : '6 - ',
                //     allDay   : false,
                //     color    : {
                //         primary  : '#1e90ff',
                //         secondary: '#D1E8FF'
                //     },
                //     resizable: {
                //         beforeStart: true,
                //         afterEnd   : true
                //     },
                //     draggable: true,
                //     meta     : {
                //         location: 'Los Angeles',
                //         notes   : 'Eos eu verear adipiscing, ex ornatus denique iracundia sed, quodsi oportere appellantur an pri.'
                //     }
                // },
                // {
                //     start    : new Date('2018-06-06T19:07:00.0000Z'),
                //     end      : new Date('2018-06-06T19:30:00.0000Z'),
                //     title    : '7 - ',
                //     allDay   : false,
                //     color    : {
                //         primary  : '#e3bc08',
                //         secondary: '#FDF1BA'
                //     },
                //     resizable: {
                //         beforeStart: true,
                //         afterEnd   : true
                //     },
                //     draggable: true,
                //     meta     : {
                //         location: 'Los Angeles',
                //         notes   : 'Eos eu verear adipiscing, ex ornatus denique iracundia sed, quodsi oportere appellantur an pri.'
                //     }
                // },
                // {
                //     start    : new Date('2018-06-06T19:08:00.0000Z'),
                //     end      : new Date('2018-06-06T19:30:00.0000Z'),
                //     title    : '8 - ',
                //     allDay   : false,
                //     color    : {
                //         primary  : '#1e90ff',
                //         secondary: '#D1E8FF'
                //     },
                //     resizable: {
                //         beforeStart: true,
                //         afterEnd   : true
                //     },
                //     draggable: true,
                //     meta     : {
                //         location: 'Los Angeles',
                //         notes   : 'Eos eu verear adipiscing, ex ornatus denique iracundia sed, quodsi oportere appellantur an pri.'
                //     }
                // },
                // {
                //     start    : new Date('2018-06-06T19:09:00.0000Z'),
                //     end      : new Date('2018-06-06T19:30:00.0000Z'),
                //     title    : '9 - ',
                //     allDay   : false,
                //     color    : {
                //         primary  : '#e3bc08',
                //         secondary: '#FDF1BA'
                //     },
                //     resizable: {
                //         beforeStart: true,
                //         afterEnd   : true
                //     },
                //     draggable: true,
                //     meta     : {
                //         location: 'Los Angeles',
                //         notes   : 'Eos eu verear adipiscing, ex ornatus denique iracundia sed, quodsi oportere appellantur an pri.'
                //     }
                // },
                // {
                //     start    : new Date('2018-06-06T19:10:00.0000Z'),
                //     end      : new Date('2018-06-06T19:30:00.0000Z'),
                //     title    : '10 - ',
                //     allDay   : false,
                //     color    : {
                //         primary  : '#1e90ff',
                //         secondary: '#D1E8FF'
                //     },
                //     resizable: {
                //         beforeStart: true,
                //         afterEnd   : true
                //     },
                //     draggable: true,
                //     meta     : {
                //         location: 'Los Angeles',
                //         notes   : 'Eos eu verear adipiscing, ex ornatus denique iracundia sed, quodsi oportere appellantur an pri.'
                //     }
                // },
                // {
                //     start    : new Date('2018-06-06T19:11:00.0000Z'),
                //     end      : new Date('2018-06-06T19:30:00.0000Z'),
                //     title    : '11 - ',
                //     allDay   : false,
                //     color    : {
                //         primary  : '#e3bc08',
                //         secondary: '#FDF1BA'
                //     },
                //     resizable: {
                //         beforeStart: true,
                //         afterEnd   : true
                //     },
                //     draggable: true,
                //     meta     : {
                //         location: 'Los Angeles',
                //         notes   : 'Eos eu verear adipiscing, ex ornatus denique iracundia sed, quodsi oportere appellantur an pri.'
                //     }
                // },
                // {
                //     start    : new Date('2018-06-06T19:12:00.0000Z'),
                //     end      : new Date('2018-06-06T19:30:00.0000Z'),
                //     title    : '12 - ',
                //     allDay   : false,
                //     color    : {
                //         primary  : '#1e90ff',
                //         secondary: '#D1E8FF'
                //     },
                //     resizable: {
                //         beforeStart: true,
                //         afterEnd   : true
                //     },
                //     draggable: true,
                //     meta     : {
                //         location: 'Los Angeles',
                //         notes   : 'Eos eu verear adipiscing, ex ornatus denique iracundia sed, quodsi oportere appellantur an pri.'
                //     }
                // },
                // {
                //     start    : new Date('2018-06-06T19:13:00.0000Z'),
                //     end      : new Date('2018-06-06T19:30:00.0000Z'),
                //     title    : '13 - ',
                //     allDay   : false,
                //     color    : {
                //         primary  : '#e3bc08',
                //         secondary: '#FDF1BA'
                //     },
                //     resizable: {
                //         beforeStart: true,
                //         afterEnd   : true
                //     },
                //     draggable: true,
                //     meta     : {
                //         location: 'Los Angeles',
                //         notes   : 'Eos eu verear adipiscing, ex ornatus denique iracundia sed, quodsi oportere appellantur an pri.'
                //     }
                // },
                // {
                //     start    : new Date('2018-06-06T19:14:00.0000Z'),
                //     end      : new Date('2018-06-06T19:30:00.0000Z'),
                //     title    : '14 - ',
                //     allDay   : false,
                //     color    : {
                //         primary  : '#1e90ff',
                //         secondary: '#D1E8FF'
                //     },
                //     resizable: {
                //         beforeStart: true,
                //         afterEnd   : true
                //     },
                //     draggable: true,
                //     meta     : {
                //         location: 'Los Angeles',
                //         notes   : 'Eos eu verear adipiscing, ex ornatus denique iracundia sed, quodsi oportere appellantur an pri.'
                //     }
                // },
                // {
                //     start    : new Date('2018-06-06T19:15:00.0000Z'),
                //     end      : new Date('2018-06-06T19:30:00.0000Z'),
                //     title    : '15 - ',
                //     allDay   : false,
                //     color    : {
                //         primary  : '#e3bc08',
                //         secondary: '#FDF1BA'
                //     },
                //     resizable: {
                //         beforeStart: true,
                //         afterEnd   : true
                //     },
                //     draggable: true,
                //     meta     : {
                //         location: 'Los Angeles',
                //         notes   : 'Eos eu verear adipiscing, ex ornatus denique iracundia sed, quodsi oportere appellantur an pri.'
                //     }
                // },







                // {
                //     start    : new Date('2018-06-07T19:01:00.0000Z'),
                //     end      : new Date('2018-06-07T19:30:00.0000Z'),
                //     title    : '1 - ',
                //     allDay   : false,
                //     color    : {
                //         primary  : '#e3bc08',
                //         secondary: '#FDF1BA'
                //     },
                //     resizable: {
                //         beforeStart: true,
                //         afterEnd   : true
                //     },
                //     draggable: true,
                //     meta     : {
                //         location: 'Los Angeles',
                //         notes   : 'Eos eu verear adipiscing, ex ornatus denique iracundia sed, quodsi oportere appellantur an pri.'
                //     }
                // },
                // {
                //     start    : new Date('2018-06-07T19:02:00.0000Z'),
                //     end      : new Date('2018-06-07T19:30:00.0000Z'),
                //     title    : '2 - ',
                //     allDay   : false,
                //     color    : {
                //         primary  : '#1e90ff',
                //         secondary: '#D1E8FF'
                //     },
                //     resizable: {
                //         beforeStart: true,
                //         afterEnd   : true
                //     },
                //     draggable: true,
                //     meta     : {
                //         location: 'Los Angeles',
                //         notes   : 'Eos eu verear adipiscing, ex ornatus denique iracundia sed, quodsi oportere appellantur an pri.'
                //     }
                // },
                // {
                //     start    : new Date('2018-06-07T19:03:00.0000Z'),
                //     end      : new Date('2018-06-07T19:30:00.0000Z'),
                //     title    : '3 - ',
                //     allDay   : false,
                //     color    : {
                //         primary  : '#e3bc08',
                //         secondary: '#FDF1BA'
                //     },
                //     resizable: {
                //         beforeStart: true,
                //         afterEnd   : true
                //     },
                //     draggable: true,
                //     meta     : {
                //         location: 'Los Angeles',
                //         notes   : 'Eos eu verear adipiscing, ex ornatus denique iracundia sed, quodsi oportere appellantur an pri.'
                //     }
                // },
                // {
                //     start    : new Date('2018-06-07T19:04:00.0000Z'),
                //     end      : new Date('2018-06-07T19:30:00.0000Z'),
                //     title    : '4 - ',
                //     allDay   : false,
                //     color    : {
                //         primary  : '#1e90ff',
                //         secondary: '#D1E8FF'
                //     },
                //     resizable: {
                //         beforeStart: true,
                //         afterEnd   : true
                //     },
                //     draggable: true,
                //     meta     : {
                //         location: 'Los Angeles',
                //         notes   : 'Eos eu verear adipiscing, ex ornatus denique iracundia sed, quodsi oportere appellantur an pri.'
                //     }
                // },
                // {
                //     start    : new Date('2018-06-07T19:05:00.0000Z'),
                //     end      : new Date('2018-06-07T19:30:00.0000Z'),
                //     title    : '5 - ',
                //     allDay   : false,
                //     color    : {
                //         primary  : '#e3bc08',
                //         secondary: '#FDF1BA'
                //     },
                //     resizable: {
                //         beforeStart: true,
                //         afterEnd   : true
                //     },
                //     draggable: true,
                //     meta     : {
                //         location: 'Los Angeles',
                //         notes   : 'Eos eu verear adipiscing, ex ornatus denique iracundia sed, quodsi oportere appellantur an pri.'
                //     }
                // },
                // {
                //     start    : new Date('2018-06-07T19:06:00.0000Z'),
                //     end      : new Date('2018-06-07T19:30:00.0000Z'),
                //     title    : '6 - ',
                //     allDay   : false,
                //     color    : {
                //         primary  : '#1e90ff',
                //         secondary: '#D1E8FF'
                //     },
                //     resizable: {
                //         beforeStart: true,
                //         afterEnd   : true
                //     },
                //     draggable: true,
                //     meta     : {
                //         location: 'Los Angeles',
                //         notes   : 'Eos eu verear adipiscing, ex ornatus denique iracundia sed, quodsi oportere appellantur an pri.'
                //     }
                // },
                // {
                //     start    : new Date('2018-06-07T19:07:00.0000Z'),
                //     end      : new Date('2018-06-07T19:30:00.0000Z'),
                //     title    : '7 - ',
                //     allDay   : false,
                //     color    : {
                //         primary  : '#e3bc08',
                //         secondary: '#FDF1BA'
                //     },
                //     resizable: {
                //         beforeStart: true,
                //         afterEnd   : true
                //     },
                //     draggable: true,
                //     meta     : {
                //         location: 'Los Angeles',
                //         notes   : 'Eos eu verear adipiscing, ex ornatus denique iracundia sed, quodsi oportere appellantur an pri.'
                //     }
                // },
                // {
                //     start    : new Date('2018-06-07T19:08:00.0000Z'),
                //     end      : new Date('2018-06-07T19:30:00.0000Z'),
                //     title    : '8 - ',
                //     allDay   : false,
                //     color    : {
                //         primary  : '#1e90ff',
                //         secondary: '#D1E8FF'
                //     },
                //     resizable: {
                //         beforeStart: true,
                //         afterEnd   : true
                //     },
                //     draggable: true,
                //     meta     : {
                //         location: 'Los Angeles',
                //         notes   : 'Eos eu verear adipiscing, ex ornatus denique iracundia sed, quodsi oportere appellantur an pri.'
                //     }
                // },
                // {
                //     start    : new Date('2018-06-07T19:09:00.0000Z'),
                //     end      : new Date('2018-06-07T19:30:00.0000Z'),
                //     title    : '9 - ',
                //     allDay   : false,
                //     color    : {
                //         primary  : '#e3bc08',
                //         secondary: '#FDF1BA'
                //     },
                //     resizable: {
                //         beforeStart: true,
                //         afterEnd   : true
                //     },
                //     draggable: true,
                //     meta     : {
                //         location: 'Los Angeles',
                //         notes   : 'Eos eu verear adipiscing, ex ornatus denique iracundia sed, quodsi oportere appellantur an pri.'
                //     }
                // },
                // {
                //     start    : new Date('2018-06-07T19:10:00.0000Z'),
                //     end      : new Date('2018-06-07T19:30:00.0000Z'),
                //     title    : '10 - ',
                //     allDay   : false,
                //     color    : {
                //         primary  : '#1e90ff',
                //         secondary: '#D1E8FF'
                //     },
                //     resizable: {
                //         beforeStart: true,
                //         afterEnd   : true
                //     },
                //     draggable: true,
                //     meta     : {
                //         location: 'Los Angeles',
                //         notes   : 'Eos eu verear adipiscing, ex ornatus denique iracundia sed, quodsi oportere appellantur an pri.'
                //     }
                // },
                // {
                //     start    : new Date('2018-06-07T19:11:00.0000Z'),
                //     end      : new Date('2018-06-07T19:30:00.0000Z'),
                //     title    : '11 - ',
                //     allDay   : false,
                //     color    : {
                //         primary  : '#e3bc08',
                //         secondary: '#FDF1BA'
                //     },
                //     resizable: {
                //         beforeStart: true,
                //         afterEnd   : true
                //     },
                //     draggable: true,
                //     meta     : {
                //         location: 'Los Angeles',
                //         notes   : 'Eos eu verear adipiscing, ex ornatus denique iracundia sed, quodsi oportere appellantur an pri.'
                //     }
                // },
                // {
                //     start    : new Date('2018-06-07T19:12:00.0000Z'),
                //     end      : new Date('2018-06-07T19:30:00.0000Z'),
                //     title    : '12 - ',
                //     allDay   : false,
                //     color    : {
                //         primary  : '#1e90ff',
                //         secondary: '#D1E8FF'
                //     },
                //     resizable: {
                //         beforeStart: true,
                //         afterEnd   : true
                //     },
                //     draggable: true,
                //     meta     : {
                //         location: 'Los Angeles',
                //         notes   : 'Eos eu verear adipiscing, ex ornatus denique iracundia sed, quodsi oportere appellantur an pri.'
                //     }
                // },
                // {
                //     start    : new Date('2018-06-07T19:13:00.0000Z'),
                //     end      : new Date('2018-06-07T19:30:00.0000Z'),
                //     title    : '13 - ',
                //     allDay   : false,
                //     color    : {
                //         primary  : '#e3bc08',
                //         secondary: '#FDF1BA'
                //     },
                //     resizable: {
                //         beforeStart: true,
                //         afterEnd   : true
                //     },
                //     draggable: true,
                //     meta     : {
                //         location: 'Los Angeles',
                //         notes   : 'Eos eu verear adipiscing, ex ornatus denique iracundia sed, quodsi oportere appellantur an pri.'
                //     }
                // },
                // {
                //     start    : new Date('2018-06-07T19:14:00.0000Z'),
                //     end      : new Date('2018-06-07T19:30:00.0000Z'),
                //     title    : '14 - ',
                //     allDay   : false,
                //     color    : {
                //         primary  : '#1e90ff',
                //         secondary: '#D1E8FF'
                //     },
                //     resizable: {
                //         beforeStart: true,
                //         afterEnd   : true
                //     },
                //     draggable: true,
                //     meta     : {
                //         location: 'Los Angeles',
                //         notes   : 'Eos eu verear adipiscing, ex ornatus denique iracundia sed, quodsi oportere appellantur an pri.'
                //     }
                // },
                // {
                //     start    : new Date('2018-06-07T19:15:00.0000Z'),
                //     end      : new Date('2018-06-07T19:30:00.0000Z'),
                //     title    : '15 - ',
                //     allDay   : false,
                //     color    : {
                //         primary  : '#e3bc08',
                //         secondary: '#FDF1BA'
                //     },
                //     resizable: {
                //         beforeStart: true,
                //         afterEnd   : true
                //     },
                //     draggable: true,
                //     meta     : {
                //         location: 'Los Angeles',
                //         notes   : 'Eos eu verear adipiscing, ex ornatus denique iracundia sed, quodsi oportere appellantur an pri.'
                //     }
                // },






                // {
                //     start    : new Date('2018-06-08T19:01:00.0000Z'),
                //     end      : new Date('2018-06-08T19:30:00.0000Z'),
                //     title    : '1 - ',
                //     allDay   : false,
                //     color    : {
                //         primary  : '#e3bc08',
                //         secondary: '#FDF1BA'
                //     },
                //     resizable: {
                //         beforeStart: true,
                //         afterEnd   : true
                //     },
                //     draggable: true,
                //     meta     : {
                //         location: 'Los Angeles',
                //         notes   : 'Eos eu verear adipiscing, ex ornatus denique iracundia sed, quodsi oportere appellantur an pri.'
                //     }
                // },
                // {
                //     start    : new Date('2018-06-08T19:02:00.0000Z'),
                //     end      : new Date('2018-06-08T19:30:00.0000Z'),
                //     title    : '2 - ',
                //     allDay   : false,
                //     color    : {
                //         primary  : '#1e90ff',
                //         secondary: '#D1E8FF'
                //     },
                //     resizable: {
                //         beforeStart: true,
                //         afterEnd   : true
                //     },
                //     draggable: true,
                //     meta     : {
                //         location: 'Los Angeles',
                //         notes   : 'Eos eu verear adipiscing, ex ornatus denique iracundia sed, quodsi oportere appellantur an pri.'
                //     }
                // },
                // {
                //     start    : new Date('2018-06-08T19:03:00.0000Z'),
                //     end      : new Date('2018-06-08T19:30:00.0000Z'),
                //     title    : '3 - ',
                //     allDay   : false,
                //     color    : {
                //         primary  : '#e3bc08',
                //         secondary: '#FDF1BA'
                //     },
                //     resizable: {
                //         beforeStart: true,
                //         afterEnd   : true
                //     },
                //     draggable: true,
                //     meta     : {
                //         location: 'Los Angeles',
                //         notes   : 'Eos eu verear adipiscing, ex ornatus denique iracundia sed, quodsi oportere appellantur an pri.'
                //     }
                // },
                // {
                //     start    : new Date('2018-06-08T19:04:00.0000Z'),
                //     end      : new Date('2018-06-08T19:30:00.0000Z'),
                //     title    : '4 - ',
                //     allDay   : false,
                //     color    : {
                //         primary  : '#1e90ff',
                //         secondary: '#D1E8FF'
                //     },
                //     resizable: {
                //         beforeStart: true,
                //         afterEnd   : true
                //     },
                //     draggable: true,
                //     meta     : {
                //         location: 'Los Angeles',
                //         notes   : 'Eos eu verear adipiscing, ex ornatus denique iracundia sed, quodsi oportere appellantur an pri.'
                //     }
                // },
                // {
                //     start    : new Date('2018-06-08T19:05:00.0000Z'),
                //     end      : new Date('2018-06-08T19:30:00.0000Z'),
                //     title    : '5 - ',
                //     allDay   : false,
                //     color    : {
                //         primary  : '#e3bc08',
                //         secondary: '#FDF1BA'
                //     },
                //     resizable: {
                //         beforeStart: true,
                //         afterEnd   : true
                //     },
                //     draggable: true,
                //     meta     : {
                //         location: 'Los Angeles',
                //         notes   : 'Eos eu verear adipiscing, ex ornatus denique iracundia sed, quodsi oportere appellantur an pri.'
                //     }
                // },
                // {
                //     start    : new Date('2018-06-08T19:06:00.0000Z'),
                //     end      : new Date('2018-06-08T19:30:00.0000Z'),
                //     title    : '6 - ',
                //     allDay   : false,
                //     color    : {
                //         primary  : '#1e90ff',
                //         secondary: '#D1E8FF'
                //     },
                //     resizable: {
                //         beforeStart: true,
                //         afterEnd   : true
                //     },
                //     draggable: true,
                //     meta     : {
                //         location: 'Los Angeles',
                //         notes   : 'Eos eu verear adipiscing, ex ornatus denique iracundia sed, quodsi oportere appellantur an pri.'
                //     }
                // },
                // {
                //     start    : new Date('2018-06-08T19:07:00.0000Z'),
                //     end      : new Date('2018-06-08T19:30:00.0000Z'),
                //     title    : '7 - ',
                //     allDay   : false,
                //     color    : {
                //         primary  : '#e3bc08',
                //         secondary: '#FDF1BA'
                //     },
                //     resizable: {
                //         beforeStart: true,
                //         afterEnd   : true
                //     },
                //     draggable: true,
                //     meta     : {
                //         location: 'Los Angeles',
                //         notes   : 'Eos eu verear adipiscing, ex ornatus denique iracundia sed, quodsi oportere appellantur an pri.'
                //     }
                // },
                // {
                //     start    : new Date('2018-06-08T19:08:00.0000Z'),
                //     end      : new Date('2018-06-08T19:30:00.0000Z'),
                //     title    : '8 - ',
                //     allDay   : false,
                //     color    : {
                //         primary  : '#1e90ff',
                //         secondary: '#D1E8FF'
                //     },
                //     resizable: {
                //         beforeStart: true,
                //         afterEnd   : true
                //     },
                //     draggable: true,
                //     meta     : {
                //         location: 'Los Angeles',
                //         notes   : 'Eos eu verear adipiscing, ex ornatus denique iracundia sed, quodsi oportere appellantur an pri.'
                //     }
                // },
                // {
                //     start    : new Date('2018-06-08T19:09:00.0000Z'),
                //     end      : new Date('2018-06-08T19:30:00.0000Z'),
                //     title    : '9 - ',
                //     allDay   : false,
                //     color    : {
                //         primary  : '#e3bc08',
                //         secondary: '#FDF1BA'
                //     },
                //     resizable: {
                //         beforeStart: true,
                //         afterEnd   : true
                //     },
                //     draggable: true,
                //     meta     : {
                //         location: 'Los Angeles',
                //         notes   : 'Eos eu verear adipiscing, ex ornatus denique iracundia sed, quodsi oportere appellantur an pri.'
                //     }
                // },
                // {
                //     start    : new Date('2018-06-08T19:10:00.0000Z'),
                //     end      : new Date('2018-06-08T19:30:00.0000Z'),
                //     title    : '10 - ',
                //     allDay   : false,
                //     color    : {
                //         primary  : '#1e90ff',
                //         secondary: '#D1E8FF'
                //     },
                //     resizable: {
                //         beforeStart: true,
                //         afterEnd   : true
                //     },
                //     draggable: true,
                //     meta     : {
                //         location: 'Los Angeles',
                //         notes   : 'Eos eu verear adipiscing, ex ornatus denique iracundia sed, quodsi oportere appellantur an pri.'
                //     }
                // },
                // {
                //     start    : new Date('2018-06-08T19:11:00.0000Z'),
                //     end      : new Date('2018-06-08T19:30:00.0000Z'),
                //     title    : '11 - ',
                //     allDay   : false,
                //     color    : {
                //         primary  : '#e3bc08',
                //         secondary: '#FDF1BA'
                //     },
                //     resizable: {
                //         beforeStart: true,
                //         afterEnd   : true
                //     },
                //     draggable: true,
                //     meta     : {
                //         location: 'Los Angeles',
                //         notes   : 'Eos eu verear adipiscing, ex ornatus denique iracundia sed, quodsi oportere appellantur an pri.'
                //     }
                // },
                // {
                //     start    : new Date('2018-06-08T19:12:00.0000Z'),
                //     end      : new Date('2018-06-08T19:30:00.0000Z'),
                //     title    : '12 - ',
                //     allDay   : false,
                //     color    : {
                //         primary  : '#1e90ff',
                //         secondary: '#D1E8FF'
                //     },
                //     resizable: {
                //         beforeStart: true,
                //         afterEnd   : true
                //     },
                //     draggable: true,
                //     meta     : {
                //         location: 'Los Angeles',
                //         notes   : 'Eos eu verear adipiscing, ex ornatus denique iracundia sed, quodsi oportere appellantur an pri.'
                //     }
                // },
                // {
                //     start    : new Date('2018-06-08T19:13:00.0000Z'),
                //     end      : new Date('2018-06-08T19:30:00.0000Z'),
                //     title    : '13 - ',
                //     allDay   : false,
                //     color    : {
                //         primary  : '#e3bc08',
                //         secondary: '#FDF1BA'
                //     },
                //     resizable: {
                //         beforeStart: true,
                //         afterEnd   : true
                //     },
                //     draggable: true,
                //     meta     : {
                //         location: 'Los Angeles',
                //         notes   : 'Eos eu verear adipiscing, ex ornatus denique iracundia sed, quodsi oportere appellantur an pri.'
                //     }
                // },
                // {
                //     start    : new Date('2018-06-08T19:14:00.0000Z'),
                //     end      : new Date('2018-06-08T19:30:00.0000Z'),
                //     title    : '14 - ',
                //     allDay   : false,
                //     color    : {
                //         primary  : '#1e90ff',
                //         secondary: '#D1E8FF'
                //     },
                //     resizable: {
                //         beforeStart: true,
                //         afterEnd   : true
                //     },
                //     draggable: true,
                //     meta     : {
                //         location: 'Los Angeles',
                //         notes   : 'Eos eu verear adipiscing, ex ornatus denique iracundia sed, quodsi oportere appellantur an pri.'
                //     }
                // },
                // {
                //     start    : new Date('2018-06-08T19:15:00.0000Z'),
                //     end      : new Date('2018-06-08T19:30:00.0000Z'),
                //     title    : '15 - ',
                //     allDay   : false,
                //     color    : {
                //         primary  : '#e3bc08',
                //         secondary: '#FDF1BA'
                //     },
                //     resizable: {
                //         beforeStart: true,
                //         afterEnd   : true
                //     },
                //     draggable: true,
                //     meta     : {
                //         location: 'Los Angeles',
                //         notes   : 'Eos eu verear adipiscing, ex ornatus denique iracundia sed, quodsi oportere appellantur an pri.'
                //     }
                // },


                // {
                //     start    : startOfDay(new Date(2018,5,3)),
                //     end      : addDays(new Date(2018,5,3),0),
                //     title    : 'My custom event 2',
                //     allDay   : false,
                //     color    : {
                //         primary  : '#1e90ff',
                //         secondary: '#D1E8FF'
                //     },
                //     resizable: {
                //         beforeStart: true,
                //         afterEnd   : true
                //     },
                //     draggable: true,
                //     meta     : {
                //         location: 'Los Angeles',
                //         notes   : 'Eos eu verear adipiscing, ex ornatus denique iracundia sed, quodsi oportere appellantur an pri.'
                //     }
                // }
                // {
                //     start    : addHours(startOfDay(new Date()), 2),
                //     end      : new Date(),
                //     title    : 'A draggable and resizable event',
                //     allDay   : false,
                //     color    : {
                //         primary  : '#e3bc08',
                //         secondary: '#FDF1BA'
                //     },
                //     resizable: {
                //         beforeStart: true,
                //         afterEnd   : true
                //     },
                //     draggable: true,
                //     meta     : {
                //         location: 'Los Angeles',
                //         notes   : 'Eos eu verear adipiscing, ex ornatus denique iracundia sed, quodsi oportere appellantur an pri.'
                //     }
                // }
            ]
        }
        }
    ];
}
