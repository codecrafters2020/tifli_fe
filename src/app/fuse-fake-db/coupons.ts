export class CouponFakeDb
{
    public static coupons = {
  		'data': [
						{
						    "code": 1,
						    "merchant":{
						    	"name": "Merchant"
						    },
						    "revenue": 300,
						    "clicks": 7,
						    "orders": 12,
						    "type": "???",
						    "title":"In-Store: $20 Off $50+ or Up to an Extra 20% Off for Macy's Cardholders",
						    "startDate":"2017-12-12T08:00:00Z",
						    "endDate":"2017-12-13T22:00:59Z",
						    "description":"Click through to get started.",
						    "tags":"",
						    "redirectUrl":"http://click.linksynergy.com/fs-bin/click?id=vl0mfKZlvKU&offerid=349860.100241472&type=3&subid=0",
						    "ongoingOffer":false,
						    "exclusiveOffer":false,
						    "freeship":false,
						    "status":"active",
						    "alertsEligible":false,
						    "restrictions":"Restrictions apply.",
						    "priority":{
						        "id":"",
						        "title":"",
						        "dateCreated":"",
						        "dateModified":"",
						        "createdBy":"",
						        "modifiedBy":""
						    },
						    "url":"/coupon-codes/go/rs?k=1582606_3",
						    "couponType":{
						        "id":"",
						        "code":"",
						        "title":"",
						        "status":"",
						        "dateCreated":"",
						        "dateModified":"",
						        "createdBy":"",
						        "modifiedBy":""
						    },
						    "originalPrice":"",
						    "dealPrice":"",
						    "savings":"0%",
						    "CouponCategory":{

						    },
						    "showSavings":false,
						    "customButtonText":"",
						    "hideExpirationDate":false,
						    "excludeSavingsFromAlgo":false,
						    "overrideSavingsInTitle":false,
						    "overrideSavingsInPod":false,
							"isCashbackCoupon":false,
						    "metaType":"sale",
						    "metaSaving":"",
						    "ImageName":"",
						    "createdBy":"",
						    "dateCreated":"20-5-017",
						    "modifiedBy":"",
						    "dateModified":""
						}
  			]
    };
}
