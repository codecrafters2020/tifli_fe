export class PaidPlacementsFakeDb
{
    public static paidPlacements = {
        'data': [
            {
                'placement_id'     : '2469',
                'coupon_id'     : '1284925_3',
                'merchant_name'    : 'Best Buy',
                'coupon_title'     : 'Up to 100% off',
                'seasonal_page' : '1',
                'price_paid'   : '9.99',
                'startDate'  : 'July 8, 2017',
                'endDate': 'Aug 31, 2099',
            }
        ]
    };

}
